#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <limits.h>

#include "pcg_variants.h"
#include "fractal-utils.h"

static _logging_details *logging_details;

/*******************************************************************************
 *
 * fractal-utils.c
 *
 *  Utility functions for generating fractals
 *
 ******************************************************************************/

/*************************************************
 * Decide if the given point is in the P1 cardioid
 ************************************************/

BOOL inP1Cardioid(
    _complex    c
) {
    long double v1;
    long double a1;
    long double y1;
    long double v2;
    long double a2;
    long double y2;

    v1 = ( 1 - sqrt( 3 - 8 * c.x ) ) / 2;
    if ( ( v1 < -1 ) || ( v1 > 1 ) ) 
        return FALSE;

    a1 = acosl( v1 );
    y1 = ( sin( a1 ) / 2.0 ) - ( sin( 2.0 * a1 ) / 4.0 );

    if ( c.y < 0 )
        c.y *= -1;

    if ( c.x < 0.25 )
        return ( c.y < y1 ) ? TRUE: FALSE;

    v2 = ( 1 + sqrt( 3 - 8 * c.x ) ) / 2;
    a2 = acosl( v2 );
    y2 = ( sin( a2 ) / 2.0 ) - ( sin( 2.0 * a2 ) / 4.0 );

    return ( c.y < y1 ) && ( c.y > y2 ) ? TRUE: FALSE;
 }

/*************************************************
 * Decide if the given point is in the P2 bulb
 ************************************************/

BOOL inP2Bulb(
    _complex    c
) {
    double c_dist;

    c_dist = ( c.x + 1 ) * ( c.x + 1 ) + c.y * c.y;

    return c_dist > 0.0625 ? FALSE : TRUE;  // .0625 = 0.25 * 0.25
}

/*************************************************
 * Return a bounded (+ve) random number
 ************************************************/

long double boundedRand(
    long double upper_bound
) {
    static pcg64_random_t *rng;
    uint64_t n;

    if( rng == NULL ) {

        if ( ( rng = ALLOC( pcg64_random_t ) ) == NULL )
            logFatal( "Could not calloc() the random num generator" );

        pcg64_srandom_r(rng, 42u, 54u);
    }

    n = pcg64_random_r( rng );

    return (long double)(n) * upper_bound / ULLONG_MAX;
}

/*************************************************
 * Process an integer parameter
 ************************************************/

void getCharParameter(
    int     argc,
    char    **argv,
    int     pnt,
    char    **value,
    char    *desc
) {
    if ( pnt < argc ) {
        *value = argv[ pnt ];
    }
    else {
        logFatal( "Missing the %s parameter", desc );
    }
}

/*************************************************
 * Process an integer parameter
 ************************************************/

void getIntParameter(
    int     argc,
    char    **argv,
    int     pnt,
    int     *value,
    char    *desc
) {
    if ( pnt < argc ) {
        *value = atoi( argv[ pnt ] );
    }
    else {
        logFatal( "Missing the %s parameter", desc );
    }
}

/*************************************************
 * Process a long double parameter
 ************************************************/

void getDoubleParameter(
    int     argc,
    char    **argv,
    int     pnt,
    long double  *value,
    char    *desc
) {
    if ( pnt < argc ) {
        *value = atof( argv[ pnt ] );
    }
    else {
        logFatal( "Missing the %s parameter", desc );
    }
}

/*************************************************
 * Setup the logging environment
 ************************************************/

void initialiseLogging(
    char                *logfile,
    _logging_level      logging_level
) {

    if (
        ( logging_details == NULL ) &&
        ( ( logging_details = ALLOC( _logging_details ) ) == NULL )
    )
        logFatal( "Failed to calloc() logging_details" );

    logging_details->logging_level = logging_level;

    logging_details->log_fp = NULL;
}

/*************************************************
 * Cleanup logging
 ************************************************/

void cleanupLogging() {

    if ( logging_details == NULL )
        return;

    if ( logging_details->log_fp != NULL )
        fclose( logging_details->log_fp );

    free( logging_details );

    logging_details = NULL;
}

/*************************************************
 * Get the logging level
 ************************************************/

_logging_level getLoggingLevel() {

    return logging_details->logging_level;
}

/*************************************************
 * Set the logging level
 ************************************************/

void setLoggingLevel(
    _logging_level      logging_level
) {

    logging_details->logging_level = logging_level;
}

/*************************************************
 * Setup a log file
 ************************************************/

void setLogFile(
    char    *log_file
) {
    if ( ( logging_details->log_fp = fopen( log_file, "w" ) ) == NULL )
        logFatal( "Unable to open the log file" );

}

/*************************************************
 * Simple logger
 ************************************************/

void _log(
    char    *format,
    va_list ap,
    char    *eol
) {

    if ( logging_details->logging_level == LOGGING_LEVEL_QUIET )
        return;

    if ( logging_details->log_fp != NULL ) {
        vfprintf( logging_details->log_fp, format, ap );
        fprintf( logging_details->log_fp, "%s", eol );
        fflush( logging_details->log_fp );
    }
    else {
        vprintf( format, ap );
        printf( "%s", eol );
        fflush( stdout );
    }

    va_end( ap );
}

/*************************************************
 * Log message
 ************************************************/

void logMsg(
    char *format,
    ...
) {
    va_list ap;

    va_start( ap, format );
    _log( format, ap, "\n" );
}

/*************************************************
 * Log message with no eol
 ************************************************/

void logTxt(
    char *format,
    ...
) {
    va_list ap;

    va_start( ap, format );
    _log( format, ap, "" );
}

/*************************************************
 * Log and exit
 ************************************************/

void logFatal(
    char *format,
    ...
) {
    va_list ap;

    va_start( ap, format );
    _log( format, ap, "\n" );

    // print message even if in quiet mode
    if ( logging_details->logging_level == LOGGING_LEVEL_QUIET ) {
        vprintf( format, ap );
        printf( "\n" );
        va_end( ap );
    }

    exit( -1 );
}

/*************************************************
 * Remove an item from a list
 ************************************************/

void releaseListItem(
    _list_item  *item,
    _list       *list
) {
    
    if ( item == list->first )
        list->first = item->next;

    if ( item == list->last )
        list->last = item->prev;

    if ( item->prev != NULL ) 
        item->prev->next = item->next;
    
    if ( item->next != NULL ) 
        item->next->prev = item->prev;

    if ( list->free_func != NULL )
        list->free_func( item->data );

    list->count--;

    free( item );
}

/*************************************************
 * Initialise a list
 ************************************************/

_list *initialiseList( void (*free_func)(void *) ) {

    _list *list;

    if ( ( list = ALLOC( _list ) ) == NULL )
        logFatal( "Failed to calloc list " );

    list->free_func = free_func;

    return list;
}

/*************************************************
 * Release a list
 ************************************************/

void releaseList(
    _list *list
) {

    emptyList( list );
    free( list );
}

/*************************************************
 * Empty a list
 ************************************************/

void emptyList(
    _list *list
) {
    _list_item      *item;

    while( ( item = list->last ) != NULL )
        releaseListItem( item, list );
}

/*************************************************
 * Add data to a list
 ************************************************/

void addToList(
    void    *data,
    _list   *list
) {
    
    _list_item *item;

    if ( ( item = ALLOC( _list_item ) ) == NULL )
        logFatal( "Could not calloc() a list item" );

    item->next = NULL;
    item->data = data;

    if ( list->count > 0 ) {
        
        item->prev = list->last;
        list->last->next = item;
        list->last = item;
    }
    else {

        list->first = item;    
        list->last = item;    
    }

    list->count++;
}

/*************************************************
 * Return data from a list by its position
 ************************************************/

void *getFromListByPos(
    short   pos,
    _list   *list
) {

    _list_item  *item;

    if ( pos < 0 )
        return NULL;

    if ( pos < list->count ) {

        for( item = list->first; item != NULL; item = item->next ) {
            if ( pos == 0 )
                return item->data;
            pos--;
        }
    }

    return NULL;
}

/*************************************************
 * Return a data from a list by its data addr
 ************************************************/

void *getFromListByData(
    void    *addr,
    _list   *list
) {

    _list_item  *item;

    for( item = list->first; item != NULL; item = item->next ) {
        if ( item->data == addr )
            return item->data;
    }

    return NULL;
}

/*************************************************
 * Find list item position of the given string
 ************************************************/

short findStringInList(
    char    *string,
    _list   *list
) {

    short   pos;
    char    *list_string;

    if ( *string == '\0' )
        return -1;

    for( pos = 0; pos < list->count; pos++ ) {

        list_string = getFromListByPos( pos, list );

        if ( strcmp( list_string, string ) == 0 )
            return pos;
    }

    return -1;
}

/*************************************************
 * Return the sin() of the value given degrees
 ************************************************/

long double sinDeg(
    long double  degrees
) {
    return sin( 2.0 * PI * degrees / 360.0 ); 
}

/*************************************************
 * Find the graticuleSeparation
 ************************************************/

long double getGraticuleSeparation(

    long double     image_xc,
    long double     image_yc,
    _orientation    orientation,
    long double     image_xsz,
    long double     image_ysz,
    long double     image_sz
) {
    long double     axis_min;
    long double     axis_max;
    long double     scale;
    long double     scaled_min;
    long double     scaled_max;
    long double     range;
    long double     value;
    long double     step;

    // Work out the separation of the graticule lines
    if ( orientation == LANDSCAPE ) {
        axis_min = image_xc - image_xsz;
        axis_max = image_xc + image_xsz;
    }
    else {
        axis_min = image_yc - image_ysz;
        axis_max = image_yc + image_ysz;
    }

    value = log( 2.0 * image_sz ) / log( 10.0 );
    if ( ( value < 0 ) && ( value != (int)value ) )
        value--;

    scale = pow( 10, (int)value );

    scaled_min = axis_min / scale;
    scaled_max = axis_max / scale;

    range = ceil( scaled_max - scaled_min );

    step = scale * (
                ( range < 2.0 ) ? 0.1
                : ( range < 3.0 ) ? 0.2
                : ( range < 6.0 ) ? 0.5
                : 1.0
    );

    return step;
}
