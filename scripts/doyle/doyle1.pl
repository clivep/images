#!/usr/bin/perl

use warnings;
use strict;

############################################################
#
#   doyle.pl
#
#   Template for image making
#
############################################################

use Math::Trig;
use Getopt::Std;

use Data::Dumper;

############################################################
# The global operating details

my $IMAGE = {
    'DISCS' =>  [],
};

############################################################
# Check the runline options

my %options;
getopts( 'hi:mD:IF:K:LS:W:X:Y:', \%options );

help() if $options{ 'h' };

help( 'Missing <rad1> and <rad2> paramters' ) unless $ARGV[ 0 ];
help( 'Missing <rad2> paramter' ) unless $ARGV[ 1 ];

$IMAGE->{ 'RAD1' } = $ARGV[ 0 ];
$IMAGE->{ 'RAD2' } = $ARGV[ 1 ];

$IMAGE->{ 'INIT_RAD' } = 1;

$IMAGE->{ 'MOBIUS_XFORM' } = $options{ 'm' };

$IMAGE->{ 'MAX_ITERS' } = $options{ 'i' } || 1;
$IMAGE->{ 'ITERS' } = 0;

$IMAGE->{ 'TEMP_PS' } = $options{ 'K' } || 'doyle.ps';
$IMAGE->{ 'DEST' } = $options{ 'F' } || 'doyle.png';

$IMAGE->{ 'SCALE' } = $options{ 'S' } || 40;

$IMAGE->{ 'DEPTH' } = $options{ 'W' } || 1000;
$IMAGE->{ 'WIDTH' } = $options{ 'D' } || 1000;

$IMAGE->{ 'XOFF' } = $options{ 'X' } || $IMAGE->{ 'DEPTH' } / 2;
$IMAGE->{ 'YOFF' } = $options{ 'Y' } || $IMAGE->{ 'WIDTH' } / 2;

$IMAGE->{ 'CURRENT_DISC' } = 0;

############################################################
# Setup the environment

my $RAD_TYPES = [
    { 'DESC'    => '  A', 'A-PWR'   =>  1, 'B-PWR'  =>  0 },
    { 'DESC'    => '  B', 'A-PWR'   =>  0, 'B-PWR'  =>  1 },
    { 'DESC'    => 'B/A', 'A-PWR'   => -1, 'B-PWR'  =>  1 },
    { 'DESC'    => '1/A', 'A-PWR'   => -1, 'B-PWR'  =>  0 },
    { 'DESC'    => '1/B', 'A-PWR'   =>  0, 'B-PWR'  => -1 },
    { 'DESC'    => 'A/B', 'A-PWR'   =>  1, 'B-PWR'  => -1 },
];

############################################################
# Process the image

processImage();
drawImage() unless $options{ 'I' };
dumpData() if $options{ 'I' };

exit;

############################################################
# Display the image data

sub dumpData {

    for( my $d = 1; $d < scalar @{ $IMAGE->{ 'DISCS' } }; $d++ ) {

        my $disc = $IMAGE->{ 'DISCS' }->[ $d ];

        printf "%3d - x: % 0.3f y: % 0.3f t: A% 3d B% 3d [",
                        $d,
                        $disc->{ 'X' },
                        $disc->{ 'Y' },
                        $disc->{ 'A-PWR' },
                        $disc->{ 'B-PWR' },
        ;

        for ( my $n = 0; $n < 6; $n++ ) {
            my $neighbour = $disc->{ 'NEIGHBOURS' }->[ $n ];
            printf "%s %s", $n ? ',' : '', $neighbour < 0 ? '-' : $neighbour;
        }

        printf " ]\n";
    }

    return;
}

############################################################
# Add a disc to the image list

sub addDisc {

    my $x = shift;
    my $y = shift;
    my $a_pwr = shift;
    my $b_pwr = shift;
    my $neighbours = shift || [ (-1)x6 ],

    my $rad = _processPwr( 1, $a_pwr, $IMAGE->{ 'RAD1' } );
    $rad = _processPwr( $rad, $b_pwr, $IMAGE->{ 'RAD2' } );

    my $disc = {
        'ID'            =>  scalar @{ $IMAGE->{ 'DISCS' } },
        'X'             =>  $x,
        'Y'             =>  $y,
        'RAD'           =>  $rad,
        'A-PWR'         =>  $a_pwr,
        'B-PWR'         =>  $b_pwr,
        'NEIGHBOURS'    =>  $neighbours,
    };

    push @{ $IMAGE->{ 'DISCS' } }, $disc;

    return $disc;
}

############################################################
# Find the angle between the prev and new centres

sub findNewCentreAngle {

    my $curr_disc = shift;
    my $prev_disc = shift;
    my $new_rad = shift;

    # Some shortcuts
    my $cr = $curr_disc->{ 'RAD' };
    my $pr = $prev_disc->{ 'RAD' };

    my $cn = $cr + $new_rad;
    my $cp = $cr + $pr;
    my $np = $new_rad + $pr;

    my $num = ( $cp * $cp ) + ( $cn * $cn ) - ( $np * $np );
    my $den = 2 * $cp * $cn;

    return acos( ( ( $cp * $cp ) + ( $cn * $cn ) - ( $np * $np ) ) / ( 2 * $cp * $cn ) );
}

############################################################
# Calculate the radius given a disc/rad_type

sub _processPwr {

    my $val = shift;
    my $pwr = shift;
    my $rad = shift;

    return $val unless $pwr;

    while ( $pwr ) {

        if ( $pwr < 0 ) {
            $val = $val / $rad;
            $pwr++;
        }
        else {
            $val = $val * $rad;
            $pwr--
        }
    }

    return $val;
}

sub caclulateRadius {

    my $rad = shift;
    my $multiplier = shift;     # maybe a disc or a rad_type

    $rad = _processPwr( $rad, $multiplier->{ 'A-PWR' }, $IMAGE->{ 'RAD1' } );
    return _processPwr( $rad, $multiplier->{ 'B-PWR' }, $IMAGE->{ 'RAD2' } );
}

############################################################
# Find the rad_type associated with the given powers

sub _findRadType {

    my $a_pwr = shift;
    my $b_pwr = shift;

    for ( my $r = 0; $r < 6; $r++ ) {
    
        return $r if (
            ( $RAD_TYPES->[ $r ]->{ 'A-PWR' } == $a_pwr ) &&
            ( $RAD_TYPES->[ $r ]->{ 'B-PWR' } == $b_pwr )
        );
    }

    printf "Invalid rad_type lookup: %d/%d\n", $a_pwr, $b_pwr;
    exit -1;
}

############################################################
# Add the discs to each others neighbour lists

sub addAsNeighbour {

    my $disc1 = shift;
    my $disc2 = shift;

    # The initial disc doesn't have a radius type...
    unless ( $disc1->{ 'ID' } ) {

        my $p = _findRadType(
            -1 * $disc2->{ 'A-PWR' },
            -1 * $disc2->{ 'B-PWR' },
        );
    
        $disc2->{ 'NEIGHBOURS' }->[ $p ] = $disc1->{ 'ID' };

        my $q = _findRadType(
            $disc2->{ 'A-PWR' },
            $disc2->{ 'B-PWR' },
        );
    
        $disc1->{ 'NEIGHBOURS' }->[ $q ] = $disc2->{ 'ID' };
    }

    my $p = _findRadType( 
            $disc1->{ 'A-PWR' } - $disc2->{ 'A-PWR' },
            $disc1->{ 'B-PWR' } - $disc2->{ 'B-PWR' },
    );

    $disc2->{ 'NEIGHBOURS' }->[ $p ] = $disc1->{ 'ID' };

    my $q = _findRadType( 
            $disc2->{ 'A-PWR' } - $disc1->{ 'A-PWR' },
            $disc2->{ 'B-PWR' } - $disc1->{ 'B-PWR' },
    );

    $disc1->{ 'NEIGHBOURS' }->[ $q ] = $disc2->{ 'ID' };
}

############################################################
# Process the initial disc

sub processInitialDisc {

    my $init_disc = addDisc( 0, 0, 0, 0 );

    my $first_disc;
    my $prev_disc;
    my $ang;
    for( my $r = 0; $r < 6; $r++ ) {

        if ( $r ) {

            my $new_rad = caclulateRadius( $init_disc->{ 'RAD' }, $RAD_TYPES->[ $r ] );
            $ang += findNewCentreAngle( $init_disc, $prev_disc, $new_rad );

            my $new_disc = addDisc(
                ( $new_rad + $init_disc->{ 'RAD' } ) * cos( $ang ),
                ( $new_rad + $init_disc->{ 'RAD' } ) * sin( $ang ),
                $RAD_TYPES->[ $r ]->{ 'A-PWR' },
                $RAD_TYPES->[ $r ]->{ 'B-PWR' },
            );

            addAsNeighbour( $init_disc, $new_disc );
            addAsNeighbour( $prev_disc, $new_disc );

            $prev_disc = $new_disc;
        } 
        else {

            my $new_disc = addDisc( 
                1 + $IMAGE->{ 'RAD1' }, 
                0, 
                $RAD_TYPES->[ $r ]->{ 'A-PWR' },
                $RAD_TYPES->[ $r ]->{ 'B-PWR' },
            );

            addAsNeighbour( $init_disc, $new_disc );

            $first_disc= $new_disc;
            $prev_disc= $new_disc;
        }
    }

    addAsNeighbour( $first_disc, $prev_disc );

    $IMAGE->{ 'CURRENT_DISC' }++;
    $IMAGE->{ 'ITERS' }++;

    return;
}

############################################################
# Find the last anticlockwise neighbour

sub getLastNeighbourType {

    my $disc = shift;

    my $neighbours = $disc->{ 'NEIGHBOURS' };

    for ( my $n = 0; $n < 6; $n++ ) {

        return $n if ( $neighbours->[ $n ] >= 0 ) && ( $neighbours->[ $n - 5 ] < 0 );
    }

    printf "Failed to find a last empty neighbour for disc %d?\n", $disc->{ 'ID' };

    return -1;
}

############################################################
# Find the first anticlockwise neighbour

sub getFirstNeighbourType {

    my $disc = shift;

    my $neighbours = $disc->{ 'NEIGHBOURS' };

    for ( my $n = 0; $n < 6; $n++ ) {

        return $n if ( $neighbours->[ $n ] >= 0 ) && ( $neighbours->[ $n - 1 ] < 0 );
    }

    printf "Failed to find a first empty neighbour for disc %d?\n", $disc->{ 'ID' };

    return -1;
}

############################################################
# Process the image

sub processImage {

    processInitialDisc();    # Ring the initial disc to get things started

    while ( $IMAGE->{ 'ITERS' } < $IMAGE->{ 'MAX_ITERS' } ) {

        my $disc_count = scalar @{ $IMAGE->{ 'DISCS' } };

        for( my $d = $IMAGE->{ 'CURRENT_DISC' }; $d < $disc_count; $d++ ) {

            my $disc = $IMAGE->{ 'DISCS' }->[ $IMAGE->{ 'CURRENT_DISC' } ];
            my $neighbours = $disc->{ 'NEIGHBOURS' };

            my $first_type = getFirstNeighbourType( $disc );
            my $first_disc = $IMAGE->{ 'DISCS' }->[ $neighbours->[ $first_type ] ];

            my $last_type = getLastNeighbourType( $disc );
            my $last_disc = $IMAGE->{ 'DISCS' }->[ $neighbours->[ $last_type ] ];

            my $ang = atan2(
                $last_disc->{ 'Y' } - $disc->{ 'Y' },
                $last_disc->{ 'X' } - $disc->{ 'X' },
            );

            for ( my $n = 0; $n < 6; $n++ ) {

                my $rad_type = ( $last_type + $n + 1 ) % 6;
                next if $neighbours->[ $rad_type ] >= 0;

                my $new_rad = caclulateRadius( $disc->{ 'RAD' }, $RAD_TYPES->[ $rad_type ] );
                $ang += findNewCentreAngle( $disc, $last_disc, $new_rad );

                my $new_disc = addDisc(
                    $disc->{ 'X' } + ( $new_rad + $disc->{ 'RAD' } ) * cos( $ang ),
                    $disc->{ 'Y' } + ( $new_rad + $disc->{ 'RAD' } ) * sin( $ang ),
                    $disc->{ 'A-PWR' } + $RAD_TYPES->[ $rad_type ]->{ 'A-PWR' },
                    $disc->{ 'B-PWR' } + $RAD_TYPES->[ $rad_type ]->{ 'B-PWR' },
                );

                addAsNeighbour( $new_disc, $disc );
                addAsNeighbour( $new_disc, $last_disc );

                $last_disc = $new_disc;
            }

            addAsNeighbour( $first_disc, $last_disc );

            $IMAGE->{ 'CURRENT_DISC' }++;
        }

        $IMAGE->{ 'ITERS' }++;
    }
}

############################################################
# Write the image header

sub writeImageHeader {

    my $fh = shift;

    printf $fh "%%!PS\n%%%%BoundingBox: 0 0 %f %f\n%%%%EndComments\n

        /imgw %f def
        /imgd %f def

        <<
            /PageSize [ imgw imgd ]
            /ImagingBBox null
        >> setpagedevice

        /mv { moveto } bind def
        /ln { lineto } bind def

        0 0 mv imgw 0 ln imgw imgd ln 0 imgd ln fill

        %f %f translate

        /Arial-Bold findfont 14 scalefont setfont

        1 setlinejoin
        1 1 0 setrgbcolor
    ",
        $IMAGE->{ 'WIDTH' }, $IMAGE->{ 'DEPTH' },
        $IMAGE->{ 'WIDTH' }, $IMAGE->{ 'DEPTH' },
        $IMAGE->{ 'XOFF' }, $IMAGE->{ 'YOFF' },
    ;

    return;
}

############################################################
# Apply the mobuis transformation to a point

sub mobiusTransformPoint {

    my $x = shift;
    my $y = shift;

    my $v = ( ( $x + 1 ) *( $x + 1 ) ) + ( $y * $y );

    return [ 10000, 10000 ] unless $v;

    return [
        ( ( $x * $x ) + ( $y * $y ) - 1 ) / $v, 
        2 * $y / $v,
    ];
}

############################################################
# Apply the mobuis transformation to a disc

sub mobiusTransformDisc {

    my $disc = shift;

    my $pt1 = mobiusTransformPoint( $disc->{ 'X' } + $disc->{ 'RAD' }, $disc->{ 'Y' } );
    my $pt2 = mobiusTransformPoint( $disc->{ 'X' } - $disc->{ 'RAD' }, $disc->{ 'Y' } );
    my $pt3 = mobiusTransformPoint( $disc->{ 'X' }, $disc->{ 'Y' } + $disc->{ 'RAD' } );

    my $n1 = ( $pt1->[ 0 ] * $pt1->[ 0 ] ) + ( $pt1->[ 1 ] * $pt1->[ 1 ] );
    my $n2 = ( $pt2->[ 0 ] * $pt2->[ 0 ] ) + ( $pt2->[ 1 ] * $pt2->[ 1 ] );
    my $n3 = ( $pt3->[ 0 ] * $pt3->[ 0 ] ) + ( $pt3->[ 1 ] * $pt3->[ 1 ] );

    my $v1 = ( $pt1->[ 0 ] - $pt2->[ 0 ] ) * ( $n2 - $n3 ) -
                        ( $pt2->[ 0 ] - $pt3->[ 0 ] ) * ( $n1 - $n2 );

    my $v2 = ( $pt2->[ 1 ] - $pt1->[ 1 ] ) * ( $pt2->[ 0 ] - $pt3->[ 0 ] ) - 
                        ( $pt1->[ 0 ] - $pt2->[ 0 ] ) * ( $pt3->[ 1 ] - $pt2->[ 1 ] );

    $disc->{ 'Y' } = $v1 / ( 2 * $v2 );
    
    $disc->{ 'X' } = ( $n1 - $n2 + 2 * ( $pt2->[ 1 ] - $pt1->[ 1 ] ) * $disc->{ 'Y' } ) /
                        ( 2 * ( $pt1->[ 0 ] - $pt2->[ 0 ] ) );
    $disc->{ 'RAD' } = sqrt( 
                ( $disc->{ 'X' } - $pt1->[ 0 ] ) * ( $disc->{ 'X' } - $pt1->[ 0 ] ) + 
                ( $disc->{ 'Y' } - $pt1->[ 1 ] ) * ( $disc->{ 'Y' } - $pt1->[ 1 ] )
    );
}

############################################################
# Write the image details

sub writeImageDetail {

    my $fh = shift;

    #printf $fh " 0 0 mv 0 10 ln 0 0 mv 0 -10 ln 0 0 mv 10 0 ln 0 0 mv -10 0 ln stroke\n";

    for( my $d = 0; $d < scalar @{ $IMAGE->{ 'DISCS' } }; $d++ ) {

        my $disc = $IMAGE->{ 'DISCS' }->[ $d ];

        mobiusTransformDisc( $disc ) if $IMAGE->{ 'MOBIUS_XFORM' };

        for( my $a = 0; $a <= 360; $a += 2 ) {

            printf $fh "%f %f %s ", 
                $IMAGE->{ 'SCALE' } * ( $disc->{ 'X' } + ( $disc->{ 'RAD' } * cos( deg2rad( $a ) ) ) ),
                $IMAGE->{ 'SCALE' } * ( $disc->{ 'Y' } + ( $disc->{ 'RAD' } * sin( deg2rad( $a ) ) ) ),
                $a ? 'ln' : 'mv',
            ;
        }

        printf $fh ( "gsave
                        1 setgray
                        %f %f moveto (%d) 
                        dup stringwidth pop -2 div -5 rmoveto 
                        show
                        grestore\n",
                            $IMAGE->{ 'SCALE' } * $disc->{ 'X' },
                            $IMAGE->{ 'SCALE' } * $disc->{ 'Y' }, 
                            $d,

        ) if ( $d && $options{ 'L' } );

        printf $fh "stroke\n";
    }

    return;
}

############################################################
# Draw the image

sub drawImage {

    open my $fh, '>', $IMAGE->{ 'TEMP_PS' } or do {
        printf "Could not open %s: $!\n", $IMAGE->{ 'TEMP_PS' };
        exit -1;
    };

    writeImageHeader( $fh );
    writeImageDetail( $fh );

    close $fh;

    my $convert = "/usr/local/bin/convert";
    $convert = "/usr/bin/convert" unless ( -f $convert );
    $convert = "/Applications/ImageMagick/ImageMagick-6.6.5/bin/convert" unless ( -f $convert );

    unless ( -f $convert ) {

        printf "Could not locate convert at $convert\n";
    }
    else {

        unlink $IMAGE->{ 'DEST' };
        my $cmd = sprintf "$convert '%s' '%s'", $IMAGE->{ 'TEMP_PS' }, $IMAGE->{ 'DEST' };
        system( $cmd );
    }

    unlink $IMAGE->{ 'TEMP_PS' } unless $options{ 'K' };

    return;
}

############################################################
# Help doco

sub help {

    print "\n@_\n" if @_;

    printf "\n$0 " .
                "[-h] [-D <depth>] [-F <file>] [-K <file>] [-S <scale>] [-W <width>] <rad1> <rad2>

        where
            <rad1>      radius of the first disc
            <rad2>      radius of the second disc

            -h          this help message

            -D <depth>  image depth (default 1000)
            -F <file>   output file (default 'doyle.png')
            -K <file>   temp ps file to retain
            -L          label the discs
            -S <scale>  drawing scale (default 40)
            -W <width>  image width (default 1000)
            -X <xoff>   x offset for the image (default width / 2)
            -Y <yoff>   y offset for the image (default depth / 2)

    \n";

    exit;
}
