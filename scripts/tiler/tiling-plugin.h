/*******************************************************************************
 *
 * tiling-plugin.h
 *
 *  Methods exposed by the tiling plugins
 *
 ******************************************************************************/

#ifndef _TILING_PLUGIN_H

#include "tiling-geometry.h"

void loadPlugin( char *plugin, _logging_level logging_level );
void releasePlugin();

short initialisePlugin( _logging_level logging_level );
void cleanupPlugin();

void drawTiling( char *json_path, double exclusion_radius, char *pdf_path ); 
short polygonTypeEdges( int type );
_polygon *getTemplateByType( int type );
_polygon *getTemplateByPos( short pos );
short getTemplateCount();
char polygonTypeChar( _polygon *polygon );
short findTemplateTypePos( int type );
char *getSideTypeDescription( int pos );
short checkForValidSides( _polygon *polygon1, short edge1,
                                _polygon *polygon2, short edge2 );
int getSideTypeCount();

#define _TILING_PLUGIN_H
#endif
