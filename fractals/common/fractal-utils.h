/*******************************************************************************
 *
 * fractal-utils.h
 *
 *  Definitions for fractal utility functions such as logging and lists
 *
 ******************************************************************************/

#ifndef _FRACTAL_UTILS_H

#include <stdio.h>
#include <stdarg.h>
#include <math.h>

#define PI              3.14159265358979323846

#define ALLOC(typ)      (typ *)calloc( 1, sizeof( typ ) )
#define NALLOC(n,typ)   (typ *)calloc( (n), sizeof( typ ) )

#define STR(str)        #str

#define MIN(x,y)        ( (x)<(y) ? (x):(y) )
#define MAX(x,y)        ( (x)>(y) ? (x):(y) )
#define MOD(z)          ( sqrt( (z.x)*(z.x) + (z.y)*(z.y) ) )

#define DEG2RAD(d)      ( PI * (d) / 180.0 )

#define HEX_VAL(h)      ( ( (h) <='9' ) ? ( (h) - '0' ) \
                            : ( (h) <= 'F' ) ? ( 10 + (h) - 'A' ) \
                            : ( (h) <= 'f' ) ? ( 10 + (h) - 'a' ) \
                            : 0 \
                        )
#define COL_FROM_HEX(h) ( ( ( 16.0 * HEX_VAL(*h) ) + HEX_VAL(*(h+1)) ) / 256.0 )

#define FALSE   0
#define TRUE    1

/*************************************************
 * Description of a point
 ************************************************/
typedef struct {

    int visit_count[ 3 ];

} _visits;

/*************************************************
 * Description of a complex number
 ************************************************/
typedef struct {

    long double x;
    long double y;

} _complex;

/*************************************************
 * Description of an image
 ************************************************/
typedef struct {

    char        *data_file;

    char        *version;
    int         type;

    char        *description;
    char        *trajectory_scheme;

    long double xc;
    long double yc;
    long double sz;

    long double escape;

    int         max_iters;

    int         max_visit_count;

    int         image_points;
    int         bucket_limit1;
    int         bucket_limit2;
    int         bucket_limit3;

    int         pic_wid;
    int         pic_dep;

    _visits      *data;

} _image;

/*************************************************
 * Orientation options
 ************************************************/
typedef enum {

    LANDSCAPE,
    PORTRAIT

} _orientation;

/*************************************************
 * A color
 ************************************************/
typedef struct {

    long double red;
    long double green;
    long double blue;

} _color ;

/*************************************************
 * List item
 ************************************************/
typedef struct _list_item {

    void                *data;
    struct _list_item   *prev;
    struct _list_item   *next;

} _list_item;

/*************************************************
 * List structure
 ************************************************/
typedef struct {

    int         count;
    _list_item  *first;
    _list_item  *last;
    void        (*free_func)(void *);

} _list;

/*************************************************
 * Logging levels
 ************************************************/
typedef enum {

    LOGGING_LEVEL_QUIET,
    LOGGING_LEVEL_DEBUG

} _logging_level;

/*************************************************
 * Loggin configuration
 ************************************************/

typedef struct {

    _logging_level      logging_level;
    FILE                *log_fp;
} _logging_details;

/*************************************************
 * Convenience types
 ************************************************/
typedef char BOOL;

/*************************************************
 * Prototypes
 ************************************************/
void getCharParameter( int argc, char **argv, int pnt, char **value, char *desc );
void getIntParameter( int argc, char **argv, int pnt, int *value, char *desc );
void getDoubleParameter( int argc, char **argv, int pnt, long double *value, char *desc );

void initialiseLogging( char *logfile, _logging_level logging_level );
void cleanupLogging();
void _log( char *format, va_list ap, char *eol );
void logTxt( char *format, ... );
void logMsg( char *format, ... );
void logFatal( char *format, ... );
void setLogFile( char *log_file );
void setLoggingLevel( _logging_level logging_level );
_logging_level getLoggingLevel();

_list *initialiseList( void (*free_func)(void *) ); 
void addToList( void *data, _list *list );
void releaseListItem( _list_item *item, _list *list );
void emptyList( _list *list );
void releaseList( _list *list );
void *getFromListByPos( short pos, _list *list );
void *getFromListByData( void *addr, _list *list );
short findStringInList( char *string, _list *list );

BOOL inP1Cardioid( _complex c );
BOOL inP2Bulb( _complex c );

long double getGraticuleSeparation( long double image_xc, long double image_yc, _orientation orientation,
                                    long double image_xsz, long double image_ysz, long double image_sz );
long double sinDeg( long double  degrees );

long double boundedRand( long double upper_bound );

#define _FRACTAL_UTILS_H
#endif
