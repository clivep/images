#!/usr/bin/perl

use warnings;
use strict;

############################################################
#
#   mobius.pl
#
#   Moubius transformations
#
############################################################

use Data::Dumper;
use Getopt::Std;
use Storable 'dclone';
use Math::Trig;
use Readonly;

############################################################
# Setup the image

my $IMAGE = {

    'DEPTH'     =>  1000,
    'WIDTH'     =>  1000,
    'SCALE'     =>  1.0,

    'MAX_ITERS' =>  0.0,
    'MIN_ITERS' =>  0.0,

    'INIT_TEMPLATE'     =>  {},
    'WORKING_TEMPLATE'  =>  {},
};

############################################################
# Check the runline options

my %options;
getopts( 'hk:m:n:o:D:S:W:', \%options );

help() if $options{ 'h' };

$IMAGE->{ 'PS_TEMP' } = $options{ 'k' } || 'mobius.ps';
$IMAGE->{ 'DEST' } = $options{ 'o' } || 'mobius.pdf';

$IMAGE->{ 'MAX_ITERS' } = $options{ 'm' } || 0;
$IMAGE->{ 'MIN_ITERS' } = $options{ 'n' } || 0;

$IMAGE->{ 'SCALE' } = $options{ 'S' } || 1.0;
$IMAGE->{ 'DEPTH' } = $options{ 'D' } || 1000;
$IMAGE->{ 'WIDTH' } = $options{ 'W' } || 1000;

prepareImage();

setupTemplate();
drawTemplate( $IMAGE->{ 'INIT_TEMPLATE' } );

$IMAGE->{ 'WORKING_TEMPLATE' } = dclone $IMAGE->{ 'INIT_TEMPLATE' };
for( my $i = 0; $i < $IMAGE->{ 'MAX_ITERS' }; $i++ ) {
    processForwardTemplate();
}

$IMAGE->{ 'WORKING_TEMPLATE' } = dclone $IMAGE->{ 'INIT_TEMPLATE' };
for( my $i = 0; $i < $IMAGE->{ 'MIN_ITERS' }; $i++ ) {
    processBackwardTemplate();
}

singleTransform();

renderImage();

exit;

############################################################
# Initialise the initial template

sub setupTemplate {

    my $sz = 0.095;

    # Just a square and a triangle
    $IMAGE->{ 'INIT_TEMPLATE' } = {

        'SCALE'    =>  $sz,
        'LINES'    =>  [
        ],
        'POLYGONS'    =>  [
            [
                { 'X' => -$sz, 'Y' =>  0,   },
                { 'X' =>    0, 'Y' =>  0,   },
                { 'X' =>    0, 'Y' =>  $sz, },
                { 'X' => -$sz, 'Y' =>  $sz, },
            ], [
                { 'X' =>   0, 'Y' =>  $sz / 2, },
                { 'X' => $sz, 'Y' =>        0, },
                { 'X' => $sz, 'Y' =>      $sz, },
            ],
        ],
    };

    # Just a disc and a line
    my @disc;
    for( my $a = 0; $a < 360; $a++ ) {

        push @disc, {
            'X' => $sz * sin( deg2rad( $a ) ),
            'Y' => $sz * cos( deg2rad( $a ) ),
        };
    }

    $IMAGE->{ 'INIT_TEMPLATE' } = {
        'POLYGONS'    =>  [
            \@disc,
            [
                { 'X' => -$sz, 'Y' =>  0,   },
                { 'X' =>  $sz, 'Y' =>  0,   },
            ],
        ],
    };
}

############################################################
# Perform a single transformation on the initial disc

sub singleTransformation {

    
}

############################################################
# Set the value of a complex point

sub setPoint {

    my $pt1 = shift;

    $pt1->{ 'X' } = shift;
    $pt1->{ 'Y' } = shift;
}

############################################################
# copyPoint return a duplicate point

sub copyPoint {

    my $pt1 = shift;

    return {
        'X' =>  $pt1->{ 'X' },
        'Y' =>  $pt1->{ 'Y' },
    }
}

############################################################
# setFromCopy the values of a complex point from another

sub setFromPoint {

    my $pt1 = shift;
    my $pt2 = shift;

    $pt1->{ 'X' } = $pt2->{ 'X' };
    $pt1->{ 'Y' } = $pt2->{ 'Y' };
}

############################################################
# Multiply a complex point by another

sub mulByPoint {

    my $pt1 = shift;
    my $pt2 = shift;

    my $x = ( $pt1->{ 'X' } * $pt2->{ 'X' } ) - ( $pt1->{ 'Y' } * $pt2->{ 'Y' } );
    my $y = ( $pt1->{ 'X' } * $pt2->{ 'Y' } ) + ( $pt1->{ 'Y' } * $pt2->{ 'X' } );

    setPoint( $pt1, $x, $y );
}

############################################################
# Divide a complex point by another

sub divByPoint {

    my $pt1 = shift;
    my $pt2 = shift;

    my $v = ( $pt2->{ 'X' } * $pt2->{ 'X' } ) + ( $pt2->{ 'Y' } * $pt2->{ 'Y' } );

    my $x = ( $pt1->{ 'X' } * $pt2->{ 'X' } ) + ( $pt1->{ 'Y' } * $pt2->{ 'Y' } );
    my $y = ( $pt1->{ 'Y' } * $pt2->{ 'X' } ) - ( $pt1->{ 'X' } * $pt2->{ 'Y' } );

    setPoint( $pt1, $x / $v, $y / $v );
}

############################################################
# Add one complex point to another

sub addToPoint {

    my $pt1 = shift;
    my $pt2 = shift;

    $pt1->{ 'X' } += $pt2->{ 'X' };
    $pt1->{ 'Y' } += $pt2->{ 'Y' };
}

############################################################
# Subtract one complex point from another

sub subFromPoint {

    my $pt1 = shift;
    my $pt2 = shift;

    $pt1->{ 'X' } -= $pt2->{ 'X' };
    $pt1->{ 'Y' } -= $pt2->{ 'Y' };
}

############################################################
# Apply the function to the given point

sub processForwardPoint {

    my $point = shift;

    my $p = { 'X' => 1, 'Y' => -5 };

    my $num = copyPoint( $p );
    mulByPoint( $num, $point );
    $num->{ 'X' } += 1;

    my $den = copyPoint( $p );
    addToPoint( $den, $point );

    divByPoint( $num, $den );

    setFromPoint( $point, $num );
}

############################################################
# Apply the inverse function to the given point

sub processBackwardPoint {

    my $point = shift;

    my $p = { 'X' => -1, 'Y' => 5 };

    my $num = copyPoint( $p );
    mulByPoint( $num, $point );
    $num->{ 'X' } += 1;

    my $den = copyPoint( $p );
    addToPoint( $den, $point );

    divByPoint( $num, $den );

    setFromPoint( $point, $num );
}

############################################################
# Draw the template

sub drawTemplate {

    my $template = shift;

    my $fh = $IMAGE->{ 'FH' };

    map {

        my $op = 'mv';
        map {
            printf $fh "%f %f %s\n",
                        $_->{ 'X' } * $IMAGE->{ 'SCALE' },
                        $_->{ 'Y' } * $IMAGE->{ 'SCALE' }, 
                        $op;
            $op = 'ln';
        } ( @{ $_ } );
        printf $fh "closepath stroke\n";

    } ( @{ $template->{ 'POLYGONS' } } );
}

############################################################
# Process the template by applying the inverse function

sub processBackwardTemplate {

    my $template = $IMAGE->{ 'WORKING_TEMPLATE' };

    map { 
        map {
            processBackwardPoint( $_ )
        } ( @{ $_ } )
    } ( @{ $template->{ 'POLYGONS' } } );

    drawTemplate( $template );
}

############################################################
# Process the template by applying the function

sub processForwardTemplate {

    my $template = $IMAGE->{ 'WORKING_TEMPLATE' };

    map { 
        map {
            processForwardPoint( $_ )
        } ( @{ $_ } )
    } ( @{ $template->{ 'POLYGONS' } } );

    drawTemplate( $template );
}

############################################################
# Prepare the image

sub prepareImage {

    open my $fh, '>', $IMAGE->{ 'PS_TEMP' } or die "Could not open $IMAGE->{ 'PS_TEMP' }: $!\n";
    
    printf $fh "%%!PS\n%%%%BoundingBox: 0 0 1000 1000\n%%%%EndComments\n
    
        /imgw %f def
        /imgd %f def

        <<
            /PageSize [ imgw imgd ]
            /ImagingBBox null
        >> setpagedevice
    
        /mv { moveto } bind def
        /ln { lineto } bind def
    
        /Arial-Bold findfont 16 scalefont setfont

        0 0 mv
        imgw 0 ln
        imgw imgd ln
        0 imgd ln
        0 setgray fill

        imgw 2 div imgd 2 div translate

        1 1 0 setrgbcolor
    ",
    $IMAGE->{ 'WIDTH' }, $IMAGE->{ 'DEPTH' },
    ;

    $IMAGE->{ 'FH' } = $fh;
}
    
############################################################
# Render the image

sub renderImage {

    close $IMAGE->{ 'FH' };
    
    system( sprintf "convert '%s' '%s'", $IMAGE->{ 'PS_TEMP' }, $IMAGE->{ 'DEST' } );
    unlink $IMAGE->{ 'PS_TEMP' } unless $options{ 'k' };
}

############################################################
# Help doco

sub help {

    print "\n@_\n" if @_;

    printf "\n$0 " .
        " [-h] [-k <path>] [-o <path>] [-D <depth>] [-S <scale>] [-W <width>]

        where
            -h          this help doco
            -k <path>   temp ps file (to retain)
            -o <path>   destination image (default: mobius.png)
            -m <iters>  number of iterations of the function to apply (default 0)
            -n <iters>  number of iterations of the inverse function to apply (default 0)

        drawing switches
            -S <scale>  drawing scale (default 1.0)
            -D <depth>  image depth (default 1000)
            -Y <width>  image width (default 1000)
    \n";

    exit;
}
