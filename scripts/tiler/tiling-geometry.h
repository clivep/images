/*******************************************************************************
 *
 * tiling-geometry.h
 *
 *  Definitions for tiling components
 *
 ******************************************************************************/

#ifndef _TILING_GEOMETRY_H

#include <stdio.h>
#include <stdarg.h>
#include <math.h>

#include "tiling-utils.h"

#define PI              3.141592653589793
#define PHI             ( ( 1.0 + sqrt( 5.0 ) ) / 2.0 )

#define RAD2DEG(r)      (int)( ( 180 * (r) / PI ) + 0.5 )

#define DIST(x,y)       ( sqrt( (x)*(x) + (y)*(y) ) )

/*************************************************
 * Polygon side status values
 ************************************************/
typedef enum {

    SIDE_STATUS_READY,
    SIDE_STATUS_DONE

} _side_status;

/*************************************************
 * Description of a polygon side
 ************************************************/
typedef struct {

    _side_status    status;
    int             type;
    void            *owner;

} _polygon_side;

/*************************************************
 * Vertex status values
 ************************************************/
typedef enum {

    VERTEX_STATUS_READY,
    VERTEX_STATUS_DONE

} _vertex_status;

/*************************************************
 * Description of a vertex
 ************************************************/
typedef struct {

    _vertex_status  status;
    double          x;
    double          y;
    double          angle_used;
    _list           *polygons;

} _polygon_vertex;

/*************************************************
 * Polygon status values
 ************************************************/
typedef enum {

    POLYGON_STATUS_READY,
    POLYGON_STATUS_DONE,
    POLYGON_STATUS_OUTSIDE

} _polygon_status;

/*************************************************
 * Definition of a polygon
 ************************************************/
typedef struct {

    int                 type;
    short               edges;
    short               anchor_edge;
    _polygon_status     status;
    double              *angles;
    double              base_angle;
    _polygon_side       **sides;   
    _polygon_vertex     **vertices;   

} _polygon;

/*************************************************
 * Common vertices
 ************************************************/
typedef struct {

    _polygon    *polygon;
    short       vertex_no;
    char        side_done;
    _polygon    *new_polygon;
    short       new_vertex_no;
    char        new_side_done;

} _common_vertex;

#define _TILING_GEOMETRY_H
#endif
