/*******************************************************************************
 *
 * fractal-draw.h
 *
 *  Definitions for rendering mandelbrot images
 *
 ******************************************************************************/

#ifndef _FRACTAL_DRAW_H

#include <stdio.h>

#include "fractal-utils.h"

/*************************************************
 * Colors
 ************************************************/
#define SET_BLACK(c)    c.red = 0;   c.green = 0;   c.blue = 0;   
#define SET_WHITE(c)    c.red = 255; c.green = 255; c.blue = 255;   
#define SET_RED(c)      c.red = 255; c.green = 0;   c.blue = 0;   
#define SET_GRAY_80(c)  c.red = 204; c.green = 204; c.blue = 204;   
#define SET_GRAY_70(c)  c.red = 178; c.green = 178; c.blue = 178;   
#define SET_GRAY_50(c)  c.red = 128; c.green = 128; c.blue = 128;   

/*************************************************
 * Prototypes
 ************************************************/
void usage( char *self, char *message );
void setupContext();
void processParameters( int argc, char **argv ); 
_image *loadImageData( char *data_path );
char *loadImageString( FILE *fp );
long double loadImageDouble( FILE *fp );
int loadImageInteger( FILE *fp );
void showImageInfo( _image *image );

void renderImageData( _image *image );
FILE *renderSetup( _image *image );
void renderSet( _image *image );
void imgPoint2Pixel( _image *image, long double img_x, long double img_y, long double *pic_x, long double *pic_y );
void drawSetGridLines( FILE *fp, _image *image, long double img_x, long double img_y, _orientation orientation ); 
void drawSetGrid( _image *image, FILE *fp );
void renderHistogram( _image *image );
void renderFinish( FILE *fp );
void showRunline( _image *image );

void checkColorScheme();
void copyColor ( _color *dest, _color *src );
void getPointColorStr( _image *image, int point, char *color_str );

_color *getDefaultColors( _image *image, int point );
_color *getTestColors( _image *image, int point );

#define _FRACTAL_DRAW_H
#endif
