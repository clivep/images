#!/usr/bin/perl

use warnings;
use strict;

############################################################
#
#   clp.pl
#
#   Just playin
#
############################################################

use Getopt::Std;
use Storable 'dclone';
use Math::Trig;

############################################################
# Prepare the environment

my $IMAGE = {};

my %options;
getopts( 'hi:k:o:r:x:y:D:S:W:X:Y:', \%options );

help() if $options{ 'h' };

$IMAGE->{ 'PS_TEMP' } = $options{ 'k' } || 'clp.ps';
$IMAGE->{ 'DEST' } = $options{ 'o' } || 'clp.pdf';

$IMAGE->{ 'SCALE' } = $options{ 'S' } || 1.0;
$IMAGE->{ 'DEPTH' } = $options{ 'D' } || 1000;
$IMAGE->{ 'WIDTH' } = $options{ 'W' } || 1000;

$IMAGE->{ 'X-OFF' } = $options{ 'X' } || 0;
$IMAGE->{ 'Y-OFF' } = $options{ 'Y' } || 0;

$IMAGE->{ 'ITERS' } = $options{ 'i' } || 1;

$IMAGE->{ 'X' } = $options{ 'x' } || 0.5;
$IMAGE->{ 'Y' } = $options{ 'y' } || 0.5;
$IMAGE->{ 'RAD' } = $options{ 'r' } || 0.5;

###########################################################
# Process the image

setupImage();

my @disc;
for( my $a = 0; $a < 360; $a += 2 ) {

    push @disc, Cnew(
        $IMAGE->{ 'X' } + $IMAGE->{ 'RAD' } * sin( deg2rad( $a ) ),
        $IMAGE->{ 'Y' } + $IMAGE->{ 'RAD' } * cos( deg2rad( $a ) ),
    );
}

#drawDisc( \@disc );
iterateDisc( \@disc, 0, '' );

exit;

###########################################################
# Function A

sub A {

    my $z = shift;

    return Cinv( 
        Csub( Cnew( 1, 0 ), Cmul( Cnew( 0, 2 ), $z ) )
    );
}

############################################################
# Function B

sub B {

    my $z = shift;

    return Cdiv( 
        Cadd( Cmul( Cnew( 1, -1 ), $z ), Cnew( 1, 0 ) ),
        Cadd( Cnew( 1, 1 ), $z ),
    );
}

############################################################
# Apply function A to the given array of points

sub applyA
{
    my $disc = shift;

    for ( my $p = 0; $p < scalar @{ $disc }; $p++ ) {

        $disc->[ $p ] = A( $disc->[ $p ] );
    }
}

############################################################
# Apply function B to the given array of points

sub applyB
{
    my $disc = shift;

    for ( my $p = 0; $p < scalar @{ $disc }; $p++ ) {

        $disc->[ $p ] = B( $disc->[ $p ] );
    }
}

############################################################
# Iterate a disc image

sub iterateDisc
{
    my $disc = shift;
    my $depth = shift;
    my $recipe = shift;

    return unless $depth < $IMAGE->{ 'ITERS' };

    my $discA = dclone( $disc );
    applyA( $discA );
    drawDisc( $discA );
    iterateDisc( $discA, $depth + 1, $recipe . 'A' );

    my $discB = dclone( $disc );
    applyB( $discB );
    drawDisc( $discB );
    iterateDisc( $discB, $depth + 1, $recipe . 'B' );
}

############################################################
# Draw the given disc

sub drawDisc {

    my $disc = shift;

    my $fh = $IMAGE->{ 'FH' };

    my $op = 'mv';
    map {
        printf $fh "%f %f %s ",
                    $IMAGE->{ 'X-OFF' } + $_->{ 'X' } * $IMAGE->{ 'SCALE' },
                    $IMAGE->{ 'Y-OFF' } + $_->{ 'Y' } * $IMAGE->{ 'SCALE' }, 
                    $op;
        $op = 'ln';
    } ( @{ $disc } );

    printf $fh "closepath stroke\n";
}

############################################################
# Invert a complex value

sub Cinv {

    return Cdiv( Cnew( 1, 0 ), shift );
}

############################################################
# Add 2 complex values

sub Cadd {

    my $z1 = shift;
    my $z2 = shift;

    return {
        'X' =>  $z1->{ 'X' } + $z2->{ 'X' },
        'Y' =>  $z1->{ 'Y' } + $z2->{ 'Y' },
    }
}

############################################################
# Subtract 2 complex values

sub Csub {

    my $z1 = shift;
    my $z2 = shift;

    return {
        'X' =>  $z1->{ 'X' } - $z2->{ 'X' },
        'Y' =>  $z1->{ 'Y' } - $z2->{ 'Y' },
    }
}

############################################################
# Create a new complex value from a coordinate pair

sub Cnew {

    return {
        'X' =>  shift,
        'Y' =>  shift,
    }
}

############################################################
# Duplicate a complex value

sub Cdup {

    my $z = shift;

    return {
        'X' =>  $z->{ 'X' },
        'Y' =>  $z->{ 'Y' },
    }
}

############################################################
# Multiply 2 complex values

sub Cmul {

    my $z1 = shift;
    my $z2 = shift;

    return {
        'X' =>  $z1->{ 'X' } * $z2->{ 'X' } - $z1->{ 'Y' } * $z2->{ 'Y' },
        'Y' =>  $z1->{ 'X' } * $z2->{ 'Y' } + $z1->{ 'Y' } * $z2->{ 'X' },
    }
}

############################################################
# Divide 2 complex values

sub Cdiv {

    my $z1 = shift;
    my $z2 = shift;

    my $v = $z2->{ 'X' } * $z2->{ 'X' } + $z2->{ 'Y' } * $z2->{ 'Y' };

    return {
        'X' =>  ( $z1->{ 'X' } * $z2->{ 'X' } + $z1->{ 'Y' } * $z2->{ 'Y' } ) / $v,
        'Y' =>  ( $z1->{ 'Y' } * $z2->{ 'X' } - $z1->{ 'X' } * $z2->{ 'Y' } ) / $v,
    }
}

############################################################
# Setup the image

sub setupImage {

    open my $fh, '>', $IMAGE->{ 'PS_TEMP' } or die "Could not open $IMAGE->{ 'PS_TEMP' }: $!\n";
    
    printf $fh "%%!PS\n%%%%BoundingBox: 0 0 1000 1000\n%%%%EndComments\n
    
        /imgw %f def
        /imgd %f def

        <<
            /PageSize [ imgw imgd ]
            /ImagingBBox null
        >> setpagedevice
    
        /mv { moveto } bind def
        /ln { lineto } bind def
    
        /Arial-Bold findfont 16 scalefont setfont

        0 0 mv
        imgw 0 ln
        imgw imgd ln
        0 imgd ln
        0 setgray fill

        imgw 2 div imgd 2 div translate

        1 1 0 setrgbcolor
    ",
    $IMAGE->{ 'WIDTH' }, $IMAGE->{ 'DEPTH' },
    ;

    $IMAGE->{ 'FH' } = $fh;
}

############################################################
# Render the image

sub renderImage {

    close $IMAGE->{ 'FH' };

    system( sprintf "convert '%s' '%s'", $IMAGE->{ 'PS_TEMP' }, $IMAGE->{ 'DEST' } );
    unlink $IMAGE->{ 'PS_TEMP' } unless $options{ 'k' };
}

############################################################
# Help doco

sub help {

    print "\n@_\n" if @_;

    printf "\n$0 " .
        "[-h] [-i <iters>] [-k <path>] [-o <path>] [-D <depth>] [-S <scale>] [-W <width>]
        [-X <xoff>] [-Y <yoff>] 

        where
            -h          this help doco
            -i <iters>  interations of the image (default 1)
            -k <path>   temp ps file (to retain)
            -o <path>   destination image (default: mobius.png)
            -x <xpos>   initial disc x pos (default 0.5)
            -y <ypos>   initial disc y pos  (default 0.5)
            -x <xpos>   initial disc rad (default 0.5)

        drawing switches
            -S <scale>  drawing scale (default 1.0)
            -D <depth>  image depth (default 1000)
            -W <width>  image width (default 1000)
            -X <xoff>   drawing horizontal offset (default 0)
            -Y <yoff>   drawing vertical offset (default 0)
    \n";

    exit;
}
