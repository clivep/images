#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <math.h>

#include "interp-utils.h"

/*******************************************************************************
 *
 * interp.c
 *
 *  A light-weight PostScript like interpreter
 *
 ******************************************************************************/

#define PI  3.141592653589793

/*************************************************
 * Parser status values
 ************************************************/
typedef enum {

    PARSER_STATUS_READY,
    PARSER_STATUS_IN_TOKEN,
    PARSER_STATUS_IN_STRING
} _parser_status;

/*************************************************
 * Input token types
 ************************************************/
typedef enum {

    TOKEN_TYPE_LITERAL,
    TOKEN_TYPE_NAME,
    TOKEN_TYPE_STRING,
    TOKEN_TYPE_INTEGER,
    TOKEN_TYPE_FLOAT,
    TOKEN_TYPE_BOOLEAN,
    TOKEN_TYPE_START_MARK,
    TOKEN_TYPE_END_MARK,
    TOKEN_TYPE_CALLBACK
} _token_type;

#define NUMERIC_TOKEN(t)    (((t)->type==TOKEN_TYPE_INTEGER)||((t)->type==TOKEN_TYPE_FLOAT))

/*************************************************
 * Description of an input token
 ************************************************/
typedef struct {

    char        *token;
    _token_type type;
    double      value;
    int         parser_line;
    int         parser_pos;

} _token_info;

/*************************************************
 * Description of a dict stack item
 ************************************************/
typedef struct {

    char        *name;
    _token_info **tokens;

} _dict_item;

/*************************************************
 * Description of a callback stack item
 ************************************************/
typedef struct {

    char    *name;
    void    (*callback)(_token_info *);

} _callback_item;

#define TOKEN_BUF_BLOCK_SZ  10

/*************************************************
 * Operating context
 ************************************************/
typedef struct {

    char            *input_file;
    FILE            *input_fp;

    char            *token_buf;
    int             token_buf_sz;

    int             parser_line;
    int             parser_pos;
    _parser_status  parser_status;

    _list           *op_stack;
    _list           *dict_stack;
    _list           *callback_stack;

} _context;

_context    *context;

/*************************************************
 * Usage message
 ************************************************/

void usage(
    char    *name
) {
    
    printf( "Usage:\n" );
    printf( "\t%s  [-h] <file>\n", name );
    printf( "where\n" );
    printf( "\t-h  this help message\n" );

    exit( 0 );
}

/*************************************************
 * Return a description of the token type
 ************************************************/

char *tokenTypeDescription(
    _token_type type
) {

    switch( type ) {

        case TOKEN_TYPE_BOOLEAN:    return "BOOLEAN";
        case TOKEN_TYPE_CALLBACK:   return "CALLBACK";
        case TOKEN_TYPE_LITERAL:    return "LITERAL";
        case TOKEN_TYPE_NAME:       return "NAME";
        case TOKEN_TYPE_INTEGER:    return "INTEGER";
        case TOKEN_TYPE_FLOAT:      return "FLOAT";
        case TOKEN_TYPE_STRING:     return "STRING";
        case TOKEN_TYPE_START_MARK: return "MARK-START";
        case TOKEN_TYPE_END_MARK:   return "MARK-END";
    }

    return "?";
}

/*************************************************
 * List the contents of the dict stack
 ************************************************/

void dumpDictStack() {

    _list_item  *item;
    _dict_item  *dict_item;
    _token_info *token;
    int         p;
    char        *sep;

    printf( "Dictionary Stack:\n" );

    for( item = context->dict_stack->first; item != NULL; item = item->next ) {

        sep = "";

        dict_item = item->data;
        printf( "%s : { ", dict_item->name ); 

        for ( p = 0; dict_item->tokens[ p ] != NULL; p++ ) {

            switch ( dict_item->tokens[ p ]->type ) {

                case TOKEN_TYPE_CALLBACK:
                    printf( "%s%s(cbk)", sep, dict_item->tokens[ p ]->token );
                    break;
                case TOKEN_TYPE_LITERAL:
                    printf( "%s/%s", sep, dict_item->tokens[ p ]->token );
                    break;
                case TOKEN_TYPE_STRING:
                    printf( "%s(%s)", sep, dict_item->tokens[ p ]->token );
                    break;
                default:
                    printf( "%s%s", sep, dict_item->tokens[ p ]->token );
                    break;
            }

            sep = ", ";
        }

        printf( " }\n" );
    }
}

/*************************************************
 * List the contents of the op stack
 ************************************************/

void dumpOpStack() {

    _list_item  *item;
    _token_info *token;

    printf( "Operations Stack:\n" );

    for( item = context->op_stack->first; item != NULL; item = item->next ) {

        token = item->data;
        printf( "%s: %s\n", tokenTypeDescription( token->type ), token->token ); 
    }
}

/*************************************************
 * Display a processing error
 ************************************************/

void displayError (
    char        *error,
    _token_info *token
) {

    logMsg( "ERROR: %s '%s' (ln %d @ %d)", error, token->token,
                                    token->parser_line, token->parser_pos );

    dumpDictStack();
    dumpOpStack();

    exit ( -1 );
}

/*************************************************
 * Add the given callback onto the dict stack
 ************************************************/

void pushDictStackCallback(
    char    *name,
    void    (*callback)(_token_info *)
) {
    _token_info     *token;
    _dict_item      *dict_item;
    _callback_item  *callback_item;

    if ( ( callback_item = ALLOC( _callback_item ) ) == NULL )
        logFatal( "Failed to allocate callback item" );

    callback_item->name = strdup( name );
    callback_item->callback = callback;

    addToList( callback_item, context->callback_stack );

    if ( ( dict_item = ALLOC( _dict_item ) ) == NULL )
        logFatal( "Failed to allocate dict item" );

    if ( ( dict_item->tokens = NALLOC( 2, _token_info * ) ) == NULL )
        logFatal( "Failed to allocate dict item token block" );

    if ( ( dict_item->tokens[ 0 ] = ALLOC( _token_info ) ) == NULL )
        logFatal( "Failed to allocate dict item token" );

    dict_item->tokens[ 0 ]->token = name;
    dict_item->tokens[ 0 ]->type = TOKEN_TYPE_CALLBACK;

    dict_item->tokens[ 1 ] = NULL;

    dict_item->name = strdup( name );

    addToList( dict_item, context->dict_stack );
}

/*************************************************
 * Duplicate a token
 ************************************************/

_token_info *copyToken(
    _token_info *src_token
) {
    _token_info *new_token;

    if ( ( new_token = ALLOC( _token_info ) ) == NULL )
        logFatal( "Could not alloc duplicate token" );

    memcpy( new_token, src_token, sizeof( _token_info ) );

    new_token->token = strdup( src_token->token );

    return new_token;
}

/*************************************************
 * Internal callbacks:
 *
 *  def         - define dict objects
 *  add         - add 2 topmost op stack items
 *  sub         - subtract 2 topmost op stack items
 *  mul         - multiply 2 topmost op stack items
 *  div         - divide 2 topmost op stack items
 *  dup         - duplicate the topmost stack item
 *  pop         - remove topmost op stack item
 *  exch        - swap 2 topmost op stack items
 *  ==          - display and remove topmost op stack item
 *  dictdump    - display the dict stack
 *  opdump      - display the op stack
 ************************************************/

/**************************************
 * dictdump - display the dict stack
 *************************************/

void callbackDictDump(
    _token_info *token
) {
    dumpDictStack();
}

/**************************************
 * opdump - display the op stack
 *************************************/

void callbackOpDump(
    _token_info *token
) {
    dumpOpStack();
}

/**************************************
 * reset a number token value
 *************************************/

void resetNumericValue(
    _token_info *token,
    _token_type type,
    double      value
) {
    char        buf[ 20 ];

    token->type = type;

    free( token->token );
    if ( type == TOKEN_TYPE_FLOAT )
        sprintf( buf, "%lf", value );
    else
        sprintf( buf, "%d", (int)value );

    token->token = strdup( buf );
    token->value = value;
}

/**************************************
 * reset a boolean token value
 *************************************/

void resetBooleanValue(
    _token_info *token,
    double      value
) {
    token->type = TOKEN_TYPE_BOOLEAN;

    free( token->token );
    token->token = strdup( value ? "true" : "false" );
    token->value = value;
}

/**************************************
 * perform a binary numeric op
 *************************************/

void binaryNumericOp(
    _token_info *op_token
) {
    _token_type type;
    _list_item  *item;
    _token_info *token;
    double      value;

    if ( context->op_stack->count < 2 )
        displayError( "Stackunderflow", op_token );

    type = TOKEN_TYPE_INTEGER;

    item = context->op_stack->last;
    token = item->data;
    if ( ! NUMERIC_TOKEN( token ) )
        displayError( "Typecheck", op_token );
    if ( token->type == TOKEN_TYPE_FLOAT )
        type = TOKEN_TYPE_FLOAT;
    value = token->value;

    item = item->prev;
    token = item->data;
    if ( ! NUMERIC_TOKEN( token ) )
        displayError( "Typecheck", op_token );
    if ( token->type == TOKEN_TYPE_FLOAT )
        type = TOKEN_TYPE_FLOAT;

    // remove the last operand
    releaseListItem( context->op_stack->last, context->op_stack );
    
    if ( strcmp( token->token, "add" ) == 0 )
        token->value += value;
    else if ( strcmp( token->token, "sub" ) == 0 )
        token->value -= value;
    else if ( strcmp( token->token, "mul" ) == 0 )
        token->value *= value;
    else if ( strcmp( token->token, "div" ) == 0 ) {
        token->value /= value;
        type = TOKEN_TYPE_FLOAT;
    }

    resetNumericValue( token, type, token->value );
}

/**************************************
 * def - define dict objects
 *************************************/

void callbackDef(
    _token_info *def_token
) {
     _list_item *item;
     _list_item *name_item;
     _dict_item *dict_item;
    _token_info *token;
     int        item_count;

     if ( context->op_stack->count < 2 )
        displayError( "Stackunderflow", def_token );

    item = context->op_stack->last;
    token = item->data;

    // Is this a single object def or a block
    if ( token->type == TOKEN_TYPE_END_MARK ) {

        // move down the stack to find the starting mark
        item_count = 0;
        while ( token->type != TOKEN_TYPE_START_MARK ) {

            if ( ( item = item->prev ) == NULL ) 
                displayError( "Missing start marker", def_token );

            token = item->data;
            item_count++;
        }
    
        // Store this now since the closing marker release
        // will change the *item values
        name_item = item->prev;

        releaseListItem( item, context->op_stack );
    }
    else {

        item_count = 1;
        name_item = item->prev;
    }

    token = name_item->data;
    if ( token->type != TOKEN_TYPE_LITERAL ) 
        displayError( "Missing name for def()", def_token );

    if ( ( dict_item = ALLOC( _dict_item ) ) == NULL )
        logFatal( "Failed to allocate dict item" );

    if ( ( dict_item->tokens = NALLOC( item_count + 1, _token_info * ) ) == NULL )
        logFatal( "Failed to allocate dict item token block" );

    token = name_item->data;
    dict_item->name = strdup( &token->token[ 1 ] );  // skip the leading '/'

    while ( item_count > 0 ) {

        item = context->op_stack->last;

        dict_item->tokens[ item_count - 1 ] = copyToken( item->data );
        item_count--;

        releaseListItem( item, context->op_stack );
    }

    // Release the name token
    releaseListItem( context->op_stack->last, context->op_stack );

    addToList( dict_item, context->dict_stack );
}

/**************************************
 * dup - duplicate the topmost item
 *************************************/

void callbackDup(
    _token_info *dup_token
) {
    _token_info *new_token;

    if ( context->op_stack->count < 1 )
        displayError( "Stackunderflow", dup_token );

    new_token = context->op_stack->last->data;

    // No need for pushOpStackToken() since this must be
    // a plain type token (not a dict stack entry)
    addToList( new_token, context->op_stack );
}

/**************************************
 * eq - equality test
 *************************************/

void callbackEquality(
    _token_info *token
) {
    _token_info *token_1;
    _token_info *token_2;
    double      value;

    if ( context->op_stack->count < 2 )
        displayError( "Stackunderflow", token );

    token_1 = context->op_stack->last->data;
    token_2 = context->op_stack->last->prev->data;

    // For numeric tests - only consider the values
    if ( NUMERIC_TOKEN( token_1 ) ) {

        value = ( token_1->value == token_2->value ) ? 1 : 0;   
    }
    else {
    
        if ( token_1->type != token_2->type )
            displayError( "Type mis-match", token );

        value = ( strcmp( token_1->token, token_2->token ) == 0 ) ? 1 : 0;   
    }

    if ( strcmp( token->token, "ne" ) == 0 )
        value = 1 - value;

    resetBooleanValue( token_2, value );

    releaseListItem( context->op_stack->last, context->op_stack );
}

/**************************************
 * exch - swap the 2 topmost entries
 *************************************/

void callbackExch(
    _token_info *exch_token
) {
    _list_item  *item_1;
    _list_item  *item_2;

    if ( context->op_stack->count < 2 )
        displayError( "Stackunderflow", exch_token );

    item_1 = context->op_stack->last;
    item_2 = item_1->prev;

    item_1->prev = item_2->prev;
    item_1->next = item_2;

    if ( item_2->prev == NULL )
        context->op_stack->first = item_1;
    else
        item_2->prev->next = item_1;

    item_2->prev = item_1;
    item_2->next = NULL;

    context->op_stack->last = item_2;
}

/**************************************
 * gt/lt - numeric comparison
 *************************************/

void callbackNumericComparison(
    _token_info *token
) {
    _token_info *token_1;
    _token_info *token_2;
    double      value;

    if ( context->op_stack->count < 2 )
        displayError( "Stackunderflow", token );

    token_1 = context->op_stack->last->data;

    if ( ! NUMERIC_TOKEN( token_1 ) )
        displayError( "Type mis-match", token_1 );

    token_2 = context->op_stack->last->prev->data;

    if ( ! NUMERIC_TOKEN( token_2 ) )
        displayError( "Type mis-match", token_2 );

    if ( strcmp( token->token, "lt" ) == 0 )
        value = ( token_2->value < token_1->value ) ? 1 : 0;
    else
        value = ( token_2->value > token_1->value ) ? 1 : 0;

    resetBooleanValue( token_2, value );

    releaseListItem( context->op_stack->last, context->op_stack );
}

/**************************************
 * sin - sin() of the topmost item
 *************************************/

void callbackSin(
    _token_info *sin_token
) {
    _token_info *token;
    
    if ( context->op_stack->count < 1 )
        displayError( "Stackunderflow", sin_token );

    token = context->op_stack->last->data;

    if ( ! NUMERIC_TOKEN( token ) )
        displayError( "Type mis-match", sin_token );

    resetNumericValue( token, TOKEN_TYPE_FLOAT,
                            sin( PI * token->value / 180.0 ) );
}

/**************************************
 * cos - cos() of the topmost item
 *************************************/

void callbackCos(
    _token_info *cos_token
) {
    _token_info *token;
    
    if ( context->op_stack->count < 1 )
        displayError( "Stackunderflow", cos_token );

    token = context->op_stack->last->data;

    if ( ! NUMERIC_TOKEN( token ) )
        displayError( "Type mis-match", cos_token );

    resetNumericValue( token, TOKEN_TYPE_FLOAT,
                            cos( PI * token->value / 180.0 ) );
}

/**************************************
 * sqrt - square root the topmost item
 *************************************/

void callbackSqrt(
    _token_info *sqrt_token
) {
    _token_info *token;
    
    if ( context->op_stack->count < 1 )
        displayError( "Stackunderflow", sqrt_token );

    token = context->op_stack->last->data;

    if ( ! NUMERIC_TOKEN( token ) )
        displayError( "Type mis-match", sqrt_token );

    resetNumericValue( token, TOKEN_TYPE_FLOAT, sqrt( token->value ) );
}

/**************************************
 * pop - remove topmost op stack item
 *************************************/

void callbackPop(
    _token_info *pop_token
) {
    if ( context->op_stack->count < 1 )
        displayError( "Stackunderflow", pop_token );

    releaseListItem( context->op_stack->last, context->op_stack );
}

/**************************************
 * disp - display (and remove) top item
 *************************************/

void callbackDisp(
    _token_info *disp_token
) {
    _list_item  *item;
    _token_info *token;

    if ( context->op_stack->count < 1 )
        displayError( "Stackunderflow", disp_token );

    item = context->op_stack->last;
    token = item->data;

    switch( token->type ) {
    
        case TOKEN_TYPE_INTEGER:
            printf( "%d\n", (int)token->value );
            break;
        case TOKEN_TYPE_FLOAT:
            printf( "%lf\n", token->value );
            break;
        case TOKEN_TYPE_STRING:
            printf( "(%s)\n", token->token );
            break;
        case TOKEN_TYPE_CALLBACK:
            printf( "callback (%s)\n", token->token );
            break;
    }

    releaseListItem( context->op_stack->last, context->op_stack );
}

/*************************************************
 * Load the dict stack with the interal callbacks
 ************************************************/

void setupInternalCallbacks() {

    pushDictStackCallback( "def", callbackDef );
    pushDictStackCallback( "add", binaryNumericOp );
    pushDictStackCallback( "sub", binaryNumericOp );
    pushDictStackCallback( "mul", binaryNumericOp );
    pushDictStackCallback( "div", binaryNumericOp );
    pushDictStackCallback( "dup", callbackDup );
    pushDictStackCallback( "pop", callbackPop );
    pushDictStackCallback( "sin", callbackSin );
    pushDictStackCallback( "cos", callbackCos );
    pushDictStackCallback( "sqrt", callbackSqrt );
    pushDictStackCallback( "exch", callbackExch );
    pushDictStackCallback( "gt", callbackNumericComparison );
    pushDictStackCallback( "lt", callbackNumericComparison );
    pushDictStackCallback( "eq", callbackEquality );
    pushDictStackCallback( "ne", callbackEquality );
    pushDictStackCallback( "==", callbackDisp );
    pushDictStackCallback( "dictdump", callbackDictDump );
    pushDictStackCallback( "opdump", callbackOpDump );
}

/*************************************************
 * Find a dict stack entry
 ************************************************/

_dict_item *findDictStackEntry(
    char    *name
) {
    _list_item  *item;
    _dict_item  *dict_item;

    for( item = context->dict_stack->first; item != NULL; item = item->next ) {

        dict_item = item->data;

        if ( strcmp( dict_item->name, name ) == 0 )
            return item->data;
    }

    return NULL;
}

/*************************************************
 * Find a callback stack entry
 ************************************************/

_callback_item *findCallbackStackEntry(
    char    *name
) {
    _list_item      *item;
    _callback_item  *callback_item;

    for( item = context->callback_stack->first; item != NULL; item = item->next ) {

        callback_item = item->data;

        if ( strcmp( callback_item->name, name ) == 0 )
            return item->data;
    }

    return NULL;
}

/*************************************************
 * Process a callback on the operations stack
 ************************************************/

void processCallback(
    _token_info     *token
) {
    _callback_item  *item;

    if ( token->type != TOKEN_TYPE_CALLBACK )
        return;

    if ( ( item = findCallbackStackEntry( token->token ) ) == NULL )
        displayError( "No such callback", token );

    item->callback( token );
}

/*************************************************
 * Push an item onto the op stack
 ************************************************/

void pushOpStackToken(
    _token_info    *token
) {
    _dict_item  *dict_item;
    int         p;

    if ( token->type == TOKEN_TYPE_NAME ) {

        if ( ( dict_item = findDictStackEntry( token->token ) ) == NULL )
            displayError( "No such name", token );

        for ( p = 0; dict_item->tokens[ p ] != NULL; p++ ) {

            dict_item->tokens[ p ]->parser_line = token->parser_line;
            dict_item->tokens[ p ]->parser_pos = token->parser_pos;
            pushOpStackToken( dict_item->tokens[ p ] );
        }
    }
    else if ( token->type == TOKEN_TYPE_CALLBACK ) {

        processCallback( token );
    }
    else if (
        ( token->type == TOKEN_TYPE_START_MARK ) ||
        ( token->type == TOKEN_TYPE_END_MARK )
    ) {

        // For the moment we do nothing - prolly should need to be
        // storing the tokens between these markers onto the dict
        // stack for ron...
    }
    else {

        addToList( token, context->op_stack );
    }
}

/*************************************************
 * Release a token
 ************************************************/

void freeToken(
    void *token_pnt
) {
    _token_info *token = token_pnt;

    free( token->token );
    free( token );
}

/*************************************************
 * Release a callback stack item
 ************************************************/

void freeCallbackItem(
    void *item_pnt
) {
    _callback_item  *item = item_pnt;

    free( item->name );
    free( item );
}

/*************************************************
 * Release a dict stack item
 ************************************************/

void freeDictItem(
    void *item_pnt
) {
    _dict_item  *item = item_pnt;
    _token_info *token;
    int         p;

    free( item->name );

    for ( p = 0; item->tokens[ p ] != NULL; p++ ) {

        if ( item->tokens[ p ]->type != TOKEN_TYPE_CALLBACK )
            freeToken( item->tokens[ p ] );
    }

    free( item );
}

/*************************************************
 * Initialise the processing context
 ************************************************/

void initialiseContext() {

    initialiseLogging( NULL, LOGGING_LEVEL_DEBUG );

    if ( ( context = ALLOC( _context ) ) == NULL )
        logFatal( "Could not allocate the processing context" );

    context->token_buf_sz = TOKEN_BUF_BLOCK_SZ;
    if ( ( context->token_buf = NALLOC( context->token_buf_sz, char ) ) == NULL )
        logFatal( "Could not allocate the input buffer" );

    context->parser_status = PARSER_STATUS_READY;

    context->callback_stack = initialiseList( freeCallbackItem );
    context->dict_stack = initialiseList( freeDictItem );
    context->op_stack = initialiseList( freeToken );
}

/*************************************************
 * Process the runline parameters
 ************************************************/

void processParameters(
    int     argc,
    char    **argv
) {
    int pnt = 1;

    while ( pnt < ( argc - 1 ) ) {
    
        if ( *argv[ pnt ] != '-' )
            logFatal( "Unknown switch %s", argv[ pnt ] );

        switch( argv[ pnt ][ 1 ] ) {

            case 'h' :
                usage( argv[ 0 ] );
                break;
        }
    }

    if ( ( pnt == argc ) || ( *argv[ pnt ] == '-' ) ) {
        logMsg( "Missing input file name" );
        usage( argv[ 0 ] );
    }

    context->input_file = argv[ pnt ];
    context->input_fp = fopen( argv[ pnt ], "r" );

    if ( context->input_fp == NULL )
        logFatal( "Unable to open input file %s", argv[ pnt ] );
}

/*************************************************
 * Build a new token object from the context
 ************************************************/

_token_info *newTokenInfo() {

    _token_info *token_info;
    _token_type type;
    char        *p;

    if ( ( token_info = ALLOC( _token_info ) ) == NULL )
        logFatal( "Could not allocate token_info" );

    token_info->token = strdup( context->token_buf );
    token_info->parser_line = context->parser_line;
    token_info->parser_pos = context->parser_pos - strlen( token_info->token ) - 1;

    token_info->type = TOKEN_TYPE_NAME;

    if ( strcmp( token_info->token, "true" ) == 0 ) {

        token_info->type = TOKEN_TYPE_BOOLEAN;
        token_info->value = 1;
    }
    else if ( strcmp( token_info->token, "false" ) == 0 ) {

        token_info->type = TOKEN_TYPE_BOOLEAN;
        token_info->value = 0;
    }
    else if ( strcmp( token_info->token, "{" ) == 0 ) {

        token_info->type = TOKEN_TYPE_START_MARK;
    }
    else if ( strcmp( token_info->token, "}" ) == 0 ) {

        token_info->type = TOKEN_TYPE_END_MARK;
    }
    else if ( *token_info->token == '/' ) {

        token_info->type = TOKEN_TYPE_LITERAL;
    }
    else if ( context->parser_status == PARSER_STATUS_IN_STRING ) {

        token_info->type = TOKEN_TYPE_STRING;
    }
    else {

        type = TOKEN_TYPE_INTEGER;

        for( p = token_info->token; *p != '\0'; p++ ) {
    
            if ( ( ! isdigit( *p ) ) && ( *p != '.' ) )
                return token_info;

            if ( *p == '.' )
                type = TOKEN_TYPE_FLOAT;
        }
    
        token_info->type = type;
        token_info->value = atof( token_info->token );
    }

    return token_info;
}

/*************************************************
 * Get the next token from the input file
 ************************************************/

_token_info *getNextInputToken() {

    char        *pos;
    char        ch;

    pos = context->token_buf;
    *pos = '\0';

    context->parser_status = PARSER_STATUS_READY;

    while ( ( ch = fgetc( context->input_fp ) ) != EOF ) {

        context->parser_pos++;

        if ( ( ch == '%' ) && ( context->parser_status != PARSER_STATUS_IN_STRING ) ){

            do {

                ch = fgetc( context->input_fp );
            } while ( ( ch != EOF ) && ( ch != '\n' ) );
        }

        if ( ( context->parser_status == PARSER_STATUS_IN_STRING ) && ( ch == ')' ) ) {
            *pos = '\0';
            return newTokenInfo();
        }

        if ( isspace( ch ) ) {

            if ( context->parser_status == PARSER_STATUS_IN_TOKEN ) {

                *pos = '\0';
                return newTokenInfo();
            }
    
            if ( ch == '\n' ) {
            
                if ( context->parser_status == PARSER_STATUS_IN_STRING )
                    logFatal( "Newline in string at line %d, pos %d",
                                    context->parser_line, context->parser_pos );

                context->parser_line++;
                context->parser_pos = 0;
            }

            continue;
        }

        if ( context->parser_status == PARSER_STATUS_READY ) {

            if ( ch == '(' ) {
                context->parser_status = PARSER_STATUS_IN_STRING;
                continue;
            }

            context->parser_status = PARSER_STATUS_IN_TOKEN;
        }

        if ( pos == ( context->token_buf + context->token_buf_sz ) ) {

            context->token_buf_sz += TOKEN_BUF_BLOCK_SZ;
            context->token_buf = realloc( context->token_buf, context->token_buf_sz );
            if ( context->token_buf == NULL )
                logFatal( "Failed to realloc token_buf" );
        }

        *pos++ = ch;
    }

    return NULL;
}

/*************************************************
 * Check if the first 4 chars are '%!PS'
 ************************************************/

short checkFileIdent() {

    char ident[ 5 ];

    if ( fgets( ident, 5, context->input_fp ) == NULL )
        return NOK;

    ident [ 4 ] = '\0';

    if ( strncmp( ident, "%!PS", 4 ) == 0 )
        return OK;
        
    return NOK;
}

/*************************************************
 * Process the input file
 ************************************************/

void processInputFile() {

    char        buf[10];
    _token_info *token;

    if ( checkFileIdent() == NOK )
        fseek( context->input_fp, 0, SEEK_SET );

    while ( ( token = getNextInputToken() ) != NULL ) {

        printf( "%s: %s\n", tokenTypeDescription( token->type ), token->token ); 
        pushOpStackToken( token );
    }
}

/*************************************************
 * Main
 ************************************************/

int main(
    int     argc,
    char    **argv
) {
    initialiseContext();
    processParameters( argc, argv );

    setupInternalCallbacks();

    processInputFile();
}
