#!/usr/bin/perl

use warnings;
use strict;

my $XFORMS = [

    [  0.00,  0.00,  0.00,  0.16, 0.00, 0.00, 0.01 ],
    [  0.20, -0.26,  0.23,  0.22, 0.00, 1.60, 0.07 ],
    [ -0.15,  0.28,  0.26,  0.24, 0.00, 0.44, 0.07 ],
    [  0.85,  0.04, -0.04,  0.85, 0.00, 1.60, 0.85 ],
];

my $MAX_POINTS = 100000;
my $TEMP = 'fern.tmp';
my $DEST = 'fern.png';
 
my $x = 0;
my $y = 0;

open my $FH, '>', $TEMP or die "Could not open $TEMP - $!\n";

printHeader( $FH );

for ( my $p = 0; $p < $MAX_POINTS; $p++ ) {

    my $rnd = ( rand 100 ) / 100.0;

    my $prob_total = 0.0;
    for my $xform ( @{ $XFORMS } ) {

        $prob_total += $xform->[ 6 ];
        if ( $rnd < $prob_total ) {

            my $nx = ( $xform->[ 0 ] * $x ) + ( $xform->[ 1 ] * $y ) + $xform->[ 4 ];
            my $ny = ( $xform->[ 2 ] * $x ) + ( $xform->[ 3 ] * $y ) + $xform->[ 5 ];

            $x = $nx;
            $y = $ny;

            print $FH "$rnd $x $y pt\n";
            last;
        }
    }
}

system( "/usr/bin/convert $TEMP $DEST" );
unlink $TEMP;

exit;

sub printHeader {

    my $hndl = shift;

    print $hndl "%!PS

        -1000 -1000 moveto
        2000 0 rlineto
        0 2000 rlineto
        -2000 0 rlineto
        fill

        /Arial findfont 10 scalefont setfont
        1 setgray
    ";

    my $y  = 40;
    for my $xform ( @{ $XFORMS } ) {

        my $x = 50;

        for my $val ( @{ $xform } ) {
        
            printf $hndl "
                $x $y moveto 
                (%0.2f) dup stringwidth pop neg 0 rmoveto 
                show
            ", $val;
            $x += 30;
        }
        $y += 14;
    }

    my $color_func = ( ( $ARGV[ 0 ] || '' ) eq '-m' ) ?
        "1 setgray"
        :
        "/rnd rnd 1000 mul dup cvi sub def
         rnd dup dup mul mul
         rnd 1000 mul dup cvi sub dup dup mul mul
         1 setrgbcolor
        "
    ;

    print $hndl "%!PS

        300 150 translate
        /sc 60 def

        /pt {

            /y exch sc mul def
            /x exch sc mul def
            /rnd exch def

            $color_func

            x y moveto
            -0.5 -0.5 rmoveto
            1 0 rlineto
            0 1 rlineto
            -1 0 rlineto
            fill

        } def
    ";
}
