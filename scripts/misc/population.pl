#!/usr/bin/perl

use warnings;
use strict;

# population multipliers
my $lr = 2.4;
my $ur = 4.0;

# display values
my $lx = 100;
my $ly = 200;

my $scale = 400;

my $density = 10000;            # image density

my $stabilise_count = 100;      # interations to allow population to stabilise
my $display_count = 5000;       # interations for display

printf "%%!PS\n\n";
printf "%f setlinewidth\n",  $scale / $density;
printf "/p { setrgbcolor moveto %f 0 rlineto stroke } def\n\n", $scale / $density;

for( my $row = 0; $row < $density; $row++ ) {

   my $y = $row / $density;
   my $r = $lr + ( $ur - $lr ) * $y;

   my $row_data = {
      'COUNT'        =>    0,
      'POINTS'       =>    [],   # actual x/y points
      'PIXEL_HITS'   =>    [],   # density entries
   };

   # initiaise the data and allow to stabilise
   my $val = 0.25;
   for ( my $c = 0; $c < $stabilise_count; $c++ ) {

      $val = $r * $val * ( 1 - $val );
   }

   # now collect the data points
   for ( my $c = 0; $c < $display_count; $c++ ) {

      $val = $r * $val * ( 1 - $val );
      ( $val > 0 ) || last;

      my $ix = int( $val * $density );

      ( $row_data->{ 'PIXEL_HITS' }->[ $ix ] ) || $row_data->{ 'COUNT' }++; 
      $row_data->{ 'PIXEL_HITS' }->[ $ix ]++;

      my $point = [ $lx + $val * $scale, $ly + $y * $scale ];
#      push @{ $row_data->{ 'POINTS' } } , $point;
   }

   printf "  %% COUNT %d\n", $row_data->{ 'COUNT' };

   # Print the actual points
#   map { printf "%f %f 0 0 0 p ", $_->[0], $_->[1] } @{ $row_data->{ 'POINTS' } };

   # Display the denisty grid
   for ( my $x = 0; $x < $density; $x++ ) {
      
      if ( $row_data->{ 'PIXEL_HITS' }->[ $x ] ) {
      
         printf "%f %f  p ",
                     $lx + $scale * $x / $density,
                     $ly + $scale * $y;
      }
   }
}

printf "\nshowpage\n";
