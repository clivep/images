#!/usr/bin/perl

use warnings;
use strict;

############################################################
#
#   apollonian.pl
#
#   The Apollonian gasket
#
############################################################

use Data::Dumper;
use Getopt::Std;
use Math::Trig;
use Storable qw(dclone);
use Readonly;

my Readonly     $SMALL   =  1e-10;

my Readonly     $BLACK   =  '0 setgray';
my Readonly     $RED     =  '1.0 0.0 0.0 setrgbcolor';
my Readonly     $GREEN   =  '0.0 1.0 0.0 setrgbcolor';
my Readonly     $BLUE    =  '0.0 0.0 1.0 setrgbcolor';
my Readonly     $CYAN    =  '0.0 1.0 1.0 setrgbcolor';
my Readonly     $MAGENTA =  '1.0 0.0 1.0 setrgbcolor';
my Readonly     $YELLOW  =  '1.0 1.0 0.0 setrgbcolor';
 
my Readonly     $CIRCLE_COLOR    =  '0.2 0.4 0.6 setrgbcolor';
my Readonly     $REFLECTOR_COLOR =  '1.0 0.6 0.0 setrgbcolor';

############################################################
# Setup the image

my $IMAGE = {

    'CIRCLES'       =>  [],

    'INTERSECTIONS' =>  {},
    'REFLECTORS'    =>  {},

    'SCALE'         =>  1.0,
};

############################################################
# Check the runline options

my %options;
getopts( '1:2:3:d:A:BD:FhiIk:Ll:MN:m:o:Rr:S:TvW:X:Y:', \%options );

help() if $options{ 'h' };

$IMAGE->{ 'PS-TEMP' } = $options{ 'k' } || 'apollonian.ps';
$IMAGE->{ 'DEST' } = $options{ 'o' } || 'apollonian.png';

$IMAGE->{ 'RAD-1' } = $options{ '1' } || 30;
$IMAGE->{ 'RAD-2' } = $options{ '2' } || 20;
$IMAGE->{ 'RAD-3' } = $options{ '3' } || 10;

$IMAGE->{ 'SCALE' } = $options{ 'S' } || 1.0;
$IMAGE->{ 'X-OFF' } = $options{ 'X' } || 0.0;
$IMAGE->{ 'Y-OFF' } = $options{ 'Y' } || 0.0;

$IMAGE->{ 'RADIUS' } = $options{ 'r' } || 450;
$IMAGE->{ 'MIN-RAD' } = $options{ 'm' } || 2;
$IMAGE->{ 'ANGLE' } = $options{ 'A' } || 0;

$IMAGE->{ 'MAX-DEPTH' } = $options{ 'd' } || 1;

$IMAGE->{ 'LINE-WIDTH' } = $options{ 'W' } || 1;

help( "Invalid value for -D <tag>" ) if 
    $options{ 'D' } &&
    ( $options{ 'D' } ne 'INNER' ) &&
    ( $options{ 'D' } ne 'OUTER' ) &&
    ( $options{ 'D' } ne 'BOTH' )
;

if ( $options{ 'N' } ) {
    setupMultiImage();
    processMultiImage();
}
else {
    setupImage();
    processImage();
}
iterateImage() if $options{ 'i' };

listCircles() if $options{ 'l' };

drawImage();

exit;

############################################################
# Initialise multiple (>3 initial) disc image

sub setupMultiImage {

    help( "Value for the -n switch must be a number" )
                            unless ( $options{ 'N' } =~ /^\d+$/ );

    help( "Value for the -n switch must be > 1" )
                            unless $options{ 'N' } > 1;

    my $sc = $IMAGE->{ 'SCALE' };
    my $xoff = $IMAGE->{ 'X-OFF' };
    my $yoff = $IMAGE->{ 'Y-OFF' };

    my $v = 1 + 1 / sin( pi / $options{ 'N' } );

    my $rad = $IMAGE->{ 'RADIUS' } / $v;
    my $h = $rad / sin( pi / $options{ 'N' } );

    my $outer = addCircle( $xoff, $yoff, $sc * $IMAGE->{ 'RADIUS' }, 1 );
    $outer->{ 'OUTER' } = 1;
    $IMAGE->{ 'OUTER-DISC' } = $outer;

    my $inner = addCircle( $xoff, $yoff, $sc * ( $h - $rad ), 1 );
    $inner->{ 'INNER' } = 1;

    $IMAGE->{ 'REFLECTORS' }->{ 'MAIN' } = {
        'X'     =>  $xoff,
        'Y'     =>  $yoff,
        'RAD'   =>  $sc * sqrt( $h * $h - $rad * $rad ),
    };

    my $prev;
    for ( my $n = 0; $n < $options{ 'N' }; $n++ ) {

        my $c = addCircle( 
            $xoff + $sc * $h * cos( $n * 2 * pi / $options{ 'N' } ),
            $yoff + $sc * $h * sin( $n * 2 * pi / $options{ 'N' } ), 
            $sc * $rad, 
            1, 
        );

        addOuterIntersection( $c, $outer );
        addIntersection( $c, $inner );
        addIntersection( $c, $prev ) if ( $n > 0 );

        $prev = $c;
    }

    addIntersection( $IMAGE->{ 'CIRCLES' }->[ 2 ], $prev );

    my $w = $h + $rad - sqrt( $h * $h - $rad * $rad );
    my $s = $w * $w / ( 2 * ( $rad + $w ) );

    for ( my $n = 0; $n < $options{ 'N' }; $n++ ) {

        my $c = addCircle( 
            $xoff + $sc * ( $IMAGE->{ 'RADIUS' } - $s ) * cos( ( 2 * $n + 1 ) * pi / $options{ 'N' } ),
            $yoff + $sc * ( $IMAGE->{ 'RADIUS' } - $s ) * sin( ( 2 * $n + 1 ) * pi / $options{ 'N' } ), 
            $sc * $s, 
            1, 
        );

        addOuterIntersection( $c, $outer );
        addIntersection( $c, $IMAGE->{ 'CIRCLES' }->[ 2 + $n ] );
        addIntersection( $c, $IMAGE->{ 'CIRCLES' }->[ 2 + ( $n + 1 ) % $options{ 'N' } ] );
    }
}

############################################################
# Process a multi (>3 initial) disc image

sub processMultiImage {

    # Create discs in the outer voids
    for ( my $n = 0; $n < $options{ 'N' }; $n++ ) {

        processSet(
            $IMAGE->{ 'CIRCLES' }->[ 0 ],
            $IMAGE->{ 'CIRCLES' }->[ 2 + $n ],
            $IMAGE->{ 'CIRCLES' }->[ 2 + ( $n + 1 ) % $options{ 'N' } ],
            $IMAGE->{ 'CIRCLES' }->[ 2 + $n + $options{ 'N' } ],
            0,
        );
    }

    # Now reflect all of these new discs in the main-reflector
    my $count = scalar @{ $IMAGE->{ 'CIRCLES' } };
    for ( my $n = 2 + $options{ 'N' }; $n < $count; $n++ ) {
        addReflectCircle( $IMAGE->{ 'CIRCLES' }->[ $n ], $IMAGE->{ 'REFLECTORS' }->{ 'MAIN' } );
    }

    # Also reflect the reflectors (for completeness)
    for my $rid ( keys $IMAGE->{ 'REFLECTORS' } ) {

        next if $rid eq 'MAIN';

        my $reflector = $IMAGE->{ 'REFLECTORS' }->{ $rid };
        $reflector->{ 'DEPTH' } = 0;    # Stop findReflectedCircle() complaining

        my $new_reflector = findReflectedCircle( 
                                $reflector,
                                $IMAGE->{ 'REFLECTORS' }->{ 'MAIN' },
        );

        my $new_id = 'R-' . $reflector->{ 'ID' };
        $new_reflector->{ 'ID' } = $new_id;

        $IMAGE->{ 'REFLECTORS' }->{ $new_id } = $new_reflector;
    }
}

############################################################
# Initialise a (3 initial disc) image

sub setupImage {

    if ( $options{ 'T' } ) {
        $IMAGE->{ 'RAD-1' } = 120;
        $IMAGE->{ 'RAD-2' } = 120;
        $IMAGE->{ 'RAD-3' } = 120;
    }

    # Position the 3 initial circles
    my $p = $IMAGE->{ 'RAD-1' } + $IMAGE->{ 'RAD-2' };
    my $q = $IMAGE->{ 'RAD-2' } + $IMAGE->{ 'RAD-3' };
    my $r = $IMAGE->{ 'RAD-3' } + $IMAGE->{ 'RAD-1' };
    
    my $x3 = ( ( $p * $p ) + ( $r * $r ) - ( $q * $q ) ) / ( 2.0 * $p );
    my $y3 = sqrt( ( $r * $r ) - ( $x3 * $x3 ) );
    
    # Find the circumscribing circle
    my $k1 = 1.0 / $IMAGE->{ 'RAD-1' };
    my $k2 = 1.0 / $IMAGE->{ 'RAD-2' };
    my $k3 = 1.0 / $IMAGE->{ 'RAD-3' };
    
    my $k4a = $k1 + $k2 + $k3 + 2.0 * sqrt( ( $k1 * $k2 ) +( $k2 * $k3 ) +( $k3 * $k1 ) );
    my $k4b = $k1 + $k2 + $k3 - 2.0 * sqrt( ( $k1 * $k2 ) +( $k2 * $k3 ) +( $k3 * $k1 ) );
    help( "Invalid starting radii" ) if ( ( $k4a > 0 ) && ( $k4b > 0 ) );
    
    my $kro = -1.0 / ( $k4a < 0 ? $k4a : $k4b );
    
    # Find the internal circle
    $p = $kro - $IMAGE->{ 'RAD-2' };
    $q = $IMAGE->{ 'RAD-1' } + $IMAGE->{ 'RAD-2' };
    $r = $kro - $IMAGE->{ 'RAD-1' };
    
    my $xo = ( ( $q * $q ) + ( $r * $r ) - ( $p * $p ) ) / ( 2.0 * $q );
    my $yo = sqrt( ( $r * $r ) - ( $xo * $xo ) );
    
    my $kri = 1.0 / ( $k4a > 0 ? $k4a : $k4b );
    
    $p = $kri + $IMAGE->{ 'RAD-2' };
    $q = $kri + $IMAGE->{ 'RAD-1' };
    $r = $IMAGE->{ 'RAD-1' } + $IMAGE->{ 'RAD-2' };
    
    my $d1 = dist( $xo, $yo, $x3, $y3 );
    my $sgn = ( abs( $d1 + $IMAGE->{ 'RAD-3' } - $kro ) > 0.001 ) ? -1 : 1;
    
    my $outer = addCircle( $xo, $sgn * $yo, $kro, 0, $YELLOW );
    $outer->{ 'OUTER' } = 1;
    $IMAGE->{ 'OUTER-DISC' } = $outer;

    addCircle( 0, 0, $IMAGE->{ 'RAD-1' }, 1, $YELLOW );
    addCircle( $IMAGE->{ 'RAD-1' } + $IMAGE->{ 'RAD-2' }, 0, $IMAGE->{ 'RAD-2' }, 1, $YELLOW );
    addCircle( $x3, $y3, $IMAGE->{ 'RAD-3' }, 1, $YELLOW );

    # Translate and scale the circles
    my $sc = $IMAGE->{ 'SCALE' };
    for my $circle ( @{ $IMAGE->{ 'CIRCLES' } } ) {
    
        $circle->{ 'X' } = $IMAGE->{ 'X-OFF' } + ( $circle->{ 'X' } - $xo ) * $sc;
        $circle->{ 'Y' } = $IMAGE->{ 'Y-OFF' } + ( $circle->{ 'Y' } - ( $sgn * $yo ) ) * $sc;
        $circle->{ 'RAD' } *= $IMAGE->{ 'SCALE' };
    }

    # Now, add the intersections for these circles
    addOuterIntersection( $IMAGE->{ 'CIRCLES' }->[ 0 ],  $IMAGE->{ 'CIRCLES' }->[ 1 ] ); 
    addOuterIntersection( $IMAGE->{ 'CIRCLES' }->[ 0 ],  $IMAGE->{ 'CIRCLES' }->[ 2 ] ); 
    addOuterIntersection( $IMAGE->{ 'CIRCLES' }->[ 0 ],  $IMAGE->{ 'CIRCLES' }->[ 3 ] ); 

    for ( my $s1 = 1; $s1 < scalar @{ $IMAGE->{ 'CIRCLES' } } - 1; $s1++ ) {

        for ( my $s2 = $s1 + 1; $s2 < scalar @{ $IMAGE->{ 'CIRCLES' } }; $s2++ ) {

            addIntersection(
                    $IMAGE->{ 'CIRCLES' }->[ $s1 ],  
                    $IMAGE->{ 'CIRCLES' }->[ $s2 ] 
            ); 
        }
    }
}

############################################################
# Iterate the image 

sub iterateImage {
 
    my $template_discs = dclone( $IMAGE->{ 'CIRCLES' } );

    for ( my $d = 0; $d < scalar @{ $IMAGE->{ 'CIRCLES' } }; $d++ ) {

        my $disc = $IMAGE->{ 'CIRCLES' }->[ $d ];

        next if $disc->{ 'OUTER' };

        for my $template_disc ( @{ $template_discs } ) {

            next if $template_disc->{ 'OUTER' };

            my $scale = $disc->{ 'RAD' } / $IMAGE->{ 'OUTER-DISC' }->{ 'RAD' };
            my $new_rad = $template_disc->{ 'RAD' } * $scale;

            next if $new_rad < $IMAGE->{ 'MIN-RAD' };

            push @{ $IMAGE->{ 'CIRCLES' } }, {

                'ID'    =>  scalar @{ $IMAGE->{ 'CIRCLES' } },
                'DEPTH' =>  $disc->{ 'DEPTH' } + 1,
                'COLOR' =>  $template_disc->{ 'COLOR' },
                'RAD'   =>  $template_disc->{ 'RAD' } * $scale,
                'X'     =>  $disc->{ 'X' } + $template_disc->{ 'X' } * $scale,
                'Y'     =>  $disc->{ 'Y' } + $template_disc->{ 'Y' } * $scale,
            }
        }
    }
} 

############################################################
# List the circle and reflector details to a file

sub listCircles {
 
    open my $fh, '>', $options{ 'l' } or die "Could not open $options{ 'l' }: $!\n";

    printf $fh "# Circles\n";
    for ( my $c = 0; $c < scalar @{ $IMAGE->{ 'CIRCLES' } }; $c++ ) {

        my $circle = $IMAGE->{ 'CIRCLES' }->[ $c ];

        next if ( $options{ 'T' } && ( $circle->{ 'OUTER' } || $circle->{ 'SETUP' } ) );

        printf $fh "%f %f %f\n",
                    $circle->{ 'X' },
                    $circle->{ 'Y' },
                    $circle->{ 'RAD' },
        ;
    }

    printf $fh "\n# Reflectors:\n";
    for my $rid ( keys %{ $IMAGE->{ 'REFLECTORS' } } ) {

        my $reflector = $IMAGE->{ 'REFLECTORS' }->{ $rid };

        printf $fh "%f %f %f\n",
                    $reflector->{ 'X' },
                    $reflector->{ 'Y' },
                    $reflector->{ 'RAD' },
        ;
    }

    close $fh;

    exit 0;
}

############################################################
# Reflect one circle in another

sub findReflectedCircle {

    my $circle = shift;
    my $reflector = shift;

    if ( $reflector->{ 'IS_LINE' } ) {

        my $m = $reflector->{ 'SLOPE' };
        my $c = $reflector->{ 'CONST' };

        my $v = ( $circle->{ 'X' } + $m * ( $circle->{ 'Y' } - $c ) ) / ( 1 + $m * $m );

        my $x = 2 * $v - $circle->{ 'X' };
        my $y = 2 * $v * $m - $circle->{ 'Y' } + 2 * $c;
        
        return {
            'X'     =>  $x,
            'Y'     =>  $y,
            'RAD'   =>  $circle->{ 'RAD' },
            'DEPTH' =>  $circle->{ 'DEPTH' } + 1,
        }
    }

    my $d = circleDist( $circle, $reflector );
    
    my $v = sq( $reflector->{ 'RAD' } ) / ( sq( $d ) - sq( $circle->{ 'RAD' } ) );

    my $x = $reflector->{ 'X' } + $v * ( $circle->{ 'X' } - $reflector->{ 'X' } );
    my $y = $reflector->{ 'Y' } + $v * ( $circle->{ 'Y' } - $reflector->{ 'Y' } );

    my $r = $circle->{ 'RAD' } * $v * ( ( $v < 0 ) ? -1.0 : 1.0 );

    return {
        'X'     =>  $x,
        'Y'     =>  $y,
        'RAD'   =>  $r,
        'DEPTH' =>  $circle->{ 'DEPTH' } + 1,
    }
}

############################################################
# Reflect one circle in another and add to the list

sub addReflectCircle {

    my $circle = shift;
    my $reflector = shift;

    my $new_circle = findReflectedCircle( $circle, $reflector );

    return addCircle( 
        $new_circle->{ 'X' },
        $new_circle->{ 'Y' },
        $new_circle->{ 'RAD' },
        $new_circle->{ 'DEPTH' },
    );
}

############################################################
# Process a standard 3 initial disc image

sub processImage {

    if ( $options{ 'T' } ) {

        my $discs;
        my $outer;
        map {
            if ( $_->{ 'OUTER' } ) {
                $outer = $_;
            } else {
                $_->{ 'SETUP' } = 1;
                push @{ $discs }, $_ ;
            }
        } ( @{ $IMAGE->{ 'CIRCLES' } } );
    
        reflectIn( $outer, $discs->[ 0 ], $discs->[ 1 ], $discs->[ 2 ] );
        push @{ $discs }, $IMAGE->{ 'CIRCLES' }->[ -1 ];

        processSet( @{ $discs }, 0 );
    }
    else {
    
        # process the initial set at depth 0
        processSet( @{ $IMAGE->{ 'CIRCLES' } }, 0 );
    }
}

############################################################
# Given 4 circles, reflect 1 in the others to get a new set

sub processSet {

    my $c0 = shift;
    my $c1 = shift;
    my $c2 = shift;
    my $c3 = shift;
    my $d = shift;

    return if $d > $IMAGE->{ 'MAX-DEPTH' };

    my $n1 = reflectIn( $c0, $c1, $c2, $c3 );
    processSet( $n1, $c1, $c2, $c3, $d+1 ) if $n1;

    my $n2 = reflectIn( $c1, $c2, $c3, $c0 );
    processSet( $n2, $c2, $c3, $c0, $d+1 ) if $n2;

    my $n3 = reflectIn( $c2, $c3, $c0, $c1 );
    processSet( $n3, $c3, $c0, $c1, $d+1 ) if $n3;

    my $n4 = reflectIn( $c3, $c0, $c1, $c2 );
    processSet( $n4, $c0, $c1, $c2, $d+1 ) if $n4;
}

############################################################
# Add a reflector given the kissing circles

sub reflectIn {

    my $c0 = shift;
    my $c1 = shift;
    my $c2 = shift;
    my $c3 = shift;

    # Dont process this circle if the radius is already small
    return if $c0->{ 'RAD' } < $IMAGE->{ 'MIN-RAD' };

    # If this reflector has already been used then this is just the
    # reverse of an existing reflection...
    my $rid = reflectorTag( $c1, $c2, $c3 );
    return if $IMAGE->{ 'REFLECTORS' }->{ $rid };

    my $reflector = addReflector( $c1, $c2, $c3 );

    # All reflections should be from the outside to the in
    my $dist = dist( $c0->{ 'X' }, $c0->{ 'Y' },
                    $reflector->{ 'X' }, $reflector->{ 'Y' } );
    return if $dist < $reflector->{ 'RAD' };

    my $rc = addReflectCircle( $c0, $reflector );

    printf( "reflect %s in (%s, %s, %s) -> %s\n",
                        $c0->{ 'ID' }, 
                        $c1->{ 'ID' }, 
                        $c2->{ 'ID' }, 
                        $c3->{ 'ID' },
                        $rc->{ 'ID' },
    ) if $options{ 'v' };

    addIntersection( $rc, $c1 ); 
    addIntersection( $rc, $c2 ); 
    addIntersection( $rc, $c3 ); 

    return $rc;
}

############################################################
# Add a reflector given the kissing circles

sub addReflector {

    my $c1 = shift;
    my $c2 = shift;
    my $c3 = shift;

    my $rid = reflectorTag( $c1, $c2, $c3 );

    my $reflector = circleFrom3Pts( 
        $IMAGE->{ 'INTERSECTIONS' }->{ intersectionTag( $c1, $c2 ) },
        $IMAGE->{ 'INTERSECTIONS' }->{ intersectionTag( $c2, $c3 ) },
        $IMAGE->{ 'INTERSECTIONS' }->{ intersectionTag( $c3, $c1 ) },
    );

    $reflector->{ 'ID' } = $rid;

    $IMAGE->{ 'REFLECTORS' }->{ $rid } = $reflector;

    return $reflector;
}

############################################################
# Render the image

sub drawImage {

    open my $fh, '>', $IMAGE->{ 'PS-TEMP' } or die "Could not open $IMAGE->{ 'PS-TEMP' }: $!\n";
    
    my $background = $options{ 'M' } || $options{ 'B' } ? '' : '0 0 mv 1000 0 ln 1000 1000 ln 0 1000 ln fill';
    my $label_color = $options{ 'M' } ? '0 setgray' : '1 setgray';

    print $fh "%!PS\n%%BoundingBox: 0 0 1000 1000\n%%EndComments\n
    
        <<
            /PageSize [ 1000 1000 ]
            /ImagingBBox null
        >> setpagedevice
    
        /mv { moveto } bind def
        /ln { lineto } bind def
    
        /Arial-Bold findfont 16 scalefont setfont
    
        /disc { % x y rad fill disc -
            gsave
            /f exch def
            /r exch def
            translate
            0 5 360 {
                /a exch def
                a cos r mul
                a sin r mul
                a 0 eq { mv } { ln } ifelse
            } for
            f { fill } { stroke } ifelse
            grestore
        } def
    
        /lbl {  % x y (l) lbl -
            3 1 roll
            gsave
            $label_color
            translate
            dup stringwidth pop -2 div -4 moveto show
            grestore
        } def
    
        $background
    
        500 500 translate
        $IMAGE->{ 'ANGLE' } rotate

        1 setlinejoin
        $IMAGE->{ 'LINE-WIDTH' } setlinewidth
    \n";
    
    drawCircles( $fh );
    drawIntersections( $fh ) if $options{ 'I' };
    drawReflectors( $fh ) if $options{ 'R' };

    close $fh;
    
    system( sprintf "convert '%s' '%s'", $IMAGE->{ 'PS-TEMP' }, $IMAGE->{ 'DEST' } );
    unlink $IMAGE->{ 'PS-TEMP' } unless $options{ 'k' };
}

############################################################
# Draw the intersection points

sub drawIntersections {

    my $fh = shift;

    print $fh "% Intersections";

    my $color = $options{ 'M' } ? $BLACK : $YELLOW;

    for my $iid ( keys %{ $IMAGE->{ 'INTERSECTIONS' } } ) {

        my $intersection = $IMAGE->{ 'INTERSECTIONS' }->{ $iid };

        printf $fh "%s %f %f %f true disc\n",
                    $color,
                    $intersection->{ 'X' },
                    $intersection->{ 'Y' },
                    3,
        ;
    }

    return;
}

############################################################
# Render the reflection circles

sub drawReflectors {

    my $fh = shift;

    print $fh "% Reflectors\n";

    my $color = $options{ 'M' } ? $BLACK
                    : $options{ 'F' } ? $REFLECTOR_COLOR
                    : $MAGENTA;

    printf $fh "%s\n", $color;

    for my $rid ( keys %{ $IMAGE->{ 'REFLECTORS' } } ) {
    
        my $reflector = $IMAGE->{ 'REFLECTORS' }->{ $rid };
    
        if ( $reflector->{ 'IS_LINE' } ) {

            printf $fh "%f %f mv %f %f ln stroke\n",
                -2 * $IMAGE->{ 'RADIUS' },
                -2 * $IMAGE->{ 'RADIUS' } * $reflector->{ 'SLOPE' } + $reflector->{ 'CONST' },
                2 * $IMAGE->{ 'RADIUS' },
                2 * $IMAGE->{ 'RADIUS' } * $reflector->{ 'SLOPE' } + $reflector->{ 'CONST' },
            ;
        }
        else {
        
            printf $fh "%f %f %f false disc\n",
                    $reflector->{ 'X' },
                    $reflector->{ 'Y' },
                    $reflector->{ 'RAD' },
            ;
        }
    }

    return;
}

############################################################
# Render the circles

sub drawCircles {

    my $fh = shift;

    print $fh "% Discs\n";

    printf $fh "%s ", $BLACK if $options{ 'M' };
    printf $fh "%s ", $CIRCLE_COLOR if $options{ 'F' };

    for ( my $c = 0; $c < scalar @{ $IMAGE->{ 'CIRCLES' } }; $c++ ) {
    
        my $circle = $IMAGE->{ 'CIRCLES' }->[ $c ];
    
        next if ( $options{ 'T' } && ( $circle->{ 'OUTER' } || $circle->{ 'SETUP' } ) );

        next if $circle->{ 'OUTER' } && ! $options{ 'D' };
        next if $circle->{ 'OUTER' } && ( $options{ 'D' } ne 'OUTER' ) && ( $options{ 'D' } ne 'BOTH' );
        next if $circle->{ 'INNER' } && ! $options{ 'D' };
        next if $circle->{ 'INNER' } && ( $options{ 'D' } ne 'INNER' ) && ( $options{ 'D' } ne 'BOTH' );

        printf $fh "%s ", $circle->{ 'COLOR' } unless $options{ 'M' } || $options{ 'F' };

        printf $fh "%f %f %f false disc %s\n",
                    $circle->{ 'X' },
                    $circle->{ 'Y' },
                    $circle->{ 'RAD' },
                    $c == 0 ?  "% Outer"
                    : $c == 1 ? "% Inner"
                    : "",
        ;
    
        if ( $c && ( $options{ 'L' } ) ) {
    
            printf $fh "%f %f (%s) lbl\n",
                    $circle->{ 'X' },
                    $circle->{ 'Y' },
                    $circle->{ 'ID' },
            ;
        }
    }

    return;
}

############################################################
# Add a circle

sub addCircle {

    my $x = shift;
    my $y = shift;
    my $rad = shift;
    my $depth = shift;
    my $col = shift || '1 1 0 setrgbcolor';

    my $cid = sprintf "C%03d", scalar @{ $IMAGE->{ 'CIRCLES' } };

    my $circle = {
        'ID'    =>  $cid,
        'X'     =>  $x,
        'Y'     =>  $y,
        'RAD'   =>  $rad,
        'COLOR' =>  $col,
        'DEPTH' =>  $depth,
    };

    push @{ $IMAGE->{ 'CIRCLES' } }, $circle;

    return $circle;
}

############################################################
# Add a intersection point for an internal and outer circle

sub addOuterIntersection {

    my $co = shift;
    my $ci = shift;

    my $iid = intersectionTag( $co, $ci );

    unless ( $IMAGE->{ 'INTERSECTIONS' }->{ $iid } ) {

        $IMAGE->{ 'INTERSECTIONS' }->{ $iid } = {
            'X' =>  
                ( ( $co->{ 'RAD' } * $ci->{ 'X' } ) - ( $ci->{ 'RAD' } * $co->{ 'X' } ) ) / 
                ( $co->{ 'RAD' } - $ci->{ 'RAD' } ),
            'Y' =>
                ( ( $co->{ 'RAD' } * $ci->{ 'Y' } ) - ( $ci->{ 'RAD' } * $co->{ 'Y' }  ) ) / 
                ( $co->{ 'RAD' } - $ci->{ 'RAD' } ),
        };
    }

    return;
}

############################################################
# Add an intersection pointer between 2 internal circles

sub addIntersection {

    my $c1 = shift;
    my $c2 = shift;

    return addOuterIntersection( $c1, $c2 ) if ( $c1->{ 'OUTER' } || $c2->{ 'OUTER' } );

    my $iid = intersectionTag( $c1, $c2 );

    unless ( $IMAGE->{ 'INTERSECTIONS' }->{ $iid } ) {

        $IMAGE->{ 'INTERSECTIONS' }->{ $iid } = {
            'X' =>  
                ( ( $c1->{ 'RAD' } * $c2->{ 'X' } ) + ( $c2->{ 'RAD' } * $c1->{ 'X' } ) ) / 
                ( $c1->{ 'RAD' } + $c2->{ 'RAD' } ),
            'Y' =>
                ( ( $c1->{ 'RAD' } * $c2->{ 'Y' } ) + ( $c2->{ 'RAD' } * $c1->{ 'Y' }  ) ) / 
                ( $c1->{ 'RAD' } + $c2->{ 'RAD' } ),
        };
    }

    return;
}

############################################################
# Return the absolute value

sub abs {

    my $v = shift;
    
    return ( $v > 0 ) ? $v : -1.0 * $v;
}

############################################################
# Return the tag for the given intersection

sub intersectionTag {

    my $c1 = shift;
    my $c2 = shift;

    return sprintf "I-%s-%s", sort( $c1->{ 'ID' }, $c2->{ 'ID' } );
}

############################################################
# Return the tag for the given reflector

sub reflectorTag {

    my $c1 = shift;
    my $c2 = shift;
    my $c3 = shift;

    return sprintf "R-%s-%s-%s", sort( $c1->{ 'ID' }, $c2->{ 'ID' }, $c3->{ 'ID' } );
}

############################################################
# Utility function to square a number

sub sq {
    my $v = shift;
    return $v * $v;
}

############################################################
# Find the circle through the 3 given intersection points

sub circleFrom3Pts {

    my $i1 = shift;
    my $i2 = shift;
    my $i3 = shift;

    # Check that the points are not colinear
    my $v = $i1->{ 'X' } * ( $i2->{ 'Y' } - $i3->{ 'Y' } ) +
            $i2->{ 'X' } * ( $i3->{ 'Y' } - $i1->{ 'Y' } ) +
            $i3->{ 'X' } * ( $i1->{ 'Y' } - $i2->{ 'Y' } );

    if ( $v * $v < $SMALL ) {

        my $s = ( $i1->{ 'X' } - $i2->{ 'X' } ) > $SMALL ?
                    ( $i1->{ 'Y' } - $i2->{ 'Y' } ) / ( $i1->{ 'X' } - $i2->{ 'X' } )
                    :
                    ( $i2->{ 'Y' } - $i3->{ 'Y' } ) / ( $i2->{ 'X' } - $i3->{ 'X' } )
        ;

        return {
            'IS_LINE'   =>  'Y',
            'SLOPE'     =>  $s,
            'CONST'     =>  $i1->{ 'Y' } / ( $s * $i1->{ 'X' } ),
        };
    }

    my $a = 2.0 * ( $i2->{ 'X' } - $i1->{ 'X' } ); 

    # If i2x = i1x then i3x must be different since the points are not collinear
    if ( $a == 0 ) {
        my $tmp = $i1;
        $i1 = $i2;
        $i2 = $i3;
        $i3 = $tmp;
    
        $a = 2.0 * ( $i2->{ 'X' } - $i1->{ 'X' } ); 
    }

    my $b = 2.0 * ( $i2->{ 'Y' } - $i1->{ 'Y' } ); 
    my $c = sq( $i2->{ 'X' } ) + sq( $i2->{ 'Y' } )
            - sq( $i1->{ 'X' } ) - sq( $i1->{ 'Y' } );

    my $d = 2.0 * ( $i3->{ 'X' } - $i1->{ 'X' } ); 
    my $e = 2.0 * ( $i3->{ 'Y' } - $i1->{ 'Y' } ); 
    my $f = sq( $i3->{ 'X' } ) + sq( $i3->{ 'Y' } )
            - sq( $i1->{ 'X' } ) - sq( $i1->{ 'Y' } );

    my $y = ( ( $a * $f ) - ( $c * $d ) ) / ( ( $a * $e ) - ( $b * $d ) );
    my $x = ( $c - ( $b * $y ) ) / $a;

    my $r = dist( $x, $y, $i1->{ 'X' }, $i1->{ 'Y' } );

    return {
        'X'     =>  $x,
        'Y'     =>  $y,
        'RAD'   =>  $r,
    };
}

############################################################
# Find the distance between circle centres

sub circleDist {

    my $c1 = shift;
    my $c2 = shift;

    return dist( 
        $c1->{ 'X' }, $c1->{ 'Y' },
        $c2->{ 'X' }, $c2->{ 'Y' },
    );
}

############################################################
# Find the distance between 2 points centres

sub dist {

    my $x1 = shift;
    my $y1 = shift;
    my $x2 = shift;
    my $y2 = shift;

    return sqrt( ( ( $x1 - $x2 ) * ( $x1 - $x2 ) ) + ( ( $y1 - $y2 ) * ( $y1 - $y2 ) ) );
}

############################################################
# Help doco

sub help {

    print "\n@_\n" if @_;

    printf "\n$0 " .
        " [-1 <rad>] [-2 <rad>] [-3 <rad>] [-A <ang>] [-B] [-d <depth>] [-D <tag>] 
          [-F] [-h] [-i] [-I] [-k <path>] [-l <path>] [-L] [-m <rad>] [-M] [-N <num>]
          [-o <path>] [-r <rad>] [-R] [-s <scale>] [-T] [-x <xoff>] [-Y <yoff>]

        where
            -1 <rad>    radius of the first setup circle (default 30)
            -2 <rad>    radius of the second setup circle (default 20)
            -3 <rad>    radius of the third setup circle (default 10)
            -d <depth>  max depth of reflections (default 1)
            -h          this help message
            -i          iterate the image discs
            -k <path>   temp ps file (to retain)
            -l <path>   list the discs details to a file (no image produced)
            -m <rad>    minimum radius to draw (default 2)
            -o <path>   destination image (default: apollonian.png)
            -r <rad>    radius of the circumscribing circle (default 450)

        drawing switches
            -A <ang>    degrees to rotate the image (default 0)
            -B          don't draw the black background (for layering images)
            -D <tag>    draw the OUTER, INNER or BOTH disc boundaries
            -F          use fixed color for circles
            -I          draw the intersections
            -L          label the circles
            -M          draw the image in mono
            -N <num>    number of internal circles to start the image
            -R          draw the reflectors
            -S <scale>  drawing scale (default 1.0)
            -T          configure for a triangular gasket
            -W <wid>    drawing linewidth (default 1)
            -X <xoff>   horizontal drawing offset (default 0)
            -Y <xoff>   horizontal drawing offset (default 0)
    \n";

    exit;
}
