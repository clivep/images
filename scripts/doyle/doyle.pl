#!/usr/bin/perl

use warnings;
use strict;

############################################################
#
#   doyle.pl
#
#   Display Doyle Spirals for the supplied parameters
#
############################################################

use Math::Trig;
use Getopt::Std;
use Readonly;

use Data::Dumper;

############################################################
# The global operating details

my Readonly $PS_TEMP    = '/tmp/doyle.ps';

my Readonly $EPSILON = 1e-10;

my Readonly $IMAGE = {
    'DISCS'         =>  [],
    'SPHERE-DEF'    =>  {
        'MIN-COLOR-PCNT'    => 30,
        'RED'               => 255,
        'GREEN'             => 0,
        'BLUE'              => 0,
        'RAD'               => 100,
        'LAMP_X'            => 60,
        'LAMP_Y'            => -60,
        'LAMP_Z'            => 150,
        'SHEEN'             => 800,
        'COLOR-DEFS'        => [],
    },
};

my Readonly $BLUE_SPHERE_DEF     = { 'func' => 'blueSphere', 'rgb' => [ 0, 0, 255 ] };
my Readonly $GREEN_SPHERE_DEF    = { 'func' => 'greenSphere', 'rgb' => [ 0, 255, 0, ] };
my Readonly $RED_SPHERE_DEF      = { 'func' => 'redSphere', 'rgb' => [ 255, 0, 0, ] };
my Readonly $YELLOW_SPHERE_DEF   = { 'func' => 'yellowSphere', 'rgb' => [ 255, 255, 0, ] };

my Readonly @SPECTRUM = (
        [ 148,   0, 211],   # Violet
        [  75,   0, 130],   # Indigo
        [   0,   0, 255],   # Blue
        [   0, 255,   0],   # Green
        [ 255, 255,   0],   # Yellow
        [ 255, 127,   0],   # Orange
        [ 255,   0 ,  0],   # Red
        [ 148,   0, 211],   # Violet
);

############################################################
# Check the runline options

my %options;
getopts( 'dhmC:D:F:KM:O:P:Q:R:S:W:X:Y:', \%options );

help() if $options{ 'h' };
help( "Missing mandatory parameters" ) unless $ARGV[ 1 ];

$IMAGE->{ 'MOBIUS_XFORM' } = $options{ 'm' };

$IMAGE->{ 'OUTPUT-FILE' } = $options{ 'O' } || 'doyle.png';

$IMAGE->{ 'MAX-DRAWING-RAD' } = $options{ 'P' } || 200;
$IMAGE->{ 'MIN-PROCESSING-RAD' } = $options{ 'Q' } || 1;
$IMAGE->{ 'MAX-PROCESSING-RAD' } = $options{ 'R' } || 100;

$IMAGE->{ 'FILLING-SCHEME' } = $options{ 'F' } || 'none';
$IMAGE->{ 'SPECTRUM-MUL' } = $options{ 'M' } || 1;

$IMAGE->{ 'SCALE' } = $options{ 'S' } || 1;

$IMAGE->{ 'DEPTH' } = $options{ 'D' } || 1000;
$IMAGE->{ 'WIDTH' } = $options{ 'W' } || 1000;

$IMAGE->{ 'P-VAL' } = $ARGV[ 0 ];
$IMAGE->{ 'Q-VAL' } = $ARGV[ 1 ];

$IMAGE->{ 'X-OFF' } = $options{ 'X' } || $IMAGE->{ 'WIDTH' } / 2;
$IMAGE->{ 'Y-OFF' } = $options{ 'Y' } || $IMAGE->{ 'DEPTH' } / 2;

# Check this last as other params may have a bearing
my $default_scheme = $IMAGE->{ 'FILLING-SCHEME' } eq 'sphere' ? '[ Y ]' : 'yellow';
$IMAGE->{ 'COLOR-SCHEME' } = $options{ 'C' } || $default_scheme;
help( "Invalid color scheme" ) unless
                getDrawColor( $IMAGE->{ 'COLOR-SCHEME' } ) ||
                checkSpectrumRenderer() ||
                checkSphereRenderer()
;

############################################################
# Process the image

processImage();
drawImage() unless $options{ 'd' };
dumpData() if $options{ 'D' };

exit;

############################################################
# Display the image data

sub dumpData {

    for( my $d = 1; $d < scalar @{ $IMAGE->{ 'DISCS' } }; $d++ ) {

        my $disc = $IMAGE->{ 'DISCS' }->[ $d ];

        printf "%3d - x: % 0.3f y: % 0.3f r: %0.3f\n",
                        $d,
                        $disc->{ 'X' },
                        $disc->{ 'Y' },
                        $disc->{ 'RAD' },
        ;
    }

    return;
}

############################################################
# Process the image

sub processImage {

    my $p = $IMAGE->{ 'P-VAL' };
    my $q = $IMAGE->{ 'Q-VAL' };

    my $root = find_root( 2, 0, $p, $q );

    die "Failed to find root for p=$p q=$q\n" unless $root->{ 'ok' };
        
    my $a = [
        $root->{ 'z' } * cos( $root->{ 't' } ),  
        $root->{ 'z' } * sin( $root->{ 't' } ),
    ];

    my $coroot = {
        'z' => _pw( $root->{ 'z' }, $p / $q ),
        't' => ( $p * $root->{ 't' } + 2 * pi ) / $q,
    };

    my $b = [
        $coroot->{ 'z' } * cos( $coroot->{ 't' } ),  
        $coroot->{ 'z' } * sin( $coroot->{ 't' } ), 
    ];

    my $start = $a;
    for ( my $i = 0; $i < $q; $i++) {

        processSpiral( $root->{ 'r' }, $start, $a, $q );
        $start = cmul( $start, $b );
    }

    return;
}

############################################################
# Multiply 2 complex values

sub cmul {

    my $z1 = shift;
    my $z2 = shift;

    return [
            $z1->[ 0 ] * $z2->[ 0 ] - $z1->[ 1 ] * $z2->[ 1 ],
            $z1->[ 0 ] * $z2->[ 1 ] + $z1->[ 1 ] * $z2->[ 0 ],
    ];
}

############################################################
# Find the modulus of the given complex value

sub cmod {

    my $z = shift;

    return sqrt( $z->[ 0 ] * $z->[ 0 ] + $z->[ 1 ] * $z->[ 1 ] );
}

############################################################
# Invert the given complex value

sub cinv {

    my $z = shift;

    my $d = $z->[ 0 ] * $z->[ 0 ] + $z->[ 1 ] * $z->[ 1 ];

    return [ $z->[ 0 ] / $d, -$z->[ 1 ] / $d ];
}

############################################################
# Add a disc to the image list

sub addDisc {

    my $x = shift;
    my $y = shift;
    my $rad = shift;
    my $p = shift;
    my $q = shift;

    my $disc = {
        'ID'            =>  scalar @{ $IMAGE->{ 'DISCS' } },
        'X'             =>  $x,
        'Y'             =>  $y,
        'RAD'           =>  $rad,
        'P'             =>  $p,
        'Q'             =>  $q,
    };

    push @{ $IMAGE->{ 'DISCS' } }, $disc;

    return $disc;
}

############################################################
# Apply the mobius transformation to a point

sub mobiusTransformPoint {

    my $x = shift;
    my $y = shift;

    my $v = ( ( $x + 1 ) *( $x + 1 ) ) + ( $y * $y );

    return [ 10000, 10000 ] unless $v;

    return [
        ( ( $x * $x ) + ( $y * $y ) - 1 ) / $v,
        2 * $y / $v,
    ];
}

############################################################
# Apply the mobuis transformation to a disc

sub mobiusTransformDisc {

    my $disc = shift;

    my $pt1 = mobiusTransformPoint( $disc->{ 'X' } + $disc->{ 'RAD' }, $disc->{ 'Y' } );
    my $pt2 = mobiusTransformPoint( $disc->{ 'X' } - $disc->{ 'RAD' }, $disc->{ 'Y' } );
    my $pt3 = mobiusTransformPoint( $disc->{ 'X' }, $disc->{ 'Y' } + $disc->{ 'RAD' } );

    my $n1 = ( $pt1->[ 0 ] * $pt1->[ 0 ] ) + ( $pt1->[ 1 ] * $pt1->[ 1 ] );
    my $n2 = ( $pt2->[ 0 ] * $pt2->[ 0 ] ) + ( $pt2->[ 1 ] * $pt2->[ 1 ] );
    my $n3 = ( $pt3->[ 0 ] * $pt3->[ 0 ] ) + ( $pt3->[ 1 ] * $pt3->[ 1 ] );

    my $v1 = ( $pt1->[ 0 ] - $pt2->[ 0 ] ) * ( $n2 - $n3 ) -
                        ( $pt2->[ 0 ] - $pt3->[ 0 ] ) * ( $n1 - $n2 );

    my $v2 = ( $pt2->[ 1 ] - $pt1->[ 1 ] ) * ( $pt2->[ 0 ] - $pt3->[ 0 ] ) - 
                        ( $pt1->[ 0 ] - $pt2->[ 0 ] ) * ( $pt3->[ 1 ] - $pt2->[ 1 ] );

    $disc->{ 'Y' } = $v1 / ( 2 * $v2 );
    
    $disc->{ 'X' } = ( $n1 - $n2 + 2 * ( $pt2->[ 1 ] - $pt1->[ 1 ] ) * $disc->{ 'Y' } ) /
                        ( 2 * ( $pt1->[ 0 ] - $pt2->[ 0 ] ) );
    $disc->{ 'RAD' } = sqrt( 
                ( $disc->{ 'X' } - $pt1->[ 0 ] ) * ( $disc->{ 'X' } - $pt1->[ 0 ] ) + 
                ( $disc->{ 'Y' } - $pt1->[ 1 ] ) * ( $disc->{ 'Y' } - $pt1->[ 1 ] )
    );

    return;
}

############################################################
# Process a spiral arm

sub processSpiral {

    my $r = shift;
    my $start_point = shift;
    my $delta = shift;
    my $q_val = shift;

    my $recip_delta = cinv( $delta );
    my $mod_delta = cmod( $delta );
    my $mod_recip_delta = 1 / $mod_delta;

    # Spiral outwards
    my $q = $start_point;
    my $mod_q = cmod( $q );
    
    my $p_val = 0;
    while ( $mod_q < $IMAGE->{ 'MAX-PROCESSING-RAD' } * $IMAGE->{ 'SCALE' } ) {

        addDisc( $q->[ 0 ], $q->[ 1 ], $mod_q * $r, $p_val, $q_val );
        $q = cmul( $q, $delta);
        $mod_q *= $mod_delta;
        $p_val = ( $p_val + 1 ) % $IMAGE->{ 'P-VAL' };
    }

    # Spiral inwards
    $q = cmul( $start_point, $recip_delta);
    $mod_q = cmod( $q );

    $p_val = $IMAGE->{ 'P-VAL' } - 1;
    while ( $mod_q * $IMAGE->{ 'SCALE' } > $IMAGE->{ 'MIN-PROCESSING-RAD' } ) {

        addDisc( $q->[ 0 ], $q->[ 1 ], $mod_q * $r, $p_val, $q_val );
        $q = cmul( $q, $recip_delta );
        $mod_q *= $mod_recip_delta;
        $p_val = ( $p_val + $IMAGE->{ 'P-VAL' } - 1 ) % $IMAGE->{ 'P-VAL' };
    }

    return;
}

############################################################
# Write the image header

sub writeImageHeader {

    my $fh = shift;

    printf $fh "%%!PS\n%%%%BoundingBox: 0 0 1000 1000\n%%%%EndComments\n

        /imgw 1000 def
        /imgd 1000 def

        <<
            /PageSize [ imgw imgd ]
            /ImagingBBox null
        >> setpagedevice

        /mv { moveto } bind def
        /ln { lineto } bind def

        0 0 mv imgw 0 ln imgw imgd ln 0 imgd ln fill

        %f %f translate

        /Arial-Bold findfont 14 scalefont setfont

        1 setlinejoin
        1 1 0 setrgbcolor
    ",
        $IMAGE->{ 'X-OFF' },
        $IMAGE->{ 'Y-OFF' },
    ;
    
    if ( $IMAGE->{ 'FILLING-SCHEME' } eq 'sphere' ) {

        # Normalise the lamp vector
        my $d = sqrt(
            $IMAGE->{ 'SPHERE-DEF' }->{ 'LAMP_X' } ** 2 +
            $IMAGE->{ 'SPHERE-DEF' }->{ 'LAMP_Y' } ** 2 +
            $IMAGE->{ 'SPHERE-DEF' }->{ 'LAMP_Z' } ** 2
        );

        $IMAGE->{ 'SPHERE-DEF' }->{ 'LAMP_X' } /= $d;
        $IMAGE->{ 'SPHERE-DEF' }->{ 'LAMP_Y' } /= $d;
        $IMAGE->{ 'SPHERE-DEF' }->{ 'LAMP_Z' } /= $d;

        # Write out the renderer functions
        my $loadedRenderers = {};
        for my $renderer ( @{ $IMAGE->{ 'SPHERE-DEF' }->{ 'COLOR-DEFS' } } ) {

            next if $loadedRenderers->{ $renderer->{ 'func' } };
            writeRenderer( $fh, $renderer );
            $loadedRenderers->{ $renderer->{ 'func' } } = 1;
        }
    }

    return;
}

############################################################
# Return the smaller of the given values

sub min {

    my $v1 = shift;
    my $v2 = shift;

    return $v1 < $v2 ? $v1 : $v2;
}

############################################################
# Write postscript for the colored sphere

sub writeRenderer {

    my $fh = shift;
    my $renderer = shift;

    my $rgb = $renderer->{ 'rgb' };

    my $sphere = $IMAGE->{ 'SPHERE-DEF' };

    printf $fh "/%s {\n", $renderer->{ 'func' };
    printf $fh "    gsave\n";
    printf $fh "    /sz %d def\n", 2 * $sphere->{ 'RAD' };
    printf $fh "    0 1 360 {\n";
    printf $fh "        /a exch def\n";
    printf $fh "        .5 a cos 2 div add .5 a sin 2 div add a 0 gt { lineto } { moveto } ifelse\n";
    printf $fh "    } for\n";
    printf $fh "    clip\n";
    printf $fh "    sz sz 8 [sz 0 0 sz neg 0 sz]\n";
    printf $fh "    {<";
    
    for( my $y = -$sphere->{ 'RAD' }; $y < $sphere->{ 'RAD' }; $y++ ) {
    
        for( my $x = -$sphere->{ 'RAD' }; $x <$sphere->{ 'RAD' }; $x++ ) {
    
            my $d = $x * $x + $y * $y;
    
            if ( $d > $sphere->{ 'RAD' } * $sphere->{ 'RAD' } ) {
    
                printf $fh "000000";
            }
            else {
    
                my $z = sqrt( $sphere->{ 'RAD' } * $sphere->{ 'RAD' } - $d );
    
                my $v = (
                    $x * $sphere->{ 'LAMP_X' } +
                    $y * $sphere->{ 'LAMP_Y' } +
                    $z * $sphere->{ 'LAMP_Z' }
                ) / $sphere->{ 'RAD' };
    
                $v = 0 if $v < 0;
    
                if ( $v * 1000 > 998 ) {
    
                    printf $fh "FFFFFF";
                }
                elsif ( $v * 1000 > $sphere->{ 'SHEEN' } ) {
    
                    my $v1 = 1000 * $v - $sphere->{ 'SHEEN' };
                    my $v2 = 1000 - $sphere->{ 'SHEEN' };
                    my $v3 = ( $v1 * $v1 ) / ( $v2 * $v2 );
    
                    my $r = $rgb->[ 0 ] * $v + $v3 * ( 255 - $rgb->[ 0 ] );
                    my $g = $rgb->[ 1 ] * $v + $v3 * ( 255 - $rgb->[ 1 ] );
                    my $b = $rgb->[ 2 ] * $v + $v3 * ( 255 - $rgb->[ 2 ] );
    
                    printf $fh "%02x%02x%02x", 
                                min( $r, 255 ),
                                min( $g, 255 ),
                                min( $b, 255 ),
                    ;
                }
                else {
    
                    $v = $sphere->{ 'MIN-COLOR-PCNT' } / 100
                                    if ( $v * 100 < $sphere->{ 'MIN-COLOR-PCNT' } );
    
                    printf $fh "%02x%02x%02x", 
                                min( $rgb->[ 0 ] * $v, 255 ),
                                min( $rgb->[ 1 ] * $v, 255 ),
                                min( $rgb->[ 2 ] * $v, 255 ),
                    ;
                }
            }
        }
    
        printf $fh "    \n";
    }
    
    printf $fh ">}\n";
    printf $fh "    false 3 colorimage\n";
    printf $fh "    grestore\n";
    printf $fh "} def\n";
}

############################################################
# Write the image details

sub writeImageDetail {

    my $fh = shift;

    for( my $d = 0; $d < scalar @{ $IMAGE->{ 'DISCS' } }; $d++ ) {

        my $disc = $IMAGE->{ 'DISCS' }->[ $d ];

        mobiusTransformDisc( $disc ) if $IMAGE->{ 'MOBIUS_XFORM' };

        next unless ( $IMAGE->{ 'SCALE' } * $disc->{ 'RAD' } ) < $IMAGE->{ 'MAX-DRAWING-RAD' };
        next if ( $IMAGE->{ 'SCALE' } * $disc->{ 'RAD' } ) < 0.1;

        for ( $IMAGE->{ 'FILLING-SCHEME' } ) {

            /none/ && do { drawDisc( $fh, $disc ); last };
            /disc/ && do { drawDisc( $fh, $disc ); last };
            /sphere/ && do { drawSphere( $fh, $disc ); last };
        }
    }

    return;
}

############################################################
# Return the color to use for drawing/filling

sub getDrawColor {

    for ( $IMAGE->{ 'COLOR-SCHEME' } ) {

        /blue/      && return '0 0 1 setrgbcolor';
        /green/     && return '0 1 0 setrgbcolor';
        /red/       && return '1 0 0 setrgbcolor';
        /yellow/    && return '1 1 0 setrgbcolor';
    }

    return;
}

############################################################
# Check for the spectrum renderer

sub checkSpectrumRenderer {

    return if $IMAGE->{ 'COLOR-SCHEME' } ne 'spectrum';

    for( my $c = 0; $c < $IMAGE->{ 'P-VAL' }; $c++ ) {

        my $y = ( $c + 1 ) / ( $IMAGE->{ 'P-VAL' } + 1 );
        $y = ( $IMAGE->{ 'SPECTRUM-MUL' } * $y ) -
                            int( $IMAGE->{ 'SPECTRUM-MUL' } * $y );

        my $blk = int( $#SPECTRUM * $y );
        my $from = $SPECTRUM[ $blk ];
        my $to = $SPECTRUM[ $blk + 1 ];

        my $v = $y - $blk / $#SPECTRUM;

        push @{ $IMAGE->{ 'SPHERE-DEF' }->{ 'COLOR-DEFS' } }, {

            'func'  => "custom$c",
            'rgb'   => [
                    ( $from->[0] + ( $to->[0] - $from->[0] ) * $v * $#SPECTRUM ),
                    ( $from->[1] + ( $to->[1] - $from->[1] ) * $v * $#SPECTRUM ),
                    ( $from->[2] + ( $to->[2] - $from->[2] ) * $v * $#SPECTRUM ),
            ],
        }
    }

    return 'OK';
}

############################################################
# Check the user supplied sphere renderer array

sub checkSphereRenderer {

    my $param = $IMAGE->{ 'COLOR-SCHEME' };
    $param =~ s/^\s*\[//;
    $param =~ s/\]\s*$//;

    for my $col ( split /,/, $param ) {
    
        $col =~ s/^\s*//;
        $col =~ s/\s*$//;

        my $color_defs = $IMAGE->{ 'SPHERE-DEF' }->{ 'COLOR-DEFS' };

        for ( $col ) {

            /B/ && do { push @{ $color_defs }, $BLUE_SPHERE_DEF; last };
            /G/ && do { push @{ $color_defs }, $GREEN_SPHERE_DEF; last };
            /R/ && do { push @{ $color_defs }, $RED_SPHERE_DEF; last };
            /Y/ && do { push @{ $color_defs }, $YELLOW_SPHERE_DEF; last };
            /\{(.*)\}/ && do { 
                            
                            $col =~ s/[\{\}]//gx;
                            my @comps = split /;/, $_;
                            help( "Invalid custom color scheme entry : '{$col}'" ) 
                                                        unless ( scalar @comps == 3);
                            push @{ $color_defs }, {
                                'func'  => sprintf( "custom%d", scalar @{ $color_defs } ),
                                'rgb'   => [
                                    min( 255, $comps[ 0 ] ),
                                    min( 255, $comps[ 1 ] ),
                                    min( 255, $comps[ 2 ] ),
                                ],
                            };
                            last;
                        };

            help( "Invalid custom color scheme entry : '$col'" );
        }
    }

    help( "Invalid custom color scheme" )
                    unless scalar @{ $IMAGE->{ 'SPHERE-DEF' }->{ 'COLOR-DEFS' } };
}
    
############################################################
# Use the user supplied array to find a sphere renderer 

sub getSphereRenderer {

    my $disc = shift;

    my $v = scalar @{ $IMAGE->{ 'SPHERE-DEF' }->{ 'COLOR-DEFS' } };

    my $p = $disc->{ 'P' } % $v;

    return $IMAGE->{ 'SPHERE-DEF' }->{ 'COLOR-DEFS' }->[ $p ]->{ 'func' };
}

############################################################
# Draw the disc boundary

sub drawDisc {

    my $fh = shift;
    my $disc = shift;
    my $op = shift;

    printf $fh "gsave %s\n", getDrawColor();

    for( my $a = 0; $a <= 360; $a += 2 ) {

        printf $fh "%f %f %s ",
            $IMAGE->{ 'SCALE' } * ( $disc->{ 'X' } + ( $disc->{ 'RAD' } * cos( deg2rad( $a ) ) ) ),
            $IMAGE->{ 'SCALE' } * ( $disc->{ 'Y' } + ( $disc->{ 'RAD' } * sin( deg2rad( $a ) ) ) ),
            $a ? 'ln' : 'mv',
        ;
    }

    printf $fh "%s grestore\n", $IMAGE->{ 'FILLING-SCHEME' } eq 'none' ? 'stroke' : 'fill';
}

############################################################
# Draw a sphere on the disc position

sub drawSphere {

    my $fh = shift;
    my $disc = shift;

    printf $fh "gsave %f %f translate %f dup scale %s grestore\n",
                    $IMAGE->{ 'SCALE' } * ( $disc->{ 'X' } - ( $disc->{ 'RAD' } ) ),
                    $IMAGE->{ 'SCALE' } * ( $disc->{ 'Y' } - ( $disc->{ 'RAD' } ) ),
                    2 * $IMAGE->{ 'SCALE' } * $disc->{ 'RAD' },
                    getSphereRenderer( $disc ),
    ;
}

############################################################
# Draw the image

sub drawImage {

    open my $fh, '>', $PS_TEMP or do {
        printf "Could not open %s: $!\n", $PS_TEMP;
        exit -1;
    };

    writeImageHeader( $fh );
    writeImageDetail( $fh );

    close $fh;

    my $convert = "/usr/local/bin/convert";
    $convert = "/usr/bin/convert" unless ( -f $convert );
    $convert = "/Applications/ImageMagick/ImageMagick-6.6.5/bin/convert" unless ( -f $convert );

    unless ( -f $convert ) {

        printf "Could not locate convert at $convert\n";
    }
    else {

        unlink $IMAGE->{ 'OUTPUT-FILE' };
        my $cmd = sprintf "$convert '%s' '%s'", $PS_TEMP, $IMAGE->{ 'OUTPUT-FILE' };
        system( $cmd );
    }

    if ( $options{ 'K' } ) {
        printf "Kept $PS_TEMP\n";
    } else { 
        unlink $PS_TEMP;
    }

    return;
}

############################################################
# Square the given value

sub _sq {

    my $v = shift;

    return $v * $v;
}
    
############################################################
# Return the first value to the power of the second

sub _pw {

    my $v = shift;
    my $p = shift;

    return $v ** $p;
}
    
############################################################
# Return the square of the distance between z*e^(it) and z*e^(it)^(p/q)

sub _d {

    my $z = shift;
    my $t = shift;
    my $p = shift;
    my $q = shift;

    my $w = _pw( $z, $p / $q );
    my $s = ( $p * $t + 2 * pi ) / $q;

    return
          _sq( $z * cos( $t ) - $w * cos( $s ) )
        + _sq( $z * sin( $t ) - $w * sin( $s ) )
    ;
}

############################################################
# The partial derivative of _d with respect to z 

sub ddz_d {

    my $z = shift;
    my $t = shift;
    my $p = shift;
    my $q = shift;

    my $w = _pw( $z, $p / $q );
    my $s = ( $p * $t + 2 * pi ) / $q,
    my $ddz_w = ( $p / $q ) * _pw( $z, ( $p - $q ) / $q );

    return
        2 * ( $w * cos( $s ) - $z * cos( $t ) ) * ( $ddz_w * cos( $s ) - cos( $t ) )
      + 2 * ( $w * sin( $s ) - $z * sin( $t ) ) * ( $ddz_w * sin( $s ) - sin( $t ) )
    ;
}

############################################################
# The partial derivative of _d with respect to t

sub ddt_d {

    my $z = shift;
    my $t = shift;
    my $p = shift;
    my $q = shift;

    my $w = _pw( $z, $p / $q );
    my $s = ( $p * $t + 2 * pi ) / $q;
    my $dds_t = ( $p / $q );

    return 
        2 * ( $z * cos( $t ) - $w * cos( $s ) ) * ( -1 * $z * sin( $t ) + $w * sin( $s ) * $dds_t )
      + 2 * ( $z * sin( $t ) - $w * sin( $s ) ) * ( $z * cos( $t ) - $w * cos( $s ) * $dds_t )
    ;
}

############################################################
# The square of the sum of the origin-distance of z*e^(it) 
# and the origin-distance of z*e^(it)^(p/q)

sub _s {

    my $z = shift;
    my $t = shift;
    my $p = shift;
    my $q = shift;

    return _sq( $z + _pw( $z, $p / $q ) );
}

############################################################
# The partial derivative of _s with respect to z

sub ddz_s {

    my $z = shift;
    my $t = shift;
    my $p = shift;
    my $q = shift;

    my $w = _pw( $z, $p / $q );
    my $ddz_w = ( $p / $q ) * _pw( $z, ( $p - $q ) / $q );

    return 2 * ( $w + $z ) * ( $ddz_w + 1 );
}

############################################################
# The square of the radius-ratio implied by having touching
# circles centred at z*e^(it) and z*e^(it)^(p/q)

sub _r {

    my $z = shift;
    my $t = shift;
    my $p = shift;
    my $q = shift;

    return _d( $z, $t, $p, $q ) / _s( $z, $t, $p, $q );
}

############################################################
# The partial derivative of _r with respect to z

sub ddz_r {

    my $z = shift;
    my $t = shift;
    my $p = shift;
    my $q = shift;

    return (
            _s( $z, $t, $p, $q ) * ddz_d( $z, $t, $p, $q )
          - _d( $z, $t, $p, $q ) * ddz_s( $z, $t, $p, $q )
    ) / _sq( _s( $z, $t, $p, $q ) );
}

############################################################
# The partial derivative of _r with respect to t

sub ddt_r {

    my $z = shift;
    my $t = shift;
    my $p = shift;
    my $q = shift;

    return (
        ddt_d( $z, $t, $p, $q) * _s( $z, $t, $p, $q)
        # - _d( $z, $t, $p, $q ) * ddt_s( $z, $t, $p, $q )  omitted because ddt_s is constant at zero
    ) / _sq( _s( $z, $t, $p, $q ) );
}

############################################################
# Function to be zero when _r(z,t,0,1) = _r(z,t,p,q)

sub _f {

    my $z = shift;
    my $t = shift;
    my $p = shift;
    my $q = shift;

    return _r( $z, $t, 0, 1 ) - _r( $z, $t, $p, $q );
}

############################################################
# The partial derivative of _f with respect to z

sub ddz_f {

    my $z = shift;
    my $t = shift;
    my $p = shift;
    my $q = shift;

    return ddz_r( $z, $t, 0, 1) - ddz_r( $z, $t, $p, $q );
}

############################################################
# The partial derivative of _f with respect to t

sub ddt_f {

    my $z = shift;
    my $t = shift;
    my $p = shift;
    my $q = shift;

    return ddt_r( $z, $t, 0 , 1 ) - ddt_r( $z, $t, $p, $q );
}

############################################################
# Function to be zero when _r(z,t,0,1) = _r(_pw(z, p/q), (p*t + 2*pi)/q, 0,1)

sub _g {

    my $z = shift;
    my $t = shift;
    my $p = shift;
    my $q = shift;

    return _r( $z, $t, 0 , 1 ) - _r( _pw( $z, ( $p / $q ) ), ( $p * $t + 2 * pi ) / $q, 0, 1 );
}

############################################################
# The partial derivative of _g with respect to z

sub ddz_g {

    my $z = shift;
    my $t = shift;
    my $p = shift;
    my $q = shift;

    return ddz_r( $z, $t, 0 ,1 ) - ddz_r( _pw( $z, $p / $q), ( $p * $t + 2 * pi ) / $q, 0, 1 ) * ( $p / $q ) * _pw( $z, ( $p - $q ) / $q );
}

############################################################
# The partial derivative of _g with respect to t

sub ddt_g {

    my $z = shift;
    my $t = shift;
    my $p = shift;
    my $q = shift;

    return ddt_r( $z, $t, 0, 1 ) - ddt_r( _pw( $z, ( $p / $q ) ), ( $p * $t + 2 * pi ) / $q, 0, 1 ) * ( $p / $q );
}

############################################################
# Use 2d Newton-Raphson for the roots of _f and _g

sub find_root {

    my $z = shift;
    my $t = shift;
    my $p = shift;
    my $q = shift;

    for(;;) {
        
        my $v_f = _f( $z, $t, $p, $q );
        my $v_g = _g( $z, $t, $p, $q );

        return {
            'ok'    => 1, 
            'z'     => $z, 
            't'     => $t, 
            'r'     => sqrt( _r( $z, $t, 0 ,1 ) ),
        } if (
            -$EPSILON < $v_f && $v_f < $EPSILON && 
            -$EPSILON < $v_g && $v_g < $EPSILON
        );
                
        my $a = ddz_f( $z, $t, $p, $q );
        my $b = ddt_f( $z, $t, $p, $q );
        my $c = ddz_g( $z, $t, $p, $q ); 
        my $d = ddt_g( $z, $t, $p, $q );
        my $det = $a * $d - $b * $c;

        return { 'ok' => 0 } if ( -$EPSILON < $det && $det < $EPSILON );
                
        $z -= ( $d * $v_f - $b * $v_g ) / $det;
        $t -= ( $a * $v_g - $c * $v_f ) / $det;
                
        return { 'ok' => 0 } if ( $z < $EPSILON );
    }
}

############################################################
# Help doco

sub help {

    print "\n@_\n" if @_;

    printf "\n$0 " .
                "[-h] [-m] [-C <scheme>] [-I] [-F <scheme>] [-K <file>] " .
                "[-M <mul>][-O <file>] [-P <rad>] [-Q <rad>] [-R <rad>] " .
                "[-S <scale>] [-X <off>] [-Y <off>] <pval> <qval>

        where
            <pval>      mandatory first spiral value
            <qval>      mandatory second spiral value

            -h          this help message

            -m          apply a Mobuis transformation to the image discs

            -C <scheme> the color scheme to use (default 'white')
            -D <depth>  image depth (default 1000)
            -I 		    dump the image data
            -F <scheme> disc filling scheme (default 'none')
            -K          retain temp ps file ($PS_TEMP)
            -M <mul>    cycle though the spectrum scheme <mul> times (default 1)
            -O <file>   output file (default 'doyle.png')
            -P <rad>    max radius disc to draw (default 200)
            -R <rad>    max radius disc to process (default 100)
            -Q <rad>    min radius disc to process (default 1)
            -S <scale>  drawing scale (default 1)
            -W <width>  image width (default 1000)
            -X <off>    horizontal drawing offset (default width/2)
            -Y <off>    vertical drawing offset (default depth/2)

        the following filling schemes are available:

            none        draw only the disc boundaries
            discs       discs are filled
            spheres     use sphere images

        the following color schemes are available:

            blue, green, red, yellow (default)
            or
            spectrum        values chosen according to the disc P-val
            or
            custom [arr]    the disc P-val is used to index the array of 
                            color indicators

            indicators available are:
                'B'         blue
                'G'         green
                'Y'         yellow
                'R'         red
                {r; g; b}   custom rgb values (0<v<255)
    \n";

    exit;
}

__END__

./doyle.pl -S 2 -F sphere -C spectrum -O $imgdir/doyle-2-25.png 2 25
./doyle.pl -S 42 -F sphere -C spectrum -O $imgdir/doyle-2-25-m.png 2 25
./doyle.pl -S 82 -F sphere -C spectrum -P 80 -O $imgdir/doyle-5-29.png 5 29
./doyle.pl -S 82 -F sphere -C spectrum -m -P 80 -O $imgdir/doyle-5-29-m.png 5 29
./doyle.pl -S 70 -F sphere -C spectrum -C '[R,Y]' -m -O $imgdir/doyle-4-37-m.png -P 110 4 37
./doyle.pl -S 82 -m -F sphere -C spectrum -C '[R,Y]' -O $imgdir/doyle-24-37.png -P 110 24 37
./doyle.pl -S 2 -F sphere -C spectrum -C '[R,Y]' -O $imgdir/doyle-24-37-m.png -P 110 24 37
./doyle.pl -S 2 -F sphere -C spectrum -C '[R,Y]' -O $imgdir/doyle-24-37-m.png -P 110 24 37

