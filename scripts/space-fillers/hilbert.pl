#!/usr/bin/perl

use warnings;
use strict;

use Getopt::Std;
use Readonly;

# square->disc
# nx = x . sqrt(1 - y*y/2)
# ny = y . sqrt(1 - x*x/2)

Readonly    my  $tmp_ps  =>  'clp.ps';

############################################################
# The drawing algorithms

Readonly    my  $algorithms   =>  {
        'H' =>  {
            # hilbert rules
            'axiom' =>  'A',
            'A'     =>  '-BF+AFA+FB-',
            'B'     =>  '+AF-BFB-FA+',
        },
        'P' =>  {
            # peano rules
            'axiom' =>  'L',
            'L'     =>  'LFRFL-F-RFLFR+F+LFRFL',
            'R'     =>  'RFLFR+F+LFRFL-F-RFLFR',
        },
};

############################################################
# Process any command line options

my %options;
getopts( 'A:i:hko:p', \%options );

help() if $options{ 'h' };
my $iterations = $options{ 'i' } || 0;

processImage();

exit;

############################################################
# Process the image

sub processImage {

    my $algorithm = $options{ 'A' } || 'P';
    my $axiom = $algorithms->{ $algorithm }->{ 'axiom' };
    help( "Not a valid algorithm: '$algorithm'" ) unless $axiom;

    my $ruleset = $algorithms->{ $algorithm }->{ $axiom };

    for ( my $i = 0; $i < $iterations; $i++ ) {

        $ruleset = processRuleset( $algorithm, $ruleset );
    }

    my $vertices = extractVertices( $ruleset );

    drawSegments( $vertices );

    return
}

############################################################
# processRuleset 
#   Iterate the image segments

sub processRuleset {

    my $algorithm = shift;
    my $rules = shift;

    my $new_rules = "";

    for my $instruction ( split( //, $rules ) ) {
    
        $new_rules .= $algorithms->{ $algorithm }->{ $instruction } || $instruction;
    }

    return $new_rules;
}

############################################################
# extractVertices
#   Find the rotate/draw instructions in the ruleset

sub extractVertices {

    my $x = 0;
    my $y = 0;
    my $dir = 0;    # 0-3 corresponding to nwse compass points

    my $vertices = [
            [ $x, $y ],
    ];

    for my $instruction ( split( //, shift ) ) {

        $dir++ if ( $instruction eq '+' );
        $dir-- if ( $instruction eq '-' );
        
        if ( $instruction eq 'F' ) {
        
            $y++ if ( ( $dir % 4 ) == 0 );
            $x-- if ( ( $dir % 4 ) == 1 );
            $y-- if ( ( $dir % 4 ) == 2 );
            $x++ if ( ( $dir % 4 ) == 3 );

            push @{ $vertices }, [ $x, $y ];
        }
    }

    return $vertices
}

############################################################
# findVertexBounds 
#   Find the boundingbox for the vetex list

sub findVertexBounds {

    my $vertices = shift;

    my $lx = $vertices->[ 0 ]->[ 0 ];
    my $ly = $vertices->[ 0 ]->[ 1 ];

    my $ux = $vertices->[ 0 ]->[ 0 ];
    my $uy = $vertices->[ 0 ]->[ 1 ];

    for my $vertex ( @{ $vertices } ) {

        $lx = $vertex->[ 0 ] if $vertex->[ 0 ] < $lx;
        $ly = $vertex->[ 1 ] if $vertex->[ 1 ] < $ly;
        $ux = $vertex->[ 0 ] if $vertex->[ 0 ] > $ux;
        $uy = $vertex->[ 1 ] if $vertex->[ 1 ] > $uy;
    }

    return ( $lx, $ly, $ux, $uy );
}

############################################################
# drawSegments 
#   Render the image segments

sub drawSegments {

    my $vertices = shift;

    open my $fh, '>', $tmp_ps || help( "Could not open " . $tmp_ps );

    my $image_wid = 1000;
    my $image_dep = 1000;

    my $image_usage = 0.9;

    my @bounds = findVertexBounds( $vertices );

    my $xsc = ( $image_wid * $image_usage ) / ( $bounds[ 2 ] - $bounds[ 0 ] );
    my $ysc = ( $image_wid * $image_usage ) / ( $bounds[ 3 ] - $bounds[ 1 ] );
    my $sc = $xsc < $ysc ? $xsc : $ysc;

    my $xoff = ( $image_wid - ( ( $bounds[ 2 ] - $bounds[ 0 ] ) * $sc ) ) / 2.0;
    my $yoff = ( $image_dep - ( ( $bounds[ 3 ] - $bounds[ 1 ] ) * $sc ) ) / 2.0;

    printf $fh "%%!PS\n%%BoundingBox: 0 0 %f %f\n%%EndComments\n

        <<
            /PageSize [ %f %f ]
            /ImagingBBox null
        >> setpagedevice

        /mv { moveto } bind def
        /ln { lineto } bind def

        /lx %f def
        /ly %f def
        /ux %f def
        /uy %f def

        /sc %f def
        /xoff %f def
        /yoff %f def

        /trns {

            /proj exch def
            /y exch def
            /x exch def

            proj {

                /nlx lx sc mul xoff add def
                /nly ly sc mul yoff add def
                /nux ux sc mul xoff add def
                /nuy uy sc mul yoff add def

                /nx x sc mul xoff add nux nlx add 2 div sub nux nlx sub 2 div div def
                /ny y sc mul yoff add nuy nly add 2 div sub nuy nly sub 2 div div def
            
                nx 1 ny dup mul 2 div sub sqrt mul nux nlx sub 2 div mul nux nlx add 2 div add
                ny 1 nx dup mul 2 div sub sqrt mul nuy nly sub 2 div mul nuy nly add 2 div add
            } {

                x sc mul xoff add
                y sc mul yoff add
            } ifelse

        } def

        0 0 mv %f 0 ln %f %f ln 0 %f ln fill

        1 setlinejoin
        1 1 0 setrgbcolor
    ",
        $image_wid, $image_dep,
        $image_wid, $image_dep,
        @bounds,
        $sc, $xoff, $yoff,
        $image_wid, $image_wid,
        $image_dep, $image_dep,
    ;

    my $op = 'mv';

    for my $vertex ( @{ $vertices } ) {

        printf $fh "%f %f %s trns %s\n", @{ $vertex }, $options{ 'p' } ? 'true' : 'false', $op;
        $op = 'ln';
    }

    printf $fh "stroke\n";

    close $fh;

    my $dest = $options{ 'o' } || 'clp.png';
    system( "convert clp.ps $dest");

    unlink 'clp.ps' unless $options{ 'k' };

    return;
}

############################################################
# help
#   Some documentation

sub help {

    print "\n@_\n" if @_;

    printf "\n$0 " .
        "[-A algorithm] [-h] [-i <iters>] [-o <dest>] [-p]

    where
        -A          algorithm to use - one of 'H' Hilbert of 'P' Peano (default)
        -h          this help message
        -i <iters>  the number of iterations to apply
        -p          project onto a sphere
        -o <dest>   final (png) file for the image (default: clp.png)
    \n";

    exit 0;
}

__DATA__


