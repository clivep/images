#!/usr/bin/perl

use warnings;
use strict;

use Math::Trig;
use Getopt::Std;

my $IFS = {

    # f | x |   | 0 1 || x |   | 4 | and 6 is the probability
    #   |   | = |     ||   | + |   | 
    #   | y |   | 2 3 || y |   | 5 ]

    "spiral-dust" =>    {
        'OFFSET'        => [ 400, 480 ],
        'ROTATE'        => 0,
        'SCALE'         => 360,
        'XFORMS'        => [
            [ .95*dcos(20), -.95*dsin(20), .95*dsin(20), .95*dcos(20), 0.0, 0.0, 0.960 ],
            [          0.4,             0,            0,          0.4, 1.0, 0.0, 0.040 ],
        ],
    },

    "snowflake-spiral" =>    {
        'OFFSET'        => [ 550, 150 ],
        'ROTATE'        => 90,
        'SCALE'         => 600,
        'XFORMS'        => [
            [0.250000,   0.000000,   0.000000,   0.250000,   0.000000,   0.400000,   0.1 ],
            [0.822978,  -0.475000,   0.474955,   0.822724,   0.301140,  -0.173839,   0.9 ],
        ],
    },

    #[0.250000,   0.000000,   0.000000,   0.250000,   0.000000,   0.500000,   0.073459 ],
    #[0.822978,  -0.475000,   0.474955,   0.822724,   0.301140,  -0.173839,   0.926541 ],
};

my $options = {};
getopts( 'hm', $options );

help() if $options->{ 'h' };
help("Missing ifs name" ) if $#ARGV < 0;
help("Invalid ifs name" ) unless $IFS->{ $ARGV[ 0 ] };

my $MAX_POINTS = 100000;
my $TEMP = 'spiral.tmp';
my $DEST = 'spiral.png';
 
my $x = 0;
my $y = 0;

my $ifs = $IFS->{ 'spiral-dust' };

open my $FH, '>', $TEMP or die "Could not open $TEMP - $!\n";

printHeader( $FH, $ifs  );

for ( my $p = 0; $p < $MAX_POINTS; $p++ ) {

    my $rnd = ( rand 100 ) / 100.0;

    my $prob_total = 0.0;
    for my $xform ( @{ $ifs->{ 'XFORMS' } } ) {

        $prob_total += $xform->[ 6 ];
        if ( $rnd < $prob_total ) {

            my $nx = ( $xform->[ 0 ] * $x ) + ( $xform->[ 1 ] * $y ) + $xform->[ 4 ];
            my $ny = ( $xform->[ 2 ] * $x ) + ( $xform->[ 3 ] * $y ) + $xform->[ 5 ];

            $x = $nx;
            $y = $ny;

            print $FH "$rnd $x $y pt\n";
            last;
        }
    }
}

system( "/usr/bin/convert $TEMP $DEST" );
unlink $TEMP;

exit;

######################################################################
# Simple trig wrappers

sub dsin { return sin( deg2rad( shift ) ) }
sub dcos { return cos( deg2rad( shift ) ) }

######################################################################
# Write the image header to the temp output file

sub printHeader {

    my $hndl = shift;
    my $ifs = shift;

    print $hndl "%!PS\n";
    print $hndl "%%BoundingBox: 0 0 1000 1000\n";
    print $hndl "%%EndComments\n";

    print $hndl "
        0 0 moveto
        1000 0 rlineto
        0 1000 rlineto
        -1000 0 rlineto
        fill

        /Arial findfont 10 scalefont setfont
        1 setgray
    ";

    my $y  = 40;
    for my $xform ( @{ $ifs->{ 'XFORMS' } } ) {

        my $x = 50;

        for my $val ( @{ $xform } ) {
        
            printf $hndl "
                $x $y moveto 
                (%0.2f) dup stringwidth pop neg 0 rmoveto 
                show
            ", $val;
            $x += 30;
        }
        $y += 14;
    }

    my $color_func = $options-> { 'm' } ?
        "1 setgray"
        :
        "/rnd rnd 1000 mul dup cvi sub def
         rnd dup dup mul mul
         rnd 1000 mul dup cvi sub dup dup mul mul
         1 setrgbcolor
        "
    ;

    printf $hndl "
        %f %f translate
        %f rotate
        /sc %f def

        /pt {

            /y exch sc mul def
            /x exch sc mul def
            /rnd exch def

            $color_func

            x y moveto
            -0.5 -0.5 rmoveto
            1 0 rlineto
            0 1 rlineto
            -1 0 rlineto
            fill

        } def
    ",
    @{ $ifs->{ 'OFFSET' } },
    $ifs->{ 'ROTATE' },
    $ifs->{ 'SCALE' },
    ;
}

######################################################################
# Usage info

sub help {

    printf "@_\n" if @_;

    printf "
    Usage: $0 [-h] [-m] <ifs name>
 
      where 
        -h  this help message
        -m  draw in mono (default randomised blueish colors)
    
      and the <ifs name> is one of:
    \n";

    for my $name ( keys %{ $IFS } ) {
        printf "        $name\n";
    };

    printf "\n";

    exit;
}
