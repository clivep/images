#!/bin/sh

src=cube.ps

rot() {
    ang="000$1"
    ang=`echo $ang | sed -e's/.*\(...\)$/\1/'`

    perl -i -pe "s/\/rx [0-9]+ def/\/rx $ang def/" $src
    perl -i -pe "s/\/ry [0-9]+ def/\/ry $ang def/" $src
    perl -i -pe "s/\/rz [0-9]+ def/\/rz $ang def/" $src
    convert -resize 300x $src images/tmp-$ang.png
}

ang=0
while [ $ang -lt 360 ];
do
    rot $ang
    ang=`expr $ang \+ 2`
done

convert -delay 20 -loop 0 images/tmp*.png images/animated.gif
#rm images/tmp*.png
