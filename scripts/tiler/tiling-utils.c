#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>

#include "tiling-utils.h"

static _logging_details *logging_details;

/*******************************************************************************
 *
 * tiling-utils.c
 *
 *  Utility functions for generating tilings
 *
 ******************************************************************************/

/*************************************************
 * Setup the logging environment
 ************************************************/

void initialiseLogging(
    char                *logfile,
    _logging_level      logging_level
) {

    if (
        ( logging_details == NULL ) &&
        ( ( logging_details = ALLOC( _logging_details ) ) == NULL )
    )
        logFatal( "Failed to calloc() logging_details" );

    logging_details->logging_level = logging_level;

    logging_details->log_fp = NULL;
}

/*************************************************
 * Cleanup logging
 ************************************************/

void cleanupLogging() {

    if ( logging_details == NULL )
        return;

    if ( logging_details->log_fp != NULL )
        fclose( logging_details->log_fp );

    free( logging_details );

    logging_details = NULL;
}

/*************************************************
 * Get the logging level
 ************************************************/

_logging_level getLoggingLevel() {

    return logging_details->logging_level;
}


/*************************************************
 * Set the logging level
 ************************************************/

void setLoggingLevel(
    _logging_level      logging_level
) {

    logging_details->logging_level = logging_level;
}

/*************************************************
 * Setup a log file
 ************************************************/

void setLogFile(
    char    *log_file
) {
    if ( ( logging_details->log_fp = fopen( log_file, "w" ) ) == NULL )
        logFatal( "Unable to open the log file" );

}

/*************************************************
 * Simple logger
 ************************************************/

void _log(
    char    *format,
    va_list ap,
    char    *eol
) {

    if ( logging_details->logging_level == LOGGING_LEVEL_QUIET )
        return;

    if ( logging_details->log_fp != NULL ) {
        vfprintf( logging_details->log_fp, format, ap );
        fprintf( logging_details->log_fp, "%s", eol );
        fflush( logging_details->log_fp );
    }
    else {
        vprintf( format, ap );
        printf( "%s", eol );
        fflush( stdout );
    }

    va_end( ap );
}

/*************************************************
 * Log message
 ************************************************/

void logMsg(
    char *format,
    ...
) {
    va_list ap;

    va_start( ap, format );
    _log( format, ap, "\n" );
}

/*************************************************
 * Log message with no eol
 ************************************************/

void logTxt(
    char *format,
    ...
) {
    va_list ap;

    va_start( ap, format );
    _log( format, ap, "" );
}

/*************************************************
 * Log and exit
 ************************************************/

void logFatal(
    char *format,
    ...
) {
    va_list ap;

    va_start( ap, format );
    _log( format, ap, "\n" );

    exit( -1 );
}

/*************************************************
 * Remove an item from a list
 ************************************************/

void releaseListItem(
    _list_item  *item,
    _list       *list
) {
    
    if ( item == list->first )
        list->first = item->next;

    if ( item == list->last )
        list->last = item->prev;

    if ( item->prev != NULL ) 
        item->prev->next = item->next;
    
    if ( item->next != NULL ) 
        item->next->prev = item->prev;

    if ( list->free_func != NULL )
        list->free_func( item->data );

    list->count--;

    free( item );
}

/*************************************************
 * Initialise a list
 ************************************************/

_list *initialiseList( void (*free_func)(void *) ) {

    _list *list;

    if ( ( list = ALLOC( _list ) ) == NULL )
        logFatal( "Failed to calloc list " );

    list->free_func = free_func;

    return list;
}

/*************************************************
 * Release a list
 ************************************************/

void releaseList(
    _list *list
) {

    emptyList( list );
    free( list );
}

/*************************************************
 * Empty a list
 ************************************************/

void emptyList(
    _list *list
) {
    _list_item      *item;

    while( ( item = list->last ) != NULL )
        releaseListItem( item, list );
}

/*************************************************
 * Add data to a list
 ************************************************/

void addToList(
    void    *data,
    _list   *list
) {
    
    _list_item *item;

    if ( ( item = ALLOC( _list_item ) ) == NULL )
        logFatal( "Could not calloc() a list item" );

    item->next = NULL;
    item->data = data;

    if ( list->count > 0 ) {
        
        item->prev = list->last;
        list->last->next = item;
        list->last = item;
    }
    else {

        list->first = item;    
        list->last = item;    
    }

    list->count++;
}

/*************************************************
 * Return data from a list by its position
 ************************************************/

void *getFromListByPos(
    short   pos,
    _list   *list
) {

    _list_item  *item;

    if ( pos < 0 )
        return NULL;

    if ( pos < list->count ) {

        for( item = list->first; item != NULL; item = item->next ) {
            if ( pos == 0 )
                return item->data;
            pos--;
        }
    }

    return NULL;
}

/*************************************************
 * Return a data from a list by its data addr
 ************************************************/

void *getFromListByData(
    void    *addr,
    _list   *list
) {

    _list_item  *item;

    for( item = list->first; item != NULL; item = item->next ) {
        if ( item->data == addr )
            return item->data;
    }

    return NULL;
}

/*************************************************
 * Find list item position of the given string
 ************************************************/

short findStringInList(
    char    *string,
    _list   *list
) {

    short   pos;
    char    *list_string;

    if ( *string == '\0' )
        return -1;

    for( pos = 0; pos < list->count; pos++ ) {

        list_string = getFromListByPos( pos, list );

        if ( strcmp( list_string, string ) == 0 )
            return pos;
    }

    return -1;
}

