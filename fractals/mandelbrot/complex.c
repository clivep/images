#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "complex.h"
#include "fractal.h"

/*******************************************************************************
 *
 * complex.c
 *
 *  Utility functions to process complex numbers
 *
 ******************************************************************************/

/*******************************************************************************
 * Set the coordinates in a complex number
 ******************************************************************************/
void setc(
    long double x,
    long double y,
    _complex    *result
) {

    result->x = x;
    result->y = y;
}

/*******************************************************************************
 * Add 2 complex numbers and return a new result
 ******************************************************************************/

void addc(
    _complex    c1,
    _complex    c2,
    _complex    *result
) {
    
    result->x = c1.x + c2.x;
    result->y = c1.y + c2.y;
}

/*******************************************************************************
 * Subtract 2 complex numbers and return a new result
 ******************************************************************************/

void subc(
    _complex    c1,
    _complex    c2,
    _complex    *result
) {
    
    result->x = c1.x - c2.x;
    result->y = c1.y - c2.y;
}

/*******************************************************************************
 * Divide 2 complex numbers and return a new result
 ******************************************************************************/

void divc(
    _complex    c1,
    _complex    c2,
    _complex    *result
) {
    
    long double m = ( c2.x * c2.x ) + ( c2.y * c2.y );

    result->x = ( ( c1.x * c2.x ) + (c1.y * c2.y ) ) / m;
    result->y = ( ( c1.y * c2.x ) - (c1.x * c2.y ) ) / m;
}

/*******************************************************************************
 * Invert a complex number and return a new result
 ******************************************************************************/

void invc(
    _complex    c,
    _complex    *result
) {

    _complex tmp;
    setc( 1.0, 0.0, &tmp );

    divc( tmp, c, result );
}

/*******************************************************************************
 * Multiply 2 complex numbers and return a new result
 ******************************************************************************/

void mulc(
    _complex    c1,
    _complex    c2,
    _complex    *result
) {
    
    result->x = ( c1.x * c2.x ) - (c1.y * c2.y );
    result->y = ( c1.x * c2.y ) + (c1.y * c2.x );
}

/*******************************************************************************
 * Raise a complex numbers to a power and return a new result
 ******************************************************************************/

void powc(
    _complex    c,
    int         p,
    _complex    *result
) {
    
    _complex tmp;   // work with a local incase c and *result are the same
    long double v;

    setc( c.x, c.y, &tmp );

    while ( p-- > 1 ) {

        v = tmp.x;
        tmp.x = ( c.x * tmp.x ) - (c.y * tmp.y );
        tmp.y = ( c.x * tmp.y ) + (c.y * v );
    }

    setc( tmp.x, tmp.y, result );
}

/*******************************************************************************
 * Return the modulus of a given complex number
 ******************************************************************************/

long double modc( 
    _complex    c
) {

    return (long double )sqrt( ( c.x * c.x ) + ( c.y * c.y ) );
}
