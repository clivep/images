#!/usr/bin/perl

use warnings;
use strict;
use Math::Trig;

my $max_depth = 3;

my $generator = [
    [ 0, 0, 0 ],
    [ 10, 0, 0 ],
    [ 10, 0, 10 ],
];

my $initiator = [
    [ 0, 0, 50 ],
    [ 0, 0, 0 ],
];

if ( $ARGV[0] ) {

    $initiator = [
        [ 0, 0, 50 ],
        [ 25, 0, 25 ],
    ];
    $max_depth = 0;
}

normaliseGenerator();

my $xxx = 0;
processLine( 0, $initiator->[ 0 ],  $initiator->[ 1 ] ); 
printf "stroke\n";
exit;

##################################################
# Process the given line segment

sub processLine {

    my $depth = shift;
    my $start = shift;
    my $end = shift;

    return if ( $depth > $max_depth );
#printf "depth $depth\n";
#printf "from %f %f %f\n", $start->[0], $start->[1], $start->[2];
#printf "to   %f %f %f\n", $end->[0], $end->[1], $end->[2];
    my $dir = [
    
        $end->[ 0 ] - $start->[ 0 ],
        $end->[ 1 ] - $start->[ 1 ],
        $end->[ 2 ] - $start->[ 2 ],
    ];

#printf "1. dir  %f %f %f\n", $dir->[0], $dir->[1], $dir->[2];
    my $len = mod( $dir );

    my $copy = copyGenerator();

    my $spine_ang = abs( ang( [ 0, 0, 1 ], $dir ) );
#printf "2. dir  %lf %lf %lf\n", $dir->[0], $dir->[1], $dir->[2];
#printf "1. z ang: %f\n", rad2deg($spine_ang);
    map { yAxisRotate( $spine_ang, $_ ) } ( @{$copy} );
#printf "1. -------------------\n"; printGenerator( $copy );

    my $xaxis_ang = ang( [ 1, 0, 0 ], [ $dir->[ 0 ], $dir->[ 1 ], 0 ] );
    $xaxis_ang *= -1 if ( $dir->[ 1 ] < 0 );
#printf "x ang: %f\n", rad2deg($xaxis_ang);
    map { zAxisRotate( $xaxis_ang, $_ ) } ( @{$copy} );
#printf "2. -------------------\n"; printGenerator( $copy );
#printf "----------------------\n";

    my $prev_vr;

    for my $vr ( @{$copy} ) {
        $vr->[ 0 ] = ( $vr->[ 0 ] * $len ) + $start->[ 0 ];
        $vr->[ 1 ] = ( $vr->[ 1 ] * $len ) + $start->[ 1 ];
        $vr->[ 2 ] = ( $vr->[ 2 ] * $len ) + $start->[ 2 ];

#printf "$depth %04d ", $xxx++;
        printVr( $vr ) if ( $depth == $max_depth );

        processLine( $depth + 1, $prev_vr, $vr ) if ( $prev_vr );
        $prev_vr = $vr;
    }
}

##################################################
# Normalise the generator:

sub normaliseGenerator {

    my @origin = @{ $generator->[ 0 ] };

    for my $vr ( @{$generator} ) {

        $vr->[ 0 ] -= $origin[ 0 ];
        $vr->[ 1 ] -= $origin[ 1 ];
        $vr->[ 2 ] -= $origin[ 2 ];
    }

    my $spine = $generator->[ -1 ];

    my $xaxis_ang = ang( [ 1, 0, 0 ], [ $spine->[ 0 ], $spine->[ 1 ], 0 ] );
    map { zAxisRotate( $xaxis_ang, $_ ) } ( @{$generator} );

    my $spine_ang = ang( [ 0, 0, 1 ], $spine );
    map { yAxisRotate( -1 * $spine_ang, $_ ) } ( @{$generator} );

    my $len = mod( $spine );
    for my $vr ( @{$generator} ) {

        $vr->[ 0 ] /= $len,
        $vr->[ 1 ] /= $len,
        $vr->[ 2 ] /= $len,
    }
}

##################################################
# Rotate the given vector about the y axis

sub yAxisRotate {

    my $ang = shift;
    my $vr = shift;

    my $nx = $vr->[ 0 ] * cos( $ang ) + $vr->[ 2 ] * sin( $ang );
    my $ny = $vr->[ 1 ];
    my $nz = $vr->[ 2 ] * cos( $ang ) - $vr->[ 0 ] * sin( $ang );

    $vr->[ 0 ] = ( int( 0.5 + $nx * 10000 ) ) / 10000.0;
    $vr->[ 1 ] = ( int( 0.5 + $ny * 10000 ) ) / 10000.0;
    $vr->[ 2 ] = ( int( 0.5 + $nz * 10000 ) ) / 10000.0;
}

##################################################
# Rotate the given vector about the z axis

sub zAxisRotate {

    my $ang = shift;
    my $vr = shift;

    my $nx = $vr->[ 0 ] * cos( $ang ) + $vr->[ 1 ] * sin( $ang );
    my $ny = $vr->[ 1 ] * cos( $ang ) - $vr->[ 0 ] * sin( $ang );
    my $nz = $vr->[ 2 ];

    $vr->[ 0 ] = ( int( 0.5 + $nx * 10000 ) ) / 10000.0;
    $vr->[ 1 ] = ( int( 0.5 + $ny * 10000 ) ) / 10000.0;
    $vr->[ 2 ] = ( int( 0.5 + $nz * 10000 ) ) / 10000.0;
}

##################################################
# Return the angle between the given vectors

sub ang {

    my $vr1 = shift;
    my $vr2 = shift;

    my $v = mod( $vr1 ) * mod( $vr2 );

    return $v ? acos( dot( $vr1, $vr2 ) / $v ) : 0.0;
}

##################################################
# Return the dot product of the given vectors

sub dot {

    my $vr1 = shift;
    my $vr2 = shift;

    return (
        ( $vr1->[ 0 ] * $vr2->[ 0 ] ) +
        ( $vr1->[ 1 ] * $vr2->[ 1 ] ) +
        ( $vr1->[ 2 ] * $vr2->[ 2 ] )
    );
}
##################################################
# Return the size of the given vector

sub mod {

    my $vr = shift;

    return sqrt(

        ( $vr->[ 0 ] * $vr->[ 0 ] ) +
        ( $vr->[ 1 ] * $vr->[ 1 ] ) +
        ( $vr->[ 2 ] * $vr->[ 2 ] ) 
    );
}

##################################################
# Copy the generator vertices

sub copyGenerator {

    my @copy;

    for my $vr ( @{$generator} ) {

        my @vr_copy = @{ $vr };
        push @copy, \@vr_copy;
    }

    return \@copy;
}

##################################################
# Display the generator vertices

sub printGenerator {

    my $vr = shift;

    for my $point ( @{$vr} ) {

        printf "   ";
        printVr( $point );
    }
}

##################################################
# Display the generator vertices

sub printVr {

    my $vr = shift;

    printf "%f %f %f doit\n", $vr->[ 0 ], $vr->[ 1 ], $vr->[ 2 ];
}
