#include <stdio.h>
#include <stdlib.h>

#include "tiling-polygons.h"
#include "template.h"

/*******************************************************************************
 *
 * template.c
 *
 *  Functions for generating tilings
 *
 ******************************************************************************/

#define DEFAULT_PDF_PATH    "template.pdf"
#define RENDER_SCRIPT       "template-draw.pl"

static _list   *templates;

/*************************************************
 * Initialise the plugin
 ************************************************/

void initialisePlugin() {
    
    initialiseLogging( NULL, LOGGING_LEVEL_DEBUG, INTERACTIVE_LEVEL_OFF );

    templates = initialiseList( releasePolygon );

    addToList( initialise????Template(), templates );
}

/*************************************************
 * Return the number of edges for the type
 ************************************************/

short polygonTypeEdges(
    _template_type type 
) {
    switch( type ) {

        case TEMPLATE_TYPE_????:
            return ?;
    }

    return -1;
}

/*************************************************
 * Initialise the ???? template polygon
 ************************************************/

_polygon *initialise????Template() {

    _polygon    *template;

    template = newPolygon( TEMPLATE_TYPE_??? );

    template->sides[ 0 ] = newSide( SIDE_TYPE_???, template );
    ...

    template->angles[ 0 ] = ??
    ...

    template->vertices[ 0 ] = newVertex( 0.0, 0.0, template->angles[ 0 ] );
    ...

    offsetPolygon( template, 0, 0 );

    logMsg( "??? template @ %lx", (long)template );

    return template;
}

/*************************************************
 * Return the char id for the polygon type
 ************************************************/

char polygonTypeChar(
    _polygon *polygon
) {
    switch( polygon->type ) {

        case TEMPLATE_TYPE_????:
            return '?';
    }

    return '?';
}

/*************************************************
 * Return a description of the given side type
 ************************************************/

char *getSideTypeDescription(
    _side_type   pos
) {

    switch( pos ) {

        case SIDE_TYPE_??:
            return "??";

        default:
            break;
    }

    return "?";
}

/*************************************************
 * Test if the sides are allowed to join
 ************************************************/

short checkForValidSides(
    _polygon    *polygon1,      // the existing polygon
    short       edge1,
    _polygon    *polygon2,      // the candidate (template) polygon
    short       edge2
) {
    _side_type  side_type1;
    _side_type  side_type2;

    // Check for a valid combination of sides
    side_type1 = polygon1->sides[ edge1 ]->type;
    side_type2 = polygon2->sides[ edge2 ]->type;

    logMsg( "Checking sides %lx/%d with %lx/%d",
                                        (long)polygon1, edge1,
                                        (long)polygon2, edge2 
    );

    if ( ?? )
        return OK;

    logMsg( "rejected - bad side combination %lx: (%c/%d) and %lx: (%c/%d)", 
                                        (long)polygon1, polygonTypeChar( polygon1 ), edge1,
                                        (long)polygon2, polygonTypeChar( polygon2 ), edge2
    );

    return NOK;
}

/*************************************************
 * Return a template by its list position
 ************************************************/

_polygon *getTemplateByPos(
    short pos
) {
    return getFromListByPos( pos, templates );
}

/*************************************************
 * Return the number of template polygons
 ************************************************/

short getTemplateCount() {
    return templates->count;
}

/*************************************************
 * Render the image to the given file
 ************************************************/

void drawTiling(
    char    *json_path,
    double  exclusion_radius,
    char    *pdf_path
) {
    char    buf[100];

    if ( pdf_path == NULL )
        pdf_path = DEFAULT_PDF_PATH;

    sprintf( buf, "%s -x %f %s %s", RENDER_SCRIPT,
                        exclusion_radius, json_path, pdf_path );

    system( buf );
}

/*************************************************
 * Return a template from the list by type
 ************************************************/

_polygon *getTemplateByType(
    _template_type   type
) {

    _polygon    *template;
    _list_item  *item;

    for( item = templates->first; item != NULL; item = item->next ) {

        template = item->data;
        if ( template->type == type )
            return template;
    }

    return NULL;
}

/*************************************************
 * Return the number of side types available
 ************************************************/

int getSideTypeCount() {

    return SIDE_TYPE_COUNT;
}

/*************************************************
 * Find the pos of the type in the template list
 ************************************************/

short findTemplateTypePos(
    _template_type  type
) {
    _polygon    *template;
    short       pos;

    for( pos = 0; pos < templates->count; pos++ ) {

        template = getFromListByPos( pos, templates );
        if ( template->type == type )
            return pos;
    }

    return -1;
}
