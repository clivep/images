/*******************************************************************************
 *
 * tiling-polygons.h
 *
 *  Definitions for managing polygons
 *
 ******************************************************************************/

#ifndef _TILING_POLYGONS_H

#include <stdio.h>

#include "tiling-geometry.h"
#include "tiling-utils.h"

#define NEXT_VTX(n,ply) ( ( (n) + 1 ) % polygonEdges(ply) )
#define PREV_VTX(n,ply) ( ( (n) + polygonEdges(ply) - 1 ) % polygonEdges(ply) )

/*************************************************
 * Function prototypes
 ************************************************/
_polygon_vertex *newVertex( double x, double y, double angle_used );
_polygon_side *newSide( int type, _polygon *owner );
_polygon *newPolygon( int type );
void releasePolygon( void *polygon_ptr );
void offsetPolygon( _polygon *polygon, double x_offset, double y_offset ); 
_polygon_side *findCommonSide( _polygon *polygon,
                                    short side, _polygon **common_polygon );
short polygonEdges ( _polygon *polygon );
short findVertexNumber( _polygon *polygon, _polygon_vertex *vertex );
short findPolygonTypeByChar( char type );

#define _TILING_POLYGONS_H
#endif
