#include <string.h>
#include <stdlib.h>
#include <dlfcn.h>

#include "tiling-utils.h"
#include "tiling-plugin.h"

static void *plugin_hndl = NULL;

/*******************************************************************************
 *
 * tiling-plugin.c
 *
 *  Functions to load the plugin resources
 *
 ******************************************************************************/

#define LOAD_RESOURCE(res)  \
    _ ## res = dlsym( plugin_hndl, STR(res) ); \
    if ( ( error = dlerror() ) != NULL ) \
        logFatal( "Failed to load %s()", STR(res) )

/*************************************************
 * Plugin functions and wrappers
 ************************************************/

void (*_drawTiling)( char *json_path, double exclusion_radius, char *pdf_path ); 
void drawTiling( char *json_path, double exclusion_radius, char *pdf_path ) {
    (*_drawTiling)( json_path, exclusion_radius, pdf_path);
}

_polygon *(*_getTemplateByType)( int type );
_polygon *getTemplateByType( int type ) {
    return (*_getTemplateByType)( type );
}

short (*_polygonTypeEdges)( int type );
short polygonTypeEdges( int type ) {
    return (*_polygonTypeEdges)( type );
}

short (*_getTemplateCount)();
short getTemplateCount() {
    return (*_getTemplateCount)();
}

_polygon *(*_getTemplateByPos)( short pos );
_polygon *getTemplateByPos( short pos ) {
    return (*_getTemplateByPos)( pos );
}

short (*_findTemplateTypePos)( int type );
short findTemplateTypePos( int type ) {
    return _findTemplateTypePos( type );
}

char *(*_getSideTypeDescription)( int pos );
char *getSideTypeDescription( int pos ) {
    return (*_getSideTypeDescription)( pos );
}

short (*_checkForValidSides)( _polygon *polygon1, short edge1,
                                _polygon *polygon2, short edge2 );
short checkForValidSides( _polygon *polygon1, short edge1,
                                _polygon *polygon2, short edge2 ) {

    return (*_checkForValidSides)( polygon1, edge1, polygon2, edge2 );
}

char (*_polygonTypeChar)( _polygon *polygon );
char polygonTypeChar( _polygon *polygon ) {
    return (*_polygonTypeChar)( polygon );
}

int (*_getSideTypeCount)();
int getSideTypeCount() {
    return (*_getSideTypeCount)();
}

void (*_cleanupPlugin)();
void cleanupPlugin() {
    return (*_cleanupPlugin)();
}

/*************************************************
 * Load and validate the plugin
 ************************************************/

void loadPlugin(
    char            *plugin,
    _logging_level  logging_level 
) {

    char    *error;
    short   (*_initialisePlugin)();
    char    *lib_path;
    char    *ch;

    // convert <plugin> to libPlugin.so
    lib_path = calloc( strlen( plugin ) + 8 + 1, sizeof(char) );

    sprintf( lib_path, "%slib%s.so",
            ( ( *plugin != '.' ) && ( *plugin != '/' ) ) ? "./" : "",
            plugin
    );

    ch = &lib_path[ strlen( lib_path ) - 3 - strlen( plugin ) ];
    if ( ( *ch >= 'a' ) && ( *ch <= 'z' ) )
        *ch += 'A' - 'a';

    plugin_hndl = dlopen( lib_path, RTLD_LAZY );
    free( lib_path );

    if ( ! plugin_hndl )
        logFatal( dlerror() );

    LOAD_RESOURCE( initialisePlugin );
    (*_initialisePlugin)( logging_level );

    LOAD_RESOURCE( drawTiling );
    LOAD_RESOURCE( polygonTypeEdges );
    LOAD_RESOURCE( getTemplateCount );
    LOAD_RESOURCE( getTemplateByType );
    LOAD_RESOURCE( getTemplateByPos );
    LOAD_RESOURCE( findTemplateTypePos );
    LOAD_RESOURCE( getSideTypeDescription );
    LOAD_RESOURCE( polygonTypeChar );
    LOAD_RESOURCE( getSideTypeCount );
    LOAD_RESOURCE( checkForValidSides );

    LOAD_RESOURCE( cleanupPlugin );
}

/*************************************************
 * Release the plugin handle
 ************************************************/

void releasePlugin() {

    cleanupPlugin();

    if ( plugin_hndl != NULL )
        dlclose( plugin_hndl );
}
