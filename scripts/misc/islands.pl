#!/usr/bin/perl

use warnings;
use strict;

############################################################
#
#   islands.pl
#
#   Wave modelling....
#
############################################################

use Math::Trig;
use Getopt::Std;
use Readonly;

use Data::Dumper;

############################################################
# Setup the environment

my $IMAGE = {
    'SEEDS' =>  [],
};

my Readonly $DEFAULT_OUTPUT =   'islands.pdf';

############################################################
# Check the runline options

my %options;
getopts( 'hD:F:K:S:W:', \%options );

help() if $options{ 'h' };

$IMAGE->{ 'TEMP_PS' } = $options{ 'K' } || 'template.ps';
$IMAGE->{ 'DEST' } = $options{ 'F' } || $DEFAULT_OUTPUT;

$IMAGE->{ 'SCALE' } = $options{ 'S' } || 1;
$IMAGE->{ 'DEPTH' } = $options{ 'W' } || 1000;
$IMAGE->{ 'WIDTH' } = $options{ 'D' } || 1000;

############################################################
# Setup the image

my $rad = 300;
for ( my $s = 0; $s < 8; $s++ ) {

    my $ang = $s * pi / 4;

    push @{ $IMAGE->{ 'SEEDS' } }, {
        'X' =>  $rad * cos( $ang ),
        'Y' =>  $rad * sin( $ang ),
    };
}

############################################################
# Process the image

processImage();
drawImage();

exit;

############################################################
# Process the image

sub processImage {

}

############################################################
# Write the image header

sub writeImageHeader {

    my $fh = shift;

    printf $fh "%%!PS\n%%%%BoundingBox: 0 0 %f %f\n%%%%EndComments\n

        /imgw %f def
        /imgd %f def

        <<
            /PageSize [ imgw imgd ]
            /ImagingBBox null
        >> setpagedevice

        /mv { moveto } bind def
        /ln { lineto } bind def

        /disc { 0 360 arc fill } def   %% x y rad disc

        0 0 mv imgw 0 ln imgw imgd ln 0 imgd ln fill

        imgw 2 div imgd 2 div translate

        1 setlinejoin
        1 1 0 setrgbcolor
    ",
        $IMAGE->{ 'WIDTH' }, $IMAGE->{ 'DEPTH' },
        $IMAGE->{ 'WIDTH' }, $IMAGE->{ 'DEPTH' },
    ;

    return;
}

############################################################
# Distance bewteen 2 points

sub distance {

    my $pt1 = shift;
    my $pt2 = shift;

    return sqrt( 
        ( $pt1->{ 'X' } - $pt2->{ 'X' } ) * ( $pt1->{ 'X' } - $pt2->{ 'X' } ) +
        ( $pt1->{ 'Y' } - $pt2->{ 'Y' } ) * ( $pt1->{ 'Y' } - $pt2->{ 'Y' } )
    );
}

############################################################
# Write the image details

sub writeImageDetail {

    my $fh = shift;

    printf $fh "/buf 256 string def\n";
    printf $fh "%d %d 8 [ %d 0 0 -%d 0 %d ]\n",
                            ( $IMAGE->{ 'WIDTH' }, $IMAGE->{ 'DEPTH' } )x2, 
                            $IMAGE->{ 'DEPTH' }
    ;
    printf $fh "{ currentfile buf readhexstring pop } image\n";

    for ( my $d = 0; $d < $IMAGE->{ 'DEPTH' }; $d++ ) {

        for ( my $w = 0; $w < $IMAGE->{ 'WIDTH' }; $w++ ) {

            my $val = 0;
            for ( my $s = 0; $s < 8; $s++ ) {
                
                my $pnt = {
                    'X' =>  $w,
                    'Y' =>  $d,
                };

                $val += sin( distance( $pnt, $IMAGE->{ 'SEEDS' }->[ $s ] ) / 100 );
            }

            printf $fh "%02x", 64 * $val;
        }

        printf $fh "\n";
    }

    print $fh "gsave 1 1 0 setrgbcolor\n";

    for ( my $s = 0; $s < 8; $s++ ) {
        
        my $seed = $IMAGE->{ 'SEEDS' }->[ $s ];
        printf $fh "%f %f 2 disc\n",
                        $seed->{ 'X' },
                        $seed->{ 'Y' },
        ;
    }

    print $fh "grestore\n";

    return;
}

############################################################
# Draw the image

sub drawImage {

    open my $fh, '>', $IMAGE->{ 'TEMP_PS' } or do {
        printf "Could not open %s: $!\n", $IMAGE->{ 'TEMP_PS' };
        exit -1;
    };

    writeImageHeader( $fh );
    writeImageDetail( $fh );

    close $fh;

    my $convert = "/usr/local/bin/convert";
    $convert = "/usr/bin/convert" unless ( -f $convert );
    $convert = "/Applications/ImageMagick/ImageMagick-6.6.5/bin/convert" unless ( -f $convert );

    unless ( -f $convert ) {

        printf "Could not locate convert at $convert\n";
    }
    else {

        unlink $IMAGE->{ 'DEST' };
        my $cmd = sprintf "$convert '%s' '%s'", $IMAGE->{ 'TEMP_PS' }, $IMAGE->{ 'DEST' };
        system( $cmd );
    }

    unlink $IMAGE->{ 'TEMP_PS' } unless $options{ 'K' };

    return;
}

############################################################
# Help doco

sub help {

    print "\n@_\n" if @_;

    printf "\n$0 " .
                "[-h] [-D <depth>] [-F <file>] [-K <file>] [-S <scale>] [-W <width>]

        where
            -h          this help message

            -D <depth>  image depth (default 1000)
            -F <file>   output file (default '$DEFAULT_OUTPUT')
            -K <file>   temp ps file to retain
            -S <scale>  drawing scale (default 1)
            -W <width>  image width (default 1000)

    \n";

    exit;
}
