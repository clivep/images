#!/usr/bin/perl

use warnings;
use strict;

use Math::Complex;
use Math::Trig;
use Getopt::Std;
use Data::Dumper;

############################################################
# Process any command line options

my %options;
getopts( 'dD:fF:g:hi:K:lmo:prR:sS:', \%options );

help() if $options{ 'h' };
my $iterations = $options{ 'i' } || 1;

my $temp_ps = $options{ 'K' } || 'clp.ps';

############################################################
# Load the available frames and generators

my $frames = loadFrames();

my $template_data = loadGenerators();

if ( $options{ 'l' } ) {
    
    printf "Available algorithms\n";

    for my $section ( sort keys %{ $template_data->{ 'SECTIONS' } } ) {
        printf "    $section\n";
    
        for my $key ( sort keys %{ $template_data } ) {
            next if $key eq 'SECTIONS';
            next if $template_data->{ $key }->{ 'SECTION' } ne $section;
            printf "        %-4s: %s\n", $key, $template_data->{ $key }->{ 'DESCRIPTION' };
        }
    }

    printf "\nAvailable frames\n";

    for my $key ( sort keys %{ $frames } ) {
        printf "        %-4s: %s\n", $key, $frames->{ $key }->{ 'DESCRIPTION' };
    }

    exit;
}

if ( $options{ 's' } ) {
    
    printf "Available sections\n";

    for my $key ( sort keys %{ $template_data->{ 'SECTIONS' } } ) {
        printf "    $key\n";
    }

    exit;
}

help( "Missing generator (-g) switch" ) unless $options{ 'g' };

help( "Not a valid generator name" ) unless $template_data->{ $options{ 'g' } };
my $generator = $template_data->{ $options{ 'g' } };

############################################################
# The color schemes supported

my $schemes = {

    'RED'       =>  "1 0 0 setrgbcolor",
    'GREEN'     =>  "0 1 0 setrgbcolor",
    'BLUE'      =>  "0 0 1 setrgbcolor",

    'YELLOW'    =>  "1 1 0 setrgbcolor",
};

help( "Not a valid scheme name" ) if  $options{ 'S' } && ! $schemes->{  $options{ 'S' } };

############################################################
# Setup the image frame
  
my $frame_key = $options{ 'F' } || $generator->{ 'FRAME' } || 'hl';
my $frame = $frames->{ $frame_key };
help( "Not a valid frame name" ) unless $frame;

for my $seg ( @{ $frame->{ 'DATA' } } ) {

    $seg->{ 'INITIAL' } = 1;
    $seg->{ 'SCHEME' } = $schemes->{ $options{ 'S' } || $seg->{ 'COL_SCHEME' } || 'YELLOW' };
}

############################################################
# Process the required number of interations
  
printf "\033[2J" if $options{ 'm' };

my $template_info = setupPathInfo( $generator, 'SEGMENTS' );

my $image_segments = $frames->{ $frame_key }->{ 'DATA' };

for ( my $i = 0; $i < $iterations; $i++ ) {

    $image_segments = processSegments( $image_segments, $template_info );
}

############################################################
# Draw or dump the image
  
$options{ 'D' } ? dumpSegments( $image_segments ) : drawSegments( $image_segments );

exit;

############################################################
# Load segment data from DATA 
  
sub loadSegmentInfo {

    my $errors = shift;
    my $line_num = shift;

    my $segment_info = [];

    while ( my $ln = <DATA> ) {

        $$line_num++;

        chomp $ln;

        last if $ln eq ']';

        my ( $v1, $v2, $v3, $v4 ) =
            ( $ln =~ /\[\s*([-\d]*)\s*,\s*([-\d]*)\s*,\s*([-\d]*)\s*,\s*([-\d]*)\s*\]/x );

        if ( $v4 ) {

            push @{ $segment_info }, [ $v1 * 1, $v2 * 1, $v3 * 1, $v4 * 1 ];
        }
        else {
    
            push ( @{ $errors }, "Missing generator vals at line " . $$line_num ) unless $v4;
        }
    }

    return $segment_info;
}

############################################################
# loadGenerators
#   Recover the available generators from the DATA segment

sub loadGenerators {

    my $generators = {
        'SECTIONS'  =>  {},
    };

    my $current_section = "";
    my $current_tag = "";
    my $line_num = 0;
    my @errors;

    while ( my $ln = <DATA> ) {

        $line_num++;

        chomp $ln;
        $ln =~ s/\s*\#.*//xg;
        $ln =~ s/^\s*//x;
        next unless $ln;

        my ( $key, $val ) = ( $ln =~ /(.*)\s*:\s*(.*)/x );
        $key ||= "";
    
        if ( $key eq 'Section' ) {

            $current_section = $val;
            $generators->{ 'SECTIONS' }->{ $current_section } = 1;
        }
        elsif ( $key eq 'Tag' ) {
    
            $generators->{ $val } = {
                'SECTION'   =>  $current_section,
            };
            $current_tag = $val;

            push @errors, "Missing tag val at line $line_num" unless $val;
        }
        elsif ( $key eq 'Description' ) {

            $generators->{ $current_tag }->{ 'DESCRIPTION' } = $val;

            push @errors, "Missing description val at line $line_num" unless $val;
        }
        elsif ( $key eq 'Grid' ) {

            $generators->{ $current_tag }->{ 'GRID' } = $val;
            push @errors, "Missing grid val at line $line_num" unless $val;

            push @errors, "Invalid val ($val) at line $line_num" if
                    ( $val &&
                        ( $val ne 'square' ) &&
                        ( $val ne 'triangle' ) &&
                        ( $val ne 'mod-triangle' )
                    );
        }
        elsif ( $key eq 'Generator' ) {

            $generators->{ $current_tag }->{ 'SEGMENTS' } = loadSegmentInfo( \@errors, \$line_num );
        }
        elsif ( $key eq 'Frame' ) {

            $generators->{ $current_tag }->{ 'FRAME' } = $val;

            push @errors, "Missing frame val at line $line_num" unless $val;
            use Data::Dumper;
            push @errors, "Missing frame val at line $line_num" unless $frames->{ $val };
        }
    }

    if ( scalar @errors > 0 ) {

        printf "There were errors in the generator data:\n";
        for my $err ( @errors ) {
            printf( "    %s\n", $err );
        }
        exit;
    }

    return $generators;
}

############################################################
# translatePath
#   Modify the path segments to match the grid type

sub translatePath {

    my $grid = shift;
    my $path = shift;

    for my $seg ( @{ $path } ) {

        if ( $grid eq 'triangle' ) {
    
            next if ( $seg->[ 1 ] == 0 );
    
            $seg->[ 0 ] = ( ( $seg->[ 0 ] == 0 ) ? $seg->[ 1 ] : $seg->[ 0 ] ) / 2.0;
            $seg->[ 1 ] *= sqrt( 3.0 ) / 2.0;
        }
        elsif ( $grid eq 'mod-triangle' ) {
    
            $seg->[ 0 ] /= 2.0;
            $seg->[ 1 ] *= sqrt( 3.0 ) / 2.0;
        }
    }

    return;
}

############################################################
# setupPathInfo
#   Concatenate the path segments

sub setupPathInfo {

    my $generator = shift;
    my $path_name = shift;

    return unless $generator->{ $path_name };

    # Modify the path to match the grid
    translatePath( $generator->{ 'GRID' }, $generator->{ $path_name } );

    my $segments = [];

    my $x = 0;
    my $y = 0;

    for my $seg ( @{$generator->{ $path_name }} ) {

        push @{ $segments }, {

            'START_PT'  =>  {
                'X' =>  $x,
                'Y' =>  $y,
            },
            'END_PT'    =>  {
                'X' =>  $x + $seg->[ 0 ],
                'Y' =>  $y + $seg->[ 1 ],
            },
            'DIRECTION' =>  $seg->[ 2 ],
            'SIDE'      =>  $seg->[ 3 ],
        };

        $x += $seg->[ 0 ];
        $y += $seg->[ 1 ];
    }

    my $s = scalar @{ $generator->{ $path_name } };

    return {
        'SEGMENTS'      =>  $segments,
        'LENGTH'        =>  sqrt( $x * $x + $y * $y ),
        'ANGLE'         =>  atan2( $y, $x ),

        # for progression metrics
        'TOTAL_SEGS'    =>  $s > 1 ? ( $s ** ( $iterations + 1 ) - $s ) / ( $s - 1 ) : $s,
        'SEGS_DONE'     =>  0,
    };
}

############################################################
# distance 
#   Return the distance between the 2 pojnts

sub distance {

    my $pt1 = shift;
    my $pt2 = shift;

    return sqrt(
        ( $pt1->{ 'X' } - $pt2->{ 'X' } ) * ( $pt1->{ 'X' } - $pt2->{ 'X' } ) +
        ( $pt1->{ 'Y' } - $pt2->{ 'Y' } ) * ( $pt1->{ 'Y' } - $pt2->{ 'Y' } ) 
    );
}

############################################################
# transformPoint 
#   Apply a transformation to the given point

sub transformPoint{

    my $pt = shift;
    my $ang = shift;
    my $scale = shift;
    my $offset = shift;

    my $nx = cos( $ang ) * $pt->{ 'X' } - sin( $ang ) * $pt->{ 'Y' };
    my $ny = sin( $ang ) * $pt->{ 'X' } + cos( $ang ) * $pt->{ 'Y' };

    return {
        'X' =>  $nx * $scale->{ 'X' } + $offset->{ 'X' },
        'Y' =>  $ny * $scale->{ 'Y' } + $offset->{ 'Y' },
    }
}

############################################################
# segmentOffset
#   Find the offset required to place the segment

sub segmentOffset {

    my $segment = shift;
    my $generator_info = shift;

    my $offset = $segment->{ 'DIRECTION' } == 1 ?
        $segment->{ 'START_PT' } 
    : 
        $segment->{ 'END_PT' };

    return $offset;
}

############################################################
# segmentScale
#   Find the scale required to place the segment

sub segmentScale {

    my $segment = shift;
    my $generator_info = shift;

    my $segment_len = distance( $segment->{ 'START_PT' }, $segment->{ 'END_PT' } );

    my $scale = {
        'X' =>  $segment_len / $generator_info->{ 'LENGTH' },
        'Y' =>  $segment_len / $generator_info->{ 'LENGTH' },
    };

    $scale->{ 'X' } *= $segment->{ 'DIRECTION' } * $segment->{ 'SIDE' };

    return $scale;
}

############################################################
# segmentRotation 
#   Find the rotation required to rotate the segment

sub segmentRotation {

    my $segment = shift;
    my $generator_info = shift;

    my $segment_ang = atan2(
                $segment->{ 'END_PT' }->{ 'Y' } - $segment->{ 'START_PT' }->{ 'Y' }, 
                $segment->{ 'END_PT' }->{ 'X' } - $segment->{ 'START_PT' }->{ 'X' }, 
    );

    my $rotation;

    if ( $segment->{ 'DIRECTION' } == 1 ) {

        $rotation = $segment->{ 'SIDE' } == 1 ?
            $segment_ang - $generator_info->{ 'ANGLE' } 
        :
            pi - $segment_ang - $generator_info->{ 'ANGLE' }
        ;
    }
    else {

        $rotation = $segment->{ 'SIDE' } == 1 ?
            -1 * ( $generator_info->{ 'ANGLE' } + $segment_ang )
        :
            pi - $generator_info->{ 'ANGLE' } + $segment_ang 
        ;
    }

    return $rotation;
}

############################################################
# applyGenerator2Segment 
#   Apply the generators to a single segment

sub applyGenerator2Segment {

    my $segment = shift;
    my $generator_info = shift;

    my $offset = segmentOffset( $segment, $generator_info );
    my $scale = segmentScale( $segment, $generator_info );
    my $rotate = segmentRotation( $segment, $generator_info );

    my @new_segments = ();

    my @generator_segments = @{ $generator_info->{ 'SEGMENTS' } };
    @generator_segments = reverse( @generator_segments ) if ( $segment->{ 'DIRECTION' } == -1 );

    for my $seg ( @generator_segments ) {

        my $pt1 = transformPoint( $seg->{ 'START_PT' }, $rotate, $scale, $offset );
        my $pt2 = transformPoint( $seg->{ 'END_PT' }, $rotate, $scale, $offset );

        push @new_segments, {

            'START_PT'  =>  ( $segment->{ 'DIRECTION' } == 1 ) ? $pt1 : $pt2,
            'END_PT'    =>  ( $segment->{ 'DIRECTION' } == 1 ) ? $pt2 : $pt1,
            'DIRECTION' =>  $segment->{ 'DIRECTION' } * $seg->{ 'DIRECTION' },
            'SIDE'      =>  $segment->{ 'SIDE' } * $seg->{ 'SIDE' },
            'SCHEME'    =>  $segment->{ 'SCHEME' },
        };
        
        $generator_info->{ 'SEGS_DONE' }++;
    }

    $new_segments[ 0 ]->{ 'INITIAL' } = 1 if $segment->{ 'INITIAL' };

    printf "\033[0;0H%0.3f%%         ", 
            100 * $generator_info->{ 'SEGS_DONE' } / $generator_info->{ 'TOTAL_SEGS' } if $options{ 'm' };

    return @new_segments;
}

############################################################
# processSegments 
#   Apply the generators to each to the set of segments

sub processSegments {

    my $segments = shift;
    my $generator_info = shift;

    my $new_segments = [];
    for my $seg ( @{$segments} ) {

       push @{ $new_segments }, applyGenerator2Segment( $seg, $generator_info );
    }

    return $new_segments;
}

############################################################
# min / max 
#   Convenience functions

sub min {
    
    my $v1 = shift;
    my $v2 = shift;

    return $v1 < $v2 ? $v1 : $v2;
}

sub max {
    
    my $v1 = shift;
    my $v2 = shift;

    return $v1 > $v2 ? $v1 : $v2;
}

############################################################
# getSegmentLimits
#   Find the bounds of the segments in the list

sub getSegmentLimits {

    my $segments = shift;

    my $lx = 0;
    my $ly = 0;
    my $ux = 0;
    my $uy = 0;

    # preparse the segments to find the image size
    for my $seg ( @{$segments} ) {

        $lx = min( $lx, $seg->{ 'START_PT' }->{ 'X' } );
        $ux = max( $ux, $seg->{ 'START_PT' }->{ 'X' } );

        $lx = min( $lx, $seg->{ 'END_PT' }->{ 'X' } );
        $ux = max( $ux, $seg->{ 'END_PT' }->{ 'X' } );

        $ly = min( $ly, $seg->{ 'START_PT' }->{ 'Y' } );
        $uy = max( $uy, $seg->{ 'START_PT' }->{ 'Y' } );

        $ly = min( $ly, $seg->{ 'END_PT' }->{ 'Y' } );
        $uy = max( $uy, $seg->{ 'END_PT' }->{ 'Y' } );
    }

    return ( $lx, $ly, $ux, $uy );
}

############################################################
# dumpSegments 
#   Write the segment values to a file

sub dumpSegments {

    my $segments = shift;

    my $dest = $options{ 'D' };

    open my $fh, '>', $dest || help( "Could not open $dest" );
    
    my @bounds = getSegmentLimits( $segments );

    printf $fh "Description: %s\n", $generator->{ 'DESCRIPTION' };
    printf $fh "Grid: %s\n", $generator->{ 'GRID' };
    printf $fh "Bounding Box: %f %f %f %f\n", @bounds;

    printf $fh "Segments:\n%0.6f %0.6f\n",
                $segments->[ 0 ]->{ 'START_PT' }->{ 'X' },
                $segments->[ 0 ]->{ 'START_PT' }->{ 'Y' },
    ;

    for my $seg ( @{$segments} ) {

        printf $fh "%0.6f %0.6f\n",
                    $seg->{ 'END_PT' }->{ 'X' },
                    $seg->{ 'END_PT' }->{ 'Y' },
        ;
    }

    close $fh;
}

############################################################
# drawSegments 
#   Render the segments details

sub drawSegments {
    
    my $segments = shift;

    printf "\033[2J\033[0;0HDrawing        "  if $options{ 'm' };

    my $image_wid = 1000;
    my $image_dep = 1000;

    my $image_usage = 0.9;

    my @bounds = getSegmentLimits( $segments );

    my $xsc = ( $image_wid * $image_usage ) / ( $bounds[ 2 ] - $bounds[ 0 ] );
    my $ysc = ( $image_wid * $image_usage ) / ( $bounds[ 3 ] - $bounds[ 1 ] );
    my $sc = $xsc < $ysc ? $xsc : $ysc;

    my $xoff = ( $image_wid - ( ( $bounds[ 2 ] + $bounds[ 0 ] ) * $sc ) ) / 2.0;
    my $yoff = ( $image_dep - ( ( $bounds[ 3 ] + $bounds[ 1 ] ) * $sc ) ) / 2.0;

    open my $fh, '>', $temp_ps || help( "Could not open " . $temp_ps );

    printf $fh "%%!PS\n%%BoundingBox: 0 0 %f %f\n%%EndComments\n

        <<
            /PageSize [ %f %f ]
            /ImagingBBox null
        >> setpagedevice

        /mv { moveto } bind def
        /ln { lineto } bind def

        /lx %f def
        /ly %f def
        /ux %f def
        /uy %f def

        /sc %f def
        /xoff %f def
        /yoff %f def

        /proj %s def

        /trns {

            /y exch def
            /x exch def

            proj {

                /nlx lx sc mul xoff add def
                /nly ly sc mul yoff add def
                /nux ux sc mul xoff add def
                /nuy uy sc mul yoff add def

                /nx x sc mul xoff add nux nlx add 2 div sub nux nlx sub 2 div div def
                /ny y sc mul yoff add nuy nly add 2 div sub nuy nly sub 2 div div def
            
                nx 1 ny dup mul 2 div sub sqrt mul nux nlx sub 2 div mul nux nlx add 2 div add
                ny 1 nx dup mul 2 div sub sqrt mul nuy nly sub 2 div mul nuy nly add 2 div add
            } {

                x sc mul xoff add
                y sc mul yoff add
            } ifelse

        } def

        0 0 mv %f 0 ln %f %f ln 0 %f ln fill

        1 setlinejoin
    ",
        $image_wid, $image_dep,
        $image_wid, $image_dep,
        @bounds,
        $sc, $xoff, $yoff, $options{ 'p' } ? 'true' : 'false',
        $image_wid, $image_wid,
        $image_dep, $image_dep,
        ;

    my $x = 0;
    my $y = 0;

    for ( my $s = 0; $s < scalar @{$segments}; $s++ ) {
    
        my $seg = $segments->[ $s ];

        if ( $seg->{ 'INITIAL' } ) {

            # add the final segment to a rounded image..?
            printf $fh "%f %f trns ln %s stroke\n",
                            $segments->[ $s - 1 ]->{ 'END_PT' }->{ 'X' },
                            $segments->[ $s - 1 ]->{ 'END_PT' }->{ 'Y' },
                            $segments->[ $s - 1 ]->{ 'SCHEME' },
                if ( $s > 0 && $options{ 'r' } )
            ;

            # start of a new frame segment
            printf $fh "%f %f trns mv\n",
                    $segments->[ 0 ]->{ 'START_PT' }->{ 'X' },
                    $segments->[ 0 ]->{ 'START_PT' }->{ 'Y' },
            ;
        }

        if ( $options{ 'r' } ) {

            if ( $s > 0 && ! $seg->{ 'INITIAL' } ) {
            
                my $pt_reset = sprintf "currentpoint %s stroke mv", $seg->{ 'SCHEME' };

                my $r = ( $options{ 'R' } || 15 ) * distance( $seg->{ 'START_PT' }, $seg->{ 'END_PT' } ) / 100.0;
                printf $fh "%f %f trns %f %f trns %f arct %s\n",
                                    $seg->{ 'START_PT' }->{ 'X' },
                                    $seg->{ 'START_PT' }->{ 'Y' },
                                    $seg->{ 'END_PT' }->{ 'X' },
                                    $seg->{ 'END_PT' }->{ 'Y' },
                                    $r * $sc,
                                    $options{ 'f' } ? "" : $pt_reset,
                ;
            }
        }
        elsif ( $options{ 'f' } ) {
        
            printf $fh "%f %f trns ln\n",
                    $seg->{ 'END_PT' }->{ 'X' },
                    $seg->{ 'END_PT' }->{ 'Y' },
            ;
        }
        else {

            drawDirectionMarkers( $fh, $seg ) if $options{ 'd' };

            printf $fh "%f %f trns mv %f %f trns ln\n",
                    $seg->{ 'START_PT' }->{ 'X' },
                    $seg->{ 'START_PT' }->{ 'Y' },
                    $seg->{ 'END_PT' }->{ 'X' },
                    $seg->{ 'END_PT' }->{ 'Y' },
            ;
        
            printf $fh "%s stroke\n", $seg->{ 'SCHEME' };
        }
    }

    if ( $options{ 'f' } ) {

        printf $fh "%f %f trns ln\n",
                    $segments->[ -1 ]->{ 'END_PT' }->{ 'X' },
                    $segments->[ -1 ]->{ 'END_PT' }->{ 'Y' },
        ;
    
        printf $fh "5000 0 rlineto 5000 -5000 ln -5000 -5000 ln -5000 %f trns ln %f %f trns ln %s fill\n",
                                $segments->[ 0 ]->{ 'START_PT' }->{ 'Y' },
                                $segments->[ 0 ]->{ 'START_PT' }->{ 'X' },
                                $segments->[ 0 ]->{ 'START_PT' }->{ 'Y' },
                                $segments->[ 0 ]->{ 'SCHEME' },
        ;
    }
    elsif( $options{ 'r' } ) {

        printf $fh "%f %f trns ln %s stroke\n",
                    $segments->[ -1 ]->{ 'END_PT' }->{ 'X' },
                    $segments->[ -1 ]->{ 'END_PT' }->{ 'Y' },
                    $segments->[ -1 ]->{ 'SCHEME' },
        ;
    }

    close $fh;

    my $dest = $options{ 'o' } || 'clp.png';
    system( "convert $temp_ps $dest");

    unlink $temp_ps unless $options{ 'K' };

    printf "\033[2J\033[0;0HDone           \n" if $options{ 'm' };

    return;
}

############################################################
# drawDirectionMarkers
#   Draw markers indicating the segment side and direction

sub drawDirectionMarkers {

    my $fh = shift;
    my $seg = shift;

    my $x1 = $seg->{ 'START_PT' }->{ 'X' };
    my $y1 = $seg->{ 'START_PT' }->{ 'Y' };

    my $x2 = $seg->{ 'END_PT' }->{ 'X' };
    my $y2 = $seg->{ 'END_PT' }->{ 'Y' };

    my $dx = $x2 - $x1;
    my $dy = $y2 - $y1;

    my $dir = $seg->{ 'DIRECTION' };
    my $side = $seg->{ 'SIDE' };

    printf $fh "gsave newpath %f %f trns mv ", $dir == 1 ? ( $x2, $y2 ) : ( $x1, $y1 );
    printf $fh "%f %f rlineto ", -30 * $dir * $dx, -30 * $dir * $dy;
    printf $fh "%f %f rlineto ", -8 * $side * $dy, 8 * $side * $dx;
    printf $fh "%f %f trns ln ", $dir == 1 ? ( $x2, $y2 ) : ( $x1, $y1 );
    printf $fh "%s fill grestore\n", $seg->{ 'SCHEME' };

    return;
}

############################################################
# loadFrames
#   Return the set of initial frameworks

sub loadFrames {

    return  {
        "sq"    =>  {
                        'DESCRIPTION'   =>  'Square',
                        'DATA'          =>  [
                            {
                                'COL_SCHEME'    =>  'RED',
                                'START_PT'      =>  { 'X' => -1, 'Y' => -1 },
                                'END_PT'        =>  { 'X' =>  1, 'Y' => -1 },
                                'DIRECTION'     =>  1, 
                                'SIDE'          =>  1,
                            },
                            {
                                'COL_SCHEME'    =>  'GREEN',
                                'START_PT'      =>  { 'X' =>  1, 'Y' => -1 },
                                'END_PT'        =>  { 'X' =>  1, 'Y' =>  1 },
                                'DIRECTION'     =>  1, 
                                'SIDE'          =>  1,
                            },
                        ],
        },
        "tri"   =>  {
                        'DESCRIPTION'   =>  'Clockwise triangle',
                        'DATA'          =>  [
                            {
                                'COL_SCHEME'    =>  'RED',
                                'START_PT'      =>  { 'X' => -1, 'Y' =>  0 },
                                'END_PT'        =>  { 'X' =>  1, 'Y' =>  0 },
                                'DIRECTION'     =>  1, 
                                'SIDE'          =>  1,
                            },
                            {
                                'COL_SCHEME'    =>  'GREEN',
                                'START_PT'      =>  { 'X' =>  1, 'Y' =>  0 },
                                'END_PT'        =>  { 'X' =>  0, 'Y' =>  sqrt( 3.0 ) },
                                'DIRECTION'     =>  1, 
                                'SIDE'          =>  1,
                            },
                            {
                                'COL_SCHEME'    =>  'BLUE',
                                'START_PT'      =>  { 'X' =>  0, 'Y' =>  sqrt( 3.0 ) },
                                'END_PT'        =>  { 'X' => -1, 'Y' =>  0 },
                                'DIRECTION'     =>  1, 
                                'SIDE'          =>  1,
                            },
                        ],
        },
        "tri2"      =>  {
                        'DESCRIPTION'   =>  'Clockwise triangle - reversed direction',
                        'DATA'          =>  [
                            {
                                'COL_SCHEME'    =>  'RED',
                                'START_PT'      =>  { 'X' => -1, 'Y' =>  0 },
                                'END_PT'        =>  { 'X' =>  1, 'Y' =>  0 },
                                'DIRECTION'     =>  -1, 
                                'SIDE'          =>  -1,
                            },
                            {
                                'COL_SCHEME'    =>  'GREEN',
                                'START_PT'      =>  { 'X' =>  1, 'Y' =>  0 },
                                'END_PT'        =>  { 'X' =>  0, 'Y' =>  sqrt( 3.0 ) },
                                'DIRECTION'     =>  -1, 
                                'SIDE'          =>  -1,
                            },
                            {
                                'COL_SCHEME'    =>  'BLUE',
                                'START_PT'      =>  { 'X' =>  0, 'Y' =>  sqrt( 3.0 ) },
                                'END_PT'        =>  { 'X' => -1, 'Y' =>  0 },
                                'DIRECTION'     =>  -1, 
                                'SIDE'          =>  -1,
                            },
                        ],
        },
        "b2b"       =>  {
                        'DESCRIPTION'   =>  '2 reversed lines',
                        'DATA'          =>  [
                            {
                                'COL_SCHEME'    =>  'RED',
                                'START_PT'      =>  { 'X' =>  0, 'Y' =>  0 },
                                'END_PT'        =>  { 'X' =>  2, 'Y' =>  0 },
                                'DIRECTION'     =>  1, 
                                'SIDE'          =>  1,
                            },
                            {
                                'COL_SCHEME'    =>  'GREEN',
                                'START_PT'      =>  { 'X' =>  2, 'Y' =>  0 },
                                'END_PT'        =>  { 'X' =>  0, 'Y' =>  0 },
                                'DIRECTION'     =>  1, 
                                'SIDE'          =>  1,
                            },
                        ],
        },
        "x-out"     =>  {
                        'DESCRIPTION'   =>  'Cross outbound from the origin',
                        'DATA'          =>  [
                            {
                                'COL_SCHEME'    =>  'RED',
                                'START_PT'      =>  { 'X' =>  0, 'Y' =>  0 },
                                'END_PT'        =>  { 'X' =>  2, 'Y' =>  2 },
                                'DIRECTION'     =>  1, 
                                'SIDE'          =>  1,
                            },
                            {
                                'COL_SCHEME'    =>  'GREEN',
                                'START_PT'      =>  { 'X' =>  0, 'Y' =>  0 },
                                'END_PT'        =>  { 'X' =>  2, 'Y' => -2 },
                                'DIRECTION'     =>  1, 
                                'SIDE'          =>  1,
                            },
                            {
                                'COL_SCHEME'    =>  'BLUE',
                                'START_PT'      =>  { 'X' =>  0, 'Y' =>  0 },
                                'END_PT'        =>  { 'X' => -2, 'Y' => -2 },
                                'DIRECTION'     =>  1, 
                                'SIDE'          =>  1,
                            },
                            {
                                'COL_SCHEME'    =>  'YELLOW',
                                'START_PT'      =>  { 'X' =>  0, 'Y' =>  0 },
                                'END_PT'        =>  { 'X' => -2, 'Y' =>  2 },
                                'DIRECTION'     =>  1, 
                                'SIDE'          =>  1,
                            },
                        ],
        },
        "x-in"      =>  {
                        'DESCRIPTION'   =>  'Cross inbound to the origin',
                        'DATA'          =>  [
                            {
                                'COL_SCHEME'    =>  'RED',
                                'START_PT'      =>  { 'X' =>  2, 'Y' =>  2 },
                                'END_PT'        =>  { 'X' =>  0, 'Y' =>  0 },
                                'DIRECTION'     =>  1, 
                                'SIDE'          =>  1,
                            },
                            {
                                'COL_SCHEME'    =>  'GREEN',
                                'START_PT'      =>  { 'X' =>  2, 'Y' => -2 },
                                'END_PT'        =>  { 'X' =>  0, 'Y' =>  0 },
                                'DIRECTION'     =>  1, 
                                'SIDE'          =>  1,
                            },
                            {
                                'COL_SCHEME'    =>  'BLUE',
                                'START_PT'      =>  { 'X' => -2, 'Y' => -2 },
                                'END_PT'        =>  { 'X' =>  0, 'Y' =>  0 },
                                'DIRECTION'     =>  1, 
                                'SIDE'          =>  1,
                            },
                            {
                                'COL_SCHEME'    =>  'YELLOW',
                                'START_PT'      =>  { 'X' => -2, 'Y' =>  2 },
                                'END_PT'        =>  { 'X' =>  0, 'Y' =>  0 },
                                'DIRECTION'     =>  1, 
                                'SIDE'          =>  1,
                            },
                        ],
        },
        "dl"        =>  {
                        'DESCRIPTION'   =>  'Diagonal line',
                        'DATA'          =>  [
                            {
                                'START_PT'      =>  { 'X' => -2, 'Y' => -2 },
                                'END_PT'        =>  { 'X' =>  2, 'Y' =>  2 },
                                'DIRECTION'     =>  1, 
                                'SIDE'          =>  1,
                            },
                        ],
        },
        "2l-in"     =>  {
                        'DESCRIPTION'   =>  '2 lines toward the origin',
                        'DATA'          =>  [
                            {
                                'COL_SCHEME'    =>  'RED',
                                'START_PT'      =>  { 'X' => -2, 'Y' =>  0 },
                                'END_PT'        =>  { 'X' =>  0, 'Y' =>  0 },
                                'DIRECTION'     =>  1, 
                                'SIDE'          =>  1,
                            },
                            {
                                'COL_SCHEME'    =>  'GREEN',
                                'START_PT'      =>  { 'X' =>  2, 'Y' =>  0 },
                                'END_PT'        =>  { 'X' =>  0, 'Y' =>  0 },
                                'DIRECTION'     =>  1, 
                                'SIDE'          =>  1,
                            },
                        ],
        },
        "2l-out"    =>  {
                        'DESCRIPTION'   =>  '2 lines away from the origin',
                        'DATA'          =>  [
                            {
                                'COL_SCHEME'    =>  'RED',
                                'START_PT'      =>  { 'X' =>  0, 'Y' =>  0 },
                                'END_PT'        =>  { 'X' => -2, 'Y' =>  0 },
                                'DIRECTION'     =>  1, 
                                'SIDE'          =>  1,
                            },
                            {
                                'COL_SCHEME'    =>  'GREEN',
                                'START_PT'      =>  { 'X' =>  0, 'Y' =>  0 },
                                'END_PT'        =>  { 'X' =>  2, 'Y' =>  0 },
                                'DIRECTION'     =>  1, 
                                'SIDE'          =>  1,
                            },
                        ],
        },
        "hl"    =>  {
                        'DESCRIPTION'   =>  'Horizontal line',
                        'DATA'          =>  [
                            {
                                'START_PT'      =>  { 'X' => -2, 'Y' =>  0 },
                                'END_PT'        =>  { 'X' =>  2, 'Y' =>  0 },
                                'DIRECTION'     =>  1, 
                                'SIDE'          =>  1,
                            },
                        ],
        },
        "7d"    =>  {
                        'DESCRIPTION'   =>  'Frame for 7d tesselation',
                        'DATA'          =>  [
                            {
                                'COL_SCHEME'    =>  'RED',
                                'START_PT'      =>  { 'X' => -1, 'Y' =>  0 },
                                'END_PT'        =>  { 'X' =>  1, 'Y' =>  0 },
                                'DIRECTION'     =>  1, 
                                'SIDE'          =>  1,
                            },
                            {
                                'COL_SCHEME'    =>  'GREEN',
                                'START_PT'      =>  { 'X' => -1, 'Y' => -2 * sqrt( 3.0 ) },
                                'END_PT'        =>  { 'X' => -2, 'Y' => -1 * sqrt( 3.0 ) },
                                'DIRECTION'     =>  1, 
                                'SIDE'          =>  1,
                            },
                            {
                                'COL_SCHEME'    =>  'BLUE',
                                'START_PT'      =>  { 'X' =>  2, 'Y' => -1 * sqrt( 3.0 ) },
                                'END_PT'        =>  { 'X' =>  1, 'Y' => -2 * sqrt( 3.0 ) },
                                'DIRECTION'     =>  1, 
                                'SIDE'          =>  1,
                            },
                        ],
        },
    };
}

############################################################
# help
#   Some documentation

sub help {

    print "\n@_\n" if @_;

    printf "\n$0 " .
        "-g <tag> [-d] [-D] [-f] [-F <frame>] [-h] [-i <iters>] [-k <path>] [-l] [-m] [-o <dest>]
        [-p] [-r] [-R <pcnt>] [-s] [-S <scheme>]

    where
        -d          display direction/side markers
        -f          fill the image instead of stroking
        -F <frame>  initial frame to use
        -g <tag>    generator to use - mandatory
        -h          this help message
        -i <iters>  the number of iterations to apply
        -K <path>   temp postscript file to retain for debugging
        -l          list the available generators
        -m          show progression metrics
        -o <dest>   final (png) file for the image (default: clp.png)
        -r          render the curve with rounded corners
        -p          project the image onto a sphere
        -s          list generator sections

        -D <path>   dump the segment vertices to a file
        -R <pcnt>   vertex rounding as a segment percentage
        -S <scheme> coloring scheme to use
    \n";

    exit 0;
}

__DATA__

# Generator vals: [ x, y, dir, side ]

############################################################
Section: Root 2 generators

Tag: 2a
Description: Harter-Heighway Dragon
Grid: square
Generator: [
    [  1,  0,  1,  1 ],
    [  0,  1, -1, -1 ],
]

Tag: 2b
Description: Polya Sweep
Grid: square
Generator: [
    [  1,  0,  1, -1 ],
    [  0,  1, -1,  1 ],
]

Tag: 2c
Description: Levy C curve
Grid: square
Generator: [
    [  1,  0,  1,  1 ],
    [  0,  1,  1,  1 ],
]

############################################################
Section: Root 3 generators

Tag: 3a
Description: Von Koch Curve
Grid: triangle
Generator: [
    [  1,  0,  1,  1 ],
    [  0,  1,  1,  1 ],
    [  1, -1,  1,  1 ],
    [  1,  0,  1,  1 ],
]

Tag: 3b
Description: Ter-Dragon
Grid: triangle
Generator: [
    [  0,  1,  1,  1 ],
    [  1, -1,  1,  1 ],
    [  0,  1,  1,  1 ],
]

Tag: 3c
Description: Inverted Ter-Dragon
Grid: triangle
Generator: [
    [  0,  1, -1,  1 ],
    [  1, -1, -1,  1 ],
    [  0,  1, -1,  1 ],
]

Tag: 3d
Description: root 3 curve
Grid: triangle
Generator: [
    [  0,  1, -1,  1 ],
    [  1, -1,  1,  1 ],
    [  0,  1, -1,  1 ],
]

Tag: 3e
Description: root 3 curve
Grid: triangle
Generator: [
    [  0,  1, -1,  1 ],
    [  0,  1, -1,  1 ],
    [  1, -1,  1, -1 ],
]

Tag: 3f
Description: Yin Dragon
Grid: triangle
Generator: [
    [  0,  1,  1,  1 ],
    [  0,  1, -1, -1 ],
    [  1, -1,  1,  1 ],
]

Tag: 3g
Description: root 3 curve
Grid: triangle
Generator: [
    [  0,  1, -1, -1 ],
    [  0,  1,  1,  1 ],
    [  1, -1,  1,  1 ],
]

############################################################
Section: Root 4 generators

Tag: 4a
Description: Dragon of Eve
Grid: square
Generator: [
    [  0,  1,  1,  1 ],
    [  1, -1,  1,  1 ],
    [  1,  0, -1, -1 ],
]

Tag: 4b
Description: root 4 curve
Grid: square
Generator: [
    [  0,  1, -1,  1 ],
    [  1,  0,  1, -1 ],
    [  0, -1, -1,  1 ],
    [  1,  0,  1, -1 ],
]

Tag: 4c
Description: root 4 curve
Grid: square
Generator: [
    [  1,  1, -1, -1 ],
    [  1,  0,  1,  1 ],
    [  0, -1,  1,  1 ],
]

Tag: 4d
Description: Serpinski Arrowhead
Grid: triangle
Generator: [
    [  0,  1,  1, -1 ],
    [  1,  0,  1,  1 ],
    [  1, -1,  1, -1 ],
]

Tag: 4e
Description: root 4 curve
Grid: triangle
Generator: [
    [  1, -1,  1,  1 ],
    [  0,  1,  1,  1 ],
    [  0,  1,  1,  1 ],
    [  1, -1,  1,  1 ],
]

Tag: 4f
Description: root 4 curve
Grid: triangle
Generator: [
    [  0,  1,  1,  1 ],
    [  0,  1, -1,  1 ],
    [  1, -1,  1, -1 ],
    [  1, -1, -1,  1 ],
]

Tag: 4g
Description: root 4 curve
Grid: triangle
Generator: [
    [  1,  0,  1,  1 ],
    [ -1,  1, -1, -1 ],
    [  1,  0,  1,  1 ],
    [  1, -1, -1, -1 ],
]

Tag: 4h
Description: Peano Sweep
Grid: square
Generator: [
    [  0,  1, -1, -1 ],
    [  1,  0,  1,  1 ],
    [  1,  0,  1,  1 ],
    [  0, -1, -1, -1 ],
]

############################################################
Section: Root 5 generators

Tag: 5a
Description: root 5 curve
Grid: square
Generator: [
    [  0,  1,  1, -1 ],
    [  1,  0,  1,  1 ],
    [  0,  1,  1, -1 ],
    [  1,  0, -1,  1 ],
    [  0, -1,  1, -1 ],
]

Tag: 5b
Description: Mandelbrot Quartet
Grid: square
Generator: [
    [  0,  1, -1, -1 ],
    [  0,  1,  1,  1 ],
    [  1,  0,  1,  1 ],
    [  0, -1, -1, -1 ],
    [  1,  0,  1,  1 ],
]

Tag: 5c
Description: Inner-flip Quartet
Grid: square
Generator: [
    [  0,  1,  1, -1 ],
    [  0,  1, -1,  1 ],
    [  1,  0, -1,  1 ],
    [  0, -1,  1, -1 ],
    [  1,  0, -1,  1 ],
]

############################################################
Section: Root 7 generators

Tag: 7a
Description: Snowflake Sweep
Grid: triangle
Generator: [
    [  0,  1,  1, -1 ],
    [  0,  1, -1,  1 ],
    [  1,  0, -1,  1 ],
    [  1, -1, -1,  1 ],
    [ -3, -1, -1, -1 ],
    [  1,  0,  1, -1 ],
    [  1,  0, -1,  1 ],
]

Tag: 7b
Description: Gosper Curve
Grid: triangle
Generator: [
    [  1,  0,  1,  1 ],
    [  0,  1, -1, -1 ],
    [ -1,  0, -1, -1 ],
    [ -1,  1,  1,  1 ],
    [  1,  0,  1,  1 ],
    [  1,  0,  1,  1 ],
    [  1, -1, -1, -1 ],
]

Tag: 7c
Description: root 7 curve
Grid: triangle
Generator: [
    [  1,  0, -1,  1 ],
    [  1,  0, -1,  1 ],
    [ -1,  1,  1, -1 ],
    [  0, -1, -1,  1 ],
    [ -1,  1, -1,  1 ],
    [  1,  0,  1, -1 ],
    [  1,  0,  1, -1 ],
]

Tag: 7d
Description: root 7 curve
Grid: triangle
Generator: [
    [  0, -1,  1,  1 ],
    [  1,  0,  1,  1 ],
    [  1,  0, -1, -1 ],
    [  1,  0,  1,  1 ],
    [ -1,  1,  1,  1 ],
    [ -1,  1, -1, -1 ],
    [  1,  0, -1, -1 ],
]

############################################################
Section: Root 9 generators

Tag: 9a
Description: Christmas Tree
Grid: triangle
Generator: [
    [  3,  1, -1, -1 ],
    [ -1,  1,  1,  1 ],
    [  1,  0, -1,  1 ],
    [  0, -1, -1,  1 ],
    [  3, -1,  1, -1 ],
]

Tag: 9b
Description: Reflected Koch
Grid: triangle
Generator: [
    [  1,  0,  1, -1 ],
    [  0,  1, -1,  1 ],
    [  1, -1, -1,  1 ],
    [ -1,  0, -1, -1 ],
    [  1, -1,  1,  1 ],
    [  0,  1,  1, -1 ],
    [  1,  0,  1, -1 ],
]

Tag: 9c
Description: Peano Curve
Grid: square
Generator: [
    [  1,  0,  1,  1 ],
    [  0,  1,  1,  1 ],
    [  1,  0,  1,  1 ],
    [  0, -1,  1,  1 ],
    [ -1,  0,  1,  1 ],
    [  0, -1,  1,  1 ],
    [  1,  0,  1,  1 ],
    [  0,  1,  1,  1 ],
    [  1,  0,  1,  1 ],
]

############################################################
Section: Root 13 curves

Tag: 13a
Description: Peano Curve
Grid: triangle
Generator: [
    [  1,  0,  1,  1 ],
    [  0,  1,  1,  1 ],
    [ -1,  0, -1, -1 ],
    [  0,  1,  1,  1 ],
    [ -1,  1, -1, -1 ],
    [  1,  0,  1,  1 ],
    [  1, -1,  1,  1 ],
    [ -1,  0, -1, -1 ],
    [  1, -1,  1,  1 ],
    [  1,  0, -1, -1 ],
    [ -1,  1,  1,  1 ],
    [  1,  0, -1, -1 ],
    [  1, -1, -1, -1 ],
]

############################################################
Section: Root 16 curves

Tag: 16a
Description: root 16 curve
Grid: triangle
Generator: [
    [  2,  0,  1,  1 ],
    [  0,  1,  1,  1 ],
    [ -1,  1, -1, -1 ],
    [  0, -1,  1,  1 ],
    [ -1,  1, -1, -1 ],
    [  2,  0, -1,  1 ],
    [  2, -2,  1, -1 ],
]

############################################################
Section: Misc curves

Tag: m1
Description: Mandala Curve
Grid: triangle      # x values will be halved and y multiplied y (sqrt(3)/2)
Generator: [
    [  0,  1, -1, -1 ],
    [ -1,  1,  1,  1 ],
    [ -1,  1,  1,  1 ],
    [  0,  1, -1, -1 ],
    [  1,  0,  1,  1 ],
    [  1,  0,  1,  1 ],
    [  1,  0,  1,  1 ],
    [  0, -1,  1,  1 ],
    [  0, -2, -1, -1 ],
    [  1, -1, -1, -1 ],
    [  0,  1, -1, -1 ],
    [  0,  1, -1, -1 ],
    [  1,  0,  1,  1 ],
]

Tag: m2
Description: Snowflake Sweep #2
Grid: mod-triangle      # x values will be halved and y multiplied y (sqrt(3)/2)
Generator: [
    [  0,  2,  1, -1 ],
    [  0,  2,  1,  1 ],
    [  3,  1,  1,  1 ],
    [  3, -1,  1,  1 ],
    [ -2,  0,  1,  1 ],
    [ -2,  0,  1, -1 ],
    [ -1, -1,  1, -1 ],
    [  1, -1,  1, -1 ],
    [  3,  1,  1,  1 ],
    [ -1, -1,  1,  1 ],
    [ -1, -1,  1, -1 ],
    [  3,  1,  1, -1 ],
    [  3,  1,  1,  1 ],
]

Tag: m3
Description: Fukuda Gosper
Grid: triangle
Generator: [
    [  1,  0,  1,  1 ],
    [  1,  0,  1,  1 ],
    [  0,  1, -1, -1 ],
    [  0,  1, -1, -1 ],
    [ -1,  1,  1,  1 ],
    [ -1,  0, -1, -1 ],
    [ -1,  0, -1, -1 ],
    [  1, -1, -1, -1 ],
    [  1,  0,  1,  1 ],
    [  0, -1,  1,  1 ],
    [ -1,  0, -1, -1 ],
    [ -1,  1,  1,  1 ],
    [ -1,  1,  1,  1 ],
    [  0,  1, -1, -1 ],
    [  1,  0,  1,  1 ],
    [  1,  0,  1,  1 ],
    [  1,  0,  1,  1 ],
    [  1, -1, -1, -1 ],
    [  1, -1, -1, -1 ],
]

############################################################
Section: Compound curves

Tag: ca
Description: Von Koch Snowflake
Grid: triangle
Frame: hl
Generator: [
    [  1,  0,  1,  1 ],
    [  0,  1,  1,  1 ],
    [  1, -1,  1,  1 ],
    [  1,  0,  1,  1 ],
]

