/*******************************************************************************
 *
 * debruijn.h
 *
 *  Definitions required for debruijn tilings
 *
 ******************************************************************************/

#include "tiling-geometry.h"
#include "tiling-utils.h"

/*************************************************
 * Template types
 ************************************************/

typedef enum {

    TEMPLATE_TYPE_WIDE,
    TEMPLATE_TYPE_THIN

} _template_type;

/*************************************************
 * Polygon side type values
 ************************************************/

typedef enum {

    SIDE_TYPE_AB,
    SIDE_TYPE_BA,
    SIDE_TYPE_CD,
    SIDE_TYPE_DC,

    SIDE_TYPE_COUNT

} _side_type;

/*************************************************
 * Prototypes
 ************************************************/

void initialisePlugin( _logging_level logging_level );
short getTemplateCount();
short polygonTypeEdges( _template_type type );
_polygon *getTemplateByType( _template_type type );
_polygon *getTemplateByPos( short pos );
char polygonTypeChar( _polygon *polygon );
short findTemplateTypePos( _template_type  type );
char *getSideTypeDescription( _side_type pos );
int getSideTypeCount();
short checkForValidSides( _polygon *polygon1, short edge1,
                                _polygon *polygon2, short edge2 );

/*************************************************
 * Prototypes for internal functions
 ************************************************/

_polygon *initialiseThinTemplate();
_polygon *initialiseWideTemplate();

