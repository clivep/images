/*******************************************************************************
 *
 * fractal.h
 *
 *  Definitions for fractal renderings
 *
 ******************************************************************************/
#ifndef _FRACTAL_H

#include "fractal-utils.h"

/*************************************************
 * Prototypes
 ************************************************/
void usage( char *self );
void processParameters( int argc, char **argv, _image *image );
void writeImage( _image *image, char *dest );

_point *iterate( _image *image, _complex *c, short xpos, short ypos );
_point *iterateJuliaPoint( _image *image, _complex *c, short xpos, short ypos );
_point *iterateJuliaSinPoint( _image *image, _complex *c, short xpos, short ypos );
_point *iterateJuliaCosPoint( _image *image, _complex *c, short xpos, short ypos );
_point *iterateJuliaTanPoint( _image *image, _complex *c, short xpos, short ypos );
_point *iterateOtherPoint( _image *image, _complex *c, short xpos, short ypos );
_point *iterateMandelbrotPoint( _image *image, _complex *c, short xpos, short ypos );
_point *iterateMandelbrot3Point( _image *image, _complex *c, short xpos, short ypos );

#define _FRACTAL_H
#endif
