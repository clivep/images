#!/usr/bin/perl

use warnings;
use strict;

sub splitGroup {

    my $grp = shift;
    my $n = shift;

    if ( $n < 4 ) {
        push @{ $grp }, $n;
        return
    }

    if ( $n % 2 ) {
        splitGroup( $grp, int( ( $n - 1 ) / 2 ) );
        splitGroup( $grp, int( ( $n + 1 ) / 2 ) );
    }
    else {
        splitGroup( $grp, int( $n / 2 ) );
        splitGroup( $grp, int( $n / 2 ) );
    }
}

my $grp = [];
splitGroup( $grp, $ARGV[0] );

#use Data::Dumper;
#printf Dumper $grp;

printf "@$grp\n";
