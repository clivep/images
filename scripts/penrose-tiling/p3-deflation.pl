#!/usr/bin/perl

use warnings;
use strict;

use Math::Trig;
use File::Path qw(make_path);
use Getopt::Std;

use Data::Dumper;

############################################################
#
#   p3-deflation.pl
#
#   Model the deflation process for Penrose P3 tilings
#
############################################################

use constant    IMAGE_WIDTH                 =>  1000;
use constant    IMAGE_DEPTH                 =>  1000;

use constant    POLYGON_TYPE_BIG_RHOMB      =>  'big-rhomb';
use constant    POLYGON_TYPE_SML_RHOMB      =>  'sml-rhomb';
use constant    POLYGON_TYPE_HALF_BIG_RHOMB =>  'half-big-rhomb';
use constant    POLYGON_TYPE_HALF_SML_RHOMB =>  'half-sml-rhomb';

use constant    SIDE_TYPE_LONG_A1           =>  'long_side_a1'; # grn .75-.25
use constant    SIDE_TYPE_LONG_A2           =>  'long_side_a2'; # grn .25-.75
use constant    SIDE_TYPE_LONG_B1           =>  'long_side_b1'; # red .75-.25
use constant    SIDE_TYPE_LONG_B2           =>  'long_side_b2'; # red .25-.75
use constant    SIDE_TYPE_OPEN_A            =>  'open_side_a'; # half big rhomb
use constant    SIDE_TYPE_OPEN_B            =>  'open_side_b'; # half sml rhomb

use constant    ARC_TYPE_NONE               =>  'none';
use constant    ARC_TYPE_DEFAULT            =>  'arcs';

use constant    STATUS_READY                =>  'status_ready';
use constant    STATUS_JOINED               =>  'status_joined';
use constant    STATUS_DONE                 =>  'status_done';

use constant    DRAWING_TYPE_BORDER         =>  'border';
use constant    DRAWING_TYPE_FILL           =>  'fill';
use constant    DRAWING_TYPE_NORMAL         =>  'normal';

use constant    PHI                         =>  ( 1.0 + sqrt( 5.0 ) ) / 2.0;

use constant    TRUE                        =>  1;
use constant    FALSE                       =>  0;

############################################################
# Check the runline options

my %options;
getopts( 'a:Acd:D:fF:hjlk:mnopr:R:s:t:uW:x:y:', \%options );

help() if $options{ 'h' };

my $DRAW_ARC_TYPE = $options{ 'a' } || ARC_TYPE_DEFAULT;
my $DRAW_ARCS_ONLY = $options{ 'A' };
my $CONSOLIDATE = $options{ 'c' };
my $DEFLATIONS = $options{ 'd' } || 1;
my $FILL_POLYGONS = $options{ 'f' };
my $LABEL_POLYGONS = $options{ 'l' };
my $COMPLETE_OPEN = $options{ 'j' };
my $CENTRE_TEMPLATES = $options{ 'm' };
my $DRAW_OPEN_SIDES = $options{ 'o' };
my $DUMP_POLYGON_DATA = $options{ 'p' };
my $RECIPE = $options{ 'r' } || 'B';
my $DRAWING_SCALE = 50 * ( $options{ 's' } || 1 );
my $SHOW_LAST_UNUSED_SIDE = $options{ 'u' };
my $TEXT_MULTIPLIER = $options{ 't' } || 1.0;
my $PS_TEMP = $options{ 'k' } || 'penrose.ps';

my $X_OFFSET->[ 0 ] = $options{ 'x' } || 0;
my $Y_OFFSET->[ 1 ] = $options{ 'y' } || 0;

my $OUTPUT_FILE = $options{ 'F' } || 'deflation.pdf';
my $IMAGE_DEPTH = $options{ 'D' } || IMAGE_DEPTH;
my $IMAGE_WIDTH = $options{ 'W' } || IMAGE_WIDTH;

############################################################
# Setup the environment

$| = 1;

my $NEXT_POLYGON_ID = 1;

my $POLYGONS = [];
my $POLYGON_LOOKUP = {};

my $IMAGE_SIZE = [ $IMAGE_WIDTH, $IMAGE_DEPTH ];

my $TEMPLATE_BIG_RHOMB = setupBigRhombTemplate( $CENTRE_TEMPLATES );
my $TEMPLATE_HALF_BIG_RHOMB = setupHalfBigRhombTemplate( $CENTRE_TEMPLATES );
my $TEMPLATE_SML_RHOMB = setupSmallRhombTemplate( $CENTRE_TEMPLATES );
my $TEMPLATE_HALF_SML_RHOMB = setupHalfSmallRhombTemplate( $CENTRE_TEMPLATES );

loadRecipe( $options{ 'R' } ) if $options{ 'R' };

help( "Not a valid arc type" ) if ( 
    ( $DRAW_ARC_TYPE ne ARC_TYPE_DEFAULT ) &&
    ( $DRAW_ARC_TYPE ne ARC_TYPE_NONE  )
);

############################################################
# Construct the initial layout

my %initial_polygon;

for( my $p = 0; $p < length( $RECIPE ); $p++ ) {

    my $instruction = substr $RECIPE, $p, 1;

    if ( $p == 0 ) {

        transformTemplate( \%initial_polygon,
                                getPolygonType( $instruction ), 0, 0, 0 );

        $POLYGONS = [ \%initial_polygon ];
    }
    else {

        my $next_polygon = findFirstReadyPolygon();
        unless ( $next_polygon ) {
            printf "Could not find a READY polygon for recipe pos $p\n";
            exit -1;
        }

        my $next_side = findFirstReadyPolygonSide( $next_polygon );
        if ( $next_side < 0 ) {
            printf "Could not find a READY side for polygon at recipe pos $p\n";
            exit -1;
        }

        if ( $instruction eq '-' ) {

            $next_polygon->{ 'SIDES' }->[ $next_side ]->{ 'STATUS' } = STATUS_DONE;
            checkPolygonStatus( $next_polygon );
            next;
        }
        else  {

            push @{ $POLYGONS }, addPolygonToSide( $next_polygon, $next_side,
                                                getPolygonType( $instruction ) );
        }
    }
}

if ( $SHOW_LAST_UNUSED_SIDE ) {
    drawImage( DRAWING_TYPE_NORMAL );
    exit;
}

############################################################
# Run the deflation then image

if ( $DEFLATIONS > 0 ) {

    $POLYGONS = createHalfPolygonList();

    for ( my $i = 0; $i < $DEFLATIONS; $i++ ) {

        my $new_list = [];
        for my $polygon ( @{ $POLYGONS } ) {
     
            isHalfBigRhomb( $polygon ) ?
                    deflateHalfBigRhomb( $new_list, $polygon )
                    :
                    deflateHalfSmlRhomb( $new_list, $polygon )
            ;
        }
        $POLYGONS = $new_list;
    }
}

if ( $CONSOLIDATE ) {

    consolidateVertices();
    rejoinHalfPolygons( $COMPLETE_OPEN );
    completeOpenPolygons() if $COMPLETE_OPEN;
}

drawImage();

dumpPolygons() if $DUMP_POLYGON_DATA;

exit;

############################################################
# Setup the big rhomnus template

sub setupBigRhombTemplate {

    my $centre_polygon = shift;

    my $ang = 2.0 * pi / 10.0;

    my $template =  {
    
        'ANGLES'    =>  [ 2.0 * $ang, 3.0 * $ang, 2.0 * $ang, 3.0 * $ang ],
    
        'SIDES'     =>  [
                newSide( SIDE_TYPE_LONG_A2 ),
                newSide( SIDE_TYPE_LONG_B2 ),
                newSide( SIDE_TYPE_LONG_B1 ),
                newSide( SIDE_TYPE_LONG_A1 ),
        ],
        'VERTICES'  =>  [
                newVertex( -1.0 * cos( $ang ), 0.0 ),
                newVertex( 0.0, sin( $ang ) ),
                newVertex( cos( $ang ), 0.0 ),
                newVertex( 0.0, -1.0 * sin( $ang ) ),
        ],
    };

    unless ( $centre_polygon ) {
    
        for my $vertex ( @{ $template->{ 'VERTICES' } } ) {
            $vertex->{ 'X' } += cos( $ang / 2.0 );   
        }
    }

    return $template;
}
    
############################################################
# Setup the half big rhomnus template

sub setupHalfBigRhombTemplate {

    my $centre_polygon = shift;

    my $ang = 2.0 * pi / 10.0;

    my $template = {
    
        'ANGLES'    =>  [ $ang, 3.0 * $ang, $ang ],
    
        'SIDES'     =>  [
                newSide( SIDE_TYPE_LONG_A2 ),
                newSide( SIDE_TYPE_LONG_B2 ),
                newSide( SIDE_TYPE_OPEN_A ),
        ],
        'VERTICES'  =>  [
                newVertex( -1.0 * cos( $ang ), 0.0 ),
                newVertex( 0.0, sin( $ang ) ),
                newVertex( cos( $ang ), 0.0 ),
        ],
    };

    unless ( $centre_polygon ) {
    
        for my $vertex ( @{ $template->{ 'VERTICES' } } ) {
            $vertex->{ 'X' } += cos( $ang / 2.0 );   
        }
    }

}
    
############################################################
# Setup the small rhombus template

sub setupSmallRhombTemplate {

    my $centre_polygon = shift; # is the polygon to be centred?

    my $ang = 2.0 * pi / 10.0;
    
    my $template = {
    
        'ANGLES'    =>  [ $ang, 3.0 * $ang, $ang, 3.0 * $ang  ],
    
        'SIDES'     =>  [
                newSide( SIDE_TYPE_LONG_A1 ),
                newSide( SIDE_TYPE_LONG_A2 ),
                newSide( SIDE_TYPE_LONG_B1 ),
                newSide( SIDE_TYPE_LONG_B2 ),
        ],
        'VERTICES'  =>  [
                newVertex( -1.0 * cos( $ang / 2.0 ), 0.0 ),
                newVertex( 0.0, 1.0 * sin( $ang / 2.0 ) ),
                newVertex(cos( $ang / 2.0 ), 0.0 ),
                newVertex( 0.0, -1.0 * sin( $ang / 2.0 ) ),
        ],
    };

    unless ( $centre_polygon ) {
    
        for my $vertex ( @{ $template->{ 'VERTICES' } } ) {
            $vertex->{ 'X' } += cos( $ang / 2.0 );   
        }
    }

    return $template;
}

############################################################
# Setup the half small rhombus template

sub setupHalfSmallRhombTemplate {

    my $centre_polygon = shift; # is the polygon to be centred?

    my $ang = 2.0 * pi / 10.0;
    
    my $template = {
    
        'ANGLES'    =>  [ $ang / 2.0, 3.0 * $ang, $ang / 2.0 ],
    
        'SIDES'     =>  [
                newSide( SIDE_TYPE_LONG_A1 ),
                newSide( SIDE_TYPE_OPEN_B ),
                newSide( SIDE_TYPE_LONG_B2 ),
        ],
        'VERTICES'  =>  [
                newVertex( -1.0 * cos( $ang / 2.0 ), 0.0 ),
                newVertex( 0.0, 1.0 * sin( $ang / 2.0 ) ),
                newVertex( 0.0, -1.0 * sin( $ang / 2.0 ) ),
        ],
    };

    unless ( $centre_polygon ) {
    
        for my $vertex ( @{ $template->{ 'VERTICES' } } ) {
            $vertex->{ 'X' } += cos( $ang / 2.0 );   
        }
    }

    return $template;
}

############################################################
# Return the polygon type decsription

sub getPolygonDescription {

    my $polygon = shift;

    return isBigRhomb( $polygon ) ? 'Big Rhomb'
         : isSmallRhomb( $polygon ) ? 'Small Rhomb'
         : isHalfBigRhomb( $polygon ) ? 'Half Big Rhomb'
         : isHalfSmallRhomb( $polygon ) ? 'Half Small Rhomb'
         : ''
    ;
}

############################################################
# Find a point the given proporation between 2 points

sub pointAlong {

    my $vertex1 = shift;
    my $vertex2 = shift;
    my $val = shift;

    return newVertex( 
            ( ( 1.0 - $val ) * $vertex1->{ 'X' } ) + ( $val * $vertex2->{ 'X' } ),
            ( ( 1.0 - $val ) * $vertex1->{ 'Y' } ) + ( $val * $vertex2->{ 'Y' } ),
    );
}

############################################################
# Add the polygons' id to each vertex polygon list

sub addVertexPolygons {

    my $polygon = shift;

    for my $vertex ( @{ $polygon->{ 'VERTICES' } } ) {

        $vertex->{ 'POLYGONS' }->{ $polygon->{ 'ID' } } = 1;
    }

    return;
}

############################################################
# Remove the id from each vertex polygon list

sub delVertexPolygons {

    my $polygon = shift;
    my $id = shift;

    for my $vertex ( @{ $polygon->{ 'VERTICES' } } ) {

        delete $vertex->{ 'POLYGONS' }->{ $id };
    }

    return;
}

############################################################
# Complete anf remaining open polygons

sub completeOpenPolygons {

    my $new_list = [];

    for my $polygon ( @{ $POLYGONS } ) {

        if ( isBigRhomb( $polygon ) || isSmallRhomb( $polygon ) ) {
            push @{ $new_list }, $polygon;
            next;
        }

        my $vertices = $polygon->{ 'VERTICES' };

        my $open_side = isHalfBigRhomb( $polygon ) ? 2 : 1;
        my $midpoint = pointAlong( $vertices->[ $open_side ], $vertices->[ $open_side - 2 ], 0.5 );

        my $new_type = isHalfBigRhomb( $polygon ) ? POLYGON_TYPE_BIG_RHOMB : POLYGON_TYPE_SML_RHOMB;
        my $template = polygonTemplate( $new_type );

        my $new_vertex = pointAlong( $vertices->[ $open_side - 1 ], $midpoint, 2 );

        my $new_polygon = {
            'ID'        =>  $NEXT_POLYGON_ID++,
            'STATUS'    =>  STATUS_READY,
            'TYPE'      =>  $new_type,
            'SIDES'     =>  [
                                $template->{ 'SIDES' }->[ 0 ],
                                $template->{ 'SIDES' }->[ 1 ],
                                $template->{ 'SIDES' }->[ 2 ],
                                $template->{ 'SIDES' }->[ 3 ],
                            ],
            'VERTICES'  =>  
                            ( $new_type eq POLYGON_TYPE_BIG_RHOMB ) ? [
                                $vertices->[ 0 ],
                                $vertices->[ 1 ],
                                $vertices->[ 2 ],
                                $new_vertex,
                            ] : [
                                $vertices->[ 0 ],
                                $vertices->[ 1 ],
                                $new_vertex,
                                $vertices->[ 2 ],
                            ],

        };

        addVertexPolygons( $new_polygon );
        delVertexPolygons( $new_polygon, $polygon->{ 'ID' } );

        push @{ $new_list }, $new_polygon;
    }

    $POLYGONS = $new_list;
}

############################################################
# Join half polygons to for full ones

sub rejoinHalfPolygons {

    my $keep_open_polygons = shift;

    my $new_list = [];

    for my $polygon ( @{ $POLYGONS } ) {

        if ( isBigRhomb( $polygon ) || isSmallRhomb( $polygon ) ) {

            push @{ $new_list }, $polygon;
            next;
        }

        next if ( $polygon->{ 'STATUS' } eq STATUS_JOINED );

        # So this is an half polygon and there will only be one polygon common
        # to vertices of the open side

        my $open_side = isHalfBigRhomb( $polygon ) ? 2 : 1;

        for my $common_id1 ( keys %{ $polygon->{ 'VERTICES' }->[ $open_side ]->{ 'POLYGONS' } } ) {

            next if ( $common_id1 == $polygon->{ 'ID' } );

            for my $common_id2 ( keys %{ $polygon->{ 'VERTICES' }->[ $open_side - 2 ]->{ 'POLYGONS' } } ) {
                
                if ( $common_id1 == $common_id2 ) {

                    my $companion = $POLYGON_LOOKUP->{ $common_id1 };

                    my $new_type = isHalfBigRhomb( $polygon ) ? POLYGON_TYPE_BIG_RHOMB : POLYGON_TYPE_SML_RHOMB;

                    my $template = polygonTemplate( $new_type );

                    my $new_polygon = {
                        'ID'        =>  $NEXT_POLYGON_ID++,
                        'STATUS'    =>  STATUS_READY,
                        'TYPE'      =>  $new_type,
                        'SIDES'     =>  
                                ( $new_type eq POLYGON_TYPE_BIG_RHOMB ) ? [
                                    $polygon->{ 'SIDES' }->[ 0 ],
                                    $polygon->{ 'SIDES' }->[ 1 ],
                                    $companion->{ 'SIDES' }->[ 1 ],
                                    $companion->{ 'SIDES' }->[ 0 ],
                                ] : [
                                    $polygon->{ 'SIDES' }->[ 0 ],
                                    $polygon->{ 'SIDES' }->[ 1 ],
                                    $companion->{ 'SIDES' }->[ 0 ],
                                    $companion->{ 'SIDES' }->[ 1 ],
                                ],
                        'VERTICES'  => 
                                ( $new_type eq POLYGON_TYPE_BIG_RHOMB ) ? [
                                    $polygon->{ 'VERTICES' }->[ 0 ],
                                    $polygon->{ 'VERTICES' }->[ 1 ],
                                    $companion->{ 'VERTICES' }->[ 2 ],
                                    $companion->{ 'VERTICES' }->[ 1 ],
                                ] : [
                                    $polygon->{ 'VERTICES' }->[ 0 ],
                                    $polygon->{ 'VERTICES' }->[ 1 ],
                                    $companion->{ 'VERTICES' }->[ 0 ],
                                    $companion->{ 'VERTICES' }->[ 2 ],
                                ],
                    };

                    push @{ $new_list }, $new_polygon;

                    $companion->{ 'STATUS' } = STATUS_JOINED;
                    $polygon->{ 'STATUS' } = STATUS_JOINED;

                    last;
                }
            }
        }

        push @{ $new_list }, $polygon if ( ( $polygon->{ 'STATUS' } ne STATUS_JOINED ) && $keep_open_polygons );
    }

    $POLYGONS = $new_list;

    return;
}

############################################################
# Return a new side object

sub newSide {

    return {
        'TYPE'      =>  shift,
        'STATUS'    =>  STATUS_READY,
    };
}

############################################################
# Deflate a half Big Rhombus

sub deflateHalfBigRhomb {

    my $new_list = shift;
    my $polygon = shift;

    my $vertices = $polygon->{ 'VERTICES' };

    my $vertex1 = pointAlong( $vertices->[ 2 ], $vertices->[ 1 ], 1 / PHI );
    $vertex1->{ 'CHECK_ME' } = TRUE;

    my $vertex2 = pointAlong( $vertices->[ 2 ], $vertices->[ 0 ], 1 / PHI );
    $vertex2->{ 'CHECK_ME' } = TRUE;

    my $new_polygon = {
        'ID'        =>  $NEXT_POLYGON_ID++,
        'STATUS'    =>  STATUS_READY,
        'TYPE'      =>  POLYGON_TYPE_HALF_BIG_RHOMB,
        'SIDES'     =>  [
                newSide( SIDE_TYPE_LONG_A1 ),
                newSide( SIDE_TYPE_LONG_A2 ),
                newSide( SIDE_TYPE_OPEN_A ),
        ],
        'VERTICES'  =>  [
                $vertices->[ 1 ],
                $vertex2,
                $vertices->[ 0 ],
        ],
    };

    push @{ $new_list }, $new_polygon;
    addVertexPolygons( $new_polygon );
    delVertexPolygons( $new_polygon, $polygon->{ 'ID' } );

    $new_polygon = {
        'ID'        =>  $NEXT_POLYGON_ID++,
        'STATUS'    =>  STATUS_READY,
        'TYPE'      =>  POLYGON_TYPE_HALF_BIG_RHOMB,
        'SIDES'     =>  [
                newSide( SIDE_TYPE_LONG_A1 ),
                newSide( SIDE_TYPE_LONG_A2 ),
                newSide( SIDE_TYPE_OPEN_A ),
        ],
        'VERTICES'  =>  [
                $vertices->[ 2 ],
                $vertex1,
                $vertex2,
        ],
    };

    push @{ $new_list }, $new_polygon;
    addVertexPolygons( $new_polygon );
    delVertexPolygons( $new_polygon, $polygon->{ 'ID' } );

    $new_polygon = {
        'ID'        =>  $NEXT_POLYGON_ID++,
        'STATUS'    =>  STATUS_READY,
        'TYPE'      =>  POLYGON_TYPE_HALF_SML_RHOMB,
        'SIDES'     =>  [
                newSide( SIDE_TYPE_LONG_B2 ),
                newSide( SIDE_TYPE_OPEN_B ),
                newSide( SIDE_TYPE_LONG_A2 ),
        ],
        'VERTICES'  =>  [
                $vertex2,
                $vertex1,
                $vertices->[ 1 ],
        ],
    };

    push @{ $new_list }, $new_polygon;
    addVertexPolygons( $new_polygon );
    delVertexPolygons( $new_polygon, $polygon->{ 'ID' } );
}

############################################################
# Deflate a half Small Rhombus

sub deflateHalfSmlRhomb {

    my $new_list = shift;
    my $polygon = shift;

    my $vertices = $polygon->{ 'VERTICES' };

    my $vertex1 = pointAlong( $vertices->[ 1 ], $vertices->[ 0 ], 1 / ( PHI * PHI ) );
    $vertex1->{ 'CHECK_ME' } = TRUE;

    my $new_polygon = {
        'ID'        =>  $NEXT_POLYGON_ID++,
        'STATUS'    =>  STATUS_READY,
        'TYPE'      =>  POLYGON_TYPE_HALF_BIG_RHOMB,
        'SIDES'     =>  [
                newSide( SIDE_TYPE_LONG_A1 ),
                newSide( SIDE_TYPE_LONG_B1 ),
                newSide( SIDE_TYPE_OPEN_A ),
        ],
        'VERTICES'  =>  [
                $vertices->[ 0 ],
                $vertex1,
                $vertices->[ 2 ],
        ],
    };

    push @{ $new_list }, $new_polygon;
    addVertexPolygons( $new_polygon );
    delVertexPolygons( $new_polygon, $polygon->{ 'ID' } );

    $new_polygon = {
        'ID'        =>  $NEXT_POLYGON_ID++,
        'STATUS'    =>  STATUS_READY,
        'TYPE'      =>  POLYGON_TYPE_HALF_SML_RHOMB,
        'SIDES'     =>  [
                newSide( SIDE_TYPE_LONG_A2 ),
                newSide( SIDE_TYPE_OPEN_B ),
                newSide( SIDE_TYPE_LONG_B2 ),
        ],
        'VERTICES'  =>  [
                $vertices->[ 2 ],
                $vertex1,
                $vertices->[ 1 ],
        ],
    };

    push @{ $new_list }, $new_polygon;
    addVertexPolygons( $new_polygon );
    delVertexPolygons( $new_polygon, $polygon->{ 'ID' } );

    return;
}

############################################################
# Convert any full polygon into 2 half ones

sub createHalfPolygonList {

    my $half_list = [];

    for my $polygon ( @{ $POLYGONS} ) {
        
        if ( isBigRhomb( $polygon ) || isSmallRhomb( $polygon ) ) {

            if ( isBigRhomb( $polygon ) ) {

                my $new_polygon = {
                    'ID'        => $NEXT_POLYGON_ID++,
                    'STATUS'    => STATUS_READY,
                    'TYPE'      => POLYGON_TYPE_HALF_BIG_RHOMB,
                    'SIDES'     => [
                                    $polygon->{ 'SIDES' }->[ 0 ],
                                    $polygon->{ 'SIDES' }->[ 1 ],
                                    { 
                                        'TYPE'      => SIDE_TYPE_OPEN_A,
                                        'STATUS'    => STATUS_READY 
                                    },
                                ],
                    'VERTICES'  => [
                                    $polygon->{ 'VERTICES' }->[ 0 ],
                                    $polygon->{ 'VERTICES' }->[ 1 ],
                                    $polygon->{ 'VERTICES' }->[ 2 ],
                                ],
                };
    
                push @{ $half_list }, $new_polygon;
                addVertexPolygons( $new_polygon );
                delVertexPolygons( $new_polygon, $polygon->{ 'ID' } );

                $new_polygon = {
                    'ID'        => $NEXT_POLYGON_ID++,
                    'STATUS'    => STATUS_READY,
                    'TYPE'      => POLYGON_TYPE_HALF_BIG_RHOMB,
                    'SIDES'     => [
                                    $polygon->{ 'SIDES' }->[ 0 ],
                                    $polygon->{ 'SIDES' }->[ 3 ],
                                    { 
                                        'TYPE'      => SIDE_TYPE_OPEN_A,
                                        'STATUS'    => STATUS_READY 
                                    },
                                ],
                    'VERTICES'  => [
                                    $polygon->{ 'VERTICES' }->[ 0 ],
                                    $polygon->{ 'VERTICES' }->[ 3 ],
                                    $polygon->{ 'VERTICES' }->[ 2 ],
                                ],
                };

                push @{ $half_list }, $new_polygon;
                addVertexPolygons( $new_polygon );
                delVertexPolygons( $new_polygon, $polygon->{ 'ID' } );
            }
            else {

                my $new_polygon = {
                    'ID'        => $NEXT_POLYGON_ID++,
                    'STATUS'    => STATUS_READY,
                    'TYPE'      => POLYGON_TYPE_HALF_SML_RHOMB,
                    'SIDES'     => [
                                    $polygon->{ 'SIDES' }->[ 0 ],
                                    { 
                                        'TYPE'      => SIDE_TYPE_OPEN_B,
                                        'STATUS'    => STATUS_READY 
                                    },
                                    $polygon->{ 'SIDES' }->[ 3 ],
                                ],
                    'VERTICES'  => [
                                    $polygon->{ 'VERTICES' }->[ 0 ],
                                    $polygon->{ 'VERTICES' }->[ 1 ],
                                    $polygon->{ 'VERTICES' }->[ 3 ],
                                ],
                };

                push @{ $half_list }, $new_polygon;
                addVertexPolygons( $new_polygon );
                delVertexPolygons( $new_polygon, $polygon->{ 'ID' } );

                $new_polygon = {
                    'ID'        => $NEXT_POLYGON_ID++,
                    'STATUS'    => STATUS_READY,
                    'TYPE'      => POLYGON_TYPE_HALF_SML_RHOMB,
                    'SIDES'     => [
                                    $polygon->{ 'SIDES' }->[ 1 ],
                                    { 
                                        'TYPE'      => SIDE_TYPE_OPEN_B,
                                        'STATUS'    => STATUS_READY 
                                    },
                                    $polygon->{ 'SIDES' }->[ 2 ],
                                ],
                    'VERTICES'  => [
                                    $polygon->{ 'VERTICES' }->[ 2 ],
                                    $polygon->{ 'VERTICES' }->[ 1 ],
                                    $polygon->{ 'VERTICES' }->[ 3 ],
                                ],
                };

                push @{ $half_list }, $new_polygon;
                addVertexPolygons( $new_polygon );
                delVertexPolygons( $new_polygon, $polygon->{ 'ID' } );
            }
        }
        else {  # must already be an half polygon
        
            push @{ $half_list }, $polygon;
        }
    }

    return $half_list;
}

############################################################
# Simple mod/min/max functions

sub min {

    my ( $v1, $v2 ) = @_;

    return ( $v1 < $v2 ) ? $v1 : $v2;
}

sub max {

    my ( $v1, $v2 ) = @_;

    return ( $v1 > $v2 ) ? $v1 : $v2;
}

sub mod {

    my $v = shift;

    return ( $v > 0 ) ? $v : ( -1 * $v );
}

sub isBigRhomb { return shift->{ 'TYPE' } eq POLYGON_TYPE_BIG_RHOMB }
sub isSmallRhomb { return shift->{ 'TYPE' } eq POLYGON_TYPE_SML_RHOMB }
sub isHalfBigRhomb { return shift->{ 'TYPE' } eq POLYGON_TYPE_HALF_BIG_RHOMB }
sub isHalfSmallRhomb { return shift->{ 'TYPE' } eq POLYGON_TYPE_HALF_SML_RHOMB }

sub getPolygonType {
    
    my $type = shift;

    return ( $type eq 'B' ) ? POLYGON_TYPE_BIG_RHOMB
        :  ( $type eq 'S' ) ? POLYGON_TYPE_SML_RHOMB
        :  ( $type eq 'b' ) ? POLYGON_TYPE_HALF_BIG_RHOMB
        :                     POLYGON_TYPE_HALF_SML_RHOMB
    ;
}

############################################################
# Find the first polygon in a READY state

sub findFirstReadyPolygon {

    foreach ( @{ $POLYGONS } ) {

        return $_ if ( $_->{ 'STATUS' } eq STATUS_READY );
    }

    return;
}

############################################################
# Find the first polygon side in a READY state

sub findFirstReadyPolygonSide{

    my $polygon = shift;

    return -1 unless $polygon;

    my $sides = $polygon->{ 'SIDES' };

    for( my $s = 0; $s < scalar( @{$sides} ); $s++ ) {

        return $s if ( $sides->[ $s ]->{ 'STATUS' } eq STATUS_READY );
    }

    return -1;
}

############################################################
# Find the sum of internal angles to the given side

sub findPolygonSideAngle {

    my $polygon_type = shift;
    my $side_no = shift;

    my $polygon = polygonTemplate( $polygon_type );

    my $vertices = $polygon->{ 'VERTICES' };

    return atan2(
                $vertices->[ $side_no - 3 ]->{ 'Y' } - $vertices->[ $side_no ]->{ 'Y' },
                $vertices->[ $side_no - 3 ]->{ 'X' } - $vertices->[ $side_no ]->{ 'X' }
    );
}

############################################################
# Find the side type to match that given

sub findMatchingSideType {

    my $side_type = shift;

    return ( $side_type eq SIDE_TYPE_LONG_A1 )   ? SIDE_TYPE_LONG_A2
         : ( $side_type eq SIDE_TYPE_LONG_A2 )   ? SIDE_TYPE_LONG_A1
         : ( $side_type eq SIDE_TYPE_LONG_B1 )   ? SIDE_TYPE_LONG_B2
         : ( $side_type eq SIDE_TYPE_LONG_B2 )   ? SIDE_TYPE_LONG_B1
         :                                         $side_type
    ;
}

############################################################
# Find the side to match that given on the polygon type

sub findSideMatch {

    my $polygon_type = shift;
    my $side_type = shift;

    my $match_type = findMatchingSideType( $side_type );
    return -1 unless $match_type;

    my $template = polygonTemplate( $polygon_type );

    for( my $s = 0; $s < scalar( @{$template->{ 'SIDES' }} ); $s++ ) {

        my $side = $template->{ 'SIDES' }->[ $s ];
        return $s if ( $side->{ 'TYPE' } eq $match_type );
    }

    return -1;
}

############################################################
# Consolidate vertices

sub consolidateVertices {

    my $max_dist = 0.00001;

    my $count = scalar @{ $POLYGONS };
    my $approx_checks = $count * ( $count + 1 ) / 2.0;

    printf "Consolidating vertices\n";

    my $c = 0;
    for ( my $p1 = 0; $p1 < $count; $p1++ ) {
     
        printf "Progress: %0.2f   \r", 100 * $c / $approx_checks;

        my $polygon1 = $POLYGONS->[ $p1 ];
        $POLYGON_LOOKUP->{ $polygon1->{ 'ID' } } = $polygon1;
    
        my $vertices1 = $polygon1->{ 'VERTICES' };

        for ( @{ $vertices1 } ) {
            $_->{ 'POLYGONS' }->{ $polygon1->{ 'ID' } } = 1;
        };

        for ( my $p2 = $p1 + 1; $p2 < $count; $p2++ ) {

            $c++;

            my $polygon2 = $POLYGONS->[ $p2 ];

            next if ( $polygon1 == $polygon2 );

            my $vertices2 = $polygon2->{ 'VERTICES' };

            for ( my $v1 = 0; $v1 < scalar @{ $vertices1 }; $v1++ ) {

                next unless $vertices1->[ $v1 ]->{ 'CHECK_ME' };
            
                my $dist = distance( $vertices1->[ 0 ], $vertices2->[ 0 ] );
                next if $dist > 6;

                for ( my $v2 = 0; $v2 < scalar @{ $vertices2 }; $v2++ ) {
            
                    next unless $vertices2->[ $v2 ]->{ 'CHECK_ME' };
                    next if ( $vertices1->[ $v1 ] == $vertices2->[ $v2 ] );

                    my $d = distance( $vertices1->[ $v1 ], $vertices2->[ $v2 ] );
                    if ( $d < $max_dist ) {

                        $vertices2->[ $v2 ] = $vertices1->[ $v1 ];
                        $vertices2->[ $v2 ]->{ 'POLYGONS' }->{ $polygon2->{ 'ID' } } = 1;
                    }
                }
            }
        }
    }

    printf "Done            \n";

    return;
}

############################################################
# Add a polygon to an existing polygon side

sub addPolygonToSide {

    my $polygon = shift;
    my $s = shift;
    my $next_type = shift;

    my $polygon_type = $polygon->{ 'TYPE' };
    my $vertices = $polygon->{ 'VERTICES' };
    my $side = $polygon->{ 'SIDES' }->[ $s ];

    my $side_number = findSideMatch( $next_type, $side->{ 'TYPE' } );

    my $side_ang = atan2(
                $vertices->[ $s - 3 ]->{ 'Y' } - $vertices->[ $s ]->{ 'Y' },
                $vertices->[ $s - 3 ]->{ 'X' } - $vertices->[ $s ]->{ 'X' }
    );
    
    # How far round to rotate the polygon to align the given side
    my $polygon_ang = pi
                        + $side_ang 
                        - findPolygonSideAngle( $next_type, $side_number );
    
    my %new_polygon;
    transformTemplate( \%new_polygon, $next_type, 
                                            $vertices->[ $s ]->{ 'X' },
                                            $vertices->[ $s ]->{ 'Y' },
                                            $polygon_ang );

    my $new_vertices = $new_polygon{ 'VERTICES' };
    my $x_offset = $new_vertices->[ ( $side_number + 1 ) % 4 ]->{ 'X' }
                                - $vertices->[ $s ]->{ 'X' };
    my $y_offset = $new_vertices->[ ( $side_number + 1 ) % 4 ]->{ 'Y' }
                                - $vertices->[ $s ]->{ 'Y' }; 

    for ( @{ $new_vertices } ) {
        $_->{ 'X' } -= $x_offset;
        $_->{ 'Y' } -= $y_offset;
    }

    $side->{ 'STATUS' } = STATUS_DONE;
    $new_polygon{ 'SIDES' }->[ $side_number ]->{ 'STATUS' } = STATUS_DONE;

    checkPolygonStatus( $polygon );

    return \%new_polygon;   
}

############################################################
# Check polygon status

sub checkPolygonStatus {

    my $polygon = shift;

    for ( @{ $polygon->{ 'SIDES' } } ) {

        if ( $_->{ 'STATUS' } eq STATUS_READY ) {

            $polygon->{ 'STATUS' } = STATUS_READY;
            return;
        }

    }

    $polygon->{ 'STATUS' } = STATUS_DONE;

    return;
}

############################################################
# Find the distance between 2 vertices

sub distance {

    my $vtx1 = shift;
    my $vtx2 = shift;

    my $dx = $vtx1->{ 'X' } - $vtx2->{ 'X' };
    my $dy = $vtx1->{ 'Y' } - $vtx2->{ 'Y' };

    return sqrt( ( $dx * $dx ) +  ( $dy * $dy ) );
}

############################################################
# Translate, rotate and scale a vertex

sub transformVertex {

    my $vertex = shift;
    my $offset_x = shift;
    my $offset_y = shift;
    my $rotate = shift;

    my $ang = atan2( $vertex->{ 'Y' },  $vertex->{ 'X' } );
    my $dist = distance( { 'X' => 0.0, 'Y' => 0.0 }, $vertex );

    $ang += $rotate;

    $vertex->{ 'X' } = $dist * cos( $ang ) + $offset_x;
    $vertex->{ 'Y' } = $dist * sin( $ang ) + $offset_y;

    return;
}

############################################################
# Return the template for the given polygon type

sub polygonTemplate {

    my $type = shift;

    return
            ( $type eq POLYGON_TYPE_BIG_RHOMB ) ?       $TEMPLATE_BIG_RHOMB
          : ( $type eq POLYGON_TYPE_SML_RHOMB ) ?       $TEMPLATE_SML_RHOMB
          : ( $type eq POLYGON_TYPE_HALF_BIG_RHOMB ) ?  $TEMPLATE_HALF_BIG_RHOMB
          :                                             $TEMPLATE_HALF_SML_RHOMB
    ;
}

############################################################
# Write the image header

sub writeImageHeader {

    my $fh = shift;

    printf $fh "%%!PS\n%%%%BoundingBox: 0 0 %f %f\n%%%%EndComments

        <<
            /PageSize [ %f %f ]
            /ImagingBBox null
        >> setpagedevice

        /mv { moveto } bind def
        /ln { lineto } bind def

        /drawArc {
            /a1 exch def
            /a2 exch def
            /rad exch def
            /yc exch def
            /xc exch def
    
            xc a1 cos rad mul add
            yc a1 sin rad mul add
            moveto
            xc yc rad a1 a2 arcn
        } bind def

        0 0 mv %f 0 ln %f %f ln 0 %f ln fill

        /Arial-Bold findfont 12 scalefont setfont 

        1 setlinejoin
        %f setlinewidth

        %f %f translate
    ",
        $IMAGE_WIDTH, $IMAGE_DEPTH,
        $IMAGE_SIZE->[ 0 ], $IMAGE_SIZE->[ 1 ],
        $IMAGE_WIDTH, $IMAGE_WIDTH,
        $IMAGE_DEPTH, $IMAGE_DEPTH,
        ( $DEFLATIONS > 0 ) ? 1 / ( PHI ** ( $DEFLATIONS - 1 ) ) : 1,
        $IMAGE_SIZE->[ 0 ] / 2.0 + $X_OFFSET->[ 0 ], 
        $IMAGE_SIZE->[ 1 ] / 2.0 + $Y_OFFSET->[ 1 ],
    ;

    return;
}

############################################################
# Draw the image

sub drawImage {

    my $drawing_type = shift;

    open my $fh, '>', $PS_TEMP or die "Could not open $PS_TEMP - $!\n";

    writeImageHeader( $fh );

    if ( $FILL_POLYGONS ) {

        drawPolygons( $fh, DRAWING_TYPE_FILL );
        drawPolygons( $fh, DRAWING_TYPE_BORDER );
    }
    else {

        drawPolygons( $fh, DRAWING_TYPE_NORMAL );
    }

    if ( $SHOW_LAST_UNUSED_SIDE ) {
    
        my $last_polygon = findFirstReadyPolygon();
        my $last_side = findFirstReadyPolygonSide( $last_polygon );

        if ( $last_side > -1 ) {

            my $vertex1 = $last_polygon->{ 'VERTICES' }->[ $last_side ];

            my $next_side = ( $last_side + 1 ) % scalar @{ $last_polygon->{ 'VERTICES' } };
            my $vertex2 = $last_polygon->{ 'VERTICES' }->[ $next_side ];

            printf $fh "gsave
                    3 setlinewidth
                    0 1 1 setrgbcolor 
                    %f %f mv 
                    %f %f ln 
                    stroke 
                    grestore\n",
                        $DRAWING_SCALE * $vertex1->{ 'X' },
                        $DRAWING_SCALE * $vertex1->{ 'Y' },
                        $DRAWING_SCALE * $vertex2->{ 'X' },
                        $DRAWING_SCALE * $vertex2->{ 'Y' };
                        
        }
    }

    close $fh;

    my $convert = "/usr/local/bin/convert";
    $convert = "/usr/bin/convert" unless ( -f $convert );
    $convert = "/Applications/ImageMagick/ImageMagick-6.6.5/bin/convert" unless ( -f $convert );

    unless ( -f $convert ) {

        printf "Count not locate convert at $convert\n";
    }
    else {

        system( "$convert $PS_TEMP $OUTPUT_FILE" );
        unlink $PS_TEMP unless $options{ 'k' };
    }

    return;
}

############################################################
# Draw the polygons

sub drawPolygons {

    my $fh = shift;
    my $drawing_type = shift;

    for my $polygon ( @{$POLYGONS} ) {

        isBigRhomb( $polygon )      ? drawBigRhomb( $fh, $drawing_type, $polygon ) :
        isSmallRhomb( $polygon )    ? drawSmallRhomb( $fh, $drawing_type, $polygon ) :
        isHalfBigRhomb( $polygon )  ? drawHalfBigRhomb( $fh, $drawing_type, $polygon ) :
                                      drawHalfSmallRhomb( $fh, $drawing_type, $polygon )
        ;
    }

    return;
}

############################################################
# Return a new vertex object

sub newVertex {

   return {
        'X'         =>  shift,
        'Y'         =>  shift,
    };
}

############################################################
# Translate, rotate and scale a copy of the template polygon

sub transformTemplate {

    my $polygon = shift;
    my $type = shift;
    my $offset_x = shift;
    my $offset_y = shift;
    my $rotate = shift;

    my $template = polygonTemplate( $type );

    $polygon->{ 'ID' } = $NEXT_POLYGON_ID++;
    $polygon->{ 'STATUS' } = STATUS_READY;
    $polygon->{ 'TYPE' } = $type;
    $polygon->{ 'VERTICES' } = [];
    $polygon->{ 'ANGLES' } = [ @{ $template->{ 'ANGLES' } } ];
    $polygon->{ 'SIDES' } = [ map { my %h = %{ $_ }; \%h } ( @{ $template->{ 'SIDES' } } ) ];

    for my $vertex ( @{ $template->{ 'VERTICES' } } ) {

        my %vertex_copy = %{ $vertex };
    
        transformVertex( \%vertex_copy, $offset_x, $offset_y, $rotate );

        push @{ $polygon->{ 'VERTICES' } }, \%vertex_copy;
    }

    addVertexPolygons( $polygon );

    return $polygon;
}

############################################################
# Return the polygons' centre point

sub findPolygonCentre {

    my $polygon = shift;

    my $vertices = $polygon->{ 'VERTICES' };
    my $cx = 0;
    my $cy = 0;

    for ( @{ $vertices } ) {
        $cx += $_->{ 'X' };
        $cy += $_->{ 'Y' };
    }

    return newVertex(
        $cx / scalar( @{ $vertices } ),
        $cy / scalar( @{ $vertices } ),
    );
}

############################################################
# Label the given polygon

sub labelPolygon {

    my $fh = shift;
    my $polygon = shift;

    my $centre = findPolygonCentre( $polygon );

    my $font_sz = 10;
    printf $fh "/Arial-Bold findfont %f scalefont setfont\n", $font_sz * $TEXT_MULTIPLIER;

    printf $fh "gsave 
        %f %f translate
        (%s) dup stringwidth pop -2 div -3 moveto
        1 setgray
        show
        grestore\n",
                $DRAWING_SCALE * $centre->{ 'X' },
                $DRAWING_SCALE * $centre->{ 'Y' },
                $polygon->{ 'ID' },
    ; 

    my $vertices = $polygon->{ 'VERTICES' };
    for ( my $v = 0; $v < scalar @{ $vertices }; $v++ ) {

        printf $fh "gsave 
            %f %f translate
            (%s) dup stringwidth pop -2 div %f moveto
            .8 setgray
            show
            grestore\n",
                $DRAWING_SCALE * ( $centre->{ 'X' } + 3 * $vertices->[ $v ]->{ 'X' } ) / 4.0,
                $DRAWING_SCALE * ( $centre->{ 'Y' } + 3 * $vertices->[ $v ]->{ 'Y' } ) / 4.0,
                $v,
                $font_sz / -3,
    } 
    return;
}

############################################################
# Draw an arc segment

sub drawArc {

    my $fh = shift;
    my $vertex = shift;
    my $point1 = shift;
    my $point2 = shift;
    my $dist = shift;
    my $obtuse = shift;

    my $ang1 = rad2deg( atan2(
                $point1->{ 'Y' } - $vertex->{ 'Y' },
                $point1->{ 'X' } - $vertex->{ 'X' }
    ) ) % 360; 

    my $ang2 = rad2deg( atan2(
                $point2->{ 'Y' } - $vertex->{ 'Y' },
                $point2->{ 'X' } - $vertex->{ 'X' }
    ) ) % 360; 

    my $from = min( $ang1, $ang2 );
    my $to = max( $ang1, $ang2 );

    if ( 
        ( ! $obtuse && ( ( $to - $from ) > 180 ) ) ||
        ( $obtuse && ( ( $to - $from ) < 180 ) )
    ) {
        my $tmp = $from;
        $from = $to;
        $to = $tmp;
    }

    printf $fh "%f %f %f %f %f drawArc stroke\n",
                                        $DRAWING_SCALE * $vertex->{ 'X' },
                                        $DRAWING_SCALE * $vertex->{ 'Y' },
                                        $DRAWING_SCALE * $dist,
                                        $from, $to
    ;

    return;
}

############################################################
# Draw a big rhomb

sub drawBigRhomb {

    my $fh = shift;
    my $drawing_type = shift;
    my $polygon = shift;

    my $vertices = $polygon->{ 'VERTICES' };

    if ( $drawing_type eq DRAWING_TYPE_NORMAL ) {

        if ( $DRAW_ARC_TYPE eq ARC_TYPE_DEFAULT ) {

            printf $fh "0 1 0 setrgbcolor\n",;
            drawArc( $fh, $vertices->[ 0 ], $vertices->[ 1 ], $vertices->[ 3 ],
                        distance( $vertices->[ 0 ], $vertices->[ 1 ] ) * 0.25 );
     
            printf $fh "1 0 0 setrgbcolor\n"; 
            drawArc( $fh, $vertices->[ 2 ], $vertices->[ 1 ], $vertices->[ 3 ],
                        distance( $vertices->[ 1 ], $vertices->[ 2 ] ) * 0.75 );
        }

        unless ( $DRAW_ARCS_ONLY ) {

            printf $fh "1 1 0 setrgbcolor\n";
            for ( my $v = 0; $v < scalar( @{$vertices} ); $v++ ) {
    
                printf $fh "%f %f %s ", 
                        $DRAWING_SCALE * $vertices->[ $v ]->{ 'X' },
                             $DRAWING_SCALE * $vertices->[ $v ]->{ 'Y' },
                            ( $v == 0 ) ? 'mv' : 'ln'
                ;
            }
    
            printf $fh "closepath stroke\n";
        }

        labelPolygon( $fh, $polygon ) if $LABEL_POLYGONS;
    }
    else {

        for ( my $v = 0; $v < 4; $v++ ) {

            printf $fh "%f %f %s ",
                        $DRAWING_SCALE * $vertices->[ $v ]->{ 'X' },
                        $DRAWING_SCALE * $vertices->[ $v ]->{ 'Y' },
                        ( $v == 0 ) ? 'mv' : 'ln'
            ;
        }

        printf $fh "closepath %s\n", ( $drawing_type eq DRAWING_TYPE_FILL ) ?
                                 "1 1 0 setrgbcolor fill" : "0 setgray stroke";
    }

    return;
}

############################################################
# Draw a small rhomb

sub drawSmallRhomb {

    my $fh = shift;
    my $drawing_type = shift;
    my $polygon = shift;

    my $vertices = $polygon->{ 'VERTICES' };

    if ( $drawing_type eq DRAWING_TYPE_NORMAL ) {

        if ( $DRAW_ARC_TYPE eq ARC_TYPE_DEFAULT ) {
        
            printf $fh "0 1 0 setrgbcolor\n",;
            drawArc( $fh, $vertices->[ 1 ], $vertices->[ 0 ], $vertices->[ 2 ],
                            distance( $vertices->[ 1 ], $vertices->[ 2 ] ) * 0.25 );
    
            printf $fh "1 0 0 setrgbcolor\n"; 
            drawArc( $fh, $vertices->[ 3 ], $vertices->[ 2 ], $vertices->[ 0 ],
                            distance( $vertices->[ 3 ], $vertices->[ 0 ] ) * 0.25 );
        }

        unless ( $DRAW_ARCS_ONLY ) {

            printf $fh "1 1 0 setrgbcolor\n";
            for ( my $v = 0; $v < scalar( @{$vertices} ); $v++ ) {
    
                printf $fh "%f %f %s ", 
                            $DRAWING_SCALE * $vertices->[ $v ]->{ 'X' },
                            $DRAWING_SCALE * $vertices->[ $v ]->{ 'Y' },
                            ( $v == 0 ) ? 'mv' : 'ln'
                ;
            }
    
            printf $fh "closepath stroke\n";
        }

        labelPolygon( $fh, $polygon ) if $LABEL_POLYGONS;
    }
    else {

        for ( my $v = 0; $v < 4; $v++ ) {

            printf $fh "%f %f %s ",
                        $DRAWING_SCALE * $vertices->[ $v ]->{ 'X' },
                        $DRAWING_SCALE * $vertices->[ $v ]->{ 'Y' },
                        ( $v == 0 ) ? 'mv' : 'ln'
            ;
        }

        printf $fh "closepath %s\n", ( $drawing_type eq DRAWING_TYPE_FILL ) ? "1 0 0 setrgbcolor fill" : "0 setgray stroke";
    }

    return;
}

############################################################
# Draw an half big rhomb

sub drawHalfBigRhomb {

    my $fh = shift;
    my $drawing_type = shift;
    my $polygon = shift;

    my $vertices = $polygon->{ 'VERTICES' };

    if ( $drawing_type eq DRAWING_TYPE_NORMAL ) {

        if ( $DRAW_ARC_TYPE eq ARC_TYPE_DEFAULT ) {

            printf $fh "0 1 0 setrgbcolor\n"; 
            drawArc( $fh, $vertices->[ 0 ], $vertices->[ 1 ], $vertices->[ 2 ],
                            distance( $vertices->[ 0 ], $vertices->[ 1 ] ) * 0.25 );
    
            printf $fh "1 0 0 setrgbcolor\n",;
            drawArc( $fh, $vertices->[ 2 ], $vertices->[ 0 ], $vertices->[ 1 ],
                            distance( $vertices->[ 0 ], $vertices->[ 1 ] ) * 0.75 );
        }
    
        unless ( $DRAW_ARCS_ONLY ) {

            if ( $DRAW_OPEN_SIDES ) {
    
                printf $fh "gsave 0 0 1 setrgbcolor %f %f mv %f %f ln stroke grestore\n",
                            $DRAWING_SCALE * $vertices->[ 0 ]->{ 'X' },
                            $DRAWING_SCALE * $vertices->[ 0 ]->{ 'Y' },
                            $DRAWING_SCALE * $vertices->[ 2 ]->{ 'X' },
                            $DRAWING_SCALE * $vertices->[ 2 ]->{ 'Y' }
                ;
            }
    
            printf $fh "1 1 0 setrgbcolor\n";
            for ( my $v = 0; $v < 3; $v++ ) {
    
                printf $fh "%f %f %s ", 
                            $DRAWING_SCALE * $vertices->[ $v ]->{ 'X' },
                            $DRAWING_SCALE * $vertices->[ $v ]->{ 'Y' },
                            ( $v == 0 ) ? 'mv' : 'ln'
                ;
            }
    
            printf $fh "stroke\n";
        }

        labelPolygon( $fh, $polygon ) if $LABEL_POLYGONS;
    }
    else {

        for ( my $v = 0; $v < 3; $v++ ) {

            printf $fh "%f %f %s ",
                        $DRAWING_SCALE * $vertices->[ $v ]->{ 'X' },
                        $DRAWING_SCALE * $vertices->[ $v ]->{ 'Y' },
                        ( $v == 0 ) ? 'mv' : 'ln'
            ;
        }

        if ( $drawing_type eq DRAWING_TYPE_FILL ) {
            
            printf $fh "1 1 0 setrgbcolor gsave closepath stroke grestore fill\n";
        }
        else {

            printf $fh "%s 0 setgray stroke\n", $DRAW_OPEN_SIDES ? 'closepath' : '' ;
        }
    }

    return;
}

############################################################
# Draw an half small rhomb

sub drawHalfSmallRhomb {

    my $fh = shift;
    my $drawing_type = shift;
    my $polygon = shift;

    my $vertices = $polygon->{ 'VERTICES' };

    if ( $drawing_type eq DRAWING_TYPE_NORMAL ) {

        if ( $DRAW_ARC_TYPE eq ARC_TYPE_DEFAULT ) {

            printf $fh "1 0 0 setrgbcolor\n",;
            drawArc( $fh, $vertices->[ 1 ], $vertices->[ 0 ], $vertices->[ 2 ],
                                distance( $vertices->[ 0 ], $vertices->[ 1 ] ) * 0.25 );
    
            printf $fh "0 1 0 setrgbcolor\n"; 
            drawArc( $fh, $vertices->[ 2 ], $vertices->[ 0 ], $vertices->[ 1 ],
                                distance( $vertices->[ 0 ], $vertices->[ 1 ] ) * 0.25 );
        }

        unless ( $DRAW_ARCS_ONLY ) {

            if ( $DRAW_OPEN_SIDES ) {
    
                printf $fh "gsave 0 0 1 setrgbcolor %f %f mv %f %f ln stroke grestore\n",
                            $DRAWING_SCALE * $vertices->[ 1 ]->{ 'X' },
                            $DRAWING_SCALE * $vertices->[ 1 ]->{ 'Y' },
                            $DRAWING_SCALE * $vertices->[ 2 ]->{ 'X' },
                            $DRAWING_SCALE * $vertices->[ 2 ]->{ 'Y' }
                ;
            }
    
            printf $fh "1 1 0 setrgbcolor\n";
            for ( my $v = -1; $v < 2; $v++ ) {
    
                printf $fh "%f %f %s ", 
                            $DRAWING_SCALE * $vertices->[ $v ]->{ 'X' },
                            $DRAWING_SCALE * $vertices->[ $v ]->{ 'Y' },
                            ( $v == -1 ) ? 'mv' : 'ln'
                ;
            }
    
            printf $fh "stroke\n";
        }
    
        labelPolygon( $fh, $polygon ) if $LABEL_POLYGONS;
    }
    else {

        for ( my $v = 0; $v < 3; $v++ ) {
    
            printf $fh "%f %f %s ", 
                        $DRAWING_SCALE * $vertices->[ ( 2 + $v ) % 3 ]->{ 'X' },
                        $DRAWING_SCALE * $vertices->[ ( 2 + $v ) % 3 ]->{ 'Y' },
                        ( $v == 0 ) ? 'mv' : 'ln'
            ;
        }
    
        if ( $drawing_type eq DRAWING_TYPE_FILL ) {
            
            printf $fh "1 0 0 setrgbcolor gsave closepath stroke grestore fill\n";
        }
        else {

            printf $fh "%s 0 setgray stroke\n", $DRAW_OPEN_SIDES ? 'closepath' : '' ;
        }
    }

    return;
}

############################################################
# Load recipe

sub loadRecipe {

    my $recipe_file = shift;

    open my $FH, '<', $recipe_file or do {
        printf "Could not open $recipe_file- $!\n";
        exit -1;
    };

    $RECIPE = <$FH>;
    chomp $RECIPE;

    close $FH;

    return;
}

############################################################
# Dump polygon status

sub dumpPolygons {

    for my $polygon ( @{ $POLYGONS } ) {

        printf "P%s - %s\n", $polygon->{ 'ID' }, getPolygonDescription( $polygon );

        for ( @{ $polygon->{ 'VERTICES' } } ) {
            my $v = $_;
            printf "    %x [ ", $v;
            printf join ',', ( sort keys %{ $v->{ 'POLYGONS' } } );
            printf " ]\n";
        };
    }

    exit;
}

############################################################
# Help doco

sub help {

    print "\n@_\n" if @_;

    printf "\n$0 " .
                "[-a <type>] [-A] [-c] [-d <deflations>] [-f] [-F <file>] [-h] " .
                "[-l] [-o] [-p] [-r <recipe>] [-R <file>] [-s <scale>] [-t <val>] " .
                "[-u] [-x <off>] [-y <off>] [-D <val>] [-W <val>]

    where
        -A  only draw the arcs
        -a  arc type to draw (see below)
        -c  consolodate half polygons into full ones
        -d  number of deflation cycles (default 1)
        -f  fill the polygons
        -h  this help message
        -j  join any open polygons left after consolidation
        -l  label the polygons
        -m  centre the templates about the origin
        -o  draw the open sides
        -p  print polgon data
        -r  recipe string
        -R  file containing a recipe string
        -s  drawing scale
        -t  text size multiplier
        -u  highlight the last unused side
        -x  horizontal drawing offset
        -y  vertical drawing offset

        -F  output file name
        -D  image depth
        -W  image width

    Valid arc types are
        'arcs'  - normal circular arcs
        'none'  - do not draw any arcs
    \n";

    exit;
}

__END__
