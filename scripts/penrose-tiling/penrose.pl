#!/usr/bin/perl

use warnings;
use strict;

use Math::Trig;
use Data::Dumper;
use Getopt::Std;
use File::Path qw(make_path);

use constant    IMAGES_DIR              =>  '../penrose-tilings';
use constant    JSON_DIR              =>  '../penrose-tilings/json';

use constant    POLYGON_TYPE_DART       =>  'dart';
use constant    POLYGON_TYPE_KITE       =>  'kite';

use constant    POLYGON_STATUS_READY    =>  'polygon_ready';
use constant    POLYGON_STATUS_OUTSIDE  =>  'polygon_outside';
use constant    POLYGON_STATUS_DONE     =>  'polygon_done';

use constant    SIDE_STATUS_READY       =>  'side_ready';
use constant    SIDE_STATUS_DONE        =>  'side_done';

use constant    SIDE_TYPE_LONG_A        =>  'long_side_a';  # phi-1
use constant    SIDE_TYPE_LONG_B        =>  'long_side_b';  # 1-phi
use constant    SIDE_TYPE_SHORT_A       =>  'short_side_a'; # 1/phi-1
use constant    SIDE_TYPE_SHORT_B       =>  'short_side_b'; # 1-1/phi

use constant    PROCESSING_OK           =>  'processing_ok';
use constant    PROCESSING_RETRY        =>  'processing_retry';
use constant    PROCESSING_FAIL         =>  'processing_fail';

use constant    VERTEX_STATUS_READY     =>  'vtx_ready';

use constant    PHI                     =>  ( 1.0 + sqrt( 5.0 ) ) / 2.0;

use constant    IMAGE_PAGE_DIMENSIONS   =>  {
                                    'A3'    =>  [ 297, 420 ],
                                    'A4'    =>  [ 210, 297 ],
                                    'A5'    =>  [ 148, 210 ],
                };

############################################################
# Check the startup options

my %OPTIONS;
getopts( 'a:dhi:I:JlP:R:S:vX:', \%OPTIONS );

help() if $OPTIONS{ 'h' };

my $DEBUG_LABEL_SIZE = $OPTIONS{ 'a' } || 10;
my $DEBUG_DUMP_DATA = $OPTIONS{ 'd' };
my $DEBUG_LABEL_POLYGONS = $OPTIONS{ 'l' };
my $DEBUG_RUN_TO_ITERATION = $OPTIONS{ 'i' };
my $DEBUG_PROMPT_AFTER_ITERATION = $OPTIONS{ 'I' };
my $VERBOSE = $OPTIONS{ 'v' };
my $DRAWING_PAGE_SIZE = $OPTIONS{ 'P' } || 'A4';
my $DRAWING_SCALE = $OPTIONS{ 'S' } || 1;
my $JSON_DATA_ONLY = $OPTIONS{ 'J' };

my $INITIAL_RECIPE = $OPTIONS{ 'R' };
my $EXCLUSION_RADIUS = $OPTIONS{ 'X' } || 100;

my $DRAWING_OFFSET_X = 0;
my $DRAWING_OFFSET_Y = 0;

my $IMAGE_ID = 1;
my $JSON_ID = 1;

$DRAWING_SCALE *= 20;   # implicit magnification

############################################################
# Setup the environment

my $ITERATION = 0;

my $SAVED_RECIPES = {};

my $POLYGONS = [];
my $POLYGON_LOOKUP = {};

my $RECIPE = "";

help( 'Invalid page size' )
                    unless IMAGE_PAGE_DIMENSIONS->{ $DRAWING_PAGE_SIZE };

my $TEMP = 'clp.ps';

my $ang = 2.0 * pi / 5.0;
my $long = 1.0 + PHI;
my $short = 1.0 + 1.0 / PHI;

my $TEMPLATE_DART = {

    'ANGLES'    =>  [ 3.0 * $ang, $ang / 2.0, $ang, $ang / 2.0  ],

    'SIDES'     =>  [
        { 'TYPE' =>  SIDE_TYPE_SHORT_A,  'STATUS' =>  SIDE_STATUS_READY },
        { 'TYPE' =>  SIDE_TYPE_LONG_A,   'STATUS' =>  SIDE_STATUS_READY },
        { 'TYPE' =>  SIDE_TYPE_LONG_B,   'STATUS' =>  SIDE_STATUS_READY },
        { 'TYPE' =>  SIDE_TYPE_SHORT_B,  'STATUS' =>  SIDE_STATUS_READY },
    ],

    'VERTICES'  =>  [
            newVertex( 0.0, 0.0, 3.0 * $ang ),
            newVertex( $short, 0, $ang / 2.0 ),
            newVertex( $short - $long * cos( $ang / 2.0 ), $long * sin( $ang / 2.0 ), $ang ),
            newVertex( $short * cos( 3.0 * $ang ), $short * sin( 3.0 * $ang ), $ang / 2.0 ),
    ],
};

my $TEMPLATE_KITE = {

    'ANGLES'    =>  [ $ang, $ang, 2.0 * $ang, $ang ],

    'SIDES'     =>  [
        { 'TYPE' =>  SIDE_TYPE_LONG_A,  'STATUS' =>  SIDE_STATUS_READY },
        { 'TYPE' =>  SIDE_TYPE_SHORT_A, 'STATUS' =>  SIDE_STATUS_READY },
        { 'TYPE' =>  SIDE_TYPE_SHORT_B, 'STATUS' =>  SIDE_STATUS_READY },
        { 'TYPE' =>  SIDE_TYPE_LONG_B,  'STATUS' =>  SIDE_STATUS_READY },
    ],

    'VERTICES'  =>  [
            newVertex( 0.0, 0.0, $ang ),
            newVertex( $long, 0, $ang ),
            newVertex( $long - $short * cos( $ang ), $short * sin( $ang ), 2 * $ang ),
            newVertex( $long * cos( $ang ), $long * sin( $ang ), $ang ),
    ],
};

my $NEXT_POLYGON_ID = 0;

############################################################
# Do the work

$RECIPE = $INITIAL_RECIPE ? substr( $INITIAL_RECIPE, 0, 1 ) : 'D';

my %trial_polygon;
my %initial_polygon;
transformTemplate( \%initial_polygon, POLYGON_TYPE_DART, 0, 0, pi / 2.0 );
$POLYGONS = [ \%initial_polygon ];

my $all_done = 0;

my $success = 1;

my $NOW = 0;
while ( 1 ) {   # Try all recipe combinations

    while ( ! $all_done ) {

        $all_done = 1;

        for my $polygon ( @{ $POLYGONS } ) {

            next if ( $polygon->{ 'STATUS' } eq POLYGON_STATUS_DONE );
            next if ( $polygon->{ 'STATUS' } eq POLYGON_STATUS_OUTSIDE );
        
            printf "calling processPolygon( $polygon\[%s\], $success );\n",
                                                        $polygon->{'ID'} if $VERBOSE;

            $ITERATION++;
            printf "Iteration: $ITERATION\n" if $VERBOSE;

            my $retval = processPolygon( $polygon, $success );

            printf "retval was $retval\n" if $VERBOSE;

            if ( $retval eq PROCESSING_RETRY ) {
                $all_done = 0;
                $success = 0;
                last;
            }

            $all_done = 0;
            $success = 1;
        }
    }

    # If we have worked through all of the possible Dart based combinations
    # then restart the recipe with a Kite - otherwise it is a valid receipe
    if ( $RECIPE ne 'D' ) {

        printf "Got a completed recipe: $RECIPE\n";
        $JSON_DATA_ONLY ? dumpJsonData() : drawImage();

        removePolygonAtPos( scalar( @{ $POLYGONS } ) - 1 );
        $success = 0;
        $all_done = 0;
    }
    else {

        $RECIPE = 'K';

        my %kite;
        transformTemplate( \%kite, POLYGON_TYPE_KITE, 0, 0, pi / 2.0 );
        $POLYGONS = [ \%kite ];

        $success = 1;
        $all_done = 0;
        $NOW = 1;
        printf "restarting with a Kite\n" if $VERBOSE;
    }
}

exit;

############################################################
# Simple mod/min/max functions

sub min {

    my ( $v1, $v2 ) = @_;

    return ( $v1 < $v2 ) ? $v1 : $v2;
}

sub max {

    my ( $v1, $v2 ) = @_;

    return ( $v1 > $v2 ) ? $v1 : $v2;
}

sub mod {

    my $v = shift;

    return ( $v > 0 ) ? $v : ( -1 * $v );
}

sub isDart { return shift->{ 'TYPE' } eq POLYGON_TYPE_DART }
sub isKite { return shift->{ 'TYPE' } eq POLYGON_TYPE_KITE }

############################################################
# Return the next type to try (always try a dart first)

sub findNextPolygonType {

    my $success = shift;    # did the last choice succeed?

    my $recipe_length = length( $RECIPE );

    if ( $INITIAL_RECIPE && ( $recipe_length < length( $INITIAL_RECIPE ) ) ) {

        my $polygon_type = substr $INITIAL_RECIPE, $recipe_length, 1;
        $RECIPE .= $polygon_type;
        printf "recipe now $RECIPE from inital\n" if $VERBOSE;
        return $polygon_type eq 'D' ? POLYGON_TYPE_DART : POLYGON_TYPE_KITE;
    }

    # Reset the initial recipe so that it is now reused if the current recipe
    # becomes shorter
    $INITIAL_RECIPE = '';

    if ( $success > 0 ) {
        $RECIPE .= "D";
        return POLYGON_TYPE_DART;
    }

    # So the last selection failed - if it was a dart then we 
    # can try a kite
    my $last_type = substr $RECIPE, -1, 1;
    printf "last type : $last_type\n" if $VERBOSE;
    if ( $last_type eq 'D' ) {
        $RECIPE = substr( $RECIPE, 0, $recipe_length - 1 ) . 'K';
        return POLYGON_TYPE_KITE;
    }

    # Then the last attempt was a kite but it was not added to the 
    # POLYGONS arrays so can be removed from the list
    $recipe_length--;
    $RECIPE = substr( $RECIPE, 0, $recipe_length );

    # Now move back through the array removing any trailing kites 
    # since those recipes lead nowhere - these entries do have
    # polygon list entries and will need to be removed...
    while ( substr( $RECIPE, $recipe_length - 1, 1 ) eq 'K' ) { 

        $recipe_length--;
        if ( $recipe_length <= 0  ) {
            printf "nothing left to try...\n" if $VERBOSE;
            exit 0;
        }

        removePolygonAtPos( $recipe_length );
        $RECIPE = substr( $RECIPE, 0, $recipe_length );
    }

    # last entry will now be a dart - returning undef will terminate
    # processing of the current polygon - the next round will request
    # this function again and the type will be reset to a kite

    printf "removing poly @ %d\n", $recipe_length - 1 if $VERBOSE;
    removePolygonAtPos( $recipe_length - 1 );
    printf "recipe now $RECIPE (%d) and arr length %d\n",
                    length($RECIPE), scalar @{ $POLYGONS } if $VERBOSE;

    $NOW = 1 if $RECIPE eq 'D';
    
    return;     # dont return a type - indicates a rollback happened
}

############################################################
# Remove the indicated polgon from the list

sub removePolygonAtPos {

    my $index = shift;

    my $polygon = $POLYGONS->[ $index ];

    printf "removing poygon at pos $index (id: %s)\n",
                                $polygon->{ 'ID' } if $VERBOSE;

    my $id = $polygon->{ 'ID' };

    for ( my $v = 0; $v < 4; $v++ ) {

        my $vertex = $polygon->{ 'VERTICES' }->[ $v ];

        # Give back the angles
        $vertex->{ 'ANG_USED' } -= $polygon->{ 'ANGLES' }->[ $v ];
        printf "returned %d to %x\n",
                    rad2deg($polygon->{ 'ANGLES' }->[ $v ]), $vertex if $VERBOSE;

        printf "remove last_polygon %s from vertex $v\n",
                    $vertex->{ 'LAST_POLYGONS' }->[ -1 ] if $VERBOSE;
        pop @{ $vertex->{ 'LAST_POLYGONS' } };

        # Reset any common side status values
        for my $key ( keys %{ $vertex->{ 'POLYGONS' } } ) {

            next if ( $key eq $id );

            my $common_polygon = $POLYGON_LOOKUP->{ $key };
        
            printf "looking for vertex %x in polygon %s\n",
                                        $vertex, $common_polygon->{'ID'} if $VERBOSE;

            my $vertex_no = findPolygonVertexNumber( $common_polygon, $vertex );

            # Check both ends of the common vertex side since on completed
            # vertices there could be sides with just the single common vertex

            my $vertices = $common_polygon->{ 'VERTICES' };
            my $sides = $common_polygon->{ 'SIDES' };
            if ( $vertices->[ $vertex_no - 1 ]->{ 'POLYGONS' }->{ $id } ) {

                printf "1. resetting side %s/%d\n", $key, $vertex_no - 1 if $VERBOSE;
                $sides->[ $vertex_no - 1 ]->{ 'STATUS' } = SIDE_STATUS_READY;
                delete $vertices->[ $vertex_no - 1 ]->{ 'POLYGONS' }->{ $id };
                $common_polygon->{ 'STATUS' } = POLYGON_STATUS_READY
                    unless ( $common_polygon->{ 'STATUS' } eq POLYGON_STATUS_OUTSIDE );
            }

            if ( $vertices->[ $vertex_no - 3 ]->{ 'POLYGONS' }->{ $id } ) {
                printf "2. resetting side %s/%d\n", $key, $vertex_no if $VERBOSE;
                $sides->[ $vertex_no ]->{ 'STATUS' } = SIDE_STATUS_READY;
                delete $vertices->[ $vertex_no ]->{ 'POLYGONS' }->{ $id };
                $common_polygon->{ 'STATUS' } = POLYGON_STATUS_READY
                    unless ( $common_polygon->{ 'STATUS' } eq POLYGON_STATUS_OUTSIDE );
            }
        }

        # Remove from the vertex polygons hash
        delete $vertex->{ 'POLYGONS' }->{ $id };
    }

    delete $POLYGON_LOOKUP->{ $id };

    # ASSUMES index is the last array entry..
    printf "removing POLYGONS->[$index] (array %d long)\n", scalar @{$POLYGONS} if $VERBOSE;
    splice @{ $POLYGONS }, $index;
}

############################################################
# Find the distance between 2 vertices

sub distance {

    my $vtx1 = shift;
    my $vtx2 = shift;

    my $dx = $vtx1->{ 'X' } - $vtx2->{ 'X' };
    my $dy = $vtx1->{ 'Y' } - $vtx2->{ 'Y' };

    return sqrt( ( $dx * $dx ) +  ( $dy * $dy ) );
}

############################################################
# Translate, rotate and scale a vertex

sub transformVertex {

    my $vertex = shift;
    my $offset_x = shift;
    my $offset_y = shift;
    my $rotate = shift;

    my $ang = atan2( $vertex->{ 'Y' },  $vertex->{ 'X' } ); 
    my $dist = distance( { 'X' => 0.0, 'Y' => 0.0 }, $vertex );

    $ang += $rotate;

    $vertex->{ 'X' } = $dist * cos( $ang ) + $offset_x;
    $vertex->{ 'Y' } = $dist * sin( $ang ) + $offset_y;
}

############################################################
# Translate, rotate and scale a copy of the template polygon

sub transformTemplate {

    my $polygon = shift;
    my $type = shift;
    my $offset_x = shift;
    my $offset_y = shift;
    my $rotate = shift;

    my $template = polygonTemplate( $type );

    my $new_id = $NEXT_POLYGON_ID++;

    $polygon->{ 'ID' } = $new_id;
    $polygon->{ 'TYPE' } = $type;
    $polygon->{ 'STATUS' } = POLYGON_STATUS_READY;
    $polygon->{ 'VERTICES' } = [];
    $polygon->{ 'ANGLES' } = [ @{ $template->{ 'ANGLES' } } ];
    $polygon->{ 'SIDES' } = [ map { my %h = %{ $_ }; \%h } ( @{ $template->{ 'SIDES' } } ) ];

    for my $vertex ( @{ $template->{ 'VERTICES' } } ) {

        my %vertex_copy = %{ $vertex };
    
        $vertex_copy{ 'STATUS' } = VERTEX_STATUS_READY;
        $vertex_copy{ 'POLYGONS' } = { $new_id => 1 };
        $vertex_copy{ 'LAST_POLYGONS' } = [ $new_id ];

        transformVertex( \%vertex_copy, $offset_x, $offset_y, $rotate );

        push @{ $polygon->{ 'VERTICES' } }, \%vertex_copy;
    }

    $POLYGON_LOOKUP->{ $new_id } = $polygon;

    return $polygon;
}

############################################################
# Return a new vertex object

sub newVertex {

    return {
        'X'         =>  shift,
        'Y'         =>  shift,
        'ANG_USED'  =>  shift,
    };
}

############################################################
# Draw the polygons

sub drawImage {

    my $page_dimensions = IMAGE_PAGE_DIMENSIONS->{ $DRAWING_PAGE_SIZE };

    $DRAWING_OFFSET_X = $page_dimensions->[ 0 ] * 2.54 / 2.0;
    $DRAWING_OFFSET_Y = $page_dimensions->[ 1 ] * 2.54 / 2.0;

    open my $fh, '>', $TEMP or die "Could not open $TEMP - $!\n";
    printf $fh "%%!PS

        <<
            /PageSize [ %f 2.54 mul %f 2.54 mul ]
            /ImagingBBox null
        >> setpagedevice

        /mv { moveto } bind def
        /ln { lineto } bind def

        /drawArc {
            /a1 exch def
            /a2 exch def
            /rad exch def
            /yc exch def
            /xc exch def
    
            xc a1 cos rad mul add
            yc a1 sin rad mul add
            moveto
            xc yc rad a1 a2 arcn
        } bind def

        /Arial findfont $DEBUG_LABEL_SIZE scalefont setfont

        0 0 mv 1000 0 ln 1000 1000 ln 0 1000 ln fill

        $DRAWING_OFFSET_X $DRAWING_OFFSET_Y translate
    ",
        $page_dimensions->[ 0 ], $page_dimensions->[ 1 ];

    if ( $EXCLUSION_RADIUS ) {

        printf $fh "1 setgray 0 0 %f 0 360 drawArc stroke\n", $EXCLUSION_RADIUS;
    }

    drawDebugImage( $fh );

    close $fh;

    my $convert = "/usr/bin/convert" unless ( -f $convert );

    unless ( -f $convert ) {

        printf "Count not locate convert at $convert\n";
    }
    else {

        my $dest_dir = sprintf "%s/%d", IMAGES_DIR, $EXCLUSION_RADIUS;
        make_path( $dest_dir );

        my $dest = sprintf "%s/%s.png", $dest_dir, $IMAGE_ID++;

        system( "$convert $TEMP $dest" );
    }
}

############################################################
# Process a polygon

sub processPolygon {

    my $polygon = shift;
    my $success = shift;

    for( my $s = 0; $s < 4; $s++ ) {

        my $side = $polygon->{ 'SIDES' }->[ $s ];
        next if ( $side->{ 'STATUS' } eq SIDE_STATUS_DONE );

        my $next_type = findNextPolygonType( $success );
        unless ( $next_type ) {
            printf "giving up\n" if $VERBOSE;
            return PROCESSING_RETRY
        }

        printf "trying %s on side %s/%s ($RECIPE)\n",
                            $next_type, $polygon->{ 'ID' }, $s if $VERBOSE;

        if ( ( $success = processPolygonSide( $polygon, $s, $next_type ) ) < 0 ) {
            printf "failed ($success)\n" if $VERBOSE;
            $s--;   # Try this side again
        }
        else {
            printf "ok\n" if $VERBOSE;
        }

        if ( $DEBUG_RUN_TO_ITERATION && ( $ITERATION >= $DEBUG_RUN_TO_ITERATION ) ) {
            drawImage() unless $JSON_DATA_ONLY;
            printf "$ITERATION completed> ";
            my $i = <STDIN>;
            exit 0;
        }

        if ( $DEBUG_PROMPT_AFTER_ITERATION &&
                            ( $ITERATION >= $DEBUG_PROMPT_AFTER_ITERATION ) ) {
        
            drawImage() unless $JSON_DATA_ONLY;
            my $in = <STDIN>;
            system("clear");
        }
    }

    $polygon->{ 'STATUS' } = POLYGON_STATUS_DONE;

    return PROCESSING_OK;
}

############################################################
# Try and place the given polygon type on a polygon side

sub processPolygonSide {

    my $polygon = shift;
    my $s = shift;
    my $next_type = shift;

    my $polygon_type = $polygon->{ 'TYPE' };
    my $vertices = $polygon->{ 'VERTICES' };
    my $side = $polygon->{ 'SIDES' }->[ $s ];

    # Matching short DART sides is never allowed
    if ( ( $polygon_type eq POLYGON_TYPE_DART ) && ( $polygon_type eq $next_type ) ) {

        return -1 if ( $side->{ 'TYPE' } eq SIDE_TYPE_SHORT_A );
        return -2 if ( $side->{ 'TYPE' } eq SIDE_TYPE_SHORT_B );
    }

    my $side_number = findSideMatch( $next_type, $side->{ 'TYPE' } );

    my $side_ang = atan2(
                $vertices->[ $s - 3 ]->{ 'Y' } - $vertices->[ $s ]->{ 'Y' },
                $vertices->[ $s - 3 ]->{ 'X' } - $vertices->[ $s ]->{ 'X' }
    ); 

    # How far round to rotate the polygon to align the given side
    my $polygon_ang =
            pi 
            + findPolygonSideAngle( $next_type, $side_number )
            - $side_number * pi
            + $side_ang;

    transformTemplate( \%trial_polygon, $next_type, $vertices->[ $s ]->{ 'X' }, 
                                    $vertices->[ $s ]->{ 'Y' }, $polygon_ang );

    my $new_vertices = $trial_polygon{ 'VERTICES' };
    my $idx = ( $polygon_type ne $next_type ) ? ( $s + 3 ) : ( 4 - $s );

    my $x_offset = $new_vertices->[ $idx % 4 ]->{ 'X' } - $new_vertices->[ 0 ]->{ 'X' };
    my $y_offset = $new_vertices->[ $idx % 4 ]->{ 'Y' } - $new_vertices->[ 0 ]->{ 'Y' };

    map { 
        $_->{ 'X' } -= $x_offset;
        $_->{ 'Y' } -= $y_offset;
    } ( @{ $new_vertices } );

    # Check the angles on these vertices
    
    my $new_vertex_angle1 = $vertices->[ $s ]->{ 'ANG_USED' } +
                            $trial_polygon{ 'ANGLES' }->[ $side_number - 3 ];
    printf "vertex %s angle overblown\n", $s
                                if ( ( $new_vertex_angle1 > 2 * pi ) && $VERBOSE );
    return -3 if ( $new_vertex_angle1 > 2 * pi );

    my $new_vertex_angle2 = $vertices->[ $s - 3 ]->{ 'ANG_USED' } +
                            $trial_polygon{ 'ANGLES' }->[ $side_number ];
    printf "vertex %s angle overblown\n", $s - 3
                                if ( ( $new_vertex_angle2 > 2 * pi ) && $VERBOSE );
    return -4 if ( $new_vertex_angle2 > 2 * pi );

    # keep an array of vertices to share incase this new poly doesnt work out...
    my $shared_vertices = [];

    printf "share vtx %s/%d and %s/%d\n",
            $polygon->{'ID'}, $s, $trial_polygon{'ID'}, ($side_number+1)%4 if $VERBOSE;

    push @{ $shared_vertices }, {
        'ID1'               =>  $polygon->{'ID'},
        'VERTEX_LIST1'      =>  $vertices,
        'VERTEX_NUMBER1'    =>  $s,
        'ID2'               =>  $trial_polygon{'ID'},
        'VERTEX_LIST2'      =>  $new_vertices,
        'VERTEX_NUMBER2'    =>  $side_number - 3,
    };

    printf "share vtx %s/%d and %s/%d\n",
            $polygon->{'ID'}, ($s+1)%4, $trial_polygon{'ID'}, $side_number if $VERBOSE;

    push @{ $shared_vertices }, {
        'ID1'               =>  $polygon->{'ID'},
        'VERTEX_LIST1'      =>  $vertices,
        'VERTEX_NUMBER1'    =>  $s - 3,
        'ID2'               =>  $trial_polygon{'ID'},
        'VERTEX_LIST2'      =>  $new_vertices,
        'VERTEX_NUMBER2'    =>  $side_number,
    };

    # If there is only 36 left on the vertex there cannot be a long kite side free
    # - so we cannot be matching a long kite side either.. 
    if (
        ( $next_type eq POLYGON_TYPE_KITE ) &&
        (
            ( rad2deg( $new_vertex_angle1 ) == 324 ) ||
            ( rad2deg( $new_vertex_angle2 ) == 324 ) 
        ) &&
        (
            ( $side->{ 'TYPE' } eq SIDE_TYPE_LONG_A ) ||
            ( $side->{ 'TYPE' } eq SIDE_TYPE_LONG_B )
        )
    )   {
        # seems to be excluding too many... :(
        # printf "36 left on %s/%d\n", $polygon->{ 'ID' }, $s - 3;
        # return -5;
    }

    # If this new polygon completes one if the vertices check for
    # another shared vertex

    if ( rad2deg( $new_vertex_angle1 ) == 0 ) {
        printf "check filler on polygon at vertex %s/$s\n", $polygon->{ 'ID' } if $VERBOSE;
        my $last_id = $vertices->[ $s ]->{ 'LAST_POLYGONS' }->[ -1 ];
        printf "last polygon was %s\n", $last_id if $VERBOSE;
        my $last_polygon = $POLYGON_LOOKUP->{ $last_id };

        my $last_side_no = findPolygonVertexNumber( $last_polygon, $vertices->[ $s ] ) - 1;
        my $last_side_type = findMatchingSideType( $last_polygon->{ 'SIDES' }->[ $last_side_no ]->{ 'TYPE' } );
        my $must_match_type = $trial_polygon{ 'SIDES' }->[ $side_number - 3 ]->{ 'TYPE' };

        if ( $VERBOSE ) {
            printf "1. shared side %d on last (%s)", $last_side_no, $last_polygon->{'ID'};
            printf " and %d on new (%s)\n", $side_number - 3, $trial_polygon{'ID'};
        }

        return -6 if ( $last_side_type ne $must_match_type );

        push @{ $shared_vertices }, {
            'ID1'               =>  $last_polygon->{'ID'},
            'VERTEX_LIST1'      =>  $last_polygon->{ 'VERTICES' },
            'VERTEX_NUMBER1'    =>  $last_side_no,
            'ID2'               =>  $trial_polygon{'ID'},
            'VERTEX_LIST2'      =>  $new_vertices,
            'VERTEX_NUMBER2'    =>  $side_number - 2,
        };

        if ( $VERBOSE ) {
            printf "1. side %s/%d now done\n", $last_polygon->{'ID'}, $last_side_no;
            printf "1. side %s/%d now done\n", $trial_polygon{'ID'}, $side_number - 3;
        }

        $last_polygon->{ 'SIDES' }->[ $last_side_no ]->{ 'STATUS' } = SIDE_STATUS_DONE;
        $trial_polygon{ 'SIDES' }->[ $side_number - 3 ]->{ 'STATUS' } = SIDE_STATUS_DONE;
    }

    if ( rad2deg( $new_vertex_angle2 ) == 0 ) {
        printf "check filler on polygon at vertex %s/%d\n", $polygon->{ 'ID' }, $s -3 if $VERBOSE;
        my $last_id = $vertices->[ $s - 3 ]->{ 'LAST_POLYGONS' }->[ -1 ];
        printf "last polygon was %s\n", $last_id if $VERBOSE;
        my $last_polygon = $POLYGON_LOOKUP->{ $last_id };
        
        my $last_side_no = findPolygonVertexNumber( $last_polygon, $vertices->[ $s - 3 ] );
        my $last_side_type = findMatchingSideType( $last_polygon->{ 'SIDES' }->[ $last_side_no ]->{ 'TYPE' } );
        my $must_match_type = $trial_polygon{ 'SIDES' }->[ $side_number - 1 ]->{ 'TYPE' };

        if ( $VERBOSE ) {
            printf "2. shared side %d on last (%s)", $last_side_no, $last_polygon->{'ID'};
            printf " and %d on new (%s)\n", $side_number - 1 , $trial_polygon{'ID'};
        }

        return -7 if ( $last_side_type ne $must_match_type );

        push @{ $shared_vertices }, {
            'ID1'               =>  $last_polygon->{'ID'},
            'VERTEX_LIST1'      =>  $last_polygon->{ 'VERTICES' },
            'VERTEX_NUMBER1'    =>  $last_side_no - 3,
            'ID2'               =>  $trial_polygon{'ID'},
            'VERTEX_LIST2'      =>  $new_vertices,
            'VERTEX_NUMBER2'    =>  $side_number - 1,
        };

        if ( $VERBOSE ) {
            printf "2. side %s/%d now done\n", $last_polygon->{'ID'}, $last_side_no;
            printf "2. side %s/%d now done\n", $trial_polygon{'ID'}, $side_number - 1;
        }

        $last_polygon->{ 'SIDES' }->[ $last_side_no ]->{ 'STATUS' } = SIDE_STATUS_DONE;
        $trial_polygon{ 'SIDES' }->[ $side_number - 1 ]->{ 'STATUS' } = SIDE_STATUS_DONE;
    }

    map { shareVertices( $_ ) } ( @{ $shared_vertices } );

    $trial_polygon{ 'SIDES' }->[ $side_number ]->{ 'STATUS' } = SIDE_STATUS_DONE;

    checkForInclusion( \%trial_polygon ) if $EXCLUSION_RADIUS;

    # So this is a keeper - make a copy for the POLYGONS array
    my %copy = %trial_polygon;
    $POLYGON_LOOKUP->{ $copy{ 'ID' } } = \%copy;
    push @{ $POLYGONS }, \%copy;

    $side->{ 'STATUS' } = SIDE_STATUS_DONE;

    return 1;
}

############################################################
# Check if this given polygon is within the exclusion radius

sub checkForInclusion {
    
    my $polygon = shift;

    my $origin = { 'X' => 0.0, 'Y' => 0.0 };

    for my $vertex ( @{$polygon->{ 'VERTICES' } } ) {

        return 1 if ( distance( $origin, $vertex ) < $EXCLUSION_RADIUS / $DRAWING_SCALE );
    }

    printf "%s is excluded\n", $polygon->{ 'ID' } if $VERBOSE;

    # must be completely outside then...
    $polygon->{ 'STATUS' } = POLYGON_STATUS_OUTSIDE; 

    return 0;
}

############################################################
# Find given vertex's number on the given polygon

sub findPolygonVertexNumber {

    my $polygon = shift;
    my $vertex = shift;

    for ( my $v = 0; $v < 4; $v++ ) {

        return $v if $polygon->{ 'VERTICES' }->[ $v ] == $vertex;
    }

    return -1;
}

############################################################
# Share the vertices

sub shareVertices {

    my $sharing_info = shift;

    my $vertex_list1 = $sharing_info->{ 'VERTEX_LIST1' };
    my $vertex_number1 = $sharing_info->{ 'VERTEX_NUMBER1' };
    my $vertex_list2 = $sharing_info->{ 'VERTEX_LIST2' };
    my $vertex_number2 = $sharing_info->{ 'VERTEX_NUMBER2' };

    printf "sharing vertices %s/%d and %s/%d\n",
                $sharing_info->{ 'ID1' }, $sharing_info->{ 'VERTEX_NUMBER1' },
                $sharing_info->{ 'ID2' }, $sharing_info->{ 'VERTEX_NUMBER2' }
                    if $VERBOSE;

    for my $id ( keys %{ $vertex_list2->[ $vertex_number2 ]->{ 'POLYGONS' } } ) {
        
        $vertex_list1->[ $vertex_number1 ]->{ 'POLYGONS' }->{ $id } = 1;
    }

    $vertex_list1->[ $vertex_number1 ]->{ 'ANG_USED' } +=
                        $vertex_list2->[ $vertex_number2 ]->{ 'ANG_USED' };

    my $last_polygon_list = $vertex_list1->[ $vertex_number1 ]->{ 'LAST_POLYGONS' };
    my $last_id = $last_polygon_list->[ -1 ];

    if ( $last_id ne $sharing_info->{ 'ID2' } ) {
    
        printf "adding last_polygon %s to %s/%d (was %s)\n",
            $sharing_info->{ 'ID2' },
            $sharing_info->{ 'ID1' },
            $vertex_number1,
            $last_id
                if  $VERBOSE;

        push @{ $last_polygon_list }, $sharing_info->{ 'ID2' };
    }

    $vertex_list2->[ $vertex_number2 ] = $vertex_list1->[ $vertex_number1 ];
}

############################################################
# Find the sum of internal angles to the given side

sub findPolygonSideAngle {

    my $polygon_type = shift;
    my $side_number = shift;

    my $ang = 0;

    my $polygon = polygonTemplate( $polygon_type );

    while ( $side_number > 0 ) {

        $ang += $polygon->{ 'ANGLES' }->[ $side_number ];
        $side_number--;
    }

    return $ang;
}

############################################################
# Return the template for the given polygon type

sub polygonTemplate {

    return ( shift eq POLYGON_TYPE_DART ) ?
                                $TEMPLATE_DART : $TEMPLATE_KITE;
}

############################################################
# Find the side to match that given on the polygon type

sub findSideMatch {

    my $polygon_type = shift;
    my $side_type = shift;

    my $match_type = findMatchingSideType( $side_type );
    return -1 unless $match_type;

    my $template = polygonTemplate( $polygon_type );

    for( my $s = 0; $s < 4; $s++ ) {
    
        my $side = $template->{ 'SIDES' }->[ $s ];
        return $s if ( $side->{ 'TYPE' } eq $match_type );
    }

    return -1;
}


############################################################
# Find the side type to match that given

sub findMatchingSideType {

    my $side_type = shift;

    return SIDE_TYPE_LONG_A if ( $side_type eq SIDE_TYPE_LONG_B );
    return SIDE_TYPE_LONG_B if ( $side_type eq SIDE_TYPE_LONG_A );
    return SIDE_TYPE_SHORT_A if ( $side_type eq SIDE_TYPE_SHORT_B );
    return SIDE_TYPE_SHORT_B if ( $side_type eq SIDE_TYPE_SHORT_A );

    return; # :(
}

############################################################
# Is the given polygon visible on the page

sub inViewport {
    
    my $polygon = shift;

    my $vertices = $polygon->{ 'VERTICES' };

    my $bounds;

    for( my $p = 0; $p < 4; $p++ ) {
        
        my $x = ( $vertices->[ $p ]->{ 'X' } * $DRAWING_SCALE ) + $DRAWING_OFFSET_X;
        my $y = ( $vertices->[ $p ]->{ 'Y' } * $DRAWING_SCALE ) + $DRAWING_OFFSET_Y;

        $bounds->[ 0 ] = $p ? min( $bounds->[ 0 ], $x ) : $x;
        $bounds->[ 1 ] = $p ? min( $bounds->[ 1 ], $y ) : $y;
        $bounds->[ 2 ] = $p ? max( $bounds->[ 2 ], $x ) : $x;
        $bounds->[ 3 ] = $p ? max( $bounds->[ 3 ], $y ) : $y;
    }

    return 0 if ( $bounds->[ 2 ] < 0 );
    return 0 if ( $bounds->[ 3 ] < 0 );

    my $page_dimensions = IMAGE_PAGE_DIMENSIONS->{ $DRAWING_PAGE_SIZE };

    return 0 if ( $bounds->[ 0 ] > ( $page_dimensions->[ 0 ] * 2.54 ) );
    return 0 if ( $bounds->[ 1 ] > ( $page_dimensions->[ 1 ] * 2.54 ) );

    return 1;
}

############################################################
# Draw a dart

sub drawDart {

    my $fh = shift;
    my $polygon = shift;

    my $vertices = $polygon->{ 'VERTICES' };

    for ( my $v = 0; $v < 4; $v++ ) {

        printf $fh "%f %f %s ", 
                    $DRAWING_SCALE * $vertices->[ $v ]->{ 'X' },
                    $DRAWING_SCALE * $vertices->[ $v ]->{ 'Y' },
                    ( $v == 0 ) ? 'mv' : 'ln'
        ;
    }

    printf $fh "closepath stroke\n", 

    my $ang1 = atan2(
                $vertices->[ 3 ]->{ 'Y' } - $vertices->[ 2 ]->{ 'Y' },
                $vertices->[ 3 ]->{ 'X' } - $vertices->[ 2 ]->{ 'X' }
    ); 
    printf $fh "1 0 0 setrgbcolor %f %f %f %f %f drawArc stroke\n",
                                        $DRAWING_SCALE * $vertices->[ 2 ]->{ 'X' },
                                        $DRAWING_SCALE * $vertices->[ 2 ]->{ 'Y' },
                                        $DRAWING_SCALE,
                                        rad2deg( $ang1 ),
                                        rad2deg( 2.0 * pi / 5.0 + $ang1 );

    my $ang2 = atan2(
                $vertices->[ 1 ]->{ 'Y' } - $vertices->[ 0 ]->{ 'Y' },
                $vertices->[ 1 ]->{ 'X' } - $vertices->[ 0 ]->{ 'X' }
    ); 
    printf $fh "0 1 0 setrgbcolor %f %f %f %f %f drawArc stroke\n",
                                        $DRAWING_SCALE * $vertices->[ 0 ]->{ 'X' },
                                        $DRAWING_SCALE * $vertices->[ 0 ]->{ 'Y' },
                                        $DRAWING_SCALE / PHI,
                                        rad2deg( $ang2 ),
                                        rad2deg( 6.0 * pi / 5.0 + $ang2 );

    return;
}

############################################################
# Draw a kite

sub drawKite {

    my $fh = shift;
    my $polygon = shift;

    my $vertices = $polygon->{ 'VERTICES' };

    for ( my $v = 0; $v < 4; $v++ ) {

        printf $fh "%f %f %s ", 
                    $DRAWING_SCALE * $vertices->[ $v ]->{ 'X' },
                    $DRAWING_SCALE * $vertices->[ $v ]->{ 'Y' },
                    ( $v == 0 ) ? 'mv' : 'ln'
        ;
    }

    printf $fh "closepath stroke\n", 

    my $ang1 = atan2(
                $vertices->[ 1 ]->{ 'Y' } - $vertices->[ 0 ]->{ 'Y' },
                $vertices->[ 1 ]->{ 'X' } - $vertices->[ 0 ]->{ 'X' }
    ); 

    printf $fh "1 0 0 setrgbcolor %f %f %f %f %f drawArc stroke\n",
                                        $DRAWING_SCALE * $vertices->[ 0 ]->{ 'X' },
                                        $DRAWING_SCALE * $vertices->[ 0 ]->{ 'Y' },
                                        $DRAWING_SCALE * PHI,
                                        rad2deg( $ang1 ),
                                        rad2deg( 2.0 * pi / 5.0 + $ang1 );

    my $ang2 = atan2(
                $vertices->[ 3 ]->{ 'Y' } - $vertices->[ 2 ]->{ 'Y' },
                $vertices->[ 3 ]->{ 'X' } - $vertices->[ 2 ]->{ 'X' }
    ); 

    printf $fh "0 1 0 setrgbcolor %f %f %f %f %f drawArc stroke\n",
                                        $DRAWING_SCALE * $vertices->[ 2 ]->{ 'X' },
                                        $DRAWING_SCALE * $vertices->[ 2 ]->{ 'Y' },
                                        $DRAWING_SCALE,
                                        rad2deg( $ang2 ),
                                        rad2deg( 4.0 * pi / 5.0 + $ang2 );

    return;
}

############################################################
# Draw the polygons and any debug info

sub drawDebugImage {

    my $fh = shift;

    printf $fh "1 setlinejoin\n";

    for my $polygon ( @{$POLYGONS} ) {

        next unless inViewport( $polygon );

        printf $fh "1 1 0 setrgbcolor\n";

        isDart( $polygon ) ?  drawDart( $fh, $polygon ) : drawKite( $fh, $polygon );

        if ( $DEBUG_LABEL_POLYGONS ) {

            my $idx = isKite( $polygon ) ? 1 : 0;

            my $vertices = $polygon->{ 'VERTICES' };
            my $x = ( $vertices->[ $idx ]->{ 'X' } + $vertices->[ $idx + 2 ]->{ 'X' } ) / 2.0;
            my $y = ( $vertices->[ $idx ]->{ 'Y' } + $vertices->[ $idx + 2 ]->{ 'Y' } ) / 2.0;

            my $id = $polygon->{ 'ID' };
            printf $fh "
                    1 setgray
                    %f %f moveto
                    ($id) dup stringwidth pop -2 div 0 rmoveto show
            ",
                $x * $DRAWING_SCALE,
                $y * $DRAWING_SCALE,
            ;
        }
    }
}

############################################################
# Dump a json file of the polgons data

sub dumpJsonData {

    my $recipe = '';
    my $counts = { 'K' => 0, 'D' => 0 };

    # Only consider those polygons inside the exclusion radius
    for my $polygon ( @{ $POLYGONS } ) {

        next unless $polygon->{ 'STATUS' } eq POLYGON_STATUS_DONE;

        my $type = uc substr $polygon->{ 'TYPE' }, 0, 1;
        $recipe .= $type;
        $counts->{ $type }++;
    }

    return if $SAVED_RECIPES->{ $recipe };

    $SAVED_RECIPES->{ $recipe } = 1;

    my $json_dir = sprintf "%s/%d", JSON_DIR, $EXCLUSION_RADIUS;
    make_path( $json_dir );

    my $dest = sprintf "%s/%s.json", $json_dir, $JSON_ID++;
    
    open JSON, '>', $dest or do {
        printf "Could not open $dest: $!\n";
        exit -1;
    };

    printf JSON qq<{\n>;
    printf JSON qq<    "RECIPE":"$recipe",\n>;
    printf JSON qq<    "COUNTS":{"KITES":%d,"DARTS":%d},\n>,
                                $counts->{ 'K' }, $counts->{ 'D' };

    # Now build the data to save

    my $polygon_id = 1;
    my $vertices = {};

    my $psep = '';
    printf JSON qq<    "POLYGONS":[\n>;

    for my $polygon ( @{ $POLYGONS } ) {

        next unless $polygon->{ 'STATUS' } eq POLYGON_STATUS_DONE;

        printf JSON qq<${psep}         {"ID":"%d",>, $polygon_id++;
        printf JSON qq<"TYPE":"%s",>, uc substr $polygon->{ 'TYPE' }, 0, 1;
        printf JSON qq<"VERTICES":[>;

        my $vsep = '';
        for my $vertex ( @{ $polygon->{ 'VERTICES' } } ) {

            my $vertex_id = sprintf "%x", $vertex;

            printf JSON qq<${vsep}"$vertex_id">;
            $vsep = ',';

            if ( $vertices->{ $vertex_id } ) {
                $vertices->{ $vertex_id }->{ 'count' }++;
                next;
            }

            $vertices->{ $vertex_id } = {
                            'x' => $vertex->{ 'X' },
                            'y' => $vertex->{ 'Y' },
                            'count' => 1 
            };
        }

        printf JSON qq<]}>;

        $psep = ",\n";
    }

    printf JSON qq<\n    ],\n>;
    printf JSON qq<    "VERTICES":{\n>;

    my $vsep = '';
    for my $vertex_id ( keys %{ $vertices } ) {

        printf JSON qq<${vsep}        "$vertex_id":{"X":%f,"Y":%f,"COUNT":%d}>, 
                        $vertices->{ $vertex_id }->{ 'x' },
                        $vertices->{ $vertex_id }->{ 'y' },
                        $vertices->{ $vertex_id }->{ 'count' }
        ;
        $vsep = ",\n";
    }

    printf JSON qq<\n    }\n}>;

    close JSON;
}

############################################################
# Dump all of the polygon data

sub dumpAllData {

    for my $polygon ( @{ $POLYGONS } ) {

        my $vertices = $polygon->{ 'VERTICES' };
        my $sides = $polygon->{ 'SIDES' };

        my $vtx_frmt = "%x(p:%s a:%d l:%s s:%s)";

        printf "% 4s (%s)[%s] $vtx_frmt $vtx_frmt $vtx_frmt $vtx_frmt\n",
                    $polygon->{ 'ID' },
                    $polygon->{ 'TYPE' },
                    polygonStatus( $polygon ),
                    vtxDetails( $vertices->[ 0 ], $sides->[ 0 ] ),
                    vtxDetails( $vertices->[ 1 ], $sides->[ 1 ] ),
                    vtxDetails( $vertices->[ 2 ], $sides->[ 2 ] ),
                    vtxDetails( $vertices->[ 3 ], $sides->[ 3 ] ),
        ;
    }
}

############################################################
# Return the status char for the given polygon

sub polygonStatus {

    my $polygon = shift;

    return
        ( $polygon->{ 'STATUS' } eq POLYGON_STATUS_READY ) ? 'R'
      : ( $polygon->{ 'STATUS' } eq POLYGON_STATUS_DONE ) ? 'D'
      : ( $polygon->{ 'STATUS' } eq POLYGON_STATUS_OUTSIDE ) ? 'O'
      : '?';
}

############################################################
# Return a list of vertex details - for $vtx_frmt above

sub vtxDetails {

    my $vertex = shift;
    my $side = shift;

    return (
        $vertex,
        vertexPolygons( $vertex ),
        rad2deg( $vertex->{ 'ANG_USED' } ),
        $vertex->{ 'LAST_POLYGONS' }->[ -1 ],
        substr( $side->{ 'STATUS' }, 5, 1 ),
    );
}

############################################################
# Return a list of vertex polygons

sub vertexPolygons {

    my $vertex = shift;

    my $sep = '';
    my $list = '';

    for my $id ( keys %{ $vertex->{ 'POLYGONS' } } ) {
        $list .= "$sep$id";
        $sep = ", ";
    }

    return $list;
}

############################################################
# Help doco

sub help {

    print "\n@_\n" if @_;

    printf "\n$0 " .
        "[-a <sz>] [-d] [-D] [-h] [-i|I <lim>] [-l] [-P <sz>] [-R <rcp>] [-S <sc>] [-v]


    where
        -a  font size for labels
        -d  dump polygon data
        -h  this help info
        -i  run to this iteration and stop
        -I  run to this iteration and then prompt per iteration
        -l  label the polygons
        -v  display debugging messages
        -P  image page size (A5, A4, or A3) - default A4
        -R  initial recipe string
        -S  scale for drawing (default 1)
        -X  exclusion radius (any polygon outside is considered completed)
    \n";

    exit;
}

__END__

