#!/usr/bin/perl

use warnings;
use strict;

use Math::Trig;
use File::Path qw(make_path);
use File::Temp qw/ tempfile tempdir /;
use Getopt::Std;

use Data::Dumper;

############################################################
#
#   socolar.pl
#
#   Model the Socolar tiling
#
############################################################

use constant    TRUE                        =>  1;
use constant    FALSE                       =>  0;

use constant    IMAGE_WIDTH                 =>  1000;
use constant    IMAGE_DEPTH                 =>  1000;

use constant    POLYGON_TYPE_HEXAGON        =>  'hexagon';
use constant    POLYGON_TYPE_SQUARE         =>  'square ';
use constant    POLYGON_TYPE_RHOMB          =>  'rhomb';

use constant    SMALL_DIST                  =>  1.0e-10;

use constant    ROOT_2                      =>  ( sqrt( 2.0 ) );
use constant    ROOT_3                      =>  ( sqrt( 3.0 ) );
use constant    SIDE_SCALE                  =>  ( 2.0 - ROOT_3 );

############################################################
# Check the runline options

my %options;
getopts( 'a:c:d:D:f:F:hi:lo:pr:s:S:tW:', \%options );

help() if $options{ 'h' };

my $TEXT_MULTIPLIER = $options{ 'a' } || 1.0;
my $CENTER_POLYGON = $options{ 'c' };
my $DRAWING_SCALE = 50 * ( $options{ 's' } || 1 );
my $DEFLATIONS = $options{ 'd' } || 1;
my $LABEL_POLYGONS = $options{ 'l' };
my $FILL_POLYGON = $options{ 'f' };
my $INITIAL_POLYGON = $options{ 'i' } || 'R';
my $IMAGE_ROTATE = $options{ 'o' } || 0;
my $DUMP_DATA = $options{ 'p' };
my $DISPLAY_RADIUS = $options{ 'r' };
my $LEAVE_PS_TEMP = $options{ 't' };

my $IMAGE_DEPTH = $options{ 'D' } || IMAGE_DEPTH;
my $IMAGE_WIDTH = $options{ 'W' } || IMAGE_WIDTH;
my $OUTPUT_FILE = $options{ 'F' } || 'tiling.pdf';
my $DRAWING_SCHEME = $options{ 'S' } || 'line';

help( "'$INITIAL_POLYGON' is not a supported initial polygon type" ) if ( 
    ( $INITIAL_POLYGON ne 'H' ) &&
    ( $INITIAL_POLYGON ne 'R' ) &&
    ( $INITIAL_POLYGON ne 'S' )
);

help( "'$DRAWING_SCHEME' is not a supported drawing scheme" ) if ( 
    ( $DRAWING_SCHEME ne 'line' ) &&
    ( $DRAWING_SCHEME ne 'fill' )
);

############################################################
# Setup the environment

$| = 1;

my $IMAGE_SIZE = [ $IMAGE_WIDTH, $IMAGE_DEPTH ];
my $DRAWING_OFFSET = [ 0, 0 ];

my $POLYGON_LOOKUP = {};

my $POLYGONS = [];
my $NEXT_POLYGON_ID = 1;

setupInitalPolygon( $INITIAL_POLYGON );

############################################################
# Process the tiling

my $CURRENT_DEFLATION = 0;

while ( $CURRENT_DEFLATION < $DEFLATIONS ) {

    my $new_polygons = [];

    for ( @{ $POLYGONS } ) {

          isHexagon( $_ )  ? subdivideHexagon( $_, $new_polygons )
        : isRhombus( $_ )  ? subdivideRhombus( $_, $new_polygons )
        : isSquare( $_ )   ? subdivideSquare( $_, $new_polygons )
        :                    push @{ $new_polygons }, $_
          ;
    #    push @{ $new_polygons }, $_;
    }

    for my $polygon ( @{ $new_polygons } ) {

        next if isHexagon( $polygon );
        reversePolygonVertices( $polygon ) if isClockwisePolygon( $polygon );
    }

    $POLYGONS = $new_polygons;
    
    $CURRENT_DEFLATION++;
}

for my $polygon ( @{ $POLYGONS } ) {

    $POLYGON_LOOKUP->{ $polygon->{ 'ID'} } = $polygon;
}

drawImage();

dumpPolygons() if $DUMP_DATA;

exit;

############################################################
# Simple mod/min/max functions

sub isHexagon { return shift->{ 'TYPE' } eq POLYGON_TYPE_HEXAGON }
sub isRhombus { return shift->{ 'TYPE' } eq POLYGON_TYPE_RHOMB }
sub isSquare  { return shift->{ 'TYPE' } eq POLYGON_TYPE_SQUARE }

############################################################
# Subdivide a Rhombus

sub subdivideRhombus {

    my $polygon = shift;
    my $new_polygons = shift;

    my $vertices = $polygon->{ 'VERTICES' };

    my $new_side = distance( $vertices->[ 0 ], $vertices->[ 1 ] ) * SIDE_SCALE;

    my $waist = $new_side * SIDE_SCALE;
    my $rhomb_midpt = pointAlong( $vertices->[ 1 ], $vertices->[ 3 ], 0.5 );

    my $new_rhomb = {

        'ID'        => $NEXT_POLYGON_ID++,
        'TYPE'      => POLYGON_TYPE_RHOMB,
        'PARENT'    => $polygon->{ 'ID' },
        'VERTICES'  => [
                $vertices->[ 1 ],
                pointPerpendicularTo( $vertices->[ 3 ], $rhomb_midpt, $waist ),
                $vertices->[ 3 ],
                pointPerpendicularTo( $vertices->[ 1 ], $rhomb_midpt, $waist ),
        ]
    };

    push @{ $new_polygons }, $new_rhomb;

    my $rhomb_vertices = $new_rhomb->{ 'VERTICES' };
    my $new_squares = [];

    for ( my $s = 0; $s < 4 ; $s++ ) {

        my $new_square = {

            'ID'        => $NEXT_POLYGON_ID++,
            'TYPE'      => POLYGON_TYPE_SQUARE,
            'PARENT'    => $polygon->{ 'ID' },
            'VERTICES'  => [
                    $rhomb_vertices->[ $s - 1 ],
                    pointPerpendicularTo( $rhomb_vertices->[ $s ], $rhomb_vertices->[ $s - 1 ], $new_side ),
                    pointPerpendicularTo( $rhomb_vertices->[ $s - 1 ], $rhomb_vertices->[ $s ], -1.0 * $new_side ),
                    $rhomb_vertices->[ $s ],
            ]
        };

        rollPolygonVertices( $new_square, 1 + ( $s % 2 ) );

        push @{ $new_squares }, $new_square;
    }

    push @{ $new_polygons }, @{ $new_squares };

    my $sq1_vertices = $new_squares->[ 1 ]->{ 'VERTICES' };
    my $v = ROOT_2 * $new_side / distance( $vertices->[ 2 ], $rhomb_midpt );

    $new_rhomb = {

        'ID'        => $NEXT_POLYGON_ID++,
        'TYPE'      => POLYGON_TYPE_RHOMB,
        'PARENT'    => $polygon->{ 'ID' },
        'VERTICES'  => [
                $new_squares->[ 1 ]->{ 'VERTICES' }->[ 1 ],
                $new_squares->[ 1 ]->{ 'VERTICES' }->[ 0 ],
                pointAlong( $vertices->[ 2 ], $rhomb_midpt, $v ),
                $new_squares->[ 2 ]->{ 'VERTICES' }->[ 0 ],
        ]
    };
    push @{ $new_polygons }, $new_rhomb;

    my $midpt = pointAlong( $new_rhomb->{ 'VERTICES' }->[ 2 ], $vertices->[ 2 ], 0.5 );

    push @{ $new_polygons }, {

        'ID'        => $NEXT_POLYGON_ID++,
        'TYPE'      => POLYGON_TYPE_SQUARE,
        'PARENT'    => $polygon->{ 'ID' },
        'VERTICES'  => [
                $new_rhomb->{ 'VERTICES' }->[ 2 ],
                pointPerpendicularTo( $vertices->[ 2 ], $midpt, $new_side / ROOT_2 ),
                $vertices->[ 2 ],
                pointPerpendicularTo( $new_rhomb->{ 'VERTICES' }->[ 2 ], $midpt, $new_side / ROOT_2 ),
        ]
    };

    $new_rhomb = {

        'ID'        => $NEXT_POLYGON_ID++,
        'TYPE'      => POLYGON_TYPE_RHOMB,
        'PARENT'    => $polygon->{ 'ID' },
        'VERTICES'  => [
                $new_squares->[ 3 ]->{ 'VERTICES' }->[ 1 ],
                $new_squares->[ 3 ]->{ 'VERTICES' }->[ 0 ],
                pointAlong( $vertices->[ 0 ], $rhomb_midpt, $v ),
                $new_squares->[ 0 ]->{ 'VERTICES' }->[ 0 ],
        ]
    };
    push @{ $new_polygons }, $new_rhomb;

    $midpt = pointAlong( $new_rhomb->{ 'VERTICES' }->[ 2 ], $vertices->[ 0 ], 0.5 );

    push @{ $new_polygons }, {

        'ID'        => $NEXT_POLYGON_ID++,
        'TYPE'      => POLYGON_TYPE_SQUARE,
        'PARENT'    => $polygon->{ 'ID' },
        'VERTICES'  => [
                $new_rhomb->{ 'VERTICES' }->[ 2 ],
                pointPerpendicularTo( $vertices->[ 0 ], $midpt, $new_side / ROOT_2 ),
                $vertices->[ 0 ],
                pointPerpendicularTo( $new_rhomb->{ 'VERTICES' }->[ 2 ], $midpt, $new_side / ROOT_2 ),
        ]
    };

    return;
}

############################################################
# Subdivide a Square

sub subdivideSquare {

    my $polygon = shift;
    my $new_polygons = shift;

    my $vertices = $polygon->{ 'VERTICES' };

    my $new_side = distance( $vertices->[ 0 ], $vertices->[ 1 ] ) * SIDE_SCALE;

    my $square_midpt = pointAlong( $vertices->[ 0 ], $vertices->[ 2 ], 0.5 );

    my $v = ( ROOT_3 - 1.0 ) * $new_side;

    my $new_square = {

        'ID'        => $NEXT_POLYGON_ID++,
        'TYPE'      => POLYGON_TYPE_SQUARE,
        'PARENT'    => $polygon->{ 'ID' },
        'VERTICES'  => [
                pointAlong( $square_midpt, $vertices->[ 0 ], SIDE_SCALE ),
                pointAlong( $square_midpt, $vertices->[ 1 ], SIDE_SCALE ),
                pointAlong( $square_midpt, $vertices->[ 2 ], SIDE_SCALE ),
                pointAlong( $square_midpt, $vertices->[ 3 ], SIDE_SCALE ),
        ]
    };
    push @{ $new_polygons }, $new_square;
    
    my $waist = $new_side * ( ROOT_3 - 1.0 ) / ( 2.0 * ROOT_2 );

    my $new_rhombs = [];
    for ( my $r = 0; $r < 4 ; $r++ ) {

        my $midpt = pointAlong( $new_square->{ 'VERTICES' }->[ $r ], $vertices->[ $r ], 0.5 );

        my $new_rhomb = {
    
            'ID'        => $NEXT_POLYGON_ID++,
            'TYPE'      => POLYGON_TYPE_RHOMB,
            'PARENT'    => $polygon->{ 'ID' },
            'VERTICES'  => [
                    $new_square->{ 'VERTICES' }->[ $r ],
                    pointPerpendicularTo( $vertices->[ $r ], $midpt, $waist ),
                    $vertices->[ $r ],
                    pointPerpendicularTo( $new_square->{ 'VERTICES' }->[ $r ], $midpt, $waist ),
            ]
        };
    
        push @{ $new_rhombs }, $new_rhomb;
    }
    push @{ $new_polygons }, @{ $new_rhombs };

    for ( my $h = 0; $h < 4; $h++ ) {

        my $hex_midpt = pointAlong(
                            $new_rhombs->[ $h - 1 ]->{ 'VERTICES' }->[ 3 ],
                            $new_rhombs->[ $h ]->{ 'VERTICES' }->[ 1 ],
                            0.5
        );

            my $hexagon = {
    
                'ID'        => $NEXT_POLYGON_ID++,
                'TYPE'      => POLYGON_TYPE_HEXAGON,
                'PARENT'    => $polygon->{ 'ID' },
                'VERTICES'  => [
                        pointAlong( $new_square->{ 'VERTICES' }->[ $h ], $hex_midpt, 2.0 ),
                        pointAlong( $new_square->{ 'VERTICES' }->[ $h - 1 ], $hex_midpt, 2.0 ),
                        $new_rhombs->[ $h ]->{ 'VERTICES' }->[ 1 ],
                        $new_square->{ 'VERTICES' }->[ $h ],
                        $new_square->{ 'VERTICES' }->[ $h - 1 ],
                        $new_rhombs->[ $h - 1 ]->{ 'VERTICES' }->[ 3 ],
                ]
            };

            reversePolygonVertices( $hexagon ) if ( $h == 0 );
            reversePolygonVertices( $hexagon ) if ( $h == 3 );
            rollPolygonVertices( $hexagon, 1 ) if ( $h == 1 );
            rollPolygonVertices( $hexagon, 1 ) if ( $h == 2 );

            push @{ $new_polygons }, $hexagon;
    }

    return;
}

############################################################
# Subdivide a Hexagon

sub subdivideHexagon {

    my $polygon = shift;
    my $new_polygons = shift;

    my $vertices = $polygon->{ 'VERTICES' };

    my $new_side = distance( $vertices->[ 0 ], $vertices->[ 1 ] ) * SIDE_SCALE;

    my $hexagon_midpt = pointAlong( $vertices->[ 0 ], $vertices->[ 3 ], 0.5 );

    my $new_hexagon = {

        'ID'        => $NEXT_POLYGON_ID++,
        'TYPE'      => POLYGON_TYPE_HEXAGON,
        'PARENT'    => $polygon->{ 'ID' },
        'VERTICES'  => [
                pointAlong( $hexagon_midpt, $vertices->[ 4 ], SIDE_SCALE ),
                pointAlong( $hexagon_midpt, $vertices->[ 3 ], SIDE_SCALE ),
                pointAlong( $hexagon_midpt, $vertices->[ 2 ], SIDE_SCALE ),
                pointAlong( $hexagon_midpt, $vertices->[ 1 ], SIDE_SCALE ),
                pointAlong( $hexagon_midpt, $vertices->[ 0 ], SIDE_SCALE ),
                pointAlong( $hexagon_midpt, $vertices->[ 5 ], SIDE_SCALE ),
        ]
    };
    push @{ $new_polygons }, $new_hexagon;

    my $direction = isClockwisePolygon( $polygon ) ? -1 : 1;

    my $new_squares = [];
    for ( my $s = 0; $s < 6; $s += 2 ) {

        my $new_square = {

            'ID'        => $NEXT_POLYGON_ID++,
            'TYPE'      => POLYGON_TYPE_SQUARE,
            'PARENT'    => $polygon->{ 'ID' },
            'VERTICES'  => [
                    $new_hexagon->{ 'VERTICES' }->[ $s ],
                    $new_hexagon->{ 'VERTICES' }->[ ( $s + 1 ) % 6 ],
                    pointPerpendicularTo( 
                            $new_hexagon->{ 'VERTICES' }->[ $s ],
                            $new_hexagon->{ 'VERTICES' }->[ ( $s + 1 ) % 6 ],
                            $direction * $new_side,
                    ),
                    pointPerpendicularTo( 
                            $new_hexagon->{ 'VERTICES' }->[ ( $s + 1 ) % 6 ],
                            $new_hexagon->{ 'VERTICES' }->[ $s ],
                            -1.0 * $direction * $new_side,
                    ),
            ]
        };
    
        rollPolygonVertices( $new_square, 1 ) if ( $s > 1 );

        push @{ $new_squares }, $new_square;
    }

    push @{ $new_polygons }, @{ $new_squares };
 
    my $square_midpt = pointAlong( 
                            $new_squares->[ 2 ]->{ 'VERTICES' }->[ 0 ],
                            $new_squares->[ 2 ]->{ 'VERTICES' }->[ 2 ],
                            0.5,
    );

    my $top_hexagon = {

        'ID'        => $NEXT_POLYGON_ID++,
        'TYPE'      => POLYGON_TYPE_HEXAGON,
        'PARENT'    => $polygon->{ 'ID' },
        'VERTICES'  => [
                $new_squares->[ 2 ]->{ 'VERTICES' }->[ 2 ],
                $new_squares->[ 2 ]->{ 'VERTICES' }->[ 1 ],
                pointAlong( $new_hexagon->{ 'VERTICES' }->[ 3 ], $square_midpt, 2 ),
                pointAlong( $new_hexagon->{ 'VERTICES' }->[ 2 ], $square_midpt, 2 ),
                pointAlong( $new_hexagon->{ 'VERTICES' }->[ 1 ], $square_midpt, 2 ),
                pointAlong( $new_hexagon->{ 'VERTICES' }->[ 0 ], $square_midpt, 2 ),
        ]
    };
    rollPolygonVertices( $top_hexagon, -2 );
    push @{ $new_polygons }, $top_hexagon;
   
    $square_midpt = pointAlong( 
                            $new_squares->[ 0 ]->{ 'VERTICES' }->[ 0 ],
                            $new_squares->[ 0 ]->{ 'VERTICES' }->[ 2 ],
                            0.5,
    );

    my $right_hexagon = {

        'ID'        => $NEXT_POLYGON_ID++,
        'TYPE'      => POLYGON_TYPE_HEXAGON,
        'PARENT'    => $polygon->{ 'ID' },
        'VERTICES'  => [
                pointAlong( $new_hexagon->{ 'VERTICES' }->[ 4 ], $square_midpt, 2 ),
                pointAlong( $new_hexagon->{ 'VERTICES' }->[ 5 ], $square_midpt, 2 ),
                $new_squares->[ 0 ]->{ 'VERTICES' }->[ 2 ],
                $new_squares->[ 0 ]->{ 'VERTICES' }->[ 3 ],
                pointAlong( $new_hexagon->{ 'VERTICES' }->[ 2 ], $square_midpt, 2 ),
                pointAlong( $new_hexagon->{ 'VERTICES' }->[ 3 ], $square_midpt, 2 ),
        ]
    };
    push @{ $new_polygons }, $right_hexagon;

    $square_midpt = pointAlong( 
                            $new_squares->[ 1 ]->{ 'VERTICES' }->[ 0 ],
                            $new_squares->[ 1 ]->{ 'VERTICES' }->[ 2 ],
                            0.5,
    );

    my $left_hexagon = {

        'ID'        => $NEXT_POLYGON_ID++,
        'TYPE'      => POLYGON_TYPE_HEXAGON,
        'PARENT'    => $polygon->{ 'ID' },
        'VERTICES'  => [
                $new_squares->[ 1 ]->{ 'VERTICES' }->[ 2 ],
                $new_squares->[ 1 ]->{ 'VERTICES' }->[ 1 ],
                pointAlong( $new_hexagon->{ 'VERTICES' }->[ 1 ], $square_midpt, 2 ),
                pointAlong( $new_hexagon->{ 'VERTICES' }->[ 0 ], $square_midpt, 2 ),
                pointAlong( $new_hexagon->{ 'VERTICES' }->[ 5 ], $square_midpt, 2 ),
                pointAlong( $new_hexagon->{ 'VERTICES' }->[ 4 ], $square_midpt, 2 ),
        ]
    };
    rollPolygonVertices( $left_hexagon, -2 );
    push @{ $new_polygons }, $left_hexagon;
 
    my $new_rhombs = [];
    my $waist = $new_side * ( ROOT_3 - 1.0 ) / ( 2.0 * ROOT_2 );

    my $midpt = pointAlong(
                    $new_hexagon->{ 'VERTICES' }->[ 3 ],
                    $new_squares->[ 2 ]->{ 'VERTICES' }->[ 2 ],
                    0.5
    );

    push @{ $new_rhombs }, {

        'ID'        => $NEXT_POLYGON_ID++,
        'TYPE'      => POLYGON_TYPE_RHOMB,
        'PARENT'    => $polygon->{ 'ID' },
        'VERTICES'  => [
                $new_squares->[ 2 ]->{ 'VERTICES' }->[ 2 ],
                pointPerpendicularTo( $new_hexagon->{ 'VERTICES' }->[ 3 ], $midpt, $direction * $waist ),
                $new_hexagon->{ 'VERTICES' }->[ 3 ],
                $new_squares->[ 2 ]->{ 'VERTICES' }->[ 3 ],
        ],
    };

    $midpt = pointAlong(
                    $new_hexagon->{ 'VERTICES' }->[ 0 ],
                    $new_squares->[ 2 ]->{ 'VERTICES' }->[ 1 ],
                    0.5
    );

    push @{ $new_rhombs }, {

        'ID'        => $NEXT_POLYGON_ID++,
        'TYPE'      => POLYGON_TYPE_RHOMB,
        'PARENT'    => $polygon->{ 'ID' },
        'VERTICES'  => [
                $new_squares->[ 2 ]->{ 'VERTICES' }->[ 1 ],
                $new_squares->[ 2 ]->{ 'VERTICES' }->[ 0 ],
                $new_hexagon->{ 'VERTICES' }->[ 0 ],
                pointPerpendicularTo( $new_squares->[ 2 ]->{ 'VERTICES' }->[ 1 ], $midpt, $direction * $waist ),
        ],
    };

    $midpt = pointAlong(
                    $new_hexagon->{ 'VERTICES' }->[ 2 ],
                    $new_squares->[ 0 ]->{ 'VERTICES' }->[ 2 ],
                    0.5
    );

    push @{ $new_rhombs }, {

        'ID'        => $NEXT_POLYGON_ID++,
        'TYPE'      => POLYGON_TYPE_RHOMB,
        'PARENT'    => $polygon->{ 'ID' },
        'VERTICES'  => [
                $new_hexagon->{ 'VERTICES' }->[ 2 ],
                pointPerpendicularTo( $new_squares->[ 0 ]->{ 'VERTICES' }->[ 2 ], $midpt, $direction * $waist ),
                $new_squares->[ 0 ]->{ 'VERTICES' }->[ 2 ],
                $new_squares->[ 0 ]->{ 'VERTICES' }->[ 1 ],
        ],
    };

    $midpt = pointAlong(
                    $vertices->[ 2 ],
                    $left_hexagon->{ 'VERTICES' }->[ 2 ],
                    0.5
    );

    push @{ $new_rhombs }, {

        'ID'        => $NEXT_POLYGON_ID++,
        'TYPE'      => POLYGON_TYPE_RHOMB,
        'PARENT'    => $polygon->{ 'ID' },
        'VERTICES'  => [
                $vertices->[ 2 ],
                pointPerpendicularTo( $vertices->[ 2 ], $midpt, -1.0 * $direction * $waist ),
                $left_hexagon->{ 'VERTICES' }->[ 2 ],
                $left_hexagon->{ 'VERTICES' }->[ 1 ],
        ],
    };

    $midpt = pointAlong(
                    $vertices->[ 3 ],
                    $new_squares->[ 0 ]->{ 'VERTICES' }->[ 2 ],
                    0.5
    );

    push @{ $new_rhombs }, {

        'ID'        => $NEXT_POLYGON_ID++,
        'TYPE'      => POLYGON_TYPE_RHOMB,
        'PARENT'    => $polygon->{ 'ID' },
        'VERTICES'  => [
                $vertices->[ 3 ],
                $right_hexagon->{ 'VERTICES' }->[ 1 ],
                $new_squares->[ 0 ]->{ 'VERTICES' }->[ 2 ],
                pointPerpendicularTo( $new_squares->[ 0 ]->{ 'VERTICES' }->[ 2 ], $midpt, -1.0 * $direction * $waist ),
        ],
    };

    $midpt = pointAlong(
                    $vertices->[ 4 ],
                    $new_squares->[ 0 ]->{ 'VERTICES' }->[ 3 ],
                    0.5
    );

    push @{ $new_rhombs }, {

        'ID'        => $NEXT_POLYGON_ID++,
        'TYPE'      => POLYGON_TYPE_RHOMB,
        'PARENT'    => $polygon->{ 'ID' },
        'VERTICES'  => [
                $new_squares->[ 0 ]->{ 'VERTICES' }->[ 3 ],
                $right_hexagon->{ 'VERTICES' }->[ 4 ],
                $vertices->[ 4 ],
                pointPerpendicularTo( $new_squares->[ 0 ]->{ 'VERTICES' }->[ 3 ], $midpt, $direction * $waist ),
        ],
    };

    $midpt = pointAlong(
                    $vertices->[ 5 ],
                    $new_squares->[ 2 ]->{ 'VERTICES' }->[ 1 ],
                    0.5
    );

    push @{ $new_rhombs }, {

        'ID'        => $NEXT_POLYGON_ID++,
        'TYPE'      => POLYGON_TYPE_RHOMB,
        'PARENT'    => $polygon->{ 'ID' },
        'VERTICES'  => [
                $new_squares->[ 2 ]->{ 'VERTICES' }->[ 1 ],
                pointPerpendicularTo( $new_squares->[ 2 ]->{ 'VERTICES' }->[ 1 ], $midpt, -1.0 * $direction * $waist ),
                $vertices->[ 5 ],
                $top_hexagon->{ 'VERTICES' }->[ 4 ],
        ],
    };

    $midpt = pointAlong(
                    $vertices->[ 0 ],
                    $new_squares->[ 2 ]->{ 'VERTICES' }->[ 2 ],
                    0.5
    );

    push @{ $new_rhombs }, {

        'ID'        => $NEXT_POLYGON_ID++,
        'TYPE'      => POLYGON_TYPE_RHOMB,
        'PARENT'    => $polygon->{ 'ID' },
        'VERTICES'  => [
                $new_squares->[ 2 ]->{ 'VERTICES' }->[ 2 ],
                $top_hexagon->{ 'VERTICES' }->[ 1 ],
                $vertices->[ 0 ],
                pointPerpendicularTo( $vertices->[ 0 ], $midpt, -1.0 * $direction * $waist ),
        ],
    };

    $midpt = pointAlong(
                    $vertices->[ 1 ],
                    $new_squares->[ 1 ]->{ 'VERTICES' }->[ 1 ],
                    0.5
    );

    push @{ $new_rhombs }, {

        'ID'        => $NEXT_POLYGON_ID++,
        'TYPE'      => POLYGON_TYPE_RHOMB,
        'PARENT'    => $polygon->{ 'ID' },
        'VERTICES'  => [
                $new_squares->[ 1 ]->{ 'VERTICES' }->[ 1 ],
                pointPerpendicularTo( $new_squares->[ 1 ]->{ 'VERTICES' }->[ 1 ], $midpt, -1.0 * $direction * $waist ),
                $vertices->[ 1 ],
                $left_hexagon->{ 'VERTICES' }->[ 4 ],
        ],
    };

    push @{ $new_polygons }, @{ $new_rhombs };

    my $new_hexagons = [];

    $midpt = pointAlong(
                    $new_rhombs->[ 2 ]->{ 'VERTICES' }->[ 1 ],
                    $new_rhombs->[ 3 ]->{ 'VERTICES' }->[ 1 ],
                    0.5
    );

    push @{ $new_hexagons }, {

        'ID'        => $NEXT_POLYGON_ID++,
        'TYPE'      => POLYGON_TYPE_HEXAGON,
        'PARENT'    => $polygon->{ 'ID' },
        'VERTICES'  => [
                pointAlong( $new_squares->[ 1 ]->{ 'VERTICES' }->[ 3 ], $midpt, 2.0 ),
                $new_rhombs->[ 3 ]->{ 'VERTICES' }->[ 1 ],
                $new_squares->[ 1 ]->{ 'VERTICES' }->[ 2 ],
                $new_squares->[ 1 ]->{ 'VERTICES' }->[ 3 ],
                $new_rhombs->[ 2 ]->{ 'VERTICES' }->[ 1 ],
                pointAlong( $new_squares->[ 1 ]->{ 'VERTICES' }->[ 2 ], $midpt, 2.0 ),
        ],
    };

    $midpt = pointAlong(
                    $new_rhombs->[ 0 ]->{ 'VERTICES' }->[ 1 ],
                    $new_rhombs->[ 8 ]->{ 'VERTICES' }->[ 1 ],
                    0.5
    );

    push @{ $new_hexagons }, {

        'ID'        => $NEXT_POLYGON_ID++,
        'TYPE'      => POLYGON_TYPE_HEXAGON,
        'PARENT'    => $polygon->{ 'ID' },
        'VERTICES'  => [
                pointAlong( $new_squares->[ 1 ]->{ 'VERTICES' }->[ 0 ], $midpt, 2.0 ),
                $new_rhombs->[ 8 ]->{ 'VERTICES' }->[ 1 ],
                $new_squares->[ 1 ]->{ 'VERTICES' }->[ 1 ],
                $new_squares->[ 1 ]->{ 'VERTICES' }->[ 0 ],
                $new_rhombs->[ 0 ]->{ 'VERTICES' }->[ 1 ],
                pointAlong( $new_squares->[ 1 ]->{ 'VERTICES' }->[ 1 ], $midpt, 2.0 ),
        ],
    };

    $midpt = pointAlong(
                    $new_rhombs->[ 1 ]->{ 'VERTICES' }->[ 3 ],
                    $new_rhombs->[ 5 ]->{ 'VERTICES' }->[ 3 ],
                    0.5
    );

    push @{ $new_hexagons }, {

        'ID'        => $NEXT_POLYGON_ID++,
        'TYPE'      => POLYGON_TYPE_HEXAGON,
        'PARENT'    => $polygon->{ 'ID' },
        'VERTICES'  => [
                pointAlong( $new_squares->[ 0 ]->{ 'VERTICES' }->[ 0 ], $midpt, 2.0 ),
                $new_rhombs->[ 5 ]->{ 'VERTICES' }->[ 3 ],
                $new_squares->[ 0 ]->{ 'VERTICES' }->[ 3 ],
                $new_squares->[ 0 ]->{ 'VERTICES' }->[ 0 ],
                $new_rhombs->[ 1 ]->{ 'VERTICES' }->[ 3 ],
                pointAlong( $new_squares->[ 0 ]->{ 'VERTICES' }->[ 3 ], $midpt, 2.0 ),
        ],
    };

    push @{ $new_polygons }, @{ $new_hexagons };

    my $new_squares2 = [];

    push @{ $new_squares2 }, {

        'ID'        => $NEXT_POLYGON_ID++,
        'TYPE'      => POLYGON_TYPE_SQUARE,
        'PARENT'    => $polygon->{ 'ID' },
        'VERTICES'  => [
                $new_hexagons->[ 2 ]->{ 'VERTICES' }->[ 5 ],
                $new_rhombs->[ 6 ]->{ 'VERTICES' }->[ 1 ],
                $new_rhombs->[ 6 ]->{ 'VERTICES' }->[ 0 ],
                $new_hexagons->[ 2 ]->{ 'VERTICES' }->[ 4 ],
        ],
    };

    push @{ $new_squares2 }, {

        'ID'        => $NEXT_POLYGON_ID++,
        'TYPE'      => POLYGON_TYPE_SQUARE,
        'PARENT'    => $polygon->{ 'ID' },
        'VERTICES'  => [
                $new_hexagons->[ 1 ]->{ 'VERTICES' }->[ 5 ],
                $new_hexagons->[ 1 ]->{ 'VERTICES' }->[ 4 ],
                $new_rhombs->[ 7 ]->{ 'VERTICES' }->[ 0 ],
                $new_rhombs->[ 7 ]->{ 'VERTICES' }->[ 3 ],
        ],
    };

    push @{ $new_squares2 }, {

        'ID'        => $NEXT_POLYGON_ID++,
        'TYPE'      => POLYGON_TYPE_SQUARE,
        'PARENT'    => $polygon->{ 'ID' },
        'VERTICES'  => [
                $new_hexagons->[ 0 ]->{ 'VERTICES' }->[ 5 ],
                $new_rhombs->[ 4 ]->{ 'VERTICES' }->[ 3 ],
                $new_rhombs->[ 4 ]->{ 'VERTICES' }->[ 2 ],
                $new_hexagons->[ 0 ]->{ 'VERTICES' }->[ 4 ],
        ],
    };

    push @{ $new_squares2 }, {

        'ID'        => $NEXT_POLYGON_ID++,
        'TYPE'      => POLYGON_TYPE_SQUARE,
        'PARENT'    => $polygon->{ 'ID' },
        'VERTICES'  => [
                $new_hexagons->[ 2 ]->{ 'VERTICES' }->[ 0 ],
                $new_hexagons->[ 2 ]->{ 'VERTICES' }->[ 1 ],
                $new_rhombs->[ 5 ]->{ 'VERTICES' }->[ 2 ],
                pointPerpendicularTo( 
                            $new_hexagons->[ 2 ]->{ 'VERTICES' }->[ 1 ],
                            $new_hexagons->[ 2 ]->{ 'VERTICES' }->[ 0 ],
                            -1.0 * $direction * $new_side
                ),
        ],
    };

    push @{ $new_squares2 }, {

        'ID'        => $NEXT_POLYGON_ID++,
        'TYPE'      => POLYGON_TYPE_SQUARE,
        'PARENT'    => $polygon->{ 'ID' },
        'VERTICES'  => [
                $new_hexagons->[ 0 ]->{ 'VERTICES' }->[ 0 ],
                $new_hexagons->[ 0 ]->{ 'VERTICES' }->[ 1 ],
                $new_rhombs->[ 3 ]->{ 'VERTICES' }->[ 0 ],
                pointPerpendicularTo( 
                            $new_hexagons->[ 0 ]->{ 'VERTICES' }->[ 1 ],
                            $new_hexagons->[ 0 ]->{ 'VERTICES' }->[ 0 ],
                            -1.0 * $direction * $new_side
                ),
        ],
    };

    push @{ $new_squares2 }, {

        'ID'        => $NEXT_POLYGON_ID++,
        'TYPE'      => POLYGON_TYPE_SQUARE,
        'PARENT'    => $polygon->{ 'ID' },
        'VERTICES'  => [
                $new_hexagons->[ 1 ]->{ 'VERTICES' }->[ 0 ],
                pointPerpendicularTo( 
                            $new_hexagons->[ 1 ]->{ 'VERTICES' }->[ 1 ],
                            $new_hexagons->[ 1 ]->{ 'VERTICES' }->[ 0 ],
                            $direction * $new_side
                ),
                $new_rhombs->[ 8 ]->{ 'VERTICES' }->[ 2 ],
                $new_hexagons->[ 1 ]->{ 'VERTICES' }->[ 1 ],
        ],
    };

    push @{ $new_squares2 }, {

        'ID'        => $NEXT_POLYGON_ID++,
        'TYPE'      => POLYGON_TYPE_SQUARE,
        'PARENT'    => $polygon->{ 'ID' },
        'VERTICES'  => [
                pointPerpendicularTo( 
                            $new_rhombs->[ 4 ]->{ 'VERTICES' }->[ 0 ],
                            $new_rhombs->[ 4 ]->{ 'VERTICES' }->[ 3 ],
                            $direction * $new_side
                ),
                pointPerpendicularTo( 
                            $new_rhombs->[ 4 ]->{ 'VERTICES' }->[ 3 ],
                            $new_rhombs->[ 4 ]->{ 'VERTICES' }->[ 0 ],
                            -1.0 * $direction * $new_side
                ),
                $new_rhombs->[ 4 ]->{ 'VERTICES' }->[ 0 ],
                $new_rhombs->[ 4 ]->{ 'VERTICES' }->[ 3 ],
        ],
    };

    push @{ $new_squares2 }, {

        'ID'        => $NEXT_POLYGON_ID++,
        'TYPE'      => POLYGON_TYPE_SQUARE,
        'PARENT'    => $polygon->{ 'ID' },
        'VERTICES'  => [
                pointPerpendicularTo( 
                            $new_rhombs->[ 7 ]->{ 'VERTICES' }->[ 2 ],
                            $new_rhombs->[ 7 ]->{ 'VERTICES' }->[ 3 ],
                            -1.0 * $direction * $new_side
                ),
                $new_rhombs->[ 7 ]->{ 'VERTICES' }->[ 3 ],
                $new_rhombs->[ 7 ]->{ 'VERTICES' }->[ 2 ],
                pointPerpendicularTo( 
                            $new_rhombs->[ 7 ]->{ 'VERTICES' }->[ 3 ],
                            $new_rhombs->[ 7 ]->{ 'VERTICES' }->[ 2 ],
                            $direction * $new_side
                ),
        ],
    };

    push @{ $new_squares2 }, {

        'ID'        => $NEXT_POLYGON_ID++,
        'TYPE'      => POLYGON_TYPE_SQUARE,
        'PARENT'    => $polygon->{ 'ID' },
        'VERTICES'  => [
                pointPerpendicularTo( 
                            $new_rhombs->[ 6 ]->{ 'VERTICES' }->[ 2 ],
                            $new_rhombs->[ 6 ]->{ 'VERTICES' }->[ 1 ],
                            $direction * $new_side
                ),
                pointPerpendicularTo( 
                            $new_rhombs->[ 6 ]->{ 'VERTICES' }->[ 1 ],
                            $new_rhombs->[ 6 ]->{ 'VERTICES' }->[ 2 ],
                            -1.0 * $direction * $new_side
                ),
                $new_rhombs->[ 6 ]->{ 'VERTICES' }->[ 2 ],
                $new_rhombs->[ 6 ]->{ 'VERTICES' }->[ 1 ],
        ],
    };

    push @{ $new_polygons }, @{ $new_squares2 };

    my $new_rhombs2 = [];

    push @{ $new_rhombs2 }, {

        'ID'        => $NEXT_POLYGON_ID++,
        'TYPE'      => POLYGON_TYPE_RHOMB,
        'PARENT'    => $polygon->{ 'ID' },
        'VERTICES'  => [
                $new_squares2->[ 1 ]->{ 'VERTICES' }->[ 3 ],
                $new_squares2->[ 7 ]->{ 'VERTICES' }->[ 0 ],
                $new_squares2->[ 5 ]->{ 'VERTICES' }->[ 0 ],
                $new_squares2->[ 1 ]->{ 'VERTICES' }->[ 0 ],
        ],
    };

    push @{ $new_rhombs2 }, {

        'ID'        => $NEXT_POLYGON_ID++,
        'TYPE'      => POLYGON_TYPE_RHOMB,
        'PARENT'    => $polygon->{ 'ID' },
        'VERTICES'  => [
                $new_squares2->[ 0 ]->{ 'VERTICES' }->[ 1 ],
                $new_squares2->[ 0 ]->{ 'VERTICES' }->[ 0 ],
                $new_squares2->[ 3 ]->{ 'VERTICES' }->[ 0 ],
                $new_squares2->[ 8 ]->{ 'VERTICES' }->[ 0 ],
        ],
    };

    push @{ $new_rhombs2 }, {

        'ID'        => $NEXT_POLYGON_ID++,
        'TYPE'      => POLYGON_TYPE_RHOMB,
        'PARENT'    => $polygon->{ 'ID' },
        'VERTICES'  => [
                $new_squares2->[ 6 ]->{ 'VERTICES' }->[ 3 ],
                $new_squares2->[ 2 ]->{ 'VERTICES' }->[ 0 ],
                $new_squares2->[ 4 ]->{ 'VERTICES' }->[ 0 ],
                $new_squares2->[ 6 ]->{ 'VERTICES' }->[ 0 ],
        ],
    };

    push @{ $new_polygons }, @{ $new_rhombs2 };

    return;
}

############################################################
# Determine if the polygon vertices are clockwise

sub isClockwisePolygon {

    my $polygon = shift;

    my $vertices = $polygon->{ 'VERTICES' };

    my $midpt = pointAlong( $vertices->[ 0 ], $vertices->[ int( (scalar @{$vertices}) / 2 ) ], 0.5 );

    my $test_point1 = pointPerpendicularTo( $vertices->[ 0 ], $vertices->[ 1 ], 5 );
    my $test_point2 = pointPerpendicularTo( $vertices->[ 0 ], $vertices->[ 1 ], -5 );

    return distance( $test_point1, $midpt ) > distance( $test_point2, $midpt );
}

############################################################
# Reverse the vertex collection of the given polygon

sub reversePolygonVertices {

    my $polygon = shift;

    my $vertices = $polygon->{ 'VERTICES' };

    my $new_vertices = [];
    push @{ $new_vertices}, $vertices->[ 0 ];

    for ( my $v = scalar( @{ $vertices } ) - 1; $v > 0; $v-- ) {
        push @{ $new_vertices}, $vertices->[ $v ];
    }

    $polygon->{ 'VERTICES' } = $new_vertices;
}

############################################################
# Roll the vertex collection of the given polygon

sub rollPolygonVertices {

    my $polygon = shift;
    my $roll = shift;

    my $vertices = $polygon->{ 'VERTICES' };

    my $new_vertices = [];
    for ( my $v = 0; $v < scalar( @{ $vertices } ); $v++ ) {

        push @{ $new_vertices}, $vertices->[ ( $v + $roll ) % scalar( @{ $vertices } ) ];
    }

    $polygon->{ 'VERTICES' } = $new_vertices;
}

############################################################
# Return the number of polygons that share the vertx

sub vertexPolygons {

    my $polygon = shift;
    my $vertex_no = shift;

    return keys %{ $polygon->{ 'VERTICES' }->[ $vertex_no ]->{ 'POLYGONS' } };
}

############################################################
# Find common polygon at a vertex - with exclusions

sub findCommonVertexPolygon {

    my $polygon = shift;
    my $vertex_no = shift;

    my $exclusions = { $polygon->{ 'ID' } => 1 };

    while ( my $exclusion = shift ) {
        $exclusions->{ $exclusion } = 1;
    }

    for ( keys %{ $polygon->{ 'VERTICES' }->[ $vertex_no ]->{ 'POLYGONS' } } ) {

        return $_ unless ( $exclusions->{ $_ } );
    }

    return -1;
}

############################################################
# Find the distance between 2 vertices

sub distance {

    my $vtx1 = shift;
    my $vtx2 = shift;

    my $dx = $vtx1->{ 'X' } - $vtx2->{ 'X' };
    my $dy = $vtx1->{ 'Y' } - $vtx2->{ 'Y' };

    return sqrt( ( $dx * $dx ) +  ( $dy * $dy ) );
}

############################################################
# Find a point on the right perpendicular v1->v2 at dist val

sub pointPerpendicularTo {

    my $vertex1 = shift;
    my $vertex2 = shift;
    my $dist = shift;

    my $val = $dist / distance( $vertex1, $vertex2 );

    return newVertex(
        $vertex2->{ 'X' } - $val * ( $vertex2->{ 'Y' } - $vertex1->{ 'Y' } ),
        $vertex2->{ 'Y' } + $val * ( $vertex2->{ 'X' } - $vertex1->{ 'X' } ),
    );
}

############################################################
# Find a point the given proportion between 2 points

sub pointAlong {

    my $vertex1 = shift;
    my $vertex2 = shift;
    my $val = shift;

    return newVertex(
            ( ( 1.0 - $val ) * $vertex1->{ 'X' } ) + ( $val * $vertex2->{ 'X' } ),
            ( ( 1.0 - $val ) * $vertex1->{ 'Y' } ) + ( $val * $vertex2->{ 'Y' } ),
    );
}

############################################################
# Add the polygons' id to each vertex polygon list

sub addVertexPolygons {

    my $polygon = shift;

    for my $vertex ( @{ $polygon->{ 'VERTICES' } } ) {

        $vertex->{ 'POLYGONS' }->{ $polygon->{ 'ID' } } = 1;
    }

    return;
}

############################################################
# Remove the id from each vertex polygon list

sub delVertexPolygons {

    my $polygon = shift;
    my $id = shift;

    for my $vertex ( @{ $polygon->{ 'VERTICES' } } ) {

        delete $vertex->{ 'POLYGONS' }->{ $id };
    }

    return;
}

############################################################
# Find the vertex number in the given polygon

sub findVertexNumber {

    my $polygon = shift;
    my $vertex = shift;

    for ( my $v = 0; $v < scalar @{ $polygon->{ 'VERTICES' } }; $v++ ) {

        return $v if ( $vertex == $polygon->{ 'VERTICES' }->[ $v ] );
    }

    return -1;
}

############################################################
# Are the 2 polygon vertices shared?

sub checkSharedVertex {

    my $polygon1 = shift;
    my $vertex_no1 = shift;
    my $polygon2 = shift;
    my $vertex_no2 = shift;

    my $vertex1 = $polygon1->{ 'VERTICES' }->[ $vertex_no1 ];
    my $vertex2 = $polygon2->{ 'VERTICES' }->[ $vertex_no2 ];

    my $dist = distance( $vertex1, $vertex2 );
    
    return 0 if ( $dist > SMALL_DIST );
    
    # Add the common polygons from vertex2 into vertex1
    for ( keys %{ $vertex2->{ 'POLYGONS' } } ) {

        $vertex1->{ 'POLYGONS' }->{ $_ } = 1;

        my $polygon = $POLYGON_LOOKUP->{ $_ };
        my $v = findVertexNumber( $polygon, $vertex2 );
        $polygon->{ 'VERTICES' }->[ $v ] = $vertex1;
    }

    $polygon2->{ 'VERTICES' }->[ $vertex_no2 ] = $vertex1;

    return 1;
}

############################################################
# Offset a vertex by the line between 1 others

sub offsetVertex {

    my $polygon = shift;
    my $vertex_no = shift;
    my $offset_polygon = shift;
    my $vertex1 = shift;
    my $vertex2 = shift;

    $polygon->{ 'VERTICES' }->[ $vertex_no ]->{ 'X' } +=
            $offset_polygon->{ 'VERTICES' }->[ $vertex1 ]->{ 'X' } -
            $offset_polygon->{ 'VERTICES' }->[ $vertex2 ]->{ 'X' };

    $polygon->{ 'VERTICES' }->[ $vertex_no ]->{ 'Y' } +=
            $offset_polygon->{ 'VERTICES' }->[ $vertex1 ]->{ 'Y' } -
            $offset_polygon->{ 'VERTICES' }->[ $vertex2 ]->{ 'Y' };

    return;
}

############################################################
# Return the closer of the polgons to the point

sub closerPolygon {

    my $point = shift;
    my $polygon1 = shift;
    my $polygon2 = shift;

    my $dist1 = distance( findPolygonCentre( $polygon1 ), $point );
    my $dist2 = distance( findPolygonCentre( $polygon2 ), $point );

    return ( $dist1 < $dist2 ) ? $polygon1 : $polygon2;
}

############################################################
# Return the number of the vertex closest to the given point

sub closerVertex {

    my $point = shift;
    my $polygon = shift;
    my $vertex1 = shift;
    my $vertex2 = shift;

    my $dist1 = distance( $point, $polygon->{ 'VERTICES' }->[ $vertex1 ] );
    my $dist2 = distance( $point, $polygon->{ 'VERTICES' }->[ $vertex2 ] );

    return ($ dist1 < $dist2 ) ? $vertex1 : $vertex2;
}

############################################################
# Setup the inital polygon

sub setupInitalPolygon {

    my $type = shift;

    my $initial_polygon;

    if ( $type eq 'H' ) {
    
        my $ang = pi / 3.0;

        $initial_polygon = {

            'ID'        => $NEXT_POLYGON_ID++,
            'TYPE'      => POLYGON_TYPE_HEXAGON,
            'PARENT'    => 0,
            'VERTICES'  => [
                newVertex( cos( 2 * $ang ), sin( 2 * $ang ) ),
                newVertex( cos( 3 * $ang ), sin( 3 * $ang ) ),
                newVertex( cos( 4 * $ang ), sin( 4 * $ang ) ),
                newVertex( cos( 5 * $ang ), sin( 5 * $ang ) ),
                newVertex( cos( 0 * $ang ), sin( 0 * $ang ) ),
                newVertex( cos( 1 * $ang ), sin( 1 * $ang ) ),
            ]
        };
    }
    elsif ( $type eq 'R' ) {

        my $ang = pi / 12;
        $initial_polygon = {

            'ID'        => $NEXT_POLYGON_ID++,
            'TYPE'      => POLYGON_TYPE_RHOMB,
            'PARENT'    => 0,
            'VERTICES'  => [
                newVertex(      cos( $ang ),                0 ),
                newVertex(                0,      sin( $ang ) ),
                newVertex( -1 * cos( $ang ),                0 ),
                newVertex(                0, -1 * sin( $ang ) ),
            ]
        };
    }
    elsif ( $type eq 'S' ) {

        $initial_polygon = {

            'ID'        => $NEXT_POLYGON_ID++,
            'TYPE'      => POLYGON_TYPE_SQUARE,
            'PARENT'    => 0,
            'VERTICES'  => [
                newVertex( -0.5,  0.5 ),
                newVertex( -0.5, -0.5 ),
                newVertex(  0.5, -0.5 ),
                newVertex(  0.5,  0.5 ),
            ]
        };
    }

    push @{ $POLYGONS }, $initial_polygon;

    $POLYGON_LOOKUP->{ $initial_polygon->{ 'ID' } } = $initial_polygon;

    return;
}

############################################################
# Return a new vertex object

sub newVertex {

   return {
        'X'         =>  shift,
        'Y'         =>  shift,
   };
}

############################################################
# Return the polygons' centre point

sub findPolygonCentre {

    my $polygon = shift;

    my $vertices = $polygon->{ 'VERTICES' };
    my $cx = 0;
    my $cy = 0;

    for ( @{ $vertices } ) {
        $cx += $_->{ 'X' };
        $cy += $_->{ 'Y' };
    }

    return newVertex(
        $cx / scalar( @{ $vertices } ),
        $cy / scalar( @{ $vertices } ),
    );
}

############################################################
# Label a polygon

sub labelPolygon {

   my $fh = shift;
   my $polygon = shift;
   my $label_vertices = shift;

   my $centre = findPolygonCentre( $polygon );

    printf $fh "gsave 
        %f %f translate
        (%s) dup stringwidth pop -2 div -3 moveto
        1 setgray
        show
        grestore\n",
                $DRAWING_SCALE * $centre->{ 'X' },
                $DRAWING_SCALE * $centre->{ 'Y' },
                $polygon->{ 'ID' },
    ; 

    return unless $label_vertices;

    my $vertices = $polygon->{ 'VERTICES' };
    for ( my $v = 0; $v < scalar @{ $vertices }; $v++ ) {

        my $font_sz = 10 / ( $DEFLATIONS ** 0.3 );

        printf $fh "gsave 
            /Arial-Bold findfont %f scalefont setfont 
            %f %f translate
            (%s) dup stringwidth pop -2 div %f moveto
            .8 setgray
            show
            grestore\n",
                $font_sz * $TEXT_MULTIPLIER,
                $DRAWING_SCALE * ( $centre->{ 'X' } + 3 * $vertices->[ $v ]->{ 'X' } ) / 4.0,
                $DRAWING_SCALE * ( $centre->{ 'Y' } + 3 * $vertices->[ $v ]->{ 'Y' } ) / 4.0,
                $v,
                $font_sz / -3,
    } 
}

############################################################
# Check drawing raduis

sub insideDrawingRadius {

    my $polygon = shift;

    return TRUE unless $DISPLAY_RADIUS;

    return ( $DRAWING_SCALE * distance( findPolygonCentre( $polygon ),
                                    { 'X' => 0, 'Y' => 0 } ) < $DISPLAY_RADIUS );
}

############################################################
# Find the color to fill the given pentagon

sub findPentagonFillColor {

    my $pentagon = shift;

    # If this polygon is at the vertex of a diamond - color it blue 
    my $boat_vertices = 0;
    my $star_vertices = 0;
    my $diamond_points_only = FALSE;

    for ( my $vertex_no = 0; $vertex_no < 5; $vertex_no++ ) {

        my $vertex = $pentagon->{ 'VERTICES' }->[ $vertex_no ];

        for ( keys %{ $vertex->{ 'POLYGONS' } } ) {

            my $polygon = $POLYGON_LOOKUP->{ $_ };

            if ( isDiamond( $polygon ) ) {
            
                my $v = findVertexNumber( $polygon, $vertex );
                $diamond_points_only = ! ( $v % 2 );
            }

            $star_vertices++ if ( isStar( $polygon ) );
            $boat_vertices++ if ( isBoat( $polygon ) );
        }
    }

    return '.12 .24 .48 setrgbcolor' if ( $diamond_points_only );
    return '1 0 0 setrgbcolor' if ( $boat_vertices == 3 );
    return '1 0 0 setrgbcolor' if ( $star_vertices == 3 );

    return '.48 setgray';
}

############################################################
# Find the color to fill the given polygon

sub findPolygonFillColor {

    my $polygon = shift;
    my $fill = shift;

    my $color = '';

    if ( $DRAWING_SCHEME eq 'fill' ) {

        $color = 
            isSquare( $polygon )    ? "1 1 0 setrgbcolor" :
            isRhombus( $polygon )     ? "0 0 1 setrgbcolor" :
            isHexagon( $polygon )   ? "1 0 0 setrgbcolor" :
            ''
        ;
    }
    elsif ( $fill ) {

        $color = ".3 setgray";
    }

    return $color ? " gsave $color fill grestore\n" : "";
}


############################################################
# Draw a polygon

sub drawPolygon {

    my $fh = shift;
    my $polygon = shift;
    my $fill = shift;

    return unless insideDrawingRadius( $polygon );

    my $vertices = $polygon->{ 'VERTICES' };

    for ( my $v = 0; $v < scalar @{ $vertices }; $v++ ) {

        printf $fh "%f %f %s ",
                    $DRAWING_SCALE * $vertices->[ $v ]->{ 'X' },
                    $DRAWING_SCALE * $vertices->[ $v ]->{ 'Y' },
                    ( $v == 0 ) ? 'mv' : 'ln'
        ;
    }

    printf $fh findPolygonFillColor( $polygon, $fill );
    printf $fh "closepath stroke\n";

    labelPolygon( $fh, $polygon, TRUE ) if ( $LABEL_POLYGONS );

    return;
}

############################################################
# Set the default line color

sub defaultLineColor {
    
    return ( $DRAWING_SCHEME eq 'fill' ) ? '0 setgray' : '1 1 0 setrgbcolor';
}

############################################################
# Draw the polygons

sub drawPolygons {

    my $fh = shift;

    for my $polygon ( @{$POLYGONS} ) {

        my $fill = $FILL_POLYGON && ( $polygon->{ 'ID' } eq $FILL_POLYGON );
        drawPolygon( $fh, $polygon, $fill ) 
    }

    return;
}

############################################################
# Write the image header

sub writeImageHeader {

    my $fh = shift;

    $DRAWING_OFFSET = [ $IMAGE_SIZE->[ 0 ] / 2.0, $IMAGE_SIZE->[ 1 ] / 2.0 ];

    if ( $CENTER_POLYGON ) {

        if ( my $polygon = $POLYGON_LOOKUP->{ $CENTER_POLYGON } ) {

            my $center = findPolygonCentre( $polygon );
            $DRAWING_OFFSET = [
                $IMAGE_SIZE->[ 0 ] / 2.0 - $DRAWING_SCALE * $center->{ 'X' }, 
                $IMAGE_SIZE->[ 0 ] / 2.0 - $DRAWING_SCALE * $center->{ 'Y' }
            ]; 
        }
        else {

            printf "Could not find polygon %s\n", $CENTER_POLYGON;
            exit -1;
        }
    }

#$DRAWING_OFFSET = [ -1568.883707, -1589.091814 ];

    my $font_sz = 12 / ( $DEFLATIONS ** 0.3 );

    printf $fh "%%!PS\n%%BoundingBox: 0 0 %f %f\n%%EndComments

        <<
            /PageSize [ %f %f ]
            /ImagingBBox null
        >> setpagedevice

        /mv { moveto } bind def
        /ln { lineto } bind def

        0 0 mv %f 0 ln %f %f ln 0 %f ln fill

        /Arial findfont %f scalefont setfont 

        %f %f translate
        %f rotate

        1 setlinejoin
        %f setlinewidth
        %s  %% default line color
    ",
        $IMAGE_WIDTH, $IMAGE_DEPTH,
        $IMAGE_SIZE->[ 0 ], $IMAGE_SIZE->[ 1 ],
        $IMAGE_WIDTH, $IMAGE_WIDTH,
        $IMAGE_DEPTH, $IMAGE_DEPTH,
        $font_sz * $TEXT_MULTIPLIER,
        $DRAWING_OFFSET->[ 0 ],
        $DRAWING_OFFSET->[ 1 ],
        $IMAGE_ROTATE,
        1 / ( $DEFLATIONS ** 0.5 ),
        defaultLineColor(),
    ;

    return;
}

############################################################
# Find intersection of lines (vtx1->vtx2) and (vtx3->vtx4)

sub findIntersection {

    my $vtx1 = shift;
    my $vtx2 = shift;
    my $vtx3 = shift;
    my $vtx4 = shift;

    my $val1 = ( $vtx1->{ 'X' } * $vtx2->{ 'Y' } - $vtx1->{ 'Y' } * $vtx2->{ 'X' } );
    my $val2 = ( $vtx3->{ 'X' } * $vtx4->{ 'Y' } - $vtx3->{ 'Y' } * $vtx4->{ 'X' } );
    my $val3 = ( ( $vtx1->{ 'X' } - $vtx2->{ 'X' } )* ( $vtx3->{ 'Y' } - $vtx4->{ 'Y' } ) -
            ( $vtx3->{ 'X' } - $vtx4->{ 'X' } )* ( $vtx1->{ 'Y' } - $vtx2->{ 'Y' } ) );

    return newVertex (

        ( $val1 * ($vtx3->{ 'X' } - $vtx4->{ 'X' } ) - $val2 * ( $vtx1->{ 'X' } - $vtx2->{ 'X' } ) ) / $val3,
        ( $val1 * ($vtx3->{ 'Y' } - $vtx4->{ 'Y' } ) - $val2 * ( $vtx1->{ 'Y' } - $vtx2->{ 'Y' } ) ) / $val3,
    )
}

############################################################
# Draw the image

sub drawImage {

    my $drawing_type = shift;

    my ( $fh, $temp_file ) = tempfile();

    writeImageHeader( $fh );

    drawPolygons( $fh );

    close $fh;

    my $convert = "/usr/local/bin/convert";
    $convert = "/usr/bin/convert" unless ( -f $convert );
    $convert = "/Applications/ImageMagick/ImageMagick-6.6.5/bin/convert" unless ( -f $convert );

    unless ( -f $convert ) {

        printf "Could not locate convert at $convert\n";
    }
    else {

        unlink $OUTPUT_FILE;
        system( "$convert $temp_file $OUTPUT_FILE" );
    }


    if( $LEAVE_PS_TEMP ) {

        printf "Temp file at $temp_file\n" ;
    }
    else {

        unlink $temp_file 
    }

    return;
}

############################################################
# Help doco

sub help {

    print "\n@_\n" if @_;

    printf "\n$0 " .
                "[-a <sz>] [-c <id>] [-d <iters>] [-f <id>] [-h] [-i <type>] [-l] " .
                "[-o <ang>] [-p] [-r <rad>] [-s <scale>] [-t] " .
                "[-D <dep>] [-F <file>] [-S <scheme>] [-W <wid>] 
    
    where
        -a  text size multiplier (default 1)
        -c  center the image on the given polygon
        -d  deflations (default 1)
        -f  fill the given polygon
        -h  this help message
        -i  initial polygon (one of H, R or S)
        -l  label the polygons
        -o  rotate the image
        -p  dump polygon data
        -r  drawing radius
        -s  drawing scale
        -t  leave the temp PS file

        -D  image depth
        -W  image width
        -F  output file
        -S  drawing scheme (see below)
        
    The following drawing schemes are supports:

        line    - outlined polygons (the default)
        fill    - simple polygon coloring
    \n";

    exit;
}

############################################################
# Return the polygon type decsription

sub getPolygonDescription {

    my $polygon = shift;

    return
            isSquare( $polygon )    ? 'Square'
          : isHexagon( $polygon )   ? 'Hexagon'
          : isRhombus( $polygon )     ? 'Rhomb'
          : '';
}

############################################################
# Dump polygon status

sub dumpPolygons {

    for my $polygon ( @{ $POLYGONS } ) {

        printf "P%s - %s (%d old) (parent P%s) consolidated\n",
                            $polygon->{ 'ID' },
                            getPolygonDescription( $polygon ),
                            $CURRENT_DEFLATION - $polygon->{ 'CREATED' },
                            $polygon->{ 'PARENT' },
                            $polygon->{ 'CONSOLIDATED' } ? '' : 'not ',
                            ;

        for ( @{ $polygon->{ 'VERTICES' } } ) {
            my $v = $_;
            printf "    %x [ ", $v;
            printf join ',', ( sort keys %{ $v->{ 'POLYGONS' } } );
            printf " ]\n";
        };
    }

    exit;
}

############################################################
# Null op

sub noop { return }

############################################################
# Report a ghastly error

sub fatal {

    printf "@_\n";
    exit -1;
}

__END__

