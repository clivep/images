#!/usr/bin/perl

use warnings;
use strict;

############################################################
#
# Convert a Delaunay Triangulation into a Voronoi diagram
#
############################################################

use Data::Dumper;
use Getopt::Std;
use JSON;
use Readonly;
use Try::Tiny;

my Readonly $DEFAULT_DEST_PDF       = '/tmp/clp.pdf';

############################################################
# Check the runline options and setup the environment

my %options;
getopts( 'dhlo:p:tv', \%options );

my $IMAGE = {
    'description'       => 'Voronoi Diagram',
    'cells'             => {},
    'temp-ps'           => '/tmp/clp.ps',
    'display-data'      => $options{ 'd' },
    'draw-labels'       => $options{ 'l' },
    'dest-pdf'          => $options{ 'o' } || $DEFAULT_DEST_PDF,
    'draw-triangles'    => $options{ 't' },
    'verbose'           => $options{ 'v' },
};

usage() if $options{ 'h' };
usage( 'Missing triangulation data file' ) unless $ARGV[0];

############################################################
# Produce the image

readTriangulationData( $ARGV[0], \%options );
processImage();
draw();

print Dumper $IMAGE if $IMAGE->{ 'display-data' };

exit;

############################################################
# Help info

sub usage {

    my $frmt = shift;

    printf( "\n$frmt\n", @_ ) if $frmt;

    print "\n$0 [-d][-h] [-l] [-o <file>] [-p <pcnt>] [-v] <file>" .
        "
        where
            -d          display image data
            -h          this help message
            -l          label the points on the image
            -o <file>   dest pdf file (default $DEFAULT_DEST_PDF)
            -p <pcnt>   image radius percentage (max 50)
            -t          draw the triangles on the image
            -v          verbose logging

        the mandatory <file> parameter is a JSON image file containing
        data for a Delaunay triangulation
    \n";

    exit;
}

############################################################
# Read image data from a file and update the IMAGE

sub readTriangulationData {

    my $triangulation_file = shift;
    my $options = shift;

    open my $fh, '<', $triangulation_file or
        usage( "Could not open %s: $!", $triangulation_file );

    my $content = do { local $/; <$fh> };

    close $fh;

    my $data;
    try {
        $data = decode_json( $content );
    }
    catch {
        usage( 'Invalid json file' );
    };

    usage( 'Not Delaunay Triangulation data' ) unless
                    $data->{ 'points' } || '' eq 'Delaunay Triangulation';

    my @elements = ( 
        'id', 'triangles', 'edges', 'points', 'image-size',
        'point-count', 'radius-pcnt',
    );

    # Recover the required elements from the data file
    for my $e (
        'id', 'triangles', 'edges', 'points', 'image-size',
        'point-count', 'radius-pcnt',
    ) {
        usage( "Data missing '%s' element", $e ) unless $data->{ $e };
        $IMAGE->{ $e } = $data->{ $e };
    }
    
    # Allow the image radius to be modified by the options
    $IMAGE->{ 'radius-pcnt' } = $options->{ 'p' } if $options->{ 'p' };
    usage( "Max value for rad is 50" ) if $IMAGE->{ 'radius-pcnt' } > 50;

    return;
}

############################################################
# Functions to retrieve image elements

sub getPoint { return $IMAGE->{ 'points' }->{ $_[0] } }
sub getEdge { return $IMAGE->{ 'edges' }->{ $_[0] } }
sub getTriangle { return $IMAGE->{ 'triangles' }->{ $_[0] } }
sub getCell { return $IMAGE->{ 'cells' }->{ $_[0] } }

############################################################
# Add a cell edge to the given triangulation edge

sub addEscapeEdge {

    my $cell = shift;
    my $eid = shift;

    my $e = getEdge( $eid );

    if ( $e->{ 'endpoint' } ) {
        push @{$cell->{ 'vertices' }}, $e->{ 'endpoint' };
        logDebug( "    using saved endpoint for edge $eid" );
        return;
    }

    my $p1 = getPoint( $e->{ 'pid1' } );
    my $x1 = $p1->{ 'x' };
    my $y1 = $p1->{ 'y' };

    my $p2 = getPoint( $e->{ 'pid2' } );
    my $x2 = $p2->{ 'x' };
    my $y2 = $p2->{ 'y' };

    my $t = getTriangle( ( keys %{$e->{ 'triangles' }} )[0] );
    logDebug( "    1. edge $eid triangle %s", ( keys %{$e->{ 'triangles' }} )[0] );

    my $v =
        ( $x1 * ( $y2 - $t->{ 'my' } ) ) -
        ( $y1 * ( $x2 - $t->{ 'mx' } ) ) +
        ( ( $x2 * $t->{ 'my' } ) - ( $t->{ 'mx' } * $y2 ) )
    ;
    my $o = ( $v < 0 ) ? 1 : -1;

    my $mx = ( $x1 + $x2 ) / 2;
    my $my = ( $y1 + $y2 ) / 2;
    my $dx = $x2 - $x1;
    my $dy = $y2 - $y1;

    my $a = ( $dx * $dx ) + ( $dy * $dy );
    my $b = 2 * $o * ( ( $dx * $my ) - ( $dy * $mx ) );
    my $c = ( $mx * $mx ) + ( $my * $my ) - 4;
    my $r = ( -1 * $b + sqrt( ( $b * $b ) - ( 4 * $a * $c ) ) ) / ( 2 * $a );

    my $ex = $mx - ( $o * $r * $dy );
    my $ey = $my + ( $o * $r * $dx );

    logDebug( "    mid pt (%f,%f)", $mx, $my );
    logDebug( "    mul    (%f,%f)", $o*$r, $o*$r );
    logDebug( "    dir    (%f,%f)", -$dy, $dx );
    logDebug( "    sign   %d", $o );
    logDebug( "    offset (%f,%f)", -1*$o*$r*$dy, $o*$r*$dx );

    # Store the endpoint in the edge element
    $e->{ 'endpoint' } = [ $ex, $ey ];

    push @{$cell->{ 'vertices' }}, [ $ex, $ey ];
    logDebug( "    edge $eid - added (%f, %f)", $ex, $ey );
    logDebug( "    dst %f", sqrt( $ex*$ex+$ey*$ey));
    
    return;
}

############################################################
# Process a boundary point

sub processPointCell {

    my $p = shift;

    logDebug( "Processing boundary point '%s'", $p->{ id } );
    my $cell = {
        'id'        =>  $p->{ id },
        'type'      => 'boundary',
        'vertices'  =>  [],
    };

    # Build a hash of triangles connected to the edges
    my %edge_triangles;
    for my $eid ( keys %{$p->{ 'edges' }} ) {
        my $e = getEdge( $eid );
        # Boundary edges have a single triangle - so append a search maker
        $edge_triangles{ $eid } = [ ( keys %{$e->{ 'triangles' }}, '-' ) ];
        logDebug( "    edge $eid: [%s,%s]", 
                            $edge_triangles{ $eid }->[0],
                            $edge_triangles{ $eid }->[1],
        );
    }

    # Start with the first edge connected to the point
    my $current_eid = (keys $p->{ 'edges' })[0];

    # The first entry for the edge will always be a triangle id
    my $first_id = $edge_triangles{ $current_eid }[0];
    logDebug( "    from edge $current_eid triangle '$first_id'" );

    my $next_id = $edge_triangles{ $current_eid }[1];
    delete $edge_triangles{ $current_eid };

    my $circuit = $first_id;
    my $t = getTriangle( $first_id );
    push @{$cell->{ 'vertices' }}, [ $t->{ 'cx' }, $t->{ 'cy' } ];

    while ( $next_id ne $first_id ) {

        usage( "Ran out of edges processing point '%s", $p->{ 'id' } )
                                                unless ( %edge_triangles );

        $circuit .= ' -> ' . $next_id;

        # Find the next vertex for the cell
        if ( $next_id eq '-' ) {
            addEscapeEdge( $cell, $current_eid );
        }
        else {
            my $t = getTriangle( $next_id );
            push @{$cell->{ 'vertices' }}, [ $t->{ 'cx' }, $t->{ 'cy' } ];
        }

        logDebug( "    looking for triangle '$next_id'" );
        my $found;
        for my $eid ( keys %edge_triangles ) {

            logDebug( "    checking edge '$eid'" );

            my $n = -1;
            if (
                ( $next_id eq $edge_triangles{ $eid }[++$n] ) ||
                ( $next_id eq $edge_triangles{ $eid }[++$n] )
            ) {
                logDebug( "    found on edge '$eid' [$n]" );

                addEscapeEdge( $cell, $eid ) if ( $next_id eq '-' );

                $next_id = $edge_triangles{ $eid }[1-$n];
                logDebug( "    next triangle is $next_id" );

                delete $edge_triangles{ $eid };
                $found = 1;
                $current_eid = $eid;
                last;
            }
        }

        usage( "Could not find triangle '$next_id'" ) unless $found;
    }
    
    $IMAGE->{ 'cells' }->{ $p->{ 'id' } } = $cell;
    logDebug( "    found circuit $circuit" );
}

############################################################
# Functions to create image elements

sub addPoint {

    my $id = $IMAGE->{ 'id' }++;

    $IMAGE->{ 'points' }->{ $id } = {
        'id'    =>  $id,
        'x'     =>  shift,
        'y'     =>  shift,
        'info'  =>  shift || '',
        'edges' =>  {},
    };

    return $id;
}

############################################################
# Process the image points

sub processImage {

    for my $pid ( keys %{$IMAGE->{ 'points' }} ) {

        my $p = getPoint( $pid );
        processPointCell( $p );
    }

    return;
}

############################################################
# Draw the current state

sub draw {

    open my $fh, '>', $IMAGE->{ 'temp-ps' } or
        usage( "Could not open %s: $!\n", $IMAGE->{ 'temp-ps' } );

    # Setup the image
    printf $fh "%%!PS\n%%%%BoundingBox: 0 0 %f %f\n%%%%EndComments\n

        /imgsz %f def
        /rad %f def

        <<
            /PageSize [ imgsz imgsz ]
            /ImagingBBox null
        >> setpagedevice

        /mv { moveto } bind def
        /ln { lineto } bind def
        /rmv { rmoveto } bind def
        /rln { rlineto } bind def

        /Ariel-Bold findfont 18 scalefont setfont

        0 0 mv imgsz 0 ln imgsz imgsz ln 0 imgsz ln fill

        1 0 0 0 setcmykcolor

        imgsz 2 div imgsz 2 div translate
    ",
    $IMAGE->{ 'image-size' }, $IMAGE->{ 'image-size' },
    $IMAGE->{ 'image-size' },
    $IMAGE->{ 'image-size' } * $IMAGE->{ 'radius-pcnt' } / 100,
    ;

    printf $fh "1 setgray
        0 5 360 {
            /a exch def
            a cos 2 mul rad mul
            a sin 2 mul rad mul
            a 0 eq { mv } { ln } ifelse
        } for
        stroke
    ";

    # Draw the cells
    for my $cid ( keys %{$IMAGE->{ 'cells' }} ) {

        printf $fh "\n%% Cell\n";
        my $c = getCell( $cid );

        my $op = 'mv';
        for my $v ( @{$c->{ 'vertices' }} ) {
            printf $fh "
                /x1 %f rad mul def
                /y1 %f rad mul def
                x1 y1 $op 
            ",
            @{$v};
            $op = 'ln';
        }
        printf $fh "closepath\n";
    }
    printf $fh "
        0 0 .1 0 setcmykcolor stroke
    ";

    # Draw the triangles
    if ( $IMAGE->{ 'draw-triangles' } ) {
            
        for my $tid ( keys %{$IMAGE->{ 'triangles' }} ) {
    
            printf $fh "\n%% Triangle $tid\n";
    
            my $t = getTriangle( $tid );
    
            my $e1 = getEdge( $t->{ 'eid1' } );
            my $e2 = getEdge( $t->{ 'eid2' } );
    
            my $p1 = getPoint( $e1->{ 'pid1' } );
            my $p2 = getPoint( $e1->{ 'pid2' } );
            my $p3 = getPoint( $e2->{ 'pid1' } );
            $p3 = getPoint( $e2->{ 'pid2' } )
                    if ( ( $p3 eq $p1 ) || ( $p3 eq $p2 ) );
    
            printf $fh "0 1 0 0 setcmykcolor
            /x1 %f rad mul def
            /y1 %f rad mul def
            /x2 %f rad mul def
            /y2 %f rad mul def
            /x3 %f rad mul def
            /y3 %f rad mul def
            x1 y1 mv
            x2 y2 ln
            x3 y3 ln
            closepath
            stroke
            ",
            $p1->{ 'x' }, $p1->{ 'y' },
            $p2->{ 'x' }, $p2->{ 'y' },
            $p3->{ 'x' }, $p3->{ 'y' },
            ;
    
            printf $fh "
                .3 setgray
                /x1 %f rad mul def
                /y1 %f rad mul def
                x1 y1 mv
                2 0 rln -2 2 rln -2 -2 rln 2 -2 rln fill
            ",
            $t->{ 'mx' }, $t->{ 'my' },
        }
    }

    # Draw the points
    for my $pid ( keys %{$IMAGE->{ 'points' }} ) {

        printf $fh "\n%% Point $pid\n";

        my $p = $IMAGE->{ 'points' }->{ $pid };

        printf $fh "
            0 1 0 setrgbcolor
            /x1 %f rad mul def
            /y1 %f rad mul def
            x1 y1 mv
            2 0 rln -2 2 rln -2 -2 rln 2 -2 rln fill
            %s {
                1 setgray
                x1 y1 moveto
                (%s) dup stringwidth pop -2 div 4 rmv show
            } if
        ",
        $p->{ 'x' }, $p->{ 'y' },
        $IMAGE->{ 'draw-labels' } ? 'true' : 'false', $pid,
        ;
    }

    # Draw the circumcentres
    for my $tid ( keys %{$IMAGE->{ 'triangles' }} ) {

        printf $fh "\n%% Centre %s\n", $tid;

        my $t = getTriangle( $tid );

        printf $fh "
            1 0 0 0 setcmykcolor
            /x1 %f rad mul def
            /y1 %f rad mul def
            x1 y1 mv
            2 0 rln -2 2 rln -2 -2 rln 2 -2 rln fill
            %s {
                1 setgray
                x1 y1 moveto
                (%s) dup stringwidth pop -2 div 4 rmv show
            } if
        ",
        $t->{ 'cx' }, $t->{ 'cy' },
        $IMAGE->{ 'draw-labels' } ? 'true' : 'false', $tid,
        ;
    }

    close $fh;

    my $cmd = sprintf "/usr/bin/convert '%s' '%s'",
                    $IMAGE->{ 'temp-ps' },
                    $IMAGE->{ 'dest-pdf' },
    ;
    system( $cmd );

    return;
}

############################################################
# Simple logger functions

sub logDebug {
    logInfo( @_ ) if $IMAGE->{ 'verbose' };
    return;
}

sub logInfo {
    printf shift . "\n", @_;
    return;
}

__END__
