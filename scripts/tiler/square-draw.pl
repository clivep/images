#!/usr/bin/perl

use warnings;
use strict;

############################################################
# 
# square-draw.pl <json> <png>
# 
#   Render a tiling from the given json data to a png file
# 
############################################################

use Math::Trig;
use Getopt::Std;
use JSON;

use constant    PAGESIZE_WIDTH  =>  530; 
use constant    PAGESIZE_DEPTH  =>  750;
use constant    DEFAULT_SCALE   =>  20;

############################################################
# Setup the environment

my %OPTIONS;
getopts( 'his:x:', \%OPTIONS );

help() if $OPTIONS{ 'h' };

help( 'Missing parameters' ) unless $ARGV[ 1 ];

my $TEMP_FILE = 'clp.ps';
my $DRAWING_SCALE = $OPTIONS{ 's' } || DEFAULT_SCALE;
my $SHOW_IDS = $OPTIONS{ 'i' };
my $EXCLUSION_RADIUS = $OPTIONS{ 'x' };

############################################################
# Load and render the image

local $/;   # Enable 'slurp' mode

open my $data, "<", $ARGV[ 0 ] or
                help( "Could not open $ARGV[ 0 ]: $!\n" );

my $json = <$data>;
close $data;

my $image_data = decode_json( $json );

my $source_file = shift;
my $image_file = shift;

open my $img, '>', $TEMP_FILE or die "Could not open $TEMP_FILE - $!\n";
printf $img "%%!PS

    <<
        /PageSize [ %f %f ]
        /ImagingBBox null
    >> setpagedevice

    /mv { moveto } bind def
    /ln { lineto } bind def

    /drawArc {
        /a1 exch def
        /a2 exch def
        /rad exch def
        /yc exch def
        /xc exch def

        xc a1 cos rad mul add
        yc a1 sin rad mul add
        moveto
        xc yc rad a1 a2 arcn
    } bind def

    /Arial findfont 10 scalefont setfont

    0 0 mv 1000 0 ln 1000 1000 ln 0 1000 ln fill
    %f %f translate
",
PAGESIZE_WIDTH, PAGESIZE_DEPTH,
PAGESIZE_WIDTH / 2, PAGESIZE_DEPTH / 2;

if ( $EXCLUSION_RADIUS ) {

    printf $img "1 setgray\n";
    printf $img "0 0 %f 0 360 drawArc stroke\n", $EXCLUSION_RADIUS * $DRAWING_SCALE;
}

for my $polygon ( @{ $image_data->{ 'POLYGONS' } } ) {

    printf $img "1 1 0 setrgbcolor\n";

    drawSquarePolygon( $img, $polygon, $image_data->{ 'VERTICES' } )
}

close $img;

my $convert = "/usr/bin/convert" unless ( -f $convert );

unless ( -f $convert ) {

    printf "Count not locate convert at $convert\n";
}
else {

    system( "$convert $TEMP_FILE $image_file" );
}
 
exit;

############################################################
# Draw a square polygon

sub drawSquarePolygon {

    my $img = shift;
    my $polygon = shift;
    my $vertex_list = shift;

    my $vertices = [];
    my $xc = 0;
    my $yc = 0;
    my $id;
    
    for ( my $v = 0; $v < 4; $v++ ) {

        push @{ $vertices }, $vertex_list->{ $polygon->{ 'VERTICES' }->[ $v ] };

        if ( ( $v == 0 ) || ( $v == 2 ) ) {
            $xc += $DRAWING_SCALE * $vertices->[ $v ]->{ 'X' } / 2;
            $yc += $DRAWING_SCALE * $vertices->[ $v ]->{ 'Y' } / 2;
        }

        printf $img "%f %f %s ", 
                    $DRAWING_SCALE * $vertices->[ $v ]->{ 'X' },
                    $DRAWING_SCALE * $vertices->[ $v ]->{ 'Y' },
                    ( $v == 0 ) ? 'mv' : 'ln'
        ;
    }

    printf $img "closepath stroke\n";

    my $ang1 = atan2(
                $vertices->[ 3 ]->{ 'Y' } - $vertices->[ 2 ]->{ 'Y' },
                $vertices->[ 3 ]->{ 'X' } - $vertices->[ 2 ]->{ 'X' }
    ); 
    printf $img "%%1 0 0 setrgbcolor %f %f %f %f %f drawArc stroke\n",
                                        $DRAWING_SCALE * $vertices->[ 2 ]->{ 'X' },
                                        $DRAWING_SCALE * $vertices->[ 2 ]->{ 'Y' },
                                        $DRAWING_SCALE,
                                        rad2deg( $ang1 ),
                                        rad2deg( 2.0 * pi / 5.0 + $ang1 );

    my $ang2 = atan2(
                $vertices->[ 1 ]->{ 'Y' } - $vertices->[ 0 ]->{ 'Y' },
                $vertices->[ 1 ]->{ 'X' } - $vertices->[ 0 ]->{ 'X' }
    ); 
    printf $img "%%0 1 0 setrgbcolor %f %f %f %f %f drawArc stroke\n",
                                        $DRAWING_SCALE * $vertices->[ 0 ]->{ 'X' },
                                        $DRAWING_SCALE * $vertices->[ 0 ]->{ 'Y' },
                                        $DRAWING_SCALE / 2,
                                        rad2deg( $ang2 ),
                                        rad2deg( 6.0 * pi / 5.0 + $ang2 );

    if ( $SHOW_IDS ) {
        $id = substr( $polygon->{ 'ID' }, -3 );
        printf $img "1 setgray\n";
        printf $img "%f %f mv (%s) dup stringwidth pop -2 div 0 rmoveto show\n", $xc, $yc, $id;
    }

    return;
}

############################################################
# Help doco

sub help {

    print "\n@_\n" if @_;

    printf "\n$0 " .
        "[-h] [-i] [-s <scale>] [-c <rad>] <data file> <image file>

    where
        -h          this help info
        -i          show ids
        -s <scale>  drawing scale
        -x <rad>    exclusion radius

    \n";

    exit;
}

__END__
