#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>

#include "fractal.h"

/*******************************************************************************
 *
 * fractal.c
 *
 *  Functions to generate fractal images
 *
 *  File versions:
 *
 *      1.0     Initial version
 *
 ******************************************************************************/

#define DEFAULT_DATA_FILE           "mandelbuddha.dat"
#define IMAGE_FILE_VERSION          "1.5"

#define DEFAULT_IMAGE_TRAJECTORIES  10000000

#define DEFAULT_IMAGE_XC            0.000
#define DEFAULT_IMAGE_YC            0.000
#define DEFAULT_IMAGE_SZ            2.000

#define ESCAPE_RADIUS               2.000

#define PIC_WID                     1000
#define PIC_DEP                     1000

/*************************************************
 * Find the image pixel number for the given point
 ************************************************/

int getImagePixel(
    _image      *image,
    long double x,
    long double y 
) {

    int         img_x;
    int         img_y;
    static long double scale_x = -1.0;
    static long double scale_y = -1.0;

    if ( scale_x < 0 ) {
        if ( image->pic_wid > image->pic_dep ) {
            scale_x = image->pic_wid / (long double)image->pic_dep;
            scale_y = 1.0;
        }
        else {
            scale_y = image->pic_dep / (long double)image->pic_wid;
            scale_x = 1.0;
        }
    }

    if ( x > image->xc + ( scale_x * image->sz ) )
        return -1;

    if ( x < image->xc - ( scale_x * image->sz ) )
        return -1;

    if ( y > image->yc + ( scale_y * image->sz ) )
        return -1;

    if ( y < image->yc - ( scale_y * image->sz ) )
        return -1;

    img_x = (int)( ( ( ( x - image->xc ) / ( scale_x * image->sz ) ) + 1 ) * image->pic_wid / 2.0 );
    img_y = (int)( ( ( ( y - image->yc ) / ( scale_y * image->sz ) ) + 1 ) * image->pic_dep / 2.0 );

    return ( img_y * image->pic_wid ) + img_x;
}

/*************************************************
 * Check if the given point has been visited
 ************************************************/

BOOL periodicityCheck(
    _complex    pnt,
    int         iters   // if < 0 then reset the check point
) {

    BOOL    retval;
    double  reset_chk;

    static  _complex    check_pnt;

    if ( iters < 0 ) {
        check_pnt.x = pnt.x;
        check_pnt.y = pnt.y;
        return TRUE;
    }

    iters++;    // the value is 0-based

    retval =
        ( check_pnt.x != pnt.x ) ||
        ( check_pnt.y != pnt.y )
    ;

    reset_chk = log( iters ) / log( 2.0 );

    if ( reset_chk == (int)(reset_chk) ) {
        check_pnt.x = pnt.x;
        check_pnt.y = pnt.y;
    }

    return retval;
}

/*************************************************
 * Add a visit to a point and check the image max
 ************************************************/

void incrementVisitCount(
    _image  *image,
    int     point,
    int     layer
) {

    if ( point < 0 ) 
        return;

    if ( point < image->pic_dep * image->pic_wid ) {
    
        image->data[ point ].buckets[ layer ]++;
        image->total_visits.buckets[ layer ]++;

        if ( image->data[ point ].buckets[ layer ] > image->max_visits.buckets[ layer ] )
            image->max_visits.buckets[ layer ] = image->data[ point ].buckets[ layer ];
    }
}

/*************************************************
 * Add a trajectory using the default scheme
 ************************************************/

void processDefaultTrajectory(
    _image  *image,
    int     *trajectory,
    int     trajectory_len,
    int     point_num
) {

    int p;
    int t;

    for( p = 0; p < trajectory_len; p++ ) {

        t = trajectory[ p ];

        incrementVisitCount( image, t, 0 );
        incrementVisitCount( image, t, 1 );
        incrementVisitCount( image, t, 2 );
    }
}

/*************************************************
 * Iterate a point c in the given layer
 ************************************************/

int iterateLayerPoint( 
    _image      *image,
    int         layer,
    _complex    c,
    int         point_num
) {
    _complex    pnt;
    int         p;
    int         image_pnt;
    int         iters;
    int         trajectory_len;
    long double escape;
    long double z;

    static _complex  *trajectory = NULL;

    if ( trajectory == NULL ) {

        trajectory = NALLOC( image->max_image_iters, _complex );
        if ( trajectory == NULL )
            logFatal( "calloc() for trajectory data failed" );
    }
    else {

        memset( trajectory, '\0', image->max_image_iters * sizeof( int ) );
    }

    trajectory_len = 0;

    pnt.x = 0.0;
    pnt.y = 0.0;

    periodicityCheck( pnt, -1 );    // prime the check point

    escape = image->escape * image->escape;

    // Find the trajectory of this point
    for( iters = 0; iters < image->max_iters.buckets[ layer ]; iters++ ) {

        z = pnt.x;
        pnt.x = ( pnt.x * pnt.x ) - ( pnt.y * pnt.y ) + c.x;
        pnt.y = 2.0 * z * pnt.y + c.y;
    
        if ( ( ( pnt.x * pnt.x ) + ( pnt.y * pnt.y ) ) > escape )
            break;

        // Check if this pnt has been visited before
        if ( ! periodicityCheck( pnt, iters ) )
            return 0;

        trajectory[ iters ].x = pnt.x;
        trajectory[ iters ].y = pnt.y;
        trajectory_len++;
    }

    // If the point escaped then add the trajectory to the image

    if ( ( ( pnt.x * pnt.x ) + ( pnt.y * pnt.y ) ) > escape ) {

        for( p = 0; p < trajectory_len; p++ ) {

            image_pnt = getImagePixel( image, trajectory[ p ].x , trajectory[ p ].y );
            if ( image_pnt > -1 )
                incrementVisitCount( image, image_pnt, layer );

            // since the image is symmetric about the x axis - add the mirrored point
            image_pnt = getImagePixel( image, trajectory[ p ].x , -1 * trajectory[ p ].y );
            if ( image_pnt > -1 )
                incrementVisitCount( image, image_pnt, layer );
        }
    }

    return trajectory_len;
}

/*************************************************
 * Store the image data
 ************************************************/

void writeImage(
    _image  *image,
    char    *dest
) {
    FILE    *fp;

    if ( ( fp = fopen( dest, "w" ) ) == NULL )
        logFatal( "Failed to open %s", dest );

    fprintf( fp, "%s\n", image->version );
    fwrite( &image->type, sizeof( int ), 1, fp );

    fprintf( fp, "%s\n", image->description );

    fwrite( &image->xc, sizeof( long double ), 1, fp );
    fwrite( &image->yc, sizeof( long double ), 1, fp );
    fwrite( &image->sz, sizeof( long double ), 1, fp );

    fwrite( &image->escape, sizeof( long double ), 1, fp );
    fwrite( &image->trajectories, sizeof( int ), 1, fp );

    fwrite( &image->max_image_iters, sizeof( int ), 1, fp );

    fwrite( &image->max_iters, sizeof( int ), 3, fp );
    fwrite( &image->max_visits, sizeof( int ), 3, fp );
    fwrite( &image->total_visits, sizeof( int ), 3, fp );

    fwrite( &image->pic_wid, sizeof( int ), 1, fp );
    fwrite( &image->pic_dep, sizeof( int ), 1, fp );

    fwrite( image->data, sizeof( _visits ), image->pic_wid*image->pic_dep, fp );

    fclose( fp );
}

/*************************************************
 * Process and runline parameters
 ************************************************/

void processParameters(
    int     argc,
    char    **argv,
    _image  *image
) {
    int     pnt = 1;

    while ( pnt < argc ) {
    
        if ( *argv[ pnt ] != '-' )
            logFatal( "Unknown switch %s", argv[ pnt ] );

        switch( argv[ pnt ][ 1 ] ) {

            case 'r' :
                getIntParameter( argc, argv, ++pnt, &image->max_iters.buckets[ 0 ], "iteration limit for red bucket" );
                break;

            case 'g' :
                getIntParameter( argc, argv, ++pnt, &image->max_iters.buckets[ 1 ], "iteration limit for green bucket" );
                break;

            case 'b' :
                getIntParameter( argc, argv, ++pnt, &image->max_iters.buckets[ 2 ], "iteration limit for blue bucket" );
                break;

            case 'h' :
                usage( argv[ 0 ] );
                break;

            case 'd' :
                getIntParameter( argc, argv, ++pnt, &image->pic_dep, "image depth" );
                break;

            case 'e' :
                getDoubleParameter( argc, argv, ++pnt, &image->escape, "escape radius" );
                break;

            case 'f' :
                getCharParameter( argc, argv, ++pnt, &image->data_file, "data file" );
                break;

            case 'n' :
                getCharParameter( argc, argv, ++pnt, &image->description, "description" );
                break;

            case 't' :
                getIntParameter( argc, argv, ++pnt, &image->trajectories, "image trajectories" );
                break;

            case 'w' :
                getIntParameter( argc, argv, ++pnt, &image->pic_wid, "image width" );
                break;

            case 's' :
                getDoubleParameter( argc, argv, ++pnt, &image->sz, "image radius" );
                if ( image->sz == 0.0 )
                    logFatal( "Image size cannot be 0" );
                break;

            case 'x' :
                getDoubleParameter( argc, argv, ++pnt, &image->xc, "image x centre" );
                break;

            case 'y' :
                getDoubleParameter( argc, argv, ++pnt, &image->yc, "image y centre" );
                break;

            default:
                logFatal( "Unknown switch %s", argv[ pnt ] );
        }

        pnt++;
    }

    image->max_image_iters = MAX(
                            image->max_iters.buckets[ 0 ],
                            MAX( image->max_iters.buckets[ 1 ], image->max_iters.buckets[ 2 ] )
    );
}

/*************************************************
 * Usage
 ************************************************/

void usage(
    char    *self
) {

    printf( "Usage: %s\n", self );
    printf( "          [-r <limit>] [-g <limit>] [-b <limit>]\n" );
    printf( "          [-x <x centre>] [-y <y centre>] [-s <img size>]\n" );
    printf( "          [-d <pic depth>] [-w <pic width>] [-t <trajectories>]\n" );
    printf( "          [-e <escape radius>]\n" );
    printf( "          [-f <data file>]\n" );
    printf( "          \n" );
    printf( "\n" );
    printf( "   where:\n" );
    printf( "       -b <limit>          iteration limit for bucket 3 (default 50)\n" );
    printf( "       -c <description>    description of the image\n" );
    printf( "       -d <pic depth>      height in pixels of the picture (default %d)\n", PIC_WID );
    printf( "       -e <escape radius>  upper bound for iteration point distance (default %0.4f)\n", ESCAPE_RADIUS );
    printf( "       -f <data file>      path to the data file (default %s)\n", DEFAULT_DATA_FILE );
    printf( "       -g <limit>          iteration limit for bucket 2 (default 500)\n" );
    printf( "       -r <limit>          iteration limit for bucket 1 (default 5000)\n" );
    printf( "       -s <img size>       image size (default %0.4f)\n", DEFAULT_IMAGE_SZ );
    printf( "       -t <trajectories>   number of trajectories to try (default 10000000)\n" );
    printf( "       -w <pic width>      width in pixels of the picture (default %d)\n", PIC_DEP );
    printf( "       -x <x centre>       image x centre in the plane (default %0.4f)\n", DEFAULT_IMAGE_XC );
    printf( "       -y <y centre>       image y centre in the plane (default %0.4f)\n", DEFAULT_IMAGE_YC );
    printf( "\n" );

    exit( 0 );
}

/*************************************************
 * Setup the image details
 ************************************************/

_image *setupImage() {

    _image      *image;

    if ( ( image = ALLOC( _image ) ) == NULL )
        logFatal( "failed the calloc() image details" );

    image->data_file = DEFAULT_DATA_FILE;

    image->version = IMAGE_FILE_VERSION;

    image->xc = DEFAULT_IMAGE_XC;
    image->yc = DEFAULT_IMAGE_YC;
    image->sz = DEFAULT_IMAGE_SZ;

    image->max_iters.buckets[ 0 ] = 5000;
    image->max_iters.buckets[ 1 ] = 500;
    image->max_iters.buckets[ 2 ] = 50;

    image->pic_dep = PIC_DEP;

    image->trajectories = DEFAULT_IMAGE_TRAJECTORIES;
    image->pic_wid = PIC_WID;

    image->escape = ESCAPE_RADIUS;

    return image;
}

/*************************************************
 * Find a random point around the image area
 ************************************************/

void getRandomPoint(
    _image *image, 
    _complex *c 
) {

    while( 1 ) {
    
        c->x = boundedRand( 4.0 * image->sz ) - ( 2.0 * image->sz ) + image->xc;
        c->y = boundedRand( 4.0 * image->sz ) - ( 2.0 * image->sz ) + image->yc;

        if ( ! ( inP1Cardioid( *c ) || inP2Bulb( *c ) ) )
            return;
    }
}

/*************************************************
 * Find a random point close to the given one
 ************************************************/

void getNearPoint(
    _image *image, 
    _complex *c 
) {

    long double incr = image->sz / MAX( image->pic_dep, image->pic_wid );

    while ( 1 ) {

        c->x += boundedRand( 2.0 * incr ) - incr;
        c->y += boundedRand( 2.0 * incr ) - incr;

        if ( ! ( inP1Cardioid( *c ) || inP2Bulb( *c ) ) )
            return;
    }
}

/*************************************************
 * Format a duration string
 ************************************************/

char *getDurationStr(
    int start
) {
    int duration = time( NULL ) - start;
    int mins;
    static char buf[ 16 ];

    if ( duration < 60 ) {
        sprintf( buf, "%02ds", duration );
    }
    else {
        mins = (int)( duration / 60 );
        sprintf( buf, "%dm %02ds", mins, duration - ( 60 * mins ) );
    }

    return buf;
}

/*************************************************
 * Main
 ************************************************/

int main(
    int         argc,
    char        **argv
) {
    _complex    c;
    int         layer_start;
    int         l;
    int         t;
    int         near_points;
    int         r;
    _image      *image;

    initialiseLogging( "fractal.log", LOGGING_LEVEL_QUIET );

    image = setupImage();
    processParameters( argc, argv, image );

    image->data = NALLOC( image->pic_dep*image->pic_wid, _visits );
    if ( image->data == NULL )
        logFatal( "calloc() for image data failed" );

    for ( l = 0; l < 3; l++ ) {

        near_points = 0;
        layer_start = time( NULL );

        // A 'good' trajectory rating will be at least 75% of the max iters
        r = (int)(image->max_iters.buckets[ l ] * 0.75);

        for ( t = 0; t < image->trajectories; t++ ) {

            if ( t % 20 == 0 ) {
                printf( "\rLayer %d - %0.3Lf%% (%s)  ",
                                l, 
                                (100.0 * t / (long double)image->trajectories), 
                                getDurationStr( layer_start) 
                );
                fflush( stdout );
            }

            getRandomPoint( image, &c );
    
            while ( ( iterateLayerPoint( image, l, c, t++ ) > r ) && ( t < image->trajectories ) ) {
                getNearPoint( image, &c );
                near_points++;
                if ( near_points > 500 )
                    break;
            }
        }

        printf( "\rLayer %d Done (%s)                \n", l, getDurationStr( layer_start) );
    }

    writeImage( image, image->data_file );

    return 0;
}
