#!/usr/bin/perl

use warnings;
use strict;

############################################################
#
# Use the Bowyer-Watson algoritm to triangulate a set of
# points
#
############################################################

use Data::Dumper;
use Getopt::Std;
use JSON;
use Readonly;
use Try::Tiny;

my Readonly $DEFAULT_DEST_PDF       = '/tmp/clp.pdf';
my Readonly $DEFAULT_RADIUS_PCNT    = 48;
my Readonly $DEFAULT_POINT_COUNT    = 50;

############################################################
# Check the runline options and setup the environment

my %options;
getopts( 'dhlo:p:n:r:svw:', \%options );

my $IMAGE = {
    'description'       => 'Delaunay Triangulation',
    'id'                => 0,
    'triangles'         => {},
    'edges'             => {},
    'points'            => {},
    'temp-ps'           => '/tmp/clp.ps',
    'image-size'        => 1000,
    'display-data'      => $options{ 'd' },
    'draw-labels'       => $options{ 'l' },
    'point-count'       => $options{ 'n' } || $DEFAULT_POINT_COUNT,
    'dest-pdf'          => $options{ 'o' } || $DEFAULT_DEST_PDF,
    'radius-pcnt'       => $options{ 'p' } || $DEFAULT_RADIUS_PCNT,
    'read-data-file'    => $options{ 'r' },
    'draw-disc'         => $options{ 's' },
    'verbose'           => $options{ 'v' },
    'write-data-file'   => $options{ 'w' },
};

usage() if $options{ 'h' };
usage( "Max value for rad is 50" ) if $IMAGE->{ 'radius-pcnt' } > 50;

usage( "options 'n' and 'p' are invalid when read data files" )
    if ( $options{ 'r' } && ( $options{ 'n' } || $options{ 'p' } ) );

############################################################
# Produce the image

init();
processImage();
draw();

print Dumper $IMAGE if $IMAGE->{ 'display-data' };
writeImageFile() if $IMAGE->{ 'write-data-file' };

exit 0;

############################################################
# Help info

sub usage {

    my $frmt = shift;

    printf( "\n$frmt\n", @_ ) if $frmt;

    print "\n$0 [-d] [-h] [-l] [-n <pts>] [-o <file>] [-p <rad>] [-r <file>] " .
        "[-s] [-v] [-w <file>]

        where
            -d          display image data
            -h          this help message
            -l          label the points on the image
            -n <pts>    number of points (default $DEFAULT_POINT_COUNT)
            -o <file>   dest pdf file (default $DEFAULT_DEST_PDF)
            -p <pcnt>   image radius percentage (max 50, default $DEFAULT_RADIUS_PCNT)
            -r <file>   source image data file
            -s          draw the disc surrounding the points
            -v          verbose logging
            -w <file>   dest image data file
    \n";

    exit 1;
}

############################################################
# Functions to create image elements

sub addPoint {

    my $id = $IMAGE->{ 'id' }++;

    $IMAGE->{ 'points' }->{ $id } = {
        'id'    =>  $id,
        'x'     =>  shift,
        'y'     =>  shift,
        'info'  =>  shift || '',
        'edges' =>  {},
    };

    return $id;
}

sub getEdgeId {

    my $pid1 = shift;
    my $pid2 = shift;

    if ( $pid2 lt $pid1 ) {
        my $tmp = $pid2;
        $pid2 = $pid1;
        $pid1 = $tmp;
    }

    return sprintf "%s-%s", $pid1, $pid2;
}

sub addEdge {

    my $pid1 = shift;
    my $pid2 = shift;

    my $id = getEdgeId( $pid1, $pid2 );

    $IMAGE->{ 'edges' }->{ $id } ||= {
        'id'        =>  $id,
        'pid1'      =>  $pid1,
        'pid2'      =>  $pid2,
        'triangles' => {},
    };

    $IMAGE->{ 'points' }->{ $pid1 }->{ 'edges' }->{ $id } = 1;
    $IMAGE->{ 'points' }->{ $pid2 }->{ 'edges' }->{ $id } = 1;

    return $id;
}

sub addTriangle {

    my $eid1 = shift;
    my $eid2 = shift;
    my $eid3 = shift;

    my $p1 = getPoint( getEdge( $eid1 )->{ 'pid1' } );
    my $p2 = getPoint( getEdge( $eid1 )->{ 'pid2' } );
    my $p3 = getPoint( getEdge( $eid3 )->{ 'pid1' } );
    $p3 = getPoint( getEdge( $eid3 )->{ 'pid2' } )
                if ( ( $p3 eq $p1 ) || ( $p3 eq $p2 ) );

    # Create a triangle element
    my $id = sprintf "%d-%d-%d",
                $p1->{ 'id' },
                $p2->{ 'id' },
                $p3->{ 'id' },
    ;

    $IMAGE->{ 'triangles' }->{ $id } = {
        'id'    =>  $id,
        'eid1'  =>  $eid1,
        'eid2'  =>  $eid2,
        'eid3'  =>  $eid3,
    };

    $IMAGE->{ 'edges' }->{ $eid1 }->{ 'triangles' }->{ $id } = 1;
    $IMAGE->{ 'edges' }->{ $eid2 }->{ 'triangles' }->{ $id } = 1;
    $IMAGE->{ 'edges' }->{ $eid3 }->{ 'triangles' }->{ $id } = 1;

    logDebug( "    adding triangle '$id'" );

    # Add the circumcentre and radius to the element
    my $x1 = $p1->{ 'x' };
    my $y1 = $p1->{ 'y' };
    my $x2 = $p2->{ 'x' };
    my $y2 = $p2->{ 'y' };
    my $x3 = $p3->{ 'x' };
    my $y3 = $p3->{ 'y' };

    my $z1 = ( $x1 * $x1 ) + ( $y1 * $y1 );
    my $z2 = ( $x2 * $x2 ) + ( $y2 * $y2 );
    my $z3 = ( $x3 * $x3 ) + ( $y3 * $y3 );

    my $a =
        $x1 * ( $y2 - $y3 ) -
        $y1 * ( $x2 - $x3 ) +
        ( $x2 * $y3 ) - ( $y2 * $x3 )
    ;

    my $bx = -1 * (
        $z1 * ( $y2 - $y3 ) -
        $y1 * ( $z2 - $z3 ) +
        ( ( $z2 * $y3 ) - ( $y2 * $z3 ) )
    );

    my $by =
        $z1 * ( $x2 - $x3 ) -
        $x1 * ( $z2 - $z3 ) +
        ( ( $z2 * $x3 ) - ( $x2 * $z3 ) )
    ;

    my $c = -1 * (
        $z1 * ( ( $x2 * $y3 ) - ( $y2 * $x3 ) ) -
        $x1 * ( ( $z2 * $y3 ) - ( $y2 * $z3 ) ) +
        $y1 * ( ( $z2 * $x3 ) - ( $x2 * $z3 ) )
    );

    my $t = getTriangle( $id );
    $t->{ 'cx' } = -1 * $bx / ( 2.0 * $a );
    $t->{ 'cy' } = -1 * $by / ( 2.0 * $a );
    my $d = ( $bx * $bx ) + ( $by * $by ) - ( 4 * $a * $c );
    $t->{ 'cr' } = sqrt( $d ) / ( 2 * abs( $a ) );
    $t->{ 'cd' } = sqrt(
        ( $t->{ 'cx' } * $t->{ 'cx' } ) + 
        ( $t->{ 'cy' } * $t->{ 'cy' } )
    );

    # Also add the triangle centre
    $t->{ 'mx' } = ( $x1 + $x2 + $x3 ) / 3.0;
    $t->{ 'my' } = ( $y1 + $y2 + $y3 ) / 3.0;

    return $id;
}

############################################################
# Functions to retrieve image elements

sub getPoint { return $IMAGE->{ 'points' }->{ $_[0] } }
sub getEdge { return $IMAGE->{ 'edges' }->{ $_[0] } }
sub getTriangle { return $IMAGE->{ 'triangles' }->{ $_[0] } }

############################################################
# Functions to delete image elements

sub deleteTriangle {

    my $tid = shift;

    my $t = getTriangle( $tid );

    # remove this triangle from the edge's list of triangles
    delete getEdge( $t->{ 'eid1' } )->{ 'triangles' }->{ $tid };
    delete getEdge( $t->{ 'eid2' } )->{ 'triangles' }->{ $tid };
    delete getEdge( $t->{ 'eid3' } )->{ 'triangles' }->{ $tid };

    # remove the triangle from the image
    delete $IMAGE->{ 'triangles' }->{ $tid };

    return;
}

sub deleteEdge {

    my $eid = shift;

    my $e = getEdge( $eid );

    # remove this edge from the point's list of edges
    delete getPoint( $e->{ 'pid1' } )->{ 'edges' }->{ $eid };
    delete getPoint( $e->{ 'pid2' } )->{ 'edges' }->{ $eid };

    # remove the edge from the image
    delete $IMAGE->{ 'edges' }->{ $eid };

    return;
}

############################################################
# Read image data from a file and update the IMAGE

sub readImageFile {

    open my $fh, '<', $IMAGE->{ 'read-data-file' } or 
        usage( "Could not open %s: $!", $IMAGE->{ 'read-data-file' } );

    my $content = do { local $/; <$fh> };

    close $fh;

    my $data;
    try {
        $data = decode_json( $content );
    }
    catch {
        usage( 'Invalid json file' );
    };

    usage( 'Datafile has no point data' ) unless $data->{ 'points' };

    # Only use point data from the file - decorations come from the params
    for my $pid ( keys %{$data->{ 'points' }} ) {
        my $p = $data->{ 'points' }->{ $pid };
        addPoint( $p->{ 'x' }, $p->{ 'y' } );
    }

    $IMAGE->{ 'radius-pcnt' } = $data->{ 'radius-pcnt' };
    $IMAGE->{ 'point-count' } = $data->{ 'point-count' };

    return;
}

############################################################
# Write the image data to a file

sub writeImageFile {

    open my $fh, '>', $IMAGE->{ 'write-data-file' } or 
        usage( "Could not open %s: $!\n", $IMAGE->{ 'write-data-file' } );

    print $fh encode_json $IMAGE;

    close $fh;

    return;
}

############################################################
# Setup the initial set of points

sub init {

    # Add the super triangle first
    my $pid1 = addPoint( -1, -1, 'super-point' );
    my $pid2 = addPoint( 1 + sqrt( 2.0 ), -1, 'super-point' );
    my $pid3 = addPoint( -1, 1 + sqrt( 2.0 ), 'super-point' );

    $IMAGE->{ 'super-id' } = addTriangle(
        addEdge( $pid1, $pid2 ),
        addEdge( $pid2, $pid3 ),
        addEdge( $pid3, $pid1 ),
    );

    # Are the points to be sourced from an image file
    if ( $IMAGE->{ 'read-data-file' } ) {
        readImageFile();
        return;
    }

    # Add some random points
    my $i = 0;
    while ( $i < $IMAGE->{ 'point-count' } ) {
        my $x = 2 * rand() - 1;
        my $y = 2 * rand() - 1;
        next if ( ( $x * $x ) + ( $y * $y ) > 1 );

        my $valid = 1;
        my $min_dist = 2 * $IMAGE->{ 'radius-pcnt' } /
                            ( 100 * $IMAGE->{ 'point-count' } );
        for my $pid ( keys %{$IMAGE->{ 'points' }} ) {
            my $p = $IMAGE->{ 'points' }->{ $pid };
            my $d = 
                ( $p->{ 'x' } - $x ) * ( $p->{ 'x' } - $x ) +
                ( $p->{ 'y' } - $y ) * ( $p->{ 'y' } - $y )
            ;
            if ( $d < .960 / $IMAGE->{ 'point-count' } ) {
                $valid = 0;
                last;
            }
        }
        if ( $valid ) {
            addPoint( $x, $y );
            $i++;
        }
    }

    return;
}

############################################################
# Is this triangle id attached to the super triangle

sub isConstructionTriangle {

    my $t = getTriangle( shift );
    return unless $t;

    my $e1 = getEdge( $t->{ 'eid1' } );
    my $e2 = getEdge( $t->{ 'eid2' } );

    my $p1 = getPoint( $e1->{ 'pid1' } );
    my $p2 = getPoint( $e1->{ 'pid2' } );
    my $p3 = getPoint( $e2->{ 'pid1' } );
    $p3 = getPoint( $e2->{ 'pid2' } )
            if ( ( $p3 eq $p1 ) || ( $p3 eq $p2 ) );

    return
        $p1->{ 'info' } eq 'super-point' ||
        $p2->{ 'info' } eq 'super-point' ||
        $p3->{ 'info' } eq 'super-point'
    ;

    return;
}

############################################################
# Is this edge id attached to the super triangle

sub isConstructionEdge {

    my $e = getEdge( shift );
    return unless $e;

    my $p1 = getPoint( $e->{ 'pid1' } );
    my $p2 = getPoint( $e->{ 'pid2' } );

    return
        $p1->{ 'info' } eq 'super-point' ||
        $p2->{ 'info' } eq 'super-point'
    ;
}

############################################################
# Remove the construction triangle and its edges/points

sub cleanupConstruction {

    logDebug( "Cleaning up construction triangle" );

    # Remove any triangles based on the original super-triangle
    for my $tid ( keys %{$IMAGE->{ 'triangles' }} ) {

        if ( isConstructionTriangle( $tid ) ) {

            # mark boundary points and edges - to help locate boundary elements
            my $t = getTriangle( $tid );

            my $e1 = getEdge( $t->{ 'eid1' } );
            $e1->{ 'boundary' } = 1;
            getPoint( $e1->{ 'pid1' } )->{ 'boundary' } = 1;
            getPoint( $e1->{ 'pid2' } )->{ 'boundary' } = 1;

            my $e2 = getEdge( $t->{ 'eid2' } );
            $e2->{ 'boundary' } = 1;
            getPoint( $e2->{ 'pid1' } )->{ 'boundary' } = 1;
            getPoint( $e2->{ 'pid2' } )->{ 'boundary' } = 1;

            getEdge( $t->{ 'eid3' } )->{ 'boundary' } = 1;

            # remove from the working list of triangles
            logDebug( "    removing construction triangle '$tid'" );
            deleteTriangle( $tid );
        }
    }

    # Remove any edges based on the original super-triangle
    for my $eid ( keys %{$IMAGE->{ 'edges' }} ) {

        if ( isConstructionEdge( $eid ) ) {

            logDebug( "    removing construction edge '$eid'" );
            deleteEdge( $eid );
        }
    }

    # Finally remove any points on the original super-triangle
    for my $pid ( keys %{$IMAGE->{ 'points' }} ) {

        my $p = getPoint( $pid );
        if ( $p->{ 'info' } eq 'super-point' ) {
            logDebug( "    removing construction point '$pid'" );
            delete $IMAGE->{ 'points' }->{ $pid };
        }
    }

    return;
}

############################################################
# Make sure that the boundary forms the convex hull 

sub fixConvexHull {

    logDebug( "Fixing convex hull" );

    # Find the boundary circuit
    my %edge_points;
    for my $eid ( keys %{$IMAGE->{ 'edges' }} ) {
        my $e = getEdge( $eid );
        next unless $e->{ 'boundary' };
        logDebug( "    boundary edge '$eid'" );
        $edge_points{ $eid } = [ $e->{ 'pid1' }, $e->{ 'pid2' } ];
    }

    # Select a random point on a random edge - and follow it
    my $first_edge = (keys %edge_points)[0];
    my $first_id = $edge_points{ $first_edge }->[0];

    logDebug( "    starting from point '$first_id'" );
    my @boundary_points = ( $first_id );

    my $next_id = $edge_points{ $first_edge }->[1];
    delete $edge_points{ $first_edge };

    # Also find a point on the hull (ie max y val and max x if more than 1) 
    my $p = getPoint( $first_id );
    my $max_x = $p->{ 'x' };
    my $max_y = $p->{ 'y' };
    my $max_idx = 0;

    while ( $next_id ne $first_id ) {

        push @boundary_points, $next_id;
        logDebug( "    looking for point '$next_id'" );

        my $found;
        for my $eid ( keys %edge_points ) {
            my $n = -1;
            if (
                ( $next_id eq $edge_points{ $eid }[++$n] ) ||
                ( $next_id eq $edge_points{ $eid }[++$n] )
            ) {
                # If the point is a y-max - it could be on the hull
                my $p = getPoint( $next_id );
                if (
                    ( $p->{ 'y' } > $max_y ) ||
                    ( ( $p->{ 'y' } == $max_y ) && ( $p->{ 'x' } > $max_x ) )
                ) {
                    $max_x = $p->{ 'x' };
                    $max_y = $p->{ 'y' };
                    $max_idx = scalar @boundary_points - 1;
                }

                $next_id = $edge_points{ $eid }[1-$n];
                logDebug( "    found on edge '$eid'" );
                delete $edge_points{ $eid };

                $found = 1;
                last;
            }
        }

        usage( "Point '$next_id' not found on boundary" ) unless $found;
    }

    # Check that the boundary is described clockwise
    logDebug( "    hull point '%s'", $boundary_points[$max_idx] );
    my $n = scalar @boundary_points;
    my $p1 = getPoint( $boundary_points[$max_idx-1] );
    my $p2 = getPoint( $boundary_points[$max_idx] );
    my $p3 = getPoint( $boundary_points[($max_idx+1) % $n] );

    my $v = 
        $p1->{ 'x' } * ( $p2->{ 'y' } - $p3->{ 'y' } ) -
        $p1->{ 'y' } * ( $p2->{ 'x' } - $p3->{ 'x' } ) +
        ( $p2->{ 'x' } * $p3->{ 'y' } ) - ( $p3->{ 'x' } * $p2->{ 'y' } )
    ;
    
    logDebug( "   %s-%s-%s => %f",
            $boundary_points[$max_idx-1],
            $boundary_points[$max_idx],
            $boundary_points[($max_idx+1) % $n],
            $v,
    );
    @boundary_points = reverse( @boundary_points ) if $v > 0;

    # Check for local concavities
    my $made_changes = 0;
    for( my $i = 0; $i < $n; $i++ ) {

        my $p1 = getPoint( $boundary_points[$i-1] );
        my $p2 = getPoint( $boundary_points[$i] );
        my $p3 = getPoint( $boundary_points[($i+1) % $n] );

        my $v = 
            $p1->{ 'x' } * ( $p2->{ 'y' } - $p3->{ 'y' } ) -
            $p1->{ 'y' } * ( $p2->{ 'x' } - $p3->{ 'x' } ) +
            ( $p2->{ 'x' } * $p3->{ 'y' } ) - ( $p3->{ 'x' } * $p2->{ 'y' } )
        ;

        if ( $v > 0 ) {

            logDebug( "   concavity %s-%s-%s",
                $boundary_points[$i-1],
                $boundary_points[$i],
                $boundary_points[($i+1) % $n],
            );
    
            my $eid1 = addEdge(
                $boundary_points[$i-1],
                $boundary_points[($i+1) % $n]
            );
            my $eid2 = getEdgeId(
                $boundary_points[$i-1], 
                $boundary_points[$i],
            );
            my $eid3 = getEdgeId( 
                $boundary_points[$i], 
                $boundary_points[($i+1) % $n],
            );
    
            my $tid = addTriangle( $eid1, $eid2, $eid3 );

            getEdge( $eid1 )->{ 'boundary' } = 1;
            getEdge( $eid2 )->{ 'boundary' } = 0;
            getEdge( $eid3 )->{ 'boundary' } = 0;
    
            # No need to test the next point as it is part of the new triangle
            $i++; 
    
            # Indicate that the boundary was altered
            $made_changes++; 
        }
    }

    return $made_changes;
}

############################################################
# Run the algorithm against the image points

sub processImage {

    my $tids = {
        $IMAGE->{ 'super-id' }  =>  1,
    };

    for my $pid ( keys %{$IMAGE->{ 'points' }} ) {

        # Skip the first 3 pts - they belong to the super-triangle
        next if $pid < 3;

        logDebug( "processing point '$pid'" );
        my $eids = {};

        # Remove triangles where the point is inside the circumscribed circle
        for my $tid ( keys %{$tids} ) {

            logDebug( "    checking triangle '$tid'" );

            next unless inCircumscribedCircle( $pid, $tid );

            logDebug( "    inscribed" );

            my $t = getTriangle( $tid );

            # Keep a count of common edegs - they will be removed
            $eids->{ $t->{ 'eid1' } } ||= 0;
            $eids->{ $t->{ 'eid2' } } ||= 0;
            $eids->{ $t->{ 'eid3' } } ||= 0;

            $eids->{ $t->{ 'eid1' } }++;
            $eids->{ $t->{ 'eid2' } }++;
            $eids->{ $t->{ 'eid3' } }++;

            logDebug( "    removing triangle '$tid'" );
            delete $tids->{ $tid };
            deleteTriangle( $tid );
        }

        # Remove any common edges
        for my $eid ( keys %{$eids} ) {
            if ( $eids->{ $eid } > 1 ) {
                logDebug( "    removing edge '$eid'" );
                delete $eids->{ $eid };
                deleteEdge( $eid );
            }
        }

        # Create triangles from the remaining edges
        for my $eid ( keys %{$eids} ) {
            my $e = getEdge( $eid );
            logDebug( "    using edge '$eid' and pt '$pid'" );
            my $eid1 = addEdge( $e->{ 'pid1' }, $pid );
            logDebug( "    added edge '$eid1' ( $e->{ 'pid1' }, $pid )" );
            my $eid2 = addEdge( $pid, $e->{ 'pid2' } );
            logDebug( "    added edge '$eid2' ( $pid, $e->{ 'pid2' } )" );
            my $tid = addTriangle( $eid1, $eid, $eid2 );
            logDebug( "    added triangle '$tid'" );
            $tids->{ $tid } = 1;
        }
    }

    cleanupConstruction();

    # Keep checking for concavities in the hull
    while ( fixConvexHull() ) {}

    logDebug( "done" );

    return
}

############################################################
# Draw the current state

sub inCircumscribedCircle {

    my $p = $IMAGE->{ 'points' }->{ $_[0] };
    my $t = $IMAGE->{ 'triangles' }->{ $_[1] };

    my $dx = $p->{ 'x' } - $t->{ 'cx' };
    my $dy = $p->{ 'y' } - $t->{ 'cy' };

    my $r = $t->{ 'cr' } * $t->{ 'cr' };

    return ( ( $dx * $dx ) + ( $dy * $dy ) ) < ( $t->{ 'cr' } * $t->{ 'cr' } );
}

############################################################
# Draw the current state

sub draw {

    open my $fh, '>', $IMAGE->{ 'temp-ps' } or 
        usage( "Could not open %s: $!\n", $IMAGE->{ 'temp-ps' } );

    # Setup the image
    printf $fh "%%!PS\n%%%%BoundingBox: 0 0 %f %f\n%%%%EndComments\n

        /imgsz %f def
        /rad %f def

        <<
            /PageSize [ imgsz imgsz ]
            /ImagingBBox null
        >> setpagedevice

        /mv { moveto } bind def
        /ln { lineto } bind def
        /rmv { rmoveto } bind def
        /rln { rlineto } bind def

        /Ariel-Bold findfont 18 scalefont setfont

        0 0 mv imgsz 0 ln imgsz imgsz ln 0 imgsz ln fill

        1 0 0 0 setcmykcolor

        imgsz 2 div imgsz 2 div translate
    ",
    $IMAGE->{ 'image-size' }, $IMAGE->{ 'image-size' },
    $IMAGE->{ 'image-size' },
    $IMAGE->{ 'image-size' } * $IMAGE->{ 'radius-pcnt' } / 100,
    ;

    # Draw the disc containing the points
    if ( $IMAGE->{ 'draw-disc' } ) {
        printf $fh "0 0 1 0 setcmykcolor
        0 3 360 {
            /a exch def
            a cos rad mul
            a sin rad mul
            a 0 eq { mv } { ln } ifelse
        } for
        stroke
        ";
    }

    # Draw the triangles
    for my $tid ( keys %{$IMAGE->{ 'triangles' }} ) {

        my $t = getTriangle( $tid );

        my $e1 = getEdge( $t->{ 'eid1' } );
        my $e2 = getEdge( $t->{ 'eid2' } );

        my $p1 = getPoint( $e1->{ 'pid1' } );
        my $p2 = getPoint( $e1->{ 'pid2' } );
        my $p3 = getPoint( $e2->{ 'pid1' } );
        $p3 = getPoint( $e2->{ 'pid2' } )
                if ( ( $p3 eq $p1 ) || ( $p3 eq $p2 ) );

        printf $fh "0 1 0 0 setcmykcolor
        /x1 %f rad mul def
        /y1 %f rad mul def
        /x2 %f rad mul def
        /y2 %f rad mul def
        /x3 %f rad mul def
        /y3 %f rad mul def
        x1 y1 mv
        x2 y2 ln
        x3 y3 ln
        closepath
        stroke
        ",
        $p1->{ 'x' }, $p1->{ 'y' },
        $p2->{ 'x' }, $p2->{ 'y' },
        $p3->{ 'x' }, $p3->{ 'y' },
        ;
    }

    # Draw the boundary edges
    printf $fh "0 .6 0 0 setcmykcolor\n";
    for my $eid ( keys %{$IMAGE->{ 'edges' }} ) {

        my $e = getEdge( $eid );
        next unless $e->{ 'boundary' };

        my $p1 = getPoint( $e->{ 'pid1' } );
        my $p2 = getPoint( $e->{ 'pid2' } );
        printf $fh "
        /x1 %f rad mul def
        /y1 %f rad mul def
        /x2 %f rad mul def
        /y2 %f rad mul def
        x1 y1 mv x2 y2 ln stroke
        ",
        $p1->{ 'x' }, $p1->{ 'y' },
        $p2->{ 'x' }, $p2->{ 'y' },
        ;
    }

    # Draw the points
    for my $pid ( keys %{$IMAGE->{ 'points' }} ) {

        my $p = $IMAGE->{ 'points' }->{ $pid };
        printf $fh "
            1 0 0 0 setcmykcolor
            /x1 %f rad mul def
            /y1 %f rad mul def
            x1 y1 mv
            2 0 rln -2 2 rln -2 -2 rln 2 -2 rln fill
            %s {
                1 setgray
                x1 y1 moveto
                (%s) dup stringwidth pop -2 div 4 rmv show
            } if
        ",
        $p->{ 'x' }, $p->{ 'y' },
        $IMAGE->{ 'draw-labels' } ? 'true' : 'false', $pid,
        ;
    }

    close $fh;

    my $cmd = sprintf "/usr/bin/convert '%s' '%s'", 
                    $IMAGE->{ 'temp-ps' },
                    $IMAGE->{ 'dest-pdf' },
    ;
    system( $cmd );

    return;
}

############################################################
# Simple logger functions

sub logDebug {
    logInfo( @_ ) if $IMAGE->{ 'verbose' };
    return;
}

sub logInfo {
    my $frmt = shift;
    printf( "$frmt\n", @_ );
    return;
}

__END__
