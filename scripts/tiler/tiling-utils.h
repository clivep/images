/*******************************************************************************
 *
 * tiling-utils.h
 *
 *  Definitions for tiling utility functions
 *
 ******************************************************************************/

#ifndef _TILING_UTILS_H

#include <stdio.h>

#define RETRY           2
#define OK              1
#define NOK             0

#define ALLOC(typ)      (typ *)calloc( 1, sizeof( typ ) )
#define NALLOC(n,typ)   (typ *)calloc( (n), sizeof( typ ) )

#define STR(str)        #str

/*************************************************
 * List item
 ************************************************/
typedef struct _list_item {

    void                *data;
    struct _list_item   *prev;
    struct _list_item   *next;

} _list_item;

/*************************************************
 * List structure
 ************************************************/
typedef struct {

    int         count;
    _list_item  *first;
    _list_item  *last;
    void        (*free_func)(void *);

} _list;

/*************************************************
 * Logging levels
 ************************************************/
typedef enum {

    LOGGING_LEVEL_QUIET,
    LOGGING_LEVEL_DEBUG

} _logging_level;

/*************************************************
 * Loggin configuration
 ************************************************/

typedef struct {

    _logging_level      logging_level;
    FILE                *log_fp;
} _logging_details;

/*************************************************
 * Prototypes
 ************************************************/
void initialiseLogging( char *logfile, _logging_level logging_level );
void cleanupLogging();
void _log( char *format, va_list ap, char *eol );
void logTxt( char *format, ... );
void logMsg( char *format, ... );
void logFatal( char *format, ... );
void setLogFile( char *log_file );
void setLoggingLevel( _logging_level logging_level );
_logging_level getLoggingLevel();

_list *initialiseList( void (*free_func)(void *) ); 
void addToList( void *data, _list *list );
void releaseListItem( _list_item *item, _list *list );
void emptyList( _list *list );
void releaseList( _list *list );
void *getFromListByPos( short pos, _list *list );
void *getFromListByData( void *addr, _list *list );
short findStringInList( char *string, _list *list );

#define _TILING_UTILS_H
#endif
