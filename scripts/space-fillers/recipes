#!/bin/bash

imgdir=../../image_files/space-fillers/
mkdir $imgdir 2> /dev/null

echo "
    name: Space-filling curves
    desc: Iterative generations and tessellations and Hilbert/Peano curves
    ref: Brainfilling Curves|http://www.fractalcurves.com/
" > $imgdir/type.info 

############################################################
# Constructions

build() {

    template=$1
    iters=$2
    desc=$3

    round=""
    [ "$4" == "" ] && round="-r"

    echo "%!PS" > construction.ps
    echo "%BoundingBox: 0 0 1000.000000 1300.000000" > construction.ps
    echo "%EndComments
        <<
            /PageSize [ 1000.000000 1300.000000 ]
            /ImagingBBox null
        >> setpagedevice
        /setpagedevice { pop } def

        /mv { moveto } bind def
        /ln { lineto } bind def

        0 0 mv 1000.000000 0 ln 1000.000000 1000.000000 ln 0 1000.000000 ln fill

        gsave 0 300 translate
    " >> construction.ps

    ./generator.pl -i $iters -g $template $round -K construction.tmp -o nil.png
    cat construction.tmp >> construction.ps

    ./generator.pl -d -i 1 -g $template -K construction.tmp -o nil.png
    echo "grestore gsave 120 10 translate .3 dup scale"  >> construction.ps
    cat construction.tmp >> construction.ps

    ./generator.pl -i 2 -g $template -K construction.tmp -o nil.png
    echo "grestore gsave 580 10 translate .3 dup scale"  >> construction.ps
    cat construction.tmp >> construction.ps

    convert construction.ps $imgdir/$template-construction.png
    rm nil.png construction.ps construction.tmp

    echo "
        group: Space-filling curves
        thumb-desc: $desc
        thumb-class: spflrThumb
        image-title: $desc
        image-detail: $desc
        image-class: spflrImg
    " > $imgdir/$template-construction.info
}

build ca  7 "Von Koch Snowflake"
build m1  4 "Mandala Curve"
build m2  3 "Snowflake Sweep"
build m3  3 "Fukuda Gosper"
build 13a 4 "Peano Curve"
build 2a 15 "Harter-Heighway Dragon"
build 2b 10 "Polya Sweep"
build 2c 14 "Levy C curve"    x
build 3a  7 "Von Koch Curve"
build 3b  7 "Ter-Dragon"
build 3c  8 "Inverted Ter-Dragon"
build 3d  7 "Root 3 curve"
build 3e  8 "Root 3 curve"
build 3f  8 "Yin Dragon"
build 3g  8 "Root 3 curve"
build 4a  8 "Dragon of Eve"
build 4b  6 "Root 4 curve"
build 4c  8 "Root 4 curve"
build 4d  7 "Serpinski Arrowhead"
build 4e  7 "Root 4 curve"
build 4f  7 "Root 4 curve"
build 4g  6 "Root 4 curve"
build 4h  6 "Peano Sweep"
build 5a  6 "Root 5 curve"
build 5b  6 "Mandelbrot Quartet"
build 5c  6 "Inner-flip Quartet"
build 7a  5 "Snowflake Sweep"
build 7b  5 "Gosper Curve"
build 7c  5 "Root 7 curve"
build 7d  5 "Root 7 curve"
build 9a  6 "Christmas Tree"
build 9b  5 "Reflected Koch"
build 9c  3 "Original Peano Curve"

###########################################################
# Hilbert and Peano curves

./hilbert.pl -i 5 -A H -o $imgdir/hilbert-5.png
echo "
    group: Hilbert/Peano Curves
    thumb-desc: Hilbert Curve
    image-title: Hilbert Curve
    image-detail: Hilbert Curve (iteration 5)
" > $imgdir/hilbert-5.info

./hilbert.pl -i 5 -A H -p -o $imgdir/hilbert-5p.png
echo "
    group: Hilbert/Peano Curves
    thumb-desc: Hilbert Curve
    image-title: Hilbert Curve
    image-detail: Hilbert Curve (iteration 5) projected onto a sphere
" > $imgdir/hilbert-5p.info

./hilbert.pl -i 3 -A P -o $imgdir/peano-3.png
echo "
    group: Hilbert/Peano Curves
    thumb-desc: Peano Curve
    image-title: Peano Curve
    image-detail: Peano Curve (iteration 3)
" > $imgdir/peano-3.info

./hilbert.pl -i 3 -A P -p -o $imgdir/hilbert-3p.png
echo "
    group: Hilbert/Peano Curves
    thumb-desc: Peano Curve
    image-title: Peano Curve
    image-detail: Peano Curve (iteration 3) projected onto a sphere
" > $imgdir/hilbert-3p.info

############################################################
# Compounds

# Closing the Mandal, Snowflake Sweep and Fukuda Gosper
./generator.pl -g m1 -i 4 -F tri2 -S YELLOW -o $imgdir/closed-madala.png
echo "
    group: Compounds
    thumb-desc: Mandala curves
    image-title: Mandala curves
    image-detail: Compound Mandala curves
" > $imgdir/closed-madala.info

./generator.pl -g m2 -i 4 -F tri2 -S YELLOW -o $imgdir/closed-snowflake-sweep.png
echo "
    group: Compounds
    thumb-desc: Snowflake curves
    image-title: Snowflake curves
    image-detail: Compound Snowflake curves
" > $imgdir/closed-snowflake-sweep.info

./generator.pl -g m3 -i 3 -F tri2 -S YELLOW -o $imgdir/closed-fukuda-gosper.png
echo "
    group: Compounds
    thumb-desc: Fukuda curves
    image-title: Fukuda curves
    image-detail: Compound Fukuda curves
" > $imgdir/closed-fukuda-gosper.info

#############################################################
# Tessellations

# Harter-Heighway dragons
./generator.pl -i 14 -g 2a -F b2b -o $imgdir/back2back-hh-dragons.png
echo "
    group: Tessellations
    thumb-desc: HH dragons
    image-title: Tessellating HH dragons
    image-detail: 2 Harter-Heighway dragons tessellated back-to-back
" > $imgdir/back2back-hh-dragons.info

./generator.pl -i 13 -g 2a -F x-in -o $imgdir/head2head-hh-dragons.png
echo "
    group: Tessellations
    thumb-desc: HH dragons
    image-title: Tessellating HH dragons
    image-detail: 4 Harter-Heighway dragons tessellated head-to-head
" > $imgdir/head2head-hh-dragons.info

./generator.pl -i 13 -g 2a -F x-out -o $imgdir/tail2tail-hh-dragons.png
echo "
    group: Tessellations
    thumb-desc: HH dragons
    image-title: Tessellating HH dragons
    image-detail: 4 Harter-Heighway dragons tessellated tail-to-tail
" > $imgdir/tail2tail-hh-dragons.info

# Tessellating root 3 curves
./generator.pl -g 3f -i 9 -F 2l-in -o $imgdir/composite-yin-dragons.png
echo "
    group: Tessellations
    thumb-desc: Yin dragons
    image-title: Tessellating Yin dragons
    image-detail: 2 tessellating Yin dragons
" > $imgdir/composite-yin-dragons.info

./generator.pl -g 3g -i 9 -F 2l-in -o $imgdir/composite-root3.png
echo "
    group: Tessellations
    thumb-desc: Root 3 curves
    image-title: Tessellating root 3 curves
    image-detail: 2 tessellating Root 3 curves
" > $imgdir/composite-root3.info

# Tessellating root 4 curves
./generator.pl -g 4b -i 6 -F x-out -o $imgdir/composite-root4.png
echo "
    group: Tessellations
    thumb-desc: Root 4 curves
    image-title: Tessellating root 4 curves
    image-detail: 2 tessellating Root 4 curves
" > $imgdir/composite-root4.info

# Tessellating root 4 curves
./generator.pl -g 7d -i 5 -F tri -o $imgdir/composite-root7-a.png
echo "
    group: Tessellations
    thumb-desc: Root 7 curves
    image-title: Tessellating root 7 curves
    image-detail: 2 tessellating Root 7 curves
" > $imgdir/composite-root7-a.info

./generator.pl -g 7d -i 5 -F 7d -o $imgdir/composite-root7-b.png
echo "
    group: Tessellations
    thumb-desc: Root 7 curves
    image-title: Tessellating root 7 curves
    image-detail: 2 tessellating Root 7 curves
" > $imgdir/composite-root7-b.info
