#include <stdio.h>
#include <stdlib.h>

#include "tiling-polygons.h"
#include "square.h"

/*******************************************************************************
 *
 * square.c
 *
 *  Functions for generating square tilings
 *
 ******************************************************************************/

#define DEFAULT_PDF_PATH    "square.pdf"
#define RENDER_SCRIPT       "square-draw.pl"

static _list   *templates;

/*************************************************
 * Initialise the plugin
 ************************************************/

void initialisePlugin(
    _logging_level logging_level 
) {
    
    initialiseLogging( NULL, logging_level );

    templates = initialiseList( releasePolygon );

    addToList( initialiseSquareTemplate(), templates );
}

/*************************************************
 * Cleanup the plugin
 ************************************************/

void cleanupPlugin() {

    releaseList( templates );
}

/*************************************************
 * Return the number of edges for the type
 ************************************************/

short polygonTypeEdges(
    _template_type type 
) {
    return 4;
}

/*************************************************
 * Initialise the Square template polygon
 ************************************************/

_polygon *initialiseSquareTemplate() {

    _polygon    *template;

    template = newPolygon( TEMPLATE_TYPE_SQUARE );

    template->sides[ 0 ] = newSide( SIDE_TYPE_EDGE, template );
    template->sides[ 1 ] = newSide( SIDE_TYPE_EDGE, template );
    template->sides[ 2 ] = newSide( SIDE_TYPE_EDGE, template );
    template->sides[ 3 ] = newSide( SIDE_TYPE_EDGE, template );

    template->angles[ 0 ] = PI / 2.0;
    template->angles[ 1 ] = PI / 2.0;
    template->angles[ 2 ] = PI / 2.0;
    template->angles[ 3 ] = PI / 2.0;

    template->vertices[ 0 ] = newVertex( 1.0, 0.0, template->angles[ 0 ] );
    template->vertices[ 1 ] = newVertex( 0.0, 1.0, template->angles[ 1 ] );
    template->vertices[ 2 ] = newVertex( -1.0, 0.0, template->angles[ 2 ] );
    template->vertices[ 3 ] = newVertex( 0.0, -1.0, template->angles[ 3 ] );

    template->base_angle = atan2 (
                    template->vertices[ 1 ]->y - template->vertices[ 0 ]->y,
                    template->vertices[ 1 ]->x - template->vertices[ 0 ]->x
    );

    offsetPolygon( template, 0, 0 );

    logMsg( "Square template @ %lx", (long)template );

    return template;
}

/*************************************************
 * Return the char id for the polygon type
 ************************************************/

char polygonTypeChar(
    _polygon *polygon
) {
    switch( polygon->type ) {

        case TEMPLATE_TYPE_SQUARE:
            return 'S';
    }

    return '?';
}

/*************************************************
 * Return a description of the given side type
 ************************************************/

char *getSideTypeDescription(
    _side_type   pos
) {
    return "E";
}

/*************************************************
 * Test if the sides are allowed to join
 ************************************************/

short checkForValidSides(
    _polygon    *polygon1,      // the existing polygon
    short       edge1,
    _polygon    *polygon2,      // the candidate (template) polygon
    short       edge2
) {
//    _side_type  side_type1;
//    _side_type  side_type2;

    // Check for a valid combination of sides
//    side_type1 = polygon1->sides[ edge1 ]->type;
//    side_type2 = polygon2->sides[ edge2 ]->type;

    logMsg( "Checking sides %lx/%d with %lx/%d",
                                        (long)polygon1, edge1,
                                        (long)polygon2, edge2 
    );

        return OK;

    logMsg( "rejected - bad side combination %lx: (%c/%d) and %lx: (%c/%d)", 
                                        (long)polygon1, polygonTypeChar( polygon1 ), edge1,
                                        (long)polygon2, polygonTypeChar( polygon2 ), edge2
    );

    return NOK;
}

/*************************************************
 * Return a template by its list position
 ************************************************/

_polygon *getTemplateByPos(
    short pos
) {
    return getFromListByPos( pos, templates );
}

/*************************************************
 * Return the number of template polygons
 ************************************************/

short getTemplateCount() {
    return templates->count;
}

/*************************************************
 * Render the image to the given file
 ************************************************/

void drawTiling(
    char    *json_path,
    double  exclusion_radius,
    char    *pdf_path
) {
    char    buf[100];

    if ( pdf_path == NULL )
        pdf_path = DEFAULT_PDF_PATH;

    sprintf( buf, "%s -x %f %s %s", RENDER_SCRIPT,  
                        exclusion_radius, json_path, pdf_path );

    system( buf );
}

/*************************************************
 * Return a template from the list by type
 ************************************************/

_polygon *getTemplateByType(
    _template_type   type
) {

    _polygon    *template;
    _list_item  *item;

    for( item = templates->first; item != NULL; item = item->next ) {

        template = item->data;
        if ( template->type == type )
            return template;
    }

    return NULL;
}

/*************************************************
 * Return the number of side types available
 ************************************************/

int getSideTypeCount() {

    return SIDE_TYPE_COUNT;
}

/*************************************************
 * Find the pos of the type in the template list
 ************************************************/

short findTemplateTypePos(
    _template_type  type
) {
    _polygon    *template;
    short       pos;

    for( pos = 0; pos < templates->count; pos++ ) {

        template = getFromListByPos( pos, templates );
        if ( template->type == type )
            return pos;
    }

    return -1;
}
