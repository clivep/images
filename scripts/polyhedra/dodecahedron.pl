#!/usr/bin/perl

use warnings;
use strict;

use Getopt::Std;
use Math::Trig;
use Data::Dumper;

################################################################################
#
#   dodecahedron.pl
#
#   Pentagonal rings in a dodecahedron
################################################################################

use constant PHI        =>  ( 1.0 + sqrt( 5.0 ) ) / 2.0;

use constant FACE_TYPE_BASE             =>  'base';
use constant FACE_TYPE_EDGE             =>  'edge';
use constant FACE_TYPE_FACE             =>  'face';
use constant FACE_TYPE_EDGE_TUBE        =>  'edge_tube';
use constant FACE_TYPE_FACE_TUBE        =>  'face_tube';

use constant YELLOW                     =>  'yellow';
use constant MAGENTA                    =>  'magenta';

use constant PS_TEMP                    =>  '/tmp/dodecahedron.ps';

use constant DEFAULT_YML_FILE           =>  '/tmp/dodecahedron.yml';

use constant DEFAULT_DEST               =>  'dodecahedron.pdf';
use constant DEFAULT_SIDE               =>  200;
use constant DEFAULT_TUBE               =>  10;
use constant DEFAULT_COLOR              =>  YELLOW;

use constant DEFAULT_BASE_MULTIPLIER    =>  0.8;
use constant DEFAULT_EDGE_MULTIPLIER    =>  1;

############################################################
# Check the runline options

my %options;
getopts( 'df:hkm:n:s:t:vx:y:z:D:Y', \%options );

help() if $options{ 'h' };

##########################################
# Setup the working environment

my $IMAGE = {

    'SIDE'              =>  $options{ 's' } || DEFAULT_SIDE,
    'TUBE'              =>  $options{ 't' } || DEFAULT_TUBE,
    'BASE_MULTIPLIER'   =>  $options{ 'm' } || DEFAULT_BASE_MULTIPLIER,
    'EDGE_MULTIPLIER'   =>  $options{ 'n' } || DEFAULT_EDGE_MULTIPLIER,

    'VERTICES'          =>  {},
    'EDGES'             =>  {},
    'FACES'             =>  {},

    'WIDTH'             =>  1000,
    'DEPTH'             =>  1000,

    'X_ANG'             =>  $options{ 'x' } || 0,
    'Y_ANG'             =>  $options{ 'y' } || 0,
    'Z_ANG'             =>  $options{ 'z' } || 0,

    'DEST'              =>  DEFAULT_DEST,
};

setupBaseDodecahedron();
processImage();
dumpImage() if $options{ 'd' } || $options{ 'v' };
dumpYaml() if $options{ 'Y' } || $options{ 'f' };
drawImage();

exit 0;

##########################################
# Setup the base polyhedron vertices

sub setupBaseVertices {

    my $sz = $IMAGE->{ 'SIDE' } / ( sqrt( 5.0 ) - 1.0 );

    my $p1 = $sz * PHI;
    my $p2 = $sz / PHI;

    addVertex(  $sz,  $sz, -$sz );
    addVertex(  $sz,  $sz,  $sz );
    addVertex( -$sz,  $sz,  $sz );
    addVertex( -$sz,  $sz, -$sz );

    addVertex(  $sz, -$sz, -$sz );
    addVertex(  $sz, -$sz,  $sz );
    addVertex( -$sz, -$sz,  $sz );
    addVertex( -$sz, -$sz, -$sz );

    addVertex(    0,  $p2,  $p1 );
    addVertex(    0, -$p2,  $p1 );
    addVertex(    0,  $p2, -$p1 );
    addVertex(    0, -$p2, -$p1 );

    addVertex(  $p2,  $p1,  0   );
    addVertex( -$p2,  $p1,  0   );
    addVertex(  $p2, -$p1,  0   );
    addVertex( -$p2, -$p1,  0   );

    addVertex(  $p1,    0,  $p2 );
    addVertex( -$p1,    0,  $p2 );
    addVertex(  $p1,    0, -$p2 );
    addVertex( -$p1,    0, -$p2 );
}

##########################################
# Setup the base polyhedron faces

sub setupBaseFaces {

    addBaseFace( "v18", "v4",  "v11", "v10", "v0" );
    addBaseFace( "v0",  "v10", "v3",  "v13", "v12" );
    addBaseFace( "v0",  "v12", "v1",  "v16", "v18" );
    addBaseFace( "v12", "v13", "v2",  "v8",  "v1" );
    addBaseFace( "v1",  "v8",  "v9",  "v5",  "v16" );
    addBaseFace( "v2",  "v13", "v3",  "v19", "v17" );
    addBaseFace( "v17", "v6",  "v9",  "v8",  "v2" );
    addBaseFace( "v3",  "v10", "v11", "v7",  "v19" );
    addBaseFace( "v14", "v15", "v7",  "v11", "v4" );
    addBaseFace( "v18", "v16", "v5",  "v14", "v4" );
    addBaseFace( "v5",  "v9",  "v6",  "v15", "v14" );
    addBaseFace( "v17", "v19", "v7",  "v15", "v6" );
}

##########################################
# Setup the base polyhedron

sub setupBaseDodecahedron {

    my $debug = $options{ 'D' } || '';

    if ( $debug =~ /\bD\b/ ) {

        my $sz = $IMAGE->{ 'SIDE' } / ( 2 * sin( pi / 5 ) );

        my $z = $IMAGE->{ 'SIDE' } * sqrt( ( 5 / 2 ) + ( 11 * sqrt( 5 ) / 10 ) ) /  2;

        for( my $a = 0; $a < 5; $a++ ) {

            addVertex(
                $sz * cos( $a * 2 * pi / 5 ),
                $sz * sin( $a * 2 * pi / 5 ),
                $z,
            );
        }
        rotateVertices();
        addBaseFace( "v0", "v1", "v2", "v3", "v4" );
        return;
    }

    setupBaseVertices();
    rotateVertices();
    setupBaseFaces();
}

##########################################
# Process the base dodecahedron

sub processImage {

    # Find the centre lines for the pentagonal tubes
    processEdges();
    processBaseFaces();

    # Now build the tubes
    processEdgeTubes();
    processFaceTubes();
}

##########################################
# Turn a vertex into a coordindate array

sub vertexAsArray {
    
    my $vid = shift;
    my $vertex = $IMAGE->{ 'VERTICES' }->{ $vid };

    return (
        $vertex->{ 'X' },
        $vertex->{ 'Y' }, 
        $vertex->{ 'Z' },
    )
}

##########################################
# Turn a vertex into a coordindate array ref

sub vertexAsArrayRef {
    
    my @arr = vertexAsArray( shift );
    return \@arr;
}

##########################################
# Dump yaml for a face

sub dumpFaceYaml {

    my $fh = shift;
    my $face = shift;

    my $vertices = $face->{ 'VERTICES' };

    my @rgb = getFaceColor( $face );

    my $format = "    object:
        type: triangle
        vertex1:
            i: %f
            j: %f
            k: %f
        vertex2:
            i: %f
            j: %f
            k: %f
        vertex3:
            i: %f
            j: %f
            k: %f
        surface: default
        scheme-name: face
        color:
            red: %f
            green: %f
            blue: %f
    \n";

    printf $fh $format, 
        vertexAsArray( $vertices->[ 0 ] ),
        vertexAsArray( $vertices->[ 1 ] ),
        vertexAsArray( $vertices->[ 2 ] ),
        @rgb,
    ;

    printf $fh $format, 
        vertexAsArray( $vertices->[ 0 ] ),
        vertexAsArray( $vertices->[ 2 ] ),
        vertexAsArray( $vertices->[ 3 ] ),
        @rgb,
    ;
}

##########################################
# Dump the image as ray-tracing yaml

sub dumpYaml {

    my $dest = $options{ 'f' } || DEFAULT_YML_FILE;

    open my $fh, '>', $dest or die "Could not open $dest $!\n";

    # Write the header yaml (from the DATA section)
    my @yml_hdr = <DATA>;
    my $in_data = 0;
    for ( @yml_hdr ) {

        print $fh $_ if $in_data;
        $in_data = 1 if /__DATA__/;
    }

    my $face_count = keys %{ $IMAGE->{ 'FACES' } };

    for my $fid ( keys %{ $IMAGE->{ 'FACES' } } ) {

        my $face = $IMAGE->{ 'FACES' }->{ $fid };
        next if (
            ( $face->{ 'TYPE' } ne FACE_TYPE_FACE_TUBE ) && 
            ( $face->{ 'TYPE' } ne FACE_TYPE_EDGE_TUBE )
        );

        dumpFaceYaml( $fh, $face );
    }

    close $fh;

    printf "%s\n", $dest unless $options{ 'f' };

    exit;
}

##########################################
# Dump the image face and edge data

sub dumpImage {

    if ( $options{ 'd' } ) {

        printf "\n";
    
        my $face_count = keys %{ $IMAGE->{ 'FACES' } };
    
        my $debug = $options{ 'D' } || '';

        for( my $f = 0; $f < $face_count; $f++ ) {

            my $fid = sprintf "f%d", $f;
            next if ( $debug =~ /\bf\d+\b/ ) && ( $debug !~ /\b$fid\b/ );

            my $face = $IMAGE->{ 'FACES' }->{ $fid };
    
            my $parent = $face->{ 'PARENT' } ? sprintf " parent: %s", $face->{ 'PARENT' } : '';

            printf "Face: %s (%s%s)\n", $fid, $face->{ 'TYPE' }, $parent;
   
            printf "    Normal [ %0.4f, %0.4f, %0.4f ]\n", @{ $face->{ 'NORMAL' } };
    
            for my $eid ( @{ $face->{ 'EDGES' } } ) {
    
                my $edge = $IMAGE->{ 'EDGES' }->{ $eid };
    
                printf "    Edge %s (%f)\n", $eid, $edge->{ 'LENTH' };
            }
        }
    }

    if ( $options{ 'v' } ) {

        printf "\n";

        my $vertex_count = keys %{ $IMAGE->{ 'VERTICES' } };

        for( my $v = 0; $v < $vertex_count; $v++ ) {
                
            my $vid = sprintf "v%d", $v;
            my $vertex = $IMAGE->{ 'VERTICES' }->{ $vid };
            printf "Vertex %s (%f, %f, %f)\n", $vid, $vertex->{ 'X' }, $vertex->{ 'Y' }, $vertex->{ 'Z' };
        }
    }

    exit;
}

##########################################
# Add a new vertex to the image

sub addVertex {

    my $vid = sprintf( "v%d", scalar keys %{ $IMAGE->{ 'VERTICES' } } );

    $IMAGE->{ 'VERTICES' }->{ $vid } = {
        X   => shift,
        Y   => shift,
        Z   => shift,
    };

    return $vid;
}

##########################################
# Return the numbered face vertex

sub getFaceVertex {

    my $face = shift;
    my $vertex = shift;

    my $vid = $face->{ 'VERTICES' }->[ $vertex ];

    return $IMAGE->{ 'VERTICES' }->{ $vid };
}

##########################################
# Normalise the given vector

sub normaliseVector {

    my $vector = shift;

    my $m = sqrt(
        $vector->[ 0 ] * $vector->[ 0 ] +
        $vector->[ 1 ] * $vector->[ 1 ] +
        $vector->[ 2 ] * $vector->[ 2 ]
    );

    $vector->[ 0 ] /= $m;
    $vector->[ 1 ] /= $m;
    $vector->[ 2 ] /= $m;
}

##########################################
# Find the distance between 2 vertex ids

sub distance {

    my $vid1 = shift;
    my $vid2 = shift;

    my $vertex1 = $IMAGE->{ 'VERTICES' }->{ $vid1 };
    my $vertex2 = $IMAGE->{ 'VERTICES' }->{ $vid2 };

    return sqrt(
            ( $vertex1->{ 'X' } - $vertex2->{ 'X' } ) * ( $vertex1->{ 'X' } - $vertex2->{ 'X' } ) +
            ( $vertex1->{ 'Y' } - $vertex2->{ 'Y' } ) * ( $vertex1->{ 'Y' } - $vertex2->{ 'Y' } ) +
            ( $vertex1->{ 'Z' } - $vertex2->{ 'Z' } ) * ( $vertex1->{ 'Z' } - $vertex2->{ 'Z' } )
    );
}

##########################################
# Find the cross product of the 2 vrs

sub crossProduct {

    my $v1 = shift;
    my $v2 = shift;

    return [
        $v1->[ 1 ] * $v2->[ 2 ] - $v2->[ 1 ] * $v1->[ 2 ],
        $v1->[ 2 ] * $v2->[ 0 ] - $v2->[ 2 ] * $v1->[ 0 ],
        $v1->[ 0 ] * $v2->[ 1 ] - $v2->[ 0 ] * $v1->[ 1 ],
    ];
}

##########################################
# Find normals for the face and it's edges

sub processFaceNormals {

    my $face = shift;

    for ( my $e = 0; $e < scalar @{ $face->{ 'EDGES' } }; $e++ ) {

        my $eid = $face->{ 'EDGES' }->[ $e ];
        my $edge = $IMAGE->{ 'EDGES' }->{ $eid };

        my $vertex1 = $IMAGE->{ 'VERTICES' }->{ $edge->{ 'VID1' } };
        my $vertex2 = $IMAGE->{ 'VERTICES' }->{ $edge->{ 'VID2' } };

        my $mid_point = $IMAGE->{ 'VERTICES' }->{ $edge->{ 'MID_POINT' } };

        # Find the normal vector at the edge midpoint (from the face centre)
        my $m_normal = [
            $mid_point->{ 'X' } - $face->{ 'XC' },
            $mid_point->{ 'Y' } - $face->{ 'YC' },
            $mid_point->{ 'Z' } - $face->{ 'ZC' },
        ];
        normaliseVector( $m_normal );
        $edge->{ 'MID_NORMAL' } = $m_normal;

        # Find the normal to the face (just once)
        if ( $e == 0 ) {

            my $edge_vector = $edge->{ 'EDGE_VECTOR' };

            # The face normal is the cross product of the edge vector and the midpoint normal
            $face->{ 'NORMAL' } = crossProduct( $m_normal, $edge_vector );
            normaliseVector( $face->{ 'NORMAL' } );
        }
    }
}

##########################################
# Return the id of an edge

sub edgeId { return sprintf "e%s-%s", shift, shift  }

##########################################
# Add a new edge to the image

sub addEdge {

    my $vid1 = shift;
    my $vid2 = shift;

    my $eid = edgeId( $vid1, $vid2 );

    unless ( $IMAGE->{ 'EDGES' }->{ $eid } ) {

        my $vertex1 = $IMAGE->{ 'VERTICES' }->{ $vid1 };
        my $vertex2 = $IMAGE->{ 'VERTICES' }->{ $vid2 };

        my @mid_point = (
            ( $vertex1->{ 'X' } + $vertex2->{ 'X' } ) / 2,
            ( $vertex1->{ 'Y' } + $vertex2->{ 'Y' } ) / 2,
            ( $vertex1->{ 'Z' } + $vertex2->{ 'Z' } ) / 2,
        );

        # Find the unit vector from the midpoint to the first vertex
        my $edge_vector = [
            $vertex1->{ 'X' } - $mid_point[ 0 ],
            $vertex1->{ 'Y' } - $mid_point[ 1 ],
            $vertex1->{ 'Z' } - $mid_point[ 2 ],
        ];
        normaliseVector( $edge_vector );

        $IMAGE->{ 'EDGES' }->{ $eid } = {
            'VID1'          => $vid1,
            'VID2'          => $vid2,
            'LENTH'         => distance( $vid1, $vid2 ),
            'FACES'         => [],
            'MID_POINT'     => addVertex( @mid_point ),
            'EDGE_VECTOR'   => $edge_vector,
        };
    }

    return $eid;
}

##########################################
# Add a new face to the image

sub addBaseFace { return addFace( FACE_TYPE_BASE, @_ ) }
sub addFaceFace { return addFace( FACE_TYPE_FACE, @_ ) }
sub addEdgeFace { return addFace( FACE_TYPE_EDGE, @_ ) }
sub addEdgeTubeFace { return addFace( FACE_TYPE_EDGE_TUBE, @_ ) }
sub addFaceTubeFace { return addFace( FACE_TYPE_FACE_TUBE, @_ ) }

sub addFace {

    my $type = shift;
    my @vertices = @_;

    my $count = scalar @vertices;
    my $fid = sprintf( "f%d", scalar keys $IMAGE->{ 'FACES' } );

    # Find the centre of the face
    my $xc = 0;
    my $yc = 0;
    my $zc = 0;

    my @edges;

    for ( my $v = 0; $v < $count; $v++ ) {

        my $vid = $vertices[ $v ];
        my $vertex = $IMAGE->{ 'VERTICES' }->{ $vid };

        $xc += $vertex->{ 'X' };
        $yc += $vertex->{ 'Y' };
        $zc += $vertex->{ 'Z' };

        my $eid = addEdge( $vid, $vertices[ $v - 1 ] );
        push @edges, $eid;

        push @{ $IMAGE->{ 'EDGES' }->{ $eid }->{ 'FACES' } }, $fid;
    };

    my $face = {
        'ID'        => $fid,
        'TYPE'      => $type,
        'EDGES'     => \@edges,
        'VERTICES'  => \@vertices,
        'XC'        => $xc / $count,
        'YC'        => $yc / $count,
        'ZC'        => $zc / $count,
    };

    processFaceNormals( $face );

    $IMAGE->{ 'FACES' }->{ $fid } = $face;

    return $fid;
}

##########################################
# Process the base dodecahedron edges

sub processEdges {

    for my $fid ( keys %{ $IMAGE->{ 'FACES' } } ) {

        my $face = $IMAGE->{ 'FACES' }->{ $fid };
        next if $face->{ 'TYPE' } ne FACE_TYPE_BASE;

        for( my $e = 0; $e < scalar @{ $face->{ 'EDGES' } }; $e++ ) {

            my $eid = $face->{ 'EDGES' }->[ $e ];
            my $edge = $IMAGE->{ 'EDGES' }->{ $eid };

            next if $edge->{ 'PROCESSED_BASE_FACE' };

            # Find the normal at the midpoint from the figure centre
            my $cnrm = vertexAsArrayRef( $edge->{ 'MID_POINT' } );
            normaliseVector( $cnrm );

            # Find the vector perpendicular to the edge and the normal
            my $pnrm = crossProduct( $edge->{ 'EDGE_VECTOR' }, $cnrm );
            normaliseVector( $pnrm );

            my $sz = ( $IMAGE->{ 'SIDE' } * $IMAGE->{ 'BASE_MULTIPLIER' } ) / ( 2 * sin( pi / 5 ) );

            # The new pentagons will be centred at the (adjusted) midpoints
            my $mpt = $IMAGE->{ 'VERTICES' }->{ $edge->{ 'MID_POINT' } };

            # Find the distance to move the edge pantagons away from their initial centres
            my $mul = $IMAGE->{ 'EDGE_MULTIPLIER' } * $IMAGE->{ 'TUBE' };

            # Move the centre points away from the figure centre
            $mpt->{ 'X' } += $mul * $cnrm->[ 0 ];
            $mpt->{ 'Y' } += $mul * $cnrm->[ 1 ];
            $mpt->{ 'Z' } += $mul * $cnrm->[ 2 ];

            my @new_vertices;

            for ( my $a = 0; $a < 5; $a++ ) {

                my $ang = $a * 2 * pi / 5;

                push @new_vertices, addVertex(
                    $mpt->{ 'X' } + ( $cnrm->[ 0 ] * cos( $ang ) + $pnrm->[ 0 ] * sin( $ang ) ) * $sz,
                    $mpt->{ 'Y' } + ( $cnrm->[ 1 ] * cos( $ang ) + $pnrm->[ 1 ] * sin( $ang ) ) * $sz,
                    $mpt->{ 'Z' } + ( $cnrm->[ 2 ] * cos( $ang ) + $pnrm->[ 2 ] * sin( $ang ) ) * $sz,
                );
            }

            addEdgeFace( @new_vertices );

            $edge->{ 'PROCESSED_BASE_FACE' } = 1;
            my $id = edgeId( $edge->{ 'VID2' }, $edge->{ 'VID1' } );
            $IMAGE->{ 'EDGES' }->{ $id }->{ 'PROCESSED_BASE_FACE' } = 1;
        }
    }
}

##########################################
# Process the base dodecahedron faces

sub processBaseFaces {

    for my $fid ( keys %{ $IMAGE->{ 'FACES' } } ) {

        my $face = $IMAGE->{ 'FACES' }->{ $fid };
        next unless $face->{ 'TYPE' } eq FACE_TYPE_BASE;

        my @new_vertices;

        my $mul = $IMAGE->{ 'BASE_MULTIPLIER' };

        for ( my $v = 0; $v < scalar @{ $face->{ 'VERTICES' } }; $v++ ) {

            my $vid =  $face->{ 'VERTICES' }->[ $v ];
            my $vertex =  $IMAGE->{ 'VERTICES' }->{ $vid };

            my $nx = ( $mul * ( $vertex->{ 'X' } - $face->{ 'XC' } ) + $face->{ 'XC' } );
            my $ny = ( $mul * ( $vertex->{ 'Y' } - $face->{ 'YC' } ) + $face->{ 'YC' } );
            my $nz = ( $mul * ( $vertex->{ 'Z' } - $face->{ 'ZC' } ) + $face->{ 'ZC' } );

            push @new_vertices, addVertex( $nx, $ny, $nz );
        }

        my $nfid = addFaceFace( @new_vertices );
        $IMAGE->{ 'FACES' }->{ $nfid }->{ 'PARENT' } = $fid;
    }
}

##########################################
# Process faces centred on the base edges

sub processEdgeTubes {

    for my $fid ( keys %{ $IMAGE->{ 'FACES' } } ) {

        my $face = $IMAGE->{ 'FACES' }->{ $fid };
        next if $face->{ 'TYPE' } ne FACE_TYPE_EDGE;

        my $edge_count = scalar @{ $face->{ 'EDGES' } };

        for( my $e = 0; $e < $edge_count; $e++ ) {

            my $eid = $face->{ 'EDGES' }->[ $e ];
            my $edge = $IMAGE->{ 'EDGES' }->{ $eid };

            next if $edge->{ 'PROCESSED_BASE_EDGE' };

            my $edge_vector = $edge->{ 'EDGE_VECTOR' };

            my ( $pnx, $pny, $pnz ) = @{ $face->{ 'NORMAL' } };
            my ( $mnx, $mny, $mnz ) = @{ $edge->{ 'MID_NORMAL' } };

            for ( my $a = 0; $a < 5; $a++ ) {

                my @new_vertices;

                my $ang = 2 * $a * pi / 5;

                my $vtx1 = $IMAGE->{ 'VERTICES' }->{ $edge->{ 'VID1' } };
                my $vtx2 = $IMAGE->{ 'VERTICES' }->{ $edge->{ 'VID2' } };

                my $ca = cos( $ang );
                my $sa = sin( $ang );

                # The edges need to meet at the elbows with adjacent edges
                my $mul = $IMAGE->{ 'TUBE' } * $ca / tan( 3 * pi / 10 );

                push @new_vertices, addVertex(
                    $vtx2->{ 'X' } + ( $mnx * $ca + $pnx * $sa ) * $IMAGE->{ 'TUBE' } - $mul * $edge_vector->[ 0 ],
                    $vtx2->{ 'Y' } + ( $mny * $ca + $pny * $sa ) * $IMAGE->{ 'TUBE' } - $mul * $edge_vector->[ 1 ],
                    $vtx2->{ 'Z' } + ( $mnz * $ca + $pnz * $sa ) * $IMAGE->{ 'TUBE' } - $mul * $edge_vector->[ 2 ],
                );

                push @new_vertices, addVertex(
                    $vtx1->{ 'X' } + ( $mnx * $ca + $pnx * $sa ) * $IMAGE->{ 'TUBE' } + $mul * $edge_vector->[ 0 ],
                    $vtx1->{ 'Y' } + ( $mny * $ca + $pny * $sa ) * $IMAGE->{ 'TUBE' } + $mul * $edge_vector->[ 1 ],
                    $vtx1->{ 'Z' } + ( $mnz * $ca + $pnz * $sa ) * $IMAGE->{ 'TUBE' } + $mul * $edge_vector->[ 2 ],
                );

                $ang = 2 * ( $a + 1 ) * pi / 5;

                $ca = cos( $ang );
                $sa = sin( $ang );

                $mul = $IMAGE->{ 'TUBE' } * $ca / tan( 3 * pi / 10 );

                push @new_vertices, addVertex(
                    $vtx1->{ 'X' } + ( $mnx * $ca + $pnx * $sa ) * $IMAGE->{ 'TUBE' } + $mul * $edge_vector->[ 0 ],
                    $vtx1->{ 'Y' } + ( $mny * $ca + $pny * $sa ) * $IMAGE->{ 'TUBE' } + $mul * $edge_vector->[ 1 ],
                    $vtx1->{ 'Z' } + ( $mnz * $ca + $pnz * $sa ) * $IMAGE->{ 'TUBE' } + $mul * $edge_vector->[ 2 ],
                );

                push @new_vertices, addVertex(
                    $vtx2->{ 'X' } + ( $mnx * $ca + $pnx * $sa ) * $IMAGE->{ 'TUBE' } - $mul * $edge_vector->[ 0 ],
                    $vtx2->{ 'Y' } + ( $mny * $ca + $pny * $sa ) * $IMAGE->{ 'TUBE' } - $mul * $edge_vector->[ 1 ],
                    $vtx2->{ 'Z' } + ( $mnz * $ca + $pnz * $sa ) * $IMAGE->{ 'TUBE' } - $mul * $edge_vector->[ 2 ],
                );

                my $nfid = addEdgeTubeFace( @new_vertices );
                $IMAGE->{ 'FACES' }->{ $nfid }->{ 'PARENT' } = $fid;
            }

            $edge->{ 'PROCESSED_BASE_EDGE' } = 1;
            my $id = edgeId( $edge->{ 'VID2' }, $edge->{ 'VID1' } );
            $IMAGE->{ 'EDGES' }->{ $id }->{ 'PROCESSED_BASE_EDGE' } = 1;
        }
    }
}

##########################################
# Process faces centred on the base faces

sub processFaceTubes {

    for my $fid ( keys %{ $IMAGE->{ 'FACES' } } ) {

        my $face = $IMAGE->{ 'FACES' }->{ $fid };
        next if $face->{ 'TYPE' } ne FACE_TYPE_FACE;

        my $edge_count = scalar @{ $face->{ 'EDGES' } };

        for( my $e = 0; $e < $edge_count; $e++ ) {

            my $eid = $face->{ 'EDGES' }->[ $e ];
            my $edge = $IMAGE->{ 'EDGES' }->{ $eid };

            my $edge_vector = $edge->{ 'EDGE_VECTOR' };

            my ( $pnx, $pny, $pnz ) = @{ $face->{ 'NORMAL' } };
            my ( $mnx, $mny, $mnz ) = @{ $edge->{ 'MID_NORMAL' } };

            for ( my $a = 0; $a < 5; $a++ ) {

                my @new_vertices;

                my $vtx1 = $IMAGE->{ 'VERTICES' }->{ $edge->{ 'VID1' } };
                my $vtx2 = $IMAGE->{ 'VERTICES' }->{ $edge->{ 'VID2' } };

                my $ang = 2 * $a * pi / 5;
                
                my $ca = cos( $ang );
                my $sa = sin( $ang );

                # The edges need to meet at the elbows with adjacent edges
                my $mul = $IMAGE->{ 'TUBE' } * $ca / tan( 3 * pi / 10 );

                push @new_vertices, addVertex(
                    $vtx2->{ 'X' } + ( $mnx * $ca + $pnx * $sa ) * $IMAGE->{ 'TUBE' } - $mul * $edge_vector->[ 0 ],
                    $vtx2->{ 'Y' } + ( $mny * $ca + $pny * $sa ) * $IMAGE->{ 'TUBE' } - $mul * $edge_vector->[ 1 ],
                    $vtx2->{ 'Z' } + ( $mnz * $ca + $pnz * $sa ) * $IMAGE->{ 'TUBE' } - $mul * $edge_vector->[ 2 ],
                );

                push @new_vertices, addVertex(
                    $vtx1->{ 'X' } + ( $mnx * $ca + $pnx * $sa ) * $IMAGE->{ 'TUBE' } + $mul * $edge_vector->[ 0 ],
                    $vtx1->{ 'Y' } + ( $mny * $ca + $pny * $sa ) * $IMAGE->{ 'TUBE' } + $mul * $edge_vector->[ 1 ],
                    $vtx1->{ 'Z' } + ( $mnz * $ca + $pnz * $sa ) * $IMAGE->{ 'TUBE' } + $mul * $edge_vector->[ 2 ],
                );

                $ang = 2 * ( $a + 1 ) * pi / 5;

                $ca = cos( $ang );
                $sa = sin( $ang );

                $mul = $IMAGE->{ 'TUBE' } * $ca / tan( 3 * pi / 10 );

                push @new_vertices, addVertex(
                    $vtx1->{ 'X' } + ( $mnx * $ca + $pnx * $sa ) * $IMAGE->{ 'TUBE' } + $mul * $edge_vector->[ 0 ],
                    $vtx1->{ 'Y' } + ( $mny * $ca + $pny * $sa ) * $IMAGE->{ 'TUBE' } + $mul * $edge_vector->[ 1 ],
                    $vtx1->{ 'Z' } + ( $mnz * $ca + $pnz * $sa ) * $IMAGE->{ 'TUBE' } + $mul * $edge_vector->[ 2 ],
                );

                push @new_vertices, addVertex(
                    $vtx2->{ 'X' } + ( $mnx * $ca + $pnx * $sa ) * $IMAGE->{ 'TUBE' } - $mul * $edge_vector->[ 0 ],
                    $vtx2->{ 'Y' } + ( $mny * $ca + $pny * $sa ) * $IMAGE->{ 'TUBE' } - $mul * $edge_vector->[ 1 ],
                    $vtx2->{ 'Z' } + ( $mnz * $ca + $pnz * $sa ) * $IMAGE->{ 'TUBE' } - $mul * $edge_vector->[ 2 ],
                );

                my $nfid = addFaceTubeFace( @new_vertices );
                $IMAGE->{ 'FACES' }->{ $nfid }->{ 'PARENT' } = $fid;
            }
        }
    }
}

##########################################
# Rotate the base dodecahedron vertices

sub rotateVertices {

    for my $vid ( keys $IMAGE->{ 'VERTICES' } ) {

        my $vertex = $IMAGE->{ 'VERTICES' }->{ $vid };

        my $x = $vertex->{ 'X' };
        my $y = $vertex->{ 'Y' };
        my $z = $vertex->{ 'Z' };

        my $xang = deg2rad( $IMAGE->{ 'X_ANG' } );
        if ( $xang ) {
            my $tmp = $y;
            $y = $tmp * cos( $xang ) - $z * sin( $xang );
            $z = $tmp * sin( $xang ) + $z * cos( $xang );
        }

        my $yang = deg2rad( $IMAGE->{ 'Y_ANG' } );
        if ( $yang ) {
            my $tmp = $x;
            $x = $tmp * cos( $yang ) + $z * sin( $yang );
            $z = $z * cos( $yang ) - $tmp * sin( $yang );
        }

        my $zang = deg2rad( $IMAGE->{ 'Z_ANG' } );
        if ( $zang ) {
            my $tmp = $x;
            $x = $tmp * cos( $zang ) - $y * sin( $zang );
            $y = $tmp * sin( $zang ) + $y * cos( $zang );
        }

        $IMAGE->{ 'VERTICES' }->{ $vid } = {
            'X' =>  $x,
            'Y' =>  $y,
            'Z' =>  $z,
        };
    }
}

##########################################
# Simple helper function

sub max { return $_[0] > $_[1] ? $_[0] : $_[1] }

##########################################
# Find the color for the face

sub getFaceColor {

    my $face = shift;

    my $face_type = $face->{ 'TYPE' };

    my $debug = $options{ 'D' } || '';

    if ( $debug =~ /\bE\b/ ) {
        return ( 0, 1, 1 ) if $face_type eq FACE_TYPE_EDGE;
        return ( 1, 0, 1 ) if $face_type eq FACE_TYPE_FACE;
        return ( 1, 1, 0 ) if $face_type eq FACE_TYPE_BASE;
    }

    return ( 1, 1, 0 ) if $face_type eq FACE_TYPE_FACE_TUBE;

    my $normal = $face->{ 'NORMAL' };
    if (
        ( $face_type eq FACE_TYPE_EDGE_TUBE )
    ) {
        my $parent = $face->{ 'PARENT' };
        $normal = $IMAGE->{ 'FACES' }->{ $parent }->{ 'NORMAL' };
    }

    my $ang = rad2deg( atan2(
        $normal->[ 1 ],
        $normal->[ 2 ],
    ) );
    $ang = ( 360 + $ang ) % 360;

    my ( $r, $g, $b, $m ) = ( 0 )x4;

    if ( $ang < 120 ) {

        $b = $ang / 120;
        $r = 1 - $b;
    }
    elsif ( $ang < 240 ) {

        $g = ( $ang - 120 ) / 120;
        $b = 1 - $g;
    }
    else {

        $r = ( $ang - 240 ) / 120;
        $g = 1 - $r;
    }

    $m = max( max( $r, $g ), $b );

    return ( $r / $m, $g / $m, $b / $m );
}

##########################################
# Draw the normals on a face

sub drawFaceNormals {

    my $fh = shift;
    my $face = shift;

    my $debug = $options{ 'D' } || '';

    return unless ( ( $debug =~ /\bN\b/ ) && ( $face->{ 'TYPE' } eq FACE_TYPE_FACE ) );

    printf $fh "%f %f mv %f %f ln 1 setgray stroke\n",
        $face->{ 'XC' },
        $face->{ 'YC' },
        $face->{ 'XC' } + $IMAGE->{ 'SIDE' } * $face->{ 'NORMAL' }->[ 0 ] / 2,
        $face->{ 'YC' } + $IMAGE->{ 'SIDE' } * $face->{ 'NORMAL' }->[ 1 ] / 2,
    ;

    for my $eid ( @{ $face->{ 'EDGES' } } ) {

        my $edge = $IMAGE->{ 'EDGES' }->{ $eid };
        my $mid_point = $IMAGE->{ 'VERTICES' }->{ $edge->{ 'MID_POINT' } };

        printf $fh "%f %f mv %f %f ln 0 1 0 setrgbcolor stroke\n",
            $mid_point->{ 'X' },
            $mid_point->{ 'Y' },
            $mid_point->{ 'X' } + $IMAGE->{ 'TUBE' } * $edge->{ 'MID_NORMAL' }->[ 0 ],
            $mid_point->{ 'Y' } + $IMAGE->{ 'TUBE' } * $edge->{ 'MID_NORMAL' }->[ 1 ],
        ;

        printf $fh "%f %f mv %f %f ln 1 1 0 setrgbcolor stroke\n",
            $mid_point->{ 'X' },
            $mid_point->{ 'Y' },
            $mid_point->{ 'X' } + $IMAGE->{ 'TUBE' } * $edge->{ 'EDGE_VECTOR' }->[ 0 ],
            $mid_point->{ 'Y' } + $IMAGE->{ 'TUBE' } * $edge->{ 'EDGE_VECTOR' }->[ 1 ],
        ;
    }
}
    
##########################################
# Draw labels

sub drawLabels {

    my $fh = shift;
    my $face = shift;
    
    my $debug = $options{ 'D' } || '';

    # Draw face labels
    if ( $debug =~ /\bV\b/ ) {

        for ( my $v = 0; $v < scalar @{ $face->{ 'VERTICES' } }; $v++ ) {

            my $vid =  $face->{ 'VERTICES' }->[ $v ];
            my $vertex =  $IMAGE->{ 'VERTICES' }->{ $vid };

            printf $fh "%f 8 add %f 8 add mv (%s) show ",
                        $vertex->{ 'X' },
                        $vertex->{ 'Y' },
                        $vid,
            ;
        }
    }

    printf $fh "%f 8 add %f 8 add mv (%s) show ",
                $face->{ 'XC' },
                $face->{ 'YC' },
                $face->{ 'ID' },
    if ( $debug =~ /\bL\b/ );
}

##########################################
# Draw a face

sub drawFace {

    my $fh = shift;
    my $fid = shift;

    my $debug = $options{ 'D' } || '';

    # Check if a specific face was requested in the debug opts
    return if ( $debug =~ /\bf\d+\b/ ) && ( $debug !~ /\b$fid\b/ );

    my $face = $IMAGE->{ 'FACES' }->{ $fid };

    # Only draw a base face if specifically requested in the degug opts
    return if ( $debug !~ /\bf\d+\b/ ) && ( $face->{ 'TYPE' } eq FACE_TYPE_BASE );

    # Hide the tubes if only drawing edges
    return if ( $debug =~ /\bE\b/ ) && ( $face->{ 'TYPE' } eq FACE_TYPE_EDGE_TUBE );
    return if ( $debug =~ /\bE\b/ ) && ( $face->{ 'TYPE' } eq FACE_TYPE_FACE_TUBE );

    # Hide the edges unless requested
    return if ( $debug !~ /\bE\b/ ) && ( $face->{ 'TYPE' } eq FACE_TYPE_EDGE );
    return if ( $debug !~ /\bE\b/ ) && ( $face->{ 'TYPE' } eq FACE_TYPE_FACE );

    # Only draw tube faces that face the viewer
    return if (
        ( $face->{ 'NORMAL' }->[ 2 ] < 0 ) &&
        (
            ( $face->{ 'TYPE' } eq FACE_TYPE_EDGE_TUBE ) ||
            ( $face->{ 'TYPE' } eq FACE_TYPE_FACE_TUBE ) 
        )
    );

    printf $fh "%% face: %s (%s)\n", $fid, $face->{ 'TYPE' };

    for ( my $v = 0; $v < scalar @{ $face->{ 'VERTICES' } }; $v++ ) {

        my $vid =  $face->{ 'VERTICES' }->[ $v ];
        my $vertex =  $IMAGE->{ 'VERTICES' }->{ $vid };

        printf $fh "%f %f %s ",
                    $vertex->{ 'X' },
                    $vertex->{ 'Y' },
                    $v ? 'ln' : 'mv',
        ;
    }

    printf $fh "closepath %f %f %f setrgbcolor stroke\n", getFaceColor( $face );

    drawLabels( $fh, $face );
    drawFaceNormals( $fh, $face );
}

##########################################
# Draw the image to the dest file

sub drawImage {

    open my $fh, '>', PS_TEMP or die "Could not open PS_TEMP: $!\n";

    printf $fh "%%!PS\n%%%%BoundingBox: 0 0 1000 1000\n%%%%EndComments\n

        /imgw %f def
        /imgd %f def

        <<
            /PageSize [ imgw imgd ]
            /ImagingBBox null
        >> setpagedevice

        /mv { moveto } bind def
        /ln { lineto } bind def

        /Arial-Bold findfont 16 scalefont setfont

        0 0 mv
        imgw 0 ln
        imgw imgd ln
        0 imgd ln
        0 setgray fill

        imgw 2 div imgd 2 div translate

        1 setlinecap
        1 1 0 setrgbcolor
    ",
        $IMAGE->{ 'WIDTH' }, $IMAGE->{ 'DEPTH' },
    ;

    map { drawFace( $fh, $_ ) } ( keys %{ $IMAGE->{ 'FACES' } } );

    close $fh;

    print "Converting\n";
    system( sprintf "convert '%s' '%s'", PS_TEMP, $IMAGE->{ 'DEST' } );
    print "Done\n";

    $options{ 'k' } ?
        printf "Retained %s\n",  PS_TEMP
        :
        unlink PS_TEMP
    ;
}

############################################################
# Help doco

sub help {

    print "\n@_\n" if @_;

    printf "\n$0 " .
        " [-d] [-h] [-k] [-m <val>] [-n <val>] [-s <side>]" .
        " [-t <tube>] [-v] [-x <ang>] [-y <ang>] [-z <ang>] [-D <opts>] [-Y]

        where
            -d          dump the image face data
            -f          alternative destination for the yml file
            -h          this help doco
            -k          retain the temp postscript file (%s)
            -m <val>    reduction factor aplpied to base faces (default %f)
            -n <val>    factor for edge faces to be moved away from base edges (default %f)
            -s <side>   length of the base dodecaehron sides (default %s)
            -t <tube>   size of the pentagonal tubes  (default %f)
            -v          dump the image vertex data
            -x <ang>    x-axis image rotation (default 0)
            -y <ang>    y-axis image rotation (default 0)
            -z <ang>    z-axis image rotation (default 0)

            -D <opts>   string of debug options (see below)
            -Y          write image ray tracing yaml (default %s)

        debug options
            D           setup with a single debug base face in the Z plane
            f{id}       draw only the face
            E           only draw the face edges (no tubes)
            L           label the faces
            N           draw the face and edge normals
            V           label the vertices
    \n",
    PS_TEMP,
    DEFAULT_BASE_MULTIPLIER,
    DEFAULT_EDGE_MULTIPLIER,
    DEFAULT_SIDE,
    DEFAULT_TUBE,
    DEFAULT_YML_FILE,
    ;

    exit;
}

__END__

http://xahlee.info/math/algorithmic_math_art.html

./dodecahedron.pl -m 0.7 -n 2.3 -t 15 -x 9 -y 13 -z 23 -f ../../../rays/configs/dodecahedron.yml

Optional plane:

    object:
        type: plane
        origin:
            i: -650
            j: -650
            k: -370
        axis1:
            i: 1300
            j: 0
            k: 0
        axis2:
            i: 0
            j: 1300
            k: 0
        surface: default
        color-name: grey8
        scheme-name: plane

__DATA__
# Config header for the ray environment

defaults:
    background:
        red: 0.0
        green: 0.0
        blue: 0.0
    properties:
        anti-alias: on
        diffusion_index: 1.0
        specular_index: 0.3
        specular_exponent: 7.0
        color_scramble: 0.00

screen:
    centre:
        i: 4500
        j: 1000
        k: 800
    offset:
        i: 0
        j: 0
        k: 0
    eye_dist: 500
    height: 100
    width: 100

image:
    height: 2000
    width: 2000
    units: PIX
    resolution: 1

illumination:
    ambience: 0.3
    specular_exponent: 9.0
    color_shift: 0.0
    lamp:
        brightness: 80
        centre:
            i: 0
            j: 1000
            k: 1000
        radius: 1
    lamp:
        brightness: 50
        centre:
            i: 1000
            j: 0
            k: 1000
        radius: 1

factor-scheme-defs:
    name: face
        diffusion_index: 0.95
        specular_index: 0.4
        specular_exponent: 5.0
        reflective_index: 0.15
    name: plane
        diffusion_index: 0.9
        specular_index: 0.4
        specular_exponent: 5.0
        reflective_index: 0.2

objects:
