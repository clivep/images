#!/usr/bin/perl

use warnings;
use strict;

use Math::Trig;
use File::Path qw(make_path);
use File::Temp qw/ tempfile tempdir /;
use Getopt::Std;

use Data::Dumper;

############################################################
#
#   9fold.pl
#
#   A substitution tiling application
#
############################################################

use constant    TRUE                        =>  1;
use constant    FALSE                       =>  0;

use constant    IMAGE_WIDTH                 =>  1000;
use constant    IMAGE_DEPTH                 =>  1000;

use constant    POLYGON_TYPE_1              =>  'triangle';
use constant    POLYGON_TYPE_2              =>  'extended';
use constant    POLYGON_TYPE_3              =>  'clipped';

use constant    SMALL_DIST                  =>  1.0e-10;

use constant    ROOT_2                      =>  ( sqrt( 2.0 ) );
use constant    ROOT_3                      =>  ( sqrt( 3.0 ) );

use constant    SCALE_DOWN                  => 2 * sin( pi / 18.0 );

############################################################
# Check the runline options

my %options;
getopts( 'a:c:d:D:f:F:hlo:pr:s:S:tW:x:y:', \%options );

help() if $options{ 'h' };

my $TEXT_MULTIPLIER = $options{ 'a' } || 1.0;
my $CENTER_POLYGON = $options{ 'c' };
my $DRAWING_SCALE = 50 * ( $options{ 's' } || 1 );
my $DEFLATIONS = $options{ 'd' } || 1;
my $LABEL_POLYGONS = $options{ 'l' };
my $FILL_POLYGON = $options{ 'f' };
my $IMAGE_ROTATE = $options{ 'o' } || 0;
my $DUMP_DATA = $options{ 'p' };
my $DISPLAY_RADIUS = $options{ 'r' };
my $LEAVE_PS_TEMP = $options{ 't' };
my $X_OFFSET = $options{ 'x' } || 0;
my $Y_OFFSET = $options{ 'y' } || 0;

my $IMAGE_DEPTH = $options{ 'D' } || IMAGE_DEPTH;
my $IMAGE_WIDTH = $options{ 'W' } || IMAGE_WIDTH;
my $OUTPUT_FILE = $options{ 'F' } || 'tiling.pdf';
my $DRAWING_SCHEME = $options{ 'S' } || 'line';

help( "'$DRAWING_SCHEME' is not a supported drawing scheme" ) if ( 
    ( $DRAWING_SCHEME ne 'line' ) &&
    ( $DRAWING_SCHEME ne 'fill' )
);

############################################################
# Setup the environment

$| = 1;

my $IMAGE_SIZE = [ $IMAGE_WIDTH, $IMAGE_DEPTH ];
my $DRAWING_OFFSET = [ 0, 0 ];

my $POLYGON_LOOKUP = {};

my $POLYGONS = [];
my $NEXT_POLYGON_ID = 1;

setupInitalPolygons();

############################################################
# Process the tiling

my $CURRENT_DEFLATION = 0;

while ( $CURRENT_DEFLATION < $DEFLATIONS ) {

    my $new_polygons = [];

    for ( @{ $POLYGONS } ) {

          isType1( $_ )  ? subdivideType1( $_, $new_polygons )
        : isType2( $_ )  ? subdivideType2( $_, $new_polygons )
        : isType3( $_ )  ? subdivideType3( $_, $new_polygons )
        :                  push @{ $new_polygons }, $_
          ;

#        push @{ $new_polygons }, $_;
    }

    $POLYGONS = $new_polygons;
    
    $CURRENT_DEFLATION++;
}

for my $polygon ( @{ $POLYGONS } ) {

    $POLYGON_LOOKUP->{ $polygon->{ 'ID'} } = $polygon;
}

drawImage();

dumpPolygons() if $DUMP_DATA;

exit;

############################################################
# Simple utility functions

sub isType1 { return shift->{ 'TYPE' } eq POLYGON_TYPE_1 }
sub isType2 { return shift->{ 'TYPE' } eq POLYGON_TYPE_2 }
sub isType3 { return shift->{ 'TYPE' } eq POLYGON_TYPE_3 }

############################################################
# Subdivide a Type1 polygon

sub subdivideType1 {

    my $polygon = shift;
    my $new_polygons = shift;

    my $vertices = $polygon->{ 'VERTICES' };

    my $new_base = distance( $vertices->[ 1 ], $vertices->[ 2 ] ) * SCALE_DOWN;

    my $top_type1_polygon = {
            'ID'        =>  $NEXT_POLYGON_ID++,
            'TYPE'      =>  POLYGON_TYPE_1,
            'VERTICES'  =>  [
                $vertices->[ 0 ],
                pointRatioAlong( $vertices->[ 0 ], $vertices->[ 1 ], SCALE_DOWN ),
                pointRatioAlong( $vertices->[ 0 ], $vertices->[ 2 ], SCALE_DOWN ),
            ],
    };
    push @{ $new_polygons }, $top_type1_polygon;

    my $only_type3_polygon = {
            'ID'        =>  $NEXT_POLYGON_ID++,
            'TYPE'      =>  POLYGON_TYPE_3,
            'VERTICES'  =>  [
                pointRatioAlong( $vertices->[ 2 ], $vertices->[ 0 ], SCALE_DOWN ),
                $top_type1_polygon->{ 'VERTICES' }->[ 2 ],
                $top_type1_polygon->{ 'VERTICES' }->[ 1 ],
            ],
    };
    push @{ $new_polygons }, $only_type3_polygon;

    my $bottom_type1_polygon = {
            'ID'        =>  $NEXT_POLYGON_ID++,
            'TYPE'      =>  POLYGON_TYPE_1,
            'VERTICES'  =>  [
                $vertices->[ 2 ],
                pointDistAlong( $vertices->[ 1 ], $vertices->[ 0 ], $new_base ),
                $vertices->[ 1 ],
            ],
    };
    push @{ $new_polygons }, $bottom_type1_polygon;
    
    my $val = $new_base * sin( 5.0 * pi / 9.0 ) / sin( 3.0 * pi / 9.0 );

    my $vtx_1 = pointDistAlong( $vertices->[ 1 ], $vertices->[ 0 ], $new_base + $val );
    my $vtx_2 = pointDistAlong( $only_type3_polygon->{ 'VERTICES' }->[ 0 ], $vtx_1, $new_base );

    my $top_type2_polygon = {
            'ID'        =>  $NEXT_POLYGON_ID++,
            'TYPE'      =>  POLYGON_TYPE_2,
            'VERTICES'  =>  [
                $top_type1_polygon->{ 'VERTICES' }->[ 1 ],
                $vtx_2,
                $vtx_1,
            ],
    };
    push @{ $new_polygons }, $top_type2_polygon;
    
    my $centre_type1_polygon = {
            'ID'        =>  $NEXT_POLYGON_ID++,
            'TYPE'      =>  POLYGON_TYPE_1,
            'VERTICES'  =>  [
                $top_type1_polygon->{ 'VERTICES' }->[ 1 ],
                $vtx_2,
                $only_type3_polygon->{ 'VERTICES' }->[ 0 ],
            ],
    };
    push @{ $new_polygons }, $centre_type1_polygon;
    
    my $lwrleft_type1_polygon = {
            'ID'        =>  $NEXT_POLYGON_ID++,
            'TYPE'      =>  POLYGON_TYPE_1,
            'VERTICES'  =>  [
                $vertices->[ 2 ],
                $vtx_2,
                $only_type3_polygon->{ 'VERTICES' }->[ 0 ],
            ],
    };
    push @{ $new_polygons }, $lwrleft_type1_polygon;
    
    my $centre_type2_polygon = {
            'ID'        =>  $NEXT_POLYGON_ID++,
            'TYPE'      =>  POLYGON_TYPE_2,
            'VERTICES'  =>  [
                $vertices->[ 2 ],
                $vtx_2,
                $vtx_1,
            ],
    };
    push @{ $new_polygons }, $centre_type2_polygon;
    
    my $bottom_type2_polygon = {
            'ID'        =>  $NEXT_POLYGON_ID++,
            'TYPE'      =>  POLYGON_TYPE_2,
            'VERTICES'  =>  [
                $vertices->[ 2 ],
                $bottom_type1_polygon->{ 'VERTICES' }->[ 1 ],
                $vtx_1,
            ],
    };
    push @{ $new_polygons }, $bottom_type2_polygon;
    
    return;
}

############################################################
# Subdivide a Type2 polygon

sub subdivideType2 {

    my $polygon = shift;
    my $new_polygons = shift;

    my $vertices = $polygon->{ 'VERTICES' };

    my $new_type1_side = distance( $vertices->[ 0 ], $vertices->[ 1 ] ) * SCALE_DOWN;
    my $new_base = $new_type1_side * SCALE_DOWN;

    my $top_type1_polygon = {
            'ID'        =>  $NEXT_POLYGON_ID++,
            'TYPE'      =>  POLYGON_TYPE_1,
            'VERTICES'  =>  [
                $vertices->[ 0 ],
                pointDistAlong( $vertices->[ 0 ], $vertices->[ 2 ], $new_type1_side ),
                pointDistAlong( $vertices->[ 0 ], $vertices->[ 1 ], $new_type1_side ),
            ],
    };
    push @{ $new_polygons }, $top_type1_polygon;

    my $val = $new_base * sin( 5.0 * pi / 9.0 ) / sin( 3.0 * pi / 9.0 );

    my $only_type3_polygon = {
            'ID'        =>  $NEXT_POLYGON_ID++,
            'TYPE'      =>  POLYGON_TYPE_3,
            'VERTICES'  =>  [
                pointDistAlong( $vertices->[ 2 ], $vertices->[ 0 ], $new_type1_side + $val ),
                $top_type1_polygon->{ 'VERTICES' }->[ 1 ],
                $top_type1_polygon->{ 'VERTICES' }->[ 2 ],
            ],
    };
    push @{ $new_polygons }, $only_type3_polygon;

    my $vtx_1 = pointDistAlong( $vertices->[ 1 ], $vertices->[ 0 ], $new_base + $val );
    my $vtx_2 = pointDistAlong( $only_type3_polygon->{ 'VERTICES' }->[ 0 ], $vtx_1, $new_base );

    my $bottom_type2_polygon = {
            'ID'        =>  $NEXT_POLYGON_ID++,
            'TYPE'      =>  POLYGON_TYPE_2,
            'VERTICES'  =>  [
                $vertices->[ 1 ],
                pointDistAlong( $vertices->[ 2 ], $vertices->[ 0 ], $val ),
                $vertices->[ 2 ],
            ],
    };
    push @{ $new_polygons }, $bottom_type2_polygon;

    my $bottom_type1_polygon = {
            'ID'        =>  $NEXT_POLYGON_ID++,
            'TYPE'      =>  POLYGON_TYPE_1,
            'VERTICES'  =>  [
                pointDistAlong( $vertices->[ 2 ], $vertices->[ 0 ], $val ),
                $vertices->[ 1 ],
                pointDistAlong( $vertices->[ 1 ], $vertices->[ 0 ], $new_base ),
            ],
    };
    push @{ $new_polygons }, $bottom_type1_polygon;

    my $top_type2_polygon = {
            'ID'        =>  $NEXT_POLYGON_ID++,
            'TYPE'      =>  POLYGON_TYPE_2,
            'VERTICES'  =>  [
                $top_type1_polygon->{ 'VERTICES' }->[ 2 ],
                $vtx_2,
                $vtx_1,
            ],
    };
    push @{ $new_polygons }, $top_type2_polygon;

    my $centre_type1_polygon = {
            'ID'        =>  $NEXT_POLYGON_ID++,
            'TYPE'      =>  POLYGON_TYPE_1,
            'VERTICES'  =>  [
                $top_type1_polygon->{ 'VERTICES' }->[ 2 ],
                $only_type3_polygon->{ 'VERTICES' }->[ 0 ],
                $vtx_2,
            ],
    };
    push @{ $new_polygons }, $centre_type1_polygon;

    my $lwrright_type1_polygon = {
            'ID'        =>  $NEXT_POLYGON_ID++,
            'TYPE'      =>  POLYGON_TYPE_1,
            'VERTICES'  =>  [
                $bottom_type1_polygon->{ 'VERTICES' }->[ 0 ],
                $vtx_2,
                $centre_type1_polygon->{ 'VERTICES' }->[ 1 ],
            ],
    };
    push @{ $new_polygons }, $lwrright_type1_polygon;
 
    my $centre_type2_polygon = {
            'ID'        =>  $NEXT_POLYGON_ID++,
            'TYPE'      =>  POLYGON_TYPE_2,
            'VERTICES'  =>  [
                $lwrright_type1_polygon->{ 'VERTICES' }->[ 0 ],
                $lwrright_type1_polygon->{ 'VERTICES' }->[ 1 ],
                $vtx_1,
            ],
    };
    push @{ $new_polygons }, $centre_type2_polygon;

    my $lower_type2_polygon = {
            'ID'        =>  $NEXT_POLYGON_ID++,
            'TYPE'      =>  POLYGON_TYPE_2,
            'VERTICES'  =>  [
                $lwrright_type1_polygon->{ 'VERTICES' }->[ 0 ],
                $bottom_type1_polygon->{ 'VERTICES' }->[ 2 ],
                $vtx_1,
            ],
    };
    push @{ $new_polygons }, $lower_type2_polygon;
    
    return;
}

############################################################
# Subdivide a Type3 polygon

sub subdivideType3 {

    my $polygon = shift;
    my $new_polygons = shift;

    my $vertices = $polygon->{ 'VERTICES' };

    my $new_type1_side = distance( $vertices->[ 0 ], $vertices->[ 2 ] ) * SCALE_DOWN;
    my $new_base = $new_type1_side * SCALE_DOWN;

    my $top_type1_polygon = {
            'ID'        =>  $NEXT_POLYGON_ID++,
            'TYPE'      =>  POLYGON_TYPE_1,
            'VERTICES'  =>  [
                $vertices->[ 0 ],
                pointDistAlong( $vertices->[ 0 ], $vertices->[ 1 ], $new_type1_side ),
                pointDistAlong( $vertices->[ 0 ], $vertices->[ 2 ], $new_type1_side ),
            ],
    };
    push @{ $new_polygons }, $top_type1_polygon;

    my $only_type3_polygon = {
            'ID'        =>  $NEXT_POLYGON_ID++,
            'TYPE'      =>  POLYGON_TYPE_3,
            'VERTICES'  =>  [
                pointRatioAlong( $vertices->[ 2 ], $vertices->[ 0 ], SCALE_DOWN ),
                $top_type1_polygon->{ 'VERTICES' }->[ 2 ],
                $top_type1_polygon->{ 'VERTICES' }->[ 1 ],
            ],
    };
    push @{ $new_polygons }, $only_type3_polygon;

    my $val = $new_base * sin( 5.0 * pi / 9.0 ) / sin( 3.0 * pi / 9.0 );

    my $vtx_1 = pointDistAlong( $vertices->[ 1 ], $vertices->[ 0 ], $val );
    my $vtx_2 = pointDistAlong( $only_type3_polygon->{ 'VERTICES' }->[ 0 ], $vtx_1, $new_base );

    my $top_type2_polygon = {
            'ID'        =>  $NEXT_POLYGON_ID++,
            'TYPE'      =>  POLYGON_TYPE_2,
            'VERTICES'  =>  [
                $top_type1_polygon->{ 'VERTICES' }->[ 1 ],
                $vtx_1,
                $vtx_2,
            ],
    };
    push @{ $new_polygons }, $top_type2_polygon;
    
    my $centre_type1_polygon = {
            'ID'        =>  $NEXT_POLYGON_ID++,
            'TYPE'      =>  POLYGON_TYPE_1,
            'VERTICES'  =>  [
                $top_type1_polygon->{ 'VERTICES' }->[ 1 ],
                $vtx_2,
                $only_type3_polygon->{ 'VERTICES' }->[ 0 ],
            ],
    };
    push @{ $new_polygons }, $centre_type1_polygon;
    
    my $lwrleft_type1_polygon = {
            'ID'        =>  $NEXT_POLYGON_ID++,
            'TYPE'      =>  POLYGON_TYPE_1,
            'VERTICES'  =>  [
                $vertices->[ 2 ],
                $only_type3_polygon->{ 'VERTICES' }->[ 0 ],
                $vtx_2,
            ],
    };
    push @{ $new_polygons }, $lwrleft_type1_polygon;
    
    my $centre_type2_polygon = {
            'ID'        =>  $NEXT_POLYGON_ID++,
            'TYPE'      =>  POLYGON_TYPE_2,
            'VERTICES'  =>  [
                $vertices->[ 2 ],
                $vtx_2,
                $vtx_1,
            ],
    };
    push @{ $new_polygons }, $centre_type2_polygon;
    
    my $bottom_type2_polygon = {
            'ID'        =>  $NEXT_POLYGON_ID++,
            'TYPE'      =>  POLYGON_TYPE_2,
            'VERTICES'  =>  [
                $vertices->[ 2 ],
                $vtx_1,
                $vertices->[ 1 ],
            ],
    };
    push @{ $new_polygons }, $bottom_type2_polygon;
    
    return;
}

############################################################
# Determine if the polygon vertices are clockwise

sub isClockwisePolygon {

    my $polygon = shift;

    my $vertices = $polygon->{ 'VERTICES' };

    my $midpt = pointAlong( $vertices->[ 0 ], $vertices->[ int( (scalar @{$vertices}) / 2 ) ], 0.5 );

    my $test_point1 = pointPerpendicularTo( $vertices->[ 0 ], $vertices->[ 1 ], 5 );
    my $test_point2 = pointPerpendicularTo( $vertices->[ 0 ], $vertices->[ 1 ], -5 );

    return distance( $test_point1, $midpt ) > distance( $test_point2, $midpt );
}

############################################################
# Reverse the vertex collection of the given polygon

sub reversePolygonVertices {

    my $polygon = shift;

    my $vertices = $polygon->{ 'VERTICES' };

    my $new_vertices = [];
    push @{ $new_vertices}, $vertices->[ 0 ];

    for ( my $v = scalar( @{ $vertices } ) - 1; $v > 0; $v-- ) {
        push @{ $new_vertices}, $vertices->[ $v ];
    }

    $polygon->{ 'VERTICES' } = $new_vertices;
}

############################################################
# Roll the vertex collection of the given polygon

sub rollPolygonVertices {

    my $polygon = shift;
    my $roll = shift;

    my $vertices = $polygon->{ 'VERTICES' };

    my $new_vertices = [];
    for ( my $v = 0; $v < scalar( @{ $vertices } ); $v++ ) {

        push @{ $new_vertices}, $vertices->[ ( $v + $roll ) % scalar( @{ $vertices } ) ];
    }

    $polygon->{ 'VERTICES' } = $new_vertices;
}

############################################################
# Return the number of polygons that share the vertx

sub vertexPolygons {

    my $polygon = shift;
    my $vertex_no = shift;

    return keys %{ $polygon->{ 'VERTICES' }->[ $vertex_no ]->{ 'POLYGONS' } };
}

############################################################
# Find common polygon at a vertex - with exclusions

sub findCommonVertexPolygon {

    my $polygon = shift;
    my $vertex_no = shift;

    my $exclusions = { $polygon->{ 'ID' } => 1 };

    while ( my $exclusion = shift ) {
        $exclusions->{ $exclusion } = 1;
    }

    for ( keys %{ $polygon->{ 'VERTICES' }->[ $vertex_no ]->{ 'POLYGONS' } } ) {

        return $_ unless ( $exclusions->{ $_ } );
    }

    return -1;
}

############################################################
# Find the distance between 2 vertices

sub distance {

    my $vtx1 = shift;
    my $vtx2 = shift;

    my $dx = $vtx1->{ 'X' } - $vtx2->{ 'X' };
    my $dy = $vtx1->{ 'Y' } - $vtx2->{ 'Y' };

    return sqrt( ( $dx * $dx ) +  ( $dy * $dy ) );
}

############################################################
# Find a point on the right perpendicular v1->v2 at dist val

sub pointPerpendicularTo {

    my $vertex1 = shift;
    my $vertex2 = shift;
    my $dist = shift;

    my $val = $dist / distance( $vertex1, $vertex2 );

    return newVertex(
        $vertex2->{ 'X' } - $val * ( $vertex2->{ 'Y' } - $vertex1->{ 'Y' } ),
        $vertex2->{ 'Y' } + $val * ( $vertex2->{ 'X' } - $vertex1->{ 'X' } ),
    );
}

############################################################
# Find a point the given proportion between 2 points

sub pointRatioAlong {

    my $vertex1 = shift;
    my $vertex2 = shift;
    my $val = shift;

    return newVertex(
            ( ( 1.0 - $val ) * $vertex1->{ 'X' } ) + ( $val * $vertex2->{ 'X' } ),
            ( ( 1.0 - $val ) * $vertex1->{ 'Y' } ) + ( $val * $vertex2->{ 'Y' } ),
    );
}

############################################################
# Find a point the given distance between 2 points

sub pointDistAlong {

    my $vertex1 = shift;
    my $vertex2 = shift;
    my $val = shift;

    $val = $val / distance( $vertex1, $vertex2 );

    return newVertex(
            ( ( 1.0 - $val ) * $vertex1->{ 'X' } ) + ( $val * $vertex2->{ 'X' } ),
            ( ( 1.0 - $val ) * $vertex1->{ 'Y' } ) + ( $val * $vertex2->{ 'Y' } ),
    );
}

############################################################
# Add the polygons' id to each vertex polygon list

sub addVertexPolygons {

    my $polygon = shift;

    for my $vertex ( @{ $polygon->{ 'VERTICES' } } ) {

        $vertex->{ 'POLYGONS' }->{ $polygon->{ 'ID' } } = 1;
    }

    return;
}

############################################################
# Remove the id from each vertex polygon list

sub delVertexPolygons {

    my $polygon = shift;
    my $id = shift;

    for my $vertex ( @{ $polygon->{ 'VERTICES' } } ) {

        delete $vertex->{ 'POLYGONS' }->{ $id };
    }

    return;
}

############################################################
# Find the vertex number in the given polygon

sub findVertexNumber {

    my $polygon = shift;
    my $vertex = shift;

    for ( my $v = 0; $v < scalar @{ $polygon->{ 'VERTICES' } }; $v++ ) {

        return $v if ( $vertex == $polygon->{ 'VERTICES' }->[ $v ] );
    }

    return -1;
}

############################################################
# Are the 2 polygon vertices shared?

sub checkSharedVertex {

    my $polygon1 = shift;
    my $vertex_no1 = shift;
    my $polygon2 = shift;
    my $vertex_no2 = shift;

    my $vertex1 = $polygon1->{ 'VERTICES' }->[ $vertex_no1 ];
    my $vertex2 = $polygon2->{ 'VERTICES' }->[ $vertex_no2 ];

    my $dist = distance( $vertex1, $vertex2 );
    
    return 0 if ( $dist > SMALL_DIST );
    
    # Add the common polygons from vertex2 into vertex1
    for ( keys %{ $vertex2->{ 'POLYGONS' } } ) {

        $vertex1->{ 'POLYGONS' }->{ $_ } = 1;

        my $polygon = $POLYGON_LOOKUP->{ $_ };
        my $v = findVertexNumber( $polygon, $vertex2 );
        $polygon->{ 'VERTICES' }->[ $v ] = $vertex1;
    }

    $polygon2->{ 'VERTICES' }->[ $vertex_no2 ] = $vertex1;

    return 1;
}

############################################################
# Offset a vertex by the line between 1 others

sub offsetVertex {

    my $polygon = shift;
    my $vertex_no = shift;
    my $offset_polygon = shift;
    my $vertex1 = shift;
    my $vertex2 = shift;

    $polygon->{ 'VERTICES' }->[ $vertex_no ]->{ 'X' } +=
            $offset_polygon->{ 'VERTICES' }->[ $vertex1 ]->{ 'X' } -
            $offset_polygon->{ 'VERTICES' }->[ $vertex2 ]->{ 'X' };

    $polygon->{ 'VERTICES' }->[ $vertex_no ]->{ 'Y' } +=
            $offset_polygon->{ 'VERTICES' }->[ $vertex1 ]->{ 'Y' } -
            $offset_polygon->{ 'VERTICES' }->[ $vertex2 ]->{ 'Y' };

    return;
}

############################################################
# Return the closer of the polgons to the point

sub closerPolygon {

    my $point = shift;
    my $polygon1 = shift;
    my $polygon2 = shift;

    my $dist1 = distance( findPolygonCentre( $polygon1 ), $point );
    my $dist2 = distance( findPolygonCentre( $polygon2 ), $point );

    return ( $dist1 < $dist2 ) ? $polygon1 : $polygon2;
}

############################################################
# Return the number of the vertex closest to the given point

sub closerVertex {

    my $point = shift;
    my $polygon = shift;
    my $vertex1 = shift;
    my $vertex2 = shift;

    my $dist1 = distance( $point, $polygon->{ 'VERTICES' }->[ $vertex1 ] );
    my $dist2 = distance( $point, $polygon->{ 'VERTICES' }->[ $vertex2 ] );

    return ($ dist1 < $dist2 ) ? $vertex1 : $vertex2;
}

############################################################
# Setup the inital polygon

sub setupInitalPolygons {

    my $ang = pi / 9.0;

#    for ( my $a = -1; $a < 1; $a++ ) {
#   for ( my $a = 0; $a < 18; $a++ ) {
for ( my $a = 13; $a < 14; $a++ ) {

        my $initial_polygon = {
            'ID'        =>  $NEXT_POLYGON_ID++,
            'TYPE'      =>  POLYGON_TYPE_1,
            'VERTICES'  =>  [
                newVertex( 0, 0 ),
                newVertex( cos( ( $a + 1 ) * $ang ), sin( ( $a + 1 ) * $ang ) ),           
                newVertex( cos( $a * $ang ), sin( $a * $ang  ) ),           
            ],
        };
    
        if ( ( $a % 2 ) == 0 ) {
            my $tmp = $initial_polygon->{ 'VERTICES' }->[ 1 ];
            $initial_polygon->{ 'VERTICES' }->[ 1 ] = $initial_polygon->{ 'VERTICES' }->[ 2 ];
            $initial_polygon->{ 'VERTICES' }->[ 2 ] = $tmp;
        }

        push @{ $POLYGONS }, $initial_polygon;
        $POLYGON_LOOKUP->{ $initial_polygon->{ 'ID' } } = $initial_polygon;
    }

    return;
}

############################################################
# Return a new vertex object

sub newVertex {

   return {
        'X'         =>  shift,
        'Y'         =>  shift,
   };
}

############################################################
# Return the polygons' centre point

sub findPolygonCentre {

    my $polygon = shift;

    my $vertices = $polygon->{ 'VERTICES' };
    my $cx = 0;
    my $cy = 0;

    for ( @{ $vertices } ) {
        $cx += $_->{ 'X' };
        $cy += $_->{ 'Y' };
    }

    return newVertex(
        $cx / scalar( @{ $vertices } ),
        $cy / scalar( @{ $vertices } ),
    );
}

############################################################
# Label a polygon

sub labelPolygon {

   my $fh = shift;
   my $polygon = shift;
   my $label_vertices = shift;

   my $centre = findPolygonCentre( $polygon );

    printf $fh "gsave 
        %f %f translate
        (%s) dup stringwidth pop -2 div -3 moveto
        1 setgray
        show
        grestore\n",
                $DRAWING_SCALE * $centre->{ 'X' },
                $DRAWING_SCALE * $centre->{ 'Y' },
                $polygon->{ 'ID' },
    ; 

    return unless $label_vertices;

    my $vertices = $polygon->{ 'VERTICES' };
    for ( my $v = 0; $v < scalar @{ $vertices }; $v++ ) {

        my $font_sz = 10 / ( $DEFLATIONS ** 0.3 );

        printf $fh "gsave 
            /Arial-Bold findfont %f scalefont setfont 
            %f %f translate
            (%s) dup stringwidth pop -2 div %f moveto
            .8 setgray
            show
            grestore\n",
                $font_sz * $TEXT_MULTIPLIER,
                $DRAWING_SCALE * ( $centre->{ 'X' } + 3 * $vertices->[ $v ]->{ 'X' } ) / 4.0,
                $DRAWING_SCALE * ( $centre->{ 'Y' } + 3 * $vertices->[ $v ]->{ 'Y' } ) / 4.0,
                $v,
                $font_sz / -3,
    } 
}

############################################################
# Check drawing raduis

sub insideDrawingRadius {

    my $polygon = shift;

    return TRUE unless $DISPLAY_RADIUS;

    return ( $DRAWING_SCALE * distance( findPolygonCentre( $polygon ),
                                    { 'X' => 0, 'Y' => 0 } ) < $DISPLAY_RADIUS );
}

############################################################
# Find the color to fill the given polygon

sub findPolygonFillColor {

    my $polygon = shift;
    my $fill = shift;

    my $color = '';

    if ( $DRAWING_SCHEME eq 'fill' ) {

        $color = 
            isType1( $polygon ) ? "1 1 0 setrgbcolor" :
            isType2( $polygon ) ? "0 1 1 setrgbcolor" :
            isType3( $polygon ) ? "0 1 0 setrgbcolor" :
            ''
        ;
    }
    elsif ( $fill ) {

        $color = ".3 setgray";
    }

    return $color ? " gsave $color fill grestore\n" : "";
}


############################################################
# Draw a polygon

sub drawPolygon {

    my $fh = shift;
    my $polygon = shift;
    my $fill = shift;

    return unless insideDrawingRadius( $polygon );

    my $vertices = $polygon->{ 'VERTICES' };

    for ( my $v = 0; $v < scalar @{ $vertices }; $v++ ) {

        printf $fh "%f %f %s ",
                    $DRAWING_SCALE * $vertices->[ $v ]->{ 'X' },
                    $DRAWING_SCALE * $vertices->[ $v ]->{ 'Y' },
                    ( $v == 0 ) ? 'mv' : 'ln'
        ;
    }

    printf $fh findPolygonFillColor( $polygon, $fill );
    printf $fh "closepath stroke\n";

    labelPolygon( $fh, $polygon, TRUE ) if ( $LABEL_POLYGONS );

    return;
}

############################################################
# Set the default line color

sub defaultLineColor {
    
    return ( $DRAWING_SCHEME eq 'fill' ) ? '0 setgray' : '1 1 0 setrgbcolor';
}

############################################################
# Draw the polygons

sub drawPolygons {

    my $fh = shift;

    for my $polygon ( @{$POLYGONS} ) {

        my $fill = $FILL_POLYGON && ( $polygon->{ 'ID' } eq $FILL_POLYGON );
        drawPolygon( $fh, $polygon, $fill ) 
    }

    return;
}

############################################################
# Write the image header

sub writeImageHeader {

    my $fh = shift;

    $DRAWING_OFFSET = [
                $IMAGE_SIZE->[ 0 ] / 2.0 - $X_OFFSET,
                $IMAGE_SIZE->[ 1 ] / 2.0 - $Y_OFFSET,
    ];

    if ( $CENTER_POLYGON ) {

        if ( my $polygon = $POLYGON_LOOKUP->{ $CENTER_POLYGON } ) {

            my $center = findPolygonCentre( $polygon );
            printf "offset: %f %f\n", 
                            $DRAWING_SCALE * $center->{ 'X' },
                            $DRAWING_SCALE * $center->{ 'Y' },
            ;

            $DRAWING_OFFSET = [
                $IMAGE_SIZE->[ 0 ] / 2.0 - $DRAWING_SCALE * $center->{ 'X' }, 
                $IMAGE_SIZE->[ 0 ] / 2.0 - $DRAWING_SCALE * $center->{ 'Y' },
            ]; 
        }
        else {

            printf "Could not find polygon %s\n", $CENTER_POLYGON;
            exit -1;
        }
    }

    my $font_sz = 12 / ( $DEFLATIONS ** 0.3 );

    printf $fh "%%!PS\n%%BoundingBox: 0 0 %f %f\n%%EndComments

        <<
            /PageSize [ %f %f ]
            /ImagingBBox null
        >> setpagedevice

        /mv { moveto } bind def
        /ln { lineto } bind def

        0 0 mv %f 0 ln %f %f ln 0 %f ln fill

        /Arial findfont %f scalefont setfont 

        %f %f translate
        %f rotate

        1 setlinejoin
        %f setlinewidth
        %s  %% default line color
    ",
        $IMAGE_WIDTH, $IMAGE_DEPTH,
        $IMAGE_SIZE->[ 0 ], $IMAGE_SIZE->[ 1 ],
        $IMAGE_WIDTH, $IMAGE_WIDTH,
        $IMAGE_DEPTH, $IMAGE_DEPTH,
        $font_sz * $TEXT_MULTIPLIER,
        $DRAWING_OFFSET->[ 0 ],
        $DRAWING_OFFSET->[ 1 ],
        $IMAGE_ROTATE,
        1 / ( $DEFLATIONS ** 0.5 ),
        defaultLineColor(),
    ;

    return;
}

############################################################
# Find intersection of lines (vtx1->vtx2) and (vtx3->vtx4)

sub findIntersection {

    my $vtx1 = shift;
    my $vtx2 = shift;
    my $vtx3 = shift;
    my $vtx4 = shift;

    my $val1 = ( $vtx1->{ 'X' } * $vtx2->{ 'Y' } - $vtx1->{ 'Y' } * $vtx2->{ 'X' } );
    my $val2 = ( $vtx3->{ 'X' } * $vtx4->{ 'Y' } - $vtx3->{ 'Y' } * $vtx4->{ 'X' } );
    my $val3 = ( ( $vtx1->{ 'X' } - $vtx2->{ 'X' } )* ( $vtx3->{ 'Y' } - $vtx4->{ 'Y' } ) -
            ( $vtx3->{ 'X' } - $vtx4->{ 'X' } )* ( $vtx1->{ 'Y' } - $vtx2->{ 'Y' } ) );

    return newVertex (

        ( $val1 * ($vtx3->{ 'X' } - $vtx4->{ 'X' } ) - $val2 * ( $vtx1->{ 'X' } - $vtx2->{ 'X' } ) ) / $val3,
        ( $val1 * ($vtx3->{ 'Y' } - $vtx4->{ 'Y' } ) - $val2 * ( $vtx1->{ 'Y' } - $vtx2->{ 'Y' } ) ) / $val3,
    )
}

############################################################
# Draw the image

sub drawImage {

    my $drawing_type = shift;

    my ( $fh, $temp_file ) = tempfile();

    writeImageHeader( $fh );

    drawPolygons( $fh );

    close $fh;

    my $convert = "/usr/local/bin/convert";
    $convert = "/usr/bin/convert" unless ( -f $convert );
    $convert = "/Applications/ImageMagick/ImageMagick-6.6.5/bin/convert" unless ( -f $convert );

    unless ( -f $convert ) {

        printf "Could not locate convert at $convert\n";
    }
    else {

        unlink $OUTPUT_FILE;
        system( "$convert $temp_file $OUTPUT_FILE" );
    }


    if( $LEAVE_PS_TEMP ) {

        printf "Temp file at $temp_file\n" ;
    }
    else {

        unlink $temp_file 
    }

    return;
}

############################################################
# Help doco

sub help {

    print "\n@_\n" if @_;

    printf "\n$0 " .
                "[-a <sz>] [-c <id>] [-d <iters>] [-f <id>] [-h] [-l] " .
                "[-o <ang>] [-p] [-r <rad>] [-s <scale>] [-t] " .
                "[-x <xoff>] [-y <yoff>] " .
                "[-D <dep>] [-F <file>] [-S <scheme>] [-W <wid>] 
    
    where
        -a  text size multiplier (default 1)
        -c  center the image on the given polygon
        -d  deflations (default 1)
        -f  fill the given polygon
        -h  this help message
        -l  label the polygons
        -o  rotate the image
        -p  dump polygon data
        -r  drawing radius
        -s  drawing scale
        -t  leave the temp PS file
        -x  x offset for the image
        -y  y offset for the image

        -D  image depth
        -W  image width
        -F  output file
        -S  drawing scheme (see below)
        
    The following drawing schemes are supports:

        line    - outlined polygons (the default)
        fill    - simple polygon coloring
    \n";

    exit;
}

############################################################
# Return the polygon type decsription

sub getPolygonDescription {

    my $polygon = shift;

    return
            isSquare( $polygon )    ? 'Square'
          : isHexagon( $polygon )   ? 'Hexagon'
          : isRhombus( $polygon )     ? 'Rhomb'
          : '';
}

############################################################
# Dump polygon status

sub dumpPolygons {

    for my $polygon ( @{ $POLYGONS } ) {

        printf "P%s - %s (%d old) (parent P%s) consolidated\n",
                            $polygon->{ 'ID' },
                            getPolygonDescription( $polygon ),
                            $CURRENT_DEFLATION - $polygon->{ 'CREATED' },
                            $polygon->{ 'PARENT' },
                            $polygon->{ 'CONSOLIDATED' } ? '' : 'not ',
                            ;

        for ( @{ $polygon->{ 'VERTICES' } } ) {
            my $v = $_;
            printf "    %x [ ", $v;
            printf join ',', ( sort keys %{ $v->{ 'POLYGONS' } } );
            printf " ]\n";
        };
    }

    exit;
}

############################################################
# Null op

sub noop { return }

############################################################
# Report a ghastly error

sub fatal {

    printf "@_\n";
    exit -1;
}

__END__

