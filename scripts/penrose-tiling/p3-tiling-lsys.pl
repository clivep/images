#!/usr/bin/perl

##################################################
#   p3-tiling-lsys.pl
#
#       Run an L-system to generate the Penrose
#       P3 tiling
#
##################################################

use warnings;
use strict;

use Math::Trig;
use File::Temp qw/ tempfile tempdir /;
use Getopt::Std;

##################################################
#   L-system config

my $rules = {

    '6' =>  '81++91----71[-81----61]++',
    '7' =>  '+81--91[---61--71]+',
    '8' =>  '-61++71[+++81++91]-',
    '9' =>  '--81++++61[+91++++71]--71',
};

##################################################
#   Setup the environment

my %options;
getopts( 'hi:o:r:s:', \%options );

usage() if $options{ 'h' };

my $ang = 36;

my $iterations = $options{ 'i' } || 4;
my $output_file = $options{ 'o' } || 'clp.pdf';
my $recipe = $options{ 'r' } || '[7]++[7]++[7]++[7]++[7]';
my $side = $options{ 's' } || 20;

my $ctm = {
    'x'         =>  0,
    'y'         =>  0,
    'ang'       =>  0,
};

##################################################
#   Run the iterations

my ( $fh, $temp_file ) = tempfile();

writeHeader( $fh );

iterateRecipe( $recipe, $ctm, 0, $fh );

close $fh;

##################################################
#   Generate the image

my $convert = "/usr/local/bin/convert";
$convert = "/usr/bin/convert" unless ( -f $convert );
$convert = "/Applications/ImageMagick/ImageMagick-6.6.5/bin/convert" unless ( -f $convert );

unless ( -f $convert ) {

    printf "Could not locate convert at $convert\n";
}
else {

    unlink $output_file;
    system( "$convert $temp_file $output_file" );
}

unlink $temp_file;

exit;

##################################################
#   Substitute the recipe components or draw them

sub iterateRecipe {

    my $recipe = shift;
    my $ctm = shift;
    my $depth = shift;
    my $fh = shift;

    return if ( $depth > $iterations );

    my %stored_ctm;

    for ( my $c = 0; $c < length( $recipe ); $c++ ) {

        my $ch = substr $recipe, $c, 1;

          ( $ch eq '[' )    ?   %stored_ctm = %{ $ctm }
        : ( $ch eq ']' )    ?   restoreCtm( $ctm, \%stored_ctm )
        : ( $ch eq '+' )    ?   $ctm->{ 'ang' } += $ang
        : ( $ch eq '-' )    ?   $ctm->{ 'ang' } -= $ang
        : ( $ch eq '6' )    ?   iterateRecipe( $rules->{ '6' }, $ctm, $depth+1, $fh )
        : ( $ch eq '7' )    ?   iterateRecipe( $rules->{ '7' }, $ctm, $depth+1, $fh )
        : ( $ch eq '8' )    ?   iterateRecipe( $rules->{ '8' }, $ctm, $depth+1, $fh )
        : ( $ch eq '9' )    ?   iterateRecipe( $rules->{ '9' }, $ctm, $depth+1, $fh )
        : ( $ch eq '1' )    ?   drawSide( $ctm, $depth, $fh )
        :                       noop()
        ;
    }

    return;
}

##################################################
#   Restore the ctm values

sub restoreCtm {

    my $ctm = shift;
    my $stored_ctm = shift;

    $ctm->{ 'x' } = $stored_ctm->{ 'x' };
    $ctm->{ 'y' } = $stored_ctm->{ 'y' };
    $ctm->{ 'ang' } = $stored_ctm->{ 'ang' };

    return;
}

##################################################
#   Draw a side and update the ctm

sub drawSide {

    my $ctm = shift;
    my $depth = shift;
    my $fh = shift;

    return if ( $depth < $iterations );

    my $nx = $ctm->{ 'x' } + $side * cos( deg2rad( $ctm->{ 'ang' } ) );
    my $ny = $ctm->{ 'y' } + $side * sin( deg2rad( $ctm->{ 'ang' } ) );

    printf $fh "%f %f moveto %f %f lineto stroke\n",
                                $ctm->{ 'x' }, $ctm->{ 'y' }, $nx, $ny;

    $ctm->{ 'x' } = $nx;
    $ctm->{ 'y' } = $ny;

    return;
}

##################################################
#   Write the image header

sub writeHeader {

    my $fh = shift;

    print $fh '%!PS

        0 0 moveto
        1000 0 lineto
        1000 1000 lineto
        0 1000 lineto
        fill

        1 1 0 setrgbcolor

        595 2 div 842 2 div translate
        .5 .5 scale
        0 0 moveto
    ';
}

##################################################
#   Usage doco

sub usage {

    print "\n@_\n" if @_;

    printf "\n$0 " .
            "[-h] [-i <count>] " .
            "[-r <recipe>] [-s <length>]

    where
        -h  this help doco
        -i  iteration count (default 4)
        -r  initial recipe (default 5 big-rhomb star)
        -s  polygon side length (default 20)
    \n";

    exit;
}

##################################################
#   Simple no-op function for the ternary layouts

sub noop {}

__END__

L-sys:

variables: 1 6 7 8 9 [ ]
constants: + -;
start: [7]++[7]++[7]++[7]++[7]
rules: 6 → 81++91----71[-81----61]++
7 → +81--91[---61--71]+
8 → -61++71[+++81++91]-
9 → --81++++61[+91++++71]--71
1 → (eliminated at each iteration)
angle: 36º
