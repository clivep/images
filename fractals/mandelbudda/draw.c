#include <stdio.h>

#define     IMG_CX      -1.000
#define     IMG_CY      0.000
#define     IMG_SIZE    5.000

#define     MAX_ITERS   1000
#define     DIST_LIMIT  2.0

#define     PIC_WID     10
#define     PIC_DEP     10

typedef struct {
    long double  x;
    long double  y;
} complex;

int iterate(
    complex pnt
) {
    int     iters;
    complex n_pnt;
    complex tmp;

    n_pnt.x = pnt.x;
    n_pnt.y = pnt.y;

    while ( iters < MAX_ITERS ) {

        tmp.x = ( n_pnt.x * n_pnt.x  ) - ( n_pnt.y * n_pnt.y ) + pnt.x;
        tmp.y = ( 2.0 * n_pnt.x * n_pnt.y  ) + pnt.y;

        n_pnt.x = tmp.x;
        n_pnt.y = tmp.y;

        if ( ( ( n_pnt.x * n_pnt.x  ) + ( n_pnt.y * n_pnt.y ) ) > ( DIST_LIMIT * DIST_LIMIT ) ) 
            return iters;

        iters++;
    }

    return iters;
}

int main( 
    int     argc,
    char    **argv
) {
    complex pnt;

    long double  img_x;
    long double  img_y;

    int     pic_x;
    int     pic_y;
    int     iters;

    for( pic_y = 0; pic_y < PIC_DEP; pic_y++ ) {
        
        img_y = IMG_CY + IMG_SIZE * ( ( pic_y / (long double)PIC_DEP ) - 0.5 );

        pnt.y = img_y;

        for( pic_x = 0; pic_x < PIC_WID; pic_x++ ) {

            img_x = IMG_CX + IMG_SIZE * ( ( pic_x / (long double)PIC_WID ) - 0.5 );
            pnt.x = img_x;
            iters = iterate( pnt );

            printf( "%c", ( iters < MAX_ITERS ) ? 'X' : ' ' );
        }

        printf( "\n" );
    }

    printf( "\n" );
}
