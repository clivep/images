#!/usr/bin/perl

use warnings;
use strict;

############################################################
#
#   web.pl
#
#   Build web pages according to the JSON in the DATA block
#
############################################################

use JSON::Parse 'parse_json';
use Getopt::Std;
use File::Copy 'cp';
use File::Basename;
use Readonly;

my Readonly    $DEFAULT_ROOT_DIR   =   './site';
my Readonly    $DEFAULT_IMAGE_DIR  =   '/home/clivep/Projects/misc/images/image_files';

my Readonly    $CSS_FILE            =  'images.css';

#############################################################
# Check the runline options

my %options;
getopts( 'hI:R:', \%options );

help() if $options{ 'h' };

my $IMAGE_DIR = $options{ 'I' } || $DEFAULT_IMAGE_DIR;
my $ROOT_DIR = $options{ 'R' } || $DEFAULT_ROOT_DIR;

mkdir $ROOT_DIR unless -d $ROOT_DIR;
mkdir "$ROOT_DIR/images" unless -d "$ROOT_DIR/images";

my $WEB_INFO = {};

############################################################
# Parse and validate the JSON in the DATA block

my $json = do{ local $/; <DATA> };
my $site_info = parse_json( $json );

############################################################
# Find and process info files in the images dir

checkTypeDirs();

############################################################
# Setup the pages

my $index_file = "$ROOT_DIR/index.html";
open my $idxfh, '>', $index_file or die "Could not open $index_file $!\n";

writeIndexHeader( $idxfh );

my $name_keys = {};
for my $name_key ( keys %{ $WEB_INFO->{ 'types' } } ) {

    my $type_info = $WEB_INFO->{ 'types' }->{ $name_key };
    $name_keys->{ $type_info ->{ 'name' } } = $name_key;
}

my $t = 0;
for my $name_key ( sort keys %{ $name_keys } ) {

    my $type_key = $name_keys->{ $name_key };
    processImageType( $idxfh, $WEB_INFO->{ 'types' }->{ $type_key }, $t++ );
}

writeIndexTrailer( $idxfh );

close $idxfh;

setupCSS( $site_info->{ 'css' } );

exit;

############################################################
# Load details from the type info file

sub parseTypeInfoFile {

    my $type_dir = shift;
    my $type_info_file = shift;

    $WEB_INFO->{ 'types' } ||= {}; 
    
    open my $typ_fh, '<', $type_info_file or die "Could not open $type_info_file: $!";  

    my $type_info = {
        "source-dir"    =>   $type_dir,
        "refs"          =>   [],
    };

    while ( my $ln = <$typ_fh> ) {

        chomp $ln;

        $ln =~ s/\s*#.*//x;

        my @parts = split( /:/x, $ln, 2 );
        next unless $parts[ 1 ];

        for my $part ( @parts ) {
            $part =~ s/^\s*//x;
            $part =~ s/\s*$//x;
        }

        if ( $parts[ 0 ] eq 'ref' ) {

            my @ref_parts = split( /\|/x, $parts[ 1 ] );
            next unless $ref_parts[ 1 ];

            for my $ref_part ( @parts ) {
                $ref_part =~ s/^\s*//x;
                $ref_part =~ s/\s*$//x;
            }

            push @{ $type_info->{ 'refs' } }, {
                "desc"  =>  $ref_parts[ 0 ],
                "href"  =>  $ref_parts[ 1 ],
            };
        }
        else {

            $type_info->{ $parts[ 0 ] } = $parts [ 1 ];
        }
    }

    close $typ_fh;

    $type_info->{ 'groups' } = [];

    $WEB_INFO->{ 'types' }->{ $type_dir } = $type_info;

    return $type_info;
}

############################################################
# Load details about the images

sub processTypeImages {

    my $type_info = shift;
    my $type_dir = shift;

    opendir( my $img_dh, $type_dir ) || die "Can't opendir $type_dir $!";
    my @img_files = grep { -f "$type_dir/$_" } readdir( $img_dh );
    closedir $img_dh;

    my $type_groups = {};
    my $group_images = {};

    for my $file ( @img_files ) {
 
        next if ( $file =~ /\.info$/x );

        my $info_file = "$type_dir/$file";
        $info_file =~ s/\.[^\.]*$/.info/x;
        next unless -f $info_file;

        my @type_refs = @{ $type_info->{ 'refs' } };

        open my $inf_fh, '<', $info_file or die "Could not open $info_file: $!";

        my $image_info = {
            "source"    =>   $file,
            "refs"      =>   \@type_refs,
        };

        while ( my $ln = <$inf_fh> ) {

            chomp $ln;

            $ln =~ s/^\s*//x;
            $ln =~ s/\s*$//x;
            $ln =~ s/\s*#.*//x;

            my @parts = split( /:/x, $ln );
            next unless $parts[ 1 ];

            for my $part ( @parts ) {
                $part =~ s/^\s*//x;
                $part =~ s/\s*$//x;
            }

            if ( $parts[ 0 ] eq 'ref' ) {

                my @ref_parts = split( /\|/x, $parts[ 1 ] );
                next unless $ref_parts[ 1 ];

                for my $ref_part ( @parts ) {
                    $ref_part =~ s/^\s*//x;
                    $ref_part =~ s/\s*$//x;
                }

                push @{ $image_info->{ 'refs' } }, {
                    "desc"  =>  $ref_parts[ 0 ],
                    "href"  =>  $ref_parts[ 1 ],
                };
            }
            else {
            
                $image_info->{ $parts[ 0 ] } = $parts [ 1 ];
            }

            if ( ! $image_info->{ 'group' } ) {

                printf "$info_file missing 'group' element\n";
                next;
            }

            $type_groups->{ $image_info->{ 'group' } } ||= {
                "desc"      =>  $image_info->{ 'group' },
                "images"    =>  []
            };
        }

        close $inf_fh;

        my $key = sprintf "%s-%s", $image_info->{ 'thumb-desc' }, $image_info->{ 'source' };
        $group_images->{ $key } = $image_info;
    }

    # Create the sorted lists
    for my $group_key ( sort keys %{ $type_groups } ) {
        push @{ $type_info->{ 'groups' } }, $type_groups->{ $group_key };
    }

    my $keys = {};
    for my $image_key ( keys %{ $group_images } ) {
        my $image_info = $group_images->{ $image_key };
        my $key = sprintf( "%05d-%s", $image_info->{ 'order' } || 999999, $image_key );
        $keys->{ $key } = $image_key;
    }

    for my $key ( sort keys %{ $keys } ) {
        my $image_key = $keys->{ $key };
        my $image_info = $group_images->{ $image_key };
        push @{ $type_groups->{ $image_info->{ 'group' } }->{ 'images' } }, $image_info;
    }

    return;
}

############################################################
# Check for details files in the type dirs

sub checkTypeDirs {

    opendir( my $type_dh, $DEFAULT_IMAGE_DIR ) || die "Can't opendir $DEFAULT_IMAGE_DIR $!";

    while ( my $entry = readdir $type_dh ) { 

        next unless -d "$DEFAULT_IMAGE_DIR/$entry";
        next if $entry =~ /^\./x;

        my $type_info_file = sprintf "%s/%s/type.info", $DEFAULT_IMAGE_DIR, $entry;
        next unless -f $type_info_file;

        my $type_info = parseTypeInfoFile( $entry, $type_info_file );
        processTypeImages( $type_info, "$DEFAULT_IMAGE_DIR/$entry" );
    }

    closedir $type_dh;

    return;
}

############################################################
# Write the css file into the site dir

sub setupCSS {

    my $css_info = shift;

    my $css_file = "$ROOT_DIR/$CSS_FILE";

    open my $cssfh, '>', $css_file or die "Could not open $css_file $!\n";

    map { print $cssfh "$_\n" } ( @{ $css_info } );

    close $cssfh;

    return;
}

############################################################
# Create a page for the given image type

sub processImageTypePage {

    my $type = shift;

    open my $typfh, '>', $type->{ 'page_path' } or die "Could not open $type->{ 'page_path' } $!\n";

    printf $typfh "<html>
      <head>
        <link rel='stylesheet' href='../images.css'>
        </head>
          <body>
            <div class='title'>%s</div>
            <ul>
    ",
    $type->{ 'name' }
    ;

    map { processImageTypeGroup( $typfh, $type, $_ ) } ( @{ $type->{ 'groups' } } );

    print $typfh "
            </ul>
            <div class='shortcuts'>
              <a href='../index.html'>
                <button class='nav'>
                  Home
                </button>
              </a>
            </div>
      </body>
    </html>
    ";

    close $typfh;

    return;
}

############################################################
# Process an image type group

sub processGroupImagePage {

    my $type = shift;
    my $group = shift;
    my $image = shift;

    open my $imgfh, '>', $image->{ 'page_path' } || die "Could not open $image->{ 'page_path' } $!\n";

    my $ref_list = "";
    for my $ref ( @{ $image->{ 'refs' } } ) {

        next if ( $ref->{ 'href' } eq "" ) && ( $ref->{ 'desc' } ne "" );
        $ref_list .= sprintf "<li><a href='%s'>%s</a>\n", $ref->{ 'href' }, $ref->{ 'desc' };
    }

    $ref_list = "<div class='subTitle'>References</div><ul>$ref_list<\ul>" if $ref_list ne "";

    printf $imgfh "<html>
        <head>
              <link rel='stylesheet' href='../images.css'>
          </head>
          <body>
            <div class='title'>%s</div>
            <table>
              <tr>
                <td rowspan=2>
                  <img class='%s' src='images/%s'><br>
                </td>
                <td class='imgDetail'>
                  <div class='subTitle'>Description</div>
                  %s
                  <p>
                  %s
                </td>
              </tr>
            </table>
            <div class='shortcuts'>
              <a href='../index.html'>
                <button class='nav'>
                  Home
                </button>
              </a>
              &nbsp;...&nbsp;
              <a href='%s'>
                <button class='nav'>
                  %s
                </button>
              </a>
            </div>
          </body>
        </html>
    ",
    $image->{ 'image-title' },
    $image->{ 'image-class' } || 'sqImg',
    basename( $image->{ 'path' } ),
    $image->{ 'image-detail' } || "",
    $ref_list,
    basename( $type->{ 'page_path' } ),
    $type->{ 'name' },
    ;

    close $imgfh;

    return;
}
    
############################################################
# Has the image changed and the dest needs updating?

sub imageNeedsUpdate {
    
    my $src_image = shift;
    my $dest_image = shift;

    return 1 unless -f $dest_image;

    my $src_tm = (stat( $src_image ))[9];
    my $dest_tm = (stat( $dest_image ))[9];

    return $src_tm > $dest_tm;
}
    
############################################################
# Process an image type group

sub processImageTypeGroup {
    
    my $fh = shift;
    my $type = shift;
    my $group = shift;

    printf "Processing group: %s\n", $group->{ 'desc' };

    printf $fh "
          <li>
            <div class='subTitle'>%s</div>
            <table>
              <tr>
    ",
    $group->{ 'desc' },
    ;

    for ( my $i = 0; $i < scalar @{ $group->{ 'images' } }; $i++ ) {

        my $image = $group->{ 'images' }->[ $i ];

        my $source_img = sprintf "%s/%s/%s", $IMAGE_DIR, $type->{'source-dir' }, $image->{ 'source' };
        unless ( -f $source_img ) {
            printf "Cannot find image '$source_img'..!\n";
            next;
        }

        printf"  image: %s\n", $source_img;

        # process the images
        my $thumb_dest = sprintf "%s/%s", $type->{ 'thumbs_dir' }, basename $image->{ 'source' };

        if ( imageNeedsUpdate( $source_img, $thumb_dest ) ) {
            my $cmd = sprintf "convert -resize 150x150 -quality 100 '%s' '%s'", $source_img, $thumb_dest;
            system( $cmd );
        }

        $image->{ 'name' } = basename $image->{ 'source' };
        $image->{ 'name' } =~ s/\.pdf$/.png/x if ( $image->{ 'name' } =~ /\.pdf$/x );

        $image->{ 'path' } = sprintf "%s/%s", $type->{ 'images_dir' }, $image->{ 'name' };

        if ( imageNeedsUpdate( $source_img, $image->{ 'path' } ) ) {
        
            my $cmd = sprintf "convert '%s' '%s'", $source_img, $image->{ 'path' };
            system( $cmd );
        }

        print $fh "
          </tr>
          <tr>
        " if $i && ( ( $i % 6 ) == 0 );

        $image->{ 'page' } = basename $image->{ 'source' };
        $image->{ 'page' } =~ s/\.[^\.]*$/.html/x;

        $image->{ 'page_path' } = sprintf "%s/%s", $type->{ 'dir' }, basename $image->{ 'page' };

        printf $fh "
                <td class='thumbCell'>
                  <a href='%s'>
                    <img class='%s' src='thumbs/%s'><br>
                    <div class='thumbDesc'>%s</div>
                  </a>
                </td>
        ",
        $image->{ 'page' },
        $image->{ 'thumb-class' } || 'sqThumb',
        basename( $image->{ 'path' } ),
        $image->{ 'thumb-desc' },
        ;

        processGroupImagePage( $type, $group, $image );
    };

    print $fh "
              </tr>
            </table>
          </li>
    ";

    return;
}

############################################################
# Process an image type

sub processImageType {

    my $fh = shift;
    my $type = shift;
    my $pos = shift;

    printf "Processing type %s\n", $type->{ 'name' };

    my $type_name = lc $type->{ 'name' };
    $type_name =~ s/^\s*//x;
    $type_name =~ s/\s+/-/gx;
    
    # Add the page path and dirs into the type
    
    $type->{ 'dir' } = "$ROOT_DIR/$type_name";
    $type->{ 'thumbs_dir' } = sprintf "%s/thumbs", $type->{ 'dir' };
    $type->{ 'images_dir' } = sprintf "%s/images", $type->{ 'dir' };

    mkdir $type->{ 'dir' } unless -d $type->{ 'dir' };
    mkdir $type->{ 'thumbs_dir' } unless -d $type->{ 'thumbs_dir' };
    mkdir $type->{ 'images_dir' } unless -d $type->{ 'images_dir' };

    my $type_page = sprintf "%s/%s.html", $type_name, $type_name;
    $type->{ 'page_path' } = sprintf "%s/%s.html", $type->{ 'dir' }, $type_name;

    printf $fh "
      </tr>
      <tr>
    " if $pos && ( ( $pos % 2 ) == 0 );

    printf $fh "
        <td>
          <ul>
            <li>
              <a style='display:block' href='%s'>
                <div class='subTitle'>%s</div>
                <div class='listText'>%s</div>
                <p/>
              </a>
            </li>
          </ul>
        </td>
    ",
    $type_page,
    $type->{ 'name' },
    $type->{ 'desc' }
    ;

    processImageTypePage( $type );

    return;
}

############################################################
# Write the header of the index html

sub writeIndexHeader {

    my $fh = shift;

    print $fh "<html>
        <head><link rel='stylesheet' href='$CSS_FILE'></head>
        <body>
          <div class='title'>IMAGES</div>
          <table>
    ";

    return;
}

############################################################
# Write the trailer of the index html

sub writeIndexTrailer {

    my $fh = shift;

    print $fh "
          </table>
        </body>
      </html>
    ";

    return;
}

############################################################
# Help doco

sub help {

    print "\n@_\n" if @_;

    printf "\n$0 " .
        " [-h] [-I <dir>] [-R <dir>]

        where
            -h          this help message
            -I <dir>    source of the image files
            -R <dir>    root dir for the web pages
    \n";

    exit
}

__DATA__

{
  "css": [
    "body { font-family: Arial; color: white; background-color: #336699; }",
    ".title { font-size: 20px; font-weight: bold; height: 50px; line-height: 50px; vertical-align: middle; }",
    ".subTitle { font-size: 16px; font-weight: bold; height: 20px; vertical-align: top; }",
    ".listText { font-size: 14px; font-weight: normal; text-indent: 20px; text-decoration: none; white-space: nowrap; overflow: hidden; }",
    ".thumbCell { text-align: center; }",
    ".thumbDesc { font-size: 12px; font-weight: normal; text-decoration: none; white-space: nowrap; overflow: hidden; height: 25px; line-height: 25px; vertical-align: bottom; text-align: center; }",
    ".imgDetail { font-size: 12px; font-weight: normal; text-decoration: none; white-space: nowrap; overflow: hidden; height: 25px; line-height: 25px; vertical-align: top; text-align: left; }",
    ".sqImg { border: 1px solid #ddd; border-radius: 4px; padding: 5px; width: 600px; height: 600px; }",
    ".sqThumb { border: 1px solid #ddd; border-radius: 4px; padding: 5px; width: 100px; height: 100px; }",
    ".spflrImg { border: 1px solid #ddd; border-radius: 4px; padding: 5px; width: 460px; height: 600px; }",
    ".spflrThumb { border: 1px solid #ddd; border-radius: 4px; padding: 5px; width: 100px; height: 130px; }",
    ".shortcuts { font-family: Arial; color: white; text-decoration: none; font-weight: bold; padding-left: 19px; }",
    ".nav { -webkit-appearance: button; -moz-appearance: button; appearance: button; font-family: Arial; color: white; text-decoration: none; font-weight: bold; background-color: #336699; border-radius: 8px; cursor: pointer; }",
    "a:hover, a:visited, a:link, a:active { font-family: Arial; color: white; text-decoration: none; }",
    "td { padding: 15px; }"
  ]
}
