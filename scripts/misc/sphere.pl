#!/usr/bin/perl

use warnings;
use strict;

use Getopt::Std;

############################################################
#
#   shpere.pl
#
#   Generate Postscript describing a hightlighted sphere
#
############################################################

my $IMAGE = {};

############################################################
# Check the runline options

my %options;
getopts( 'b:d:g:hm:r:s:x:y:z:', \%options );

help() if $options{ 'h' };

$IMAGE->{ 'MIN-COLOR-PCNT' } = $options{ 'm' } || 30;

$IMAGE->{ 'RED' } = $options{ 'r' } || 256;
$IMAGE->{ 'GREEN' } = $options{ 'g' } || 0;
$IMAGE->{ 'BLUE' } = $options{ 'b' } || 0;

$IMAGE->{ 'RAD' } = $options{ 'd' } ||  100;

$IMAGE->{ 'LAMP_X' } = $options{ 'x' } || 60;
$IMAGE->{ 'LAMP_Y' } = $options{ 'y' } || -60;
$IMAGE->{ 'LAMP_Z' } = $options{ 'z' } || 150;

$IMAGE->{ 'SHEEN' } = $options{ 's' } || 800;

normaliseLamp();

printf "%%!PS\n";
printf "gsave\n";
printf "/sz %d def\n", 2 * $IMAGE->{ 'RAD' };
printf "500 500 scale\n";
printf "0 1 360 {\n";
printf "    /a exch def\n";
printf "    .5 a cos 2 div add .5 a sin 2 div add a 0 gt { lineto } { moveto } ifelse\n";
printf "} for\n";
printf "clip\n";
printf "sz sz 8 [sz 0 0 sz neg 0 sz]\n";
printf "{<\n";

for( my $y = -$IMAGE->{ 'RAD' }; $y < $IMAGE->{ 'RAD' }; $y++ ) {

    for( my $x = -$IMAGE->{ 'RAD' }; $x <$IMAGE->{ 'RAD' }; $x++ ) {

        my $d = $x * $x + $y * $y;

        if ( $d > $IMAGE->{ 'RAD' } * $IMAGE->{ 'RAD' } ) {

            printf "000000";
        }
        else {

            my $z = sqrt( $IMAGE->{ 'RAD' } * $IMAGE->{ 'RAD' } - $d );

            my $v = (
                $x * $IMAGE->{ 'LAMP_X' } +
                $y * $IMAGE->{ 'LAMP_Y' } +
                $z * $IMAGE->{ 'LAMP_Z' }
            ) / $IMAGE->{ 'RAD' };

            $v = 0 if $v < 0;

            if ( $v * 1000 > 998 ) {

                printf "FFFFFF";
            }
            elsif ( $v * 1000 > $IMAGE->{ 'SHEEN' } ) {

                my $v1 = 1000 * $v - $IMAGE->{ 'SHEEN' };
                my $v2 = 1000 - $IMAGE->{ 'SHEEN' };
                my $v3 = ( $v1 * $v1 ) / ( $v2 * $v2 );

                my $r = $IMAGE->{ 'RED' } * $v + $v3 * ( 256 - $IMAGE->{ 'RED' } );
                my $g = $IMAGE->{ 'GREEN' } * $v + $v3 * ( 256 - $IMAGE->{ 'GREEN' } );
                my $b = $IMAGE->{ 'BLUE' } * $v + $v3 * ( 256 - $IMAGE->{ 'BLUE' } );

                printf "%02x%02x%02x", 
                            min( $r, 255 ),
                            min( $g, 255 ),
                            min( $b, 255 ),
                ;
            }
            else {

                $v = $IMAGE->{ 'MIN-COLOR-PCNT' } / 100
                                if ( $v * 100 < $IMAGE->{ 'MIN-COLOR-PCNT' } );

                printf "%02x%02x%02x", 
                            min( $IMAGE->{ 'RED' } * $v, 255 ),
                            min( $IMAGE->{ 'GREEN' } * $v, 255 ),
                            min( $IMAGE->{ 'BLUE' } * $v, 255 ),
                ;
            }
        }
    }

    printf "\n";
}

printf ">}\n";
printf "false 3 colorimage\n";
printf "grestore\n";

exit;

############################################################
# Return the smaller of the given values

sub min {

    my $v1 = shift;
    my $v2 = shift;

    return $v1 < $v2 ? $v1 : $v2;
}

############################################################
# Normalise the lamp vector

sub normaliseLamp {

    my $d = sqrt( 
        $IMAGE->{ 'LAMP_X' } * $IMAGE->{ 'LAMP_X' } +
        $IMAGE->{ 'LAMP_Y' } * $IMAGE->{ 'LAMP_Y' } +
        $IMAGE->{ 'LAMP_Z' } * $IMAGE->{ 'LAMP_Z' }
    );

    $IMAGE->{ 'LAMP_X' } /= $d;
    $IMAGE->{ 'LAMP_Y' } /= $d;
    $IMAGE->{ 'LAMP_Z' } /= $d;
}

############################################################
# Help doco

sub help {

    print "\n@_\n" if @_;

    printf "\n$0 " .
                "[-h] [-b <blue>] [-d <rad>] [-g <green>] [-m <amb>] [-r <red>] " .
                "[-s <shn>] [-x <xpos>] [-y <ypos>] [-z <zpos>] 

        where
            -h          this help message
            -d          sphere radius (default 100)
            -b          base sphere blue value 0<b<255 (default 256)
            -g          base sphere green value 0<g<255 (default 0)
            -m          min ambience (30%)
            -s          start value for the sheen 0<s<1000 (default 800)
            -r          base sphere red value 0<r<255 (default 0)
            -x          x coord for the lamp
            -y          y coord for the lamp
            -z          z coord for the lamp

    \n";

    exit;
}

__END__
