#!/usr/bin/perl
 
use warnings;
use strict;
 
use Math::Trig;
use Data::Dumper;
use Getopt::Std;

use constant    POLYGON_STATUS_READY    =>  'POLYGON_READY';
use constant    POLYGON_STATUS_DONE     =>  'POLYGON_DONE';
 
use constant    INVDISC_STATUS_READY    =>  'INVDISC_READY';
use constant    INVDISC_STATUS_SHARED   =>  'INVDISC_SHARED';
use constant    INVDISC_STATUS_DONE     =>  'INVDISC_DONE';

use constant    VERTEX_STATUS_READY     =>  'VERTEX_READY';
use constant    VERTEX_STATUS_DONE      =>  'VERTEX_DONE';

use constant    SCALE                   =>  300;

use constant    BIGNUM                  =>  10E10;
use constant    SMALLNUM                =>  10E-10;

use constant    TRUE                    =>  1;
use constant    FALSE                   =>  0;

use constant    LINE_FACTOR             =>  .5;

use constant    EXTERNAL_SHADE          =>  250;
use constant    INTERNAL_SHADE          =>  1000;

use constant    DEFAULT_DEST            =>  'tess.pdf';
use constant    TEMP                    =>  'clp.ps';


my $LOOKUP = {};

my $POLYGONS = [];
my $DEPTH_IDS = {};

############################################################
# Parse any run-line arguments

my %OPTIONS;
getopts('a:c:dD:e:i:If:hlLo:Op:R:s:tvWx:X:z:', \%OPTIONS);

help() if $OPTIONS{ 'h' };

my $FONT_SIZE = $OPTIONS{ 'a' } || 8;
my $TILING_CONFIG = $OPTIONS{ 'c' };
my $DEBUG_DUMP_ALL_DATA = $OPTIONS{ 'd' };
my $DRAW_POLYGON = $OPTIONS{ 'D' };
my $EXTERNAL_SHADE = $OPTIONS{ 'e' } || EXTERNAL_SHADE;
my $LINE_FACTOR = $OPTIONS{ 'f' } || LINE_FACTOR;
my $INTERNAL_SHADE = $OPTIONS{ 'i' } || INTERNAL_SHADE;
my $DRAW_POLYGON_INVDISCS = $OPTIONS{ 'I' };
my $DRAW_LABELS = $OPTIONS{ 'l' };
my $DRAW_POLYGON_LINES = $OPTIONS{ 'L' };
my $DRAW_OVERLAYS = $OPTIONS{ 'O' };
my $DEST = $OPTIONS{ 'o' } || DEFAULT_DEST;
my $VERTEX_POLYGONS = $OPTIONS{ 'p' };
my $POLYGON_SIDES = $OPTIONS{ 's' };
my $ROTATION_OFFSET = pi * ( $OPTIONS{ 'R' } || 0 ) / 180;
my $TRIANGULATE_POLYGONS = $OPTIONS{ 't' };
my $DEBUG_DUMP_VERTICES = $OPTIONS{ 'v' };
my $DRAW_POLYGON_WIREFRAMES = $OPTIONS{ 'W' };
my $MAX_DEPTH = $OPTIONS{ 'x' } || 0;
my $DEBUG_EXPLAIN_VERTEX = $OPTIONS{ 'X' };
my $LAST_POLYGON = $OPTIONS{ 'z' };

my $DEBUG_EXPLAIN_VERTEX_POLYGON_ID = '';
my $DEBUG_EXPLAIN_VERTEX_NUMBER = '';

unless ( $TILING_CONFIG || ( $VERTEX_POLYGONS && $POLYGON_SIDES ) ) {

    printf "Missing tiling configuration details\n";
    help();
}

if ( $DEBUG_EXPLAIN_VERTEX ) {

    (
        $DEBUG_EXPLAIN_VERTEX_POLYGON_ID,
        $DEBUG_EXPLAIN_VERTEX_NUMBER,
    ) = $DEBUG_EXPLAIN_VERTEX =~ /(.*)\/(.*)/;

    help( 'Bad vertex id' ) unless defined $DEBUG_EXPLAIN_VERTEX_NUMBER;
}

############################################################
# Do the tiling...

$POLYGONS = setupPolygons();

my $all_done = 0;
while ( ! $all_done ) {

    $all_done = 1;

    my $p_max = scalar @{$POLYGONS};    # count now since the list will be altered

    for ( my $p = 0; $p < $p_max; $p++ ) {

        my $polygon = $POLYGONS->[ $p ];

        next if ( $polygon->{ 'STATUS' } eq POLYGON_STATUS_DONE );

        $all_done = 0;

        processPolygon( $polygon );
    
        last if $LAST_POLYGON && $LOOKUP->{ $LAST_POLYGON };
    }

    last if $LAST_POLYGON && $LOOKUP->{ $LAST_POLYGON };
}

explainVertex() if ( $DEBUG_EXPLAIN_VERTEX_POLYGON_ID );

if ( $DEBUG_DUMP_ALL_DATA ) {
    dumpAllData();
} elsif( $DEBUG_DUMP_VERTICES ) {
    dumpVertices();
}
else {
    draw();
}

exit;

############################################################
# Setup the initial set of polygons and inversion discs

sub setupPolygons {

    if ( $TILING_CONFIG ) {
        
        my $config;

        for ( $TILING_CONFIG ) {
            /test/ && do { $config = \&testConfig; last };
        }

        unless ( $config ) {
            printf "Unknown config\n";
            help();
        };

        return $config->();
    }
    else {
        return setupFromSideAndVertexCount()
    }
}

############################################################
# testConfig

sub testConfig {

    # Find vertices for a hexagonal initial polygon and construct
    # 6 kite shaped quadrilaterals using the centre as a vertex

    my $base_polygon = findInitialPolygon( 6, 4 );

    $POLYGON_SIDES = 4;

    my $centre = {
        'STATUS'        =>  VERTEX_STATUS_DONE,
        'X'             =>  0,
        'Y'             =>  0,
        'REF_MAX'       =>  5,
        'REFS'          =>  6,
    };

    my $polygon_list = [];
    for ( my $p = 0; $p < 6; $p++ ) {

        my $polygon = {
            'ID'        =>  $p + 1,
            'STATUS'    =>  POLYGON_STATUS_READY,
            'DEPTH'     =>  0,
        };

        my $invdisc1 = $base_polygon->{ 'INVDISCS' }->[ $p - 1 ];

        my $dist = distance( { 'X' => 0, 'Y' => 0 }, $invdisc1 );
        my $nx1 = $invdisc1->{ 'X' } * ( $dist - $invdisc1->{ 'RADIUS' } ) / $dist;
        my $ny1 = $invdisc1->{ 'Y' } * ( $dist - $invdisc1->{ 'RADIUS' } ) / $dist;

        my $invdisc2 = $base_polygon->{ 'INVDISCS' }->[ $p ];

        $dist = distance( { 'X' => 0, 'Y' => 0 }, $invdisc2 );
        my $nx2 = $invdisc2->{ 'X' } * ( $dist - $invdisc2->{ 'RADIUS' } ) / $dist;
        my $ny2 = $invdisc2->{ 'Y' } * ( $dist - $invdisc2->{ 'RADIUS' } ) / $dist;

        my $vertices = [
            $centre,
            ( $p > 0 ) ?
                $polygon_list->[ $p - 1 ]->{ 'VERTICES' }->[ 3 ]
            :
                {
                    'STATUS'        =>  VERTEX_STATUS_READY,
                    'X'             =>  $nx1,
                    'Y'             =>  $ny1,
                    'REF_MAX'       =>  5,
                    'REFS'          =>  2,          # count of surrounding polygons
                    'LAST_POLYGON'  =>  $polygon,   # last polygon built on this vertex
                },
            $base_polygon->{ 'VERTICES' }->[ $p ],
            ( $p == 5 ) ?
                $polygon_list->[ 0 ]->{ 'VERTICES' }->[ 1 ]
            :
                {
                    'STATUS'        =>  VERTEX_STATUS_READY,
                    'X'             =>  $nx2,
                    'Y'             =>  $ny2,
                    'REF_MAX'       =>  5,
                    'REFS'          =>  2,          # count of surrounding polygons
                    'LAST_POLYGON'  =>  $polygon,   # last polygon built on this vertex
                },
        ];
        $vertices->[ 2 ]->{ 'REF_MAX' } = 4;
        
        my $invdiscs = [
            {
                'INFINITE'  => TRUE,
                'STATUS'    => 'INVDISC_SHARED',
            },
            $invdisc1,
            $invdisc2,
            {
                'INFINITE'  => TRUE,
                'STATUS'    => 'INVDISC_SHARED',
            },
        ];

        $polygon->{ 'VERTICES' } = $vertices;
        $polygon->{ 'INVDISCS' } = $invdiscs;

        findPolygonBBox( $polygon );
        addLookup( $polygon );

        push @{ $polygon_list }, $polygon;
    }
    
    return $polygon_list;
}

############################################################
# Setup the config from polygon sides and vertex values

sub setupFromSideAndVertexCount {

    unless ( ( $VERTEX_POLYGONS * ( 1 - 2 / $POLYGON_SIDES ) ) > 2 ) {

        printf "Not a valid tiling combination...\n";
        exit;
    }

    my $polygon_list = [];
    my $initial_polygon = findInitialPolygon( $POLYGON_SIDES, $VERTEX_POLYGONS );

    if ( $TRIANGULATE_POLYGONS ) {

        my $origin = {
                        'STATUS'        =>  VERTEX_STATUS_DONE,
                        'X'             =>  0.0,
                        'Y'             =>  0.0,
                        'REFS'          =>  $POLYGON_SIDES,
                        'REF_MAX'       =>  $POLYGON_SIDES,
                        'LAST_POLYGON'  =>  $initial_polygon,
        };

        for ( my $side = 0; $side < $POLYGON_SIDES; $side++ ) {

            my $vertices = [
                $origin,
                $initial_polygon->{ 'VERTICES' }->[ $side ],
                $initial_polygon->{ 'VERTICES' }->[ ( $side + 1 ) % $POLYGON_SIDES ],
            ];

            $vertices->[ 1 ]->{ 'REFS' } = 2;
            $vertices->[ 1 ]->{ 'REF_MAX' } = $VERTEX_POLYGONS * 2;
            $vertices->[ 2 ]->{ 'REFS' } = 2;
            $vertices->[ 2 ]->{ 'REF_MAX' } = $VERTEX_POLYGONS * 2;

            my $this_id = sprintf "0-%d", $side + 1;
            my $next_id = sprintf "0-%d", 1 + ( $side + 1 ) % $POLYGON_SIDES;
            my $prev_id = sprintf "0-%d", 1 + ( $side - 1 ) % $POLYGON_SIDES;

            $vertices->[ 0 ]->{ 'POLYGONS' }->{ $this_id } = 1;
            $vertices->[ 0 ]->{ 'POLYGONS' }->{ $next_id } = 1;
            $vertices->[ 0 ]->{ 'POLYGONS' }->{ $prev_id } = 1;

            $vertices->[ 1 ]->{ 'POLYGONS' }->{ $this_id } = 1;
            $vertices->[ 1 ]->{ 'POLYGONS' }->{ $prev_id } = 1;

            $vertices->[ 2 ]->{ 'POLYGONS' }->{ $this_id } = 1;
            $vertices->[ 2 ]->{ 'POLYGONS' }->{ $next_id } = 1;

            my $invdiscs = [
                {
                    'INFINITE'  => TRUE,
                    'STATUS'    => 'INVDISC_SHARED',
                },
                $initial_polygon->{ 'INVDISCS' }->[ $side ],
                {
                    'INFINITE'  => TRUE,
                    'STATUS'    => 'INVDISC_SHARED',
                },
            ];

            my $polygon = {

                'ID'        =>  $side + 1,
                'STATUS'    =>  POLYGON_STATUS_READY,
                'DEPTH'     =>  0,
                'VERTICES'  =>  $vertices,
                'INVDISCS'  =>  $invdiscs,
            };

            findPolygonBBox( $polygon );
            addLookup( $polygon );

            push @{$polygon_list}, $polygon;
        }

        $POLYGON_SIDES = 3;
    }
    else {

        map { $_->{ 'POLYGONS' }->{ '0-1' } = 1 } ( @{$initial_polygon->{ 'VERTICES' }} );

        findPolygonBBox( $initial_polygon );
        addLookup( $initial_polygon );

        push @{$polygon_list}, $initial_polygon;
    }

    return $polygon_list;
}

############################################################
# Build the initial polygon...

sub findInitialPolygon {

    my $polygon_sides = shift;
    my $vertex_polygons = shift;

    my $polygon_ang = 2 * pi / $polygon_sides;
    my $vertex_ang = 2 * pi / $vertex_polygons;

    my $ang = ( pi / 2.0 ) - 0.5 * ( $polygon_ang + $vertex_ang );

    my $w = sin( $ang ) + cos( $ang ) * tan( $polygon_ang / 2.0 );
    my $x = tan( $polygon_ang / 2.0 );
    my $disc_rad = sqrt( $x * $x / ( $w * $w - $x * $x ) );

    my $dist = sqrt( 1 + $disc_rad * $disc_rad );
    my $vertex_dist = ( $dist - $disc_rad * cos( $ang ) ) / cos( $polygon_ang / 2.0 );

    my $initial_vertices = [];
    my $initial_invdiscs = [];

    my $polygon = {

        'ID'        =>  1,
        'STATUS'    =>  POLYGON_STATUS_READY,
        'DEPTH'     =>  0,
    };

    for ( my $vtx = 0; $vtx < $polygon_sides; $vtx++ )  {

        $ang = 2 * pi * $vtx / $polygon_sides;

        push @{$initial_vertices}, {

            'STATUS'        =>  VERTEX_STATUS_READY,
            'X'             =>  SCALE * $vertex_dist * cos( $ROTATION_OFFSET + $ang ), 
            'Y'             =>  SCALE * $vertex_dist * sin( $ROTATION_OFFSET + $ang ), 
            'REF_MAX'       =>  $vertex_polygons,
            'REFS'          =>  1,          # count of surrounding polygons
            'LAST_POLYGON'  =>  $polygon,   # last polygon built on this vertex
        };

        $ang = pi * ( 0.50 + $vtx ) * 2 / $polygon_sides;

        push @{$initial_invdiscs}, {

            'STATUS'    =>  INVDISC_STATUS_READY,
            'X'         =>  SCALE * $dist * cos( $ROTATION_OFFSET + $ang ), 
            'Y'         =>  SCALE * $dist * sin( $ROTATION_OFFSET + $ang ), 
            'RADIUS'    =>  SCALE * $disc_rad,
        };
    }
     
    $polygon->{ 'VERTICES' } = $initial_vertices;
    $polygon->{ 'INVDISCS' } = $initial_invdiscs;

    return $polygon;
}

############################################################
# Display some debugging info

sub invdiscStatus {
    
    my $invdisc = shift;

    return 'I' if ( $invdisc->{ 'INFINITE' } );
    return 'R' if ( $invdisc->{ 'STATUS' } eq INVDISC_STATUS_READY );
    return 'S' if ( $invdisc->{ 'STATUS' } eq INVDISC_STATUS_SHARED );
    return 'D' if ( $invdisc->{ 'STATUS' } eq INVDISC_STATUS_DONE );

    return '?';
}

sub vertexStatus {
    
    my $vertex = shift;

    return 'R' if ( $vertex->{ 'STATUS' } eq VERTEX_STATUS_READY );
    return 'D' if ( $vertex->{ 'STATUS' } eq VERTEX_STATUS_DONE );

    return '?';
}

############################################################
# Display the polygon vertex data

sub dumpVertices {

    my $use_polygon = shift;

    my @list = $use_polygon ? ( $use_polygon ) : @{$POLYGONS};

    for my $polygon ( @list ) {

        printf "polygon %s\n", polygonId( $polygon );
        printf "    %0.5f %0.5f %0.5f %0.5f\n", 
                    $polygon->{ 'BBOX' }->[ 0 ],
                    $polygon->{ 'BBOX' }->[ 1 ],
                    $polygon->{ 'BBOX' }->[ 2 ],
                    $polygon->{ 'BBOX' }->[ 3 ];

        for ( my $s = 0; $s < $POLYGON_SIDES; $s++ ) {
            printf "        %0.5f %0.5f\n",
                            $polygon->{ 'VERTICES' }->[ $s ]->{ 'X' },
                            $polygon->{ 'VERTICES' }->[ $s ]->{ 'Y' };
        }
    }
}

############################################################
# Display the POLYGON data struct

sub dumpAllData {

    for my $polygon ( @{$POLYGONS} ) {

        printf "%02d-%02d (%s): ",
                $polygon->{ 'DEPTH' },
                $polygon->{ 'ID' },
                ( $polygon->{ 'STATUS' } eq POLYGON_STATUS_READY ) ? 'R' : 'D';

        for ( my $s = 0; $s < $POLYGON_SIDES; $s++ ) {

            printf "%x %s (%d) -%s- ",
                $polygon->{ 'VERTICES' }->[ $s ],
                vertexStatus( $polygon->{ 'VERTICES' }->[ $s ] ),
                $polygon->{ 'VERTICES' }->[ $s ]->{ 'REFS' },
                invdiscStatus( $polygon->{ 'INVDISCS' }->[ $s ] );
        }

        printf "\n";
    }
}

############################################################
# Process the given polygon

sub processPolygon {

    my $polygon = shift;

    # mark this now - so we dont process it again if at MAX_DEPTH
    $polygon->{ 'STATUS' } = POLYGON_STATUS_DONE;

    return unless ( $polygon->{ 'DEPTH' } < $MAX_DEPTH );

    my $this_id = polygonId( $polygon );

    for ( my $d = 0; $d < $POLYGON_SIDES; $d++ ) {

        next if $LAST_POLYGON && $LOOKUP->{ $LAST_POLYGON };

        my $invdisc = $polygon->{ 'INVDISCS' }->[ $d ];
        next if ( $invdisc->{ 'STATUS' } ne INVDISC_STATUS_READY );

        $invdisc->{ 'STATUS' } = INVDISC_STATUS_DONE;

        my $vertex1 = $polygon->{ 'VERTICES' }->[ $d ];
        my $vertex2 = $polygon->{ 'VERTICES' }->[ ( $d+1 ) % $POLYGON_SIDES ];

        # Check if this polygon has already been drawn from another edge...
        
        next if ( $vertex1->{ 'STATUS' } eq VERTEX_STATUS_DONE );
        next if ( $vertex2->{ 'STATUS' } eq VERTEX_STATUS_DONE );

        my $new_vertices = [ $vertex1, $vertex2 ];

        my %invdisc_copy = %{ $invdisc };
        $invdisc_copy{ 'STATUS' } = INVDISC_STATUS_SHARED;
        my $new_invdiscs = [ \%invdisc_copy ];

        # reflect the other vertices in $invdisc

        for ( my $v = 0; $v < $POLYGON_SIDES - 2; $v++ ) {

            my $pnt = ( $v + $d + 2 ) % $POLYGON_SIDES;

            my $vertex = $polygon->{ 'VERTICES' }->[ $pnt ];
            my $new_vertex = invertPoint( $vertex, $invdisc );
    
            $new_vertex->{ 'REF_MAX' } = $vertex->{ 'REF_MAX' };
            $new_vertex->{ 'STATUS' } = VERTEX_STATUS_READY;
            $new_vertex->{ 'REFS' } = 1;

            push @{$new_vertices}, $new_vertex;
        }

        # reflect the remaining invdiscs in $invdisc

        for ( my $v = 0; $v < $POLYGON_SIDES - 1; $v++ ) {

            my $pnt = ( $v + $d + 1 ) % $POLYGON_SIDES;

            my $new_disc =
                invertDisc( $polygon->{ 'INVDISCS' }->[ $pnt ], $invdisc ) ||
                invertInfiniteDisc( $polygon, $d, $pnt, $invdisc, $new_vertices )
            ;

            $new_disc->{ 'STATUS' } = INVDISC_STATUS_READY;

            push @{$new_invdiscs}, $new_disc;
        }

        incrementVertexRefs( $vertex1 );
        incrementVertexRefs( $vertex2 );

        my $next_depth = nextDepthId( $polygon->{ 'DEPTH' } );

        my $new_id = sprintf "%d-%d", $polygon->{ 'DEPTH' } + 1, $next_depth;

        map { $_->{ 'POLYGONS' }->{ $new_id } = 1 } ( @{ $new_vertices } );
        $vertex1->{ 'POLYGONS' }->{ $new_id } = 1;
        $vertex2->{ 'POLYGONS' }->{ $new_id } = 1;

        my $new_polygon = {

            'STATUS'    =>  POLYGON_STATUS_READY,
            'DEPTH'     =>  $polygon->{ 'DEPTH' } + 1,
            'ID'        =>  $next_depth,
            'VERTICES'  =>  $new_vertices,
            'INVDISCS'  =>  $new_invdiscs,
        };

        if ( $vertex1->{ 'STATUS' } eq VERTEX_STATUS_DONE ) {

            my $last_polygon = $vertex1->{ 'LAST_POLYGON' };
            my $idx = findVertexIndex( $vertex1, $last_polygon );
            my $d1 = distance( $new_vertices->[ -1 ], $last_polygon->{ 'VERTICES' }->[ $idx - 1 ] );
            my $p1 = ( $idx + 1 ) % $POLYGON_SIDES;
            my $d2 = distance( $new_vertices->[ -1 ], $last_polygon->{ 'VERTICES' }->[ $p1 ] );
            my $x = ( $d1 < $d2 ) ? $idx - 1 : $p1;

            $new_polygon->{ 'VERTICES' }->[ -1 ] = $last_polygon->{ 'VERTICES' }->[ $x ];
            $new_polygon->{ 'VERTICES' }->[ -1 ]->{ 'POLYGONS' }->{ $new_id } = 1;
            incrementVertexRefs( $new_polygon->{ 'VERTICES' }->[ -1 ] );
            #incrementVertexRefs( $last_polygon->{ 'VERTICES' }->[ $x ] );
        
            $last_polygon->{ 'INVDISCS' }->[ $idx ]->{ 'STATUS' } = INVDISC_STATUS_SHARED;
            $last_polygon->{ 'INVDISCS' }->[ $idx - 1 ]->{ 'STATUS' } = INVDISC_STATUS_SHARED;
        }

        if ( $vertex2->{ 'STATUS' } eq VERTEX_STATUS_DONE ) {

            my $last_polygon = $vertex2->{ 'LAST_POLYGON' };
            my $idx = findVertexIndex( $vertex2, $last_polygon );
            my $d1 = distance( $new_vertices->[ -1 ], $last_polygon->{ 'VERTICES' }->[ $idx - 1 ] );
            my $p1 = ( $idx + 1 ) % $POLYGON_SIDES;
            my $d2 = distance( $new_vertices->[ -1 ], $last_polygon->{ 'VERTICES' }->[ $p1 ] );
            my $x = ( $d1 < $d2 ) ? $idx - 1 : $p1;

            # Check for the special case where both vertices are now complete but vertex that
            # was shared is the same for both (could only happen for triangular polygons)
            if ( ( $vertex1->{ 'STATUS' } eq VERTEX_STATUS_DONE ) && ( $POLYGON_SIDES == 3 ) ) {

                # Change all instances of this vertex - could well be shared many times
                updateVertex( $last_polygon->{ 'VERTICES' }->[ 2 ],
                                                $new_polygon->{ 'VERTICES' }->[ 2 ] );
                incrementVertexRefs( $last_polygon->{ 'VERTICES' }->[ $x ] );
            }
            
            $new_polygon->{ 'VERTICES' }->[ 2 ] = $last_polygon->{ 'VERTICES' }->[ $x ];
            $new_polygon->{ 'VERTICES' }->[ 2 ]->{ 'POLYGONS' }->{ $new_id } = 1;

            incrementVertexRefs( $new_polygon->{ 'VERTICES' }->[ 2 ] );
        
            $last_polygon->{ 'INVDISCS' }->[ $idx ]->{ 'STATUS' } = INVDISC_STATUS_SHARED;
            $last_polygon->{ 'INVDISCS' }->[ $idx - 1 ]->{ 'STATUS' } = INVDISC_STATUS_SHARED;
        }

        findPolygonBBox( $new_polygon );

        # set the last polygon for each of the vertices
        map { $_->{ 'LAST_POLYGON' } = $new_polygon } @{ $new_vertices };

        addLookup( $new_polygon );

        push @{$POLYGONS}, $new_polygon;

        last if (
            ( $d == ( $POLYGON_SIDES - 1 ) ) &&
            ( $vertex2->{ 'STATUS' } eq VERTEX_STATUS_DONE )
        );
    }
}

############################################################
# Invert an infinite disc in the given disc - find a disc 
# that touches the invdisc centre and the 2 vertices common
# to the invdisc

sub invertInfiniteDisc {

    my $polygon = shift;
    my $vertex = shift;
    my $pnt = shift;
    my $invdisc = shift;
    my $new_vertices = shift;

    my $vertex2 = ( $vertex + 1 ) % $POLYGON_SIDES;

    my $ax = $invdisc->{ 'X' };
    my $ay = $invdisc->{ 'Y' };

    my $bx = $new_vertices->[ $pnt - $vertex ]->{ 'X' };
    my $by = $new_vertices->[ $pnt - $vertex ]->{ 'Y' };

    my $cx = $new_vertices->[ $pnt - $vertex + 1 ]->{ 'X' };
    my $cy = $new_vertices->[ $pnt - $vertex + 1 ]->{ 'Y' };

    if ( mod( $ax - $bx ) < SMALLNUM ) {

        my $tmp;

        $tmp = $bx; $bx = $cx; $cx = $tmp;
        $tmp = $by; $by = $cy; $cy = $tmp;
    }

    my $a = $ax * $ax + $ay * $ay;
    my $b = 2 * ( $bx - $ax );
    my $c = $bx * $bx + $by * $by;
    my $d = 2 * ( $ay - $by );

    my $e = 2 * ( $cx - $ax );
    my $f = $cx * $cx + $cy * $cy;
    my $g = 2 * ( $ay - $cy );

    # Does the infinite disc (line) passes through the centre of the invdisc...
    if ( mod ( $e * $d - $g * $b ) < SMALLNUM ) {
        my %disc_copy = %{ $invdisc };
        $disc_copy{ 'STATUS' } = INVDISC_STATUS_DONE;
        return \%disc_copy;
    }

    my $oy = ( $b * ( $f - $a ) + $e * ( $a - $c ) ) / ( $e * $d - $g * $b );
    my $ox = ( $c - $a + $d * $oy ) / $b;

    my $r = sqrt( ( $ax - $ox ) * ( $ax - $ox ) + ( $ay - $oy ) * ( $ay - $oy ) );

    return {
        'X'         => $ox,
        'Y'         => $oy,
        'RADIUS'    => $r,
        'INFINITE'  => FALSE,
    }
}

############################################################
# Simple mod/min/max functions

sub min {

    my ( $v1, $v2 ) = @_;

    return ( $v1 < $v2 ) ? $v1 : $v2;
}

sub max {

    my ( $v1, $v2 ) = @_;

    return ( $v1 > $v2 ) ? $v1 : $v2;
}

sub mod {

    my $v = shift;

    return ( $v > 0 ) ? $v : ( -1 * $v );
}

############################################################
# Invert the references around a gicen point

sub incrementVertexRefs {

    my $vertex = shift;

    $vertex->{ 'REFS' }++;
    $vertex->{ 'STATUS' } = VERTEX_STATUS_DONE
                if ( $vertex->{ 'REFS' } == $vertex->{ 'REF_MAX' } );
}

############################################################
# Calculate the boundingboxc for the given polygon

sub findPolygonBBox {

    my $polygon = shift;

    my $lx = $polygon->{ 'VERTICES' }->[ 0 ]->{ 'X' };
    my $ly = $polygon->{ 'VERTICES' }->[ 0 ]->{ 'Y' };
    my $ux = $lx;
    my $uy = $ly;

    for ( my $s = 1; $s < scalar @{ $polygon->{ 'VERTICES' } }; $s++ ) {

        $lx = min( $polygon->{ 'VERTICES' }->[ $s ]->{ 'X' }, $lx );
        $ly = min( $polygon->{ 'VERTICES' }->[ $s ]->{ 'Y' }, $ly );

        $ux = max( $polygon->{ 'VERTICES' }->[ $s ]->{ 'X' }, $ux );
        $uy = max( $polygon->{ 'VERTICES' }->[ $s ]->{ 'Y' }, $uy );
    }

    $polygon->{ 'BBOX' } = [ $lx, $ly, $ux, $uy ];
}

############################################################
# Invert the given point in the given disc

sub invertPoint {

    my $point = shift;
    my $invdisc = shift;

    my $dx = $point->{ 'X' } - $invdisc->{ 'X' };
    my $dy = $point->{ 'Y' } - $invdisc->{ 'Y' };

    my $r2 = $invdisc->{ 'RADIUS' } * $invdisc->{ 'RADIUS' };
    my $v = ( $dx * $dx ) + ( $dy * $dy );

    return undef unless $v;

    my $x = $invdisc->{ 'X' } + $dx * $r2 / $v;
    my $y = $invdisc->{ 'Y' } + $dy * $r2 / $v;

    return {
        'X'         => $x, 
        'Y'         => $y,
        'VAL'       => $r2 / $v,
    };
}

############################################################
# Invert one disc in another

sub invertDisc {

    my $disc = shift;
    my $invdisc = shift;

    return undef if ( $disc->{ 'INFINITE' } );

    my $dx = $disc->{ 'X' } - $invdisc->{ 'X' };
    my $dy = $disc->{ 'Y' } - $invdisc->{ 'Y' };

    my $dist = distance( $disc, $invdisc );

    my $pt1 = {
                'X' => $invdisc->{ 'X' } + $dx * ( 1 + $disc->{ 'RADIUS' } / $dist ),
                'Y' => $invdisc->{ 'Y' } + $dy * ( 1 + $disc->{ 'RADIUS' } / $dist ),
    };

    my $vtx1 = invertPoint( $pt1, $invdisc );
    return { 'INFINITE'  => TRUE } unless $vtx1;
    #return undef unless $vtx1;

    my $pt2 = {
                'X' => $invdisc->{ 'X' } + $dx * ( 1 - $disc->{ 'RADIUS' } / $dist ),
                'Y' => $invdisc->{ 'Y' } + $dy * ( 1 - $disc->{ 'RADIUS' } / $dist ),
    };
    
    my $vtx2 = invertPoint( $pt2, $invdisc );
    return { 'INFINITE'  => TRUE } unless $vtx2;

    my $inf_disc = ( ( $vtx1->{ 'VAL' } > BIGNUM ) || ( $vtx2->{ 'VAL' } > BIGNUM ) );

    return ( $vtx1 && $vtx2 && ! $inf_disc ) ? {
        'X'         => ( $vtx1->{ 'X' } + $vtx2->{ 'X' } ) / 2, 
        'Y'         => ( $vtx1->{ 'Y' } + $vtx2->{ 'Y' } ) / 2, 
        'RADIUS'    => distance( $vtx1, $vtx2 ) / 2,
        'INFINITE'  => FALSE,
    }
    : {
        'INFINITE'  => TRUE,
    };
}

############################################################
# Find the distance between 2 vertices

sub distance {

    my $vtx1 = shift;
    my $vtx2 = shift;

    my $dx = $vtx1->{ 'X' } - $vtx2->{ 'X' };
    my $dy = $vtx1->{ 'Y' } - $vtx2->{ 'Y' };

    return sqrt( ( $dx * $dx ) +  ( $dy * $dy ) );
}

############################################################
# Draw the polygons

sub draw {

    my $wireframe = $DRAW_POLYGON_WIREFRAMES ? '0' : 'mx';

    open my $FH, '>', TEMP or die sprintf "Could not open i%s - $!\n", TEMP;

    print $FH "%!PS

        <<
            /PageSize [ 640 640 ]
            /ImagingBBox null
        >> setpagedevice

        0 0 moveto
        640 0 lineto
        640 640 lineto
        0 640 lineto
        fill

        /lineFactor $LINE_FACTOR def  % polysize to linewidth
        /polysize 300 def   % initial default

        320 320 translate
        %.95 dup scale

        /prepDisc {
            /rad exch def
            /y exch def
            /x exch def
            0 5 365 {
                /a exch def
                rad a cos mul x add
                rad a sin mul y add
                a 0 eq { moveto } { lineto } ifelse
            } for
        } def
        /myYellow { v mx div dup 0 setrgbcolor } def
        /myRed { v mx div 0 0 setrgbcolor } def
        /myRed2 { v mx div .8 mul 0 0 setrgbcolor } def
        /doClip { true } def
        /myStroke {
            gsave
            doClip { clip } if
            /mx polysize lineFactor mul def
            1 1 $wireframe {    % 0 or mx
                /v exch def
                gsave
                myColor
                mx 1 add v sub setlinewidth
                stroke
                grestore
            } for
            grestore
            lastColor stroke
        } def
        /drawDisc { prepDisc myStroke } def
        /fillDisc { prepDisc fill } def
        /drawArc1{ arc } def
        /drawArc2{ arcn } def

        1 setlinejoin
    ";

    defaultDraw( $FH );

    close $FH;

    my $convert = "/usr/bin/convert";

    unless ( -f $convert ) {

        printf "Could not locate convert at $convert\n";
    }
    else {

        system( sprintf "$convert %s $DEST", TEMP );
    }
}

############################################################
# Draw the polygons with hyperbolic lines

sub defaultDraw {

    my $fh = shift;

    print $fh "

        /polysize $EXTERNAL_SHADE $LINE_FACTOR div def
        /doClip { false } def
        /myColor { myRed2 } def
        /lastColor { 0 0 0 setrgbcolor } def
        0 0 300 prepDisc myStroke

        /doClip { true } def
        /polysize $INTERNAL_SHADE $LINE_FACTOR div def
        /myColor { myRed2 } def
        /lastColor { .8 0 0 setrgbcolor } def
        0 0 300 prepDisc myStroke

        /myColor { myYellow } def
        /lastColor { 1 1 .6 setrgbcolor } def
    ";

    drawAllPolygons( $fh, $DRAW_LABELS, FALSE );

    if ( $DRAW_POLYGON && ( my $polygon = $LOOKUP->{ $DRAW_POLYGON } ) ) {
        printf $fh "/lastColor { 0 1 0 setrgbcolor } def\n";
        drawPolygon( $fh, $polygon, TRUE, FALSE );
        if ( $DRAW_POLYGON_INVDISCS ) {
            for ( my $s = 0; $s < $POLYGON_SIDES; $s++ ) {
                drawInvDisc( $fh, $polygon->{ 'INVDISCS' }->[ $s ] );
            }
        }
    }

# Custom drawings

#    if ( my $polygon = $LOOKUP->{ '1-4' } ) {
#        printf $fh "/lastColor { 0 0 1 setrgbcolor } def\n";
#        drawInvDisc( $fh, $polygon->{ 'INVDISCS' }->[ 2 ] );
#        printf $fh "/lastColor { 0 1 0 setrgbcolor } def\n";
#        drawVertex( $fh, $polygon->{ 'VERTICES' }->[ 0 ] );
#        drawVertex( $fh, $polygon->{ 'VERTICES' }->[ 1 ] );
#        drawVertex( $fh, $polygon->{ 'VERTICES' }->[ 2 ] );
#        drawVertex( $fh, $polygon->{ 'VERTICES' }->[ 3 ] );
#    }
}

############################################################
# Draw an inversion disc

sub drawInvDisc {

    my $fh = shift;
    my $inv_disc = shift;

    return if $inv_disc->{ 'INFINITE' };

    printf $fh "0 1 0 setrgbcolor\n";

    printf $fh "%f %f %f drawDisc\n",
                                $inv_disc->{ 'X' },
                                $inv_disc->{ 'Y' },
                                2;
        
    printf $fh "%f %f %f drawDisc\n",
                                $inv_disc->{ 'X' },
                                $inv_disc->{ 'Y' },
                                $inv_disc->{ 'RADIUS' };

    return;
}

############################################################
# Draw a vertex

sub drawVertex {

    my $fh = shift;
    my $vertex = shift;

    printf $fh "%f %f %f drawDisc\n",
                            $vertex->{ 'X' },
                            $vertex->{ 'Y' },
                            2;

    return;
}

############################################################
# Draw a polygon

sub drawPolygon {

    my $fh = shift;
    my $polygon = shift;
    my $label_vertices = shift || 0;
    my $draw_invdiscs = shift || 0;

    map { drawInvDisc( $fh, $_ ) } ( @{$polygon->{ 'INVDISCS' } } )
                                                    if ( $draw_invdiscs );

    my $wid = $polygon->{ 'BBOX' }->[ 2 ] - $polygon->{ 'BBOX' }->[ 0 ];
    my $dep = $polygon->{ 'BBOX' }->[ 3 ] - $polygon->{ 'BBOX' }->[ 1 ];
    my $polygon_size = max( $wid, $dep );

    printf $fh "/polysize %f def\n", $polygon_size;

    # First find the centre of the polygon
    my $centre = {
        'X' =>   0,
        'Y' =>   0,
    };

    for ( my $v = 0; $v < $POLYGON_SIDES; $v++ ) {

        $centre->{ 'X' } += $polygon->{ 'VERTICES' }->[ $v ]->{ 'X' };
        $centre->{ 'Y' } += $polygon->{ 'VERTICES' }->[ $v ]->{ 'Y' };
    }

    $centre->{ 'X' } /= $POLYGON_SIDES;
    $centre->{ 'Y' } /= $POLYGON_SIDES;

    for ( my $v = 0; $v < $POLYGON_SIDES; $v++ ) {

        my $vertex1 = $polygon->{ 'VERTICES' }->[ $v ];
        my $vertex2 = $polygon->{ 'VERTICES' }->[ ( $v + 1 ) % $POLYGON_SIDES ];

        my $invdisc = $polygon->{ 'INVDISCS' }->[ $v ];

        if ( $invdisc->{ 'INFINITE' } || $DRAW_POLYGON_LINES ) {
            
            printf $fh "%f %f moveto %f %f lineto\n",
                                    $vertex1->{ 'X' }, $vertex1->{ 'Y' },
                                    $vertex2->{ 'X' }, $vertex2->{ 'Y' };
            next;
        }

        my $ang1 = rad2deg( atan2(
                                $vertex1->{ 'Y' } - $invdisc->{ 'Y' },
                                $vertex1->{ 'X' } - $invdisc->{ 'X' } 
                            ) 
        );
        $ang1 += 360 if $ang1 < 0;

        my $ang2 = rad2deg( atan2(
                                $vertex2->{ 'Y' } - $invdisc->{ 'Y' },
                                $vertex2->{ 'X' } - $invdisc->{ 'X' } 
                            )
        );
        $ang2 += 360 if $ang2 < 0;

        my $mid_x = ( $vertex1->{ 'X' } + $vertex2->{ 'X' } ) / 2.0;
        my $mid_y = ( $vertex1->{ 'Y' } + $vertex2->{ 'Y' } ) / 2.0;

        my $val =
            ( ( $mid_x - $invdisc->{ 'X' } ) * ( $vertex1->{ 'Y' } - $invdisc->{ 'Y' } ) ) -
            ( ( $mid_y - $invdisc->{ 'Y' } ) * ( $vertex1->{ 'X' } - $invdisc->{ 'X' } ) );

        my $arc_renderer = ( $ang1 > $ang2 ) ? 2 : 1;
        $arc_renderer = 3 - $arc_renderer if ( mod( $ang1 - $ang2 ) > 180 );

        printf $fh "%f %f %f %f %f drawArc%d\n",
                        $invdisc->{ 'X' },
                        $invdisc->{ 'Y' },
                        $invdisc->{ 'RADIUS' },
                        $ang1,
                        $ang2,
                        $arc_renderer;
    }

    printf $fh "myStroke\n";

    if ( $label_vertices ) {

        printf $fh "
            gsave
            1 1 1 setrgbcolor
            /Arial-Bold findfont %d scalefont setfont
            %f %f moveto (%s-%s) dup
            stringwidth pop -0.5 mul 0 rmoveto
            show
        ", 
            $FONT_SIZE,
            $centre->{ 'X' }, $centre->{ 'Y' },
            $polygon->{ 'DEPTH' }, $polygon->{ 'ID' } || '?';

        for ( my $v = 0; $v < $POLYGON_SIDES; $v++ ) {

            my $vertex = $polygon->{ 'VERTICES' }->[ $v ];

            my $x = $centre->{ 'X' } + ( $vertex->{ 'X' } - $centre->{ 'X' } ) * .9;
            my $y = $centre->{ 'Y' } + ( $vertex->{ 'Y' } - $centre->{ 'Y' } ) * .9;
    
            printf $fh "
                $x $y moveto (%s) dup
                stringwidth pop -0.5 mul 0 rmoveto
                show
            ", $v;
        }

        printf $fh "grestore\n";
    }
}

############################################################
# Return the next depth id

sub nextDepthId {

    my $depth = shift;

    $DEPTH_IDS->{ $depth } ||= 1;

    return $DEPTH_IDS->{ $depth }++;
}

############################################################
# Find the index of the vertex in the given polygon

sub findVertexIndex {

    my $vertex = shift;
    my $polygon = shift;

    for ( my $v = 0; $v < scalar @{ $polygon->{ 'VERTICES' } }; $v++ ) {

        return $v if ( $vertex == $polygon->{ 'VERTICES' }->[ $v ] );
    }

    return -1;
}

############################################################
# Add a lookup entry for the given polygon

sub addLookup {

    my $polygon = shift;

    my $key = polygonId( $polygon );

    $LOOKUP->{ $key } = $polygon;
}

############################################################
# Return the polygon id

sub polygonId {

    my $polygon = shift;

    return sprintf "%s-%s",
                    $polygon->{ 'DEPTH' }, $polygon->{ 'ID' };
}

############################################################
# Draw the polygons

sub drawAllPolygons {

    my $fh = shift;
    my $label_vertices = shift || 0;
    my $draw_invdiscs = shift || 0;

    printf $fh "/lastColor { 0 0 1 setrgbcolor } def\n" if $DRAW_OVERLAYS;

    for my $polygon ( @{$POLYGONS} ) {

        drawPolygon( $fh, $polygon, $label_vertices, $draw_invdiscs );
    }

    return unless ( $DRAW_OVERLAYS );

    # overlays are drawn around the vertices
    printf $fh "1 setgray\n";

    my $done = {};
    for my $polygon ( @{$POLYGONS} ) {

        my $v = -1;
        for my $vertex ( @{ $polygon->{ 'VERTICES' } } ) {

            $v++;
            next unless $vertex->{ 'STATUS' } eq VERTEX_STATUS_DONE;
            next if $done->{ $vertex };

            my $new_vertices = {};
            for my $polygon_id ( keys %{$vertex->{ 'POLYGONS' }} ) {

                my $centroid = findPolygonCentroid( $polygon_id );
                my $rads = atan2(
                                $centroid->{ 'Y' } - $vertex->{ 'Y' },
                                $centroid->{ 'X' } - $vertex->{ 'X' } 
                          ); 
                my $ang = ( rad2deg( $rads ) + 360 ) % 360;
                my $key = sprintf "%03d", $ang;

                $new_vertices->{ $key } = $centroid;
            }
        
            $done->{ $vertex } = 1;

            my $op = 'moveto';
            for my $ang ( sort keys %{ $new_vertices } ) {

                printf $fh "%f %f $op ",
                                $new_vertices->{ $ang }->{ 'X' },
                                $new_vertices->{ $ang }->{ 'Y' };
                $op = 'lineto';
            }

            print $fh "closepath stroke\n";
        }
    }
}

############################################################
# Change any occurance of the first vertex to the second

sub findPolygonCentroid {

    my $polygon_id = shift;

    my $polygon = $LOOKUP->{ $polygon_id };

    unless ( $polygon->{ 'CENTROID' } ) {

        my $centroid = { 'X' => 0, 'Y' => 0 };

        my $c = 0;
        for my $vertex ( @{ $polygon->{ 'VERTICES' } } ) {

            $c++;
            $centroid->{ 'X' } += $vertex->{ 'X' };
            $centroid->{ 'Y' } += $vertex->{ 'Y' };
        }

        $centroid->{ 'X' } /= $c;
        $centroid->{ 'Y' } /= $c;

        $polygon->{ 'CENTROID' } = $centroid;
    }

    return $polygon->{ 'CENTROID' };
}

############################################################
# Change any occurance of the first vertex to the second

sub updateVertex {

    my $from_vertex = shift;
    my $to_vertex = shift;

    for my $polygon ( @{$POLYGONS} ) {
    
        for ( my $v = 0; $v < scalar @{ $polygon->{ 'VERTICES' } }; $v++ ) {

            if ( $polygon->{ 'VERTICES' }->[ $v ] == $from_vertex ) {
            
                $polygon->{ 'VERTICES' }->[ $v ] = $to_vertex;
            }
        }
    }
}

############################################################
# Display details about a vertex

sub explainVertex {

    my $polygon = $LOOKUP->{ $DEBUG_EXPLAIN_VERTEX_POLYGON_ID };
    help( "No such polygon: $DEBUG_EXPLAIN_VERTEX_POLYGON_ID" ) unless $polygon;

    my $vertex = $polygon->{ 'VERTICES' }->[ $DEBUG_EXPLAIN_VERTEX_NUMBER ];
    help( "No such vertex: $DEBUG_EXPLAIN_VERTEX_NUMBER" ) unless $vertex;

    printf "Vertex $DEBUG_EXPLAIN_VERTEX_POLYGON_ID/$DEBUG_EXPLAIN_VERTEX_NUMBER\n";
    printf "    status   : %s\n", vertexStatus( $vertex );
    printf "    last     : %s\n", $vertex->{ 'LAST_POLYGON' } || '?';
    printf "    pt       : %f, %f\n", $vertex->{ 'X' }, $vertex->{ 'Y' };
    printf "    polygons : ";

    my $sep = '';
    for my $key ( keys %{ $vertex->{ 'POLYGONS' } } ) { 

        printf "$sep$key";
        $sep=', ';
    }
    printf "\n";
}

############################################################
# Help doco

sub help {

    printf "\n@_\n" if @_;

    printf "\n$0 [-a <sz>] [-c cfg] [-d] [-D <id>] [-e <sz>] [-i <sz>]
                 [-I] [-f <r>] [-h] [-l] [-L] [-O] [-o <dest>] [-p <#>]
                 [-r degs] [-s <#>] [-t] [-v] [-x <#>] [-z <id>]

    where
        -a  font_size
        -c  tiling configuration
        -d  dump full polygon data
        -D  highlight the given polygon
        -e  size of external shading (default 250)
        -f  ratio of polygon size to line width (default .5)
        -h  this help info
        -i  size of internal shading (default 1500)
        -I  draw invdiscs when hightighting polygons
        -l  draw polygon labels
        -L  draw straight lines only
        -R  rotational offset (in degs)
        -o  destination file
        -O  draw overlays
        -p  number of polygons around each vertex
        -s  number of polygon sides
        -t  draw triangular image
        -v  dump vertex details
        -x  max iteration depth (default 0)
        -X  explain a vertex (eg -X '6-61/3')
        -W  wire-frame presentation
        -z  stop after creating this polygon

    note:
        one of -c or both of -p and -s must be supplied
    \n";

    exit;
}
