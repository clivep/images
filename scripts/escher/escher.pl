#!/usr/bin/perl

use warnings;
use strict;

use Getopt::Std;
use Math::Trig;
use Data::Dumper;
use Readonly;

my Readonly    $TRIANGLE_STATUS_READY       =   "ready";
my Readonly    $TRIANGLE_STATUS_DONE        =   "done";

my Readonly    $TRIANGLE_TYPE_APEX_UP       =   "apex_up";
my Readonly    $TRIANGLE_TYPE_APEX_DOWN     =   "apex_down";

############################################################
# The global image description

my $IMAGE = {

    'TAPES'         =>  0,
    'TAPE_PAIRS'    =>  0,
    'SIZE'          =>  0,

    'TEMP_PS'       =>  '',
    'KEEP_TEMP'     =>  '',
    'DEST_IMAGE'    =>  '',

    'VERTICES'      =>  {},
    'TRIANGLES'     =>  {},
    'TRAPESIUMS'    =>  {},

    'LOOKUP'        =>  {}, # row-col =>    triangle tids 

    'IMG_WID'       =>  1000,
    'IMG_DEP'       =>  1000,
    'XOFF'          =>  -500,
    'YOFF'          =>  -500,

    'INNER_RAD'     =>  100,
    'OUTER_RAD'     =>  450,
};

############################################################
# Color processing

my $INIT_COLSET = {
    'body'      => '0 0 1 setrgbcolor',
    'spots'     => '0 1 0 setrgbcolor',
    'wings'     => '1 1 0 setrgbcolor',
    'unused'	=> '1 0 0 setrgbcolor',
};

############################################################
# Layout for a trapesium

my $TRAP_TEMPLATE = {

    "wings" =>  [
        [ newVertex( 1.0 / 8.0, sqrt( 3.0 ) / -8.0 ),       'mv' ],
        [ newVertex( 7.0 / 8.0, sqrt( 3.0 ) / -8.0 ),       'ln' ],
        [ newVertex( 6.0 / 8.0, sqrt( 3.0 ) * 2.0 / -8.0 ), 'ln' ],
        [ newVertex( 2.0 / 8.0, sqrt( 3.0 ) * 2.0 / -8.0 ), 'ln fill' ],
    ],
    "body" =>  [
        [ newVertex( 17.0 / 40.0, 0 ),                  'mv' ],
        [ newVertex( 17.0 / 40.0, sqrt( 3.0 ) / -4.0 ), 'ln' ],
        [ newVertex( 23.0 / 40.0, sqrt( 3.0 ) / -4.0 ), 'ln' ],
        [ newVertex( 23.0 / 40.0, 0 ),                  'ln fill' ],
    ],
    "spots" =>  [
        [ newVertex( 3.0 / 16.0, 1.0 / -20.0 ),                         'mv' ],
        [ newVertex( 3.0 / 16.0, 1.0 / -20.0 + sqrt( 3.0 ) / -10.0 ),   'ln' ],
        [ newVertex( 6.0 / 16.0, 1.0 / -20.0 + sqrt( 3.0 ) / -10.0 ),   'ln' ],
        [ newVertex( 6.0 / 16.0, 1.0 / -20.0 ),                         'ln fill' ],
        [ newVertex( 13.0 / 16.0, 1.0 / -20.0 ),                        'mv' ],
        [ newVertex( 13.0 / 16.0, 1.0 / -20.0 + sqrt( 3.0 ) / -10.0 ),  'ln' ],
        [ newVertex( 10.0 / 16.0, 1.0 / -20.0 + sqrt( 3.0 ) / -10.0 ),  'ln' ],
        [ newVertex( 10.0 / 16.0, 1.0 / -20.0 ),                        'ln fill' ],
    ],
};

############################################################
# Process any command line options

my %options;
getopts( 'hk:L:Mo:S:T:', \%options );

usage() if $options{ 'h' };

$IMAGE->{ 'KEEP_TEMP' } = $options{ 'k' };
$IMAGE->{ 'TEMP_PS' } = $options{ 'k' } || 'escher.ps';
$IMAGE->{ 'DEST_IMAGE' } = $options{ 'o' } || 'escher.png';

$IMAGE->{ 'TAPE_PAIRS' } = $options{ 'L' } || 5;
$IMAGE->{ 'SIZE' } = $options{ 'S' } || 50;
$IMAGE->{ 'TAPES' } = $options{ 'T' } || 4;

$IMAGE->{ 'MAP_TO_DISC' } = $options{ 'M' };

open my $fh, '>', $IMAGE->{ 'TEMP_PS' } || help( "Could not open " . $IMAGE->{ 'TEMP_PS' } );

############################################################
# Process the image tapes

createVertices();
createTriangles();
assignColsets();
writeHeaders( $fh );
drawImage( $fh );

close $fh;

system( sprintf "convert %s %s", $IMAGE->{ 'TEMP_PS' }, $IMAGE->{ 'DEST_IMAGE' } );

unlink $IMAGE->{ 'TEMP_PS' } unless $IMAGE->{ 'KEEP_TEMP' };

exit;

############################################################
# Rotate the colors for the 6-fold transformation

sub colorTransform6 {

    my $color_set = shift;
    my $iters = shift || 1;

    my $new_set = { %{ $color_set } };

    while ( $iters > 0 ) {

        $new_set = {
            'body'      => $new_set->{ 'unused' },
            'spots'     => $new_set->{ 'spots' },
            'wings'     => $new_set->{ 'body' },
            'unused'    => $new_set->{ 'wings' },
        };

        $iters--;
    }

    return $new_set;
}

############################################################
# Rotate the colors for the 3-fold transformation

sub colorTransform3 {

    my $color_set = shift;
    my $iters = shift || 1;

    my $new_set = { %{ $color_set } };

    while ( $iters > 0 ) {

        $new_set = {
            'body'      => $new_set->{ 'spots' },
            'spots'     => $new_set->{ 'wings' },
            'wings'     => $new_set->{ 'body' },
            'unused'    => $new_set->{ 'unused' },
        };

        $iters--;
    }

    return $new_set;
}

############################################################
# Rotate the colors for the 2-fold transformation

sub colorTransform2 {

    my $color_set = shift;
    my $iters = shift || 1;

    my $new_set = { %{ $color_set } };

    while ( $iters > 0 ) {

        $new_set = {
            'body'      => $new_set->{ 'spots' },
            'spots'     => $new_set->{ 'body' },
            'wings'     => $new_set->{ 'unused' },
            'unused'    => $new_set->{ 'wings' },
        };

        $iters--;
    }

    return $new_set;
}

############################################################
# Return the vertex id given a col and row

sub vertexId {

    return sprintf "V-%d-%d", @_;
}

############################################################
# Wrap the tape coords around a disc

sub mapCoords {

    my $x = shift;
    my $y = shift;

    return $x, $y unless $IMAGE->{ 'MAP_TO_DISC' };

    my $max_x = 2 * $IMAGE->{ 'TAPE_PAIRS' } * $IMAGE->{ 'SIZE' };
    my $max_y = $IMAGE->{ 'TAPES' } * $IMAGE->{ 'SIZE' } * sqrt( 3.0 );

    my $ang = 360 * $x / $max_x;
    my $rad = ( ( $IMAGE->{ 'OUTER_RAD' } * $y ) + ( $IMAGE->{ 'INNER_RAD' } * ( $max_y - $y ) ) ) / $max_y;

    return $rad * cos( deg2rad( $ang ) ), $rad * sin( deg2rad( $ang ) );
}

############################################################
# Return the vertex coords

sub vertexCoords {

    my $vid = shift;

    my $x = $IMAGE->{ 'VERTICES' }->{ $vid }->{ 'X' };
    my $y = $IMAGE->{ 'VERTICES' }->{ $vid }->{ 'Y' };

    return ( $x, $y );
}

############################################################
# Return a new vertex object

sub newVertex {

    return {
        'X' =>  shift,
        'Y' =>  shift,
    };
}

############################################################
# Setup the image vertices

sub createVertices {

    for ( my $r = 0; $r < 1 + $IMAGE->{ 'TAPES' }; $r++ ) {

        my $offset = ( $r % 2 ) * $IMAGE->{ 'SIZE' };

        for ( my $c = 0; $c < ( 1 + $IMAGE->{ 'TAPE_PAIRS' } ) * 2; $c++ ) {

            my $vid = vertexId( $c, $r );

            $IMAGE->{ 'VERTICES' }->{ $vid } = newVertex(
                                                ( ( $c + ( $r % 2 ) ) * $IMAGE->{ 'SIZE' } ) - $offset, 
                                                $r * sqrt( 3.0 ) * $IMAGE->{ 'SIZE' }
            );
        }
    }

    return;
}

############################################################
# Convenience function to rmove a point

sub rmv {

    my $pt = shift;
    my $x = shift;
    my $y = shift;

    $pt->[ 0 ] += $x;
    $pt->[ 1 ] += $y;

    return ( $pt->[ 0 ], $pt->[ 1 ] );
}

############################################################
# Scale, offset and rotate the given vertex

sub vertexTransform {

    my $vertex = shift;
    my $scale = shift;
    my $ang = shift;
    my $xoff = shift;
    my $yoff = shift;

    my $x = $vertex->{ 'X' };
    my $y = $vertex->{ 'Y' };
    
    return {
        'X' =>  $x * $scale * cos( deg2rad( $ang ) ) + $y * $scale * sin( deg2rad( $ang ) ) + $xoff,
        'Y' =>  $y * $scale * cos( deg2rad( $ang ) ) - $x * $scale * sin( deg2rad( $ang ) ) + $yoff,
    }
}

############################################################
# Draw a trapesium at the given location and rotation

sub drawTrapesium {

    my $fh = shift;
    my $x = shift;
    my $y = shift;
    my $ang = shift;
    my $colset = shift;

    my $sz = $IMAGE->{ 'SIZE' } * 4.0 / 3.0;

    for my $component ( "wings", "spots", "body" ) {    # ( keys %{ $TRAP_TEMPLATE } ) {

        printf $fh "%% %s\n%s\n", $component, $colset->{ $component };

        for my $action ( @{ $TRAP_TEMPLATE->{ $component } } ) {

            my $vertex = vertexTransform( $action->[ 0 ], $sz, -1 * $ang, $x, $y );
            printf $fh "%f %f %s ", mapCoords( $vertex->{ 'X' }, $vertex->{ 'Y' } ), $action->[ 1 ];
        }
    }

    return;
}

############################################################
# Draw the image triangles

sub drawTriangles {

    my $fh = shift;

    for my $tid ( keys %{ $IMAGE->{ 'TRIANGLES' } } ) {

        my $triangle = $IMAGE->{ 'TRIANGLES' }->{ $tid };
        my $vertices = $triangle->{ 'VERTICES' };

        printf $fh "%f %f mv %f %f ln %f %f ln %f %f ln stroke\n",
                        mapCoords( vertexCoords( $vertices->[ 0 ] ) ),
                        mapCoords( vertexCoords( $vertices->[ 1 ] ) ),
                        mapCoords( vertexCoords( $vertices->[ 2 ] ) ),
                        mapCoords( vertexCoords( $vertices->[ 0 ] ) ),
        ;

        if ( $triangle->{ 'TYPE' } eq $TRIANGLE_TYPE_APEX_UP ) {
         
            drawTrapesium( $fh, vertexCoords( $vertices->[ 0 ] ),  60, $triangle->{ 'COLSETS' }->[ 1 ] );
            drawTrapesium( $fh, vertexCoords( $vertices->[ 1 ] ), 300, $triangle->{ 'COLSETS' }->[ 0 ] );
            drawTrapesium( $fh, vertexCoords( $vertices->[ 2 ] ), 180, $triangle->{ 'COLSETS' }->[ 2 ] );
        }
        else {

            drawTrapesium( $fh, vertexCoords( $vertices->[ 0 ] ),   0, $triangle->{ 'COLSETS' }->[ 0 ] );
            drawTrapesium( $fh, vertexCoords( $vertices->[ 1 ] ), 120, $triangle->{ 'COLSETS' }->[ 1 ] );
            drawTrapesium( $fh, vertexCoords( $vertices->[ 2 ] ), 249, $triangle->{ 'COLSETS' }->[ 2 ] );
        }
    }
}

############################################################
# Draw the image

sub drawImage {

    my $fh = shift;

    drawTriangles( $fh );

    return;
}

############################################################
# Add the figure to the IMAGE object - if not already there

sub addTriangle {

    my $type = shift;
    my $vlist = shift;

    my $tid = sprintf "T-%s-%s-%s", sort( @{ $vlist } );

    unless ( $IMAGE->{ 'TRIANGLES' }->{ $tid } ) {

        $IMAGE->{ 'TRIANGLES' }->{ $tid } = {
            'TYPE'          =>  $type,
            'STATUS'        =>  $TRIANGLE_STATUS_READY,
            'VERTICES'      =>  $vlist,
        };
    }

    return $tid;
}

############################################################
# Process the image tapes

sub getTriangleColsets {

    my $xpos = shift;
    my $ypos = shift;

    if ( $xpos == 0 ) {
    
        if ( $ypos == 0 ) {

            return [
                $INIT_COLSET,
                colorTransform3( $INIT_COLSET ),
                colorTransform3( $INIT_COLSET, 2 ),
            ];
        }
        elsif ( $ypos % 2 ) {

            my $triangle = lookupTriangle( sprintf "%d-%d", $xpos, $ypos - 1 );
            my $colset = colorTransform6( $triangle->{ 'COLSETS' }->[ 0 ] ); 
            return [
                colorTransform3( $colset, 2 ),
                $colset,
                colorTransform3( $colset, 1 ),
            ];
        }
        else {

            my $triangle = lookupTriangle( sprintf "%d-%d", $xpos, $ypos - 1 );
            my $colset = colorTransform6( $triangle->{ 'COLSETS' }->[ 0 ], 3 ); 
            return [
                colorTransform3( $colset, 2 ),
                $colset,
                colorTransform3( $colset, 1 ),
            ];
        }
    }

    if ( $ypos % 2 ) {
    
        if ( $xpos % 2 ) {

            my $triangle = lookupTriangle( sprintf "%d-%d", $xpos - 1, $ypos );
            my $colset = colorTransform6( $triangle->{ 'COLSETS' }->[ 0 ] ); 
            return [
                $colset,
                colorTransform3( $colset ),
                colorTransform3( $colset, 2 ),
            ];
        }
        else {

            my $triangle = lookupTriangle( sprintf "%d-%d", $xpos, $ypos - 1 );
            my $colset = colorTransform6( $triangle->{ 'COLSETS' }->[ 0 ] ); 
            return [
                colorTransform3( $colset, 2 ),
                $colset,
                colorTransform3( $colset ),
            ];
        }
    }

    if ( $xpos % 2 ) {

        my $triangle = lookupTriangle( sprintf "%d-%d", $xpos - 1, $ypos );
        my $colset = colorTransform6( $triangle->{ 'COLSETS' }->[ 2 ] ); 
        return [
            $colset,
            colorTransform3( $colset ),
            colorTransform3( $colset, 2 ),
        ];
    }
    else {

        my $triangle = lookupTriangle( sprintf "%d-%d", $xpos - 1, $ypos );
        my $colset = colorTransform6( $triangle->{ 'COLSETS' }->[ 0 ] ); 
        return [
            $colset,
            colorTransform3( $colset, 1 ),
            colorTransform3( $colset, 2 ),
        ];
    }
}

############################################################
# Find a triangle by x/y location

sub lookupTriangle {

    my $id = shift;

    my $tid = $IMAGE->{ 'LOOKUP' }->{ $id };
    printf "missing lookup '$id': $tid\n" unless $tid;

    return $IMAGE->{ 'TRIANGLES' }->{ $tid };
}

############################################################
# Assign colsets to the triangles

sub assignColsets {

    for ( my $tape = 0 ; $tape < $IMAGE->{ 'TAPES' }; $tape++ ) {

        for ( my $pair = 0 ; $pair < $IMAGE->{ 'TAPE_PAIRS' }; $pair++ ) {

            my $triangle = lookupTriangle( sprintf "%d-%d", $pair * 2, $tape );
            $triangle->{ 'COLSETS' } = getTriangleColsets( $pair * 2, $tape );

            $triangle = lookupTriangle( sprintf "%d-%d", 1 + ( $pair * 2 ), $tape );
            $triangle->{ 'COLSETS' } = getTriangleColsets( 1 + ( $pair * 2 ), $tape );
        }
    }

    return;
}

############################################################
# Create triangles in the tapes

sub createTriangles {

    for ( my $tape = 0 ; $tape < $IMAGE->{ 'TAPES' }; $tape++ ) {

        for ( my $pair = 0 ; $pair < $IMAGE->{ 'TAPE_PAIRS' }; $pair++ ) {

            my $t = $tape % 2;

            my $id = sprintf "%d-%d", $pair * 2, $tape;

            $IMAGE->{ 'LOOKUP' }->{ $id } = addTriangle(

                    $t ? $TRIANGLE_TYPE_APEX_UP : $TRIANGLE_TYPE_APEX_DOWN,
                    [
                        vertexId( ( $pair * 2 ) + 0, $tape + 1 - $t ),                   
                        vertexId( ( $pair * 2 ) + 1, $tape + $t ),                   
                        vertexId( ( $pair * 2 ) + 2, $tape + 1 - $t ),                   
                    ],
            );

            $id = sprintf "%d-%d", 1 + ( $pair * 2 ), $tape;

            $IMAGE->{ 'LOOKUP' }->{ $id } = addTriangle(

                    $t ? $TRIANGLE_TYPE_APEX_DOWN : $TRIANGLE_TYPE_APEX_UP,
                    [
                        vertexId( ( $pair * 2 ) + 1, $tape + $t ),                   
                        vertexId( ( $pair * 2 ) + 2, $tape + 1 - $t ),                   
                        vertexId( ( $pair * 2 ) + 3, $tape + $t ),                   
                    ],
            );
        }
    }
}

############################################################
# Write the image headers

sub writeHeaders {

    printf $fh "%%!PS\n%%%%BoundingBox: 0 0 %f %f\n%%%%EndComments\n

        <<
            /PageSize [ %f %f ]
            /ImagingBBox null
        >> setpagedevice

        /mv { moveto } bind def
        /ln { lineto } bind def
        /rln { rlineto } bind def

        /Arial-Bold findfont 15 scalefont setfont

        /col1 { 1 0 0 setrgbcolor } def
        /col2 { 0 1 0 setrgbcolor } def
        /col3 { 0 0 1 setrgbcolor } def
        /col4 { 0 1 1 setrgbcolor } def
        /setcol {
            /c exch def
            c 1 eq { col1 } if
            c 2 eq { col2 } if
            c 3 eq { col3 } if
            c 4 eq { col3 } if
        } def

        0 0 mv %f 0 ln %f %f ln 0 %f ln fill
	
        1 1 0 setrgbcolor
    ",
        $IMAGE->{ 'IMG_WID' }, $IMAGE->{ 'IMG_DEP' },
        $IMAGE->{ 'IMG_WID' }, $IMAGE->{ 'IMG_DEP' },
        $IMAGE->{ 'IMG_WID' }, $IMAGE->{ 'IMG_WID' },
        $IMAGE->{ 'IMG_DEP' }, $IMAGE->{ 'IMG_DEP' },
    ;

    printf $fh "%f %f translate\n", $IMAGE->{ 'MAP_TO_DISC' } ?
            ( $IMAGE->{ 'IMG_WID' } / 2.0, $IMAGE->{ 'IMG_DEP' } / 2.0 )
            :
            ( $IMAGE->{ 'XOFF' } + $IMAGE->{ 'IMG_WID' } / 2.0, $IMAGE->{ 'YOFF' } + $IMAGE->{ 'IMG_DEP' } / 2.0 )
    ;
}

############################################################
# Some usage info

sub usage {

    print "\n@_\n" if @_;

    printf "\n$0 " .
        "[-h] [-k <tmp>] [-L <len>] [-M] [-o <dest>] [-S <sz>] [-T <tapes>]

    where
        -h          this help message
        -k <tmp>    temp file for the postscript (retained)
        -o <dest>   location for image (default butterflies.png)

    image control params

        -L <len>    number of triangle pairs in a tape
        -M          map the tapes to a disc
        -T <tapes>  number of rows in the image (default 5)
        -S <size>   size of the hexagon sides (defautl 35)
    \n";

    exit 0;
}

sub showColset {

    my $colset = shift;

    printf "body: %s\nspots: %s\nwings: %s\n", 
                $colset->{ 'body' },    
                $colset->{ 'spots' },    
                $colset->{ 'wings' },    
    ;
}

__DATA__

tape/trap layout:
         ________   y
       /\\ 0 /  /   2
      / 0\\-- 2/
     /1\__\\1\/
    /__/_2_\\/      
    ________        1
    \0  / 2//\
     \--  // 0\
      \1\// \__\
       \//1_/_2_\   0

    ^   ^   ^   ^ 
x   0   1   2   3
