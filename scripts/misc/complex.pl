#!/usr/bin/perl

use warnings;
use strict;

############################################################
#
#   complex.pl
#
#   Discs on the complex plane
#
############################################################

use Math::Trig;
use Getopt::Std;

use Data::Dumper;

############################################################
# Setup the environment

my $IMAGE = {};

$IMAGE->{ 'DISCS' } = [];

############################################################
# Check the runline options

my %options;
getopts( 'hD:I:F:K:W:', \%options );

help() if $options{ 'h' };

help( "Missing <rad1> and <rad2> params" ) unless $ARGV[ 0 ];
help( "Missing <rad2> param" ) unless $ARGV[ 1 ];

$IMAGE->{ 'RAD1' } = $ARGV[ 0 ];
$IMAGE->{ 'RAD2' } = $ARGV[ 1 ];

$IMAGE->{ 'TEMP_PS' } = $options{ 'K' } || 'complex.ps';
$IMAGE->{ 'DEST' } = $options{ 'F' } || 'complex.png';

$IMAGE->{ 'INIT_RAD' } = $options{ 'I' } || 0.5;

$IMAGE->{ 'SCALE' } = $options{ 'S' } || 30;
$IMAGE->{ 'DEPTH' } = $options{ 'W' } || 1000;
$IMAGE->{ 'WIDTH' } = $options{ 'D' } || 1000;

############################################################
# Process the image

processImage();
drawImage();

exit;

############################################################
# Add a disc to the image list

sub addDisc {

    my $x = shift;
    my $y = shift;
    my $rad = shift;

    push @{ $IMAGE->{ 'DISCS' } }, {

        'X'     =>  $x,
        'Y'     =>  $y,
        'RAD'   =>  $rad,
    };

    return;
}

############################################################
# Process the image

sub processImage {

    my $r  = $IMAGE->{ 'INIT_RAD' };
    my $r1 = $IMAGE->{ 'RAD1' };
    my $r2 = $IMAGE->{ 'RAD2' };

    addDisc( 1, 0, $r );

    my $d1 = $r1 / $r;

    my $a1 = ( $r + $d1 * $r );
    my $v1= ( ( $d1 * $d1 ) + 1.0 - ( $a1 * $a1 ) ) / ( 2 * $d1 );

    help( "$r1 is not a valid rad1 value" ) if ( $v1 > 1 );

    my $ang1 = acos( $v1 );
    addDisc( $d1 * cos( $ang1 ), $d1 * sin( $ang1 ), $r1 );
    
    my $d2 = $r2 / $r;

    my $a2 = ( $r1 + $r2 );
    my $v2 = ( ( $d1 * $d1 ) + ( $d2 * $d2 ) - ( $a2 * $a2 ) ) / ( 2 * $d1 * $d2 );

    help( "$r2 is not a valid rad2 value" ) if ( $v2 > 1 );

    my $ang2 = $ang1 + acos( $v2 );
    addDisc( $d2 * cos( $ang2 ), $d2 * sin( $ang2 ), $r2 );
}

############################################################
# Write the image header

sub writeImageHeader {

    my $fh = shift;

    printf $fh "%%!PS\n%%%%BoundingBox: 0 0 %f %f\n%%%%EndComments\n

        /imgw %f def
        /imgd %f def

        <<
            /PageSize [ imgw imgd ]
            /ImagingBBox null
        >> setpagedevice

        /mv { moveto } bind def
        /ln { lineto } bind def

        0 0 mv imgw 0 ln imgw imgd ln 0 imgd ln fill

        imgw 2 div imgd 2 div translate

        1 setlinejoin
        1 1 0 setrgbcolor
    ",
        $IMAGE->{ 'WIDTH' }, $IMAGE->{ 'DEPTH' },
        $IMAGE->{ 'WIDTH' }, $IMAGE->{ 'DEPTH' },
    ;

    return;
}

############################################################
# Write the image details

sub writeImageDetail {

    my $fh = shift;

    printf $fh "
        0 0 mv 0 10 ln
        0 0 mv 0 -10 ln
        0 0 mv 10 0 ln
        0 0 mv -10 0 ln
        stroke
    ";

    for my $disc ( @{ $IMAGE->{ 'DISCS' } } ) {

        for( my $a = 0; $a < 360; $a += 2 ) {

            printf $fh "%f %f %s ",
                $IMAGE->{ 'SCALE' } * ( $disc->{ 'X' } + ( $disc->{ 'RAD' } * cos( deg2rad( $a ) ) ) ),
                $IMAGE->{ 'SCALE' } * ( $disc->{ 'Y' } + ( $disc->{ 'RAD' } * sin( deg2rad( $a ) ) ) ),
                $a ? 'ln' : 'mv',
            ;
        }

        printf $fh "stroke\n";
    }

    return;
}

############################################################
# Draw the image

sub drawImage {

    open my $fh, '>', $IMAGE->{ 'TEMP_PS' } or do {
        printf "Could not open %s: $!\n", $IMAGE->{ 'TEMP_PS' };
        exit -1;
    };

    writeImageHeader( $fh );
    writeImageDetail( $fh );

    close $fh;

    my $convert = "/usr/local/bin/convert";
    $convert = "/usr/bin/convert" unless ( -f $convert );
    $convert = "/Applications/ImageMagick/ImageMagick-6.6.5/bin/convert" unless ( -f $convert );

    unless ( -f $convert ) {

        printf "Could not locate convert at $convert\n";
    }
    else {

        unlink $IMAGE->{ 'DEST' };
        my $cmd = sprintf "$convert '%s' '%s'", $IMAGE->{ 'TEMP_PS' }, $IMAGE->{ 'DEST' };
        system( $cmd );
    }

    unlink $IMAGE->{ 'TEMP_PS' } unless $options{ 'K' };

    return;
}

############################################################
# Help doco

sub help {

    print "\n@_\n" if @_;

    printf "\n$0 " .
                "[-h] [-D <depth>] [-I >rad>] [-F <file>] [-K <file>] [-S <scale>] [-W <width>] <rad1> <rad2>

        where
            <rad1>      radius of the first disc (required)
            <rad2>      radius of the second disc (required)

            -h          this help message

            -D <depth>  image depth (default 1000)
            -I <rad>    radius of the initial disc (default 0.5)
            -F <file>   output file (default 'complex.png')
            -K <file>   temp ps file to retain
            -S <scale>  drawing scale (default 30)
            -W <width>  image width (default 1000)

    \n";

    exit;
}
