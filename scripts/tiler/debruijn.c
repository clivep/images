#include <stdio.h>
#include <stdlib.h>

#include "tiling-polygons.h"
#include "debruijn.h"

/*******************************************************************************
 *
 * debruijn.c
 *
 *  Functions for generating tilings
 *
 ******************************************************************************/

#define DEFAULT_PDF_PATH    "debruijn.pdf"
#define RENDER_SCRIPT       "debruijn-draw.pl"

static _list   *templates;

/*************************************************
 * Initialise the plugin
 ************************************************/

void initialisePlugin(
    _logging_level logging_level 
) {
    
    _polygon *template;
    _list_item  *item;

    initialiseLogging( NULL, logging_level );

    templates = initialiseList( releasePolygon );

    // Must be added in the order specified in the enum _template_type
    addToList( initialiseWideTemplate(), templates );
    addToList( initialiseThinTemplate(), templates );

    for( item = templates->first; item != NULL; item = item->next ) {

        template = item->data;

        template->base_angle = atan2 (
                template->vertices[ 1 ]->y - template->vertices[ 0 ]->y,
                template->vertices[ 1 ]->x - template->vertices[ 0 ]->x
        );
    }
}

/*************************************************
 * Cleanup the plugin
 ************************************************/

void cleanupPlugin() {

    releaseList( templates );
}

/*************************************************
 * Return the number of edges for the type
 ************************************************/

short polygonTypeEdges(
    _template_type type 
) {
    switch( type ) {

        case TEMPLATE_TYPE_THIN:
        case TEMPLATE_TYPE_WIDE:
            return 4;
    }

    return -1;
}

/*************************************************
 * Initialise the Thin template polygon
 ************************************************/

_polygon *initialiseThinTemplate() {

    _polygon    *template;

    double      ang = PI / 5.0;
    
    template = newPolygon( TEMPLATE_TYPE_THIN );

    template->sides[ 0 ] = newSide( SIDE_TYPE_AB, template );
    template->sides[ 1 ] = newSide( SIDE_TYPE_BA, template );
    template->sides[ 2 ] = newSide( SIDE_TYPE_CD, template );
    template->sides[ 3 ] = newSide( SIDE_TYPE_DC, template );

    template->angles[ 0 ] = ang;
    template->angles[ 1 ] = 4.0 * ang;
    template->angles[ 2 ] = ang;
    template->angles[ 3 ] = 4.0 * ang;

    template->vertices[ 0 ] = newVertex( 0.0, 0.0, template->angles[ 0 ] );
    template->vertices[ 1 ] = newVertex( cos( ang * 2.0 ), sin( ang * 2.0 ),
                                                        template->angles[ 1 ] );
    template->vertices[ 2 ] = newVertex( 0.0, 2.0 * sin( ang * 2.0 ),
                                                        template->angles[ 2 ] );
    template->vertices[ 3 ] = newVertex( -1.0 * cos( ang * 2.0 ), sin( ang * 2.0 ),
                                                        template->angles[ 3 ] );

    offsetPolygon( template, 0, 0 );

    logMsg( "Thin template @ %lx", (long)template );

    return template;
}

/*************************************************
 * Initialise the Wide template polygon
 ************************************************/

_polygon *initialiseWideTemplate() {

    _polygon    *template;

    double      ang = PI / 5.0;
    
    template = newPolygon( TEMPLATE_TYPE_WIDE );

    template->sides[ 0 ] = newSide( SIDE_TYPE_CD, template );
    template->sides[ 1 ] = newSide( SIDE_TYPE_DC, template );
    template->sides[ 2 ] = newSide( SIDE_TYPE_BA, template );
    template->sides[ 3 ] = newSide( SIDE_TYPE_AB, template );

    template->angles[ 0 ] = 3.0 * ang;
    template->angles[ 1 ] = 2.0 * ang;
    template->angles[ 2 ] = 3.0 * ang;
    template->angles[ 3 ] = 2.0 * ang;

    template->vertices[ 0 ] = newVertex( 0.0, 0.0, template->angles[ 0 ] );
    template->vertices[ 1 ] = newVertex( cos( ang ),
                                sin( ang ), template->angles[ 1 ] );
    template->vertices[ 2 ] = newVertex( 0.0, 2.0 * sin( ang ),
                                                        template->angles[ 2 ] );
    template->vertices[ 3 ] = newVertex( -1.0 * cos( ang ),
                                sin( ang ), template->angles[ 3 ] );

    offsetPolygon( template, 0, 0 );

    logMsg( "Wide template @ %lx", (long)template );

    return template;
}

/*************************************************
 * Return the char id for the polygon type
 ************************************************/

char polygonTypeChar(
    _polygon *polygon
) {
    switch( polygon->type ) {

        case TEMPLATE_TYPE_THIN:
            return 'T';

        case TEMPLATE_TYPE_WIDE:
            return 'W';
    }

    return '?';
}

/*************************************************
 * Return a description of the given side type
 ************************************************/

char *getSideTypeDescription(
    _side_type   pos
) {

    switch( pos ) {

        case SIDE_TYPE_AB:
            return "SIDE A->B";

        case SIDE_TYPE_BA:
            return "SIDE B->A";

        case SIDE_TYPE_CD:
            return "SIDE C->D";

        case SIDE_TYPE_DC:
            return "SIDE D->C";

        default:
            break;
    }

    return "?";
}

/*************************************************
 * Test if the sides are allowed to join
 ************************************************/

short checkForValidSides(
    _polygon    *polygon1,      // the existing polygon
    short       edge1,
    _polygon    *polygon2,      // the candidate (template) polygon
    short       edge2
) {
    short       side1_is_ab_type;
    short       side2_is_ab_type;
    _side_type  side_type1;
    _side_type  side_type2;

    // Check for a valid combination of sides
    side_type1 = polygon1->sides[ edge1 ]->type;
    side_type2 = polygon2->sides[ edge2 ]->type;

    logMsg( "Checking sides %lx/%d with %lx/%d",
                                        (long)polygon1, edge1,
                                        (long)polygon2, edge2 
    );

    side1_is_ab_type = ( side_type1 == SIDE_TYPE_AB ) || ( side_type1 == SIDE_TYPE_BA );
    side2_is_ab_type = ( side_type2 == SIDE_TYPE_AB ) || ( side_type2 == SIDE_TYPE_BA );

    if ( side1_is_ab_type == side2_is_ab_type )
        return OK;

    logMsg( "rejected - bad side combination %lx: (%c/%d) and %lx: (%c/%d)", 
                                        (long)polygon1, polygonTypeChar( polygon1 ), edge1,
                                        (long)polygon2, polygonTypeChar( polygon2 ), edge2
    );

    return NOK;
}

/*************************************************
 * Return a template by its list position
 ************************************************/

_polygon *getTemplateByPos(
    short pos
) {
    return getFromListByPos( pos, templates );
}

/*************************************************
 * Return the number of template polygons
 ************************************************/

short getTemplateCount() {
    return templates->count;
}

/*************************************************
 * Render the image to the given file
 ************************************************/

void drawTiling(
    char    *json_path,
    double  exclusion_radius,
    char    *pdf_path
) {
    char    buf[100];

    if ( pdf_path == NULL )
        pdf_path = DEFAULT_PDF_PATH;

    sprintf( buf, "%s -s 50 -i -x %f %s %s", RENDER_SCRIPT,
                exclusion_radius, json_path, pdf_path );

    system( buf );
}

/*************************************************
 * Return a template from the list by type
 ************************************************/

_polygon *getTemplateByType(
    _template_type   type
) {

    _polygon    *template;
    _list_item  *item;

    for( item = templates->first; item != NULL; item = item->next ) {

        template = item->data;
        if ( template->type == type )
            return template;
    }

    return NULL;
}

/*************************************************
 * Return the number of side types available
 ************************************************/

int getSideTypeCount() {

    return SIDE_TYPE_COUNT;
}

/*************************************************
 * Find the pos of the type in the template list
 ************************************************/

short findTemplateTypePos(
    _template_type  type
) {
    _polygon    *template;
    short       pos;

    for( pos = 0; pos < templates->count; pos++ ) {

        template = getFromListByPos( pos, templates );
        if ( template->type == type )
            return pos;
    }

    return -1;
}
