#!/usr/bin/perl
 
use warnings;
use strict;
 
use Math::Trig;
use Data::Dumper;
use Getopt::Std;

use constant    POLYGON_TYPE_INITIAL    =>  'INITIAL_TYPE';
use constant    POLYGON_TYPE_SHOULDER_L =>  'LEFT_SHOULDER_TYPE';
use constant    POLYGON_TYPE_SHOULDER_R =>  'RIGHT_SHOULDER_TYPE';
use constant    POLYGON_TYPE_NECK       =>  'NECK_TYPE';
use constant    POLYGON_TYPE_FILLER     =>  'NECK_FILLER';

use constant    POLYGON_STATUS_READY    =>  'POLYGON_READY';
use constant    POLYGON_STATUS_DONE     =>  'POLYGON_DONE';

use constant    POLYGON_SIDE_READY      =>  'SIDE_READY';
use constant    POLYGON_SIDE_DONE       =>  'SIDE_DONE';

use constant    VERTEX_STATUS_READY     =>  'VERTEX_READY';
use constant    VERTEX_STATUS_DONE      =>  'VERTEX_DONE';

use constant    VERTEX_TYPE_NORMAL      =>  'VERTEX_NORMAL';
use constant    VERTEX_TYPE_CLOSURE     =>  'VERTEX_CLOSURE';

use constant    INIT_POLYGON_SIZE       =>  100;

use constant    IMAGE_PAGE_DIMENSIONS   =>  {
                                    'A3'    =>  [ 297, 420 ],
                                    'A4'    =>  [ 210, 297 ],
                                    'A5'    =>  [ 148, 210 ],
                                    'SQ'    =>  [ 300, 300 ],
                };

############################################################
# Check the startup options

my $DEFAULT_DEST = 'kites.pdf';

my %OPTIONS;
getopts('a:C:dDhH:Jk:lo:P:S:T:x:X:z:Z:', \%OPTIONS);

help() if $OPTIONS{ 'h' };

my $DUMP_AS_JSON = $OPTIONS{ 'J' };
my $DEBUGGING = $OPTIONS{ 'D' };
my $DEBUG_LABEL_SIZE = $OPTIONS{ 'a' } || 8.0;
my $DEBUG_DUMP_ALL_DATA = $OPTIONS{ 'd' };
my $DEBUG_LABEL_POLYGONS = $OPTIONS{ 'l' };
my $DEBUG_EXPLAIN_VERTEX = $OPTIONS{ 'X' };
my $DEBUG_LAST_CREATED_POLYGON = $OPTIONS{ 'z' };
my $DEBUG_LAST_PROCESSED_POLYGON = $OPTIONS{ 'Z' };
my $DEBUG_DRAW_POLYGON = $OPTIONS{ 'H' };

my $KEEP_TEMP_FILE = $OPTIONS{ 'k' };

my $DEST = $OPTIONS{ 'o' } || $DEFAULT_DEST;

my $DRAWING_IMAGE_TYPE = $OPTIONS{ 'T' } || 'debug';

my $DRAWING_CENTRE_POLYGON = $OPTIONS{ 'C' };
my $DRAWING_SCALE = $OPTIONS{ 'S' } || 1;
my $DRAWING_PAGE_SIZE = $OPTIONS{ 'P' } || 'A4';

my $DRAWING_OFFSET_X = 0;
my $DRAWING_OFFSET_Y = 0;

my $MAX_ITERATION_DEPTH = $OPTIONS{ 'x' } || 0;

my $DEBUG_EXPLAIN_VERTEX_POLYGON_ID = '';
my $DEBUG_EXPLAIN_VERTEX_NUMBER = '';

############################################################
# Setup the environment

my $POLYGONS = [];
my $DEPTH_IDS = {};
my $POLYGON_LOOKUP = {};

my $TEMP = $OPTIONS{ 'k' } || 'clp.ps';

my $h = 1.0 / cos( pi / 6.0 );

my $TEMPLATE_POLYGON = {

    'VERTICES'  =>  [
                        newVertex( 0.0, 0.0, 60 ),
                        newVertex( $h * cos( pi / 6.0 ), 0.0, 90 ),
                        newVertex( 1.0, $h * sin( pi / 6.0 ), 120 ),
                        newVertex( cos( pi / 3.0 ), sin( pi / 3.0 ), 90 ),
                    ],
};

help( 'Invalid page size' )
                    unless IMAGE_PAGE_DIMENSIONS->{ $DRAWING_PAGE_SIZE };

if ( $DEBUG_EXPLAIN_VERTEX ) {

    ( 
        $DEBUG_EXPLAIN_VERTEX_POLYGON_ID,
        $DEBUG_EXPLAIN_VERTEX_NUMBER,
    ) = $DEBUG_EXPLAIN_VERTEX =~ /(.*)\/(.*)/;

    help( 'Bad vertex id' ) unless defined $DEBUG_EXPLAIN_VERTEX_NUMBER;
}

my $RENDERER;
for ( $DRAWING_IMAGE_TYPE ) {

    /^(d|debug)$/ix     && do { $RENDERER = \&drawDebug; last };
    /^(o|outline)$/ix   && do { $RENDERER = \&drawOutlines; last };
    /^(s|spiral)/ix     && do { $RENDERER = \&drawSpirals; last };
    /^(t|triangles)/ix  && do { $RENDERER = \&drawTriangles; last };
    /^(v|vertices)/ix   && do { $RENDERER = \&drawVertices; last };
    /.*/                && help( "Unsupported drawing type" );
}

setupInitialPolygons();

processPolygons();

dumpJsonData() if $DUMP_AS_JSON;
dumpAllData() if $DEBUG_DUMP_ALL_DATA;

drawImage();

exit;

############################################################
# Process the image

sub processPolygons {

    my $all_done = 0;

    while ( ! $all_done ) {

        $all_done = 1;

        my $prev_polygon;   # used to check for rings...
        my $ring_start;
        my $ring_count = 0;

        for my $polygon ( @{ $POLYGONS} ) {

            next if $polygon->{ 'STATUS' } eq POLYGON_STATUS_DONE;
            next unless $polygon->{ 'DEPTH' } < $MAX_ITERATION_DEPTH;

            $all_done = 0;

            my $this_polygon_id = polygonId( $polygon );

            printf "processing %s\n", $this_polygon_id if $DEBUGGING;

            my $vertices = $polygon->{ 'VERTICES' };
            my $sides = $polygon->{ 'SIDES' };

            for ( my $s = 0; $s < 4; $s++ ) {

                next if ( $sides->[ $s ] eq POLYGON_SIDE_DONE );

                my $side_color = $polygon->{ 'SIDE_COLOR' }->[ $s ];

                printf "processing side $s\n" if $DEBUGGING;

                my $dist = distance( $vertices->[ $s ], $vertices->[ $s - 3 ] );

                my $ang = nextVertexAng( $polygon, $s );

                my $reverse = ( $s > 1 );
                $ang -= $reverse ? pi : ( pi / 3.0 );

                my $from_vertex = ( $s + ( $reverse ? 1 : 0 ) ) % 4;
            
                my $new_depth = $polygon->{ 'DEPTH' } + 1;
                my $new_depth_id = nextDepthId( $new_depth );
                my $new_polygon_id = sprintf "%s-%s", $new_depth, $new_depth_id;

                my $new_polygon = transformTemplate(
                    $new_polygon_id,
                    $vertices->[ $from_vertex ]->{ 'X' },
                    $vertices->[ $from_vertex ]->{ 'Y' },
                    $ang, $dist
                );
                $new_polygon->{ 'STATUS' } = POLYGON_STATUS_READY;
                $new_polygon->{ 'DEPTH' } = $new_depth;
                $new_polygon->{ 'ID' } = $new_depth_id;

                $new_polygon->{ 'SIDE_COLOR' }->[ 0 ] = 1 - $side_color;
                $new_polygon->{ 'SIDE_COLOR' }->[ 1 ] = 1 - $side_color;
                $new_polygon->{ 'SIDE_COLOR' }->[ 2 ] = $side_color;
                $new_polygon->{ 'SIDE_COLOR' }->[ 3 ] = 1 - $side_color;

                $new_polygon->{ 'TYPE' } = 
                                ( $s == 1 ) ? POLYGON_TYPE_SHOULDER_R :
                                ( $s == 2 ) ? POLYGON_TYPE_SHOULDER_L :
                                              POLYGON_TYPE_NECK
                                ;

                $POLYGON_LOOKUP->{ $new_polygon_id } = $new_polygon;

                $polygon->{ 'SIDES' }->[ $s ] = POLYGON_SIDE_DONE;
                $new_polygon->{ 'SIDES' }->[ $reverse ? 0 : 3 ] = POLYGON_SIDE_DONE;

                shareVertex( $polygon, $from_vertex, $new_polygon, 0 );
 
                my $offset = $reverse ? 1 : 3;
                shareVertex( $polygon, $from_vertex - $offset, $new_polygon, $offset );
                
                push @{ $POLYGONS }, $new_polygon;
                printf "created %s\n", $new_polygon_id if $DEBUGGING;

                my $check_filler = #0 && ( ( $s == 1 ) || ( $s == 2 ) ) &&
                    ( $vertices->[ $from_vertex ]->{ 'TYPE' } eq VERTEX_TYPE_CLOSURE );

                my $p = -1;
                my $new_vertices = $new_polygon->{ 'VERTICES' };

                # If this polygon completes a vertex there will be common vertices to share

                if (
                    $check_filler ||
                    ( $vertices->[ $from_vertex ]->{ 'ANG_USED' } == 360 ) 
                )   {
                
                    $new_polygon->{ 'TYPE' } = POLYGON_TYPE_FILLER;
                    $vertices->[ $from_vertex ]->{ 'TYPE' } = VERTEX_TYPE_NORMAL;

                    $new_polygon->{ 'SIDES' }->[ 0 ] = POLYGON_SIDE_DONE;
                    $new_polygon->{ 'SIDES' }->[ 3 ] = POLYGON_SIDE_DONE;
                
                    my $last_id = $vertices->[ $from_vertex ]->{ 'LAST_POLYGON' };
                    my $last_polygon = $POLYGON_LOOKUP->{ $last_id };

                    $last_polygon = $vertices->[ $from_vertex ]->{ 'RING_POLYGON' }
                                                                    if ( $check_filler );


                    my $v = findPolygonVertex( $vertices->[ $from_vertex ], $last_polygon );

                    my $last_vertices = $last_polygon->{ 'VERTICES' };

                    my $d1 = distance( $last_vertices->[ ($v + 1) % 4 ], $new_vertices->[ 4 - $offset ] );
                    my $d2 = distance( $last_vertices->[ ($v + 3) % 4 ], $new_vertices->[ 4 - $offset ] );

                    my $v1 = ( $d1 < $d2 ) ? ( $v + 1 ) : ( $v - 1 );

                    my $last_side = $v - ( $s == 2 ? 0 : 1 );
                    $last_side = 0 if ( $s == 3 );

                    $last_polygon->{ 'SIDES' }->[ $last_side ] = POLYGON_SIDE_DONE;

                    printf "%s: FILLER: %s last side - $last_id/$v-> side %d (s $s)\n",
                                                            $this_polygon_id,
                                                            $new_polygon_id,
                                                            $last_side,
                                                            $new_polygon_id
                                                                    if $DEBUGGING;
                    printf "F vtx %s/%d - %s/%d\n",
                                    polygonId( $last_polygon ), $last_side,
                                    $new_polygon_id, $v ? 3 : 1
                                                                    if $DEBUGGING;

                    shareVertex( $last_polygon, $v1, $new_polygon, 4 - $offset );

                    $new_vertices->[ 4 - $offset ]->{ 'LAST_POLYGON' } = $new_polygon_id;
                }
                elsif (
                    ( $new_vertices->[ ++$p ]->{ 'ANG_USED' } > 360 ) ||
                    ( $new_vertices->[ ++$p ]->{ 'ANG_USED' } > 360 ) ||
                    ( $new_vertices->[ ++$p ]->{ 'ANG_USED' } > 360 ) ||
                    ( $new_vertices->[ ++$p ]->{ 'ANG_USED' } > 360 ) 
                )   {

                    printf "greedy %s/$p\n",$new_polygon_id if $DEBUGGING;

                    $new_polygon->{ 'SIDES' }->[ $p ] = POLYGON_SIDE_DONE;
                    $new_polygon->{ 'SIDES' }->[ $p - 3 ] = POLYGON_SIDE_DONE;

                    # Find the over-lapping polyon that shares this vertex
                    my $dist = -1;
                    my $overlap_polygon;
                    for my $id ( keys %{ $new_vertices->[ $p ]->{ 'POLYGONS' } } ) {

                        next if $id eq $new_polygon_id;
                    
                        my $try_polygon = $POLYGON_LOOKUP->{ $id };

                        my $d = distance( $new_vertices->[ 4 - $p ],
                                                $try_polygon->{ 'VERTICES' }->[ $p ] );
                    
                        if ( ( $d < $dist ) || ( $dist < 0 ) ) {
                            $dist = $d;
                            $overlap_polygon = $try_polygon;
                        }
                    }
                    printf "overlap %s common vtxs %s / %s\n", polygonId( $overlap_polygon ), $p, 4-$p if $DEBUGGING;
                    
                    $overlap_polygon->{ 'SIDES' }->[ 1 ] = POLYGON_SIDE_DONE;
                    $overlap_polygon->{ 'SIDES' }->[ 2 ] = POLYGON_SIDE_DONE;

                    shareVertex( $overlap_polygon, 4 - $p, $new_polygon, $p, 1 );
                    shareVertex( $overlap_polygon, $p, $new_polygon, 4 - $p );

                    $new_vertices->[ $p ]->{ 'ANG_USED' } -= 60;
                    $new_vertices->[ 4 - $p ]->{ 'ANG_USED' } -= 60;

                    $new_vertices->[ 4 - $p ]->{ 'LAST_POLYGON' } = $new_polygon_id;
                }

                $vertices->[ $s ]->{ 'LAST_POLYGON' } = $new_polygon_id;
                $vertices->[ $s - 3 ]->{ 'LAST_POLYGON' } = $new_polygon_id;

                # Check to see if this polygon completes a ring - in which case
                # there is a vertex to share with the polygon that started the
                # ring.
                # Always start with vtx 1..?

                my $count = 1;
                my $check_polygon1 = $new_polygon;
                my $check_polygon2;
                
                while ( $count < 6 ) { 
                
                    $check_polygon2 = findVertexDepthPolygon( $check_polygon1, 1 );
                    last unless $check_polygon2;
                    last if $check_polygon2 == $new_polygon;
                    last unless $check_polygon1->{ 'VERTICES' }->[ 1 ] ==
                                            $check_polygon2->{ 'VERTICES' }->[ 3 ] ;

                    $count++;

                    my $dist = distance( $check_polygon2->{'VERTICES'}->[1], 
                                                $new_polygon->{'VERTICES'}->[3] );

                    if ( ( $count == 6 ) && ( $dist < 1E-6 )) {

                        printf "RING %s/1(%x) -> %s/3(%x)\n",
                                    polygonId( $check_polygon2 ),
                                    $check_polygon2->{'VERTICES'}->[1],
                                    $new_polygon_id,
                                    $new_polygon->{'VERTICES'}->[3],
                                                                if $DEBUGGING;

                        shareVertex( $check_polygon2, 1, $new_polygon, 3 )
                                    if ( $check_polygon2->{'VERTICES'}->[1] !=
                                                        $new_polygon->{'VERTICES'}->[3] ) ;

                        my $vertex = $check_polygon2->{ 'VERTICES' }->[ 1 ];
                        $vertex->{ 'TYPE' } = VERTEX_TYPE_CLOSURE;
                        $vertex->{ 'RING_POLYGON' } = $new_polygon;
                    }

                    $check_polygon1 = $check_polygon2;
                }

                $prev_polygon = $new_polygon;

                if ( $new_polygon_id eq ( $DEBUG_LAST_CREATED_POLYGON || '' ) ) {
            
                    explainVertex() if ( $DEBUG_EXPLAIN_VERTEX_POLYGON_ID );

                    dumpJsonData() if $DUMP_AS_JSON;
                    dumpAllData() if $DEBUG_DUMP_ALL_DATA;
                    return;
                }
            }

            $polygon->{ 'STATUS' } = POLYGON_STATUS_DONE;

            explainVertex() if ( $this_polygon_id eq $DEBUG_EXPLAIN_VERTEX_POLYGON_ID );

            if ( $this_polygon_id eq ( $DEBUG_LAST_PROCESSED_POLYGON || '' ) ) {
    
                dumpJsonData() if $DUMP_AS_JSON;
                dumpAllData() if $DEBUG_DUMP_ALL_DATA;
                return;
            }
        }
    }

    explainVertex() if ( $DEBUG_EXPLAIN_VERTEX_POLYGON_ID );
}

############################################################
# Find a polygon at the given vertex of the same depth

sub findVertexDepthPolygon {

    my $polygon = shift;
    my $vertex_number = shift;

    my $vertex = $polygon->{ 'VERTICES' }->[ $vertex_number ];
    
    for my $check_id ( keys %{ $vertex->{ 'POLYGONS' } } ) {

        my $check_polygon = $POLYGON_LOOKUP->{ $check_id };
        next unless $check_polygon;

        next if $check_polygon->{ 'ID' } eq $polygon->{ 'ID' };
        next if $check_polygon->{ 'DEPTH' } != $polygon->{ 'DEPTH' };

        return $check_polygon;
    }

    return;
}

############################################################
# Share the given vertices

sub shareVertex {

    my $polygon_1 = shift;
    my $vertex_1 = shift;
    my $polygon_2 = shift;
    my $vertex_2 = shift;
    my $common = shift;     # ie at the end of a ring

    my $vertex1 = $polygon_1->{ 'VERTICES' }->[ $vertex_1 ];
    my $vertex2 = $polygon_2->{ 'VERTICES' }->[ $vertex_2 ];

    unless ( $common ) {
        $vertex1->{ 'ANG_USED' } += $vertex2->{ 'ANG_USED' };
    }

    my $vertex1_polygons = $vertex1->{ 'POLYGONS' };
    my $vertex2_polygons = $vertex2->{ 'POLYGONS' };

    for my $id ( keys %{ $vertex2_polygons } ) {

        $vertex1_polygons->{ $id } = 1;
    }

    my $vertex1_last = $vertex1->{ 'LAST_POLYGON' } || '0';
    my $vertex2_last = $vertex2->{ 'LAST_POLYGON' } || '0';

    $vertex1->{ 'LAST_POLYGON' } = 
                ( $vertex1_last gt $vertex2_last ) ? $vertex1_last : $vertex2_last;

    $polygon_2->{ 'VERTICES' }->[ $vertex_2 ] = $vertex1;

    printf "sharing vertices %s/%d - %s/%d\n",
                                    polygonId( $polygon_1 ), $vertex_1,
                                    polygonId( $polygon_2 ), $vertex_2
                                                        if $DEBUGGING;
}

############################################################
# Setup the initial polygons

sub setupInitialPolygons {

    my $color = 0;

    for ( my $p = 0; $p < 6; $p++ ) {
    
        my $id = sprintf "0-%d", $p + 1;
        my $polygon = transformTemplate( $id, 0, 0, $p*pi/3.0, INIT_POLYGON_SIZE );

        $polygon->{ 'TYPE' } = POLYGON_TYPE_INITIAL;
        $polygon->{ 'DEPTH' } = 0;
        $polygon->{ 'ID' } = $p + 1;

        $POLYGON_LOOKUP->{ polygonId( $polygon ) } = $polygon;

        my $vertices = $polygon->{ 'VERTICES' };

        if ( $p > 0 ) {

            shareVertex( $POLYGONS->[ 0 ], 0, $polygon, 0 );
            shareVertex( $POLYGONS->[ -1 ], 3, $polygon, 1 );
            #shareVertex( $polygon, 0, $POLYGONS->[ 0 ], 0 );
            #shareVertex( $polygon, 1, $POLYGONS->[ -1 ], 3 );
        }

        $vertices->[ 1 ]->{ 'ANG_USED' } = 180;
        $vertices->[ 2 ]->{ 'ANG_USED' } = 120;

        if ( $p == 5 ) {

            $vertices->[ 3 ] = $POLYGONS->[ 0 ]->{ 'VERTICES' }->[ 1 ];
        }

        $polygon->{ 'SIDES' } = [];
        $polygon->{ 'SIDES' }->[ 0 ] = POLYGON_SIDE_DONE;
        $polygon->{ 'SIDES' }->[ 1 ] = POLYGON_SIDE_READY;
        $polygon->{ 'SIDES' }->[ 2 ] = POLYGON_SIDE_READY;
        $polygon->{ 'SIDES' }->[ 3 ] = POLYGON_SIDE_DONE;
        
        $polygon->{ 'SIDE_COLOR' }->[ 0 ] = $color;
        $polygon->{ 'SIDE_COLOR' }->[ 1 ] = $color;
        $polygon->{ 'SIDE_COLOR' }->[ 2 ] = 1 - $color;
        $polygon->{ 'SIDE_COLOR' }->[ 3 ] = $color;
        $color = 1 - $color;

        push @{ $POLYGONS }, $polygon;
    }

    $POLYGONS->[ 0 ]->{ 'VERTICES' }->[ 0 ]->{ 'ANG_USED' } = 360;
}

############################################################
# Find the angle from a vertex point to the next vertex

sub nextVertexAng {

    my $polygon = shift;
    my $vertex_no = shift;

    my $vertices = $polygon->{ 'VERTICES' };

    return atan2(
        $vertices->[ $vertex_no - 3 ]->{ 'Y' } - $vertices->[ $vertex_no ]->{ 'Y' },
        $vertices->[ $vertex_no - 3 ]->{ 'X' } - $vertices->[ $vertex_no ]->{ 'X' }
    );
}

############################################################
# Return a new vertex object

sub newVertex {

    return {
        'STATUS'    =>  VERTEX_STATUS_READY,
        'TYPE'      =>  VERTEX_TYPE_NORMAL,
        'X'         =>  shift,
        'Y'         =>  shift,
        'ANG_USED'  =>  shift,
    };
}

############################################################
# Translate, rotate and scale a vertex

sub transformVertex {

    my $vertex = shift;
    my $offset_x = shift;
    my $offset_y = shift;
    my $rotate = shift;
    my $scale = shift;

    my $ang = atan2( $vertex->{ 'Y' },  $vertex->{ 'X' } ); 
    my $dist = distance( { 'X' => 0.0, 'Y' => 0.0 }, $vertex );

    $ang += $rotate;

    $vertex->{ 'X' } = $scale * $dist * cos( $ang ) + $offset_x;
    $vertex->{ 'Y' } = $scale * $dist * sin( $ang ) + $offset_y;
}

############################################################
# Translate, rotate and scale a copy of the template polygon

sub transformTemplate {

    my $id = shift;
    my $offset_x = shift;
    my $offset_y = shift;
    my $rotate = shift;
    my $scale = shift;

    my $polygon = {
        
        'STATUS'    => POLYGON_STATUS_READY,
        'VERTICES'  => [],
        'SIDES'     => [ ( POLYGON_SIDE_READY ) x 4 ],
    };

    for my $vertex ( @{ $TEMPLATE_POLYGON->{ 'VERTICES' } } ) {

        my %copy = %{ $vertex };
    
        $copy{ 'STATUS' } = VERTEX_STATUS_READY;
        $copy{ 'POLYGONS' } = { $id => 1 };

        transformVertex( \%copy, $offset_x, $offset_y, $rotate, $scale );

        push @{ $polygon->{ 'VERTICES' } }, \%copy;
    }

    return $polygon;
}

############################################################
# Simple mod/min/max functions

sub min {

    my ( $v1, $v2 ) = @_;

    return ( $v1 < $v2 ) ? $v1 : $v2;
}

sub max {

    my ( $v1, $v2 ) = @_;

    return ( $v1 > $v2 ) ? $v1 : $v2;
}

sub mod {

    my $v = shift;

    return ( $v > 0 ) ? $v : ( -1 * $v );
}

############################################################
# Find the distance between 2 vertices

sub distance {

    my $vtx1 = shift;
    my $vtx2 = shift;

    my $dx = $vtx1->{ 'X' } - $vtx2->{ 'X' };
    my $dy = $vtx1->{ 'Y' } - $vtx2->{ 'Y' };

    return sqrt( ( $dx * $dx ) +  ( $dy * $dy ) );
}

############################################################
# Draw a polygon

sub drawPolygon {

    my $fh = shift;
    my $polygon = shift;

    my $xc = 0;
    my $yc = 0;

    my $vertices = $polygon->{ 'VERTICES' };

    for ( my $v = 0; $v < 4; $v++ ) {

        printf $fh "%f %f %s\n", 
                        $vertices->[ $v ]->{ 'X' },
                        $vertices->[ $v ]->{ 'Y' },
                        ( $v == 0 ) ? 'moveto' : 'lineto'
        ;

        $xc += $vertices->[ $v ]->{ 'X' },;
        $yc += $vertices->[ $v ]->{ 'Y' },;
    }

    $xc = $xc / 4.0;
    $yc = $yc / 4.0;

    printf $fh "%f %f lineto\n", 
                        $vertices->[ 0 ]->{ 'X' },
                        $vertices->[ 0 ]->{ 'Y' }
    ;

    my $operation = ( $DEBUG_DRAW_POLYGON || '' ) eq polygonId( $polygon ) ?
                                                "fill" : "stroke";
  
    printf $fh "setDrawColor $operation\n";

    if ( $DEBUG_LABEL_POLYGONS ) {
    
        printf $fh "%f %f (%s) csh\n",
                                $xc, $yc, polygonId( $polygon );
    
        for ( my $v = 0; $v < 4; $v++ ) {

            printf $fh "%f %f (%d) csh\n",
                $xc + ( ( $vertices->[ $v ]->{ 'X' } - $xc ) * .8 ),
                $yc + ( ( $vertices->[ $v ]->{ 'Y' } - $yc ) * .8 ),
                $v
        }
    }
}

############################################################
# Draw polygon outlines

sub drawImage {

    open my $fh, '>', $TEMP or die "Could not open $TEMP - $!\n";

    my $page_dimensions = IMAGE_PAGE_DIMENSIONS->{ $DRAWING_PAGE_SIZE };

    $DRAWING_OFFSET_X = $page_dimensions->[ 0 ] * 2.54 / 2.0;
    $DRAWING_OFFSET_Y = $page_dimensions->[ 1 ] * 2.54 / 2.0;

    if ( my $polygon = $POLYGON_LOOKUP->{ $DRAWING_CENTRE_POLYGON || 'x' } ) {

        my $vertices = $polygon->{ 'VERTICES' };
        my $x = 0;
        my $y = 0;

        for ( my $v = 0; $v < 4; $v++ ) {
        
            $x += $vertices->[ $v ]->{ 'X' };
            $y += $vertices->[ $v ]->{ 'Y' };
        }

        $DRAWING_OFFSET_X -= ( $x * $DRAWING_SCALE / 4.0 );
        $DRAWING_OFFSET_Y -= ( $y * $DRAWING_SCALE / 4.0 );
    }

    printf $fh "%%!PS\n%%%%BoundingBox: 0 0 %f %f\n%%%%EndComments\n

        <<
            /PageSize [ %f %f ]
            /ImagingBBox null 
        >> setpagedevice

        /mv { moveto } bind def
        /ln { lineto } bind def

        0 0 mv 1000 0 ln 1000 1000 ln 0 1000 ln fill

        $DRAWING_OFFSET_X $DRAWING_OFFSET_Y translate
        $DRAWING_SCALE dup scale
    ",
        $page_dimensions->[ 0 ] * 2.54, $page_dimensions->[ 1 ] * 2.54,
        $page_dimensions->[ 0 ] * 2.54, $page_dimensions->[ 1 ] * 2.54,
    ;

    &{$RENDERER}( $fh );

    close $fh;

    my $convert = "/usr/bin/convert";

    unless ( -f $convert ) {

        printf "Count not locate convert at $convert\n";
    }
    else {

        system( "$convert $TEMP $DEST" );
    }

    unlink $TEMP unless $KEEP_TEMP_FILE;
}

############################################################
# Draw polygon triangles

sub drawTriangles {

    my $fh = shift;

    printf $fh "1 setlinejoin 1 $DRAWING_SCALE div setlinewidth\n";

    for my $polygon ( @{$POLYGONS} ) {

        next unless inViewport( $polygon );

        my $vertices = $polygon->{ 'VERTICES' };

        printf $fh "%f %f mv ", $vertices->[ 0 ]->{ 'X' }, $vertices->[ 0 ]->{ 'Y' };
        printf $fh "%f %f ln ", $vertices->[ 1 ]->{ 'X' }, $vertices->[ 1 ]->{ 'Y' };
        printf $fh "%f %f ln ", $vertices->[ 2 ]->{ 'X' }, $vertices->[ 2 ]->{ 'Y' };
        printf $fh "gsave ";
        printf $fh "%f %f ln ", $vertices->[ 3 ]->{ 'X' }, $vertices->[ 3 ]->{ 'Y' };
        printf $fh "1 0 0 setrgbcolor fill grestore ";
        printf $fh "1 1 0 setrgbcolor fill\n";
    }
}

############################################################
# Draw polygon spirals

sub drawSpirals {

    my $fh = shift;

    my $colors = [ '1 1 0', '1 0 0' ];

    printf $fh "1 setlinejoin 1 $DRAWING_SCALE div setlinewidth\n";

    my $color = 0;

    for my $polygon ( @{$POLYGONS} ) {

        next unless inViewport( $polygon );

        my $vertices = $polygon->{ 'VERTICES' };

        $color = $polygon->{ 'SIDE_COLOR' }->[ 0 ] || 0;

        for ( my $v = 0; $v < 4; $v++ ) {

            printf $fh "%f %f %s ", 
                        $vertices->[ $v ]->{ 'X' },
                        $vertices->[ $v ]->{ 'Y' },
                        ( $v == 0 ) ? 'mv' : 'ln'
            ;
        }

        printf $fh "%s setrgbcolor fill\n", $colors->[ $color ], 

        $color = 1 - $color;

        my $dist = distance( $vertices->[ 1 ], $vertices->[ 2 ] );

        my $dx1 = $vertices->[ 1 ]->{ 'X' } - $vertices->[ 0 ]->{ 'X' };
        my $dy1 = $vertices->[ 1 ]->{ 'Y' } - $vertices->[ 0 ]->{ 'Y' };
        my $dx2 = $vertices->[ 2 ]->{ 'X' } - $vertices->[ 1 ]->{ 'X' };
        my $dy2 = $vertices->[ 2 ]->{ 'Y' } - $vertices->[ 1 ]->{ 'Y' };

        my $x1 = $vertices->[ 2 ]->{ 'X' };
        my $y1 = $vertices->[ 2 ]->{ 'Y' };
        my $x2 = $vertices->[ 3 ]->{ 'X' };
        my $y2 = $vertices->[ 3 ]->{ 'Y' };

        my @spiral = ( [ $x1, $y1 ] );
        my @spiral_back = ( [ $x2, $y2 ] );

        $x2 -= $dx2;
        $y2 -= $dy2;
        push @spiral_back, [$x2, $y2];

        my $div = 3.0;
        while( mod( $DRAWING_SCALE * $dist / $div ) > 1 ) {

            $x1 -= $dx1 / $div;
            $y1 -= $dy1 / $div;
            push @spiral, [ $x1, $y1 ];

            $x1 -= $dx2 / $div;
            $y1 -= $dy2 / $div;
            push @spiral, [ $x1, $y1 ];

            $x2 += $dx1 / $div;
            $y2 += $dy1 / $div;
            push @spiral_back, [ $x2, $y2 ];

            $x2 += $dx2 / $div;
            $y2 += $dy2 / $div;
            push @spiral_back, [ $x2, $y2 ];

            $div *= -3.0;
        }

        my $op = "mv";

        map {
            printf $fh "%f %f $op ", $_->[0], $_->[1];
            $op = "ln";
        } ( @spiral, reverse @spiral_back );

        printf $fh "%f %f $op ", $spiral[0]->[0], $spiral[0]->[1];
        printf $fh "%s setrgbcolor fill\n", $colors->[ $color ];
    }
}

############################################################
# Draw polygon vertices

sub drawVertices {

    my $fh = shift;

    print $fh "

        1 setlinejoin
        1 $DRAWING_SCALE div setlinewidth

        1 setgray

        /rad 1 $DRAWING_SCALE div def

        /disc {     % x y disc -
            /y exch def
            /x exch def
            0 10 360 {
                /a exch def
                x a cos rad mul add
                y a sin rad mul add
                a 0 eq { moveto } { lineto } ifelse
            } for
            fill
        } def
    \n";

    for my $polygon ( @{$POLYGONS} ) {

        map {
            printf $fh "%f %f disc\n", $_->{ 'X' }, $_->{ 'Y' };
        } ( @{ $polygon->{ 'VERTICES' } } );
    }
}

############################################################
# Draw polygon outlines

sub drawOutlines {

    my $fh = shift;

    printf $fh "

        1 setlinejoin
        1 $DRAWING_SCALE div setlinewidth

        1 1 0 setrgbcolor
    ";

    for my $polygon ( @{$POLYGONS} ) {

        next unless inViewport( $polygon );

        my $vertices = $polygon->{ 'VERTICES' };

        for ( my $v = 0; $v < 4; $v++ ) {

            printf $fh "%f %f %s ", 
                        $vertices->[ $v ]->{ 'X' },
                        $vertices->[ $v ]->{ 'Y' },
                        ( $v == 0 ) ? 'mv' : 'ln'
            ;
        }

        printf $fh "stroke\n", 
    }
}

############################################################
# Draw polygon details

sub drawDebug {

    my $fh = shift;

    printf $fh "

        1 setlinejoin
        1 $DRAWING_SCALE div setlinewidth

        /Arial findfont %f scalefont setfont

        /csh {
            3 1 roll 
            moveto 
            dup stringwidth pop -2 div 0 rmoveto 
            1 setgray show 
        } def

        /setDefaultColor { /drawColor{ 1 1 0 setrgbcolor } def } def
        /setDebugColor { /drawColor{ 0 1 0 setrgbcolor } def } def

        /setDrawColor { drawColor } def
    ",
        $DEBUG_LABEL_SIZE,
    ;

    for my $polygon ( @{$POLYGONS} ) {

        print $fh "setDefaultColor\n";
        print $fh "/drawColor{ 1 0 0 setrgbcolor } def\n"
                            if ( $polygon->{ 'TYPE' } eq POLYGON_TYPE_SHOULDER_R );
        print $fh "/drawColor{ 0 1 0 setrgbcolor } def\n"
                            if ( $polygon->{ 'TYPE' } eq POLYGON_TYPE_SHOULDER_L );
        print $fh "/drawColor{ 0 0 1 setrgbcolor } def\n"
                            if ( $polygon->{ 'TYPE' } eq POLYGON_TYPE_NECK );
        print $fh "/drawColor{ 0 1 1 setrgbcolor } def\n"
                            if ( $polygon->{ 'TYPE' } eq POLYGON_TYPE_FILLER );

        drawPolygon( $fh, $polygon );
    }

    if (
        $DEBUG_DRAW_POLYGON &&
        ( my $polygon = $POLYGON_LOOKUP->{ $DEBUG_DRAW_POLYGON } )
    ) {
    
        printf $fh "setDebugColor\n";
        drawPolygon( $fh, $polygon );
    }
}

############################################################
# Is the given polygon visible on the page

sub inViewport {
    
    my $polygon = shift;

    my $vertices = $polygon->{ 'VERTICES' };

    my $bounds;

    for( my $p = 0; $p < 4; $p++ ) {
        
        my $x = ( $vertices->[ $p ]->{ 'X' } * $DRAWING_SCALE ) + $DRAWING_OFFSET_X;
        my $y = ( $vertices->[ $p ]->{ 'Y' } * $DRAWING_SCALE ) + $DRAWING_OFFSET_Y;

        $bounds->[ 0 ] = $p ? min( $bounds->[ 0 ], $x ) : $x;
        $bounds->[ 1 ] = $p ? min( $bounds->[ 1 ], $y ) : $y;
        $bounds->[ 2 ] = $p ? max( $bounds->[ 2 ], $x ) : $x;
        $bounds->[ 3 ] = $p ? max( $bounds->[ 3 ], $y ) : $y;
    }

    return 0 if ( $bounds->[ 2 ] < 0 );
    return 0 if ( $bounds->[ 3 ] < 0 );

    my $page_dimensions = IMAGE_PAGE_DIMENSIONS->{ $DRAWING_PAGE_SIZE };

    return 0 if ( $bounds->[ 0 ] > ( $page_dimensions->[ 0 ] * 2.54 ) );
    return 0 if ( $bounds->[ 1 ] > ( $page_dimensions->[ 1 ] * 2.54 ) );

    return 1;
}

############################################################
# Return the next depth id

sub nextDepthId {

    my $depth = shift;

    $DEPTH_IDS->{ $depth } ||= 1;

    return $DEPTH_IDS->{ $depth }++;
}

############################################################
# Return the polygon id

sub polygonId {

    my $polygon = shift;

    return sprintf "%s-%s",
                    $polygon->{ 'DEPTH' }, $polygon->{ 'ID' };
}

############################################################
# Return a status flag for the given vertex

sub vertexStatus {
    
    my $vertex = shift;

    return 'R' if ( $vertex->{ 'STATUS' } eq VERTEX_STATUS_READY );
    return 'D' if ( $vertex->{ 'STATUS' } eq VERTEX_STATUS_DONE );

    return '?';
}

############################################################
# Return a status flag for the given side

sub vertexSideStatus {
    
    my $side = shift;

    return 'R' if ( $side eq POLYGON_SIDE_READY );
    return 'D' if ( $side eq POLYGON_SIDE_DONE );

    return '?';
}

############################################################
# Locate the vertex on the given polygon

sub findPolygonVertex {

    my $vertex = shift;
    my $polygon = shift;

    for ( my $v = 0; $v < 4; $v++ ) {

        return $v if ( $vertex == $polygon->{ 'VERTICES' }->[ $v ] );
    }

    return -1;
}

############################################################
# Display details about a vertex

sub explainVertex {


    my $polygon = $POLYGON_LOOKUP->{ $DEBUG_EXPLAIN_VERTEX_POLYGON_ID };
    help( "No such polygon: $DEBUG_EXPLAIN_VERTEX_POLYGON_ID" ) unless $polygon;

    my $vertex = $polygon->{ 'VERTICES' }->[ $DEBUG_EXPLAIN_VERTEX_NUMBER ];
    help( "No such vertex: $DEBUG_EXPLAIN_VERTEX_NUMBER" ) unless $vertex;

    printf "Vertex $DEBUG_EXPLAIN_VERTEX_POLYGON_ID/$DEBUG_EXPLAIN_VERTEX_NUMBER\n";
    printf "    status   : %s\n", vertexStatus( $vertex );
    printf "    type     : %s\n", substr( $vertex->{ 'TYPE' }, 7, 1 );
    printf "    last     : %s\n", $vertex->{ 'LAST_POLYGON' } || '?';
    printf "    ang used : %s\n", $vertex->{ 'ANG_USED' };
    printf "    pt       : %f, %f\n", $vertex->{ 'X' }, $vertex->{ 'Y' };
    printf "    polygons : ";

    my $sep = '';
    for my $id ( sort keys %{ $vertex->{ 'POLYGONS' } } ) { 

        printf "$sep$id";
        $sep=', ';
    }
    printf "\n";
}

############################################################
# Display the POLYGON data in json format

sub dumpJsonData {

    printf "[\n";

    my $sep = "";
    for my $polygon ( @{$POLYGONS} ) {
        printf "%s  {", $sep;
        printf "'id':'%s',", polygonId( $polygon );
        printf "'vertices':[";

        for ( my $v = 0; $v < 4; $v++ ) {
            printf "%s{ 'x':%f, 'y':%f }",
                    ( $v == 0 ) ? "" : ",",
                    $polygon->{ 'VERTICES' }->[ $v ]->{ 'X' },
                    $polygon->{ 'VERTICES' }->[ $v ]->{ 'Y' },
        }
        printf "]}";

        $sep = ",\n";
    }
    printf "\n]\n";

    exit;
}

############################################################
# Display the POLYGON data struct

sub dumpAllData {

    for my $polygon ( @{$POLYGONS} ) {

        printf "%s (%s): ",
                polygonId( $polygon ),
                ( $polygon->{ 'STATUS' } eq POLYGON_STATUS_READY ) ? 'R' : 'D';

        for ( my $s = 0; $s < 4; $s++ ) {

            printf "%x %s/%s (%s%03d) ",
                $polygon->{ 'VERTICES' }->[ $s ],
                vertexStatus( $polygon->{ 'VERTICES' }->[ $s ] ),
                vertexSideStatus( $polygon->{ 'SIDES' }->[ $s ] ),
                substr( $polygon->{ 'VERTICES' }->[ $s ]->{ 'TYPE' }, 7, 1 ),
                $polygon->{ 'VERTICES' }->[ $s ]->{ 'ANG_USED' };
        }

        printf "\n";
    }

    exit;
}

############################################################
# Help doco

sub help {

    print "\n@_\n" if @_;

    printf "\n$0 " .
        "[-a <sz>] [-C <id>] [-d] [-D] [-h] [-H <id>] [-k >tmp>] [-l] " .
        "[-o <path>] [-P <pg>] [-S <sc>] " .
        "[-T <typ>] [-x <depth>] [-X <vtx>] [-z <id>] [-Z <id>]

    where
        -a  label font suze
        -d  dump full polygon data
        -C  centre image on polygon
        -D  turn on debugging logging
        -h  this help info
        -H  hightlight this polygon
        -J  dump all data as JSON
        -k  temp file to use (and retain)
        -l  label the vertices (with optional font size)
        -o  output file
        -P  image page size (SQ, A5, A4, or A3) - default A4
        -S  scale for drawing (default 1)
        -T  image rendering type (DEBUG, SPIRAL, OUTLINE) default DEBUG
        -x  max iteration depth to process to
        -X  explain a vertex (eg -X 6-61/3)
        -z  stop processing once this polygon is created
        -Z  stop processing once this polygon is processed
    \n";

    exit;
}

__END__

sharing problem
kites.pl -D -x7  -l -a.6 -S20 -C'6-194' -Z'6-186'

