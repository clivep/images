#include <stdio.h>
#include <stdlib.h>

#include "tiling-geometry.h"
#include "tiling-polygons.h"
#include "tiling-plugin.h"

/*******************************************************************************
 *
 * tiling-polygons.c
 *
 *  Functions for managing polygon objects
 *
 ******************************************************************************/

/*************************************************
 * Return a new polygon side object
 ************************************************/

_polygon_side *newSide(
    int         type,
    _polygon    *owner
) {
    _polygon_side *side;

    if ( ( side = ALLOC( _polygon_side ) ) == NULL )
        logFatal( "Failed to calloc() a new side" );

    side->status = SIDE_STATUS_READY;
    side->type = type;
    side->owner = owner;

    return side;
}

/*************************************************
 * Return a new polygon vertex object
 ************************************************/

_polygon_vertex *newVertex(
    double x,
    double y,
    double angle_used
) {
    _polygon_vertex *vertex;

    if ( ( vertex = ALLOC( _polygon_vertex ) ) == NULL )
        logFatal( "Failed to calloc() a new vertex" );

    vertex->status = VERTEX_STATUS_READY;

    vertex->x = x;
    vertex->y = y;
    vertex->angle_used = angle_used;

    vertex->polygons = initialiseList( NULL );

    return vertex;
}

/*************************************************
 * Return the polygon type given its char
 ************************************************/

short findPolygonTypeByChar(
    char        type 
) {
    short       pos;
    _polygon    *template;

    for( pos = 0; pos < getTemplateCount(); pos++ ) {

        template = getTemplateByPos( pos );

        if ( polygonTypeChar( template ) == type )
            return template->type;
    }

    return -1;
}

/*************************************************
 * Return a new polygon object
 ************************************************/

_polygon *newPolygon(
    int         type
) {
    _polygon    *polygon;
    _polygon    *template;
    short       edges;
    short       p;

    edges = polygonTypeEdges( type );

    if ( ( polygon = ALLOC( _polygon ) ) == NULL )
        logFatal( "Failed to calloc() a new polygon" );

    polygon->type = type;
    polygon->edges = edges;
    polygon->anchor_edge = -1;

    if ( ( polygon->angles = NALLOC( edges, double ) ) == NULL )
        logFatal( "Failed to calloc() polygon edges" );

    if ( ( polygon->sides = NALLOC( edges, _polygon_side * ) ) == NULL )
        logFatal( "Failed to calloc() a polygon sides" );

    if ( ( polygon->vertices = NALLOC( edges, _polygon_vertex * ) ) == NULL )
        logFatal( "Failed to calloc() polygon vertices" );

    // If this is a call to create one of the templates, then return
    // wth the basic allocations
    template = getTemplateByType( type );
    if ( template == NULL )
        return polygon;

    // So we are copying on of the templates...
    for( p = 0; p < polygon->edges; p++ )
        polygon->angles[ p ] = template->angles[ p ];

    for( p = 0; p < polygon->edges; p++ )
        polygon->sides[ p ] = newSide( template->sides[ p ]->type, polygon );

    for( p = 0; p < polygon->edges; p++ ) {
        polygon->vertices[ p ] = newVertex(
                                    template->vertices[ p ]->x,
                                    template->vertices[ p ]->y,
                                    template->vertices[ p ]->angle_used
                                 );

        addToList( polygon, polygon->vertices[ p ]->polygons );
    }

    return polygon;
}

/*************************************************
 * Find the postiion of the vertex in the polygon
 ************************************************/

short findVertexNumber(
    _polygon        *polygon,
    _polygon_vertex *vertex
) {
    short v;

    for( v = 0; v < polygon->edges; v++ ) {

        if ( polygon->vertices[ v ] == vertex )
            return v;
    }

    return -1;
}

/*************************************************
 * Return the number of edges for the polygon
 ************************************************/

short polygonEdges (
    _polygon *polygon
) {
    return polygonTypeEdges( polygon->type );
}

/*************************************************
 * Release polygon memory
 ************************************************/

void releasePolygon(
    void *polygon_ptr
) {
    _polygon        *polygon = polygon_ptr;
    _polygon_vertex *vertex;
    _polygon_side   *side;
    _polygon        *common_polygon;
    _list_item      *item;
    short           v;

    logMsg( "Releasing polygon %lx", (long)polygon );

    for( v = 0; v < polygon->edges; v++ ) {

        if ( polygon->sides[ v ]->status == SIDE_STATUS_DONE ) {

            if ( ( side = findCommonSide( polygon, v, &common_polygon ) ) != NULL ) {

                side->status = SIDE_STATUS_READY;

                // Do not reset the status if the polygon is outside
                if ( common_polygon->status == POLYGON_STATUS_DONE )
                    common_polygon->status = POLYGON_STATUS_READY;
            }
            else {
                logMsg( "No common side found for %lx/%d", (long)polygon, v );
            }
        }

        free( polygon->sides[ v ] );
    }

    for( v = 0; v < polygon->edges; v++ ) {
        
        vertex = polygon->vertices[ v ];
        if ( vertex->polygons->count > 1 ) {

            // Return the angle to the vertex
            logMsg( "Returning %d to %ls", RAD2DEG( polygon->angles[ v ] ), (long)vertex );
            vertex->angle_used -= polygon->angles[ v ];

            // Remove this polygon from the vertex list
            for( item = vertex->polygons->first; item != NULL; item = item->next ) {
            
                if ( item->data == polygon ) {
                    logMsg( "Removing %lx from vertex %lx polygon list", (long)polygon, (long)vertex );
                    releaseListItem( item, vertex->polygons );
                    break;
                }
            }
        } 
        else {

            logMsg( "Freeing vertex %lx (and polygon list) %lx/%d", (long)vertex, (long)polygon, v );
            releaseList( vertex->polygons );
            free( vertex );
        }
    }

    free( polygon->angles );
    free( polygon->vertices );
    free( polygon->sides );

    free( polygon );
}

/*************************************************
 * Offset a polygon
 ************************************************/

void offsetPolygon(
    _polygon    *polygon,
    double      x_offset,
    double      y_offset
) {
    _polygon_vertex *vertex;
    short           v;

    for( v = 0; v < polygon->edges; v++ ) {

        vertex = polygon->vertices[ v ];

        vertex->x += x_offset;
        vertex->y += y_offset;
    }
}

/*************************************************
 * Find the common side
 ************************************************/

_polygon_side *findCommonSide(
    _polygon    *polygon,
    short       side,
    _polygon    **common_polygon
) {
    _polygon        *trial_polygon;
    _polygon_vertex *vertex1;
    _polygon_vertex *vertex2;
    _list_item      *item1;
    short           pos;
    short           trial_side;

    vertex1 = polygon->vertices[ side ];
    vertex2 = polygon->vertices[ NEXT_VTX( side, polygon ) ];

    // For each of the polygons around vertex1 only 1 should also 
    // be present around vertex2...
    for( item1 = vertex1->polygons->first; item1 != NULL; item1 = item1->next ) {

        trial_polygon = item1->data;

        if ( trial_polygon != polygon ) {
    
            for( pos = 0; pos < vertex2->polygons->count; pos++ ) {

                if ( trial_polygon == getFromListByPos( pos, vertex2->polygons ) ) {
                    trial_side = findVertexNumber( trial_polygon, vertex2 );
                    logMsg( "Found common side for %lx/%d at %lx/%d",
                                    (long)polygon, side, (long)trial_polygon, trial_side );
                    *common_polygon = trial_polygon;
                    return trial_polygon->sides[ trial_side ];
                }
            }
        }
    }

    return NULL;
}

