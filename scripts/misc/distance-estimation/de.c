#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <math.h>

#include "de.h"

/*******************************************************************************
 *
 * de.c
 *
 *  Investigations of distance estimationa to a isosurface
 *
 *  Refs:
 *      http://iquilezles.org/www/articles/distance/distance.htm
 *
 ******************************************************************************/

#define     DEFAULT_IMAGE_RAD       1.0
#define     DEFAULT_IMAGE_XC        0.0
#define     DEFAULT_IMAGE_YC        0.0

#define     DEFAULT_IMAGE_PIXEL_WID 1000
#define     DEFAULT_IMAGE_PIXEL_DEP 1000

#define     TMP_POSTSCRIPT_FILE     "de.ps"

#define     ABS( a )                ( (a) > 0 ? (a) : -1.0 * (a) )
#define     MIN( a, b )             ( (a) < (b) ? (a) : (b) )

#define     RAD( a )                sqrt( ( (a)->ximg * (a)->ximg ) + ( (a)->yimg * (a)->yimg ) )

_env env;

int main(
    int argc,
    char **argv
) {
    setupEnv();
    processParameters( argc, argv ); 
    processImage();
    drawImage();

    exit( 0 );
}

/*************************************************
 * Setup the default processing environment
 ************************************************/
void setupEnv() {

    env.tmp_ps  = '\0';

    env.image_pixel_dep = DEFAULT_IMAGE_PIXEL_DEP;
    env.image_pixel_wid = DEFAULT_IMAGE_PIXEL_WID;

    env.data  = (_point *)calloc( env.image_pixel_wid * env.image_pixel_dep, sizeof( _point ) );

    env.image_rad = DEFAULT_IMAGE_RAD;
    env.image_xc = DEFAULT_IMAGE_XC;
    env.image_yc = DEFAULT_IMAGE_YC;
}

/*************************************************
 * Log an error and exit
 ************************************************/
void logFatal(
    char *format,
    ...
) {
    va_list ap;

    va_start( ap, format );
    vprintf( format, ap );
    va_end( ap );

    exit( -1 );
}

/*************************************************
 * Process an integer parameter
 ************************************************/
void getCharParameter(
    int     argc,
    char    **argv,
    int     pnt,
    char    **value,
    char    *desc
) {
    if ( pnt < argc ) {
        *value = argv[ pnt ];
    }
    else {
        logFatal( "Missing the %s parameter", desc );
    }
}

/*************************************************
 * Process an integer parameter
 ************************************************/
void getIntParameter(
    int     argc,
    char    **argv,
    int     pnt,
    int     *value,
    char    *desc
) {
    if ( pnt < argc ) {
        *value = atoi( argv[ pnt ] );
    }
    else {
        logFatal( "Missing the %s parameter", desc );
    }
}

/*************************************************
 * Process a long double parameter
 ************************************************/
void getDoubleParameter(
    int     argc,
    char    **argv,
    int     pnt,
    long double  *value,
    char    *desc
) {
    if ( pnt < argc ) {
        *value = atof( argv[ pnt ] );
    }
    else {
        logFatal( "Missing the %s parameter", desc );
    }
}

/*************************************************
 * Process any runline parameters
 ************************************************/
void processParameters(
    int     argc,
    char    **argv
) {
    int     pnt = 1;

    while ( ( pnt < argc ) && ( *argv[ pnt ] == '-' ) ) {

        switch( argv[ pnt ][ 1 ] ) {

            case 'd' :
                getIntParameter( argc, argv, ++pnt, &env.image_pixel_dep, "image pixel depth" );
                break;

            case 'h' :
                usage( argv[ 0 ] );
                break;

            case 'k' :
                getCharParameter( argc, argv, ++pnt, &env.tmp_ps, "temp ps file" );
                break;

            case 'r' :
                getDoubleParameter( argc, argv, ++pnt, &env.image_rad, "image radius" );
                break;

            case 'w' :
                getIntParameter( argc, argv, ++pnt, &env.image_pixel_wid, "image pixel width" );
                break;

            case 'x' :
                getDoubleParameter( argc, argv, ++pnt, &env.image_xc, "image x centre" );
                break;

            case 'y' :
                getDoubleParameter( argc, argv, ++pnt, &env.image_yc, "image y centre" );
                break;
        }

        pnt++;
    }
}

/*************************************************
 * The func applied to a point
 ************************************************/
long double f(
    _point  *pt
) {
    long double rad;
    long double ang;
    
    rad = RAD( pt );
    ang = atan2( pt->yimg, pt->ximg );

    return rad - 1.0 + ( 0.5 * sin( 3.0 * ang + 2.0 * rad * rad ) );
}

/*************************************************
 * The func applied to a coordinate
 ************************************************/
long double fAtXY(
    long double x,
    long double y
) {
    _point pt;

    pt.ximg = x;
    pt.yimg = y;

    return f( &pt );
}

/*************************************************
 * Process the image
 ************************************************/
void processImage() {
    
    _point      *pt;
    int         x;
    int         y;
    long double val;

    for( y = 0; y < env.image_pixel_dep; y++ ) {

        for( x = 0; x < env.image_pixel_wid; x++ ) {

            pt = &env.data[ ( y * env.image_pixel_wid ) + x ];
            pt->xpxl = x;
            pt->ypxl = y;

            val = MIN( env.image_pixel_wid, env.image_pixel_dep );
                        
            pt->ximg = env.image_xc + env.image_rad * ( ( (long double)2.0 * pt->xpxl / val ) - 1.0 );
            pt->yimg = env.image_yc + env.image_rad * ( ( (long double)2.0 * pt->ypxl / val ) - 1.0 );

            pt->val = f( pt );

            pt->grad = RAD( grad( pt ) );
            pt->grad2 = RAD( grad2( pt ) );

            pt->dist = ABS( pt->val ) / pt->grad;
        }
    }
}

/*************************************************
 * Clamp the given value a range
 ************************************************/
long double clamp(
    long double val,
    long double upper_val,
    long double lower_val
) {
    if ( val < lower_val )
        return lower_val;

    if ( val > upper_val )
        return upper_val;

    return val;
}

/*************************************************
 * A smoothstep function returns 0 <= v <= 1
 ************************************************/
long double smoothStep(
    long double edge0,
    long double edge1,
    long double val
) {
    val = clamp( ( val - edge0 ) / ( edge1 - edge0 ), 0.0, 1.0 ); 

    return val * val * ( 3.0 - 2.0 * val );
}
    
/*************************************************
 * Function to evauate the gradient at a point
 ************************************************/
_point *grad(
    _point  *pt
) {
    static _point grad;   

    _point tmp;
    long double rad;
    long double ang;
    long double val;

    rad = RAD( pt );
    ang = atan2( pt->yimg, pt->ximg );

    tmp.ximg = pt->yimg / ( rad * rad );
    tmp.yimg = -1.0 * pt->ximg / ( rad * rad );

    val = cos( ( 3.0 * ang ) + ( 2.0 * rad * rad ) );

    grad.ximg = ( pt->ximg / rad ) + val * ( 1.5 * tmp.ximg + 2.0 * pt->ximg );
    grad.yimg = ( pt->yimg / rad ) + val * ( 1.5 * tmp.yimg + 2.0 * pt->yimg );

    return &grad;
}

/*************************************************
 * Function to evauate the gradient at a point
 ************************************************/
_point *grad2(
    _point  *pt
) {
    static _point grad;   
    long double dx;

    dx = 0.01;

    grad.ximg = ( fAtXY( pt->ximg + dx, pt->yimg ) - fAtXY( pt->ximg - dx, pt->yimg ) ) / ( 2.0 * dx );
    grad.yimg = ( fAtXY( pt->ximg, pt->yimg + dx ) - fAtXY( pt->ximg, pt->yimg - dx ) ) / ( 2.0 * dx );

    return &grad;
}

/*************************************************
 * Render the image
 ************************************************/
_color *pointColor(
    _point  *pt
) {
    static  _color color;
    //static long double sz = -1;
    long double val;
    
    /* Color the isoline blue, close to the line yellow else a gray scale
    if ( sz < 0 ) {
        val = MIN( env.image_pixel_wid, env.image_pixel_dep );
        sz = 2.0 * env.image_rad / val;
    }

    if ( ABS(pt->dist) > 10.0 * sz ) {
        color.r = color.g = color.b = MIN(255 * sqrt( ABS(pt->val) ), 255 );
    } else if ( ABS(pt->dist) > 4 * sz ) {
        color.r = color.g = 255;
        color.b = 0;
    } else {
        color.r = color.g = 0;
        color.b = 255;
    }
    */

    val = ABS( 1000 * ( pt->grad - pt->grad2 ) );

    color.r = color.g = color.b = MIN( 255, val );

    return &color;
}

/*************************************************
 * Draw additional image decorations
 ************************************************/
void drawExtras(
    FILE    *fp
) {
    //drawDiscAt( fp, 0.0, 0.5 );
    //drawDiscAt( fp, 0.0, 1.0 );
    //drawDiscAt( fp, 0.0, 1.5 );

    //drawDiscAt( fp, 0.5, 0.5 );
    //drawDiscAt( fp, 0.6, 0.6 );
    //drawDiscAt( fp, 0.7, 0.7 );
    //drawDiscAt( fp, 0.8, 0.8 );
    //drawDiscAt( fp, 0.9, 0.9 );
    //drawDiscAt( fp, 1.0, 1.0 );
    //drawDiscAt( fp, 1.1, 1.1 );
    //drawDiscAt( fp, 1.2, 1.2 );
    //drawDiscAt( fp, 1.3, 1.3 );
    //drawDiscAt( fp, 1.4, 1.4 );
    drawDiscAt( fp, 1.5, 1.5 );
}

/*************************************************
 * Draw a disc on the image at the given position
 ************************************************/
void drawDiscAt(
    FILE        *fp,
    long double xc,
    long double yc
) {
    int         x;
    int         y;
    int         val;
    _point      *pt;
 
    // find the data point to use...
    val = MIN( env.image_pixel_wid, env.image_pixel_dep ) / ( 2.0 * env.image_rad );
    x = ( xc - env.image_xc + env.image_rad ) * val;
    y = ( yc - env.image_yc + env.image_rad ) * val;

    if ( ( x < env.image_pixel_wid ) && ( y < env.image_pixel_dep ) ) {
        pt = &env.data[ x + y * env.image_pixel_wid ];
        drawDisc( fp, xc, yc, pt->dist );
    }
}

/*************************************************
 * Draw a disc on the image
 ************************************************/
void drawDisc(
    FILE    *fp,
    long double xc,
    long double yc,
    long double rad
) {
    long double r;
    long double x;
    long double y;
    long double val;

    val = MIN( env.image_pixel_wid, env.image_pixel_dep ) / ( 2.0 * env.image_rad );
    x = val * ( xc - env.image_xc );
    y = val * ( yc - env.image_yc );

    r = val * rad;

    fprintf( fp, "%Le %Le %Le disc\n", x, y, r );
}

/*************************************************
 * Render the image
 ************************************************/
void drawImage() {
    FILE    *fp;
    char    *tmp_ps = TMP_POSTSCRIPT_FILE;
    int     x;
    int     y;
    _color  *color;

    if ( env.tmp_ps != '\0' )
        tmp_ps = env.tmp_ps;
    
    if ( ( fp = fopen( tmp_ps, "w" ) ) == NULL ) {
        printf( "Could not open image file %s\n", tmp_ps);
        exit( -1 );
    }

    fprintf( fp, "%%!PS\n" );
    fprintf( fp, "%%%%BoundingBox: 0 0 %d %d\n", env.image_pixel_wid, env.image_pixel_dep );
    fprintf( fp, "%%%%EndComments\n" );
    fprintf( fp, "\n" );
    fprintf( fp, "<<\n" );
    fprintf( fp, "  /PageSize [ %d %d ]\n", env.image_pixel_wid, env.image_pixel_dep );
    fprintf( fp, "  /ImagingBBox null\n" );
    fprintf( fp, ">>\n" );
    fprintf( fp, "setpagedevice\n" );
    fprintf( fp, "\n" );

    fprintf( fp, "/disc { \n" );
    fprintf( fp, "  /r exch def\n" );
    fprintf( fp, "  /yc exch %d add def\n", env.image_pixel_dep / 2);
    fprintf( fp, "  /xc exch %d add def\n", env.image_pixel_wid / 2 );
    fprintf( fp, "  gsave\n" );
    fprintf( fp, "  xc yc translate\n" );
    fprintf( fp, "  -2 -2 moveto 4 0 rlineto 0 4 rlineto -4 0 rlineto fill\n" );
    fprintf( fp, "  0 5 360 {\n" );
    fprintf( fp, "    /a exch def\n" );
    fprintf( fp, "    a cos r mul\n" );
    fprintf( fp, "    a sin r mul\n" );
    fprintf( fp, "    a 0 eq { moveto } { lineto } ifelse \n" );
    fprintf( fp, "  } for\n" );
    fprintf( fp, "  1 0 0 setrgbcolor stroke\n" );
    fprintf( fp, "  grestore\n" );
    fprintf( fp, "} def\n" );

    fprintf( fp, "gsave\n" );
    fprintf( fp, "/buf %d string def\n", env.image_pixel_wid * 6 );
    fprintf( fp, "%d %d scale\n", env.image_pixel_dep, env.image_pixel_dep );
    fprintf( fp, "%d %d 8\n", env.image_pixel_wid, env.image_pixel_dep );
    fprintf( fp, "[%d 0 0 %d 0 0] {\n", env.image_pixel_wid, env.image_pixel_dep );
    fprintf( fp, "currentfile buf readhexstring pop\n" );
    fprintf( fp, "} false 3 colorimage\n" );

    for( y = 0; y < env.image_pixel_dep; y++ ) {   

        for( x = 0; x < env.image_pixel_wid; x++ ) {   

            color = pointColor( &env.data[ ( y * env.image_pixel_wid ) + x ] );
            fprintf( fp, "%02x%02x%02x", color->r, color->g, color->b );
        }
    }

    fprintf( fp, "\ngrestore\n" );

    drawExtras( fp );

    fprintf( fp, "showpage\n" );

    fclose( fp );
}

/*************************************************
 * Usage info
 ************************************************/
void usage(
    char    *self
) {

    printf( "\n" );
    printf( "Usage: %s\n", self );
    printf( "          [-h] [-r <radius>] [-s <img size>] [-x <x centre>] [-y <y centre>]\n" );
    printf( "          [-d <dep>] [-w <wid>]\n" );
    printf( "\n" );
    printf( "   where:\n" );
    printf( "       -d <dep>            image pixel depth (default %d)\n", DEFAULT_IMAGE_PIXEL_DEP );
    printf( "       -h                  this help information\n" );
    printf( "       -k <tmp ps file>    postscript working file to retain\n" );
    printf( "       -r <rad>            the image radius (default %le)\n", DEFAULT_IMAGE_RAD );
    printf( "       -w <wid>            image pixel width (default %d)\n", DEFAULT_IMAGE_PIXEL_WID );
    printf( "       -x <x centre>       image centre (default %le)\n", DEFAULT_IMAGE_XC );
    printf( "       -y <y centre>       image centre (default %le)\n", DEFAULT_IMAGE_YC );
    printf( "\n" );

    exit( 0 );
}


