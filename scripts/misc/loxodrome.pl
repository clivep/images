#!/usr/bin/perl

use warnings;
use strict;

use Math::Trig;
use Getopt::Std;

############################################################
# Check the runline options

my %options;
getopts( 'c:hm:p:s:o:D', \%options );

help() if $options{ 'h' };

############################################################
# Setup the environment

my $const = $options{ 'c' } || 3.0;
my $step = $options{ 's' } || 0.1;

my $scale = $options{ 'm' } || 500;

my $config = $options{ 'o' } || 'loxodrome.cfg';

my $param_max = $options{ 'p' } || 50;

my $fh;
unless ( $options{ 'D' } ) {

    open $fh, '>', $config or die "Could not open $config: $!\n";

    while ( my $ln = <DATA> ) { print $fh $ln }
}

############################################################
# Generate the config

my $coords = [];

for( my $t = -1 * $param_max; $t < $param_max; $t += $step ) {

    my $coord0 = getCoords( $t - $step );
    my $coord1 = getCoords( $t + $step );

    my $centre = [
        ( $coord0->[ 0 ] + $coord1->[ 0 ] ) / 2.0,
        ( $coord0->[ 1 ] + $coord1->[ 1 ] ) / 2.0,
        ( $coord0->[ 2 ] + $coord1->[ 2 ] ) / 2.0,
    ];

    my $radius = sqrt( 
        ( $coord0->[ 0 ] - $coord1->[ 0 ] ) * ( $coord0->[ 0 ] - $coord1->[ 0 ] ) +
        ( $coord0->[ 1 ] - $coord1->[ 1 ] ) * ( $coord0->[ 1 ] - $coord1->[ 1 ] ) +
        ( $coord0->[ 2 ] - $coord1->[ 2 ] ) * ( $coord0->[ 2 ] - $coord1->[ 2 ] )
    ) / 4.0;

    sphere( $fh, $centre, $radius );
}

close $fh unless $options{ 'D' };

exit;

# render a tape with triangles
for( my $t = -1 * $param_max; $t < $param_max; $t += $step ) {

    $coords->[ 0 ] = getCoords( $t );

    if ( $options{ 'D' } ) {

        printf "%f, %f, %f\n", @{ $coords->[ 0 ] };
        next;
    }

    $coords->[ 1 ] = getCoords( $t + $step );

    $coords->[ 2 ] = getCoords( $t - 64 * $step );
    $coords->[ 3 ] = getCoords( $t - 63 * $step );

    #$coords->[ 4 ] = getCoords( $t + 63 * $step );
    #$coords->[ 5 ] = getCoords( $t + 64 * $step );

    for( my $p = 0; $p < 3; $p++ ) {

        $coords->[ 2 ]->[ $p ] = ( 2.0 * $coords->[ 0 ]->[ $p ] + $coords->[ 2 ]->[ $p ] ) / 3.0;
        $coords->[ 3 ]->[ $p ] = ( 2.0 * $coords->[ 1 ]->[ $p ] + $coords->[ 3 ]->[ $p ] ) / 3.0;

        $coords->[ 4 ]->[ $p ] = ( $coords->[ 2 ]->[ $p ] + $coords->[ 3 ]->[ $p ] ) / 2.0;

    #    $coords->[ 4 ]->[ $p ] = ( 2.0 * $coords->[ 0 ]->[ $p ] + $coords->[ 4 ]->[ $p ] ) / 3.0;
    #    $coords->[ 5 ]->[ $p ] = ( 2.0 * $coords->[ 1 ]->[ $p ] + $coords->[ 5 ]->[ $p ] ) / 3.0;
    }

    triangle( $fh, $coords->[ 0 ], $coords->[ 1 ], $coords->[ 4 ] );
    triangle( $fh, $coords->[ 0 ], $coords->[ 4 ], $coords->[ 2 ] );
    triangle( $fh, $coords->[ 1 ], $coords->[ 3 ], $coords->[ 4 ] );

    #triangle( $fh, $coords->[ 0 ], $coords->[ 4 ], $coords->[ 1 ] );
    #triangle( $fh, $coords->[ 1 ], $coords->[ 4 ], $coords->[ 5 ] );
}

close $fh unless $options{ 'D' };

exit;

############################################################
# Return the coordinates for a param value

sub getCoords {

    my $t = shift;

    my $r = deg2rad( $t );

    my $c = atan( $const * $r );

    return [
        $scale * cos( $t ) * cos( $c ),
        $scale * sin( $t ) * cos( $c ),
        $scale * -sin( $c ),
    ];
}

############################################################
# Write a sphere config entry

sub sphere {

    my $fh = shift;
    my $centre = shift;
    my $radius = shift;

    printf $fh "    object:
        type: sphere
        surface: default
        centre:
            i: %f
            j: %f
            k: %f
        radius: %f
        color-name: grey8
     \n",
        $centre->[ 0 ],
        $centre->[ 1 ],
        $centre->[ 2 ],
        $radius,
    ;
}

############################################################
# Write a triangle config entry

sub triangle {

    my $fh = shift;
    my $coords1 = shift;
    my $coords2 = shift;
    my $coords3 = shift;

    printf $fh "    object:
        type: triangle
        surface: default
        color-name: grey8
        vertex1:
            i: %f
            j: %f
            k: %f
        vertex2:
            i: %f
            j: %f
            k: %f
        vertex3:
            i: %f
            j: %f
            k: %f
    \n",
        @{ $coords1 },
        @{ $coords2 },
        @{ $coords3 },
    ;

    return;
}

############################################################
# Help doco

sub help {

    print "\n@_\n" if @_;

    printf "\n$0 " .
                "[-c <const>] [-h] [-k <ps file>] [-m <mul>] [-o <dest>] [-p <max>] [-s <step>] [-D]

        where
            -c  const multiplier in the calcs (default 3)
            -h  this help message
            -m  image multiplier (default 500)
            -o  output file (default 'loxodrome.cfg')
            -p  param max (limit 50)
            -s  param step value (default 0.1)

            -D  printf the param coords

    \n";

    exit;
}

__DATA__

defaults:
    background:
        red: 0.0
        green: 0.0
        blue: 0.0
    properties:
        anti-alias: on
        diffusion_index: 1.0
        specular_index: 0.3
        specular_exponent: 7.0
        color_scramble: 0.05

screen:
    centre:
        i: 10000
        j: 10000
        k: 5000
    offset:
        i: 0
        j: 0
        k: 0
    eye_dist: 100
    height: 10
    width: 10

factor-scheme-defs:
    name: face
        diffusion_index: 0.95
        specular_index: 0.4
        specular_exponent: 5.0
        reflective_index: 0.15
    name: plane
        diffusion_index: 0.9
        specular_index: 0.4
        specular_exponent: 5.0
        reflective_index: 0.2

custom-color-defs:
    name: grey8
        red: 0.80
        green: 0.80
        blue: 0.80

image:
    height: 1000
    width: 1000
    units: PIX
    resolution: 1

illumination:
    ambience: 0.3
    lamp:
        brightness: 80
        sample_size: 20
        centre:
            i: 1000
            j: 1000
            k: 1000
        radius: 5
    lamp:
        brightness: 100
        sample_size: 20
        centre:
            i: 1000
            j: 0
            k: 2000
        radius: 5

objects:

    object:
        type: plane
        surface: default
        color-name: grey8
        scheme-name: plane
        origin:
            i: -500
            j: -500
            k: -500
        axis1:
            i: 1500
            j: -500
            k: 0
        axis2:
            i: -500
            j: 1500
            k: 0

