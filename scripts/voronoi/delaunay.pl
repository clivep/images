#!/usr/bin/perl

use warnings;
use strict;

use Getopt::Std;
use Readonly;
use Math::Trig;

my Readonly    $PS_TEMP                 =   '/tmp/voronoi.ps';

my Readonly    $DEFAULT_OUTPUT_FILE     =   'voronoi.pdf';
my Readonly    $DEFAULT_IMAGE_DEP       =   1000;
my Readonly    $DEFAULT_IMAGE_WID       =   1000;
my Readonly    $DEFAULT_IMAGE_MRGN      =   50;

my Readonly    $DEFAULT_NODE_PATTERN    =   'random';

my Readonly    $DEFAULT_NODE_COUNT      =   20;
my Readonly    $DEFAULT_NODE_SEED       =   8;
my Readonly    $NODE_RADIUS             =   3;

my Readonly    $FALSE                   =   0;
my Readonly    $TRUE                    =   ! $FALSE;

my Readonly    $SMALL                   =   1E-10;

my Readonly    $EXIT_OPTIONS            =   [
                        "setupNodes", 
                        "initTriangulation",
                        "fixedTriangulation",
];

my Readonly    $NODE_PATTERNS           =   {
    'circular'      =>  [   "Concentric circles",
                            \&setupCircularNodes, 
                        ],
    'random'        =>  [   "Random distribution of nodes",
                            \&setupRandomNodes, 
                        ],
    'spiral'        =>  [   "Sqrt(2) spiral",
                            \&setupSpiralNodes, 
                        ],
    'star'          =>  [   "Octagonal start",
                            \&setupStarNodes, 
                        ],
};

my Readonly     $SPHERE_DEF =   {
    'lamp_x'            =>  60,
    'lamp_y'            =>  -60,
    'lamp_z'            =>  150,
    'min-color-pcnt'    =>  30,
    'samples'           =>  100,
    'sheen'             =>  800,
};

############################################################
# Check the runline options

my %options;
getopts( 'c:hkn:pr:s:vBCD:ILM:NO:PTVW:X:', \%options );

help() if $options{ 'h' };

my $NODE_PATTERN = $ARGV[ 0 ] || $DEFAULT_NODE_PATTERN;
help( "Unknown node pattern: " . $NODE_PATTERN )
                    unless $NODE_PATTERNS->{ $NODE_PATTERN };

my $IMAGE_DEP = $options{ 'D' } || $DEFAULT_IMAGE_DEP;
my $IMAGE_WID = $options{ 'W' } || $DEFAULT_IMAGE_WID;
my $IMAGE_MRGN = $options{ 'M' } || $DEFAULT_IMAGE_MRGN;

my $NODE_COUNT = $options{ 'c' } || $DEFAULT_NODE_COUNT;

my $OUTPUT_FILE = $options{ 'O' } || $DEFAULT_OUTPUT_FILE;

srand( $options{ 'n' } || $DEFAULT_NODE_SEED );

############################################################
# Setup the working environment

my $IMAGE = {
    'BOUNDS'            =>  [ $IMAGE_WID, $IMAGE_DEP, 0, 0 ],
    'BOUNDARY'          =>  [],
    'EDGES'             =>  {},
    'NODES'             =>  [],
    'TRIANGLES'         =>  {},
    'POLYGONS'          =>  {},
    'ORIGIN'            =>  {
                                'X' =>  $IMAGE_WID / 2.0,
                                'Y' =>  $IMAGE_DEP / 2.0,
                            },

    'SCALE'             =>  $options{ 's' } || 1,
    'NEXT_TRIANGLE_ID'  =>  1,
    'SPHERE_RENDERERS'  =>  [],
};

############################################################
# Do the work

setupNodes();
processImage() unless ( $options{ 'X' } || '' ) eq "setupNodes";
dumpData() if $options{ 'I' };
renderImage();

exit;

############################################################
# Dump the image data

sub dumpData {

    use Data::Dumper;
    print Dumper $IMAGE;

    exit;
}

############################################################
# Return index of the lowest (smallest y) node in the list

sub findLowestNode {

    my $nodes = shift;

    return -1 unless scalar @{ $nodes };

    my $ly = $nodes->[ 0 ]->{ 'Y' };
    my $p = 0;

    for ( my $i = 1; $i < scalar @{ $nodes }; $i++ ) {

        if ( $nodes->[ $i ]->{ 'Y' } < $ly ) {
                $ly = $nodes->[ $i ]->{ 'Y' };
                $p = $i;
        }
    }

    return $p;
}

############################################################
# Setup circular nodes

sub setupCircularNodes {

    my @node_list;

    my $id = 1;

    push @node_list, {
        'ID'    => $id++,
        'X'     => $IMAGE_WID / 2,
        'Y'     => $IMAGE_DEP / 2,
    };     

    # Here the node count represents the number of circles
    for ( my $c = 0; $c < $NODE_COUNT; $c++ ) {

        my $rx = ( $IMAGE_WID - ( 2 * $IMAGE_MRGN ) ) * $c / $NODE_COUNT;
        my $ry = ( $IMAGE_DEP - ( 2 * $IMAGE_MRGN ) ) * $c / $NODE_COUNT;
        my $r = ( ( $rx < $ry ) ? $rx : $ry ) / 2;

        my $nodes = int( ( 2 * pi * $r ) / 40 );
    
        my $a = 0;
        for ( my $n = 0; $n < $nodes; $n++ ) {

            push @node_list, {
                'ID'    => $id++,
                'X'     => $IMAGE->{ 'ORIGIN' }->{ 'X' } + ( $r * cos( $a ) ),
                'Y'     => $IMAGE->{ 'ORIGIN' }->{ 'Y' } + ( $r * sin( $a ) ),
            };     

            $a += 2 * pi / $nodes;
        }
    }

    return @node_list;
}

############################################################
# Setup nodes on a root-2 spiral

sub setupSpiralNodes {

    my @node_list;
    my $phi = sqrt( 2 );

    # Find the multiplier to keep the spiral in the image
    my $max_r = sqrt( $NODE_COUNT ) * 2;
    my $mx = ( $IMAGE_WID - ( 2 * $IMAGE_MRGN ) ) / $max_r;
    my $my = ( $IMAGE_DEP - ( 2 * $IMAGE_MRGN ) ) / $max_r;
    my $m = ( $mx < $my ) ? $mx : $my;

    my $id = 1;

    for ( my $c = 0; $c < $NODE_COUNT; $c++ ) {
        
        my $a = $c * pi * $phi;
        my $r = sqrt( $c ) * $m;

        push @node_list, {
            'ID'    => $id++,
            'X'     => $IMAGE->{ 'ORIGIN' }->{ 'X' } + ( $r * cos( $a ) ),
            'Y'     => $IMAGE->{ 'ORIGIN' }->{ 'Y' } + ( $r * sin( $a ) ),
        };
    }

    return @node_list;
}

############################################################
# Setup nodes in a octagonal star

sub setupStarNodes {

    my @node_list;

    # the diagonals
    my $dx = ( $IMAGE_WID - 2 * $IMAGE_MRGN ) / sqrt ( 2 );
    my $dy = ( $IMAGE_DEP - 2 * $IMAGE_MRGN ) / sqrt ( 2 );
    my $d = ( $dx < $dy ) ? $dx : $dy;

    # the normal square array
    my $mx = ( $IMAGE_WID - $d ) / 2;
    my $my = ( $IMAGE_DEP - $d ) / 2;
    my $m = ( $mx < $my ) ? $mx : $my;

    my $id = 1;

    for ( my $r = 0; $r < $NODE_COUNT; $r++ ) {

        for ( my $c = 0; $c < $NODE_COUNT; $c++ ) {
    
            my $x = ( $c * $d ) / ( $NODE_COUNT - 1 );
            my $y = ( $r * $d ) / ( $NODE_COUNT - 1 );

            push @node_list, {
                'ID'    => $id++,
                'X'     => $mx + $x,
                'Y'     => $my + $y,
            };
        }
    }

    for ( my $r = 0; $r < $NODE_COUNT; $r++ ) {

        for ( my $c = 0; $c < $NODE_COUNT; $c++ ) {
    
            next if
                ( $NODE_COUNT % 2 ) &&
                ( $c == int( $NODE_COUNT / 2 ) ) &&
                ( $r == int( $NODE_COUNT / 2 ) )
            ;

            # the rotated array
            my $x = ( $c - $r ) * $d / ( sqrt( 2 ) * ( $NODE_COUNT - 1 ) );
            my $y = ( $c + $r ) * $d / ( sqrt( 2 ) * ( $NODE_COUNT - 1 ) );

            push @node_list, {
                'ID'    => $id++,
                'X'     => ( $IMAGE_WID / 2 ) + $x,
                'Y'     => $IMAGE_MRGN + $y,
            };
        }
    }

    return @node_list;
}

############################################################
# Setup nodes in a random spread

sub setupRandomNodes {

    my @node_list;

    for ( my $i = 0; $i < $NODE_COUNT; $i++ ) {

        my $x = $IMAGE_MRGN + ( $IMAGE_WID - 2 * $IMAGE_MRGN ) * rand();
        my $y = $IMAGE_MRGN + ( $IMAGE_DEP - 2 * $IMAGE_MRGN ) * rand();

        push @node_list, {
            'ID'    => $i,
            'X'     => $x,
            'Y'     => $y,
        };
    }

    return @node_list;
}

############################################################
# Setup the nodes and order by distance from the origin

sub setupNodes {

    my @node_list = $NODE_PATTERNS->{ $NODE_PATTERN }->[ 1 ]->();

    for my $node ( @node_list ) {
 
        $node->{ 'DIST' } = distance( $IMAGE->{ 'ORIGIN' }, $node );

        my $x = $node->{ 'X' };
        my $y = $node->{ 'Y' };

        $IMAGE->{ 'BOUNDS' }->[ 0 ] = $x if ( $x < $IMAGE->{ 'BOUNDS' }->[ 0 ] );
        $IMAGE->{ 'BOUNDS' }->[ 1 ] = $y if ( $y < $IMAGE->{ 'BOUNDS' }->[ 1 ] );
        $IMAGE->{ 'BOUNDS' }->[ 2 ] = $x if ( $x > $IMAGE->{ 'BOUNDS' }->[ 2 ] );
        $IMAGE->{ 'BOUNDS' }->[ 3 ] = $y if ( $y > $IMAGE->{ 'BOUNDS' }->[ 3 ] );
    }

    my @sorted_list = sort { $a->{ 'DIST' } <=> $b->{ 'DIST' } } @node_list;
    $IMAGE->{ 'NODES' } = \@sorted_list;
}

############################################################
# Find the distance between 2 nodes according to the metric

sub distance {

    my $node1 = shift;
    my $node2 = shift;

    my $dx = $node1->{ 'X' } - $node2->{ 'X' };
    my $dy = $node1->{ 'Y' } - $node2->{ 'Y' };

    return sqrt( ( $dx * $dx ) + ( $dy * $dy ) );
}

############################################################
# Return the prev triangle id

sub nextTriangleId {

    return $IMAGE->{ 'NEXT_TRIANGLE_ID' }++;
}

############################################################
# Return edge id prev triangle id

sub getEdgeId {

    my $id1 = shift->{ 'ID' };
    my $id2 = shift->{ 'ID' };

    return sprintf "%d-%d", $id1 < $id2 ? ( $id1, $id2 ) : ( $id2, $id1 );
}

############################################################
# Are the 3 nodes in a clockwise sense

sub clockwiseNodes {

    my $node1 = shift;
    my $node2 = shift;
    my $node3 = shift;

    my $d =
        ( $node3->{ 'X' } - $node1->{ 'X' } ) * ( $node2->{ 'Y' } - $node1->{ 'Y' } ) -
        ( $node3->{ 'Y' } - $node1->{ 'Y' } ) * ( $node2->{ 'X' } - $node1->{ 'X' } )
    ;

    return $d > $SMALL;
}

############################################################
# Create a edge for the 2 nodes and triangle

sub addEdge {

    my $node1 = shift;
    my $node2 = shift;
    my $triangle_id = shift;

    my $edge_id = getEdgeId( $node1, $node2 );

    my $edge_info = $IMAGE->{'EDGES' }->{ $edge_id } || {
        'NODE1'     =>  $node1,
        'NODE2'     =>  $node2,
        'TRIANGLES' =>  [],
    };

    push @{ $edge_info->{ 'TRIANGLES' } }, $triangle_id;

    $IMAGE->{'EDGES' }->{ $edge_id } = $edge_info;

    return;
}

############################################################
# Create a new triangle from the given vertices

sub addTriangle {

    my $node1 = shift;
    my $node2 = shift;
    my $node3 = shift;
    my $checkOrientation = shift;

    my $id = nextTriangleId();

    if ( $checkOrientation && clockwiseNodes( $node1, $node2, $node3 ) ) {
        my $tmp = $node2;
        $node2 = $node3;
        $node3 = $tmp;
    }

    my $d1 = distance( $node1, $node2 );
    my $d2 = distance( $node2, $node3 );
    my $d3 = distance( $node3, $node1 );

    my $ang1 = rad2deg( acos( ( $d1 * $d1 + $d3 * $d3 - $d2 * $d2 ) / ( 2 * $d1 * $d3 ) ) );
    my $ang2 = rad2deg( acos( ( $d1 * $d1 + $d2 * $d2 - $d3 * $d3 ) / ( 2 * $d1 * $d2 ) ) );

    my $circum_circle = getCircumCircle( $node1, $node2, $node3 );

    my $triangle = {
        'ID'            =>  $id,
        'NODES'         =>  [ $node1, $node2, $node3 ],
        'ANGLES'        =>  [ $ang1, $ang2, 180 - $ang1 - $ang2 ],
        'CIRCUMCIRCLE'  =>  $circum_circle,
    };

    printf( "new triangle ($id) %s-%s-%s (%d,%d,%d)\n", 
                $node1->{ 'ID' }, $node2->{ 'ID' }, $node3->{ 'ID' },
                @{ $triangle->{ 'ANGLES' } } ) if $options{ 'v' };

    $IMAGE->{ 'TRIANGLES' }->{ $id } = $triangle;

    push @{ $node1->{ 'TRIANGLES' } }, $id;
    push @{ $node2->{ 'TRIANGLES' } }, $id;
    push @{ $node3->{ 'TRIANGLES' } }, $id;

    addEdge( $node1, $node2, $id);
    addEdge( $node2, $node3, $id);
    addEdge( $node3, $node1, $id);

    return $triangle;
}

############################################################
# Show the ids in the given node list

sub showNodeListIds {

    my $list = shift;

    printf "@_ " if @_;

    my $sep = "( ";
    for ( my $b = 0; $b < scalar @{ $list }; $b++ ) {
        printf "%s%s", $sep, $list->[ $b ]->{ 'ID' };
        $sep = ", ";
    }

    printf " )\n";
}

sub showBoundaryIds { showNodeListIds( $IMAGE->{ 'BOUNDARY' }, @_ ); }

############################################################
# Create triangles with the nodes as vertices

sub initialTriangulation {

    # the first 3 points have already been used to make a triangle
    for ( my $n = 3; $n < scalar @{ $IMAGE->{ 'NODES' } }; $n++ ) {

        my $node = $IMAGE->{ 'NODES' }->[ $n ];
        printf "\nProcessing %s\n", $node->{ 'ID' } if $options{ 'v' };
        
        # Circle around the current boundary looking for new triangles
        my $boundary_count = scalar @{ $IMAGE->{ 'BOUNDARY' } };

        my $saved_orientation;
        my @endpoints = ( 0, 0 );      # first and last boundary indicies that can 'see' the new node

        for ( my $b = 0; $b < $boundary_count; $b++ ) {
    
            my $src_node = $IMAGE->{ 'BOUNDARY' }->[ $b ];
            my $next_node = $IMAGE->{ 'BOUNDARY' }->[ ( $b + 1 ) % $boundary_count ];

            my $isClockwise = clockwiseNodes( $src_node, $next_node, $node );

            addTriangle( $src_node, $node, $next_node, $FALSE ) if ( $isClockwise );

            $saved_orientation = $isClockwise unless $b;
            if ( $saved_orientation != $isClockwise ) {
                $endpoints[ $saved_orientation ? 1 : 0 ] = $b
            }

            $saved_orientation = $isClockwise;
        }

        my @boundary = ( $node );
        my $c = $endpoints[1];
        while ( 1 ) {

            my $idx = $c % $boundary_count;
            push @boundary, $IMAGE->{ 'BOUNDARY' }->[ $idx ];
            last if ( $idx == $endpoints[ 0 ] );
            $c++;
        }

        $IMAGE->{ 'BOUNDARY' } = \@boundary;

        return if ( $options{ 'X' } && ( $node->{ 'ID' } eq $options{ 'X' } ) );
    }
}

############################################################
# Remove the triangle from the image

sub removeTriangle {

    my $triangle = shift;

    printf( "    remove triangle %s\n", $triangle->{ 'ID' } ) if $options{ 'v' };

    for ( my $e = 0; $e < 3; $e++ ) {

        my $edge_id = getEdgeId(
                            $triangle->{ 'NODES' }->[ $e - 1 ],
                            $triangle->{ 'NODES' }->[ $e],
        );

        my $edge = $IMAGE->{ 'EDGES' }->{ $edge_id };

        my $idx = ( $edge->{ 'TRIANGLES' }->[ 0 ] == $triangle->{ 'ID' } ) ? 0 : 1;
        splice @{ $edge->{ 'TRIANGLES' } }, $idx, 1;
    }

    delete $IMAGE->{ 'TRIANGLES' }->{ $triangle->{ 'ID' } };
}

############################################################
# Flip the common edge between the 2 triangles

sub flipEdgeTriangles {

    my $edge_id = shift;
    my $flip_info1 = shift;
    my $flip_info2 = shift;

    my $triangle1 = addTriangle(
        $IMAGE->{ 'EDGES' }->{ $edge_id }->{ 'NODE1' },       
        $flip_info1->{ 'TRIANGLE' }->{ 'NODES' }->[ $flip_info1->{ 'NODE_IDX' } ],
        $flip_info2->{ 'TRIANGLE' }->{ 'NODES' }->[ $flip_info2->{ 'NODE_IDX' } ],
        $TRUE,      # check orientation
    );

    my $triangle2 = addTriangle(
        $IMAGE->{ 'EDGES' }->{ $edge_id }->{ 'NODE2' },       
        $flip_info1->{ 'TRIANGLE' }->{ 'NODES' }->[ $flip_info1->{ 'NODE_IDX' } ],
        $flip_info2->{ 'TRIANGLE' }->{ 'NODES' }->[ $flip_info2->{ 'NODE_IDX' } ],
        $TRUE,      # check orientation
    );

    removeTriangle( $flip_info1->{ 'TRIANGLE' } );
    removeTriangle( $flip_info2->{ 'TRIANGLE' } );
}

############################################################
# Check a triangulation edge for Delaunay consistency

sub processEdge {

    my $edge_id = shift;
    my $pending_edges = shift;

    printf "Processing edge $edge_id\n" if $options{ 'v' };

    my $edge = $IMAGE->{ 'EDGES' }->{ $edge_id };
    return if ( scalar @{ $edge->{ 'TRIANGLES' } } != 2 );

    my @flip_info;

    for ( my $t = 0; $t < 2; $t++ ) {

        my $triangle = $IMAGE->{ 'TRIANGLES' }->{ $edge->{ 'TRIANGLES' }->[ $t ] };

        for ( my $n = 0; $n < 3; $n++ ) {
            
            if (
                ( $triangle->{ 'NODES' }->[ $n ]->{ 'ID' } != $edge->{ 'NODE1' }->{ 'ID' } ) &&
                ( $triangle->{ 'NODES' }->[ $n ]->{ 'ID' } != $edge->{ 'NODE2' }->{ 'ID' } )
            ) {
                $flip_info[ $t ] = {
                    'ANGLE'     => $triangle->{ 'ANGLES' }->[ $n ],
                    'NODE_IDX'  => $n,
                    'TRIANGLE'  => $IMAGE->{ 'TRIANGLES' }->{ $edge->{ 'TRIANGLES' }->[ $t ] },
                };

                last;
            }
        }
    }

    if ( $flip_info[ 0 ]->{ 'ANGLE' } + $flip_info[ 1 ]->{ 'ANGLE' } > 180 ) {

        # Now add in the surrounding quadrilateral edges back into the pending hash
        for ( my $t = 0; $t < 2; $t++ ) {

            for ( my $e = 0; $e < 3; $e++ ) {

                my $triangle = $IMAGE->{ 'TRIANGLES' }->{ $edge->{ 'TRIANGLES' }->[ $t ] };

                my $id = getEdgeId( $triangle->{ 'NODES' }->[ $e - 1 ], $triangle->{ 'NODES' }->[ $e ] );
                next if $id eq $edge_id;
                next if defined $pending_edges->{ $id };

                my $edge = $IMAGE->{ 'EDGES' }->{ $id };
                next if scalar @{ $edge->{ 'TRIANGLES' } } != 2;

                printf "adding edge $id to pending list\n" if $options{ 'v' };
                $pending_edges->{ $id } = $edge;
            }
        }

        flipEdgeTriangles( $edge_id, @flip_info );
    }
}

############################################################
# Make sure the triangulation is Delaunay 

sub fixTriangulation {

    my %pending_edges = %{ $IMAGE->{ 'EDGES' } };

    # Remove the boundary edges
    for ( my $b = 0; $b < scalar @{ $IMAGE->{ 'BOUNDARY' } }; $b++ ) {
        
        my $edge_id = getEdgeId( 
                        $IMAGE->{ 'BOUNDARY' }->[ $b - 1 ],
                        $IMAGE->{ 'BOUNDARY' }->[ $b ],
        );

        delete $pending_edges{ $edge_id };
    }

    while ( 1 ) {

        my $edge_count = scalar ( keys %pending_edges );
        last unless $edge_count;

        for my $edge_id ( keys %pending_edges ) {

            processEdge( $edge_id, \%pending_edges );
            delete $pending_edges{ $edge_id };
        }
    }

    return;
}
    
############################################################
# Find the index of an id in the given node list

sub findNodeIDInList {

    my $node_id = shift;
    my $node_list = shift;

    for ( my $t = 0; $t < scalar @{ $node_list }; $t++ ) {

        return $t if $node_id eq $node_list->[ $t ]->{ 'ID' };
    }

    return -1;
}
    
############################################################
# Create Voronoi polygons

sub nodeFromCircumCircle {

    my $id = shift;

    my $triangle = $IMAGE->{ 'TRIANGLES' }->{ $id };
    my $circum_circle = $triangle->{ 'CIRCUMCIRCLE' };

    return {
        'X' =>  $triangle->{ 'CIRCUMCIRCLE' }->{ 'X' },
        'Y' =>  $triangle->{ 'CIRCUMCIRCLE' }->{ 'Y' },
    }
}

############################################################
# Create Voronoi polygons

sub createPolygons {

    # mark the boundary nodes - so they can be skipped later
    for my $node ( @{ $IMAGE->{ 'BOUNDARY' } } ) {
        $node->{ 'BOUNDARY' } = $TRUE;
    }

    for ( my $n = 0; $n < scalar @{ $IMAGE->{ 'NODES' } }; $n++ ) {

        my $node = $IMAGE->{ 'NODES' }->[ $n ];
        next if $node->{ 'BOUNDARY' };

        my @triangle_ids;
        for my $t ( @{ $node->{ 'TRIANGLES' } } ) {
            next unless $IMAGE->{ 'TRIANGLES' }->{ $t };
            push @triangle_ids, $t;
        }

        my $init_triangle = $IMAGE->{ 'TRIANGLES' }->{ shift @triangle_ids };

        my $idx = findNodeIDInList( $node->{ 'ID' }, $init_triangle->{ 'NODES' } ) - 1;
        my $id = $init_triangle->{ 'NODES' }->[ $idx ]->{ 'ID' };
        my @node_list = ( 
                nodeFromCircumCircle( $init_triangle->{ 'ID' } ),
        );

        while ( scalar @triangle_ids ) {

            for ( my $t = 0; $t < scalar @triangle_ids; $t++ ) {

                my $tid = $triangle_ids[ $t ];
                
                my $triangle = $IMAGE->{ 'TRIANGLES' }->{ $tid };
                my $idx = findNodeIDInList( $id, $triangle->{ 'NODES' } );
                next if $idx < 0;

                $idx = ( $idx + 1 ) % 3;

                $id = $triangle->{ 'NODES' }->[ $idx ]->{ 'ID' };
                push @node_list, nodeFromCircumCircle( $tid );

                splice @triangle_ids, $t, 1;
                last;
            }
        }

        $IMAGE->{ 'POLYGONS' }->{ $node->{ 'ID' } } = {
            'ID'    =>  $node->{ 'ID' },
            'NODES' =>  \@node_list,
        };
    }
}

############################################################
# Process the image

sub processImage {

    my $triangle = addTriangle(
            $IMAGE->{ 'NODES' }->[ 0 ],
            $IMAGE->{ 'NODES' }->[ 1 ],
            $IMAGE->{ 'NODES' }->[ 2 ],
            $TRUE,      # check orientation
    );

    my $nodes = $triangle->{ 'NODES' };

    my $idx = $nodes->[ 0 ]->{ 'X' } < $nodes->[ 1 ]->{ 'X' } ? 0 : 1;
    $idx = $nodes->[ 2 ]->{ 'X' } < $nodes->[ $idx ]->{ 'X' } ? 2 : $idx;

    # the triangle nodes were added anti-clockwise from the leftmost node
    push @{ $IMAGE->{ 'BOUNDARY' } },
            $nodes->[ $idx ],
            $nodes->[ ( $idx + 1 ) % 3 ],
            $nodes->[ ( $idx + 2 ) % 3 ],
    ;

    printf "Initial triangulation\n" if $options{ 'v' };
    initialTriangulation();
    return if ( $options{ 'X' } || '' ) eq "initTriangulation";

    printf "Fixing triangulation\n" if $options{ 'v' };
    fixTriangulation();
    return if ( $options{ 'X' } || '' ) eq "fixedTriangulation";

    printf "Creating polygons\n" if $options{ 'v' };
    createPolygons();
}

############################################################
# Write postscript for the colored sphere

sub writeSphereRenderer {

    my $fh = shift;
    my $name = shift;
    my $rgb = shift;

    my $sphere_def = $IMAGE->{ 'SPHERE-DEF' };

    printf $fh "/%s {\n", $name;
    printf $fh "    gsave\n";
    printf $fh "    /sz %d def\n", 2 * $sphere_def->{ 'samples' };
    printf $fh "    0 1 360 {\n";
    printf $fh "        /a exch def\n";
    printf $fh "        .5 a cos 2 div add .5 a sin 2 div add a 0 gt { lineto } { moveto } ifelse\n";
    printf $fh "    } for\n";
    printf $fh "    clip\n";
    printf $fh "    sz sz 8 [sz 0 0 sz neg 0 sz]\n";
    printf $fh "    {<";
    
    for( my $y = -$sphere_def->{ 'samples' }; $y < $sphere_def->{ 'samples' }; $y++ ) {
    
        for( my $x = -$sphere_def->{ 'samples' }; $x <$sphere_def->{ 'samples' }; $x++ ) {
    
            my $d = $x * $x + $y * $y;
    
            if ( $d > $sphere_def->{ 'samples' } * $sphere_def->{ 'samples' } ) {
    
                printf $fh "000000";
            }
            else {
    
                my $z = sqrt( $sphere_def->{ 'samples' } * $sphere_def->{ 'samples' } - $d );
    
                my $v = (
                    $x * $sphere_def->{ 'lamp_x' } +
                    $y * $sphere_def->{ 'lamp_y' } +
                    $z * $sphere_def->{ 'lamp_z' }
                ) / $sphere_def->{ 'samples' };
    
                $v = 0 if $v < 0;
    
                if ( $v * 1000 > 998 ) {
    
                    printf $fh "FFFFFF";
                }
                elsif ( $v * 1000 > $sphere_def->{ 'sheen' } ) {
    
                    my $v1 = 1000 * $v - $sphere_def->{ 'sheen' };
                    my $v2 = 1000 - $sphere_def->{ 'sheen' };
                    my $v3 = ( $v1 * $v1 ) / ( $v2 * $v2 );
    
                    my $r = $rgb->[ 0 ] * $v + $v3 * ( 255 - $rgb->[ 0 ] );
                    my $g = $rgb->[ 1 ] * $v + $v3 * ( 255 - $rgb->[ 1 ] );
                    my $b = $rgb->[ 2 ] * $v + $v3 * ( 255 - $rgb->[ 2 ] );
    
                    printf $fh "%02x%02x%02x", 
                                min( $r, 255 ),
                                min( $g, 255 ),
                                min( $b, 255 ),
                    ;
                }
                else {
    
                    $v = $sphere_def->{ 'min-color-pcnt' } / 100
                                    if ( $v * 100 < $sphere_def->{ 'min-color-pcnt' } );
    
                    printf $fh "%02x%02x%02x", 
                                min( $rgb->[ 0 ] * $v, 255 ),
                                min( $rgb->[ 1 ] * $v, 255 ),
                                min( $rgb->[ 2 ] * $v, 255 ),
                    ;
                }
            }
        }
    
        printf $fh "    \n";
    }
    
    printf $fh ">}\n";
    printf $fh "    false 3 colorimage\n";
    printf $fh "    grestore\n";
    printf $fh "} def\n";
}

############################################################
# Find the circumcircle of the 3 nodes

sub getCircumCircle {

    my $node1 = shift;
    my $node2 = shift;
    my $node3 = shift;

    my $d = 2 * (
        $node1->{ 'X' } * ( $node2->{ 'Y' } - $node3->{ 'Y' } ) +
        $node2->{ 'X' } * ( $node3->{ 'Y' } - $node1->{ 'Y' } ) +
        $node3->{ 'X' } * ( $node1->{ 'Y' } - $node2->{ 'Y' } ) 
    );

    # the  squares of the coordinates
    my $n1x2 = $node1->{ 'X' } * $node1->{ 'X' };
    my $n2x2 = $node2->{ 'X' } * $node2->{ 'X' };
    my $n3x2 = $node3->{ 'X' } * $node3->{ 'X' };
    my $n1y2 = $node1->{ 'Y' } * $node1->{ 'Y' };
    my $n2y2 = $node2->{ 'Y' } * $node2->{ 'Y' };
    my $n3y2 = $node3->{ 'Y' } * $node3->{ 'Y' };

    my $cx = ( 
        ( $n1x2 + $n1y2 ) * ( $node2->{ 'Y' } - $node3->{ 'Y' } ) +
        ( $n2x2 + $n2y2 ) * ( $node3->{ 'Y' } - $node1->{ 'Y' } ) +
        ( $n3x2 + $n3y2 ) * ( $node1->{ 'Y' } - $node2->{ 'Y' } )
    ) / $d;

    my $cy = ( 
        ( $n1x2 + $n1y2 ) * ( $node3->{ 'X' } - $node2->{ 'X' } ) +
        ( $n2x2 + $n2y2 ) * ( $node1->{ 'X' } - $node3->{ 'X' } ) +
        ( $n3x2 + $n3y2 ) * ( $node2->{ 'X' } - $node1->{ 'X' } )
    ) / $d;

    # a translated set of nodes
    my $nbx = $node2->{ 'X' } - $node1->{ 'X' };
    my $nby = $node2->{ 'Y' } - $node1->{ 'Y' };
    my $ncx = $node3->{ 'X' } - $node1->{ 'X' };
    my $ncy = $node3->{ 'Y' } - $node1->{ 'Y' };

    my $nd = 2 * ( $nbx * $ncy - $nby * $ncx );

    my $nx = (
        $ncy * ( $nbx * $nbx + $nby * $nby ) -
        $nby * ( $ncx * $ncx + $ncy * $ncy ) 
    ) / $nd;
 
    my $ny = (
        $nbx * ( $ncx * $ncx + $ncy * $ncy ) -
        $ncx * ( $nbx * $nbx + $nby * $nby ) 
    ) / $nd;
 
    return {
        'X'  =>  $cx,
        'Y'  =>  $cy,
        'RADIUS'    =>  sqrt( $nx * $nx + $ny * $ny ),
    };
}

############################################################
# Render the triangulation circumcircles

sub renderCircumCircles {

    my $fh = shift;

    printf $fh "gsave 0 1 1 setrgbcolor\n";

    for my $id ( keys $IMAGE->{ 'TRIANGLES' } ) {

        my $triangle =  $IMAGE->{ 'TRIANGLES' }->{ $id };
        my $circum_circle = getCircumCircle( @{ $triangle->{ 'NODES' } } );

        printf $fh "%f %f %f circle\n",
            $circum_circle->{ 'RADIUS' },
            $circum_circle->{ 'X' },
            $circum_circle->{ 'Y' },
        ;

        printf $fh "%f %f %f disc\n",
            $NODE_RADIUS / $IMAGE->{ 'SCALE' },
            $circum_circle->{ 'X' },
            $circum_circle->{ 'Y' },
        ;

    }

    printf $fh "grestore\n";
}

############################################################
# Return the centroid of the triangle

sub triangleCentroid {

    my $triangle = shift;

    return {
            'X' => (
                    $triangle->{ 'NODES' }->[ 0 ]->{ 'X' } +
                    $triangle->{ 'NODES' }->[ 1 ]->{ 'X' } +
                    $triangle->{ 'NODES' }->[ 2 ]->{ 'X' }
                ) / 3,
            'Y' => (
                    $triangle->{ 'NODES' }->[ 0 ]->{ 'Y' } +
                    $triangle->{ 'NODES' }->[ 1 ]->{ 'Y' } +
                    $triangle->{ 'NODES' }->[ 2 ]->{ 'Y' }
                ) / 3,
    }
}

############################################################
# Return the midpoint of the edge

sub edgeMidpoint {

    my $edge = shift;

    return {
            'X' => (
                    $edge->{ 'NODE1' }->{ 'X' } +
                    $edge->{ 'NODE2' }->{ 'X' } 
                ) / 2,
            'Y' => ( 
                    $edge->{ 'NODE1' }->{ 'Y' } + 
                    $edge->{ 'NODE2' }->{ 'Y' }
                ) / 2,
    }
}

############################################################
# Render the Voronoi diagram of the triangulation

sub renderVoronoi {

    my $fh = shift;

    printf $fh "gsave 1 1 0 setrgbcolor\n";

    for my $id ( keys $IMAGE->{ 'EDGES' } ) {

        my $edge = $IMAGE->{ 'EDGES' }->{ $id };

        my $triangle_count = scalar @{ $edge->{ 'TRIANGLES' } };
        next unless $triangle_count;

        for ( my $t = 0; $t < 2; $t++ ) {

            my $triangle = $IMAGE->{ 'TRIANGLES' }->{ $edge->{ 'TRIANGLES' }->[ $t ] };
            my $circum_circle = $triangle->{ 'CIRCUMCIRCLE' };

            printf $fh "%f %f %s ",
                        $circum_circle->{ 'X' },
                        $circum_circle->{ 'Y' },
                        $t ? 'ln' : 'mv',
            ;

            if ( $triangle_count == 1 ) {

                my $ang = atan2(
                    $edge->{ 'NODE1' }->{ 'Y' } - $edge->{ 'NODE2' }->{ 'Y' },
                    $edge->{ 'NODE1' }->{ 'X' } - $edge->{ 'NODE2' }->{ 'X' },
                ) + ( pi / 2 );

                printf $fh "%f %f mv %f %f ln ",
                        $circum_circle->{ 'X' },
                        $circum_circle->{ 'Y' }, 
                        $IMAGE_WID * cos( $ang ) + $circum_circle->{ 'X' },
                        $IMAGE_DEP * sin( $ang ) + $circum_circle->{ 'Y' },
                ;

                last;
            }
        }

        print $fh "stroke\n";
    }

    printf $fh "grestore\n";
}

############################################################
# Render the polygons

sub renderPolygons {

    my $fh = shift;

    printf $fh "gsave\n";

    for my $id ( keys $IMAGE->{ 'POLYGONS' } ) {

        my $polygon = $IMAGE->{ 'POLYGONS' }->{ $id };

        for ( my $n = 0; $n < scalar @{ $polygon->{ 'NODES' } }; $n++ ) {

            printf $fh "%f %f %s ", 
                    $polygon->{ 'NODES' }->[ $n ]->{ 'X' }, 
                    $polygon->{ 'NODES' }->[ $n ]->{ 'Y' }, 
                    $n ? 'ln' : 'mv',
            ;
        }
    
        printf $fh ".3 0 0 setrgbcolor fill %% Polygon %s dist %f\n",
                                $id, $IMAGE->{ 'NODES' }->[ $id ]->{ 'DIST' };
    }

    printf $fh "grestore\n";
}

############################################################
# Render the triangles

sub renderTriangles {

    my $fh = shift;

    printf $fh "gsave\n";

    for my $id ( keys $IMAGE->{ 'TRIANGLES' } ) {

        my $triangle = $IMAGE->{ 'TRIANGLES' }->{ $id };
        my $nodes = $triangle->{ 'NODES' };

        printf $fh "%% Triangle %s\n", $id;
        printf $fh "%%  dist %f\n", distance( $triangle->{ 'CIRCUMCIRCLE' }, $IMAGE->{ 'ORIGIN' } );

        printf $fh "%f %f mv %f %f ln %f %f ln closepath\n",
            $nodes->[ 0 ]-> { 'X' }, $nodes->[ 0 ]-> { 'Y' },
            $nodes->[ 1 ]-> { 'X' }, $nodes->[ 1 ]-> { 'Y' },
            $nodes->[ 2 ]-> { 'X' }, $nodes->[ 2 ]-> { 'Y' },
        ;

        printf $fh "1 1 1 setrgbcolor stroke\n";

        if ( $options{ 'L' } ) { # label the triangle
            printf $fh "1 setgray %f %f 6 sub mv ",
                ( $nodes->[ 0 ]-> { 'X' } + $nodes->[ 1 ]-> { 'X' } + $nodes->[ 2 ]-> { 'X' } ) / 3,
                ( $nodes->[ 0 ]-> { 'Y' } + $nodes->[ 1 ]-> { 'Y' } + $nodes->[ 2 ]-> { 'Y' } ) / 3,
            ;

            printf $fh "(%s) dup stringwidth pop -2 div 0 rmoveto show\n", $triangle->{ 'ID' };
        }
    }

    printf $fh "grestore\n";
}

############################################################
# Render the node boundary

sub renderBoundary {

    my $fh = shift;

    for ( my $b = 0; $b < scalar @{ $IMAGE->{ 'BOUNDARY' } }; $b++ ) {

        printf $fh "%f %f %s ", 
                $IMAGE->{ 'BOUNDARY' }->[ $b ]->{ 'X' },
                $IMAGE->{ 'BOUNDARY' }->[ $b ]->{ 'Y' },
                $b ? 'ln' : 'mv',
        ;
    }

    printf $fh "closepath 1 1 0 setrgbcolor stroke\n";
}

############################################################
# Render the nodes

sub renderNodes {

    my $fh = shift;

    printf $fh "1 setgray\n";

    for ( my $i = 0; $i < scalar @{ $IMAGE->{ 'NODES' } }; $i++ ) {

        my $node = $IMAGE->{ 'NODES' }->[ $i ];

        printf $fh "%f %f %f disc\n", 
                        $NODE_RADIUS / $IMAGE->{ 'SCALE' },
                        $node-> { 'X' },
                        $node-> { 'Y' },
        ;

        if ( $options{ 'L' } ) {    # label the node
            printf $fh "%f %f 16 sub mv ", $node-> { 'X' }, $node-> { 'Y' };
            printf $fh "(%d) dup stringwidth pop -2 div 0 rmoveto show\n", $node-> { 'ID' };
        }
    }
}

############################################################
# Render the image

sub renderImage {

    open my $fh, '>', $PS_TEMP or die "Could not open $PS_TEMP: $!\n";

    writeImageHeader( $fh );

    renderPolygons( $fh ) if $options{ 'P' };
    renderTriangles( $fh ) if $options{ 'T' };
    renderCircumCircles( $fh ) if $options{ 'C' };
    renderVoronoi( $fh ) if $options{ 'V' };
    renderNodes( $fh ) if $options{ 'N' };
    renderBoundary( $fh ) if $options{ 'B' };

    close $fh;

    printf "Rendering: done     \nConverting..." if $options{ 'v' };

    system( sprintf "convert '%s' '%s'", $PS_TEMP, $OUTPUT_FILE );
    if ( $options{ 'k' } ) {
        printf "Kept: %s\n", $PS_TEMP;
    }
    else {
        unlink $PS_TEMP;
    }

    printf "\nDone\n" if $options{ 'v' };
}

############################################################
# Scale the image to fit the rendering area

sub writeImageHeader {

    my $fh = shift;

    printf $fh "%%!PS\n%%%%BoundingBox: 0 0 %f %f\n%%%%EndComments\n

        /imgw %f def
        /imgd %f def

        <<
            /PageSize [ imgw imgd ]
            /ImagingBBox null
        >> setpagedevice

        /sc 1 def
        /mv { sc mul exch sc mul exch moveto } def
        /ln { sc mul exch sc mul exch lineto } def

        /Arial-Bold findfont 12 %f div scalefont setfont

        /circle {  %% fill rad x y disc -
                false
                4 1 roll
                draw_disc
        } def

        /disc {  %% fill rad x y disc -
                true
                4 1 roll
                draw_disc
        } def

        /draw_disc { %% fill rad x y disc -
            newpath
            sc mul exch sc mul exch
            3 2 roll
            0 360 arc
            { fill } { stroke } ifelse
        } def

        0 0 mv
        imgw 2 mul 0 ln
        imgw 2 mul imgd 2 mul ln
        0 imgd 2 mul ln
        0 setgray fill

        %% rotate the image
        imgw 2 div imgd 2 div translate
        %f rotate
        %f dup scale
        imgw -2 div imgd -2 div translate

        %% centre the image
        %f %f translate

        0.5 %f div setlinewidth
    ",
    $IMAGE_WID, $IMAGE_DEP,
    $IMAGE_WID, $IMAGE_DEP,
    $IMAGE->{ 'SCALE' },
    $options{ 'r' } || 0,
    $IMAGE->{ 'SCALE' },
    ( $IMAGE_WID - $IMAGE->{ 'BOUNDS' }->[ 0 ] - $IMAGE->{ 'BOUNDS' }->[ 2 ] ) / 2.0,
    ( $IMAGE_DEP - $IMAGE->{ 'BOUNDS' }->[ 1 ] - $IMAGE->{ 'BOUNDS' }->[ 3 ] ) / 2.0,
    $IMAGE->{ 'SCALE' },
    ;
}

############################################################
# Help doco

sub help {

    print "\n@_\n" if @_;

    my $node_patterns = "";
    map {
        $node_patterns .= sprintf "% 14s - %s\n", $_, $NODE_PATTERNS->{ $_ }->[ 0 ];
    } ( keys $NODE_PATTERNS );

    my $exit_options = join ", ", @{ $EXIT_OPTIONS };

    printf "\n$0 " .
                "[-c <count>] [-h] [-k] [-n <int>] [-r <ang>] [-s <sc>] [-v]" .
                "[-B [-C] [-D <dep>] [-I] [-L] [-M <mrgn>] [-N] [-O <file>] " .
                "[-P] [-T] [-V] [-W <wid>] [-X <id>] " .
                "<node layout>

    where
        -c  number of nodes (default $DEFAULT_NODE_COUNT)
        -h  this help documnetation
        -k  retain the temp ps file ($PS_TEMP)
        -n  the rand() seed to use for nodes (default $DEFAULT_NODE_SEED)
        -r  rotation of the image (default 0)
        -s  image scaling factor (default 1)
        -v  verbose logging

        -B  draw the node boundary
        -C  draw the circumcircles
        -D  depth of the image (default $DEFAULT_IMAGE_DEP)
        -I  dump the image info
        -L  label the nodes and triangles
        -M  image margin (default $DEFAULT_IMAGE_MRGN)
        -N  draw the nodes
        -O  output file (default $DEFAULT_OUTPUT_FILE)
        -P  draw the polygons
        -T  draw the triangles
        -V  draw the Voronoi diagram for the triangulation
        -W  width of the image (default $DEFAULT_IMAGE_WID)
        -X  exit after processing this step

    valid node patterns:
    \n$node_patterns

    valid options for the -X switch
        $exit_options
    \n";

    exit;
}

__END__

Delauney triangulation
http://www.geom.uiuc.edu/~samuelp/del_project.html

Circumcircles
https://en.wikipedia.org/wiki/Circumscribed_circle
https://www.mathopenref.com/trianglecircumcircle.html

Problems
./delaunay.pl -c 10 -L 7

