#!/usr/bin/perl

use warnings;
use strict;

############################################################
#
#   map.pl
#
#   Just playin
#
############################################################

use Getopt::Std;
use Storable 'dclone';
use Math::Trig;
use Data::Dumper;

############################################################
# Prepare the environment

my $IMAGE = {};

my %options;
getopts( 'hk:o:D:N:R:S:W:X:Y:', \%options );

help() if $options{ 'h' };

$IMAGE->{ 'PS_TEMP' } = $options{ 'k' } || 'map.ps';
$IMAGE->{ 'DEST' } = $options{ 'o' } || 'map.pdf';

$IMAGE->{ 'SCALE' } = $options{ 'S' } || 1.0;
$IMAGE->{ 'DEPTH' } = $options{ 'D' } || 1000;
$IMAGE->{ 'WIDTH' } = $options{ 'W' } || 1000;

$IMAGE->{ 'IMG-XC' } = $options{ 'X' } || 0.5;
$IMAGE->{ 'IMG-YC' } = $options{ 'Y' } || 0;
$IMAGE->{ 'IMG-RAD' } = $options{ 'R' } || 0.5;

$IMAGE->{ 'NUM-DISCS' } = $options{ 'N' } || 50;

my @SPECTRUM = (
        [255, 0 , 0],     # Red
        [255, 127, 0],    # Orange
        [255, 255, 0],    # Yellow
        [0, 255, 0],      # Green
        [0, 0, 255],      # Blue
        [75, 0, 130],     # Indigo
        [148, 0, 211],    # Violet
);

###########################################################
# Process the image

setupImage();

for( my $x = 0; $x < $IMAGE->{ 'NUM-DISCS' }; $x++ ) {

    my $rad = $IMAGE->{ 'WIDTH' } / ( 2 * $IMAGE->{ 'NUM-DISCS' } );

    my $xoff = ( 1 + 2 * $x )  * $rad;

    my $rx = 2 * ( $x / $IMAGE->{ 'NUM-DISCS' } - 0.5 );

    for( my $y = 0; $y < $IMAGE->{ 'NUM-DISCS' }; $y++ ) {

        my $yoff = ( 1 + 2 * $y ) * $rad;

        my $ry = 2 * ( $y / $IMAGE->{ 'NUM-DISCS' } - 0.5 );
        my $r = sqrt( $rx * $rx + $ry * $ry ) / sqrt ( 2.0001 );

        my $blk = int($#SPECTRUM * $r);
        my $from = $SPECTRUM[ $blk ];
        my $to = $SPECTRUM[ $blk + 1 ];
    
        my $blkwid = 1 / $#SPECTRUM;
        my $v = $r - $blk * $blkwid;

        my $disc = {
        
            'RGB'   =>  [
                            ( $from->[0] + ($to->[0] - $from->[0]) * $v / $blkwid ) / 256,
                            ( $from->[1] + ($to->[1] - $from->[1]) * $v / $blkwid ) / 256,
                            ( $from->[2] + ($to->[2] - $from->[2]) * $v / $blkwid ) / 256,
            ],
            'PTS'   =>  [],
        };

        for( my $a = 0; $a < 360; $a += 1 ) {

            push @{ $disc->{ 'PTS' } }, Cnew(
                $xoff + $rad * cos( deg2rad( $a ) ) - $IMAGE->{ 'WIDTH' } / 2,
                $yoff + $rad * sin( deg2rad( $a ) ) - $IMAGE->{ 'DEPTH' } / 2,
            );
        }

    #    drawDisc( $disc );

        applyB( $disc );
        drawDisc( $disc );
    }
}

exit;

###########################################################
# Function A

sub A {

    return Cinv( 
        Csub( Cnew( 1, 0 ), Cmul( Cnew( 0, 2 ), shift ) )
    );
}

############################################################
# Function B

sub B {

    my $z = shift;

    return Cdiv( 
        Cadd( Cmul( Cnew( 1, -1 ), $z ), Cnew( 1, 0 ) ),
        Cadd( Cnew( 1, 1 ), $z ),
    );
}

############################################################
# Apply function A to the given array of points

sub applyA
{
    my $disc = shift;

    for ( my $p = 0; $p < scalar @{ $disc->{ 'PTS' } }; $p++ ) {
        my $c = img2Plane( $disc->{ 'PTS' }->[ $p ] );
        my $z = A( $c );
        $disc->{ 'PTS' }->[ $p ] = plane2Img( $z );
    }
}

############################################################
# Apply function B to the given array of points

sub applyB
{
    my $disc = shift;

    for ( my $p = 0; $p < scalar @{ $disc->{ 'PTS' } }; $p++ ) {
        my $c = img2Plane( $disc->{ 'PTS' }->[ $p ] );
        my $z = B( $c );
        $disc->{ 'PTS' }->[ $p ] = plane2Img( $z );
    }
}

############################################################
# Map a plane coord to the image

sub plane2Img {

    my $p = shift;

    return {
        'X' => ( 1 + ( $p->{ 'X' } - $IMAGE->{ 'IMG-XC' } ) / $IMAGE->{ 'IMG-RAD' } ) * $IMAGE->{ 'WIDTH' } / 2,
        'Y' => ( 1 + ( $p->{ 'Y' } - $IMAGE->{ 'IMG-YC' } ) / $IMAGE->{ 'IMG-RAD' } ) * $IMAGE->{ 'DEPTH' } / 2,
    };
}

############################################################
# Map an image coord to the plane

sub img2Plane {

    my $p = shift;

    return {
        'X' => $IMAGE->{ 'IMG-RAD' } * ( 2 * $p->{ 'X' } / $IMAGE->{ 'WIDTH' } - 1 ) + $IMAGE->{ 'IMG-XC' },
        'Y' => $IMAGE->{ 'IMG-RAD' } * ( 2 * $p->{ 'Y' } / $IMAGE->{ 'DEPTH' } - 1 ) + $IMAGE->{ 'IMG-YC' },
    };
}

############################################################
# Draw the given disc

sub drawDisc {

    my $disc = shift;

    my $fh = $IMAGE->{ 'FH' };

    printf $fh "%f %f %f setrgbcolor ", @{ $disc->{ 'RGB' } };

    my $op = 'mv';
    map {
        printf $fh "%f %f %s ",
                    $_->{ 'X' } * $IMAGE->{ 'SCALE' },
                    $_->{ 'Y' } * $IMAGE->{ 'SCALE' }, 
                    $op;
        $op = 'ln';
    } ( @{ $disc->{ 'PTS' } } );

    printf $fh "closepath stroke\n";
}

############################################################
# Invert a complex value

sub Cinv {

    return Cdiv( Cnew( 1, 0 ), shift );
}

############################################################
# Add 2 complex values

sub Cadd {

    my $z1 = shift;
    my $z2 = shift;

    return {
        'X' =>  $z1->{ 'X' } + $z2->{ 'X' },
        'Y' =>  $z1->{ 'Y' } + $z2->{ 'Y' },
    }
}

############################################################
# Subtract 2 complex values

sub Csub {

    my $z1 = shift;
    my $z2 = shift;

    return {
        'X' =>  $z1->{ 'X' } - $z2->{ 'X' },
        'Y' =>  $z1->{ 'Y' } - $z2->{ 'Y' },
    }
}

############################################################
# Create a new complex value from a coordinate pair

sub Cnew {

    return {
        'X' =>  shift,
        'Y' =>  shift,
    }
}

############################################################
# Duplicate a complex value

sub Cdup {

    my $z = shift;

    return {
        'X' =>  $z->{ 'X' },
        'Y' =>  $z->{ 'Y' },
    }
}

############################################################
# Multiply 2 complex values

sub Cmul {

    my $z1 = shift;
    my $z2 = shift;

    return {
        'X' =>  $z1->{ 'X' } * $z2->{ 'X' } - $z1->{ 'Y' } * $z2->{ 'Y' },
        'Y' =>  $z1->{ 'X' } * $z2->{ 'Y' } + $z1->{ 'Y' } * $z2->{ 'X' },
    }
}

############################################################
# Divide 2 complex values

sub Cdiv {

    my $z1 = shift;
    my $z2 = shift;

    my $v = $z2->{ 'X' } * $z2->{ 'X' } + $z2->{ 'Y' } * $z2->{ 'Y' };

    return {
        'X' =>  ( $z1->{ 'X' } * $z2->{ 'X' } + $z1->{ 'Y' } * $z2->{ 'Y' } ) / $v,
        'Y' =>  ( $z1->{ 'Y' } * $z2->{ 'X' } - $z1->{ 'X' } * $z2->{ 'Y' } ) / $v,
    }
}

############################################################
# Setup the image

sub setupImage {

    open my $fh, '>', $IMAGE->{ 'PS_TEMP' } or die "Could not open $IMAGE->{ 'PS_TEMP' }: $!\n";
    
    printf $fh "%%!PS\n%%%%BoundingBox: 0 0 1000 1000\n%%%%EndComments\n
    
        /imgw %f def
        /imgd %f def

        <<
            /PageSize [ imgw imgd ]
            /ImagingBBox null
        >> setpagedevice
    
        /mv { moveto } bind def
        /ln { lineto } bind def
    
        /Arial-Bold findfont 16 scalefont setfont

        0 0 mv
        imgw 0 ln
        imgw imgd ln
        0 imgd ln
        0 setgray fill

        imgw 2 div imgd 2 div translate

        1 1 0 setrgbcolor
    ",
    $IMAGE->{ 'WIDTH' }, $IMAGE->{ 'DEPTH' },
    ;

    $IMAGE->{ 'FH' } = $fh;
}

############################################################
# Render the image

sub renderImage {

    close $IMAGE->{ 'FH' };

    system( sprintf "convert '%s' '%s'", $IMAGE->{ 'PS_TEMP' }, $IMAGE->{ 'DEST' } );
    unlink $IMAGE->{ 'PS_TEMP' } unless $options{ 'k' };
}

############################################################
# Help doco

sub help {

    print "\n@_\n" if @_;

    printf "\n$0 " .
        "[-h] [-k <path>] [-o <path>]
        [-D <depth>] [-N <discs>] [-S <scale>] [-W <width>]
        [-X <xoff>] [-Y <yoff>] [-R <rad>]

        where
            -h          this help doco
            -k <path>   temp ps file (to retain)
            -o <path>   destination image (default: mobius.png)

        drawing switches
            -D <depth>  image depth (default 1000)
            -N <discs>  number of discs (default 50)
            -R <rad>    image radius (default 0.5)
            -S <scale>  drawing scale (default 1.0)
            -W <width>  image width (default 1000)
            -X <xoff>   image centre X (default 0)
            -Y <yoff>   image centre Y (default 0)
    \n";

    exit;
}
