#!/bin/sh

dest="~/Projects/ipad-pics"
dest="./images"

format=jpg
format=pdf

mkdir $dest 2> /dev/null

./p1-tiling.pl -i 5 -s 30 -S fill -W 1024 -D 768 -F $dest/p1-i5-s30.$format
./p1-tiling.pl -i 5 -s 30 -S fill -W 1024 -D 768 -d -F $dest/p1-i5-s30-d.$format
./p1-tiling.pl -i 5 -s 30 -S fill -r 450 -F $dest/p1-i5-s30-r450.$format
./p1-tiling.pl -i 5 -s 30 -S fill -r 450 -d -F $dest/p1-i5-s30-r450-d.$format

./p1-tiling.pl -i 5 -s 30 -W 1024 -D 768 -F $dest/p1-l-i5-s30.$format
./p1-tiling.pl -i 5 -s 30 -W 1024 -D 768 -d -F $dest/p1-l-i5-s30-d.$format
./p1-tiling.pl -i 5 -s 30 -r 450 -F $dest/p1-l-i5-s30-r450.$format
./p1-tiling.pl -i 5 -s 30 -r 450 -d -F $dest/p1-l-i5-s30-r450-d.$format

./p2-deflation.pl -r 'KK---K--K--K' -d 6 -s 3.5 -f -F $dest/p2-d6-K-s3p5.$format
./p2-deflation.pl -r 'KK---K--K--K' -d 8 -s 8 -f -W 1024 -D 768 -F $dest/p2-d6-D-s8.$format
./p2-deflation.pl -r 'D-D---D--D--D' -d 6 -s 3.7 -f -F $dest/p2-d6-D-s3p7.$format
./p2-deflation.pl -r 'D-D---D--D--D' -d 8 -s 8 -f -W 1024 -D 768 -F $dest/p2-d6-D-s8.$format

./p2-deflation.pl -r 'KK---K--K--K' -a none -d 6 -s 3.5 -F $dest/p2-l-d6-K-s3p5.$format
./p2-deflation.pl -r 'KK---K--K--K' -a none -d 8 -s 8 -W 1024 -D 768 -F $dest/p2-l-d6-D-s8.$format
./p2-deflation.pl -r 'D-D---D--D--D' -a none -d 6 -s 3.7 -F $dest/p2-l-d6-D-s3p7.$format
./p2-deflation.pl -r 'D-D---D--D--D' -a none -d 8 -s 8 -W 1024 -D 768 -F $dest/p2-l-d6-D-s8.$format

./p3-deflation.pl -W 1024 -D 768 -d 8 -s 18 -r 'BB--BB----B' -f -F $dest/p3-xB-d8-s18a.$format
./p3-deflation.pl -W 1024 -D 768 -d 8 -s 18 -r 'SS--S-S--S-S----S-S--S-S' -f -F $dest/p3-xS-d8-s18a.$format
./p3-deflation.pl -W 1024 -D 768 -d 10 -s 50 -r 'S' -f -m -F $dest/p3-S-d10-s50.$format
./p3-deflation.pl -W 1024 -D 768 -d 10 -s 50 -r 'B' -f -m -F $dest/p3-B-d10-s50.$format

./p3-deflation.pl -W 1024 -D 768 -a none -d 8 -s 18 -r 'BB--BB----B' -F $dest/p3-xB-l-d8-s18a.$format
./p3-deflation.pl -W 1024 -D 768 -a none -d 8 -s 18 -r 'SS--S-S--S-S----S-S--S-S' -F $dest/p3-xS-l-d8-s18a.$format
./p3-deflation.pl -W 1024 -D 768 -a none -d 10 -s 50 -r 'S' -m -F $dest/p3-S-l-d10-s50.$format
./p3-deflation.pl -W 1024 -D 768 -a none -d 10 -s 50 -r 'B' -m -F $dest/p3-B-l-d10-s50.$format
