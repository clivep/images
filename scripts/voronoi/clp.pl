#!/usr/bin/perl

use warnings;
use strict;

my $b = [ 1, 2, 3 ];

my $a = \@{ $b };

printf "was @{$a}\n";
doit($a);
printf "now @{$a}\n";

exit;

sub doit {

    $a = shift;
    push @{$a}, 'xx', 0;
    #$a = [ 'xx' ];
}
