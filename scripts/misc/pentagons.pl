#!/usr/bin/perl

use warnings;
use strict;

############################################################
#
#   pentagons.pl
#
#   Planar tiling with pentagons and bowties
#
############################################################

use Math::Trig;
use File::Path qw(make_path);
use File::Temp qw/ tempfile tempdir /;
use Getopt::Std;

use Data::Dumper;

use constant    POLYGON_TYPE_PENTAGON   =>  'pentagon';
use constant    POLYGON_TYPE_DIAMOND    =>  'diamond';

use constant    POLYGON_STATUS_READY    =>  'ready';
use constant    POLYGON_STATUS_DONE     =>  'done';

use constant    SIDE_STATUS_DONE        =>  'done';
use constant    SIDE_STATUS_DIAMOND     =>  'diamond';
use constant    SIDE_STATUS_READY       =>  'ready';

############################################################
# Check the runline options

my %options;
getopts( 'a:cDhi:lF:s:', \%options );

help() if $options{ 'h' };

my $SCALE = ( $options{ 's' } || 1 ) * 30;
my $ITERATIONS = $options{ 'i' } || 0;

my $COLOR_POLYGONS = $options{ 'c' };
my $LABEL_POLYGONS = $options{ 'l' };

my $DUMP_DATA = $options{ 'D' };
my $OUTPUT_FILE = $options{ 'F' } || 'pentagons.pdf';

my $IMAGE_DEPTH = 1000;
my $IMAGE_WIDTH = 1000;

my $NEXT_ID = 1;

############################################################
# Setup the environment

$| = 1;

my $POLYGONS = [];
my $POLYGON_LOOKUP = {};

my $READY_SIDES = [];

my $ang = 2 * pi / 5;

my $initial_pentagon = {

    'ID'        =>  $NEXT_ID++,
    'TYPE'      =>  POLYGON_TYPE_PENTAGON,
    'ITERATION' =>  0,
    'STATUS'    =>  POLYGON_STATUS_READY,
    'SIDES'     =>  [
                        SIDE_STATUS_READY,
                        SIDE_STATUS_READY,
                        SIDE_STATUS_READY,
                        SIDE_STATUS_READY,
                        SIDE_STATUS_READY,
                    ],
    'VERTICES'  =>  [
                        newVertex( cos( pi / 2.0 + 0 * $ang ), sin ( pi / 2.0 + 0 * $ang ) ),
                        newVertex( cos( pi / 2.0 + 1 * $ang ), sin ( pi / 2.0 + 1 * $ang ) ),
                        newVertex( cos( pi / 2.0 + 2 * $ang ), sin ( pi / 2.0 + 2 * $ang ) ),
                        newVertex( cos( pi / 2.0 + 3 * $ang ), sin ( pi / 2.0 + 3 * $ang ) ),
                        newVertex( cos( pi / 2.0 + 4 * $ang ), sin ( pi / 2.0 + 4 * $ang ) ),
                    ],
};

for my $vertex ( @{ $initial_pentagon->{ 'VERTICES' } } ) {

    $vertex->{ 'ANGLE' } = 108;
    $vertex->{ 'POLYGONS' }->{ $initial_pentagon->{ 'ID' } } = 1;
}

push @{ $POLYGONS }, $initial_pentagon;
$POLYGON_LOOKUP->{ $initial_pentagon->{ 'ID' } } = $initial_pentagon;

for ( my $i = 1; $i <= $ITERATIONS; $i++ ) {

    iteratePolygons( $i );
}

drawImage();

dumpPolygons() if $DUMP_DATA;

exit;

############################################################
# Find the distance between 2 vertices

sub distance {

    my $vtx1 = shift;
    my $vtx2 = shift;

    my $dx = $vtx1->{ 'X' } - $vtx2->{ 'X' };
    my $dy = $vtx1->{ 'Y' } - $vtx2->{ 'Y' };

    return sqrt( ( $dx * $dx ) +  ( $dy * $dy ) );
}

############################################################
# Reflect a point in the line between 2 other points

sub reflectPoint {

    my $point  = shift;  # the point to reflect
    my $point1 = shift;  # first line point
    my $point2 = shift;  # second line point

    my $pt1 = newVertex( 
                $point->{ 'X' } - $point1->{ 'X' },
                $point->{ 'Y' } - $point1->{ 'Y' },
    );

    my $pt2 = newVertex( 
                $point2->{ 'X' } - $point1->{ 'X' },
                $point2->{ 'Y' } - $point1->{ 'Y' },
    );

    my $val =
            ( ( $pt1->{ 'X' } * $pt2->{ 'X' } ) + ( $pt1->{ 'Y' } * $pt2->{ 'Y' } ) ) /
            ( ( $pt2->{ 'X' } * $pt2->{ 'X' } ) + ( $pt2->{ 'Y' } * $pt2->{ 'Y' } ) )
    ;

    return newVertex (
            $point1->{ 'X' } + ( 2 * $val * $pt2->{ 'X' } ) - $pt1->{ 'X' },
            $point1->{ 'Y' } + ( 2 * $val * $pt2->{ 'Y' } ) - $pt1->{ 'Y' },
    );
}

############################################################
# Reflect a polygon in one of its sides

sub reflectPentagon {

    my $iteration = shift;
    my $polygon = shift;
    my $side = shift;

    my $pt1 = $polygon->{ 'VERTICES' }->[ $side ];
    my $pt2 = $polygon->{ 'VERTICES' }->[ ( $side + 1 ) % 5 ];

    my $new_pentagon = {

        'ID'        =>  $NEXT_ID++,
        'TYPE'      =>  POLYGON_TYPE_PENTAGON,
        'ITERATION' =>  $iteration,
        'STATUS'    =>  POLYGON_STATUS_READY,
        'SIDES'     =>  [
                            SIDE_STATUS_DIAMOND,
                            SIDE_STATUS_READY,
                            SIDE_STATUS_READY,
                            SIDE_STATUS_DIAMOND,
                            SIDE_STATUS_DONE,
                        ],
        'VERTICES'  =>  [
                            $pt1,
                            reflectPoint( $polygon->{ 'VERTICES' }->[ ( $side + 4 ) % 5 ], $pt1, $pt2 ),
                            reflectPoint( $polygon->{ 'VERTICES' }->[ ( $side + 3 ) % 5 ], $pt1, $pt2 ),
                            reflectPoint( $polygon->{ 'VERTICES' }->[ ( $side + 2 ) % 5 ], $pt1, $pt2 ),
                            $pt2,
                        ],
    };

    my $ang = 3 * 36;
    for my $vertex ( @{ $new_pentagon->{ 'VERTICES' } } ) {

        $vertex->{ 'POLYGONS' }->{ $new_pentagon->{ 'ID' } } = 1;
        $vertex->{ 'ANGLE' } += $ang;
    }

    $POLYGON_LOOKUP->{ $new_pentagon->{ 'ID' } } = $new_pentagon;

    return $new_pentagon;
}

############################################################
# Share 2 vertices

sub shareVertices {

    my $polygon1 = shift;
    my $vertex_no1 = shift;
    my $polygon2 = shift;
    my $vertex_no2 = shift;

    my $vertex1 = $polygon1->{ 'VERTICES' }->[ $vertex_no1 ];
    my $vertex2 = $polygon2->{ 'VERTICES' }->[ $vertex_no2 ];

    $vertex1->{ 'ANGLE' } += $vertex2->{ 'ANGLE' };
    for my $key ( keys %{ $vertex2->{ 'POLYGONS' } } ) {

        $vertex2->{ 'POLYGONS' }->{ $key } = 1;
    }

    $polygon2->{ 'VERTICES' }->[ $vertex_no2 ] = $vertex1;
}

############################################################
# Find common polygon at a vertex - with exclusions

sub findCommonVertexPolygon {

    my $polygon = shift;
    my $vertex_no = shift;

    my $exclusions = { $polygon->{ 'ID' } => 1 };

    while ( my $exclusion = shift ) {
        $exclusions->{ $exclusion } = 1;
    }

    for ( keys %{ $polygon->{ 'VERTICES' }->[ $vertex_no ]->{ 'POLYGONS' } } ) {

        return $_ unless ( $exclusions->{ $_ } );
    }

    return -1;
}

############################################################
# Find the vertex number in the given polygon

sub findVertexNumber {

    my $polygon = shift;
    my $vertex = shift;

    for ( my $v = 0; $v < scalar @{ $polygon->{ 'VERTICES' } }; $v++ ) {

        return $v if ( $vertex == $polygon->{ 'VERTICES' }->[ $v ] );
    }

    return -1;
}

############################################################
# Check for a diamond at the given vertex

sub checkForDiamond {
    
    my $iteration = shift;
    my $polygon = shift;
    my $vertex_no = shift;

    my $vertex = $polygon->{ 'VERTICES' }->[ $vertex_no ];

    return if ( $vertex->{ 'ANGLE' } != 216 );

    my $polygon2 = $POLYGON_LOOKUP->{ findCommonVertexPolygon( $polygon, 4 ) };
    my $vertex2_no = findVertexNumber( $polygon2, $polygon->{ 'VERTICES' }->[ 4 ] );

    my $polygon3 = $POLYGON_LOOKUP->{ findCommonVertexPolygon( $polygon, 3 ) };

    my $new_diamond = {

        'ID'        =>  $NEXT_ID++,
        'TYPE'      =>  POLYGON_TYPE_DIAMOND,
        'ITERATION' =>  $iteration,
        'STATUS'    =>  POLYGON_STATUS_DONE,
        'SIDES'     =>  [
                            SIDE_STATUS_DONE,
                            SIDE_STATUS_DONE,
                            SIDE_STATUS_DONE,
                            SIDE_STATUS_DONE,
                        ],
        'VERTICES'  =>  [
                            $polygon->{ 'VERTICES' }->[ $vertex_no ],
                            $polygon->{ 'VERTICES' }->[ 4 ],
                            $polygon2->{ 'VERTICES' }->[ ( $vertex2_no + 1 ) % 5 ],
                            $polygon3->{ 'VERTICES' }->[ 0 ],
                        ],
    };

    $new_diamond->{ 'VERTICES' }->[ 0 ]->{ 'ANGLE' } += 36;
    $new_diamond->{ 'VERTICES' }->[ 1 ]->{ 'ANGLE' } += 144;
    $new_diamond->{ 'VERTICES' }->[ 2 ]->{ 'ANGLE' } += 36;
    $new_diamond->{ 'VERTICES' }->[ 3 ]->{ 'ANGLE' } += 144;

    $POLYGON_LOOKUP->{ $new_diamond->{ 'ID' } } = $new_diamond;

    push @{ $POLYGONS }, $new_diamond;
}

############################################################
# Process each of the 'ready' sides

sub iteratePolygons {
    
    my $iteration = shift;

    my $new_polygons = [];

    my $prev_pentagon;
    my $first_pentagon;
    my $last_pentagon;

    for my $polygon ( @{ $POLYGONS } ) {

        next unless $polygon->{ 'STATUS' } eq POLYGON_STATUS_READY;

        my $sides = $polygon->{ 'SIDES' };

        for ( my $s = 0; $s < 5; $s++ ) {

            next if $sides->[ $s ] ne SIDE_STATUS_READY;
            next if $polygon->{ 'VERTICES' }->[ $s ]->{ 'ANGLE' } > 252;
            next if $polygon->{ 'VERTICES' }->[ ($s + 1 ) % 5 ]->{ 'ANGLE' } > 252;

            my $new_pentagon = reflectPentagon( $iteration, $polygon, $s );

            $new_pentagon->{ 'ITERATION' } = $iteration;

            $first_pentagon = $new_pentagon unless $first_pentagon;
            $last_pentagon = $new_pentagon;

            if ( $prev_pentagon ) {

                if ( distance( $new_pentagon->{ 'VERTICES' }->[ 1 ], $prev_pentagon->{ 'VERTICES' }->[ 3 ] ) < 0.0001 ) {
                
                    shareVertices( $new_pentagon, 1, $prev_pentagon, 3 );
                    checkForDiamond( $iteration, $prev_pentagon, 3 );
                }

                if ( distance( $new_pentagon->{ 'VERTICES' }->[ 0 ], $prev_pentagon->{ 'VERTICES' }->[ 3 ] ) < 0.0001 ) {

                    shareVertices( $new_pentagon, 0, $prev_pentagon, 3 );
                }
            }

            push @{ $new_polygons }, $new_pentagon;

            $prev_pentagon = $new_pentagon;

            $sides->[ $s ] = SIDE_STATUS_DONE;
        }
        
        $polygon->{ 'STATUS' } = POLYGON_STATUS_DONE;
    }

    if ( distance( $first_pentagon->{ 'VERTICES' }->[ 1 ], $last_pentagon->{ 'VERTICES' }->[ 3 ] ) < 0.0001) {

        shareVertices( $first_pentagon, 1, $last_pentagon, 3 );
        checkForDiamond( $iteration, $last_pentagon, 3 );
    }
    elsif ( distance( $first_pentagon->{ 'VERTICES' }->[ 1 ], $last_pentagon->{ 'VERTICES' }->[ 4 ] ) < 0.0001) {

        shareVertices( $first_pentagon, 1, $last_pentagon, 4 );
        checkForDiamond( $iteration, $last_pentagon, 4 );

    }

#    if ( distance( $first_pentagon->{ 'VERTICES' }->[ 0 ], $last_pentagon->{ 'VERTICES' }->[ 3 ] ) < 0.0001 ) {
#
#        shareVertices( $first_pentagon, 0, $last_pentagon, 3 );
#    }

    push @{ $POLYGONS }, @{ $new_polygons };
}

############################################################
# Return a new vertex object

sub newVertex {

   return {
        'X'         =>  shift,
        'Y'         =>  shift,
        'POLYGONS'  =>  {},
        'ANGLE'     =>  0,
   };
}

############################################################
# Return a description of the polygon side

sub getSideDescription {

    my $polygon = shift;
    my $side_no = shift;

    my $sides = $polygon->{ 'SIDES' };

    return $sides->[ $side_no ] eq SIDE_STATUS_DONE     ? 'D'
         : $sides->[ $side_no ] eq SIDE_STATUS_DIAMOND  ? 'A'
         : $sides->[ $side_no ] eq SIDE_STATUS_READY    ? 'R'
         : ''
    ;
}

############################################################
# Return a description of the polygon

sub getPolygonDescription {

    my $polygon = shift;

    return $polygon->{ 'TYPE' } eq POLYGON_TYPE_DIAMOND     ? 'diamond'
         : $polygon->{ 'TYPE' } eq POLYGON_TYPE_PENTAGON    ? 'pentagon'
         : ''
    ;
}

############################################################
# Dump the polygons

sub dumpPolygons {

    my $polygon_hash = {};

    for my $polygon ( @{ $POLYGONS } ) {

        $polygon_hash->{ $polygon->{ 'ID' } } = $polygon;
    }

    for ( sort { $a <=> $b } keys %{ $polygon_hash } ) {

        my $polygon = $polygon_hash->{ $_ };

        my $vertices = $polygon->{ 'VERTICES' };

        printf "%s - %s (iter %d)\n",
                            $polygon->{ 'ID' },
                            getPolygonDescription( $polygon ),
                            $polygon->{ 'ITERATION' }
        ;

        for ( my $v = 0; $v < scalar ( @{$vertices} ); $v++ ) {

            printf "    %x (a: %d, s: %s p: ",
                                    $vertices->[ $v ], 
                                    $vertices->[ $v ]->{ 'ANGLE' },
                                    getSideDescription( $polygon, $v ),
            ;

            my $sep = '';
            for my $key ( keys %{ $vertices->[ $v ]->{ 'POLYGONS' } } ) {

                printf "$sep$key",
                       $sep=', ';
            }
            printf ")\n";
        }
    }
}

############################################################
# Draw the polygons

sub drawPolygons {

    my $fh = shift;

    my $iteration_count = {};

    for my $polygon ( @{ $POLYGONS } ) {

        my $vertices = $polygon->{ 'VERTICES' };

        my $iteration = $polygon->{ 'ITERATION' };

        $iteration_count->{ $iteration } ||= 0;
        $iteration_count->{ $iteration }++;

        my $cx = 0;
        my $cy = 0;

        for ( my $v = 0; $v < scalar ( @{$vertices} ); $v++ ) {

            printf $fh "%f %f %s\n",
                            $SCALE * $vertices->[ $v ]->{ 'X' },
                            $SCALE * $vertices->[ $v ]->{ 'Y' },
                            $v ? 'lineto' : 'moveto'
            ;

            $cx += $vertices->[ $v ]->{ 'X' };
            $cy += $vertices->[ $v ]->{ 'Y' };
        }

        my $style = 0;

        if ( $COLOR_POLYGONS ) {

            my $color =
                  $polygon->{ 'TYPE' } eq POLYGON_TYPE_DIAMOND                  ? "0 0 1"
                : ( $style == 0 ) && ( $iteration % 2 )                         ? "1 1 0"
                : ( $style == 1 ) && ( $iteration_count->{ $iteration } % 2 )   ? "1 1 0"
                : "1 0 0"
            ;

            printf $fh "
                closepath
                gsave
                $color setrgbcolor fill
                grestore
                0 setgray stroke
            ";
        }
        else {

            printf $fh "closepath stroke\n";
        }

        $cx /= scalar ( @{$vertices} );
        $cy /= scalar ( @{$vertices} );

        if ( $LABEL_POLYGONS  ) {

            printf $fh "    
                            gsave
                            1 setgray
                            %f %f moveto
                            (%s) dup stringwidth pop -2 div 0 rmoveto 
                            show
                            grestore\n",
                    $SCALE * $cx,
                    $SCALE * $cy,
                    $polygon->{ 'ID' }
            ;

            for ( my $v = 0; $v < scalar ( @{$vertices} ); $v++ ) {

                printf $fh "
                            gsave
                            %f %f moveto (%d)dup stringwidth pop -2 div 0 rmoveto
                            show
                            grestore\n",
                                $SCALE * ( 2 * $vertices->[ $v ]->{ 'X' } + $cx ) / 3.0,
                                $SCALE * ( 2 * $vertices->[ $v ]->{ 'Y' } + $cy ) / 3.0,
                                $v
                ;
            }
        }
    }
}

############################################################
# Write the image header

sub writeImageHeader {

    my $fh = shift;

    printf $fh "%%!PS\n%%BoundingBox: 0 0 %f %f\n%%EndComments\n

        <<
            /PageSize [ %f %f ]
            /ImagingBBox null
        >> setpagedevice

        /mv { moveto } bind def
        /ln { lineto } bind def

        /Arial-Bold findfont 12 scalefont setfont

        0 0 mv %f 0 ln %f %f ln 0 %f ln fill

        %f %f translate

        1 setlinejoin
        1 1 0 setrgbcolor
    ",
        $IMAGE_WIDTH, $IMAGE_DEPTH,
        $IMAGE_WIDTH, $IMAGE_WIDTH,
        $IMAGE_WIDTH, $IMAGE_WIDTH,
        $IMAGE_DEPTH, $IMAGE_DEPTH,
        $IMAGE_DEPTH / 2, $IMAGE_DEPTH / 2,
    ;

    return;
}

############################################################
# Draw the image

sub drawImage {

#    my ( $fh, $temp_file ) = tempfile();

    my $temp_file = 'clp.ps';
    open my $fh, '>', $temp_file;

    writeImageHeader( $fh );

    drawPolygons( $fh );

    close $fh;

    my $convert = "/usr/local/bin/convert";
    $convert = "/usr/bin/convert" unless ( -f $convert );
    $convert = "/Applications/ImageMagick/ImageMagick-6.6.5/bin/convert" unless ( -f $convert );

    unless ( -f $convert ) {

        printf "Could not locate convert at $convert\n";
    }
    else {

        unlink $OUTPUT_FILE;
        system( "$convert $temp_file $OUTPUT_FILE" );
    }

#    unlink $temp_file;

    return;
}

############################################################
# Help doco

sub help {

    print "\n@_\n" if @_;

    printf "\n$0 " .
                "[-a <ang>] [-D] [-h] [-i <iters>] [-F <file>] [-s <mul>]

        where
            -c  color the polygons
            -h  this help message
            -i  iteration count
            -l  label the polygons
            -s  drawing scale

            -D  dump polygon data
            -F  output file

    \n";

    exit;
}
