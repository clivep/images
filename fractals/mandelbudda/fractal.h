/*******************************************************************************
 *
 * fractal.h
 *
 *  Definitions for fractal renderings
 *
 ******************************************************************************/
#ifndef _FRACTAL_H

#include "fractal-utils.h"

/*************************************************
 * Prototypes
 ************************************************/
void usage( char *self );
void processParameters( int argc, char **argv, _image *image );
void writeImage( _image *image, char *dest );
void incrementVisitCount( _image *image, int point, int col );
void processLayerTrajectory( _image *image, int *trajectory, int trajectory_len, int point_num, int layer );
int iterateLayerPoint( _image *image, int layer, _complex c, int point_num );
int getImagePixel( _image *image, long double x, long double y );
void getRandomPoint( _image *image, _complex *c );
void getNearPoint( _image *image, _complex *c );

#define _FRACTAL_H
#endif
