/*******************************************************************************
 *
 * tiling.h
 *
 *  Definitions for tilings
 *
 ******************************************************************************/

#include <stdio.h>
#include <stdarg.h>

#include "tiling-utils.h"
#include "tiling-geometry.h"

#define RETRY           2
#define OK              1
#define NOK             0

#define ALLOC(typ)      (typ *)calloc( 1, sizeof( typ ) )
#define NALLOC(n,typ)   (typ *)calloc( (n), sizeof( typ ) )

/*************************************************
 * Interactive levels
 ************************************************/
typedef enum {

    INTERACTIVE_LEVEL_OFF,
    INTERACTIVE_LEVEL_LOW,
    INTERACTIVE_LEVEL_HI

} _interactive_level;

/*************************************************
 * Options to the dumpJsonData routine
 ************************************************/
typedef enum {

    DUMP_JSON_ONLY_INSIDE_POLYGONS,
    DUMP_JSON_ALL_POLYGONS

} _dump_json_opt;

/*************************************************
 * Image details
 ************************************************/
typedef struct {

    char                *plugin;
    _list               polygons;
    float               exclusion_radius;
    char                *command;
    int                 iteration;
    int                 pause_at;
    FILE                *log_fp;
    char                *tiling_dir;
    char                *tiling_format;
    char                *initial_recipe;
    _list               *recipe_list;
    _interactive_level  interactive_level;
} _details;

/*************************************************
 * Prototypes
 ************************************************/
void dumpAllData();
void dumpPolygonData( _polygon *polygon );
void dumpListData();
void dumpJsonData( char *json_path, _dump_json_opt option );
void initialiseDetails();
void cleanup();
void processParameters( int argc, char **argv );
void addToPolygonList( _polygon *polygon );
_polygon *getPolygonByPos( short pos ); 
char polygonStatus( _polygon *polygon );
char vertexStatus( _polygon_vertex *vertex );
void checkForCompletedPolygon( _polygon *polygon );
char polygonSideStatusChar( _polygon *polygon, short s );
_polygon *getLastPolygon();
char *getRecipeString();
void removeLastPolygon();
void processPolygons();
short processPolygon( _polygon *polygon );
double sumInternalAngles( int type, short vertex ); 
short findSideNumberOnPolygon( _polygon *polygon, _polygon_side *side );
void commitCommonVertices( _list *common_vertices );
short findCommonVertices( _polygon *polygon1, short edge1,
                            _polygon *polygon2, short edge2,
                            _list *common_vertices );
void addCommonVertex( _list *common_vertices,
                            _polygon *polygon1, short edge1, char side1_done,
                            _polygon *polygon2, short edge2, char sides_done ); 
void addPolygonToSide( _polygon *polygon, short edge,
                            _polygon *trial_polygon, short trial_edge,
                            _list *common_vertices );
short checkForValidTiling( _polygon *polygon1, short s1,
                            _polygon *polygon2, short s2 );
short shareVertices( _polygon *polygon1, short s1,
                            _polygon *polygon2, short s2,
                            _list *common_vertices );
_polygon *findNextReadyPolygon();
short rewindPolygonList();
void showWorking( _interactive_level interactive_level, char *format, ... );
void saveTilingData();
char *getSideTypeDescription( int pos );
double templatePolygonAngle( _polygon *template );
short setupFromRecipe( char *initial_recipe );
void processTiling();
