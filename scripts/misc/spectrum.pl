#!/usr/bin/perl

use warnings;
use strict;

my @range = (

        [148, 0, 211],    # Violet
        [75, 0, 130],     # Indigo
        [0, 0, 255],      # Blue
        [0, 255, 0],      # Green
        [255, 255, 0],    # Yellow
        [255, 127, 0],    # Orange
        [255, 0, 0],      # Red
);

my $wid = 800;

open my $fh, '>', 'spectrum.ps' or die "Could not open outpuit: $!\n";

printf $fh "%%!PS\n%%%%BoundingBox: 0 0 1000 1000%%%%EndComments

    <<
        /PageSize [ 1000 1000 ]
        /ImagingBBox null
    >> setpagedevice

    0 0 moveto
    1000 0 lineto
    1000 1000 lineto
    0 1000 lineto
    0 0 lineto
    fill

    /sp_wid $wid def
    /sp_dep 200 def

    1000 sp_wid sub 2 div 700 translate
";

my $blkwid = $wid / $#range;

for( my $x = 0; $x < $wid; $x++ ) {

    my $y = $x / $wid;

    my $blk = int($#range * $y);
    my $from = $range[ $blk ];
    my $to = $range[ $blk + 1 ];

    my $v = $y * $wid - $blk * $blkwid;

    my $r = ( $from->[0] + ($to->[0] - $from->[0]) * $v / $blkwid ) / 256;
    my $g = ( $from->[1] + ($to->[1] - $from->[1]) * $v / $blkwid ) / 256;
    my $b = ( $from->[2] + ($to->[2] - $from->[2]) * $v / $blkwid ) / 256;

print "$y -> $r $g $b\n";
    printf $fh "
        $r $g $b setrgbcolor
        $x 0 moveto
        $x sp_dep lineto
        stroke
    ";
}

close $fh;
