#!/usr/bin/perl

use warnings;
use strict;

############################################################
#
#   bowtie.pl
#
#   Planar tiling with bowties
#
############################################################

use Math::Trig;
use File::Path qw(make_path);
use File::Temp qw/ tempfile tempdir /;
use Getopt::Std;

use Data::Dumper;

############################################################
# Check the runline options

my %options;
getopts( 'a:DhF:s:x:y:', \%options );

help() if $options{ 'h' };

my $ANGLE = $options{ 'a' } || 25;
my $SCALE = $options{ 's' } || 30;
my $X_ITERS = $options{ 'x' } || 5;
my $Y_ITERS = $options{ 'y' } || 5;

my $DUMP_DATA = $options{ 'D' };
my $OUTPUT_FILE = $options{ 'F' } || 'bowtie.pdf';

my $IMAGE_DEPTH = 1000;
my $IMAGE_WIDTH = 1000;

############################################################
# Setup the environment

$| = 1;

my $POLYGONS = [];
my $POLYGON_LOOKUP = {};

############################################################
# Process the image

my $rad = deg2rad($ANGLE);

my $cs = cos( $rad );
my $sn = sin( $rad );

for ( my $y = 0; $y < $Y_ITERS; $y++ ) {

    my $y_cen = ( $y - ( $Y_ITERS / 2.0 ) ) * 2.0 * $cs + 2.0 * $cs;

    for ( my $x = 0; $x < $X_ITERS; $x++ ) {

        my $x_cen = ( $x - ( $X_ITERS / 2.0 ) ) * 2.0 * $cs;

        my $vertices = ( ($x + $y) % 2 ) ?
            [       # a horizontal polygon
                newVertex( $x_cen, $y_cen ),
                newVertex( $x_cen + $cs, $y_cen + $sn ),

                newVertex( $x_cen + 2 * $cs, $y_cen ),
                newVertex( $x_cen + 2 * $cs - $sn, $y_cen - $cs ),

                newVertex( $x_cen + 2 * $cs, $y_cen - 2 * $cs ),
                newVertex( $x_cen + $cs, $y_cen - 2 * $cs - $sn ),

                newVertex( $x_cen, $y_cen - 2 * $cs ),
                newVertex( $x_cen + $sn, $y_cen - $cs ),
            ] : [   # a vertical polygon
                newVertex( $x_cen, $y_cen ),
                newVertex( $x_cen + $cs, $y_cen - $sn ),

                newVertex( $x_cen + 2 * $cs, $y_cen ),
                newVertex( $x_cen + 2 * $cs + $sn, $y_cen - $cs ),

                newVertex( $x_cen + 2 * $cs, $y_cen - 2 * $cs ),
                newVertex( $x_cen + $cs, $y_cen - 2 * $cs + $sn ),

                newVertex( $x_cen, $y_cen - 2 * $cs ),
                newVertex( $x_cen - $sn, $y_cen - $cs ),
            ];

        if ( $x > 0 ) {

            my $prev_polygon = getPolygonByPos( $x - 1, $y );
            my $prev_vertices = $prev_polygon->{ 'VERTICES' };

            if ( ($x + $y) % 2 ) {

                $vertices->[ 0 ] = $prev_vertices->[ 2 ];
                $vertices->[ 6 ] = $prev_vertices->[ 4 ];
                $vertices->[ 7 ] = $prev_vertices->[ 3 ];
            }
            else {

                $vertices->[ 0 ] = $prev_vertices->[ 2 ];
                $vertices->[ 6 ] = $prev_vertices->[ 4 ];
                $vertices->[ 7 ] = $prev_vertices->[ 3 ];
            }
        }

        if ( $y > 0 ) {

            my $prev_polygon = getPolygonByPos( $x, $y - 1 );
            my $prev_vertices = $prev_polygon->{ 'VERTICES' };

            if ( ($x + $y) % 2 ) {

                $vertices->[ 4 ] = $prev_vertices->[ 2 ];
                $vertices->[ 5 ] = $prev_vertices->[ 1 ];
                $vertices->[ 6 ] = $prev_vertices->[ 0 ];
            }
            else {

                $vertices->[ 4 ] = $prev_vertices->[ 2 ];
                $vertices->[ 5 ] = $prev_vertices->[ 1 ];
                $vertices->[ 6 ] = $prev_vertices->[ 0 ];
            }
        }

        my $key = polygonId( $x, $y );

        my $new_polygon = {
            'ID'        =>  $key,
            'VERTICES'  =>  $vertices,
        };

        push @{ $POLYGONS }, $new_polygon;
        $POLYGON_LOOKUP->{ $key } = $new_polygon;
    }
}

drawImage();

dumpPolygons() if $DUMP_DATA;

exit;

############################################################
# Return a polygon given its position

sub getPolygonByPos {
    
    return $POLYGON_LOOKUP->{ sprintf "%s-%s", shift, shift };
}

############################################################
# Return a polygon key given its position

sub polygonId {

    return sprintf "%s-%s", shift, shift;
}

############################################################
# Return a new vertex object

sub newVertex {

   return {
        'X'         =>  shift,
        'Y'         =>  shift,
        'POLYGONS'  =>  {},
   };
}

############################################################
# Dump the polygons

sub dumpPolygons {

    for my $polygon ( @{ $POLYGONS } ) {

        my $vertices = $polygon->{ 'VERTICES' };

        printf "%s\n", $polygon->{ 'ID' };

        for ( my $v = 0; $v < scalar ( @{$vertices} ); $v++ ) {

            printf "    %x\n", $vertices->[ $v ];
        }
    }
}

############################################################
# Draw the polygons

sub drawPolygons {

    my $fh = shift;

    for my $polygon ( @{ $POLYGONS } ) {

        my $vertices = $polygon->{ 'VERTICES' };

        for ( my $v = 0; $v < scalar ( @{$vertices} ); $v++ ) {

            printf $fh "%f %f %s\n",
                            $SCALE * $vertices->[ $v ]->{ 'X' },
                            $SCALE * $vertices->[ $v ]->{ 'Y' },
                            $v ? 'lineto' : 'moveto'
            ;
        }

        printf $fh "closepath stroke\n",
    }
}

############################################################
# Write the image header

sub writeImageHeader {

    my $fh = shift;

    printf $fh "%%!PS\n%%BoundingBox: 0 0 %f %f\n%%EndComments\n

        <<
            /PageSize [ %f %f ]
            /ImagingBBox null
        >> setpagedevice

        /mv { moveto } bind def
        /ln { lineto } bind def

        0 0 mv %f 0 ln %f %f ln 0 %f ln fill

        %f %f translate

        1 setlinejoin
        1 1 0 setrgbcolor
    ",
        $IMAGE_WIDTH, $IMAGE_DEPTH,
        $IMAGE_WIDTH, $IMAGE_WIDTH,
        $IMAGE_WIDTH, $IMAGE_WIDTH,
        $IMAGE_DEPTH, $IMAGE_DEPTH,
        $IMAGE_DEPTH / 2, $IMAGE_DEPTH / 2,
    ;

    return;
}

############################################################
# Draw the image

sub drawImage {

# my ( $fh, $temp_file ) = tempfile();

    my $temp_file = 'clp.ps';
    open my $fh, '>', $temp_file;

    writeImageHeader( $fh );

    drawPolygons( $fh );

    close $fh;

    my $convert = "/usr/local/bin/convert";
    $convert = "/usr/bin/convert" unless ( -f $convert );
    $convert = "/Applications/ImageMagick/ImageMagick-6.6.5/bin/convert" unless ( -f $convert );

    unless ( -f $convert ) {

        printf "Could not locate convert at $convert\n";
    }
    else {

        unlink $OUTPUT_FILE;
        system( "$convert $temp_file $OUTPUT_FILE" );
    }

#unlink $temp_file;

    return;
}

############################################################
# Help doco

sub help {

    print "\n@_\n" if @_;

    printf "\n$0 " .
                "[-a <ang>] [-D] [-h] [-F <file>] [-s <mul>] [-x <iters>] [-y <iters>]

        where
            -a  angle of the bowties (default 25)
            -h  this help message
            -s  drawing scale (default 30)
            -x  x iterations (default 5)
            -y  y iterations (default 5)

            -D  dump polygon data
            -F  output file

    \n";

        exit;
}
