#!/usr/bin/perl

use warnings;
use strict;

use File::Path qw(make_path);
use Getopt::Std;

die "Deprecated\n";

############################################################
#
#   a4-tilting.pl
#
#   Model the Ammann A4 tiling
#
############################################################

use constant    TRUE                    =>  1;
use constant    FALSE                   =>  0;

use constant    IMAGE_WIDTH             =>  1000;
use constant    IMAGE_DEPTH             =>  1000;

use constant    POLYGON_TYPE_BOWTIE     =>  'bowtie';
use constant    POLYGON_TYPE_HKEY       =>  'hkey';
use constant    POLYGON_TYPE_VKEY       =>  'vkey';

use constant    POLYGON_TYPE_RHOMB      =>  'rhomb';
use constant    POLYGON_TYPE_SQUARE     =>  'square';

use constant    DRAWING_STYLE_LINE      =>  'line';
use constant    DRAWING_STYLE_FILL      =>  'fill';

use constant    PS_TEMP                 =>  'clp.ps';

use constant    FCTR                    =>  ( sqrt( 2.0 ) - 1 );

############################################################
# Check the runline options

my %options;
getopts( '5d:hi:lr:s:t:x:y:C:D:F:H:L:MS:W:?', \%options );

help() if $options{ 'h' };

my $DEFLATIONS = $options{ 'd' } || 1;
my $LABEL_POLYGONS = $options{ 'l' };
my $INITIAL_POLYGON = $options{ 'i' } || 'B';
my $DRAWING_RADIUS = $options{ 'r' };
my $DRAWING_SCALE = $options{ 's' } || 1;
my $TEXT_MULTIPLIER = $options{ 't' } || 1;

my $LEVEL = $options{ '5' } ? 5 : 4;

my $CENTRE = $options{ 'C' } || '';
my $OUTPUT_FILE = $options{ 'F' } || 'deflation.pdf';
my $IMAGE_DEPTH = $options{ 'D' } || IMAGE_DEPTH;
my $HIGHLIGHT_POLYGON = $options{ 'H' };
my $LINEWIDTH = $options{ 'L' } || 1;
my $METRICS = $options{ 'M' };
my $IMAGE_WIDTH = $options{ 'W' } || IMAGE_WIDTH;
my $DRAWING_STYLE = $options{ 'S' } || DRAWING_STYLE_LINE;

my $DUMP_DATA = $options{ '?' };

my $X_OFFSET = $options{ 'x' } || 0;
my $Y_OFFSET = $options{ 'y' } || 0;

############################################################
# Setup the environment

$| = 1;

my $NEXT_POLYGON_ID = 0;
my $POLYGON_LOOKUP = {};

my $POLYGON_COLORS = {
                    POLYGON_TYPE_BOWTIE,    '1 0 0 setrgbcolor',
                    POLYGON_TYPE_HKEY,      '0 1 0 setrgbcolor',
                    POLYGON_TYPE_VKEY,      '0 1 1 setrgbcolor',
                    POLYGON_TYPE_RHOMB,     '1 1 0 setrgbcolor',
                    POLYGON_TYPE_SQUARE,    '1 0 0 setrgbcolor',
};

############################################################
# Setup the inital bow-tie polygon

my $size = 10; 
my $off = $size * ( 1 + FCTR ) / -2.0;

my $initial_polygon =
          ( $INITIAL_POLYGON eq 'B' ) ? transformBowTieTemplate( $size, $off, $off, 0, 0 )
        : ( $INITIAL_POLYGON eq 'H' ) ? transformHkeyTemplate( $size, $off, $off, 0, 0 )
        : ( $INITIAL_POLYGON eq 'V' ) ? transformVkeyTemplate( $size, $off, $off, 0, 0 )
        :                               usage( "$INITIAL_POLYGON in not a valid initial polygon type" );

my $POLYGONS = [ $initial_polygon ];

for ( my $d = 0; $d < $DEFLATIONS; $d++ ) {

    deflatePolygons();
}

a5Deform() if $LEVEL == 5;

drawImage();

dumpPolygons() if $DUMP_DATA;

exit;

############################################################
# Some utility functions

sub isBowtie { return shift->{ 'TYPE' } eq POLYGON_TYPE_BOWTIE }
sub isHkey   { return shift->{ 'TYPE' } eq POLYGON_TYPE_HKEY }
sub isVkey   { return shift->{ 'TYPE' } eq POLYGON_TYPE_VKEY }
sub isRhomb  { return shift->{ 'TYPE' } eq POLYGON_TYPE_RHOMB }
sub isSquare { return shift->{ 'TYPE' } eq POLYGON_TYPE_SQUARE }

############################################################
# Deform the a4 polygons into a5

sub a5Deform {

    my $new_polygons = [];

    for my $polygon ( @{ $POLYGONS } ) {

        my $vertices = $polygon->{ 'VERTICES' };

        if ( $polygon->{ 'TYPE' } eq POLYGON_TYPE_BOWTIE ) {

            my $new_polygon = {
                    'ID'        =>  $NEXT_POLYGON_ID++,
                    'TYPE'      =>  POLYGON_TYPE_RHOMB,
                    'VERTICES'  =>  [
                                    $vertices->[ 0 ],
                                    $vertices->[ 2 ],
                                    $vertices->[ 4 ],
                                    $vertices->[ 6 ],
                    ],
            };

            $new_polygon->{ 'VERTICES' }->[ 0 ]->{ 'ANGLE' } += 45;
            $new_polygon->{ 'VERTICES' }->[ 1 ]->{ 'ANGLE' } += 135;
            $new_polygon->{ 'VERTICES' }->[ 2 ]->{ 'ANGLE' } += 45;
            $new_polygon->{ 'VERTICES' }->[ 3 ]->{ 'ANGLE' } += 135;

            addVertexPolygons( $new_polygon );
            delVertexPolygons( $new_polygon, $polygon->{ 'ID' } );
            $POLYGON_LOOKUP->{ $new_polygon->{ 'ID' } } = $new_polygon;

            push @{ $new_polygons }, $new_polygon;
        }
        elsif ( $polygon->{ 'TYPE' } eq POLYGON_TYPE_HKEY ) {

            my $xflip = ( $vertices->[ 4 ]->{ 'X' } < $vertices->[ 0 ]->{ 'X' } ) ? -1 : 1;
            my $yflip = ( $vertices->[ 4 ]->{ 'Y' } < $vertices->[ 0 ]->{ 'Y' } ) ? -1 : 1;

            my $side = $polygon->{ 'SIDE' } * FCTR;

            my $common_hkey = findCommonEdgePolygon( $polygon, 0 );
            if ( $common_hkey && $common_hkey->{ 'EXTRA_VTX' } ) {

                $polygon->{ 'EXTRA_VTX' } = $common_hkey->{ 'EXTRA_VTX' };
            }
            else {

                $polygon->{ 'EXTRA_VTX' } = newVertex(
                                    $vertices->[ 0 ]->{ 'X' } + $xflip * $side,
                                    $vertices->[ 0 ]->{ 'Y' },
                );
            }

            my $common_bowtie = $POLYGON_LOOKUP->{ findCommonVertexPolygon( $polygon, 3 ) };

            my $new_polygon = {
                    'ID'        =>  $NEXT_POLYGON_ID++,
                    'TYPE'      =>  POLYGON_TYPE_SQUARE,
                    'VERTICES'  =>  [
                                    $polygon->{ 'EXTRA_VTX' },
                                    $vertices->[ 2 ],
                                    $common_bowtie->{ 'VERTICES' }->[ 4 ],
                                    $vertices->[ 5 ],
                    ],
            };

            $new_polygon->{ 'VERTICES' }->[ 0 ]->{ 'ANGLE' } += 90;
            $new_polygon->{ 'VERTICES' }->[ 1 ]->{ 'ANGLE' } += 90;
            $new_polygon->{ 'VERTICES' }->[ 2 ]->{ 'ANGLE' } += 90;
            $new_polygon->{ 'VERTICES' }->[ 3 ]->{ 'ANGLE' } += 90;

            addVertexPolygons( $new_polygon );
            delVertexPolygons( $new_polygon, $polygon->{ 'ID' } );
            delVertexPolygons( $new_polygon, $common_bowtie->{ 'VERTICES' }->[ 4 ] );
            $POLYGON_LOOKUP->{ $new_polygon->{ 'ID' } } = $new_polygon;

            push @{ $new_polygons }, $new_polygon;
        }
        elsif ( $polygon->{ 'TYPE' } eq POLYGON_TYPE_VKEY ) {

            my $xflip = ( $vertices->[ 4 ]->{ 'X' } < $vertices->[ 0 ]->{ 'X' } ) ? -1 : 1;
            my $yflip = ( $vertices->[ 4 ]->{ 'Y' } < $vertices->[ 0 ]->{ 'Y' } ) ? -1 : 1;

            my $side = $polygon->{ 'SIDE' } * FCTR;

            my $common_vkey = findCommonEdgePolygon( $polygon, 0 );
            if ( $common_vkey && $common_vkey->{ 'EXTRA_VTX' } ) {

                $polygon->{ 'EXTRA_VTX' } = $common_vkey->{ 'EXTRA_VTX' };
            }
            else {

                $polygon->{ 'EXTRA_VTX' } = newVertex(
                                    $vertices->[ 0 ]->{ 'X' },
                                    $vertices->[ 0 ]->{ 'Y' } + $yflip * $side,
                );
            }

            my $common_bowtie = $POLYGON_LOOKUP->{ findCommonVertexPolygon( $polygon, 3 ) };

            my $new_polygon = {
                    'ID'        =>  $NEXT_POLYGON_ID++,
                    'TYPE'      =>  POLYGON_TYPE_SQUARE,
                    'VERTICES'  =>  [
                                    $polygon->{ 'EXTRA_VTX' },
                                    $vertices->[ 2 ],
                                    $common_bowtie->{ 'VERTICES' }->[ 4 ],
                                    $vertices->[ 5 ],
                    ],
            };

            $new_polygon->{ 'VERTICES' }->[ 0 ]->{ 'ANGLE' } += 90;
            $new_polygon->{ 'VERTICES' }->[ 1 ]->{ 'ANGLE' } += 90;
            $new_polygon->{ 'VERTICES' }->[ 2 ]->{ 'ANGLE' } += 90;
            $new_polygon->{ 'VERTICES' }->[ 3 ]->{ 'ANGLE' } += 90;

            addVertexPolygons( $new_polygon );
            delVertexPolygons( $new_polygon, $polygon->{ 'ID' } );
            delVertexPolygons( $new_polygon, $common_bowtie->{ 'VERTICES' }->[ 4 ] );
            $POLYGON_LOOKUP->{ $new_polygon->{ 'ID' } } = $new_polygon;

            push @{ $new_polygons }, $new_polygon;
        }
    }

    $POLYGONS = $new_polygons;

    consolidateVertices();

    # Make another pass across the polygons looking for rhomb holes...
    
    $new_polygons = [];

    for my $polygon ( @{ $POLYGONS } ) {

        next unless $polygon->{ 'TYPE' } eq POLYGON_TYPE_RHOMB;

        my $vertices = $polygon->{ 'VERTICES' };

        for ( my $v = 1; $v < 4; $v += 2 ) {    # only check vertices 1 & 3

            next unless $vertices->[ $v ]->{ 'ANGLE' } == 225;

            my $square1_id = findCommonVertexPolygon( $polygon, $v );
            my $square1 = $POLYGON_LOOKUP->{ $square1_id };
            next unless $square1->{ 'TYPE' } eq POLYGON_TYPE_SQUARE;

            my $v_no = findVertexNumber( $square1, $vertices->[ $v ] ) - 1;

            my $v1 = $square1->{ 'VERTICES' }->[ $v_no ]->{ 'ANGLE' } == 360 ?
                                                    ( $v_no + 2 ) % 4 : $v_no;                   

            my $closest_id = -1;
            my $closest_dist;

            for my $trial_id ( keys %{ $square1->{ 'VERTICES' }->[ $v1 ]->{ 'POLYGONS' } } ) {

                next if $trial_id eq $polygon->{ 'ID' };
                next if $trial_id eq $square1->{ 'ID' };
                
                my $trial_polygon = $POLYGON_LOOKUP->{ $trial_id };
                next unless isSquare( $trial_polygon );

                my $centre = findPolygonCentre( $trial_polygon );
                my $dist = distance( $centre, $vertices->[ $v ] );

                if ( ( $closest_id < 0 ) || ( $dist < $closest_dist ) ) {

                    $closest_id = $trial_id;
                    $closest_dist = $dist;
                }
            }

            next if $closest_id < 0;

            my $square2 = $POLYGON_LOOKUP->{ $closest_id };

            $v_no = findVertexNumber( $square2, $square1->{ 'VERTICES' }->[ $v1 ] );
            my $v2 = $square2->{ 'VERTICES' }->[ $v_no ]->{ 'ANGLE' } == 225 ?
                                                    $v_no - 1 : ( $v_no + 1 ) % 4;

            my $new_polygon = {
                    'ID'        =>  $NEXT_POLYGON_ID++,
                    'TYPE'      =>  POLYGON_TYPE_RHOMB,
                    'VERTICES'  =>  [
                                    $vertices->[ 0 ],
                                    $vertices->[ $v ],
                                    $square1->{ 'VERTICES' }->[ $v1 ],
                                    $square2->{ 'VERTICES' }->[ $v2 ],
                    ],
            };

            $new_polygon->{ 'VERTICES' }->[ 0 ]->{ 'ANGLE' } += 45;
            $new_polygon->{ 'VERTICES' }->[ 1 ]->{ 'ANGLE' } += 135;
            $new_polygon->{ 'VERTICES' }->[ 2 ]->{ 'ANGLE' } += 45;
            $new_polygon->{ 'VERTICES' }->[ 3 ]->{ 'ANGLE' } += 135;

            addVertexPolygons( $new_polygon );
            delVertexPolygons( $new_polygon, $polygon->{ 'ID' } );
            $POLYGON_LOOKUP->{ $new_polygon->{ 'ID' } } = $new_polygon;

            push @{ $new_polygons }, $new_polygon;
        }
    }

    push @{ $POLYGONS }, @{ $new_polygons };

    # Make a last pass across the polygons looking for rhombs between squares
    
    $new_polygons = [];

    for my $polygon ( @{ $POLYGONS } ) {

        next unless $polygon->{ 'TYPE' } eq POLYGON_TYPE_SQUARE;

        my $vertices = $polygon->{ 'VERTICES' };

        next if $vertices->[ 0 ]->{ 'ANGLE' } == 360;
        next if $vertices->[ 3 ]->{ 'ANGLE' } == 360;

        my $square2_id = findCommonVertexPolygon( $polygon, 0 );
        next if ( $square2_id < 0 );
        my $square2 = $POLYGON_LOOKUP->{ $square2_id };

        unless ( isSquare( $square2 ) ) {
            $square2_id = findCommonVertexPolygon( $polygon, 0, $square2_id );
            $square2 = $POLYGON_LOOKUP->{ $square2_id };
        }

        my $square3;
        for my $square3_id ( keys %{ $square2->{ 'VERTICES' }->[ 3 ]->{ 'POLYGONS' } } ) {

            my $trial_polygon = $POLYGON_LOOKUP->{ $square3_id };

            next if $square3_id == $square2_id;
            next unless isSquare( $trial_polygon );

            next if $trial_polygon->{ 'VERTICES' }->[ 0 ]->{ 'ANGLE' } == 360;

            $square3 = $trial_polygon;
        }

        next unless $square3;

        my $square4_id = findCommonVertexPolygon( $square3, 0 );
        next if ( $square4_id < 0 );
        my $square4 = $POLYGON_LOOKUP->{ $square4_id };

        unless ( isSquare( $square4 ) ) {
            $square4_id = findCommonVertexPolygon( $square3, 0, $square4_id );
            $square4 = $POLYGON_LOOKUP->{ $square4_id };
        }

        my $new_polygon = {
                'ID'        =>  $NEXT_POLYGON_ID++,
                'TYPE'      =>  POLYGON_TYPE_RHOMB,
                'VERTICES'  =>  [
                                $vertices->[ 0 ],
                                $square2->{ 'VERTICES' }->[ 3 ],
                                $square3->{ 'VERTICES' }->[ 0 ],
                                $vertices->[ 3 ],
                ],
        };

        $new_polygon->{ 'VERTICES' }->[ 0 ]->{ 'ANGLE' } += 90;
        $new_polygon->{ 'VERTICES' }->[ 1 ]->{ 'ANGLE' } += 90;
        $new_polygon->{ 'VERTICES' }->[ 2 ]->{ 'ANGLE' } += 90;
        $new_polygon->{ 'VERTICES' }->[ 3 ]->{ 'ANGLE' } += 90;

        addVertexPolygons( $new_polygon );
        delVertexPolygons( $new_polygon, $polygon->{ 'ID' } );
        $POLYGON_LOOKUP->{ $new_polygon->{ 'ID' } } = $new_polygon;

        push @{ $new_polygons }, $new_polygon;
    }

    push @{ $POLYGONS }, @{ $new_polygons };
}

############################################################
# Consolidate vertices

sub consolidateVertices {

    my $max_dist = 0.00001;

    my $count = scalar @{ $POLYGONS };
    my $approx_checks = $count * ( $count + 1 ) / 2.0;

    printf "Consolidating vertices\n";

    my $c = 0;
    for ( my $p1 = 0; $p1 < $count; $p1++ ) {
     
        printf "Progress: %0.2f   \r", 100 * $c / $approx_checks if $METRICS;

        my $polygon1 = $POLYGONS->[ $p1 ];
    
        my $vertices1 = $polygon1->{ 'VERTICES' };

        for ( my $p2 = $p1 + 1; $p2 < $count; $p2++ ) {

            $c++;

            my $polygon2 = $POLYGONS->[ $p2 ];

            next if ( $polygon1 == $polygon2 );

            my $vertices2 = $polygon2->{ 'VERTICES' };

            for ( my $v1 = 0; $v1 < scalar @{ $vertices1 }; $v1++ ) {
            
                for ( my $v2 = 0; $v2 < scalar @{ $vertices2 }; $v2++ ) {
            
                    next if ( $vertices1->[ $v1 ] == $vertices2->[ $v2 ] );

                    my $d = distance( $vertices1->[ $v1 ], $vertices2->[ $v2 ] );
                    if ( $d < $max_dist ) {

                        $vertices1->[ $v1 ]->{ 'ANGLE' } += $vertices2->[ $v2 ]->{ 'ANGLE' } unless $vertices1->[ $v1 ]->{ 'SHARED' };
                        $vertices2->[ $v2 ] = $vertices1->[ $v1 ];
                        $vertices2->[ $v2 ]->{ 'POLYGONS' }->{ $polygon2->{ 'ID' } } = 1;
                        $vertices2->[ $v2 ]->{ 'SHARED' } = 1;
                        last;
                    }
                }
            }
        }
    }

    printf "Done            \n";

    return;
}

############################################################
# Find a polygon of the same type that shares the given edge

sub findCommonEdgePolygon {

    my $polygon = shift;
    my $edge_no = shift;

    my $vtx1_polygons = $polygon->{ 'VERTICES' }->[ $edge_no ]->{ 'POLYGONS' };

    my $vtx2_no = ( $edge_no + 1 ) % scalar @{ $polygon->{ 'VERTICES' } };
    my $vtx2_polygons = $polygon->{ 'VERTICES' }->[ $vtx2_no ]->{ 'POLYGONS' };

    foreach my $vtx1_polygon ( keys %{ $vtx1_polygons } ) {

        next if ( $vtx1_polygon = $polygon->{ 'ID' } );
    
        foreach my $vtx2_polygon ( keys %{ $vtx2_polygons } ) {

            return $POLYGON_LOOKUP->{ $vtx2_polygon }
                        if ( $vtx1_polygon eq $vtx2_polygon );
        }
    }

    return;
}

############################################################
# Find common polygon at a vertex - with exclusions

sub findCommonVertexPolygon {

    my $polygon = shift;
    my $vertex_no = shift;

    my $exclusions = { $polygon->{ 'ID' } => 1 };

    while ( my $exclusion = shift ) {
        $exclusions->{ $exclusion } = 1;
    }

    for ( keys %{ $polygon->{ 'VERTICES' }->[ $vertex_no ]->{ 'POLYGONS' } } ) {

        return $_ unless ( $exclusions->{ $_ } );
    }

    return -1;
}

############################################################
# Return a horizontal key template, scaled and offset

sub transformHkeyTemplate {

    my $scale = shift || 1.0;
    my $xoff = shift || 0.0;
    my $yoff = shift || 0.0;
    my $flipx = shift;
    my $flipy = shift;
    my $marker = shift;

    my $l = 1;
    my $s = FCTR;

    my $polygon = {
            'ID'        =>  $NEXT_POLYGON_ID++,
            'TYPE'      =>  POLYGON_TYPE_HKEY,
            'SIDE'      =>  $scale,
            'VERTICES'  =>  [
                            newVertex( 00, 00 ),
                            newVertex( $l + $s, 00 ),
                            newVertex( $l + $s, $s ),
                            newVertex( $l, $s ),
                            newVertex( $l, $l ),
                            newVertex( 00, $l ),
            ],
    };

    $polygon->{ $marker } = 1 if $marker;

    for ( @{ $polygon->{ 'VERTICES' } } ) {

        $_->{ 'X' } = ( $_->{ 'X' } * $scale * ( $flipx ? -1 : 1 ) ) + $xoff;
        $_->{ 'Y' } = ( $_->{ 'Y' } * $scale * ( $flipy ? -1 : 1 ) ) + $yoff;
    }

    addVertexPolygons( $polygon );
    $POLYGON_LOOKUP->{ $polygon->{ 'ID' } } = $polygon;

    return $polygon;
}

############################################################
# Return a vertical key template, scaled and offset

sub transformVkeyTemplate {

    my $scale = shift || 1.0;
    my $xoff = shift || 0.0;
    my $yoff = shift || 0.0;
    my $flipx = shift;
    my $flipy = shift;
    my $marker = shift;

    my $l = 1;
    my $s = FCTR;

    my $polygon = {
            'ID'        =>  $NEXT_POLYGON_ID++,
            'TYPE'      =>  POLYGON_TYPE_VKEY,
            'SIDE'      =>  $scale,
            'VERTICES'  =>  [
                            newVertex( 00, 00 ),
                            newVertex( 00, $l + $s ),
                            newVertex( $s, $l + $s ),
                            newVertex( $s, $l ),
                            newVertex( $l, $l ),
                            newVertex( $l, 00 ),
            ],
    };

    $polygon->{ $marker } = 1 if $marker;

    for ( @{ $polygon->{ 'VERTICES' } } ) {

        $_->{ 'X' } = ( $_->{ 'X' } * $scale * ( $flipx ? -1 : 1 ) ) + $xoff;
        $_->{ 'Y' } = ( $_->{ 'Y' } * $scale * ( $flipy ? -1 : 1 ) ) + $yoff;
    }

    addVertexPolygons( $polygon );
    $POLYGON_LOOKUP->{ $polygon->{ 'ID' } } = $polygon;

    return $polygon;
}

############################################################
# Return a bowtie template, scaled and offset

sub transformBowTieTemplate {

    my $scale = shift || 1.0;
    my $xoff = shift || 0.0;
    my $yoff = shift || 0.0;
    my $flipx = shift;
    my $flipy = shift;
    my $marker = shift;

    my $l = 1;
    my $s = FCTR;

    my $polygon = {
            'ID'        =>  $NEXT_POLYGON_ID++,
            'TYPE'      =>  POLYGON_TYPE_BOWTIE,
            'SIDE'      =>  $scale,
            'VERTICES'  =>  [
                            newVertex( 00, 00 ),
                            newVertex( 00, $l ),
                            newVertex( $s, $l ),
                            newVertex( $s, $l + $s ),
                            newVertex( $l + $s, $l + $s ),
                            newVertex( $l + $s, $s ),
                            newVertex( $l, $s ),
                            newVertex( $l, 00 ),
            ],
    };

    $polygon->{ $marker } = 1 if $marker;

    for ( @{ $polygon->{ 'VERTICES' } } ) {

        $_->{ 'X' } = ( $_->{ 'X' } * $scale * ( $flipx ? -1 : 1 ) ) + $xoff;
        $_->{ 'Y' } = ( $_->{ 'Y' } * $scale * ( $flipy ? -1 : 1 ) ) + $yoff;
    }

    addVertexPolygons( $polygon );
    $POLYGON_LOOKUP->{ $polygon->{ 'ID' } } = $polygon;

    return $polygon;
}

############################################################
# Draw a line (or moveto) a vertex

sub drawToVertex {

    my $fh = shift;
    my $operation = shift;
    my $vertex = shift;

    printf $fh "%f %f %s\n",
                $DRAWING_SCALE * $vertex->{ 'X' },
                $DRAWING_SCALE * $vertex->{ 'Y' },
                $operation
    ; 
     
    return;
}

############################################################
# Fill the initial polygon

sub fillInitialPolygon {

    my $fh = shift;

    my $vertices = $initial_polygon->{ 'VERTICES' };

    for ( my $v = 0; $v < scalar @{ $vertices }; $v++ ) {
        
        printf $fh "%f %f %s\n",
                        $DRAWING_SCALE * $vertices->[ $v ]->{ 'X' },
                        $DRAWING_SCALE * $vertices->[ $v ]->{ 'Y' },
                        $v ? 'ln' : 'mv'
        ;
    }

    printf $fh "1 1 0 setrgbcolor fill\n";
}

############################################################
# Draw the given polygon

sub drawPolygon {

    my $fh = shift;
    my $polygon = shift;

    my $vertices = $polygon->{ 'VERTICES' };

    printf $fh "%% Polygon %s\n", $polygon->{ 'ID' };

    if ( $DRAWING_RADIUS ) {
    
        my $centre = findPolygonCentre( $polygon );
        return if distance( $centre, { 'X' => 0, 'Y' => 0 } ) > ( $DRAWING_RADIUS / $DRAWING_SCALE );
    }

    for ( my $v = 0; $v < scalar @{ $vertices }; $v++ ) {

        printf $fh "%f %f %s\n",
                            $DRAWING_SCALE * $vertices->[ $v ]->{ 'X' },
                            $DRAWING_SCALE * $vertices->[ $v ]->{ 'Y' },
                            $v ? 'ln' : 'mv'
        ;
    }

    if ( ( $HIGHLIGHT_POLYGON || '' ) eq $polygon->{ 'ID' } ) {

        printf $fh "0 1 1 setrgbcolor fill\n";
    }
    elsif ( $DRAWING_STYLE eq DRAWING_STYLE_FILL ) {
        
        printf $fh "closepath gsave %s fill grestore 0 setgray stroke\n",
                                            $POLYGON_COLORS->{ $polygon->{ 'TYPE' } };
    }
    else {
    
        printf $fh "closepath 1 1 0 setrgbcolor stroke\n", $LINEWIDTH;
    }
}

############################################################
# Add the polygons' id to each vertex polygon list

sub addVertexPolygons {

    my $polygon = shift;

    for my $vertex ( @{ $polygon->{ 'VERTICES' } } ) {

        $vertex->{ 'POLYGONS' }->{ $polygon->{ 'ID' } } = 1;
    }

    return;
}

############################################################
# Remove the id from each vertex polygon list

sub delVertexPolygons {

    my $polygon = shift;
    my $id = shift;

    for my $vertex ( @{ $polygon->{ 'VERTICES' } } ) {

        delete $vertex->{ 'POLYGONS' }->{ $id };
    }

    return;
}

############################################################
# Combine the 2 given vertices (keep addr of vertex 1)

sub shareVertices {

    my $polygon1 = shift;
    my $vertex_no1 = shift;
    my $polygon2 = shift;
    my $vertex_no2 = shift;

    my $vertex1 = $polygon1->{ 'VERTICES' }->[ $vertex_no1 ];
    my $vertex2 = $polygon2->{ 'VERTICES' }->[ $vertex_no2 ];

    # Add the common polygons from vertex2 into vertex1
    for ( keys %{ $vertex2->{ 'POLYGONS' } } ) {

        $vertex1->{ 'POLYGONS' }->{ $_ } = 1;

        my $polygon = $POLYGON_LOOKUP->{ $_ };
        my $v = findVertexNumber( $polygon, $vertex2 );
        $polygon->{ 'VERTICES' }->[ $v ] = $vertex1;
    }

    $polygon2->{ 'VERTICES' }->[ $vertex_no2 ] = $vertex1;
    $vertex1->{ 'SHARED' } = TRUE;
}

############################################################
# Create a polygon from a template and deflate it

sub transformTemplatePolygon {

    my $type = shift;
    my $side = shift;
    my $origin = shift;
    my $xflip = shift;
    my $yflip = shift;
    my $marker = shift;

    my $template_func =
            ( $type eq POLYGON_TYPE_BOWTIE ) ? \&transformBowTieTemplate
          : ( $type eq POLYGON_TYPE_HKEY )   ? \&transformHkeyTemplate
          : ( $type eq POLYGON_TYPE_VKEY )   ? \&transformVkeyTemplate
          :                                    undef
    ;

    return unless $template_func;

    return $template_func->( $side, $origin->{ 'X' }, $origin->{ 'Y' }, $xflip, $yflip, $marker ),
}

############################################################
# Return the polygons' centre point

sub findPolygonCentre {

    my $polygon = shift;
    my $first = shift || 0;
    my $last = shift || scalar @{ $polygon->{ 'VERTICES' } };

    my $vertices = $polygon->{ 'VERTICES' };
    my $cx = 0;
    my $cy = 0;

    for ( my $v = $first; $v < $last; $v++ ) {
        $cx += $vertices->[ $v ]->{ 'X' };
        $cy += $vertices->[ $v ]->{ 'Y' };
    }

    return newVertex(
        $cx / ( $last - $first ),
        $cy / ( $last - $first ),
    );
}

############################################################
# Find the vertex number in the given polygon

sub findVertexNumber {

    my $polygon = shift;
    my $vertex = shift;

    for ( my $v = 0; $v < scalar @{ $polygon->{ 'VERTICES' } }; $v++ ) {

        return $v if ( $vertex == $polygon->{ 'VERTICES' }->[ $v ] );
    }

    return -1;
}

############################################################
# Deflate the given polygon

sub deflatePolygons {

    my $new_polygons = [];

    for my $polygon ( @{ $POLYGONS } ) {

        my $vertices = $polygon->{ 'VERTICES' };
    
        my $side = $polygon->{ 'SIDE' } * FCTR;
    
        my $xflip = ( $vertices->[ 4 ]->{ 'X' } < $vertices->[ 0 ]->{ 'X' } );
        my $yflip = ( $vertices->[ 4 ]->{ 'Y' } < $vertices->[ 0 ]->{ 'Y' } );
    
        if ( $polygon->{ 'TYPE' } eq POLYGON_TYPE_BOWTIE ) {
    
            my $bowtie_polygons = [
                transformTemplatePolygon( POLYGON_TYPE_BOWTIE, $side, $vertices->[ 0 ], $xflip, $yflip ),
                transformTemplatePolygon( POLYGON_TYPE_VKEY, $side, $vertices->[ 1 ], $xflip, ! $yflip ),
                transformTemplatePolygon( POLYGON_TYPE_HKEY, $side, $vertices->[ 7 ], ! $xflip, $yflip ),
                transformTemplatePolygon( POLYGON_TYPE_BOWTIE, $side, $vertices->[ 4 ], ! $xflip, ! $yflip ),
                transformTemplatePolygon( POLYGON_TYPE_BOWTIE, $side, $vertices->[ 6 ], ! $xflip, $yflip ),
                transformTemplatePolygon( POLYGON_TYPE_HKEY, $side, $vertices->[ 3 ], $xflip, ! $yflip ),
                transformTemplatePolygon( POLYGON_TYPE_VKEY, $side, $vertices->[ 5 ], ! $xflip, $yflip ),
            ];
    
            shareVertices( $bowtie_polygons->[ 0 ], 1, $bowtie_polygons->[ 1 ], 1 );
            shareVertices( $bowtie_polygons->[ 0 ], 2, $bowtie_polygons->[ 1 ], 2 );
            shareVertices( $bowtie_polygons->[ 0 ], 3, $bowtie_polygons->[ 1 ], 3 );
            shareVertices( $bowtie_polygons->[ 0 ], 4, $bowtie_polygons->[ 4 ], 6 );
            shareVertices( $bowtie_polygons->[ 0 ], 5, $bowtie_polygons->[ 2 ], 3 );
            shareVertices( $bowtie_polygons->[ 0 ], 6, $bowtie_polygons->[ 2 ], 2 );
            shareVertices( $bowtie_polygons->[ 0 ], 7, $bowtie_polygons->[ 2 ], 1 );
    
            shareVertices( $bowtie_polygons->[ 1 ], 4, $bowtie_polygons->[ 4 ], 5 );
            shareVertices( $bowtie_polygons->[ 1 ], 5, $bowtie_polygons->[ 4 ], 4 );
    
            shareVertices( $bowtie_polygons->[ 2 ], 4, $bowtie_polygons->[ 4 ], 7 );
            shareVertices( $bowtie_polygons->[ 2 ], 5, $bowtie_polygons->[ 4 ], 0 );
            shareVertices( $bowtie_polygons->[ 2 ], 5, $bowtie_polygons->[ 6 ], 5 );
    
            shareVertices( $bowtie_polygons->[ 3 ], 1, $bowtie_polygons->[ 6 ], 1 );
            shareVertices( $bowtie_polygons->[ 3 ], 2, $bowtie_polygons->[ 6 ], 2 );
            shareVertices( $bowtie_polygons->[ 3 ], 3, $bowtie_polygons->[ 6 ], 3 );
            shareVertices( $bowtie_polygons->[ 3 ], 4, $bowtie_polygons->[ 4 ], 2 );
            shareVertices( $bowtie_polygons->[ 3 ], 5, $bowtie_polygons->[ 5 ], 3 );
            shareVertices( $bowtie_polygons->[ 3 ], 6, $bowtie_polygons->[ 5 ], 2 );
            shareVertices( $bowtie_polygons->[ 3 ], 7, $bowtie_polygons->[ 5 ], 1 );
    
            shareVertices( $bowtie_polygons->[ 4 ], 0, $bowtie_polygons->[ 6 ], 5 );
            shareVertices( $bowtie_polygons->[ 4 ], 1, $bowtie_polygons->[ 6 ], 4 );
            shareVertices( $bowtie_polygons->[ 4 ], 3, $bowtie_polygons->[ 5 ], 4 );
            shareVertices( $bowtie_polygons->[ 4 ], 4, $bowtie_polygons->[ 5 ], 5 );
    
            shareVertices( $polygon, 0, $bowtie_polygons->[ 0 ], 0 );
            shareVertices( $polygon, 1, $bowtie_polygons->[ 1 ], 0 );
            shareVertices( $polygon, 2, $bowtie_polygons->[ 1 ], 5 );
            shareVertices( $polygon, 2, $bowtie_polygons->[ 5 ], 5 );
            shareVertices( $polygon, 2, $bowtie_polygons->[ 4 ], 4 );
            shareVertices( $polygon, 3, $bowtie_polygons->[ 5 ], 0 );
            shareVertices( $polygon, 4, $bowtie_polygons->[ 3 ], 0 );
            shareVertices( $polygon, 4, $bowtie_polygons->[ 3 ], 0 );
            shareVertices( $polygon, 5, $bowtie_polygons->[ 6 ], 0 );
            shareVertices( $polygon, 6, $bowtie_polygons->[ 4 ], 0 );
            shareVertices( $polygon, 6, $bowtie_polygons->[ 6 ], 5 );
            shareVertices( $polygon, 6, $bowtie_polygons->[ 2 ], 5 );
            shareVertices( $polygon, 7, $bowtie_polygons->[ 2 ], 0 );

            for ( @{ $bowtie_polygons } ) {
                delVertexPolygons( $_, $polygon->{ 'ID' } );
            }

            push @{ $new_polygons }, @{ $bowtie_polygons };
        }
        elsif ( $polygon->{ 'TYPE' } eq POLYGON_TYPE_VKEY ) {
    
            my $new_polygon = 
                    transformBowTieTemplate(
                                $side, 
                                $vertices->[ 0 ] ->{ 'X' },
                                $vertices->[ 0 ] ->{ 'Y' } + ( $yflip ? -1 : 1 ) * $side,
                                $xflip, $yflip
            );
    
            my $vkey_polygons = [
                $new_polygon,
                transformTemplatePolygon( POLYGON_TYPE_BOWTIE, $side, $vertices->[ 5 ], ! $xflip, $yflip ),
                transformTemplatePolygon( POLYGON_TYPE_HKEY, $side, $vertices->[ 0 ], $xflip, $yflip ),
                transformTemplatePolygon( POLYGON_TYPE_VKEY, $side, $vertices->[ 1 ], $xflip, ! $yflip ),
                transformTemplatePolygon( POLYGON_TYPE_VKEY, $side, $vertices->[ 4 ], ! $xflip, ! $yflip ),
            ];
    
            shareVertices( $vkey_polygons->[ 0 ], 0, $vkey_polygons->[ 2 ], 5 );
            shareVertices( $vkey_polygons->[ 0 ], 1, $vkey_polygons->[ 3 ], 1 );
            shareVertices( $vkey_polygons->[ 0 ], 2, $vkey_polygons->[ 3 ], 2 );
            shareVertices( $vkey_polygons->[ 0 ], 3, $vkey_polygons->[ 3 ], 3 );
            shareVertices( $vkey_polygons->[ 0 ], 4, $vkey_polygons->[ 4 ], 5 );
            shareVertices( $vkey_polygons->[ 0 ], 5, $vkey_polygons->[ 4 ], 4 );
            shareVertices( $vkey_polygons->[ 0 ], 6, $vkey_polygons->[ 1 ], 4 );
            shareVertices( $vkey_polygons->[ 0 ], 7, $vkey_polygons->[ 2 ], 4 );
    
            shareVertices( $vkey_polygons->[ 1 ], 1, $vkey_polygons->[ 4 ], 1 );
            shareVertices( $vkey_polygons->[ 1 ], 2, $vkey_polygons->[ 4 ], 2 );
            shareVertices( $vkey_polygons->[ 1 ], 3, $vkey_polygons->[ 4 ], 3 );
            shareVertices( $vkey_polygons->[ 1 ], 5, $vkey_polygons->[ 2 ], 3 );
            shareVertices( $vkey_polygons->[ 1 ], 6, $vkey_polygons->[ 2 ], 2 );
            shareVertices( $vkey_polygons->[ 1 ], 7, $vkey_polygons->[ 2 ], 1 );
    
            shareVertices( $polygon, 0, $vkey_polygons->[ 2 ], 0 );
            shareVertices( $polygon, 1, $vkey_polygons->[ 3 ], 0 );
            shareVertices( $polygon, 2, $vkey_polygons->[ 3 ], 5 );
            shareVertices( $polygon, 3, $vkey_polygons->[ 3 ], 4 );
            shareVertices( $polygon, 4, $vkey_polygons->[ 4 ], 0 );
            shareVertices( $polygon, 5, $vkey_polygons->[ 1 ], 0 );

            for ( @{ $vkey_polygons } ) {
                delVertexPolygons( $_, $polygon->{ 'ID' } );
            }

            push @{ $new_polygons }, @{ $vkey_polygons };
        }
        elsif ( $polygon->{ 'TYPE' } eq POLYGON_TYPE_HKEY ) {
    
            my $new_polygon = 
                        transformBowTieTemplate(
                                    $side, 
                                    $vertices->[ 0 ] ->{ 'X' } + ( $xflip ? -1 : 1 ) * $side,
                                    $vertices->[ 0 ] ->{ 'Y' },
                                    $xflip, $yflip,
                                    'CENTRE'
            );
    
            my $hkey_polygons = [
                $new_polygon,
                transformTemplatePolygon( POLYGON_TYPE_BOWTIE, $side, $vertices->[ 5 ], $xflip, ! $yflip ),
                transformTemplatePolygon( POLYGON_TYPE_VKEY, $side, $vertices->[ 0 ], $xflip, $yflip ),
                transformTemplatePolygon( POLYGON_TYPE_HKEY, $side, $vertices->[ 1 ], ! $xflip, $yflip ),
                transformTemplatePolygon( POLYGON_TYPE_HKEY, $side, $vertices->[ 4 ], ! $xflip, ! $yflip ),
            ];
    
            shareVertices( $hkey_polygons->[ 0 ], 0, $hkey_polygons->[ 2 ], 5 );
            shareVertices( $hkey_polygons->[ 0 ], 1, $hkey_polygons->[ 2 ], 4 );
            shareVertices( $hkey_polygons->[ 0 ], 2, $hkey_polygons->[ 1 ], 4 );
            shareVertices( $hkey_polygons->[ 0 ], 3, $hkey_polygons->[ 4 ], 4 );
            shareVertices( $hkey_polygons->[ 0 ], 4, $hkey_polygons->[ 4 ], 5 );
            shareVertices( $hkey_polygons->[ 0 ], 5, $hkey_polygons->[ 3 ], 3 );
            shareVertices( $hkey_polygons->[ 0 ], 6, $hkey_polygons->[ 3 ], 2 );
            shareVertices( $hkey_polygons->[ 0 ], 7, $hkey_polygons->[ 3 ], 1 );
    
            shareVertices( $hkey_polygons->[ 1 ], 1, $hkey_polygons->[ 2 ], 1 );
            shareVertices( $hkey_polygons->[ 1 ], 2, $hkey_polygons->[ 2 ], 2 );
            shareVertices( $hkey_polygons->[ 1 ], 3, $hkey_polygons->[ 2 ], 3 );
            shareVertices( $hkey_polygons->[ 1 ], 5, $hkey_polygons->[ 4 ], 3 );
            shareVertices( $hkey_polygons->[ 1 ], 6, $hkey_polygons->[ 4 ], 2 );
            shareVertices( $hkey_polygons->[ 1 ], 7, $hkey_polygons->[ 4 ], 1 );
    
            shareVertices( $polygon, 0, $hkey_polygons->[ 2 ], 0 );
            shareVertices( $polygon, 1, $hkey_polygons->[ 3 ], 0 );
            shareVertices( $polygon, 2, $hkey_polygons->[ 3 ], 5 );
            shareVertices( $polygon, 3, $hkey_polygons->[ 3 ], 4 );
            shareVertices( $polygon, 4, $hkey_polygons->[ 4 ], 0 );
            shareVertices( $polygon, 5, $hkey_polygons->[ 1 ], 0 );

            for ( @{ $hkey_polygons } ) {
                delVertexPolygons( $_, $polygon->{ 'ID' } );
            }

            push @{ $new_polygons }, @{ $hkey_polygons };
        }
    }

    $POLYGONS = $new_polygons;

    return;
}

############################################################
# Label the given polygon

sub labelPolygon {

    my $fh = shift;
    my $polygon = shift;

    my $vertices = $polygon->{ 'VERTICES' };

    my $font_sz = 10 / ( $DEFLATIONS ** 0.3 );

    my $centre = findPolygonCentre( $polygon );

    my $color = ( $DRAWING_STYLE eq DRAWING_STYLE_FILL ) ?
                        '0 setgray' : $POLYGON_COLORS->{ $polygon->{ 'TYPE' } };

    printf $fh "

        gsave 
            %s
            /Arial-Bold findfont %f scalefont setfont 
            %f %f moveto
            (%s) dup stringwidth pop -2 div 0 rmoveto
            show
        grestore
    ",
        $color,
        $font_sz * $TEXT_MULTIPLIER,
        $DRAWING_SCALE * $centre->{ 'X' },
        $DRAWING_SCALE * $centre->{ 'Y' },
        $polygon->{ 'ID' },
    ;

    for ( my $v = 0; $v < scalar @{ $vertices }; $v++ ) {

        printf $fh "gsave 
            /Arial-Bold findfont %f scalefont setfont 
            %f %f translate
            (%s) dup stringwidth pop -2 div %f moveto
            %s
            show
            grestore\n",
                $font_sz * $TEXT_MULTIPLIER,
                $DRAWING_SCALE * ( $centre->{ 'X' } + 3 * $vertices->[ $v ]->{ 'X' } ) / 4.0,
                $DRAWING_SCALE * ( $centre->{ 'Y' } + 3 * $vertices->[ $v ]->{ 'Y' } ) / 4.0,
                $v,
                $font_sz / -3,
                $color,
    } 
}

############################################################
# Find the distance between 2 vertices

sub distance {

    my $vtx1 = shift;
    my $vtx2 = shift;

    my $dx = $vtx1->{ 'X' } - $vtx2->{ 'X' };
    my $dy = $vtx1->{ 'Y' } - $vtx2->{ 'Y' };

    return sqrt( ( $dx * $dx ) +  ( $dy * $dy ) );
}

############################################################
# Return a point offset from the given vertex

sub offsetVertex {

    my $vertex = shift;

    return {
        'X' =>  $vertex->{ 'X' } + shift,
        'Y' =>  $vertex->{ 'Y' } + shift,
    };
}

############################################################
# Return a new vertex object

sub newVertex {

    return {
        'ANGLE'     =>  0,
        'X'         =>  shift,
        'Y'         =>  shift,
    };
}

############################################################
# Draw the polyons

sub drawImage {

    if ( my @ids = split /\//, $CENTRE ) {
        
        my $polygon = $POLYGON_LOOKUP->{ $ids[0] };
        my $centre = findPolygonCentre( $polygon );
        $centre = $polygon->{ 'VERTICES' }->[ $ids[ 1 ] ] if defined $ids[ 1 ];

        $X_OFFSET -= $DRAWING_SCALE * $centre->{ 'X' };
        $Y_OFFSET -= $DRAWING_SCALE * $centre->{ 'Y' };
    }

    open my $fh, '>', PS_TEMP or die sprintf( "Could not open %s - $!\n", PS_TEMP );

    printf $fh "%%!PS\n%%%%BoundingBox: 0 0 %f %f\n%%%%EndComments

        <<
            /PageSize [ %f %f ]
            /ImagingBBox null
        >> setpagedevice

        /mv { moveto } bind def
        /ln { lineto } bind def

        0 0 mv %f 0 ln %f %f ln 0 %f ln fill

        /Arial-Bold findfont 12 scalefont setfont

        1 setlinejoin
        %f setlinewidth

        %f %f translate
    ",
        $IMAGE_WIDTH, $IMAGE_DEPTH,
        $IMAGE_WIDTH, $IMAGE_DEPTH,
        $IMAGE_WIDTH, $IMAGE_WIDTH,
        $IMAGE_DEPTH, $IMAGE_DEPTH,
        $LINEWIDTH,
        $IMAGE_WIDTH / 2.0 + $X_OFFSET,
        $IMAGE_DEPTH / 2.0 + $Y_OFFSET,
    ;

    foreach ( @{ $POLYGONS } ) {

        drawPolygon( $fh, $_ ); 
        labelPolygon( $fh, $_ ) if $LABEL_POLYGONS;
    }

    close $fh;

    my $convert = "/usr/local/bin/convert";
    $convert = "/usr/bin/convert" unless ( -f $convert );
    $convert = "/Applications/ImageMagick/ImageMagick-6.6.5/bin/convert" unless ( -f $convert );

    unless ( -f $convert ) {

        printf "Count not locate convert at $convert\n";
    }
    else {

        system( sprintf "$convert '%s' '%s'", PS_TEMP, $OUTPUT_FILE );
    }

    return;
}

############################################################
# return a decription of the given polygon

sub getPolygonDescription {

    my $polygon = shift;

    return
            ( $polygon->{ 'TYPE' } eq POLYGON_TYPE_BOWTIE ) ?   'bowtie'
          : ( $polygon->{ 'TYPE' } eq POLYGON_TYPE_HKEY )   ?   'h-key'
          : ( $polygon->{ 'TYPE' } eq POLYGON_TYPE_VKEY )   ?   'v-key'
          : ( $polygon->{ 'TYPE' } eq POLYGON_TYPE_RHOMB )  ?   'rhomb'
          : ( $polygon->{ 'TYPE' } eq POLYGON_TYPE_SQUARE ) ?   'square'
          :                                                     ''
    ;
}

############################################################
# Dump polygon status

sub dumpPolygons {

    for my $polygon ( @{ $POLYGONS } ) {

        printf "P%s - %s\n", $polygon->{ 'ID' }, getPolygonDescription( $polygon );

        for ( my $v = 0; $v < scalar @{ $polygon->{ 'VERTICES' } }; $v++ ) {

            my $vtx = $polygon->{ 'VERTICES' }->[ $v ];
            printf "    %x (%f) %s [ ", $vtx, $vtx->{ 'ANGLE' }, $vtx->{ 'SHARED' } ? 'SHARED' : '';
            printf join ',', ( sort keys %{ $vtx->{ 'POLYGONS' } } );
            printf " ]\n";
        };
    }

    exit;
}

############################################################
# Help doco

sub help {

    print "\n@_\n" if @_;

    printf "\n$0 " .
                "[-d <deflations>] [-h] [-i <typ>] [-l] [-r <rad>] [-s <scale>] [-t <mul>] " .
                "[-x <off>] [-y <off>] " .
                "[-C <id[/vtx]>] [-F <file>] [-L <wid>] [-H <id>] [-D <val>] [-W <val>] " .
                "[-M] [-S <style>]

    where
        -d  number of deflation cycles (default 1)
        -h  this help message
        -i  initial polygon type ( 'B', 'H' or 'K' )
        -l  label the polygons
        -r  drawing radius
        -s  drawing scale
        -t  text multiplier
        -x  horizontal drawing offset
        -y  vertical drawing offset

        -?  dump the polygoon data

        -C  centre the images on a polygon[/vertex]
        -D  image depth
        -F  output file name
        -H  highligh the given polygon
        -L  linewidth for the image
        -M  show consolidation progress
        -S  drawing style (either 'fill' or 'draw' - the default)
        -W  image width

    \n";

    exit;
}

__END__
