#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <complex.h>

#include "fractal.h"
#include "complex.h"

/*******************************************************************************
 *
 * fractal.c
 *
 *  Functions to generate fractal images
 *
 *  File versions:
 *
 *      1.4     Now using long doubles
 *      1.3     Included image seed 
 *      1.2     Image type saved as an enum
 *      1.1     Included escape value
 *      1.0     Initial version
 *
 ******************************************************************************/

#define DATA_FILE   "fractal.dat"
#define IMAGE_FILE_VERSION  "1.5"

#define IMAGE_XC            -0.500
#define IMAGE_YC            0.000
#define IMAGE_SZ            2.500

#define JULIA_X_SEED        0.00
#define JULIA_Y_SEED        0.00

#define ESCAPE_RADIUS       2.000

#define MAX_ITERS           10000

#define PIC_WID             2860
#define PIC_DEP             1920

/*************************************************
 * Iterate the given point
 ************************************************/

_point *iterate( 
    _image      *image,
    _complex    *c,
    short       xpos, 
    short       ypos
) {

    if ( image->type == MANDELBROT_SET )
        return iterateMandelbrotPoint( image, c, xpos, ypos );

    if ( image->type == MANDELBROT3_SET )
        return iterateMandelbrot3Point( image, c, xpos, ypos );

    if ( image->type == JULIA_SET )
        return iterateJuliaPoint( image, c, xpos, ypos );

    if ( ( image->type == JULIA_SIN1 ) || ( image->type == JULIA_SIN2 ) )
        return iterateJuliaSinPoint( image, c, xpos, ypos );

    if ( ( image->type == JULIA_COS1 ) || ( image->type == JULIA_COS2 ) )
        return iterateJuliaCosPoint( image, c, xpos, ypos );

    if ( ( image->type == JULIA_TAN1 ) || ( image->type == JULIA_TAN2 ) )
        return iterateJuliaTanPoint( image, c, xpos, ypos );

    if ( image->type == OTHER )
        return iterateOtherPoint( image, c, xpos, ypos );

    logFatal( "No iteration func available" );

    return NULL;
}

/*************************************************
 * Find the type of attractor
 ************************************************/

complex long double findAttractorType(
    _image      *image
) {

    long double epsilon = nextafterl(2.0, 4.0) - 2.0;
    long double mzp = 1.0 / 0.0;
    complex long double c = image->seed.x + image->seed.y * I;
    complex long double z = c;
    complex long double f;
    complex long double df;
    complex long double w;
    complex long double dw;
    complex long double z0;
    complex long double z1;
    int p = 0;
    long double mzn;
    int i;
    int j;
    int n;

    z = image->seed.x + I * image->seed.y;

    for( n = 1; n < image->max_iters; ++n ) {

        mzn = cabs( z );

        if ( mzn < mzp ) {

            mzp = mzn;
            p = n;

            z0 = z;

            for ( i = 0; i < 64; ++i) {

                f = z0;
                df = 1.0;

                for( j = 0; j < p; ++j ) {

                    df = 2 * f * df;
                    f = f * f + c;
                }

                z1 = z0 - (f - z0) / (df - 1);
                if ( cabs( z1 - z0 ) <= epsilon ) {
                    z0 = z1;
                    break;
                }

                if ( 
                    isinf( creal(z1) ) || 
                    isinf( cimag(z1) ) ||
                    isnan( creal(z1) ) || 
                    isnan( cimag(z1) )
                ) {
                    break;
                }

                z0 = z1;
            }

            w = z0;
            dw = 1;

            for( i = 0; i < p; ++i ) {
                dw = 2 * w * dw;
                w = w * w + c;
            }

            if ( cabs( dw ) <= 1 ) {
                image->period = p;
                return z0;
            }
        }
        z = z * z + c;
    }
  
    image->period = 0;
  
    return 0;
}

/*************************************************
 * Iterate a point in the Julia space
 ************************************************/

_point *iterateJuliaPoint( 
    _image              *image,
    _complex            *c,
    short               xpos,
    short               ypos
) {
    complex long double seed;
    complex long double pnt;
    complex long double dist;
    long double         escape;
    _point              *point;

    seed = image->seed.x + I * image->seed.y;
    pnt = c->x + I * c->y;
    dist = 1.0;

    escape = image->escape * image->escape;

    point = &image->data[ ( ypos * image->pic_wid ) + xpos ];
    point->xpos = xpos;
    point->ypos = ypos;

    for( point->iters = 0; point->iters < image->max_iters; point->iters++ ) {

        dist = 2.0 * dist * pnt;
        pnt = pnt * pnt + seed;

        if ( cabsl( pnt ) > escape ) {
            point->escaped_x = creal(pnt);
            point->escaped_y = cimag(pnt);
            break;
        }
    }

    // src: http://imajeenyus.com/mathematics/20121112_distance_estimates/distance_estimation_method_for_fractals.pdf
    if ( point->iters < image->max_iters )
        point->dist = cabsl( pnt ) * log( cabsl( pnt ) ) / cabsl( dist );
    else
        point->dist = 0.0;

    return point;
}

/*************************************************
 * Iterate a point in the julia trig (sin) space
 ************************************************/

_point *iterateJuliaSinPoint( 
    _image              *image,
    _complex            *c,
    short               xpos,
    short               ypos
) {
    complex long double dist;
    complex long double pnt;
    complex long double seed;
    long double         escape;
    _point              *point;

    dist = 1.0;
    pnt = c->x + I * c->y;
    seed = image->seed.x + I * image->seed.y;

    escape = image->escape * image->escape;

    point = &image->data[ ( ypos * image->pic_wid ) + xpos ];
    point->xpos = xpos;
    point->ypos = ypos;
    point->dist = 0.0;

    for( point->iters = 0; point->iters < image->max_iters; point->iters++ ) {

        if ( image->type == JULIA_SIN1 )
            pnt = seed * csinl( pnt );
        else
            pnt = seed + csinl( pnt );
    
        if ( cabsl( pnt ) > escape ) {
            point->escaped_x = creal( pnt );
            point->escaped_y = cimag( pnt );
            point->dist = 255.0;
            break;
        }
    }

    // src: http://imajeenyus.com/mathematics/20121112_distance_estimates/distance_estimation_method_for_fractals.pdf
    if ( point->iters < image->max_iters )
        point->dist = cabsl( pnt ) * log( cabsl( pnt ) ) / cabsl( dist );

    return point;
}

/*************************************************
 * Iterate a point in the julia trig (cos) space
 ************************************************/

_point *iterateJuliaCosPoint( 
    _image              *image,
    _complex            *c,
    short               xpos,
    short               ypos
) {
    complex long double dist;
    complex long double pnt;
    complex long double seed;
    long double         escape;
    _point              *point;

    dist = 1.0;
    pnt = c->x + I * c->y;
    seed = image->seed.x + I * image->seed.y;

    escape = image->escape * image->escape;

    point = &image->data[ ( ypos * image->pic_wid ) + xpos ];
    point->xpos = xpos;
    point->ypos = ypos;

    point->dist = 0.0;

    for( point->iters = 0; point->iters < image->max_iters; point->iters++ ) {

        if ( image->type == JULIA_COS1 )
            pnt = seed * ccosl( pnt );
        else
            pnt = seed + ccosl( pnt );
   
        if ( cabsl( pnt ) > escape ) {
            point->escaped_x = creal( pnt );
            point->escaped_y = cimag( pnt );
            point->dist = 255.0;
            break;
        }
    }

    // src: http://imajeenyus.com/mathematics/20121112_distance_estimates/distance_estimation_method_for_fractals.pdf
    if ( point->iters < image->max_iters )
        point->dist = cabsl( pnt ) * log( cabsl( pnt ) ) / cabsl( dist );

    return point;
}

/*************************************************
 * Iterate a point in the julia trig (tan) space
 ************************************************/

_point *iterateJuliaTanPoint( 
    _image              *image,
    _complex            *c,
    short               xpos,
    short               ypos
) {
    complex long double dist;
    complex long double pnt;
    complex long double seed;
    complex long double ctmp;
    long double         escape;
    _point              *point;
    char                zero_denominator = 0;

    pnt = c->x + I * c->y;
    seed = image->seed.x + I * image->seed.y;
    dist = 1.0;

    escape = image->escape * image->escape;

    point = &image->data[ ( ypos * image->pic_wid ) + xpos ];
    point->xpos = xpos;
    point->ypos = ypos;

    point->dist = 0.0;

    for( point->iters = 0; point->iters < image->max_iters; point->iters++ ) {

        ctmp = ccosl( pnt );

        if ( cabsl( ctmp ) == 0 ) {

            zero_denominator = 1;
        }
        else {

            if ( image->type == JULIA_TAN1 )
                pnt = seed * ctanl( pnt );
            else
                pnt = seed + ctanl( pnt );
        }
    
        if ( zero_denominator || ( cabsl( pnt ) > escape ) ) {
            point->escaped_x = creal( pnt );
            point->escaped_y = cimag( pnt );
            point->dist = 255.0;
            break;
        }
    }

    // src: http://imajeenyus.com/mathematics/20121112_distance_estimates/distance_estimation_method_for_fractals.pdf
    if ( point->iters < image->max_iters )
        point->dist = cabsl( pnt ) * log( cabsl( pnt ) ) / cabsl( dist );

    return point;
}

/*************************************************
 * Check if the given point has been visited
 ************************************************/

BOOL alreadyVisited(
    _complex    pnt,
    int         iters   // if < 0 then reset the check point
) {

    BOOL    retval;
    double  reset_chk;

    static  _complex    check_pnt;

    if ( iters < 0 ) {
        check_pnt.x = pnt.x;
        check_pnt.y = pnt.y;
        return FALSE;
    }

    iters++;    // the value is 0-based

    retval = ( check_pnt.x == pnt.x ) && ( check_pnt.y == pnt.y );
   printf("%d: (%0.20Lf vs %0.20Lf) %d & (%0.20Lf vs %0.20Lf) %d = %d\n",
                   iters,
                   pnt.x, check_pnt.x,
                   ( check_pnt.x == pnt.x ) ? 1 : 0,
                   pnt.y, check_pnt.y, 
                   ( check_pnt.y == pnt.y ) ? 1 : 0,
                   retval
    ); 

    reset_chk = log( iters ) / log( 2.0 );

    if ( reset_chk == (int)(reset_chk) ) {
            printf("reset\n");
        check_pnt.x = pnt.x;
        check_pnt.y = pnt.y;
    }

    return retval;
}

/*************************************************
 * Iterate a point in another alternate space
 ************************************************/

_point *iterateOtherPoint( 
    _image      *image,
    _complex    *c,
    short       xpos,
    short       ypos
) {
    _complex    pnt;
    _complex    tmp1;
    _complex    tmp2;
    _complex    tmp3;
    long double escape;
    _point      *point;

    pnt.x = c->x;
    pnt.y = c->y;

    alreadyVisited( pnt, -1 );    // prime the check point

    escape = image->escape * image->escape;

    point = &image->data[ ( ypos * image->pic_wid ) + xpos ];
    point->xpos = xpos;
    point->ypos = ypos;

    point->dist = 0.0;

    for( point->iters = 0; point->iters < image->max_iters; point->iters++ ) {

        powc( pnt, 3, &tmp1 );

        addc( tmp1, image->seed, &tmp2 );
        subc( tmp1, image->seed, &tmp3 );
        divc( tmp2, tmp3, &pnt );
   
        if ( alreadyVisited( pnt, point->iters ) ) {
                printf(".");
            point->dist = 1.0;
            break;
        }

        if ( ( ( pnt.x * pnt.x ) + ( pnt.y * pnt.y ) ) > escape ) {
            point->escaped_x = pnt.x;
            point->escaped_y = pnt.y;
            point->dist = 1.0;
            break;
        }
    }

    // src: http://imajeenyus.com/mathematics/20121112_distance_estimates/distance_estimation_method_for_fractals.pdf
    //if ( point->iters < image->max_iters )
    //    point->dist = MOD( pnt ) * log( MOD( pnt ) ) / MOD( d );
    //
    return point;
}

/*************************************************
 * Iterate a point in the Mandelbrot space
 ************************************************/

_point *iterateMandelbrotPoint( 
    _image      *image,
    _complex    *c,
    short       xpos,
    short       ypos
) {
    _complex    pnt;
    _complex    d;
    long double escape;
    long double z;
    _point      *point;

    pnt.x = 0.0;
    pnt.y = 0.0;

    d.x = 1.0;
    d.y = 0.0;

    escape = image->escape * image->escape;

    point = &image->data[ ( ypos * image->pic_wid ) + xpos ];
    point->xpos = xpos;
    point->ypos = ypos;

    for( point->iters = 0; point->iters < image->max_iters; point->iters++ ) {

        z = d.x;
        d.x = 2.0 * ( ( d.x * pnt.x ) - ( d.y * pnt.y ) ) + 1.0;
        d.y = 2.0 * ( ( z * pnt.y ) + ( d.y * pnt.x ) );

        z = pnt.x;
        pnt.x = ( pnt.x * pnt.x ) - ( pnt.y * pnt.y ) + c->x;
        pnt.y = 2.0 * z * pnt.y + c->y;
    
        if ( ( ( pnt.x * pnt.x ) + ( pnt.y * pnt.y ) ) > escape ) {
            point->escaped_x = pnt.x;
            point->escaped_y = pnt.y;
            break;
        }
    }

    // src: http://imajeenyus.com/mathematics/20121112_distance_estimates/distance_estimation_method_for_fractals.pdf
    if ( point->iters < image->max_iters )
        point->dist = MOD( pnt ) * log( MOD( pnt ) ) / MOD( d );
    else
        point->dist = 0.0;

    return point;
}

/*************************************************
 * Iterate a point in the Mandelbrot cubed space
 ************************************************/

_point *iterateMandelbrot3Point( 
    _image      *image,
    _complex    *c,
    short       xpos,
    short       ypos
) {
    _complex    pnt;
    _complex    d;
    long double escape;
    long double z;
    _point      *point;

    pnt.x = 0.0;
    pnt.y = 0.0;

    d.x = 1.0;
    d.y = 0.0;

    escape = image->escape * image->escape;

    point = &image->data[ ( ypos * image->pic_wid ) + xpos ];
    point->xpos = xpos;
    point->ypos = ypos;

    for( point->iters = 0; point->iters < image->max_iters; point->iters++ ) {

        z = d.x;
        d.x = 2.0 * ( ( d.x * pnt.x ) - ( d.y * pnt.y ) ) + 1.0;
        d.y = 2.0 * ( ( z * pnt.y ) + ( d.y * pnt.x ) );

        z = pnt.x;
        pnt.x = ( pnt.x * pnt.x ) - ( pnt.y * pnt.y ) + c->x;
        pnt.y = 2.0 * z * pnt.y + c->y;
    
        if ( ( ( pnt.x * pnt.x ) + ( pnt.y * pnt.y ) ) > escape ) {
            point->escaped_x = pnt.x;
            point->escaped_y = pnt.y;
            break;
        }
    }

    // src: http://imajeenyus.com/mathematics/20121112_distance_estimates/distance_estimation_method_for_fractals.pdf
    if ( point->iters < image->max_iters )
        point->dist = MOD( pnt ) * log( MOD( pnt ) ) / MOD( d );
    else
        point->dist = 0.0;

    return point;
}

/*************************************************
 * Store the image data
 ************************************************/

void writeImage(
    _image  *image,
    char    *dest
) {
    FILE    *fp;

    if ( ( fp = fopen( dest, "w" ) ) == NULL )
        logFatal( "Failed to open %s", dest );

    printf( "Writing...\n" );

    fprintf( fp, "%s\n", image->version );
    fwrite( &image->type, sizeof( int ), 1, fp );

    fprintf( fp, "%s\n", image->description );

    fwrite( &image->seed.x, sizeof( long double ), 1, fp );
    fwrite( &image->seed.y, sizeof( long double ), 1, fp );

    fwrite( &image->xc, sizeof( long double ), 1, fp );
    fwrite( &image->yc, sizeof( long double ), 1, fp );
    fwrite( &image->sz, sizeof( long double ), 1, fp );

    fwrite( &image->escape, sizeof( long double ), 1, fp );
    fwrite( &image->max_iters, sizeof( int ), 1, fp );
    fwrite( &image->max_image_iters, sizeof( int ), 1, fp );

    fwrite( &image->pic_wid, sizeof( int ), 1, fp );
    fwrite( &image->pic_dep, sizeof( int ), 1, fp );

    fwrite( &image->attractor, sizeof( int ), 1, fp );
    fwrite( &image->period, sizeof( int ), 1, fp );

    fwrite( image->data, sizeof( _point ), image->pic_wid*image->pic_dep, fp );

    fclose( fp );
}

/*************************************************
 * Process and runline parameters
 ************************************************/

void processParameters(
    int     argc,
    char    **argv,
    _image  *image
) {
    int     pnt = 1;
    char    ch;

    while ( pnt < argc ) {
    
        if ( *argv[ pnt ] != '-' )
            logFatal( "Unknown switch %s", argv[ pnt ] );

        if ( strcmp( &argv[ pnt ][ 1 ], "-julia" ) == 0 ) {
            image->type = JULIA_SET;
            image->data_file = "julia.dat";
            pnt++;
            continue;
        }

        if ( strncmp( &argv[ pnt ][ 1 ], "-julia-sin", 10 ) == 0 ) {
            image->type = ( argv[ pnt ][ 2 ] == '2' ) ? JULIA_SIN2 : JULIA_SIN1;
            image->data_file = "julia-sin.dat";
            pnt++;
            continue;
        }

        if ( strncmp( &argv[ pnt ][ 1 ], "-julia-cos", 10 ) == 0 ) {
            image->type = ( argv[ pnt ][ 2 ] == '2' ) ? JULIA_COS2 : JULIA_COS1;
            image->data_file = "julia-cos.dat";
            pnt++;
            continue;
        }

        if ( strncmp( &argv[ pnt ][ 1 ], "-julia-tan", 10 ) == 0 ) {
            image->type = ( argv[ pnt ][ 2 ] == '2' ) ? JULIA_TAN2 : JULIA_TAN1;
            image->data_file = "julia-tan.dat";
            pnt++;
            continue;
        }

        if ( strcmp( &argv[ pnt ][ 1 ], "-other" ) == 0 ) {
            image->type = OTHER;
            image->data_file = "other.dat";
            pnt++;
            continue;
        }

        if ( strcmp( &argv[ pnt ][ 1 ], "-mandelbrot" ) == 0 ) {
            image->type = MANDELBROT_SET;
            image->data_file = "mandelbrot.dat";
            pnt++;
            continue;
        }

        switch( argv[ pnt ][ 1 ] ) {

            case 'h' :
                usage( argv[ 0 ] );
                break;

            case 'd' :
                getShortParameter( argc, argv, ++pnt, &image->pic_dep, "image depth" );
                break;

            case 'i' :
                getIntParameter( argc, argv, ++pnt, &image->max_iters, "iteration limit" );
                break;

            case 'e' :
                getDoubleParameter( argc, argv, ++pnt, &image->escape, "escape radius" );
                break;

            case 'f' :
                getCharParameter( argc, argv, ++pnt, &image->data_file, "data file" );
                break;

            case 'j' :
                if ( ( ch = argv[ pnt ][ 2 ] ) == 'x' )
                    getDoubleParameter( argc, argv, ++pnt, &image->seed.x, "data file" );
                else if ( ch  == 'y' )
                    getDoubleParameter( argc, argv, ++pnt, &image->seed.y, "data file" );
                else
                    logFatal( "Unknown switch %s", argv[ pnt ] );
                break;

            case 'n' :
                getCharParameter( argc, argv, ++pnt, &image->description, "description" );
                break;

            case 'w' :
                getShortParameter( argc, argv, ++pnt, &image->pic_wid, "image width" );
                break;

            case 's' :
                getDoubleParameter( argc, argv, ++pnt, &image->sz, "image radius" );
                if ( image->sz == 0.0 )
                    logFatal( "Image size cannot be 0" );
                break;

            case 'x' :
                getDoubleParameter( argc, argv, ++pnt, &image->xc, "image x centre" );
                break;

            case 'y' :
                getDoubleParameter( argc, argv, ++pnt, &image->yc, "image y centre" );
                break;

            default:
                logFatal( "Unknown switch %s", argv[ pnt ] );
        }

        pnt++;
    }

}

/*************************************************
 * Setup the image details
 ************************************************/

_image *setupImage() {

    _image      *image;

    if ( ( image = ALLOC( _image ) ) == NULL )
        logFatal( "failed the calloc() image details" );

    image->data_file = DATA_FILE;

    image->version = IMAGE_FILE_VERSION;
    image->type = MANDELBROT_SET;

    image->xc = IMAGE_XC;
    image->yc = IMAGE_YC;
    image->sz = IMAGE_SZ;

    image->max_iters = MAX_ITERS;

    image->pic_dep = PIC_DEP;
    image->pic_wid = PIC_WID;

    image->escape = ESCAPE_RADIUS;

    image->seed.x = JULIA_X_SEED;
    image->seed.y = JULIA_Y_SEED;

    return image;
}

/*************************************************
 * Main
 ************************************************/

int main(
    int         argc,
    char        **argv
) {
    _complex    c;
    short       x;
    short       y;
    long double scale_x;
    long double scale_y;
    _image      *image;
    _point      *point;

    initialiseLogging( "fractal.log", LOGGING_LEVEL_QUIET );

    image = setupImage();
    processParameters( argc, argv, image );

    image->data = NALLOC( image->pic_dep*image->pic_wid, _point );
    if ( image->data == NULL )
        logFatal( "calloc() for image data failed" );

    if ( image->pic_wid > image->pic_dep ) {
        scale_x = image->pic_wid / (long double)image->pic_dep;
        scale_y = 1.0;
    }
    else {
        scale_y = image->pic_dep / (long double)image->pic_wid;
        scale_x = 1.0;
    }

    // Test a single point
    //setc(-0.75, -1, &c);
    //iterate( image, &c, 0, 0 );
    //exit(-1);

    for( y = 0; y < image->pic_dep; y++ ) {

        printf( "\rDone - %0.3Lf%%", (100.0 * y / (long double)image->pic_dep ) );
        fflush( stdout );

        c.y = image->yc + scale_y * image->sz * ( ( 2.0 * ( y + 0.5 ) / (long double)image->pic_dep ) - 1.0 );
    
        for( x = 0; x < image->pic_wid; x++ ) {

            c.x = image->xc + scale_x * image->sz * ( ( 2.0 * ( x + 0.5 ) / (long double)image->pic_wid ) - 1.0 );

            point = iterate( image, &c, x, y );

            if ( point->iters > image->max_image_iters )
                image->max_image_iters = point->iters;
        }
    }

    printf( "\rDone - 100%%   \n" );

    writeImage( image, image->data_file );

    return 0;
}

/*************************************************
 * Usage
 ************************************************/

void usage(
    char    *self
) {

    printf( "\n" );
    printf( "Usage: %s\n", self );
    printf( "          [--julia|--julia-sin*|--julia-cos*|--julia-tan*|--mandelbrot|--other]\n" );
    printf( "          [-x <x centre>] [-y <y centre>] [-s <img size>]\n" );
    printf( "          [-x <x centre>] [-y <y centre>] [-s <img size>]\n" );
    printf( "          [-jx <x seed>] [-jy <y seed>]\n" );
    printf( "          [-d <pic depth>] [-w <pic width>]\n" );
    printf( "          [-e <escape radius>] -i <iters>\n" );
    printf( "          [-c <desc>] [-f <data file>]\n" );
    printf( "\n" );
    printf( "   where:\n" );
    printf( "       --julia         Julia set z = z*z+c (default output - julia.dat)\n");
    printf( "       --julia-sin1    Julia set for z = c.sin(z) (default output - julia-sin.dat)\n");
    printf( "       --julia-sin2    Julia set for z = c+sin(z) (default output - julia-sin.dat)\n");
    printf( "       --julia-cos1    Julia set for z = c.cos(z) (default output - julia-cos.dat)\n");
    printf( "       --julia-cos2    Julia set for z = c+cos(z) (default output - julia-cos.dat)\n");
    printf( "       --julia-tan1    Julia set for z = c.tan(z) (default output - julia-tan.dat)\n");
    printf( "       --julia-tan2    Julia set for z = c+tan(z) (default output - julia-tan.dat)\n");
    printf( "       --other         alternative set (default output - other.dat)\n");
    printf( "       --mandelbox     Mandelbox set (default output - mandelbox.dat)\n");
    printf( "       --mandelbrot    Mandelbrot set (default output - mandelbrot.dat)\n");
    printf( "\n" );
    printf( "   and other parameters are:\n" );
    printf( "       -c <description>    description of the image\n" );
    printf( "       -d <pic depth>      height in pixels of the picture (default %d)\n", PIC_DEP );
    printf( "       -e <escape radius>  upper bound for iteration point distance (default %f)\n", ESCAPE_RADIUS );
    printf( "       -f <data file>      path to the data file (default %s)\n", DATA_FILE );
    printf( "       -i <max iters>      iteration limit (default %d)\n", MAX_ITERS );
    printf( "       -jx <x seed>        initial Julia seed (default %f)\n", JULIA_X_SEED );
    printf( "       -jy <y seed>        initial Julia seed (default %f)\n", JULIA_Y_SEED );
    printf( "       -s <img size>       image size (default %f)\n", IMAGE_SZ );
    printf( "       -w <pic width>      width in pixels of the picture (default %d)\n", PIC_WID );
    printf( "       -x <x centre>       image x centre in the plane (default %f)\n", IMAGE_XC );
    printf( "       -y <y centre>       image y centre in the plane (default %f)\n", IMAGE_YC );

    exit( 0 );
}
