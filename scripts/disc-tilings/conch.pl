#!/usr/bin/perl

use warnings;
use strict;

use Math::Trig;
use Getopt::Std;

use constant    POLYGON_TYPE_YELLOW => 'polygon_type_yellow';
use constant    POLYGON_TYPE_RED    => 'polygon_type_red';

use constant    HEMISPHERE_LOWER    => 'lower';
use constant    HEMISPHERE_UPPER    => 'upper';

my $INFLATIONS = 2;

my $POLYGONS = [];

my $IMAGE_WIDTH = 1000;
my $IMAGE_DEPTH = 1000;

############################################################
# Check the runline options

my %options;
getopts( 'b:d:hkn:o:p:r:s:', \%options );

help() if $options{ 'h' };

my $BANDS = $options{ 'b' } || 3;
my $KINK_DIST = $options{ 'd' } || 1.0 / 7.0;
my $DRAW_KINKS = $options{ 'k' };
my $N_GON = $options{ 'n' } || 18;
my $DEST = $options{ 'o' } || 'conch.pdf';
my $KINK_PERP = $options{ 'p' } || 1.0 / 5.0;
my $RADIUS = $options{ 'r' } || 400;
my $SCALE = $options{ 's' } || 1.0;

############################################################
# Process the image

setupInitialPolygons();

processBands();
#inflatePolygons();

drawImage();

exit;

################################################################################
# Setup initial polygons

sub setupInitialPolygons {

    for ( my $n = 0; $n < $N_GON; $n++ ) {
    
        my $ang1 = $n * 2 * pi / $N_GON;
        my $ang2 = ( $n + 1 ) * 2 * pi / $N_GON;
    
        my $offset = $n < ($N_GON / 2.0 ) ? $RADIUS / $BANDS : 0;
    
        push @{ $POLYGONS }, {
            'TYPE'          =>  ( $n % 2 ) > 0 ? POLYGON_TYPE_YELLOW : POLYGON_TYPE_RED,
            'BAND'          =>  0,
            'POSN'          =>  $n,
            'HEMISPHERE'    =>  ( $ang1 < pi ) ? HEMISPHERE_UPPER : HEMISPHERE_LOWER,
            'VERTICES'      =>  [
                        { 'X' => $offset, 'Y' => 0.0 },
                        { 'X' => $offset + ( $RADIUS * cos( $ang1 ) ), 'Y' => $RADIUS * sin( $ang1 ) },
                        { 'X' => $offset + ( $RADIUS * cos( $ang2 ) ), 'Y' => $RADIUS * sin( $ang2 ) },
            ],
        };
    }
}

################################################################################
# Process the bands

sub processBands {

    my $new_polygons = [];

    for my $polygon ( @{ $POLYGONS } ) {
    
        my $vertices = $polygon->{ 'VERTICES' };

        my $same_type = $polygon->{ 'TYPE' };
        my $other_type = $polygon->{ 'TYPE' } eq POLYGON_TYPE_YELLOW ? POLYGON_TYPE_RED : POLYGON_TYPE_YELLOW;
    
        for ( my $b = 0; $b < $BANDS; $b++ ) {

            my $lo_band1 = pointAlong( $vertices->[ 0 ], $vertices->[ 1 ], $b / $BANDS  );
            my $lo_band2 = pointAlong( $vertices->[ 0 ], $vertices->[ 2 ], $b / $BANDS  );

            my $hi_band1 = pointAlong( $vertices->[ 0 ], $vertices->[ 1 ], ( $b + 1 ) / $BANDS  );
            my $hi_band2 = pointAlong( $vertices->[ 0 ], $vertices->[ 2 ], ( $b + 1 ) / $BANDS  );

            for ( my $t = 0; $t < 2 * $b + 1; $t++ ) {

                my $h = ( $polygon->{ 'HEMISPHERE' } eq HEMISPHERE_LOWER ) ? 0 : 1;
                if ( $h && $b ) { $h++ }
                    

                my $polygon_type = ( ( $polygon->{ 'POSN' } + $t + 0 ) % 2 ) == 0 ? POLYGON_TYPE_RED : POLYGON_TYPE_YELLOW;

                if ( $t % 2 ) {

                    push @{ $new_polygons }, {
                            'TYPE'          =>  $polygon_type,
                            'HEMISPHERE'    =>  $polygon->{ 'HEMISPHERE' },
                            'BAND'          =>  $b,
                            'VERTICES'      =>  [
                                    pointAlong( $hi_band1, $hi_band2, ( ( $t + 1 ) / 2 ) / ( $b + 1 ) ),
                                    pointAlong( $lo_band1, $lo_band2, ( ( $t + 1 ) / 2 ) / $b ),
                                    pointAlong( $lo_band1, $lo_band2, ( ( $t - 1 ) / 2 ) / $b ),
                            ],
                    };
                }
                else {

                    push @{ $new_polygons }, {
                            'TYPE'          =>  $polygon_type,
                            'HEMISPHERE'    =>  $polygon->{ 'HEMISPHERE' },
                            'BAND'          =>  $b,
                            'VERTICES'      =>  [
                                    pointAlong( $lo_band1, $lo_band2, $b ? $t / ( $b * 2 ) : 0 ),
                                    pointAlong( $hi_band1, $hi_band2, ( $t / 2 ) / ( $b + 1 ) ),
                                    pointAlong( $hi_band1, $hi_band2, ( ( $t / 2 ) + 1 ) / ( $b + 1 ) ),
                            ],
                    };
                }
            }
        }
    }

    $POLYGONS = $new_polygons;
}

################################################################################
# Swap the polygon type

sub swapPolygonType {

    my $polygon_type = shift;

    if ( $polygon_type eq POLYGON_TYPE_YELLOW ) {

        $polygon_type = POLYGON_TYPE_RED;
    }
    else {

        $polygon_type = POLYGON_TYPE_YELLOW;
    }

    return $polygon_type;
}

################################################################################
# Return a point a given proportion along a line between a points

sub pointAlong {

    my $vertex1 = shift;
    my $vertex2 = shift;
    my $proportion = shift;

    return {
        'X' =>  $vertex1->{ 'X' } + ( $vertex2->{ 'X' } - $vertex1->{ 'X' } ) * $proportion,
        'Y' =>  $vertex1->{ 'Y' } + ( $vertex2->{ 'Y' } - $vertex1->{ 'Y' } ) * $proportion,
    }
}

################################################################################
# Inflate the polygons

sub inflatePolygons {

    for ( my $b = 0; $b < $INFLATIONS; $b++ ) {
    
        my $new_polygons = [];
    
        for my $polygon ( @{ $POLYGONS } ) {
    
            my $vertices = $polygon->{ 'VERTICES' };
    
            my $midpt1 = {
                'X' =>  ( $vertices->[ 0 ]->{ 'X' } + $vertices->[ 1 ]->{ 'X' } ) / 2.0,
                'Y' =>  ( $vertices->[ 0 ]->{ 'Y' } + $vertices->[ 1 ]->{ 'Y' } ) / 2.0,  
            };
    
            my $midpt2 = {
                'X' =>  ( $vertices->[ 1 ]->{ 'X' } + $vertices->[ 2 ]->{ 'X' } ) / 2.0,
                'Y' =>  ( $vertices->[ 1 ]->{ 'Y' } + $vertices->[ 2 ]->{ 'Y' } ) / 2.0,  
            };
    
            my $midpt3 = {
                'X' =>  ( $vertices->[ 0 ]->{ 'X' } + $vertices->[ 2 ]->{ 'X' } ) / 2.0,
                'Y' =>  ( $vertices->[ 0 ]->{ 'Y' } + $vertices->[ 2 ]->{ 'Y' } ) / 2.0,  
            };
    
            push @{ $new_polygons }, {
                'TYPE'      =>  $polygon->{ 'TYPE' },
                'VERTICES'  =>  [
                        $vertices->[ 0 ],
                        $midpt1,
                        $midpt3,
                ],
            };
    
            push @{ $new_polygons }, {
                'TYPE'      =>  $polygon->{ 'TYPE' },
                'VERTICES'  =>  [
                        $midpt1,
                        $vertices->[ 1 ],
                        $midpt2,
                ],
            };
    
            push @{ $new_polygons }, {
                'TYPE'      =>  $polygon->{ 'TYPE' } eq POLYGON_TYPE_YELLOW ? POLYGON_TYPE_RED : POLYGON_TYPE_YELLOW,
                'VERTICES'  =>  [
                        $midpt2,
                        $midpt3,
                        $midpt1,
                ],
            };
    
            push @{ $new_polygons }, {
                'TYPE'      =>  $polygon->{ 'TYPE' },
                'VERTICES'  =>  [
                        $vertices->[ 2 ],
                        $midpt3,
                        $midpt2,
                ],
            };
        }
    
        $POLYGONS = $new_polygons;
    }
}

################################################################################
# Draw a kinked line between 2 points

sub drawKink {
    
    my $fh = shift;
    my $vtx1 = shift;
    my $vtx2 = shift;
    my $reverse = shift || 0;

    my $k_dist = $KINK_DIST;
    my $k_perp = $KINK_PERP;

    my $tmp_vtx = {
        'X' =>  $k_dist * ( $vtx2->{ 'X' } - $vtx1->{ 'X' } ) + $vtx1->{ 'X' },
        'Y' =>  $k_dist * ( $vtx2->{ 'Y' } - $vtx1->{ 'Y' } ) + $vtx1->{ 'Y' },
    };

    my $nvtx1 = {
        'X' =>  $tmp_vtx->{ 'X' } - $k_perp * ( $vtx2->{ 'Y' } - $vtx1->{ 'Y' } ),
        'Y' =>  $tmp_vtx->{ 'Y' } + $k_perp * ( $vtx2->{ 'X' } - $vtx1->{ 'X' } ),
    };

    $tmp_vtx = {
        'X' =>  ( 1.0 - $k_dist ) * ( $vtx2->{ 'X' } - $vtx1->{ 'X' } ) + $vtx1->{ 'X' },
        'Y' =>  ( 1.0 - $k_dist ) * ( $vtx2->{ 'Y' } - $vtx1->{ 'Y' } ) + $vtx1->{ 'Y' },
    };

    my $nvtx2 = {
        'X' =>  $tmp_vtx->{ 'X' } + $k_perp * ( $vtx2->{ 'Y' } - $vtx1->{ 'Y' } ),
        'Y' =>  $tmp_vtx->{ 'Y' } - $k_perp * ( $vtx2->{ 'X' } - $vtx1->{ 'X' } ),
    };

    if ( $reverse ) {
        printf $fh "%f %f lineto ", $nvtx1->{ 'X' }, $nvtx1->{ 'Y' };
        printf $fh "%f %f lineto ", $nvtx2->{ 'X' }, $nvtx2->{ 'Y' };
        printf $fh "%f %f lineto ", $vtx2->{ 'X' }, $vtx2->{ 'Y' };
    }
    else {
        printf $fh "%f %f lineto ", $nvtx2->{ 'X' }, $nvtx2->{ 'Y' };
        printf $fh "%f %f lineto ", $nvtx1->{ 'X' }, $nvtx1->{ 'Y' };
        printf $fh "%f %f lineto ", $vtx1->{ 'X' }, $vtx1->{ 'Y' };
    }
}

################################################################################
# Draw the polygons

sub drawImage {

    my $temp_file = 'conch.ps';
    open my $fh, '>', $temp_file;
    
    printf $fh "%%!PS\n%%BoundingBox: 0 0 %f %f\n%%EndComments\n
    
            <<
                /PageSize [ %f %f ]
                /ImagingBBox null
            >> setpagedevice
    
            /mv { moveto } bind def
            /ln { lineto } bind def
    
            0 0 mv %f 0 ln %f %f ln 0 %f ln fill
    
            %f %f translate
            %f dup scale
    
            1 setlinejoin
            1 1 0 setrgbcolor
        ",
            $IMAGE_WIDTH, $IMAGE_DEPTH,
            $IMAGE_WIDTH, $IMAGE_WIDTH,
            $IMAGE_WIDTH, $IMAGE_WIDTH,
            $IMAGE_DEPTH, $IMAGE_DEPTH,
            ( $IMAGE_DEPTH / 2 ) - $RADIUS / ( 2 * $BANDS ), $IMAGE_DEPTH / 2,
            $SCALE,
    ;
    
    for my $polygon ( @{ $POLYGONS } ) {
    
        my $vertices = $polygon->{ 'VERTICES' };
        
        if ( $DRAW_KINKS ) {

            printf $fh "%f %f moveto %f %f lineto ", 
                        $vertices->[ 1 ]->{ 'X' }, $vertices->[ 1 ]->{ 'Y' }, 
                        $vertices->[ 2 ]->{ 'X' }, $vertices->[ 2 ]->{ 'Y' }, 
            ;
    
            drawKink( $fh, $vertices->[ 0 ], $vertices->[ 2 ], 0 );
            drawKink( $fh, $vertices->[ 0 ], $vertices->[ 1 ], 1 );
        }
        else {

            printf $fh "%f %f moveto %f %f lineto %f %f lineto ", 
                        $vertices->[ 0 ]->{ 'X' }, $vertices->[ 0 ]->{ 'Y' }, 
                        $vertices->[ 1 ]->{ 'X' }, $vertices->[ 1 ]->{ 'Y' }, 
                        $vertices->[ 2 ]->{ 'X' }, $vertices->[ 2 ]->{ 'Y' }, 
            ;
        }

        if ( $polygon->{ 'TYPE' } eq POLYGON_TYPE_YELLOW ) {
            printf $fh "1 1 0 setrgbcolor\n";
        } else {
            printf $fh "1 0 0 setrgbcolor\n";
        }
    
        printf $fh "fill\n";
        #printf $fh "0 setgray closepath stroke\n";
    }
    
    close $fh;
    
    system( "convert $temp_file $DEST" );

    unlink $temp_file;
}

################################################################################
# Help doco

sub help {

    print "\n@_\n" if @_;

    printf "\n$0 " .
        "[-b <bands>] [-h] [-k] [-n <gon>] [-r <rad>] -s <sc>]

    where
        -b  number of segment bands (default 3)
        -d  dist along the axis for a kink
        -h  this help message
        -k  draw kinked polygons
        -n  number of sides to the initial polygon (default 18)
        -p  dist from the axis for a kink
        -r  initial polygon radius (default 400)
        -s  drawing scale (default 1.0)
        \n";

    exit 0;
}
