#!/usr/bin/perl

use warnings;
use strict;
use Math::Trig;

my $GENERATOR = 'FF-[-F+F+F]+[+F-F-F]';
$GENERATOR = 'F+F-F';

my $ANG = deg2rad( 120 );
my $LINE_LEN = 1;

my $MAX_DEPTH = 20;

my $current_x = 0;
my $current_y = 0;
my $current_ang = 0;

my $count = 0;

my @g_states;

print "%!PS

    0 0 moveto
    1000 0 rlineto
    0 1000 rlineto
    -1000 0 rlineto
    fill

    120 180 translate
    0.3 setlinewidth
";

processGenerator( 0 );

exit 0;

sub grestore {

    my $state = pop @g_states;

    $current_x = $state->{ 'saved_x' };
    $current_y = $state->{ 'saved_y' };
    $current_ang = $state->{ 'saved_ang' };
}

sub gsave {

    push @g_states, {

        'saved_x'     =>  $current_x,
        'saved_y'     =>  $current_y,
        'saved_ang'   =>  $current_ang,
    };
}

sub processLine {

    my $depth = shift;

    if ( $depth < $MAX_DEPTH ) {
        
        my $new_depth = $depth + 1;

        if ( $new_depth == $MAX_DEPTH ) {

            my $red = rand( 256 ) / 256;
            my $green = rand( 256 ) / 256;

            $red = $count / 120000;
            $green = $count / 120000;

            printf "$red $green 1 setrgbcolor stroke\n";
        }
        processGenerator( $depth + 1 );
    } 
    else {

        printf "%f %f moveto \n", $current_x, $current_y;

        $current_x += $LINE_LEN * sin( $current_ang );
        $current_y += $LINE_LEN * cos( $current_ang );

        printf "%f %f lineto \n", $current_x, $current_y;
    }

    $count++;
}

sub processGenerator {

    my $depth = shift;

    for ( split //, $GENERATOR ) {

        /F/ && do { processLine( $depth+1 ); next };
        /-/ && do { $current_ang += $ANG; next };
        /\+/ && do { $current_ang -= $ANG; next };
        /\[/ && do { gsave(); next };
        /\]/ && do { grestore(); next };
    }
}
