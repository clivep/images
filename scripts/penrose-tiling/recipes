#!/bin/sh

imgdir=../../image_files/penrose-tilings/
mkdir -p $imgdir 2> /dev/null

echo "
    name: Penrose Tiling
    desc: Penrose Tiling
    ref: Penrose tiling - Wikipedia|https://www.google.com.au/url?sa=t&rct=j&q=&esrc=s&source=web&cd=3&cad=rja&uact=8&ved=0ahUKEwit2rXtif_XAhWGnZQKHVL0AyoQFggvMAI&url=https%3A%2F%2Fen.wikipedia.org%2Fwiki%2FPenrose_tiling&usg=AOvVaw2p9P1vOno1HLV7KVy1A_2L
    ref: Images for penrose tiling|https://www.google.com.au/search?q=penrose+tiling&rlz=1C1GIGM_enAU541AU541&tbm=isch&tbo=u&source=univ&sa=X&ved=0ahUKEwit2rXtif_XAhWGnZQKHVL0AyoQsAQIaA
" > $imgdir/type.info

################################################################################
# Penrose P1 Tilings (stars, boats, pentagon and diamonds)

./p1-tiling.pl -i 5 -s 30 -S fill -W 1000 -D 1000 -F $imgdir/p1-i5-s30.png
echo "
    group: Penrose P1 Tiling
    thumb-desc: Penrose P1 Tiling
    image-title: Penrose P1 Tiling
    image-detail: Penrose P1 Tiling (5 deflation)
" > $imgdir/p1-i5-s30.info

./p1-tiling.pl -i 5 -s 30 -S fill -r 450 -F $imgdir/p1-i5-s30-r450.png
echo "
    group: Penrose P1 Tiling
    thumb-desc: Penrose P1 Tiling
    image-title: Penrose P1 Tiling
    image-detail: Penrose P1 Tiling (5th deflation)
" > $imgdir/p1-i5-s30-r450.info

./p1-tiling.pl -i 3 -s 25 -S fill -r 450 -F $imgdir/p1-i3-s25-r450.png
echo "
    group: Penrose P1 Tiling
    thumb-desc: Penrose P1 Tiling
    image-title: Penrose P1 Tiling
    image-detail: Penrose P1 Tiling (3rd deflation)
" > $imgdir/p1-i3-s25-r450.info

./p1-tiling.pl -i 5 -s 30 -W 1000 -D 1000 -F $imgdir/p1-l-i5-s30.png
echo "
    group: Penrose P1 Tiling
    thumb-desc: Penrose P1 Tiling
    image-title: Penrose P1 Tiling
    image-detail: Penrose P1 Tiling (5th deflation)
" > $imgdir/p1-l-i5-s30.info

./p1-tiling.pl -i 5 -s 30 -r 450 -F $imgdir/p1-l-i5-s30-r450.png
echo "
    group: Penrose P1 Tiling
    thumb-desc: Penrose P1 Tiling
    image-title: Penrose P1 Tiling
    image-detail: Penrose P1 Tiling (5th deflation)
" > $imgdir/p1-l-i5-s30-r450.info

################################################################################
# Penrose P2 Tilings (kites and darts)

./p2-deflation.pl -r 'KK---K--K--K' -d 6 -s 3.5 -f -F $imgdir/p2-d6-K-s3p5.png
./p2-deflation.pl -r 'KK---K--K--K' -d 8 -s 8 -f -W 1000 -D 1000 -F $imgdir/p2-d6-K-s8.png
./p2-deflation.pl -r 'D-D---D--D--D' -d 6 -s 3.7 -f -F $imgdir/p2-d6-D-s3p7.png
./p2-deflation.pl -r 'D-D---D--D--D' -d 8 -s 8 -f -W 1000 -D 1000 -F $imgdir/p2-d6-D-s8.png
echo "
    group: Penrose P2 Tiling
    thumb-desc: Penrose P2 Tiling
    image-title: Penrose P2 Tiling
    image-detail: Penrose P2 Tiling - 8th deflation of recipie 'D-D---D--D--D'
" > $imgdir/p2-l-d6-K-s3p5.info

./p2-deflation.pl -r 'KK---K--K--K' -a none -d 6 -s 3.5 -F $imgdir/p2-l-d6-K-s3p5.png
echo "
    group: Penrose P2 Tiling
    thumb-desc: Penrose P2 Tiling
    image-title: Penrose P2 Tiling
    image-detail: Penrose P2 Tiling - 6th deflation of recipie 'KK---K--K--K'
" > $imgdir/p2-l-d6-K-s3p5.info

./p2-deflation.pl -r 'KK---K--K--K' -a none -d 8 -s 8 -W 1000 -D 1000 -F $imgdir/p2-l-d8-K-s8.png
echo "
    group: Penrose P2 Tiling
    thumb-desc: Penrose P2 Tiling
    image-title: Penrose P2 Tiling
    image-detail: Penrose P2 Tiling - 8th deflation of recipie 'KK---K--K--K'
" > $imgdir/p2-l-d8-K-s8.info

./p2-deflation.pl -r 'D-D---D--D--D' -a none -d 6 -s 3.7 -F $imgdir/p2-l-d6-D-s3p7.png
echo "
    group: Penrose P2 Tiling
    thumb-desc: Penrose P2 Tiling
    image-title: Penrose P2 Tiling
    image-detail: Penrose P2 Tiling - 6th deflation of recipie 'D-D---D--D--D
" > $imgdir/p2-l-d6-D-s3p7.info

./p2-deflation.pl -r 'D-D---D--D--D' -a none -d 8 -s 8 -W 1000 -D 1000 -F $imgdir/p2-l-d6-D-s8.png
echo "
    group: Penrose P2 Tiling
    thumb-desc: Penrose P2 Tiling
    image-title: Penrose P2 Tiling
    image-detail: Penrose P2 Tiling 8th deflation of recipie 'D-D---D--D--D
" > $imgdir/p2-l-d6-D-s8.info


./p2-deflation.pl -r 'KK---K--K--K' -d 3 -s 3.5 -F ../../image_files/penrose-tilings/p2-d3-K-A-s35.png
echo "
    group: Penrose P2 Tiling
    thumb-desc: Penrose P2 Tiling
    image-title: Penrose P2 Tiling
    image-detail: Penrose P2 Tiling - 3rd deflation of recipie 'KK---K--K--K'
" > $imgdir/p2-d4-K-A-s35.info

./p2-deflation.pl -r 'KK---K--K--K' -d 4 -s 3.5 -F ../../image_files/penrose-tilings/p2-d4-K-A-s35.png
echo "
    group: Penrose P2 Tiling
    thumb-desc: Penrose P2 Tiling
    image-title: Penrose P2 Tiling
    image-detail: Penrose P2 Tiling - 4th deflation of recipie 'KK---K--K--K'
" > $imgdir/p2-d4-K-A-s35.info

./p2-deflation.pl -r 'D-D---D--D--D' -d 3 -s 3.5 -F ../../image_files/penrose-tilings/p2-d3-D-A-s35.png
echo "
    group: Penrose P2 Tiling
    thumb-desc: Penrose P2 Tiling
    image-title: Penrose P2 Tiling
    image-detail: Penrose P2 Tiling - 3rd deflation of recipie 'D-D---D--D--D'
" > $imgdir/p2-d3-D-A-s35.info

./p2-deflation.pl -r 'D-D---D--D--D' -d 4 -s 3.5 -F ../../image_files/penrose-tilings/p2-d4-D-A-s35.png
echo "
    group: Penrose P2 Tiling
    thumb-desc: Penrose P2 Tiling
    image-title: Penrose P2 Tiling
    image-detail: Penrose P2 Tiling - 4th deflation of recipie 'D-D---D--D--D'
" > $imgdir/p2-d4-D-A-s35.info

./p2-deflation.pl -r 'KK---K--K--K' -d 5 -s 3.5 -A -F ../../image_files/penrose-tilings/p2-a-d5-K-A-s35.png
echo "
    group: Penrose P2 Tiling
    thumb-desc: Penrose P2 Tiling
    image-title: Penrose P2 Tiling
    image-detail: Penrose P2 Tiling - 5th deflation of recipie 'KK---K--K--K'
" > $imgdir/p2-a-d5-K-A-s35.info

./p2-deflation.pl -r 'D-D---D--D--D' -d 5 -s 3.5 -A -F ../../image_files/penrose-tilings/p2-a-d5-D-A-s35.png
echo "
    group: Penrose P2 Tiling
    thumb-desc: Penrose P2 Tiling
    image-title: Penrose P2 Tiling
    image-detail: Penrose P2 Tiling - 5th deflation of recipie 'D-D---D--D--D'
" > $imgdir/p2-a-d5-D-A-s35.info

################################################################################
# Penrose P3 Tilings (large and small rhombi)

./p3-deflation.pl -W 1000 -D 1000 -d 8 -s 18 -r 'SS--S-S--S-S----S-S--S-S' -f -F $imgdir/p3-xS-d8-s18a.png
echo "
    group: Penrose P3 Tiling
    thumb-desc: Penrose P3 Tiling
    image-title: Penrose P3 Tiling
    image-detail: Penrose P3 Tiling - 8th deflation of recipie 'SS--S-S--S-S----S-S--S-S'
" > $imgdir/p3-xS-d8-s18a.info

./p3-deflation.pl -W 1000 -D 1000 -d 10 -s 50 -r 'S' -f -m -F $imgdir/p3-S-d10-s50.png
echo "
    group: Penrose P3 Tiling
    thumb-desc: Penrose P3 Tiling
    image-title: Penrose P3 Tiling
    image-detail: Penrose P3 Tiling - 10th deflation of a small rhombus
" > $imgdir/p3-S-d10-s50.info

./p3-deflation.pl -W 1000 -D 1000 -d 10 -s 50 -r 'B' -f -m -F $imgdir/p3-B-d10-s50.png
echo "
    group: Penrose P3 Tiling
    thumb-desc: Penrose P3 Tiling
    image-title: Penrose P3 Tiling
    image-detail: Penrose P3 Tiling - 10th deflation of a big rhombus
" > $imgdir/p3-B-l-d10-s50.info

./p3-deflation.pl -W 1000 -D 1000 -a none -d 8 -s 18 -r 'SS--S-S--S-S----S-S--S-S' -F $imgdir/p3-xS-l-d8-s18a.png
echo "
    group: Penrose P3 Tiling
    thumb-desc: Penrose P3 Tiling
    image-title: Penrose P3 Tiling
    image-detail: Penrose P3 Tiling - 8th deflation of recipie 'SS--S-S--S-S----S-S--S-S'
" > $imgdir/p3-xS-l-d8-s18a.info

./p3-deflation.pl -W 1000 -D 1000 -a none -d 10 -s 50 -r 'S' -m -F $imgdir/p3-S-l-d10-s50.png
echo "
    group: Penrose P3 Tiling
    thumb-desc: Penrose P3 Tiling
    image-title: Penrose P3 Tiling
    image-detail: Penrose P3 Tiling - 10th deflation of a small rhombus
" > $imgdir/p3-B-l-d10-s50.info

./p3-deflation.pl -W 1000 -D 1000 -a none -d 10 -s 50 -r 'B' -m -F $imgdir/p3-B-l-d10-s50.png
echo "
    group: Penrose P3 Tiling
    thumb-desc: Penrose P3 Tiling
    image-title: Penrose P3 Tiling
    image-detail: Penrose P3 Tiling - 10th deflation of a big rhombus
" > $imgdir/p3-B-l-d10-s50.info

# p1 deflation example
./p1-tiling.pl -i 3 -s 8 -W 1000 -D 1000 -k clp1.ps -F clp.png
./p1-tiling.pl -i 2 -s 8 -S fill -W 1000 -D 1000 -k clp.ps -F clp.png
echo '0 0 1 setrgbcolor 2 setlinewidth' >> clp.ps
sed -n '22,$p' clp1.ps >> clp.ps
convert clp.ps $imgdir/p1-const-d3.png
rm clp.ps clp1.ps clp.png
echo "
    group: Penrose P1 Tiling
    thumb-desc: Penrose P1 Tiling
    image-title: Penrose P1 Tiling
    image-detail: Example deflation of a Penrose P1 Tiling
" > $imgdir/p1-const-d3.info

# p2 deflation example
./p2-deflation.pl -r 'KK---K--K--K' -d 3  -k clp.ps -s 3 -f -F clp.png
./p2-deflation.pl -r 'KK---K--K--K' -d 4  -k clp1.ps -s 3 -a none -F clp.png
echo '0 0 1 setrgbcolor 2 setlinewidth' >> clp.ps
sed -n '34,$p' clp1.ps | sed -e 's/1 1 0 setrgbcolor//' >> clp.ps
convert clp.ps $imgdir/p2-const-d4.png
rm clp.ps clp1.ps clp.png
echo "
    group: Penrose P2 Tiling
    thumb-desc: Penrose P2 Tiling
    image-title: Penrose P2 Tiling
    image-detail: Example deflation of a Penrose P2 Tiling
" > $imgdir/p2-const-d4.info

# p3 deflation example
./p3-deflation.pl  -d 5 -s 12 -r 'B' -f -m -k clp.ps -F clp.png
./p3-deflation.pl  -d 6 -s 12 -r 'B' -m -a none -k clp1.ps -F clp.png
echo '0 0 1 setrgbcolor 2 setlinewidth' >> clp.ps
sed -n '35,$p' clp1.ps | sed -e 's/1 1 0 setrgbcolor//' >> clp.ps
convert clp.ps $imgdir/p3-const-d6.png
rm clp.ps clp1.ps clp.png
echo "
    group: Penrose P3 Tiling
    thumb-desc: Penrose P3 Tiling
    image-title: Penrose P3 Tiling
    image-detail: Example deflation of a Penrose P3 Tiling
" > $imgdir/p3-const-d6.info
