#!/bin/sh

TILING_DIR=./tilings

# Usage: draw-all.sh <plugin> <radius>

if [ -z "$2" ]; then
    echo "Usage: draw-all.sh <plugin> <radius> [drawer opts]"
    exit -1
fi

plugin=$1
shift
tiling_dir="$TILING_DIR/$plugin"
if [ ! -d $tiling_dir ]; then
    echo "Could not find plugin dir at $tiling_dir"
    exit -1
fi

radius=$1
shift
tiling_dir="$tiling_dir/$radius"
if [ ! -d $tiling_dir ]; then
    echo "Could not find tilings dir at $tiling_dir"
    exit -1
fi

drawer="./${plugin}-draw.pl"
if [ ! -f $drawer ]; then
    echo "Could not find renderer at $drawer"
    exit -1
fi

for file in `ls -1 $tiling_dir`
do
    if [ "$file" != "index" ];
    then
        $drawer $* "$tiling_dir/$file" clp.pdf
        sleep 1
    fi
done
