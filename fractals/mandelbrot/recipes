#!/bin/bash

img_dir=../../image_files/fractals
mkdir -p $img_dir 2> /dev/null

writeInfoFile() {

    file=$1;shift
    title=$1;shift
    thumb=$1;shift
    opts="$*"

    opts=`echo "$opts" | perl -pe 's/--([^ ]*) //g'`
    opts=`echo "$opts" | perl -pe 's/-([A-Za-z]+) /\1: /g'`
    echo "
        group: $GROUP
        thumb-desc: $title
        image-title: $thumb
        image-detail: $thumb ($opts)
    " > $img_dir/${file}.info
}

processImage() {
    style=$1;shift
    dest=$1;shift
    fractal $* -f /tmp/frac.dat
    fractal-draw -c $style /tmp/frac.dat $img_dir/$dest.png
    rm /tmp/frac.dat
}

monoImage() {
    title=$1;shift
    thumb=$1;shift
    dest=$1;shift
    close=$1;shift

    processImage "mono -p1 $close" $dest $*
    writeInfoFile $dest "$title" "$thumb" $*
}

iterGrayImage() {
    title=$1;shift
    thumb=$1;shift
    dest=$1;shift
    draw_params=$1;shift

    processImage "iter-gray $draw_params" $dest $*
    writeInfoFile $dest "$title" "$thumb" $*
}

defaultImage() {
    title=$1;shift
    thumb=$1;shift
    dest=$1;shift

    processImage bronze $dest $*
    writeInfoFile $dest "$title" "$thumb" $*
}

################################################################################
# MANDELBROT SET

export GROUP="Mandelbrot Sets"

# FULL SET
# --------------
defaultImage "Full Set" "Full Mandelbrot Set" mandelbrot_set -x -0.5 -y 0 -s 1.15 -i 1000000 -e 1000000

# ELEPHANT VALLEY
# ---------------
defaultImage "Elephant Valley" "Elephant Valley" mandelbrot-elephantvalley-1 -x 0.311177 -y -0.026804 -s 0.000002 -i 1000000 -e 1000000

# SEAHORSE VALLEY
# ---------------
defaultImage "Seahorse Valley" "Seahorse Valley" mandelbrot-seahorse-valley-1 -x -0.74 -y .15 -s 0.01 -i 1000000 -e 1000000
defaultImage "Seahorse Valley" "Seahorse Valley" mandelbrot-seahorse-valley-2 -x -0.7802105 -y 0.133636 -s 0.00001 -i 1000000 -e 1000000
defaultImage "Seahorse Valley" "Seahorse Valley" mandelbrot-seahorse-valley-3 -x -0.74002395 -y .152305033 -s 0.00000015 -i 1000000 -e 1000000

# SCEPTRE VALLEY
# --------------
defaultImage "Sceptre Valley" "Sceptre Valley" mandelbrot-sceptre-valley-1 -x -1.37130225 -y 0.00970100 -s 0.00001000 -i 1000000 -e 1000000.000000

# NEEDLE
# ------
defaultImage "Needle" "Needle" mandelbrot-needle-1 -x -1.745932000 -y 0.003433000 -s 0.000001000 -i 1000000 -e 1000000

defaultImage "Needle" "Needle" mandelbrot-needle-2 -x -1.74975917367725004598 -y 0.0000006412991 -s 0.000000000002 -i 1000000 -e 1000000
defaultImage "Needle" "Needle" mandelbrot-needle-3 -x -1.94053419769609352958 -y 0.0000000074472235 -s 0.000000000000013 -i 1000000 -e 1000000

################################################################################
# MANDELBROT JOURNEYS

export GROUP="Mandelbrot Journeys"

defaultImage "Journey 1" "Journey 1" mandelbrot-journey1a -x -1.2542 -y 0.053 -s 0.001 -i 1000000 -e 1000000
defaultImage "Journey 1" "Journey 1" mandelbrot-journey1b -x -1.25 -y 0.0526 -s 0.005 -i 1000000 -e 1000000
defaultImage "Journey 1" "Journey 1" mandelbrot-journey1c -x -1.25399 -y 0.05291 -s 0.00007

defaultImage "Journey 2" "Journey 2" mandelbrot-journey2a -x -1.26 -y 0.0397 -s 0.004 -i 1000
defaultImage "Journey 2" "Journey 2" mandelbrot-journey2b -x -1.2614 -y 0.0398 -s 0.0002 -i 1000

defaultImage "Journey 3" "Journey 3" mandelbrot-journey3a -x -1.256 -y 0.381 -s 0.01
defaultImage "Journey 3" "Journey 3" mandelbrot-journey3b -x -1.2575875 -y 0.38197 -s 0.000035 -i 1000

defaultImage "Journey 4" "Journey 4" mandelbrot-journey4a -x -0.74575 -y .112813 -s 0.00004

################################################################################
# JULIA SETS

export GROUP="Julia Sets"

defaultImage "Julia Set" "Julia Set" julia-1 --julia -jx -0.7589 -jy 0.0753 -x 0 -y -0 -s 1.05 -i 100000 -e 100000
defaultImage "Julia Set" "Julia Set" julia-2 --julia -jx 0.311177 -jy -0.026804 -s 1 -x 0 -y -0 -s 1.05 -i 100000 -e 100000
defaultImage "Julia Set" "Julia Set" julia-3 --julia -jx -1.37130225 -jy 0.00970100 -s 1.3 -x 0 -y -0  -i 100000 -e 100000
defaultImage "Julia Set" "Julia Set" julia-3a --julia -jx -1.37130225 -jy 0.00970100 -s .22 -x 0 -y -0  -i 100000 -e 100000
defaultImage "Julia Set" "Julia Set" julia-4 --julia -jx -0.0534 -jy 0.6712 -s 1.2 -x 0 -y -0  -i 100000 -e 100000

iterGrayImage "Julia Set" "Julia Set" julia-5 "-p1 0.6" --julia -jx -0.1 -jy 0.651 -s 1.2 -x 0 -y -0  -i 100000 -e 100000
iterGrayImage "Julia Set" "Julia Set" julia-6 "-p1 0.6" --julia -jx -0.2 -jy -0.7 -s 1.2 -x 0 -y -0  -i 10000 -e 100000
iterGrayImage "Julia Set" "Julia Set" julia-7 "-p1 0.6" --julia -jx -1.125 -jy -0.21650635094611 -s 1.5 -x 0 -y -0  -i 100000 -e 100000

iterGrayImage "Julia Set" "Julia Set" julia-sin-1 "-p1 0.6" --julia-sin1 -s 6.282 -e 200 -i 200 -x 0 -y 0 -jx 1.0 -jy 0.25
iterGrayImage "Julia Set" "Julia Set" julia-sin-2 "-p1 0.4" --julia-sin1 -s 2.000 -e 700 -i 700 -x 0 -y 0 -jx .75 -jy .68 

iterGrayImage "Julia Set" "Julia Set" julia-cos-1 "-p1 0.6" --julia-cos1 -s 6.282 -e 300 -i 300 -x 0 -y 0 -jx 1.275 -jy 0.35
iterGrayImage "Julia Set" "Julia Set" julia-cos-2 "-p1 0.6" --julia-cos1 -s 1.000 -e 300 -i 300 -x 0 -y 0 -jx 1.275 -jy 0.35
