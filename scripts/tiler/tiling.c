#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <sys/stat.h>

#include <math.h>
#include <dlfcn.h>

#include "tiling.h"
#include "tiling-utils.h"
#include "tiling-plugin.h"
#include "tiling-geometry.h"
#include "tiling-polygons.h"


#define _DEFAULT_TILING_DIR     "./tilings"
#define _DEFAULT_TILING_FORMAT  "json"

typedef enum {
    REWIND_TO_LAST_CHANGEABLE,
    REWIND_TO_LAST_INSIDE
} _rewind_type;

/*******************************************************************************
 *
 * tiling.c
 *
 *  Generate tilings
 *
 ******************************************************************************/

_details    *details = NULL;

/*************************************************
 * Display the runline options
 ************************************************/

void usage(
    char *name
) {

    printf( "Usage:\n" );
    printf( "\t%s: [-h] [-i] [-I] [-l <log file>] [-q] [-R <recipe>] [-x <rad>] <plugin>\n\n", name );
    printf( "\twhere\n" );
    printf( "\t\t-h             this help message\n" );
    printf( "\t\t-i             interactive level low\n" );
    printf( "\t\t-I             interactive level high\n" );
    printf( "\t\t-l <log file>  log messages to a file\n" );
    printf( "\t\t-q             no log messages\n" );
    printf( "\t\t-R <recipe>    initial recipe to use\n" );
    printf( "\t\t-x <rad>       exclusion radius\n" );

    exit( 0 );
}

/*************************************************
 * Initialise the details structure
 * return !=0 on error
 ************************************************/

void processParameters(
    int     argc,
    char    **argv
) {
    int pnt = 1;

    while ( pnt < ( argc - 1 ) ) {
    
        if ( *argv[ pnt ] != '-' )
            logFatal( "Unknown switch %s", argv[ pnt ] );

        switch( argv[ pnt ][ 1 ] ) {

            case 'd' :
                pnt++;
                if ( pnt < argc ) {
                    details->tiling_dir = argv[ pnt ];
                }
                else {
                    logFatal( "Missing the tiling directory" );
                }
                break;

            case 'h' :
                usage( argv[ 0 ] );
                break;

            case 'f' :
                pnt++;
                if ( pnt < argc ) {
                    details->tiling_format = argv[ pnt ];
                }
                else {
                    logFatal( "Missing the tiling format" );
                }
                break;

            case 'i' :
                details->interactive_level = INTERACTIVE_LEVEL_LOW;
                break;

            case 'I' :
                details->interactive_level = INTERACTIVE_LEVEL_HI;
                break;

            case 'l' :
                pnt++;
                if ( pnt < argc ) {
                    setLogFile( argv[ pnt ] );
                }
                else {
                    logFatal( "Missing the log file name" );
                }
                break;

            case 'q' :
                setLoggingLevel( LOGGING_LEVEL_QUIET );
                break;

            case 'R' :
                pnt++;
                if ( pnt < argc ) {
                    details->initial_recipe = argv[ pnt ];
                }
                else {
                    logFatal( "Missing the initial recipe" );
                }
                break;

            case 'x' :
                pnt++;
                if ( pnt < argc ) {
                    details->exclusion_radius = atof( argv[ pnt ] );
                    if ( details->exclusion_radius == 0 )
                        logFatal( "Invalid exclusion radius value" );
                }
                else {
                    logFatal( "Missing the radius value" );
                }
                break;

            default:
                logFatal( "Unknown switch %s", argv[ pnt ] );
        }

        pnt++;
    }

    if ( ( pnt == argc ) || ( *argv[ pnt ] == '-' ) ) {
        logMsg( "Missing plugin name" );
        usage( argv[ 0 ] );
    }

    details->plugin = argv[ pnt ];
    loadPlugin( argv[ pnt ], getLoggingLevel() );

    // Verify supplied info

    if ( details->exclusion_radius == 0 ) {
        logMsg( "Defaulting exclusion radius to 5" );
        details->exclusion_radius = 5;
    }
}

/*************************************************
 * Return a polygon from one of the lists
 ************************************************/

_polygon *getPolygonByPos(
    short pos
) {
    return getFromListByPos( pos, &details->polygons );
}

/*************************************************
 * Initialise the details structure
 ************************************************/

void initialiseDetails() {

    if ( ( details = ALLOC( _details ) ) == NULL )
        logFatal( "Failed to calloc() the details structure" );

    details->tiling_dir = _DEFAULT_TILING_DIR;
    details->tiling_format = _DEFAULT_TILING_FORMAT;

    details->interactive_level = INTERACTIVE_LEVEL_OFF;

    details->recipe_list = initialiseList( free );

    details->command = "none";
}

/*************************************************
 * Rotate a polygon
 ************************************************/

void rotatePolygon(
    _polygon    *polygon,
    double      rotation
) {
    _polygon_vertex *vertex;
    double          ang;
    double          dist;
    short           v;

    for( v = 0; v < polygon->edges; v++ ) {

        vertex = polygon->vertices[ v ];
        ang = atan2( vertex->y, vertex->x );
        dist = sqrt( vertex->x * vertex->x + vertex->y * vertex->y );

        ang += rotation;

        vertex->x = dist * cos( ang );
        vertex->y = dist * sin( ang );
    }
}

/*************************************************
 * Add the polygon to one of the lists
 ************************************************/

void addToPolygonList(
    _polygon    *polygon
) {
    details->iteration++;
    logMsg( "Iteration: %d added %lx to list",
                    details->iteration, (long)polygon );

    addToList( polygon, &details->polygons );

    if ( details->iteration == details->pause_at ) {
        details->command = "none";
        showWorking( INTERACTIVE_LEVEL_LOW, "Pausing at iteration" );
    }
}

/*************************************************
 * Return the char id for the polygon type
 ************************************************/

char polygonSideStatusChar(
    _polygon    *polygon,
    short       s
) {
    switch( polygon->sides[ s ]->status ) {

        case SIDE_STATUS_READY:
            return 'R';

        case SIDE_STATUS_DONE:
            return 'D';
    }

    return '?';
}

/*************************************************
 * Return the vertex status
 ************************************************/

char vertexStatus (
    _polygon_vertex *vertex
) {
    switch( vertex->status ) {

        case VERTEX_STATUS_DONE:
            return 'D';

        case VERTEX_STATUS_READY:
            return 'R';
    }

    return '?';
}

/*************************************************
 * Return the polygon status
 ************************************************/

char polygonStatus (
    _polygon *polygon
) {
    switch( polygon->status ) {

        case POLYGON_STATUS_DONE:
            return 'D';

        case POLYGON_STATUS_OUTSIDE:
            return 'O';

        case POLYGON_STATUS_READY:
            return 'R';
    }

    return '?';
}

/*************************************************
 * Dump the data and build an image
 ************************************************/

void showWorking (
    _interactive_level  interactive_level,
    char                *format,
    ...
) {
    va_list ap;
    char    buf[100];
    int     i;

    dumpJsonData( "/tmp/clp.json", DUMP_JSON_ALL_POLYGONS );

    if ( interactive_level > details->interactive_level )
        return;

    if ( *details->command == 'c' )
        return;

    if ( details->polygons.count > 0 )
        drawTiling( "/tmp/clp.json", details->exclusion_radius, "clp.pdf" );

    details->command = "none";

    va_start( ap, format );
    vprintf( format, ap );
    logTxt( "> " );
    va_end( ap );

    while ( 1 ) {

        *buf = '\0';

        fgets(buf, 10, stdin);

        if ( *buf == 10 ) {
            printf( "\n" );
            return;
        }

        if ( *buf == '\0' ) {
            printf( "\n" );
            exit( -1 );
        }

        if ( *buf == 'q' )
            exit( -1 );

        if ( *buf == 'c' ) {
            details->command = "continue";
            return;
        }

        if ( *buf == 'i' ) {
            details->interactive_level = INTERACTIVE_LEVEL_LOW;
            return;
        }

        if ( *buf == 'I' ) {
            details->interactive_level = INTERACTIVE_LEVEL_HI;
            return;
        }

        if ( ( *buf == 'd' ) || ( *buf == 'D' ) )
            dumpAllData( NULL );

        if ( ( *buf == 'j' ) || ( *buf == 'J' ) )
            dumpJsonData( NULL, DUMP_JSON_ALL_POLYGONS );

        if ( ( i = atoi( buf ) ) > 0 )
            details->pause_at = i;
            
        printf( "> " );
    }
}

/*************************************************
 * Dump the data for all polygons
 ************************************************/

void dumpJsonData(
    char            *json_path,
    _dump_json_opt  option
) {

    short           pos;
    short           v;
    _polygon        *polygon;
    _list           *vertex_list;
    _polygon_vertex *vertex;
    FILE            *fh;

    vertex_list = initialiseList( NULL );

    if ( json_path != NULL ) {

        if ( ( fh = fopen( json_path, "w" ) ) == NULL )
            logFatal( "Could not open %s", json_path );
    }
    else {

        fh = ( details->log_fp == NULL ) ? stdout : details->log_fp;
    }

    fprintf( fh, "{\n    \"POLYGONS\":[\n" );
    for( pos = 0; pos < details->polygons.count; pos++ ) {

        polygon = getPolygonByPos( pos );

        if (
            ( option == DUMP_JSON_ONLY_INSIDE_POLYGONS ) &&
            ( polygon->status == POLYGON_STATUS_OUTSIDE )
        )
            continue;

        fprintf( fh, "%s        {\"ID\":\"%lx\",\"TYPE\":\"%c\",\"VERTICES\":[",
                                        (pos > 0 ) ? ",\n" : "",
                                        (long)polygon,
                                        polygonTypeChar( polygon ) );

        for( v = 0; v < polygon->edges; v++ ) {

            fprintf( fh, "%s\"%lx\"", 
                        ( v > 0 ) ? "," : "",
                        (long)polygon->vertices[ v ] );

            if ( getFromListByData( polygon->vertices[ v ], vertex_list ) == NULL )
                addToList( polygon->vertices[ v ], vertex_list );
        }

        fprintf( fh, "]}" );
    }
    fprintf( fh, "\n    ],\n" );

    fprintf( fh, "    \"POLYGON_TYPES\":{\n" );
    for( pos = 0; pos < getTemplateCount(); pos++ ) {

        polygon = getTemplateByPos( pos );
        fprintf( fh, "%s        \"%c\":{\"SIDES\":[", 
                                        (pos > 0 ) ? ",\n" : "",
                                        polygonTypeChar( polygon ) );
        for( v = 0; v < polygon->edges; v++ ) {

            fprintf( fh, "%s%d", (v > 0 ) ? "," : "", polygon->sides[ v ]->type );
        }
        fprintf( fh, "]}" );
    }
    fprintf( fh, "\n    },\n" );

    fprintf( fh, "    \"SIDE_TYPES\":{\n");
    for( pos = 0; pos < getSideTypeCount(); pos++ ) {

        fprintf( fh, "%s        \"%d\":\"%s\"",
                                        (pos > 0 ) ? ",\n" : "",
                                        pos, getSideTypeDescription( pos ) );
    }

    fprintf( fh, "\n    },\n" );

    fprintf( fh, "    \"VERTICES\":{\n");
    for( pos = 0; pos < vertex_list->count; pos++ ) {

        vertex = getFromListByPos( pos, vertex_list );

        fprintf( fh, "%s        \"%lx\":{\"X\":%f,\"Y\":%f,\"ANG\":%d,\"POLYGONS\":[",
                                        (pos > 0 ) ? ",\n" : "",
                                        (long)vertex, vertex->x, vertex->y,
                                        (int)RAD2DEG( vertex->angle_used )
        );
        for( v = 0; v < vertex->polygons->count; v++ ) 
            fprintf( fh, "%s\"%lx\"",
                    (v > 0 ) ? "," : "", (long)getFromListByPos( v, vertex->polygons ) );

        fprintf( fh, "]}" );
    }

    fprintf( fh, "\n    }\n}\n" );

    releaseList( vertex_list );

    if ( json_path != NULL )
        fclose( fh );
}

/*************************************************
 * Display data for a single polygon
 ************************************************/

void dumpPolygonData(
    _polygon    *polygon
) {

    _polygon_vertex *vertex;
    char            *sep;
    short           v;
    _list_item      *item;

    logTxt( "Polygon %0lx (%c) %c ", (long)polygon,
                        polygonTypeChar( polygon ), polygonStatus( polygon ) );

    logTxt( "(" );
    for( v = 0; v < polygon->edges; v++ ) {
        logTxt( "%c", polygonSideStatusChar( polygon, v ) );
    }
    logMsg( ")" );
                        
    for( v = 0; v < polygon->edges; v++ ) {

        vertex = polygon->vertices[ v ];
        logTxt( "  vtx %d (%lx) %c - x:% 9f y:% 9f a:%d", v,
                                (long)vertex,
                                vertexStatus( vertex ),
                                vertex->x, vertex->y, 
                                (int)(180 * vertex->angle_used / PI) );

        sep = " l:";
        for( item = vertex->polygons->first; item != NULL; item = item->next ) {
            logTxt( "%s%lx", sep, (long)item->data );
            sep = ", ";
        }
        logMsg( "" );
    }
}

/*************************************************
 * Display all polygon data
 ************************************************/

void dumpAllData() {

    _list_item  *item;

    for( item = details->polygons.first; item != NULL; item = item->next )
        dumpPolygonData( item->data );
}

/*************************************************
 * Dump the poygon list data
 ************************************************/

void dumpListData() {

    int         p = 1;
    _list_item  *item;

    logMsg( "Polygons list - first %0lx, last %0lx",
                        (long)details->polygons.first,
                        (long)details->polygons.last );

    for( item = details->polygons.first; item != NULL; item = item->next ) {

        logMsg( "%03d: %09lx prev %09lx, next %09lx, data %09lx",
                                        p++,
                                        (long)item,
                                        (long)item->prev,
                                        (long)item->next,
                                        (long)item->data ); 
    }
}

/*************************************************
 * Return the current recipe - caller to free
 ************************************************/

char *getRecipeString()
{
    char        *recipe;
    _polygon    *polygon;
    _list_item  *item;
    int         buf_size;
    int         pos = 0;

    buf_size = 2 * details->polygons.count + 1;

    if ( ( recipe = calloc( buf_size, sizeof(char) ) ) == NULL ) 
        logFatal( "Could not calloc() for the recipe string" );

    for( item = details->polygons.first; item != NULL; item = item->next ) {

        polygon = item->data;
        recipe[ pos ] = polygonTypeChar( polygon );

        recipe[ pos+1 ] = ( pos == 0 ) ? '-' : '0' + polygon->anchor_edge;
            
        pos += 2;
    }

    return recipe;
}

/*************************************************
 * Remove the last item in the polygons list
 ************************************************/

void removeLastPolygon() {

    _list_item  *item;

    if ( details->polygons.count == 0 )
        return;

    details->polygons.count--;

    item = details->polygons.last;

    if ( details->polygons.count == 0 ) {

        details->polygons.first = NULL;
        details->polygons.last = NULL;
    }
    else {

        details->polygons.last = item->prev;
        details->polygons.last->next = NULL;
    }

    free( item );
}

/*************************************************
 * Return the last polygon the list
 ************************************************/

_polygon *getLastPolygon() {

    _list_item  *item;

    item = details->polygons.last;
    return ( item == NULL ) ? item : item->data;
}

/*************************************************
 * Commit any common vertices from the tiling
 ************************************************/

void commitCommonVertices(
    _list       *common_vertices
) {

    _list_item      *item;
    _common_vertex  *common;
    _polygon_vertex *vertex;
    short           vertex_no;

    for( item = common_vertices->first; item != NULL; item = item->next ) {

        common = item->data;

        logMsg( "sharing existing vtx %d of %lx with %d of %lx",
                            common->vertex_no, (long)common->polygon,
                            common->new_vertex_no, (long)common->new_polygon
        );

        common->polygon->vertices[ common->vertex_no ]->angle_used +=
                common->new_polygon->vertices[ common->new_vertex_no ]->angle_used;

        vertex = common->new_polygon->vertices[ common->new_vertex_no ];
        releaseList( vertex->polygons );
        free( vertex );

        common->new_polygon->vertices[ common->new_vertex_no ] = 
                                    common->polygon->vertices[ common->vertex_no ];

        addToList( common->new_polygon, common->polygon->vertices[ common->vertex_no ]->polygons );
        logMsg( "adding %lx to last polygon list of %lx/%d",
                    (long)common->new_polygon, (long)common->polygon, common->vertex_no);

        vertex_no = 
                ( common->side_done == 'Y' ) ? common->vertex_no :
                ( common->side_done == 'P' ) ? PREV_VTX( common->vertex_no, common->polygon ) :
                                               -1;

        if ( vertex_no > -1 ) {
            common->polygon->sides[ vertex_no ]->status = SIDE_STATUS_DONE;
            logMsg( "Side now done: %lx/%d", (long)common->polygon, vertex_no );
        }

        vertex_no = 
                ( common->new_side_done == 'Y' ) ? common->new_vertex_no :
                ( common->new_side_done == 'P' ) ? PREV_VTX( common->new_vertex_no, common->new_polygon ) :
                                                   -1;

        if ( vertex_no > -1 ) {
            common->new_polygon->sides[ vertex_no ]->status = SIDE_STATUS_DONE;
            logMsg( "Side now done: %lx/%d", (long)common->new_polygon, vertex_no );
        }

        checkForCompletedPolygon( common->new_polygon );
    }
}

/*************************************************
 * Add a common vertex to the list
 ************************************************/

void addCommonVertex(
    _list       *list,
    _polygon    *polygon,      // the existing polygon
    short       vertex_no,
    char        side_done,
    _polygon    *new_polygon,  // the new polygon
    short       new_vertex_no,
    char        new_side_done
) {
    _common_vertex  *common_vertex;

    if ( ( common_vertex = ALLOC( _common_vertex ) ) == NULL )
        logFatal( "Could not calloc common_vertex" );

    common_vertex->polygon = polygon;
    common_vertex->vertex_no = vertex_no;
    common_vertex->side_done = side_done;
    common_vertex->new_polygon = new_polygon;
    common_vertex->new_vertex_no = new_vertex_no;
    common_vertex->new_side_done = new_side_done;

    addToList( common_vertex, list );
}

/*************************************************
 * Find any common vertices from the tiling
 ************************************************/

short findCommonVertices(
    _polygon    *polygon1,      // the existing polygon
    short       edge1,
    _polygon    *polygon2,      // the new polygon
    short       edge2,
    _list       *common_vertices
) {
    _polygon        *last_polygon;
    short           last_vertex;
    short           check_vertex;
    double          angle1;
    double          angle2;
    short           next_edge1;
    short           next_edge2;
    short           prev_edge2;

    next_edge1 = NEXT_VTX( edge1, polygon1 );
    next_edge2 = NEXT_VTX( edge2, polygon2 );
    prev_edge2 = PREV_VTX( edge2, polygon2 );

    angle1 = polygon1->vertices[ edge1 ]->angle_used +
                            polygon2->vertices[ next_edge2 ]->angle_used;

    angle2 = polygon1->vertices[ next_edge1 ]->angle_used +
                                                polygon2->vertices[ edge2 ]->angle_used;

    addCommonVertex( common_vertices, polygon1, edge1, '-', polygon2, next_edge2, '-' );
    addCommonVertex( common_vertices, polygon1, next_edge1, '-', polygon2, edge2, '-' );

    if( RAD2DEG( angle1 ) == 360 ) {

        logMsg( "vertex %lx/%d is full", (long)polygon2, next_edge2 );

        last_polygon = polygon1->vertices[ edge1 ]->polygons->last->data;
        if ( last_polygon == polygon1 )
            last_polygon = polygon1->vertices[ edge1 ]->polygons->last->prev->data;
        logMsg( "last polygon was %lx", (long)last_polygon );

        last_vertex = findVertexNumber( last_polygon, polygon1->vertices[ edge1 ] );
        logMsg( "last vertex of %lx was %d", (long)last_polygon, last_vertex );

        check_vertex = PREV_VTX( last_vertex, last_polygon );
        if ( checkForValidSides( polygon2, next_edge2, last_polygon, check_vertex ) == NOK )
            return NOK;

        addCommonVertex( common_vertices,
                                last_polygon, check_vertex, 'Y',
                                polygon2, NEXT_VTX( next_edge2, polygon2 ), 'P'
        );
    }

    if( RAD2DEG( angle2 ) == 360 ) {

        logMsg( "vertex %lx/%d is full", (long)polygon1, next_edge1 );

        last_polygon = polygon1->vertices[ next_edge1 ]->polygons->last->data;
        if ( last_polygon == polygon1 )
            last_polygon = polygon1->vertices[ next_edge1 ]->polygons->last->prev->data;
        logMsg( "last polygon was %lx", (long)last_polygon );

        last_vertex = findVertexNumber( last_polygon, polygon1->vertices[ next_edge1 ] );
        logMsg( "last vertex of %lx was %d", (long)last_polygon, last_vertex );

        check_vertex = NEXT_VTX( last_vertex, last_polygon );
        if ( checkForValidSides( polygon2, prev_edge2, last_polygon, last_vertex ) == NOK )
            return NOK;
    
        addCommonVertex( common_vertices,
                                last_polygon, check_vertex, 'P',
                                polygon2, prev_edge2, 'Y'
        );
    }

    return OK;
}

/*************************************************
 * Test if the polygons are allowed to join
 ************************************************/

short checkForValidTiling(
    _polygon    *polygon1,      // the existing polygon
    short       edge1,
    _polygon    *polygon2,      // the candidate (template) polygon
    short       edge2
) {
    double      vertex_angle;

    logMsg( "trying polygon %lx (%c) side %d on %lx (%c) side %d",
                (long)polygon2, polygonTypeChar(polygon2), edge2,
                (long)polygon1, polygonTypeChar(polygon1), edge1
    );

    // Check the angles at the vertices
    vertex_angle = polygon1->vertices[ edge1 ]->angle_used;   
    vertex_angle += polygon2->vertices[ NEXT_VTX( edge2, polygon2 ) ]->angle_used;   
    if ( vertex_angle > ( 2 * PI ) ) {
        logMsg( "rejected - overblown angle (vtx %d)", edge1 );
        return NOK;
    }

    vertex_angle = polygon1->vertices[ NEXT_VTX( edge1, polygon1 ) ]->angle_used;   
    vertex_angle += polygon2->vertices[ edge2 ]->angle_used;   
    if ( vertex_angle > ( 2 * PI ) ) {
        logMsg( "rejected - overblown angle (vtx %d)", NEXT_VTX( edge1, polygon1 ) );
        return NOK;
    }

    return checkForValidSides( polygon1, edge1, polygon2, edge2 );
}

/*************************************************
 * Sum the internal angles up to a vertex
 ************************************************/

double sumInternalAngles(
    int             type,
    short           vertex
) {
    _polygon    *template;
    double      sum = 0;    

    template = getTemplateByType( type );

    while ( vertex > 0 ) {
        sum += template->angles[ vertex ];
        vertex--;
    }

    return sum;
}

/*************************************************
 * Is the polygon outside the exclusion radius
 ************************************************/

void checkExclusionStatus(
    _polygon    *polygon
) {
    short           v;
    _polygon_vertex *vertex;

    for( v = 0; v < polygonEdges( polygon ); v++ ) {

        vertex = polygon->vertices[ v ];

        if ( DIST( vertex->x, vertex->y ) < details->exclusion_radius )
            return;
    }

    polygon->status = POLYGON_STATUS_OUTSIDE;
}

/*************************************************
 * Check if all sides are complete
 ************************************************/

void checkForCompletedPolygon(
    _polygon    *polygon
) {
    int edge;

    for ( edge = 0; edge < polygonEdges( polygon ); edge++ ) {

        if ( polygon->sides[ edge ]->status != SIDE_STATUS_DONE )
            return;
    }

    polygon->status = POLYGON_STATUS_DONE;
}

/*************************************************
 * Add the given polygon to the given side
 ************************************************/

void addPolygonToSide(
    _polygon        *polygon, 
    short           edge, 
    _polygon        *trial_polygon,
    short           trial_edge,
    _list           *common_vertices
) {
    short           next_vertex;
    double          rotate;
    double          base_angle;

    trial_polygon->anchor_edge = trial_edge;

    next_vertex = NEXT_VTX( edge, polygon );
    base_angle = atan2 (
        polygon->vertices[ next_vertex ]->y - polygon->vertices[ edge ]->y,
        polygon->vertices[ next_vertex ]->x - polygon->vertices[ edge ]->x
    );

    rotate = PI
            + sumInternalAngles( trial_polygon->type, trial_edge )
            - ( trial_edge * PI )
            + base_angle
            - trial_polygon->base_angle;
             
    logMsg( "Rotating poygon %lx by %d", (long)polygon, RAD2DEG( rotate ) );

    rotatePolygon( trial_polygon, rotate );

    offsetPolygon( trial_polygon,
                    polygon->vertices[ next_vertex ]->x - trial_polygon->vertices[ trial_edge ]->x,
                    polygon->vertices[ next_vertex ]->y - trial_polygon->vertices[ trial_edge ]->y
    );

    commitCommonVertices( common_vertices );

    checkExclusionStatus( trial_polygon );

    addToPolygonList( trial_polygon );

    polygon->sides[ edge ]->status = SIDE_STATUS_DONE;
    trial_polygon->sides[ trial_edge ]->status = SIDE_STATUS_DONE;

    checkForCompletedPolygon( polygon );
}

/*************************************************
 * Process a polygon side
 ************************************************/

short processPolygonSide(
    _polygon    *polygon,
    short       edge,
    short       initial_trial_polygon,
    short       initial_trial_edge
) {
    _polygon    *trial_template;
    _polygon    *trial_polygon;
    short       trial_edge;
    short       pos;
    _list       *common_vertices;

    common_vertices = initialiseList( free );

    // try each of the available templates
    for( pos = initial_trial_polygon; pos < getTemplateCount(); pos++ ) {

        trial_template = getTemplateByPos( pos );
        trial_polygon = newPolygon( trial_template->type );
        logMsg( "Trial polygon type %d/%c", initial_trial_polygon, polygonTypeChar( trial_polygon ) );

        // and each of the sides for the trial template
        for( trial_edge = initial_trial_edge; trial_edge < trial_polygon->edges; trial_edge++ ) {

            emptyList( common_vertices );

            // Check for the immediate tiling (on the contact side vertices)
            if ( checkForValidTiling( polygon, edge, trial_polygon, trial_edge ) == OK ) {

                // Validate any additional completed vertices
                if ( findCommonVertices( polygon, edge, trial_polygon, trial_edge, common_vertices ) == NOK ) {
                    emptyList( common_vertices );
                    continue;
                }

                addPolygonToSide( polygon, edge, trial_polygon, trial_edge, common_vertices );
                logMsg( "side done for %lx/%d", (long)polygon, edge );

                showWorking( INTERACTIVE_LEVEL_HI, "added polygon to list" );

                releaseList( common_vertices );

                return OK;
            }
        }

        releasePolygon( trial_polygon );

        // try one of the other template types then...
        initial_trial_edge = 0;
    }

    releaseList( common_vertices );

    return NOK;
}

/*************************************************
 * Find the polygon side number
 ************************************************/

short findSideNumberOnPolygon( 

    _polygon        *polygon,
    _polygon_side   *side
) {
    short s;

    for( s = 0; s < polygon->edges; s++ ) {
        if ( side == polygon->sides[ s ] )
            return s;
    }

    return -1;
}

/*************************************************
 * Find a different setup with existing polygons
 ************************************************/

short rewindPolygonList(
    _rewind_type    rewind_type
) {

    _polygon        *trial_polygon;
    char            trial_type;
    short           trial_edge;
    short           trial_type_pos;
    _polygon        *anchor_polygon;
    short           anchor_vertex_no;
    short           next_vertex_no;
    _polygon_vertex *trial_vertex;
    char            buf[100];

    logMsg( "======================================================" );
    logMsg( "Begining rewind" );
    showWorking( INTERACTIVE_LEVEL_LOW, "");

    
    // Remove all trailing polygons that are outside - this should only
    // be done after a completed tiling has been written away - otherwise
    // the currnet inside arrangement will be disacarded despite the chance
    // to complete the tiling by changing other outside polygons
    if ( rewind_type == REWIND_TO_LAST_INSIDE ) {
    
        while (
            ( ( trial_polygon = getLastPolygon() ) != NULL ) &&
            ( trial_polygon->status == POLYGON_STATUS_OUTSIDE ) 
        ) {
    
            releasePolygon( trial_polygon );
            removeLastPolygon();
        }
    }

    // Try a different edge on the last polygon
    while ( ( trial_polygon = getLastPolygon() ) != NULL ) {

        trial_edge = trial_polygon->anchor_edge;

        if ( trial_edge < 0 ) {
            logMsg( "really hope this is the first polygon..." );
            releasePolygon( trial_polygon );
            removeLastPolygon();
            return OK;
        }

        logMsg( "Last polygon was %lx anchored on edge %d",
                                    (long)trial_polygon, trial_edge );
        trial_type = polygonTypeChar( trial_polygon );
        trial_type_pos = trial_polygon->type;
        trial_vertex = trial_polygon->vertices[ trial_edge ];

        findCommonSide( trial_polygon, trial_edge, &anchor_polygon );

        anchor_vertex_no = PREV_VTX(
                findVertexNumber( anchor_polygon, trial_vertex ),
                anchor_polygon
        );
        logMsg( "Anchor polygon was %lx edge %d",
                                (long)anchor_polygon, anchor_vertex_no );

        next_vertex_no = NEXT_VTX( trial_edge, trial_polygon );

        sprintf(buf, "Removing polygon %lx\n", (long)trial_polygon);

        releasePolygon( trial_polygon );
        removeLastPolygon();

        showWorking( INTERACTIVE_LEVEL_HI, buf );

        // If this was the last edge of the trial_polygon - try
        // the next polygon (if any)
        if ( next_vertex_no == 0 ) {
            trial_type_pos++;
            trial_polygon = getTemplateByPos( trial_type_pos );
            if ( trial_polygon ) {
                trial_type = polygonTypeChar( trial_polygon );
                logMsg( "Exhausted vertices on type trying next type (%d/%c)", trial_type_pos, trial_type );
            }
            else {

                logMsg( "Exhausted vertices on last trial type" );
            }
        }

        if ( trial_type_pos < getTemplateCount() ) {

            logMsg( "Trying %c/%d on %lx/%d",
                                    trial_type, next_vertex_no,
                                    anchor_polygon, anchor_vertex_no
            );

            if ( processPolygonSide( anchor_polygon, anchor_vertex_no,
                        trial_type_pos, next_vertex_no ) == OK ) {
                showWorking( INTERACTIVE_LEVEL_HI, "Rewind complete - polygon successful" );
                return OK;
            }
        }
    }

    // Could not find a polygon that could be changed
    logMsg( "Rewind complete - no polygon could be changed" );

    return NOK;
}

/*************************************************
 * Process a polygon
 ************************************************/

short processPolygon(
    _polygon    *polygon
) {

    short           retval;
    short           edge;
    _polygon_side   *side;

    for( edge = 0; edge < polygon->edges; edge++ ) {

        side = polygon->sides[ edge ];
        if ( side->status == SIDE_STATUS_DONE )
            continue;

        retval = processPolygonSide( polygon, edge, 0, 0 );

        if ( retval == NOK ) {

            // So we could not place any kind of polygon on this
            // side - then the current set of polygons is not a
            // valid coverage - so move back through the polygons
            // trying different edges until a new (valid) setup
            // is found

            showWorking( INTERACTIVE_LEVEL_HI,
                            "Failed to process polygon %lx", (long)polygon );

            return rewindPolygonList( REWIND_TO_LAST_CHANGEABLE );
        }
    }

    polygon->status = POLYGON_STATUS_DONE;

    return OK;
}

/*************************************************
 * Fond the nex polygon for processing
 ************************************************/

_polygon *findNextReadyPolygon() {

    _polygon    *polygon;
    _list_item  *item;

    for( item = details->polygons.first; item != NULL; item = item->next ) {

        polygon = item->data;

        if ( polygon->status == POLYGON_STATUS_READY ) 
            return polygon;
    }

    return NULL;
}

/*************************************************
 * Process any outstanding polygons
 ************************************************/

void processPolygons() {

    _polygon    *polygon;
    short       all_done = 0;

    while ( ! all_done ) {

        all_done = 1;

        if ( ( polygon = findNextReadyPolygon() ) != NULL ) {

            logMsg( "Processing polygon at %lx", polygon );
        
            all_done = 0;

            if ( processPolygon( polygon ) == NOK )
                break;
        }
    }
}

/*************************************************
 * Cleanup
 ************************************************/

void cleanup() {

    releasePlugin();
    releaseList( details->recipe_list );

    cleanupLogging();

    free( details );
}

/*************************************************
 * Save the data to the tiling dir
 ************************************************/

void saveTilingData() {

    char        path[ 256 ];
    char        *recipe;
    FILE        *fp;
    _polygon    *polygon;
    short       pos;
    char        *pnt;

    static char dir[ 128 ];
    static int  tiling_id = 0;

    recipe = getRecipeString();
    logMsg( "Tiling recipe: %s", recipe );

    // Find the recipe string for the polygons that are to be 
    // saved - if outside polygons are omitted this will differ
    // from the string returned by getRecipeString

    // reuse the recipe string - it must be long enough already
    *recipe = '\0';
    pnt = recipe;
    for( pos = 0; pos < details->polygons.count; pos++ ) {

        polygon = getPolygonByPos( pos );
        if ( polygon->status != POLYGON_STATUS_OUTSIDE ) {
            *pnt = polygonTypeChar( polygon );
            *(pnt + 1) = ( pnt == recipe ) ? '-' : '0' + polygon->anchor_edge;
            pnt += 2;
            *pnt = '\0';
        }
    }

    // Make sure that there was at least 1 polygon not rejected
    if ( *recipe == '\0' ) {
        free( recipe );
        return;
    }

    logMsg( "Saving recipe: %s", recipe );

    if ( findStringInList( recipe, details->recipe_list ) > -1 ) {
        free( recipe );
        return;
    }

    addToList( recipe, details->recipe_list );   

    if ( tiling_id == 0 ) {

        sprintf( dir, "%s", details->tiling_dir );
        mkdir( dir, S_IRUSR | S_IWUSR | S_IXUSR | S_IRGRP | S_IWGRP | S_IXGRP );

        sprintf( dir, "%s/%s", details->tiling_dir, details->plugin );
        mkdir( dir, S_IRUSR | S_IWUSR | S_IXUSR | S_IRGRP | S_IWGRP | S_IXGRP );

        sprintf( dir, "%s/%s/%0.4f",
                details->tiling_dir, details->plugin, details->exclusion_radius );
        mkdir( dir, S_IRUSR | S_IWUSR | S_IXUSR | S_IRGRP | S_IWGRP | S_IXGRP );
    }

    tiling_id++;

    sprintf( path, "%s/%04d.%s", dir, tiling_id, details->tiling_format );
    dumpJsonData( path, DUMP_JSON_ONLY_INSIDE_POLYGONS );
                                            
    sprintf( path, "%s/index", dir );
    if ( ( fp = fopen( path, tiling_id > 1 ? "a" : "w" ) ) != NULL ) {
        fprintf( fp, "%04d: %s\n", tiling_id, recipe );
        logMsg( "INDEX %04d: %s\n", tiling_id, recipe );
        fclose( fp );
    }
    else 
        logMsg( "Failed to open index file" );
}

/*************************************************
 * Process tiling
 ************************************************/

short setupFromRecipe(
    char        *recipe
) {
    char        *chr;
    int         edge;
    short       type;
    _polygon    *new_polygon;
    _list       *common_vertices;

    _polygon    *current_polygon = NULL;
    int         current_edge = 0;
    short       current_polygon_pos = 0;
    short       retval = OK;

    logMsg( "Loading from recipe: %s", recipe );

    common_vertices = initialiseList( free );

    for ( chr = recipe; *chr != '\0'; chr++ ) {

        if (
            ( current_polygon != NULL ) &&
            ( current_edge == current_polygon->edges ) 
        ) {
            
            current_polygon_pos++;
            current_polygon = getFromListByPos( current_polygon_pos, &details->polygons );
            current_edge = 0;
        }

        if ( *chr == '-' ) {
            current_edge++;
            continue;
        }

        if ( ( type = findPolygonTypeByChar( *chr ) ) < 0 )
            logFatal( "Unknown polygon type %c in recipe #%d", *chr, ( chr - recipe ) );

        new_polygon = newPolygon( type );

        if ( chr == recipe ) {
            
            if ( *(++chr) != '*' )
                logFatal( "Invalid char #2 in recipe" );

            addToPolygonList( new_polygon );

            current_polygon = new_polygon;

            continue;
        }

        edge = atoi( ++chr );

        emptyList( common_vertices );

        if ( 
            ( checkForValidTiling( current_polygon, current_edge, new_polygon, edge ) == NOK ) ||
            ( findCommonVertices( current_polygon, current_edge, new_polygon, edge, common_vertices ) == NOK )
        ) {
    
            retval = NOK;
            break;
        }

        addPolygonToSide( current_polygon, current_edge,
                                new_polygon, edge, common_vertices ); 

        current_edge++;

        showWorking( INTERACTIVE_LEVEL_HI, "added polygon to side" );
    }

    releaseList( common_vertices );

    if ( retval == OK )
        showWorking( INTERACTIVE_LEVEL_LOW, "Inital setup loaded from recipe" );
    else
        logFatal( "Not a valid tiling polygon at recipe #%d", chr - recipe );

    return retval;
}

/*************************************************
 * Process tiling
 ************************************************/

void processTiling() {

    do {
        processPolygons();
        logMsg( "All polygons done or outside" );
        if ( details->polygons.count > 0 )
            saveTilingData();
    } while ( rewindPolygonList( REWIND_TO_LAST_INSIDE ) == OK );
}

/*************************************************
 * Main
 ************************************************/

int main (
    int     argc,
    char    **argv
) {
    short       pos;
    _polygon    *template;
    _polygon    *initial_polygon;

    initialiseLogging( NULL, LOGGING_LEVEL_DEBUG );

    initialiseDetails();
    processParameters( argc, argv );

    showWorking( INTERACTIVE_LEVEL_LOW, "Start" );

    if ( details->initial_recipe != NULL ) {

        if ( setupFromRecipe( details->initial_recipe ) == OK )
            processTiling();
    }
    else {

        for( pos = 0; pos < getTemplateCount(); pos++ ) {

            template = getTemplateByPos( pos );
            logMsg( "Initial polygon: %c", polygonTypeChar( template ) );

            initial_polygon = newPolygon( template->type );
            addToPolygonList( initial_polygon );

            processTiling();

            showWorking( INTERACTIVE_LEVEL_LOW, "done polygon type" );
        }
    }

    cleanup();

    exit( 0 );
}
