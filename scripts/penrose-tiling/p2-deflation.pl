#!/usr/bin/perl

use warnings;
use strict;

use Math::Trig;
use File::Path qw(make_path);
use Getopt::Std;

use Data::Dumper;

############################################################
#
#   p2-deflation.pl
#
#   Model the deflation process for Penrose P2 tilings
#
############################################################

use constant    IMAGE_WIDTH             =>  1000;
use constant    IMAGE_DEPTH             =>  1000;

use constant    POLYGON_TYPE_DART       =>  'dart';
use constant    POLYGON_TYPE_KITE       =>  'kite';
use constant    POLYGON_TYPE_HALF_DART  =>  'half-dart';
use constant    POLYGON_TYPE_HALF_KITE  =>  'half-kite';

use constant    SIDE_TYPE_LONG_A        =>  'long_side_a';  # phi-1
use constant    SIDE_TYPE_LONG_B        =>  'long_side_b';  # 1-phi
use constant    SIDE_TYPE_SHORT_A       =>  'short_side_a'; # 1/phi-1
use constant    SIDE_TYPE_SHORT_B       =>  'short_side_b'; # 1-1/phi
use constant    SIDE_TYPE_OPEN          =>  'open';

use constant    ARC_TYPE_NONE           =>  'none';
use constant    ARC_TYPE_DEFAULT        =>  'arcs';

use constant    STATUS_READY            =>  'status_ready';
use constant    STATUS_JOINED           =>  'status_joined';
use constant    STATUS_DONE             =>  'status_done';

use constant    DRAWING_TYPE_BORDER     =>  'border';
use constant    DRAWING_TYPE_FILL       =>  'fill';
use constant    DRAWING_TYPE_NORMAL     =>  'normal';

use constant    PHI                     =>  ( 1.0 + sqrt( 5.0 ) ) / 2.0;

use constant    TRUE                    =>  1;
use constant    FALSE                   =>  0;

############################################################
# Check the runline options

my %options;
getopts( 'a:Acd:D:fF:hk:lnopr:R:s:uW:x:y:', \%options );

help() if $options{ 'h' };

my $DRAW_ARC_TYPE = $options{ 'a' } || ARC_TYPE_DEFAULT;
my $DRAW_ARCS_ONLY = $options{ 'A' };
my $CONSOLIDATE = $options{ 'c' };
my $DEFLATIONS = $options{ 'd' } || 1;
my $FILL_POLYGONS = $options{ 'f' };
my $LABEL_POLYGONS = $options{ 'l' };
my $DRAW_OPEN_SIDES = $options{ 'o' };
my $DUMP_POLYGON_DATA = $options{ 'p' };
my $RECIPE = $options{ 'r' } || 'D';
my $DRAWING_SCALE = 50 * ( $options{ 's' } || 1 );
my $SHOW_LAST_UNUSED_SIDE = $options{ 'u' };

my $PS_TEMP = $options{ 'k' } || 'penrose.ps';

my $X_OFFSET->[ 0 ] = $options{ 'x' } || 0;
my $Y_OFFSET->[ 1 ] = $options{ 'y' } || 0;

my $OUTPUT_FILE = $options{ 'F' } || 'deflation.pdf';
my $IMAGE_DEPTH = $options{ 'D' } || IMAGE_DEPTH;
my $IMAGE_WIDTH = $options{ 'W' } || IMAGE_WIDTH;

loadRecipe( $options{ 'R' } ) if $options{ 'R' };

help( "Not a valid arc type" ) if ( 
    ( $DRAW_ARC_TYPE ne ARC_TYPE_DEFAULT ) &&
    ( $DRAW_ARC_TYPE ne ARC_TYPE_NONE  )
);

############################################################
# Setup the environment

$| = 1;

my $NEXT_POLYGON_ID = 1;

my $POLYGONS = [];
my $POLYGON_LOOKUP = {};

my $IMAGE_SIZE = [ $IMAGE_WIDTH, $IMAGE_DEPTH ];

my $TEMPLATE_DART = setupDartTemplate();
my $TEMPLATE_KITE = setupKiteTemplate();

############################################################
# Construct the initial layout

my %initial_polygon;

for( my $p = 0; $p < length( $RECIPE ); $p++ ) {

    my $instruction = substr $RECIPE, $p, 1;

    if ( $p == 0 ) {

        transformTemplate( \%initial_polygon,
                                getPolygonType( $instruction ), 0, 0, pi / 2.0 );

        # If a dart - translate the apex to the origin
        if ( isDart( \%initial_polygon ) ) {

            my $vertices = $initial_polygon{ 'VERTICES' };
            my $x_offset = $vertices->[ 2 ]->{ 'X' } - $vertices->[ 0 ]->{ 'X' };
            my $y_offset = $vertices->[ 2 ]->{ 'Y' } - $vertices->[ 0 ]->{ 'Y' };

            for ( @{ $vertices } ) {
                $_->{ 'X' } -= $x_offset;
                $_->{ 'Y' } -= $y_offset;
            };
        }

        $POLYGONS = [ \%initial_polygon ];
    }
    else {

        my $next_polygon = findFirstReadyPolygon();
        unless ( $next_polygon ) {
            printf "Could not find a READY polygon for recipe pos $p\n";
            exit -1;
        }

        my $next_side = findFirstReadyPolygonSide( $next_polygon );
        if ( $next_side < 0 ) {
            printf "Could not find a READY side for polygon at recipe pos $p\n";
            exit -1;
        }

        if ( $instruction eq '-' ) {

            $next_polygon->{ 'SIDES' }->[ $next_side ]->{ 'STATUS' } = STATUS_DONE;
            checkPolygonStatus( $next_polygon );
            next;
        }
        else  {

            push @{ $POLYGONS }, addPolygonToSide( $next_polygon, $next_side,
                                                getPolygonType( $instruction ) );
        }
    }
}

if ( $SHOW_LAST_UNUSED_SIDE ) {
    drawImage( DRAWING_TYPE_NORMAL );
    exit;
}

############################################################
# Run the deflation then image

for ( my $i = 0; $i < $DEFLATIONS; $i++ ) {

    my $half_polygon_list = createHalfPolygonList();

    my $new_list = [];
    for my $polygon ( @{ $half_polygon_list } ) {
    
        deflateHalfPolygon( $new_list, $polygon );
    }
    $POLYGONS = $new_list;
}

if ( $CONSOLIDATE ) {

    consolidateVertices();
    rejoinHalfPolygons();
}

if ( $FILL_POLYGONS ) {

    drawImage( DRAWING_TYPE_FILL );
    drawImage( DRAWING_TYPE_BORDER );
}
else {

    drawImage( DRAWING_TYPE_NORMAL );
}

dumpPolygons() if $DUMP_POLYGON_DATA;

exit;

############################################################
# Setup the dart template

sub setupDartTemplate {

    my $ang = 2.0 * pi / 5.0;
    my $long = 1.0 + PHI;
    my $short = 1.0 + 1.0 / PHI;
    
    return {
    
        'ANGLES'    =>  [ 3.0 * $ang, $ang / 2.0, $ang, $ang / 2.0  ],
    
        'SIDES'     =>  [
                newSide( SIDE_TYPE_SHORT_A ),
                newSide( SIDE_TYPE_LONG_A ),
                newSide( SIDE_TYPE_LONG_B ),
                newSide( SIDE_TYPE_SHORT_B ),
        ],
        'VERTICES'  =>  [
                newVertex( 0.0, 0.0 ),
                newVertex( $short, 0 ),
                newVertex( $short - $long * cos( $ang / 2.0 ), $long * sin( $ang / 2.0 ) ),
                newVertex( $short * cos( 3.0 * $ang ), $short * sin( 3.0 * $ang ) ),
        ],
    };
}
    
############################################################
# Setup the kite template

sub setupKiteTemplate {

    my $ang = 2.0 * pi / 5.0;
    my $long = 1.0 + PHI;
    my $short = 1.0 + 1.0 / PHI;
    
    return {
    
        'ANGLES'    =>  [ $ang, $ang, 2.0 * $ang, $ang ],
    
        'SIDES'     =>  [
                newSide( SIDE_TYPE_LONG_A ),
                newSide( SIDE_TYPE_SHORT_A ),
                newSide( SIDE_TYPE_SHORT_B ),
                newSide( SIDE_TYPE_LONG_B ),
        ],
        'VERTICES'  =>  [
                newVertex( 0.0, 0.0 ),
                newVertex( $long, 0 ),
                newVertex( $long - $short * cos( $ang ), $short * sin( $ang ) ),
                newVertex( $long * cos( $ang ), $long * sin( $ang ) ),
        ],
    };
}

############################################################
# Return the polygon type decsription

sub getPolygonDescription {

    my $polygon = shift;

    return isDart( $polygon ) ? 'Dart'
         : isKite( $polygon ) ? 'Kite'
         : isHalfDart( $polygon ) ? 'Half-Dart'
         : isHalfKite( $polygon ) ? 'Half-Kite'
         : ''
    ;
}

############################################################
# Find a point the given proporation between 2 points

sub pointAlong {

    my $vertex1 = shift;
    my $vertex2 = shift;
    my $val = shift;

    return newVertex( 
            ( ( 1.0 - $val ) * $vertex1->{ 'X' } ) + ( $val * $vertex2->{ 'X' } ),
            ( ( 1.0 - $val ) * $vertex1->{ 'Y' } ) + ( $val * $vertex2->{ 'Y' } ),
    );
}

############################################################
# Join half polygons to for full ones

sub rejoinHalfPolygons {

    my $new_list = [];

    for my $polygon ( @{ $POLYGONS } ) {

        if ( isDart( $polygon ) || isKite( $polygon ) ) {

            push @{ $new_list }, $polygon;
            next;
        }

        next if ( $polygon->{ 'STATUS' } eq STATUS_JOINED );

        # So this is an half polygon and the open side is #2 - there will only
        # be one polygon common to vertices #0 and #2 and that is the companion

        for my $common_id1 ( keys %{ $polygon->{ 'VERTICES' }->[ 0 ]->{ 'POLYGONS' } } ) {

            next if ( $common_id1 == $polygon->{ 'ID' } );

            for my $common_id2 ( keys %{ $polygon->{ 'VERTICES' }->[ 2 ]->{ 'POLYGONS' } } ) {
                
                if ( $common_id1 == $common_id2 ) {

                    my $companion = $POLYGON_LOOKUP->{ $common_id1 };

                    my $new_type = isHalfDart( $polygon ) ? POLYGON_TYPE_DART : POLYGON_TYPE_KITE;

                    my $template = polygonTemplate( $new_type );

                    my $first_polygon = ( $polygon->{ 'SIDES' }->[ 0 ]->{ 'TYPE' } eq
                                                    $template->{ 'SIDES' }->[ 0 ]->{ 'TYPE' } ) ?
                                                        $polygon : $companion;
                    my $second_polygon = ( $first_polygon == $polygon ) ? $companion : $polygon;

                    my $new_polygon = {
                                'ID'        =>  $NEXT_POLYGON_ID++,
                                'STATUS'    =>  STATUS_READY,
                                'TYPE'      =>  $new_type,
                                'SIDES'     =>  [
                                        $first_polygon->{ 'SIDES' }->[ 0 ],
                                        $first_polygon->{ 'SIDES' }->[ 1 ],
                                        $first_polygon->{ 'SIDES' }->[ 1 ],
                                        $second_polygon->{ 'SIDES' }->[ 0 ],
                                ],
                                'VERTICES'  =>  [
                                        $first_polygon->{ 'VERTICES' }->[ 0 ],
                                        $first_polygon->{ 'VERTICES' }->[ 1 ],
                                        $first_polygon->{ 'VERTICES' }->[ 2 ],
                                        $second_polygon->{ 'VERTICES' }->[ 1 ],
                                ],
                    };

                    push @{ $new_list }, $new_polygon;

                    $companion->{ 'STATUS' } = STATUS_JOINED;
                }
            }
        }
    }

    $POLYGONS = $new_list;

    return;
}

############################################################
# Return a new side object

sub newSide {

    return {
        'TYPE'      =>  shift,
        'STATUS'    =>  STATUS_READY,
    };
}

############################################################
# Deflate a polygon

sub deflateHalfPolygon {

    my $new_list = shift;
    my $polygon = shift;

    my $vertices = $polygon->{ 'VERTICES' };

    if ( isHalfKite( $polygon ) ) {

        push @{ $new_list }, {
            'ID'        =>  $NEXT_POLYGON_ID++,
            'STATUS'    =>  STATUS_READY,
            'TYPE'      =>  POLYGON_TYPE_KITE,
            'SIDES'     =>  [
                    newSide( SIDE_TYPE_LONG_A ),
                    newSide( SIDE_TYPE_SHORT_A ),
                    newSide( SIDE_TYPE_SHORT_B ),
                    newSide( SIDE_TYPE_LONG_B ),
            ],
            'VERTICES'  =>  [
                    $vertices->[ 1 ],
                    pointAlong( $vertices->[ 1 ], $vertices->[ 0 ], 1 / PHI ),
                    pointAlong( $vertices->[ 2 ], $vertices->[ 0 ], 1 / ( PHI * PHI ) ),
                    $vertices->[ 2 ],
            ],
        };

        push @{ $new_list }, {
            'ID'        =>  $NEXT_POLYGON_ID++,
            'STATUS'    =>  STATUS_READY,
            'TYPE'      =>  POLYGON_TYPE_HALF_DART,
            'SIDES'     =>  [
                    newSide( SIDE_TYPE_LONG_A ),
                    newSide( SIDE_TYPE_SHORT_A ),
                    newSide( SIDE_TYPE_OPEN ),
            ],
            'VERTICES'  =>  [
                    pointAlong( $vertices->[ 1 ], $vertices->[ 0 ], 1 / PHI ),
                    pointAlong( $vertices->[ 2 ], $vertices->[ 0 ], 1 / ( PHI * PHI ) ),
                    $vertices->[ 0 ],
            ],
        };
    }
    else {
        
        push @{ $new_list }, {
            'ID'        =>  $NEXT_POLYGON_ID++,
            'STATUS'    =>  STATUS_READY,
            'TYPE'      =>  POLYGON_TYPE_HALF_DART,
            'SIDES'     =>  [
                    newSide( SIDE_TYPE_LONG_A ),
                    newSide( SIDE_TYPE_SHORT_A ),
                    newSide( SIDE_TYPE_OPEN ),
            ],
            'VERTICES'  =>  [
                    pointAlong( $vertices->[ 1 ], $vertices->[ 2 ], 1 / ( PHI * PHI ) ),
                    $vertices->[ 0 ],
                    $vertices->[ 1 ],
            ],
        };
        
        push @{ $new_list }, {
            'ID'        =>  $NEXT_POLYGON_ID++,
            'STATUS'    =>  STATUS_READY,
            'TYPE'      =>  POLYGON_TYPE_HALF_KITE,
            'SIDES'     =>  [
                    newSide( SIDE_TYPE_LONG_A ),
                    newSide( SIDE_TYPE_SHORT_A ),
                    newSide( SIDE_TYPE_OPEN ),
            ],
            'VERTICES'  =>  [
                    $vertices->[ 2 ],
                    pointAlong( $vertices->[ 1 ], $vertices->[ 2 ], 1 / ( PHI * PHI ) ),
                    $vertices->[ 0 ],
            ],
        };
    }

    return;
}

############################################################
# Convert any full polygon into 2 half ones

sub createHalfPolygonList {

    my $half_list = [];

    for my $polygon ( @{ $POLYGONS} ) {
        
        if ( isDart( $polygon ) || isKite( $polygon ) ) {

            my $new_type = isDart( $polygon ) ?
                    POLYGON_TYPE_HALF_DART : POLYGON_TYPE_HALF_KITE;

            my $new_polygon = {
                'ID'        => $NEXT_POLYGON_ID++,
                'STATUS'    => STATUS_READY,
                'TYPE'      => $new_type,
                'SIDES'     => [
                                $polygon->{ 'SIDES' }->[ 0 ],
                                $polygon->{ 'SIDES' }->[ 1 ],
                                { 
                                    'TYPE'      => SIDE_TYPE_OPEN,
                                    'STATUS'    => STATUS_READY 
                                },
                            ],
                'VERTICES'  => [
                                $polygon->{ 'VERTICES' }->[ 0 ],
                                $polygon->{ 'VERTICES' }->[ 3 ],
                                $polygon->{ 'VERTICES' }->[ 2 ],
                            ],
            };

            push @{ $half_list }, $new_polygon;

            $polygon->{ 'TYPE' } = $new_type;
            pop @{ $polygon->{ 'VERTICES' } };
            pop @{ $polygon->{ 'SIDES' } };
            $polygon->{ 'SIDES' }->[ 2 ]->{ 'TYPE' } = SIDE_TYPE_OPEN;

            push @{ $half_list }, $polygon;
        }
        else {  # must already be an half polygon
        
            push @{ $half_list }, $polygon;
        }
    }

    return $half_list;
}

############################################################
# Simple mod/min/max functions

sub min {

    my ( $v1, $v2 ) = @_;

    return ( $v1 < $v2 ) ? $v1 : $v2;
}

sub max {

    my ( $v1, $v2 ) = @_;

    return ( $v1 > $v2 ) ? $v1 : $v2;
}

sub mod {

    my $v = shift;

    return ( $v > 0 ) ? $v : ( -1 * $v );
}

sub isDart { return shift->{ 'TYPE' } eq POLYGON_TYPE_DART }
sub isKite { return shift->{ 'TYPE' } eq POLYGON_TYPE_KITE }
sub isHalfDart { return shift->{ 'TYPE' } eq POLYGON_TYPE_HALF_DART }
sub isHalfKite { return shift->{ 'TYPE' } eq POLYGON_TYPE_HALF_KITE }

sub getPolygonType {
    
    my $type = shift;

    return ( $type eq 'D' ) ? POLYGON_TYPE_DART
        :  ( $type eq 'K' ) ? POLYGON_TYPE_KITE
        :  ( $type eq 'd' ) ? POLYGON_TYPE_HALF_DART
        :                     POLYGON_TYPE_HALF_KITE
    ;
}

############################################################
# Find the first polygon in a READY state

sub findFirstReadyPolygon {

    foreach ( @{ $POLYGONS } ) {

        return $_ if ( $_->{ 'STATUS' } eq STATUS_READY );
    }

    return;
}

############################################################
# Find the first polygon side in a READY state

sub findFirstReadyPolygonSide{

    my $polygon = shift;

    return -1 unless $polygon;

    my $sides = $polygon->{ 'SIDES' };

    for( my $s = 0; $s < scalar( @{$sides} ); $s++ ) {

        return $s if ( $sides->[ $s ]->{ 'STATUS' } eq STATUS_READY );
    }

    return -1;
}

############################################################
# Find the sum of internal angles to the given side

sub findPolygonSideAngle {

    my $polygon_type = shift;
    my $side_number = shift;

    my $ang = 0;

    my $polygon = polygonTemplate( $polygon_type );

    while ( $side_number > 0 ) {

        $ang += $polygon->{ 'ANGLES' }->[ $side_number ];
        $side_number--;
    }

    return $ang;
}

############################################################
# Find the side type to match that given

sub findMatchingSideType {

    my $side_type = shift;

    return ( $side_type eq SIDE_TYPE_LONG_A )  ? SIDE_TYPE_LONG_B
         : ( $side_type eq SIDE_TYPE_LONG_B )  ? SIDE_TYPE_LONG_A
         : ( $side_type eq SIDE_TYPE_SHORT_A ) ? SIDE_TYPE_SHORT_B
         :                                       SIDE_TYPE_SHORT_A
    ;
}

############################################################
# Find the side to match that given on the polygon type

sub findSideMatch {

    my $polygon_type = shift;
    my $side_type = shift;

    my $match_type = findMatchingSideType( $side_type );
    return -1 unless $match_type;

    my $template = polygonTemplate( $polygon_type );

    for( my $s = 0; $s < scalar( @{$template->{ 'SIDES' }} ); $s++ ) {

        my $side = $template->{ 'SIDES' }->[ $s ];
        return $s if ( $side->{ 'TYPE' } eq $match_type );
    }

    return -1;
}

############################################################
# Consolidate vertices

sub consolidateVertices {

    my $max_dist = 0.00001;

    my $count = scalar @{ $POLYGONS };
    my $approx_checks = $count * ( $count + 1 ) / 2.0;

    printf "Consolidating vertices\n";

    my $c = 0;
    for ( my $p1 = 0; $p1 < $count; $p1++ ) {
     
        printf "Progress: %0.2f   \r", 100 * $c / $approx_checks;

        my $polygon1 = $POLYGONS->[ $p1 ];
        $POLYGON_LOOKUP->{ $polygon1->{ 'ID' } } = $polygon1;
    
        my $vertices1 = $polygon1->{ 'VERTICES' };

        for ( @{ $vertices1 } ) {
            $_->{ 'POLYGONS' }->{ $polygon1->{ 'ID' } } = 1;
        };

        for ( my $p2 = $p1 + 1; $p2 < $count; $p2++ ) {

            $c++;

            my $polygon2 = $POLYGONS->[ $p2 ];

            next if ( $polygon1 == $polygon2 );

            my $vertices2 = $polygon2->{ 'VERTICES' };

            for ( my $v1 = 0; $v1 < scalar @{ $vertices1 }; $v1++ ) {
            
                my $dist = distance( $vertices1->[ 0 ], $vertices2->[ 0 ] );
                next if $dist > 6;

                for ( my $v2 = 0; $v2 < scalar @{ $vertices2 }; $v2++ ) {
            
                    next if ( $vertices1->[ $v1 ] == $vertices2->[ $v2 ] );

                    my $d = distance( $vertices1->[ $v1 ], $vertices2->[ $v2 ] );
                    if ( $d < $max_dist ) {

                        $vertices2->[ $v2 ] = $vertices1->[ $v1 ];
                        $vertices2->[ $v2 ]->{ 'POLYGONS' }->{ $polygon2->{ 'ID' } } = 1;
                    }
                }
            }
        }
    }

    printf "Done            \n";

    return;
}

############################################################
# Add a polygon to an existing polygon side

sub addPolygonToSide {

    my $polygon = shift;
    my $s = shift;
    my $next_type = shift;

    my $polygon_type = $polygon->{ 'TYPE' };
    my $vertices = $polygon->{ 'VERTICES' };
    my $side = $polygon->{ 'SIDES' }->[ $s ];

    my $side_number = findSideMatch( $next_type, $side->{ 'TYPE' } );

    my $side_ang = atan2(
                $vertices->[ $s - 3 ]->{ 'Y' } - $vertices->[ $s ]->{ 'Y' },
                $vertices->[ $s - 3 ]->{ 'X' } - $vertices->[ $s ]->{ 'X' }
    );

    # How far round to rotate the polygon to align the given side
    my $polygon_ang =
                pi
                + findPolygonSideAngle( $next_type, $side_number )
                - $side_number * pi
                + $side_ang;
    
    my %new_polygon;
    transformTemplate( \%new_polygon, $next_type, 
                                            $vertices->[ $s ]->{ 'X' },
                                            $vertices->[ $s ]->{ 'Y' },
                                            $polygon_ang );

    my $new_vertices = $new_polygon{ 'VERTICES' };
    my $idx = ( $polygon_type ne $next_type ) ? ( $s + 3 ) : ( 4 - $s );
    my $x_offset = $new_vertices->[ $idx % 4 ]->{ 'X' }
                                - $new_vertices->[ 0 ]->{ 'X' };
    my $y_offset = $new_vertices->[ $idx % 4 ]->{ 'Y' }
                                - $new_vertices->[ 0 ]->{ 'Y' }; 
    for ( @{ $new_vertices } ) {
        $_->{ 'X' } -= $x_offset;
        $_->{ 'Y' } -= $y_offset;
    }


    $side->{ 'STATUS' } = STATUS_DONE;
    $new_polygon{ 'SIDES' }->[ $side_number ]->{ 'STATUS' } = STATUS_DONE;

    checkPolygonStatus( $polygon );

    return \%new_polygon;   
}

############################################################
# Check polygon status

sub checkPolygonStatus {

    my $polygon = shift;

    for ( @{ $polygon->{ 'SIDES' } } ) {

        if ( $_->{ 'STATUS' } eq STATUS_READY ) {

            $polygon->{ 'STATUS' } = STATUS_READY;
            return;
        }

    }

    $polygon->{ 'STATUS' } = STATUS_DONE;

    return;
}

############################################################
# Find the distance between 2 vertices

sub distance {

    my $vtx1 = shift;
    my $vtx2 = shift;

    my $dx = $vtx1->{ 'X' } - $vtx2->{ 'X' };
    my $dy = $vtx1->{ 'Y' } - $vtx2->{ 'Y' };

    return sqrt( ( $dx * $dx ) +  ( $dy * $dy ) );
}

############################################################
# Translate, rotate and scale a vertex

sub transformVertex {

    my $vertex = shift;
    my $offset_x = shift;
    my $offset_y = shift;
    my $rotate = shift;

    my $ang = atan2( $vertex->{ 'Y' },  $vertex->{ 'X' } );
    my $dist = distance( { 'X' => 0.0, 'Y' => 0.0 }, $vertex );

    $ang += $rotate;

    $vertex->{ 'X' } = $dist * cos( $ang ) + $offset_x;
    $vertex->{ 'Y' } = $dist * sin( $ang ) + $offset_y;

    return;
}

############################################################
# Return the template for the given polygon type

sub polygonTemplate {

    my $type = shift;

    return ( $type eq POLYGON_TYPE_DART ) ?      $TEMPLATE_DART
         : ( $type eq POLYGON_TYPE_KITE ) ?      $TEMPLATE_KITE
         : ( $type eq POLYGON_TYPE_HALF_DART ) ? $TEMPLATE_DART
         :                                       $TEMPLATE_KITE
    ;
}

############################################################
# Write the image header

sub writeImageHeader {

    my $fh = shift;

    printf $fh "%%!PS\%%%%BoundingBox: 0 0 %f %f\n%%%%EndComments

        <<
            /PageSize [ %f %f ]
            /ImagingBBox null
        >> setpagedevice

        /mv { moveto } bind def
        /ln { lineto } bind def

        /drawArc {
            /a1 exch def
            /a2 exch def
            /rad exch def
            /yc exch def
            /xc exch def
    
            xc a1 cos rad mul add
            yc a1 sin rad mul add
            moveto
            xc yc rad a1 a2 arcn
        } bind def

        0 0 mv %f 0 ln %f %f ln 0 %f ln fill

        /Arial-Bold findfont 12 scalefont setfont 

        1 setlinejoin
        %f setlinewidth

        %f %f translate
    ",
        $IMAGE_WIDTH, $IMAGE_DEPTH,
        $IMAGE_SIZE->[ 0 ], $IMAGE_SIZE->[ 1 ],
        $IMAGE_WIDTH, $IMAGE_WIDTH,
        $IMAGE_DEPTH, $IMAGE_DEPTH,
        ( $DEFLATIONS > 0 ) ? 1 / ( PHI ** ( $DEFLATIONS - 1 ) ) : 1,
        $IMAGE_SIZE->[ 0 ] / 2.0 + $X_OFFSET->[ 0 ], 
        $IMAGE_SIZE->[ 1 ] / 2.0 + $Y_OFFSET->[ 1 ],
    ;

    return;
}

############################################################
# Draw the image

sub drawImage {

    my $drawing_type = shift;

    open my $fh, '>', $PS_TEMP or die "Could not open $PS_TEMP - $!\n";

    writeImageHeader( $fh );

    if ( $drawing_type eq DRAWING_TYPE_NORMAL ) {

        drawPolygons( $fh, DRAWING_TYPE_NORMAL );
    }
    else {

        drawPolygons( $fh, DRAWING_TYPE_FILL );
        drawPolygons( $fh, DRAWING_TYPE_BORDER );
    }

    if ( $SHOW_LAST_UNUSED_SIDE ) {
    
        my $last_polygon = findFirstReadyPolygon();
        my $last_side = findFirstReadyPolygonSide( $last_polygon );

        if ( $last_side > -1 ) {

            my $vertex1 = $last_polygon->{ 'VERTICES' }->[ $last_side ];

            my $next_side = ( $last_side + 1 ) % scalar @{ $last_polygon->{ 'VERTICES' } };
            my $vertex2 = $last_polygon->{ 'VERTICES' }->[ $next_side ];

            printf $fh "gsave
                    3 setlinewidth
                    0 1 1 setrgbcolor 
                    %f %f mv 
                    %f %f ln 
                    stroke 
                    grestore\n",
                        $DRAWING_SCALE * $vertex1->{ 'X' },
                        $DRAWING_SCALE * $vertex1->{ 'Y' },
                        $DRAWING_SCALE * $vertex2->{ 'X' },
                        $DRAWING_SCALE * $vertex2->{ 'Y' };
                        
        }
    }

    close $fh;

    my $convert = "/usr/bin/convert";

    unless ( -f $convert ) {

        printf "Count not locate convert at $convert\n";
    }
    else {

        system( "$convert $PS_TEMP $OUTPUT_FILE" );
        unlink $PS_TEMP unless $options{ 'k' };
    }

    return;
}

############################################################
# Draw the polygons

sub drawPolygons {

    my $fh = shift;
    my $drawing_type = shift;

    for my $polygon ( @{$POLYGONS} ) {

        isDart( $polygon )     ? drawDart( $fh, $drawing_type, $polygon ) :
        isKite( $polygon )     ? drawKite( $fh, $drawing_type, $polygon ) :
        isHalfDart( $polygon ) ? drawHalfDart( $fh, $drawing_type, $polygon ) :
                                 drawHalfKite( $fh, $drawing_type, $polygon )
        ;
    }

    return;
}

############################################################
# Return a new vertex object

sub newVertex {

   return {
        'X'         =>  shift,
        'Y'         =>  shift,
    };
}

############################################################
# Translate, rotate and scale a copy of the template polygon

sub transformTemplate {

    my $polygon = shift;
    my $type = shift;
    my $offset_x = shift;
    my $offset_y = shift;
    my $rotate = shift;

    my $template = polygonTemplate( $type );

    $polygon->{ 'ID' } = $NEXT_POLYGON_ID++;
    $polygon->{ 'STATUS' } = STATUS_READY;
    $polygon->{ 'TYPE' } = $type;
    $polygon->{ 'VERTICES' } = [];
    $polygon->{ 'ANGLES' } = [ @{ $template->{ 'ANGLES' } } ];
    $polygon->{ 'SIDES' } = [ map { my %h = %{ $_ }; \%h } ( @{ $template->{ 'SIDES' } } ) ];

    for my $vertex ( @{ $template->{ 'VERTICES' } } ) {

        my %vertex_copy = %{ $vertex };
    
        transformVertex( \%vertex_copy, $offset_x, $offset_y, $rotate );

        push @{ $polygon->{ 'VERTICES' } }, \%vertex_copy;
    }

    if ( 
        ( $type eq POLYGON_TYPE_HALF_DART ) ||
        ( $type eq POLYGON_TYPE_HALF_KITE ) 
    ) {
        pop @{ $polygon->{ 'VERTICES' } };
        pop @{ $polygon->{ 'SIDES' } };
        $polygon->{ 'SIDES' }->[ 2 ]->{ 'TYPE' } = SIDE_TYPE_OPEN;
    }

    return $polygon;
}

############################################################
# Label the given polygon

sub labelPolygon {

    my $fh = shift;
    my $polygon = shift;

    my $idx = isKite( $polygon ) ? 1 : 0;

    my $vertices = $polygon->{ 'VERTICES' };
    my $x = ( $vertices->[ $idx ]->{ 'X' } + $vertices->[ $idx + 2 ]->{ 'X' } ) / 2.0;
    my $y = ( $vertices->[ $idx ]->{ 'Y' } + $vertices->[ $idx + 2 ]->{ 'Y' } ) / 2.0;

    if ( isHalfDart( $polygon ) || isHalfKite( $polygon ) ) {
        $x = ( 2.0 * $x / 3.0 ) + ( $vertices->[ 1 ]->{ 'X' } / 3.0 );
        $y = ( 2.0 * $y / 3.0 ) + ( $vertices->[ 1 ]->{ 'Y' } / 3.0 );
    }

    my $id = $polygon->{ 'ID' };
    printf $fh "
            1 setgray
            %f %f moveto
            ($id) dup stringwidth pop -2 div 0 rmoveto show
        ",
        $x * $DRAWING_SCALE,
        $y * $DRAWING_SCALE,
    ;

    return;
}

############################################################
# Draw an arc segment

sub drawArc {

    my $fh = shift;
    my $vertex = shift;
    my $point1 = shift;
    my $point2 = shift;
    my $dist = shift;
    my $obtuse = shift;

    my $ang1 = rad2deg( atan2(
                $point1->{ 'Y' } - $vertex->{ 'Y' },
                $point1->{ 'X' } - $vertex->{ 'X' }
    ) ) % 360; 

    my $ang2 = rad2deg( atan2(
                $point2->{ 'Y' } - $vertex->{ 'Y' },
                $point2->{ 'X' } - $vertex->{ 'X' }
    ) ) % 360; 

    my $from = min( $ang1, $ang2 );
    my $to = max( $ang1, $ang2 );

    if ( 
        ( ! $obtuse && ( ( $to - $from ) > 180 ) ) ||
        ( $obtuse && ( ( $to - $from ) < 180 ) )
    ) {
        my $tmp = $from;
        $from = $to;
        $to = $tmp;
    }

    printf $fh "%f %f %f %f %f drawArc stroke\n",
                                        $DRAWING_SCALE * $vertex->{ 'X' },
                                        $DRAWING_SCALE * $vertex->{ 'Y' },
                                        $DRAWING_SCALE * $dist,
                                        $from, $to
    ;

    return;
}

############################################################
# Draw a dart

sub drawDart {

    my $fh = shift;
    my $drawing_type = shift;
    my $polygon = shift;

    my $vertices = $polygon->{ 'VERTICES' };

    if ( $drawing_type eq DRAWING_TYPE_NORMAL ) {

        if ( $DRAW_ARC_TYPE eq ARC_TYPE_DEFAULT ) {

            printf $fh "1 0 0 setrgbcolor\n"; 
            drawArc( $fh, $vertices->[ 0 ], $vertices->[ 1 ], $vertices->[ 3 ],
                        distance( $vertices->[ 0 ], $vertices->[ 1 ] ) * ( 1 - 1/PHI ), TRUE );
     
            printf $fh "0 1 0 setrgbcolor\n",;
            drawArc( $fh, $vertices->[ 2 ], $vertices->[ 1 ], $vertices->[ 3 ],
                        distance( $vertices->[ 1 ], $vertices->[ 2 ] ) * ( 1 - 1/PHI ) );
        }

        unless ( $DRAW_ARCS_ONLY ) {

            printf $fh "1 1 0 setrgbcolor\n";
            for ( my $v = 0; $v < scalar( @{$vertices} ); $v++ ) {
    
                printf $fh "%f %f %s ", 
                        $DRAWING_SCALE * $vertices->[ $v ]->{ 'X' },
                             $DRAWING_SCALE * $vertices->[ $v ]->{ 'Y' },
                            ( $v == 0 ) ? 'mv' : 'ln'
                ;
            }
    
            printf $fh "closepath stroke\n";
        }

        labelPolygon( $fh, $polygon ) if $LABEL_POLYGONS;
    }
    else {

        for ( my $v = 0; $v < 4; $v++ ) {

            printf $fh "%f %f %s ",
                        $DRAWING_SCALE * $vertices->[ $v ]->{ 'X' },
                        $DRAWING_SCALE * $vertices->[ $v ]->{ 'Y' },
                        ( $v == 0 ) ? 'mv' : 'ln'
            ;
        }

        if ( $drawing_type eq DRAWING_TYPE_FILL ) {

            printf $fh "1 0 0 setrgbcolor fill\n";
        }
        else {

            printf $fh "closepath 0 setgray stroke\n";
        }
    }

    return;
}

############################################################
# Draw a kite

sub drawKite {

    my $fh = shift;
    my $drawing_type = shift;
    my $polygon = shift;

    my $vertices = $polygon->{ 'VERTICES' };

    if ( $drawing_type eq DRAWING_TYPE_NORMAL ) {

        if ( $DRAW_ARC_TYPE eq ARC_TYPE_DEFAULT ) {
        
            printf $fh "1 0 0 setrgbcolor\n",;
            drawArc( $fh, $vertices->[ 2 ], $vertices->[ 1 ], $vertices->[ 3 ],
                            distance( $vertices->[ 1 ], $vertices->[ 2 ] ) / PHI );
    
            printf $fh "0 1 0 setrgbcolor\n"; 
            drawArc( $fh, $vertices->[ 0 ], $vertices->[ 1 ], $vertices->[ 3 ],
                            distance( $vertices->[ 0 ], $vertices->[ 1 ] ) / PHI );
        }

        unless ( $DRAW_ARCS_ONLY ) {

            printf $fh "1 1 0 setrgbcolor\n";
            for ( my $v = 0; $v < scalar( @{$vertices} ); $v++ ) {
    
                printf $fh "%f %f %s ", 
                            $DRAWING_SCALE * $vertices->[ $v ]->{ 'X' },
                            $DRAWING_SCALE * $vertices->[ $v ]->{ 'Y' },
                            ( $v == 0 ) ? 'mv' : 'ln'
                ;
            }
    
            printf $fh "closepath stroke\n";
        }

        labelPolygon( $fh, $polygon ) if $LABEL_POLYGONS;
    }
    else {

        for ( my $v = 0; $v < 4; $v++ ) {

            printf $fh "%f %f %s ",
                        $DRAWING_SCALE * $vertices->[ $v ]->{ 'X' },
                        $DRAWING_SCALE * $vertices->[ $v ]->{ 'Y' },
                        ( $v == 0 ) ? 'mv' : 'ln'
            ;
        }

        if ( $drawing_type eq DRAWING_TYPE_FILL ) {

            printf $fh "1 1 0 setrgbcolor fill\n";
        }
        else {

            printf $fh "closepath 0 setgray stroke\n";
        }
    }

    return;
}

############################################################
# Draw an half dart

sub drawHalfDart {

    my $fh = shift;
    my $drawing_type = shift;
    my $polygon = shift;

    my $vertices = $polygon->{ 'VERTICES' };

    if ( $drawing_type eq DRAWING_TYPE_NORMAL ) {

        if ( $DRAW_ARC_TYPE eq ARC_TYPE_DEFAULT ) {

            printf $fh "1 0 0 setrgbcolor\n"; 
            drawArc( $fh, $vertices->[ 0 ], $vertices->[ 1 ], $vertices->[ 2 ],
                        distance( $vertices->[ 0 ], $vertices->[ 1 ] ) * ( 1 - 1/PHI ) );
    
            printf $fh "0 1 0 setrgbcolor\n",;
            drawArc( $fh, $vertices->[ 2 ], $vertices->[ 0 ], $vertices->[ 1 ],
                            distance( $vertices->[ 1 ], $vertices->[ 2 ] ) * ( 1 - 1/PHI ) );
        }
    
        unless ( $DRAW_ARCS_ONLY ) {

            if ( $DRAW_OPEN_SIDES ) {
    
                printf $fh "gsave 0 0 1 setrgbcolor %f %f mv %f %f ln stroke grestore\n",
                            $DRAWING_SCALE * $vertices->[ 0 ]->{ 'X' },
                            $DRAWING_SCALE * $vertices->[ 0 ]->{ 'Y' },
                            $DRAWING_SCALE * $vertices->[ 2 ]->{ 'X' },
                            $DRAWING_SCALE * $vertices->[ 2 ]->{ 'Y' }
                ;
            }
    
            printf $fh "1 1 0 setrgbcolor\n";
            for ( my $v = 0; $v < 3; $v++ ) {
    
                printf $fh "%f %f %s ", 
                            $DRAWING_SCALE * $vertices->[ $v ]->{ 'X' },
                            $DRAWING_SCALE * $vertices->[ $v ]->{ 'Y' },
                            ( $v == 0 ) ? 'mv' : 'ln'
                ;
            }
    
            printf $fh "stroke\n";
        }

        labelPolygon( $fh, $polygon ) if $LABEL_POLYGONS;
    }
    else {

        for ( my $v = 0; $v < 3; $v++ ) {

            printf $fh "%f %f %s ",
                        $DRAWING_SCALE * $vertices->[ $v ]->{ 'X' },
                        $DRAWING_SCALE * $vertices->[ $v ]->{ 'Y' },
                        ( $v == 0 ) ? 'mv' : 'ln'
            ;
        }

        if ( $drawing_type eq DRAWING_TYPE_FILL ) {

            printf $fh "1 0 0 setrgbcolor gsave closepath stroke grestore fill\n";
        }
        else {
    
            printf $fh "%s 0 setgray stroke\n", $DRAW_OPEN_SIDES ? 'closepath' : '' ;
        }
    }

    return;
}

############################################################
# Draw an half kite

sub drawHalfKite {

    my $fh = shift;
    my $drawing_type = shift;
    my $polygon = shift;

    my $vertices = $polygon->{ 'VERTICES' };

    if ( $drawing_type eq DRAWING_TYPE_NORMAL ) {

        if ( $DRAW_ARC_TYPE eq ARC_TYPE_DEFAULT ) {

            printf $fh "1 0 0 setrgbcolor\n",;
            drawArc( $fh, $vertices->[ 2 ], $vertices->[ 0 ], $vertices->[ 1 ],
                                distance( $vertices->[ 1 ], $vertices->[ 2 ] ) / PHI );
    
            printf $fh "0 1 0 setrgbcolor\n"; 
            drawArc( $fh, $vertices->[ 0 ], $vertices->[ 1 ], $vertices->[ 2 ],
                                distance( $vertices->[ 0 ], $vertices->[ 1 ] ) / PHI );
        }

        unless ( $DRAW_ARCS_ONLY ) {

            if ( $DRAW_OPEN_SIDES ) {
    
                printf $fh "gsave 0 0 1 setrgbcolor %f %f mv %f %f ln stroke grestore\n",
                            $DRAWING_SCALE * $vertices->[ 0 ]->{ 'X' },
                            $DRAWING_SCALE * $vertices->[ 0 ]->{ 'Y' },
                            $DRAWING_SCALE * $vertices->[ 2 ]->{ 'X' },
                            $DRAWING_SCALE * $vertices->[ 2 ]->{ 'Y' }
                ;
            }
    
            printf $fh "1 1 0 setrgbcolor\n";
            for ( my $v = 0; $v < 3; $v++ ) {
    
                printf $fh "%f %f %s ", 
                            $DRAWING_SCALE * $vertices->[ $v ]->{ 'X' },
                            $DRAWING_SCALE * $vertices->[ $v ]->{ 'Y' },
                            ( $v == 0 ) ? 'mv' : 'ln'
                ;
            }
    
            printf $fh "stroke\n";
        }
    
        labelPolygon( $fh, $polygon ) if $LABEL_POLYGONS;
    }
    else {

        for ( my $v = 0; $v < 3; $v++ ) {
    
            printf $fh "%f %f %s ", 
                        $DRAWING_SCALE * $vertices->[ $v ]->{ 'X' },
                        $DRAWING_SCALE * $vertices->[ $v ]->{ 'Y' },
                        ( $v == 0 ) ? 'mv' : 'ln'
            ;
        }
    
        if ( $drawing_type eq DRAWING_TYPE_FILL ) {

            printf $fh "1 1 0 setrgbcolor gsave closepath stroke grestore fill\n";
        }
        else {
    
            printf $fh "%s 0 setgray stroke\n", $DRAW_OPEN_SIDES ? 'closepath' : '' ;
        }
    }

    return;
}

############################################################
# Load recipe

sub loadRecipe {

    my $recipe_file = shift;

    open my $FH, '<', $recipe_file or do {
        printf "Could not open $recipe_file- $!\n";
        exit -1;
    };

    $RECIPE = <$FH>;
    chomp $RECIPE;

    close $FH;

    return;
}

############################################################
# Dump polygon status

sub dumpPolygons {

    for my $polygon ( @{ $POLYGONS } ) {

        printf "P%s - %s\n", $polygon->{ 'ID' }, getPolygonDescription( $polygon );

        for ( @{ $polygon->{ 'VERTICES' } } ) {
            my $v = $_;
            printf "    %x [ ", $v;
            printf join ',', ( sort keys %{ $v->{ 'POLYGONS' } } );
            printf " ]\n";
        };
    }

    exit;
}

############################################################
# Help doco

sub help {

    print "\n@_\n" if @_;

    printf "\n$0 " .
                "[-a <type>] [-A] [-c] [-d <deflations>] [-f] [-F <file>] [-h] " .
                "[-l] [-o] [-p] [-r <recipe>] [-R <file>] [-s <scale>] [-u] [-x]

    where
        -A  only draw the arcs
        -a  arc type to draw (see below)
        -c  consolodate half polygons into full ones
        -d  number of deflation cycles (default 1)
        -f  fill the polygons
        -F  output file name
        -h  this help message
        -l  label the polygons
        -o  draw the open sides
        -p  print polgon data
        -r  recipe string
        -R  file containing a recipe string
        -s  drawing scale
        -u  highlight the last unused side

    Valid arc types are
        'arcs'  - normal circular arcs
        'none'  - do not draw any arcs
    \n";

    exit;
}

__END__
