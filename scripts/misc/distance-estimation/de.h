/*******************************************************************************
 *
 * de.h
 *
 * Definitions for the distance estimation investigation
 *
 ******************************************************************************/

#ifndef _DE_H

typedef struct {
    int         xpxl;
    int         ypxl;

    long double ximg;
    long double yimg;

    long double val;
    long double dist;

    long double grad;
    long double grad2;
} _point;

typedef struct {
    char        *tmp_ps;
    _point      *data;

    int         image_pixel_dep;
    int         image_pixel_wid;

    long double image_rad;
    long double image_xc;
    long double image_yc;
} _env;

typedef struct {
    int r;
    int g;
    int b;
} _color;

/*************************************************
 * Prototypes
 ************************************************/
long double f( _point *pt );
long double fAtXY( long double x, long double y );

void setupEnv();
void logFatal( char *format, ...);
void getCharParameter( int argc, char **argv, int pnt, char **value, char *desc);
void getIntParameter( int argc, char **argv, int pnt, int *value, char *desc);
void getDoubleParameter( int argc, char **argv, int pnt, long double *value, char *desc);
void processParameters( int argc, char **argv);
void processImage();
void drawExtras( FILE *fp );
void drawDisc( FILE *fp, long double xc, long double yc, long double rad );
void drawDiscAt( FILE *fp, long double xc, long double yc );
long double clamp( long double val, long double upper_val, long double lower_val );
long double smoothStep( long double edge0, long double edge1, long double val );
_point *grad( _point  *pt );
_point *grad2( _point  *pt );
_color *pointColor( _point *pt );
void drawImage();
void usage( char *self);

#define _DE_H
#endif
