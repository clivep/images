/*******************************************************************************
 *
 * complex.h
 *
 *  Utility functions for complex numbers
 *
 ******************************************************************************/

#include "fractal.h"

#ifndef _MY_COMPLEX_H

void setc( long double x, long double  y, _complex *res );
void cpyc( _complex c, _complex *res );
void mulc( _complex c1, _complex c2, _complex *res );
void divc( _complex c1, _complex c2, _complex *res );
void addc( _complex c1, _complex c2, _complex *res );
void subc( _complex c1, _complex c2, _complex *res );
void invc( _complex c, _complex *res );
void powc( _complex c, int p, _complex *res );
long double modc( _complex c );

#define _MY_COMPLEX_H
#endif
