#!/bin/sh

src=fractal.dat
dst=clp.pdf

opts=

usage() {

    fractal-draw -h
    exit
}

while [ "`echo \"$1\" | sed -e 's/^\(.\).*$/\1/'`" == '-' ]; do

    [ "$1" == '-h' ] && usage

    [ "$1" == '-j' ] && src=julia.dat

    if [ "$1" == '-c' ]; then
        shift 
        opts="$opts -c $1"
    fi

    if [ "$1" == '-dx' ]; then
        shift 
        opts="$opts -dx $1"
    fi

    if [ "$1" == '-dy' ]; then
        shift 
        opts="$opts -dy $1"
    fi

    if [ "$1" == '-l' ]; then
        opts="$opts --show-runline"
    fi

    if [ "$1" == '-r' ]; then
        shift 
        opts="$opts -r $1"
    fi

    if [ "$1" == '-g' ]; then
        opts="$opts --show-grid"
    fi

    if [ "$1" == '-o' ]; then
        opts="$opts --show-outline"
    fi

    shift

done

[ "$1" ] && src=$1
[ "$2" ] && dst=$2

fractal-draw $opts $src $dst
