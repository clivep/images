#!/usr/bin/sh

echo "%!PS" > composite.ps

multi=17
depth=4
ang1=90
ang2=`perl -e "printf '%f', 90 + 360 / ( $multi * 2 )"`

echo "gsave" >> composite.ps
./apollonian.pl -D -d 4 -r 450 -A $ang1 -N $multi -k clp.ps
cat clp.ps >> composite.ps
echo "1 1 0 setrgbcolor" >> composite.ps
echo "0.000000 0.000000 450.000000 false disc" >> composite.ps
echo "grestore" >> composite.ps

sc=`perl -e 'use Math::Trig; $s = sin( pi / '$multi' ); printf "%f", 450 * ( 1 - $s ) / ( 1 + $s )'`

echo "gsave" >> composite.ps
./apollonian.pl -D -d 3 -A $ang2 -B -r $sc -N $multi -k clp.ps
cat clp.ps >> composite.ps
echo "grestore" >> composite.ps

while [ $depth -gt 0 ]; do

    sc=`perl -e 'use Math::Trig; $s = sin( pi / '$multi' ); printf "%f", '$sc' * ( 1 - $s ) / ( 1 + $s )'`

    echo "gsave" >> composite.ps
    ./apollonian.pl -D -d 2 -A $ang1 -B -r $sc -N $multi -k clp.ps
    cat clp.ps >> composite.ps
    echo "grestore" >> composite.ps

    sc=`perl -e 'use Math::Trig; $s = sin( pi / '$multi' ); printf "%f", '$sc' * ( 1 - $s ) / ( 1 + $s )'`

    echo "gsave" >> composite.ps
    ./apollonian.pl -D -d 2 -A $ang2 -B -r $sc -N $multi -k clp.ps
    cat clp.ps >> composite.ps
    echo "grestore" >> composite.ps

    depth=`expr $depth - 1`
done

convert composite.ps composite.png
# rm composite.ps
