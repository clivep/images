#!/usr/bin/perl

use warnings;
use strict;

use Getopt::Std;
use Math::Trig;
use Data::Dumper;
use Readonly;

my Readonly    $HEXAGON_STATUS_READY        =   "ready";
my Readonly    $HEXAGON_STATUS_IN_PROGRESS  =   "in progress";
my Readonly    $HEXAGON_STATUS_DONE         =   "done";

my Readonly    $TRIANGLE_STATUS_READY       =   "ready";
my Readonly    $TRIANGLE_STATUS_DONE        =   "done";

############################################################
# The global image description

my $IMAGE = {
    'ROWS'          =>  0,
    'COLS'          =>  0,
    'SIZE'          =>  0,

    'TEMP_PS'       =>  '',
    'KEEP_TEMP'     =>  '',
    'DEST_IMAGE'    =>  '',

    'INNER_RAD'     =>  50,
    'OUTER_RAD'     =>  150,

    'VERTICES'      =>  {},
    'HEXAGONS'      =>  {},
    'TRIANGLES'     =>  {},
    'TRAPESIUMS'    =>  {},
};

############################################################
# Color processing

my $COLORS = [
    'col1',   
    'col2',   
    'col3',   
    'col4',   
];

my $COLOR_SET = {
    'body'  => $COLORS->[ 0 ],
    'spots' => $COLORS->[ 1 ],
    'wings' => $COLORS->[ 2 ],
};

############################################################
# Process any command line options

my %options;
getopts( 'dhC:k:Lo:R:S:T:', \%options );

usage() if $options{ 'h' };

$IMAGE->{ 'COLS' } = $options{ 'C' } || 13;
$IMAGE->{ 'ROWS' } = $options{ 'R' } || 5;
$IMAGE->{ 'SIZE' } = $options{ 'S' } || 35;

$IMAGE->{ 'KEEP_TEMP' } = $options{ 'k' };
$IMAGE->{ 'TEMP_PS' } = $options{ 'k' } || '/tmp/butterflies.ps';
$IMAGE->{ 'DEST_IMAGE' } = $options{ 'o' } || 'butterflies.png';

prepareImage();
processImage();
drawImage();

exit;

############################################################
# Process the image

sub processImage {

    processHexagon( 'H-1-1', 'x' );

    printf Dumper $IMAGE if $options{ 'd' };
}

############################################################
# Find the triangle position in the hexagons' triangle list

sub findHexagonTriangle {

    my $hid = shift;
    my $tid = shift;

    my $hex = $IMAGE->{ 'HEXAGONS' }->{ $hid };

    for ( my $t = 0; $t < 6; $t++ ) {

        return $t if ( $hex->{ 'TRIANGLES' }->[ $t ] eq $tid );
    }

    return 0;
}

############################################################
# Process the trapesiums around the centre of the hexagon

sub processHexagon {

    my $hid = shift;
    my $seed_tid = shift;

    my $hex = $IMAGE->{ 'HEXAGONS' }->{ $hid };

    return if $hex->{ 'STATUS' } ne $HEXAGON_STATUS_READY;

    $hex->{ 'STATUS' } = $HEXAGON_STATUS_IN_PROGRESS;

    my $toff = findHexagonTriangle( $hid, $seed_tid );

    for ( my $t = 0; $t < 6; $t++ ) {
       
        my $tid = $hex->{ 'TRIANGLES' }->[ ( $toff + $t ) % 6 ];
        my $triangle = $IMAGE->{ 'TRIANGLES' }->{ $tid };

        for ( my $v = 0; $v < 3; $v++ ) {

            my $thid = $triangle->{ 'HEXAGONS' }->[ $v ];

            next unless $thid;
            next if $thid eq $hid;

            processHexagon( $thid, $tid );
        }
    }

    $IMAGE->{ 'HEXAGONS' }->{ $hid }->{ 'STATUS' } = $HEXAGON_STATUS_DONE;

    return;
}

############################################################
# Return the color not used in the given color set

sub findUnusedColor {

    my $color_set = shift;

    for ( my $unused = 0; $unused < 3; $unused++ ) {
    
        next if $color_set->{ 'body' } eq $COLORS->[ $unused ];
        next if $color_set->{ 'spots' } eq $COLORS->[ $unused ];
        next if $color_set->{ 'wings' } eq $COLORS->[ $unused ];

        return $unused
    }

    return 3;
}

############################################################
# Rotate the colors for the 6-fold transformation

sub colorTransform6 {

    my $color_set = shift;

    my $unused = findUnusedColor ( $color_set );

    return {
        'body'  => $unused,
        'spots' => $color_set->{ 'spots' },
        'wings' => $color_set->{ 'body' },
    }
}

############################################################
# Rotate the colors for the 3-fold transformation

sub colorTransform3 {

    my $color_set = shift;

    my $unused = findUnusedColor ( $color_set );

    return {
        'body'  => $color_set->{ 'spots' },
        'spots' => $color_set->{ 'wings' },
        'wings' => $color_set->{ 'body' },
    }
}

############################################################
# Return the vertex id given a col and row

sub vertexId {

    return sprintf "V-%d-%d", @_;
}

############################################################
# Return the vertex at the given col and row

sub vertexByColRow {

    return $IMAGE->{ 'VERTICES' }-> { vertexId( @_ ) };
}

############################################################
# Return the coords mapped to a disc

sub mapCoords {

    my $x = shift;
    my $y = shift;
return $x, $y;

    my $max_x = 2 * ( $IMAGE->{ 'COLS' } - 1 ) * $IMAGE->{ 'SIZE' };
    my $max_y = ( $IMAGE->{ 'ROWS' } - 1 ) * $IMAGE->{ 'SIZE' } * sqrt( 3.0 );

    my $ang = 360 * $x / $max_x;
    my $rad = ( ( $IMAGE->{ 'OUTER_RAD' } * $y ) + ( $IMAGE->{ 'INNER_RAD' } * ( $max_y - $y ) ) ) / $max_y;

    return $rad * cos( deg2rad( $ang ) ), $rad * sin( deg2rad( $ang ) );
}

############################################################
# Return the vertex coords mapped to a disc

sub mapVertexByIdCoords {

    my $vid = shift;

    my ( $x, $y ) = vertexByIdCoords( $vid );

    return mapCoords( $x, $y );
}

############################################################
# Return the vertex by id and return its' coords as an array

sub vertexByIdCoords {

    my $vid = shift;

    return ( 
        $IMAGE->{ 'VERTICES' }->{ $vid }->{ 'X' } * $IMAGE->{ 'SIZE' },
        $IMAGE->{ 'VERTICES' }->{ $vid }->{ 'Y' } * $IMAGE->{ 'SIZE' },
    );
}

############################################################
# Return a new vertex object

sub newVertex {

    return {
        'X' =>  shift,
        'Y' =>  shift,
    };
}

############################################################
# Setup the image vertices

sub createVertices {

    # Create the vertices
    for ( my $r = 0; $r < $IMAGE->{ 'ROWS' }; $r++ ) {

        for ( my $c = 0; $c < $IMAGE->{ 'COLS' }; $c++ ) {

            my $vid = vertexId( $c, $r );

            $IMAGE->{ 'VERTICES' }->{ $vid } = newVertex( ( $c * 2.0 ) + ( $r % 2 ), $r * sqrt( 3.0 ) );
        }
    }

    return;
}

############################################################
# Setup the image hexagons

sub createHexagons {

    # Find the hexagons
    for ( my $r = 1; $r < $IMAGE->{ 'ROWS' } - 1; $r++ ) {

        for ( my $c = 1; $c < $IMAGE->{ 'COLS' } - 1; $c++ ) {

            my $hid = sprintf "H-%d-%d", $c, $r;
            my $centre = sprintf( "V-%d-%d", $c, $r );

            unless ( $r % 2 ) {

                $IMAGE->{ 'HEXAGONS' }->{ $hid } = {
                    'STATUS'    =>  $HEXAGON_STATUS_READY,
                    'CENTRE'    =>  $centre,
                    'VERTICES'  =>  [
                            vertexId( $c+1, $r ),
                            vertexId( $c, $r+1 ),
                            vertexId( $c-1, $r+1 ),
                            vertexId( $c-1, $r ),
                            vertexId( $c-1, $r-1 ),
                            vertexId( $c, $r-1 ),
                    ],
                };
            }
            else {

                $IMAGE->{ 'HEXAGONS' }->{ $hid } = {
                    'STATUS'    =>  $HEXAGON_STATUS_READY,
                    'CENTRE'    =>  $centre,
                    'VERTICES'  =>  [
                            vertexId( $c+1, $r ),
                            vertexId( $c+1, $r+1 ),
                            vertexId( $c, $r+1 ),
                            vertexId( $c-1, $r ),
                            vertexId( $c, $r-1 ),
                            vertexId( $c+1, $r-1 ),
                    ],
                };
            }
        }
    }

    return;
}

############################################################
# Return a vertex marking the centroid of the given vertices

sub getCentroid {

    my @vlist = @_;

    my $x = 0;
    my $y = 0;
    my $p = 0;

    for my $vid ( @vlist ) {

        my $vertex = $IMAGE->{ 'VERTICES' }->{ $vid };
        $x += $vertex->{ 'X' };
        $y += $vertex->{ 'Y' };
        $p++;
    }

    return {
        'X' =>  $x / $p,
        'Y' =>  $y / $p,
    }
}

############################################################
# Add the triangle if not existing

sub loadTriangle {

    my $hid = shift;
    my $num = shift;
    my @vlist = @_;

    my $tid = sprintf "T-%s-%s-%s", sort( @vlist );

    unless ( $IMAGE->{ 'TRIANGLES' }->{ $tid } ) {

        my $centre = getCentroid( @vlist );
        my $cid = sprintf "TC-%s-%s-%s", sort( @vlist );
        $IMAGE->{ 'VERTICES' }->{ $cid } = $centre;

        $IMAGE->{ 'TRIANGLES' }->{ $tid } = {
            'STATUS'        =>  $TRIANGLE_STATUS_READY,
            'HEX-POS'       =>  $num,
            'VERTICES'      =>  \@vlist,
            'CENTRE'        =>  $cid,
            'HEXAGONS'      =>  [],
            'TRAPESIUMS'    =>  [],
        };
    }

    push @{ $IMAGE->{ 'TRIANGLES' }->{ $tid }->{ 'HEXAGONS' } }, $hid;

    return $tid;
}

############################################################
# Setup the image triangles

sub createTriangles {

    for my $hid ( keys %{ $IMAGE->{ 'HEXAGONS' } } ) {

        my $hex = $IMAGE->{ 'HEXAGONS' }->{ $hid };

        $hex->{ 'TRIANGLES' } = [];

        for ( my $t = 0; $t < 6; $t++ ) {

            my $tid = loadTriangle(
                $hid,
                $t,
                $hex->{ 'CENTRE' },
                $hex->{ 'VERTICES' }->[ $t ],
                $hex->{ 'VERTICES' }->[ ( $t + 1 ) % 6],
            );

            push @{ $hex->{ 'TRIANGLES' } }, $tid;
        }
    }

    return;
}

############################################################
# Setup the image polygons and vertices

sub prepareImage {

    createVertices();
    createHexagons();
    createTriangles();
}

############################################################
# Display the image

sub drawImage {

    my $image_wid = 1000;
    my $image_dep = 1000;

    open my $fh, '>', $IMAGE->{ 'TEMP_PS' } || help( "Could not open " . $IMAGE->{ 'TEMP_PS' } );

    printf $fh "%%!PS\n%%BoundingBox: 0 0 %f %f\n%%EndComments\n

        <<
            /PageSize [ %f %f ]
            /ImagingBBox null
        >> setpagedevice

        /mv { moveto } bind def
        /ln { lineto } bind def
        /rln { rlineto } bind def

        0 0 mv %f 0 ln %f %f ln 0 %f ln fill

        1 1 0 setrgbcolor
        1 setlinejoin
50 200 translate
%%500 500 translate
%%3 3 scale
        /Arial-Bold findfont 15 scalefont setfont 

        /col1 { 1 0 0 setrgbcolor } def
        /col2 { 0 1 0 setrgbcolor } def
        /col3 { 0 0 1 setrgbcolor } def
        /col4 { 0 1 1 setrgbcolor } def
        /setcol { 
            /c exch def
            c 1 eq { col1 } if
            c 2 eq { col2 } if
            c 3 eq { col3 } if
            c 4 eq { col3 } if
        } def

        /trap {
            /sz exch def
            3 1 roll
            gsave translate
            0 0 mv
            rotate

            /c1 exch def
            /c2 exch def
            /c3 exch def

            sz 8 div sz 3 sqrt mul -8 div mv
            sz 3 mul 4 div 0 rln
            sz -8 div sz 3 sqrt mul -8 div rln
            sz -2 div 0 rln
            sz -8 div sz 3 sqrt mul 8 div rln
            c1 setcol fill 

            sz 19 mul 40 div 0 mv
            0 sz 3 sqrt mul -4 div rln
            sz 20 div 0 rln
            0 sz 3 sqrt mul 4 div rln
            sz -20 div 0 rln
            c2 setcol fill

            sz 4 div sz -20 div  mv
            0 sz 3 sqrt mul -13 div rln
            sz 8 div 0 rln
            0 sz 3 sqrt mul 13 div rln
            sz -8 div 0 rln
            c3 setcol fill

            sz 3 mul 4 div sz -20 div  mv
            0 sz 3 sqrt mul -13 div rln
            sz -8 div 0 rln
            0 sz 3 sqrt mul 13 div rln
            sz 8 div 0 rln
            c3 setcol fill

            grestore
        } def
    ",
        $image_wid, $image_dep,
        $image_wid, $image_dep,
        $image_wid, $image_wid,
        $image_dep, $image_dep,
    ;

#    for my $hid ( keys %{ $IMAGE->{ 'HEXAGONS' } } ) {
#        
#        my $hex = $IMAGE->{ 'HEXAGONS' }->{ $hid };
#
#        printf $fh "%f %f mv %f %f ln %f %f ln %f %f ln %f %f ln %f %f ln closepath stroke\n",
#                    mapVertexByIdCoords( $hex->{ 'VERTICES' }->[ 0 ] ),
#                    mapVertexByIdCoords( $hex->{ 'VERTICES' }->[ 1 ] ),
#                    mapVertexByIdCoords( $hex->{ 'VERTICES' }->[ 2 ] ),
#                    mapVertexByIdCoords( $hex->{ 'VERTICES' }->[ 3 ] ),
#                    mapVertexByIdCoords( $hex->{ 'VERTICES' }->[ 4 ] ),
#                    mapVertexByIdCoords( $hex->{ 'VERTICES' }->[ 5 ] ),
#        ;
#    }

    for my $tid ( keys %{ $IMAGE->{ 'TRIANGLES' } } ) {

        my $triangle = $IMAGE->{ 'TRIANGLES' }->{ $tid };
        printf $fh "%f %f mv %f %f ln %f %f ln closepath stroke\n",
            mapVertexByIdCoords( $triangle->{ 'VERTICES' }->[ 0 ] ),
            mapVertexByIdCoords( $triangle->{ 'VERTICES' }->[ 1 ] ),
            mapVertexByIdCoords( $triangle->{ 'VERTICES' }->[ 2 ] ),
        ;
    }

#    my $sz = 4.0 * $IMAGE->{ 'SIZE' } / 3.0;
#
#    for my $tid ( keys %{ $IMAGE->{ 'TRIANGLES' } } ) {
#
#        next if $options{ 'T' } && ( $tid ne $options{ 'T' } );
#
#        my $triangle = $IMAGE->{ 'TRIANGLES' }->{ $tid };
#        my $vertices = $triangle ->{ 'VERTICES' };
#
#        next if $triangle->{ 'STATUS' } eq $TRIANGLE_STATUS_DONE;
#
#        my $ang = $triangle->{ 'HEX-POS' } * 60;
#        printf $fh "1 2 3 %f %f %f %f trap\n", vertexByIdCoords( $vertices->[ 0 ] ), 60 + $ang, $sz;
#        printf $fh "2 3 1 %f %f %f %f trap\n", vertexByIdCoords( $vertices->[ 1 ] ), 180 + $ang, $sz;
#        printf $fh "3 1 2 %f %f %f %f trap\n", vertexByIdCoords( $vertices->[ 2 ] ),  300 + $ang, $sz;
#
#        $triangle->{ 'STATUS' } = $TRIANGLE_STATUS_DONE;
#    }
    
#    if ( $options{ 'L' } ) {
#
#        for my $tid ( keys %{ $IMAGE->{ 'TRIANGLES' } } ) {
#
#            my $triangle = $IMAGE->{ 'TRIANGLES' }->{ $tid };
#            printf $fh "gsave %f %f moveto \n", 
#                vertexByIdCoords( $triangle->{ 'CENTRE' } );
#    
#            my $id = $tid;
#            $id =~ s/-//g;
#
#            printf $fh "%% %s\n", $id;
#            printf $fh "1 setgray (%s) dup stringwidth pop -2 div 0 rln show grestore\n", $id; 
#        }
#    }

    close $fh;

    system( sprintf "convert %s %s", $IMAGE->{ 'TEMP_PS' }, $IMAGE->{ 'DEST_IMAGE' } );

    unlink $IMAGE->{ 'TEMP_PS' } unless $IMAGE->{ 'KEEP_TEMP' };

    return;
}

############################################################
# Some usage info

sub usage {

    print "\n@_\n" if @_;

    printf "\n$0 " .
        "[-C <cols>] [-d] [-h] [-k <tmp>] [-L] [-o <dest>] [-R <rows>] [-S <sz>] [-T <tid>]

    where
        -d          dump image data
        -h          this help message
        -k <tmp>    temp file for the postscript (retained)
        -o <dest>   location for image (default butterflies.png)

    image control params

        -C <cols>   number of hexagon cols in the image (default 10)
        -L          label the triangles
        -R <rows>   number of hexagon rows in the image (default 5)
        -S <size>   size of the hexagon sides (defautl 35)
        -T <tid>    only draw this triangle
    \n";

    exit 0;
}

__END__
