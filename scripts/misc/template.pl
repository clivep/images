#!/usr/bin/perl

use warnings;
use strict;

############################################################
#
#   template.pl
#
#   Template for image making
#
############################################################

use Math::Trig;
use Getopt::Std;

use Data::Dumper;

############################################################
# Setup the environment

my $IMAGE = {};

############################################################
# Check the runline options

my %options;
getopts( 'hD:F:K:S:W:', \%options );

help() if $options{ 'h' };

$IMAGE->{ 'TEMP_PS' } = $options{ 'K' } || 'template.ps';
$IMAGE->{ 'DEST' } = $options{ 'F' } || 'template.png';

$IMAGE->{ 'SCALE' } = $options{ 'S' } || 1;
$IMAGE->{ 'DEPTH' } = $options{ 'W' } || 1000;
$IMAGE->{ 'WIDTH' } = $options{ 'D' } || 1000;

############################################################
# Process the image

processImage();
drawImage();

exit;

############################################################
# Process the image

sub processImage {

}

############################################################
# Write the image header

sub writeImageHeader {

    my $fh = shift;

    printf $fh "%%!PS\n%%%%BoundingBox: 0 0 %f %f\n%%%%EndComments\n

        /imgw %f def
        /imgd %f def

        <<
            /PageSize [ imgw imgd ]
            /ImagingBBox null
        >> setpagedevice

        /mv { moveto } bind def
        /ln { lineto } bind def

        0 0 mv imgw 0 ln imgw imgd ln 0 imgd ln fill

        imgw 2 div imgd 2 div translate

        1 setlinejoin
        1 1 0 setrgbcolor
    ",
        $IMAGE->{ 'WIDTH' }, $IMAGE->{ 'DEPTH' },
        $IMAGE->{ 'WIDTH' }, $IMAGE->{ 'DEPTH' },
    ;

    return;
}

############################################################
# Write the image details

sub writeImageDetail {

    my $fh = shift;

    return;
}

############################################################
# Draw the image

sub drawImage {

    open my $fh, '>', $IMAGE->{ 'TEMP_PS' } or do {
        printf "Could not open %s: $!\n", $IMAGE->{ 'TEMP_PS' };
        exit -1;
    };

    writeImageHeader( $fh );
    writeImageDetail( $fh );

    close $fh;

    my $convert = "/usr/local/bin/convert";
    $convert = "/usr/bin/convert" unless ( -f $convert );
    $convert = "/Applications/ImageMagick/ImageMagick-6.6.5/bin/convert" unless ( -f $convert );

    unless ( -f $convert ) {

        printf "Could not locate convert at $convert\n";
    }
    else {

        unlink $IMAGE->{ 'DEST' };
        my $cmd = sprintf "$convert '%s' '%s'", $IMAGE->{ 'TEMP_PS' }, $IMAGE->{ 'DEST' };
        system( $cmd );
    }

    unlink $IMAGE->{ 'TEMP_PS' } unless $options{ 'K' };

    return;
}

############################################################
# Help doco

sub help {

    print "\n@_\n" if @_;

    printf "\n$0 " .
                "[-h] [-D <depth>] [-F <file>] [-K <file>] [-S <scale>] [-W <width>]

        where
            -h          this help message

            -D <depth>  image depth (default 1000)
            -F <file>   output file (default 'template.png')
            -K <file>   temp ps file to retain
            -S <scale>  drawing scale (default 1)
            -W <width>  image width (default 1000)

    \n";

    exit;
}
