#include <stdio.h>
#include <stdlib.h>

#include "tiling-polygons.h"
#include "penrose.h"

/*******************************************************************************
 *
 * penrose.c
 *
 *  Functions for generating Penrose tilings
 *
 ******************************************************************************/

#define DEFAULT_PDF_PATH    "penrose.pdf"
#define RENDER_SCRIPT       "penrose-draw.pl"

static _list   *templates;

/*************************************************
 * Initialise the plugin
 ************************************************/

void initialisePlugin(
    _logging_level   logging_level   
) {
    
    _polygon *template;
    _list_item  *item;

    initialiseLogging( NULL, logging_level );

    templates = initialiseList( releasePolygon );

    addToList( initialiseDartTemplate(), templates );
    addToList( initialiseKiteTemplate(), templates );

    for( item = templates->first; item != NULL; item = item->next ) {

        template = item->data;

        template->base_angle = atan2 (
                template->vertices[ 1 ]->y - template->vertices[ 0 ]->y,
                template->vertices[ 1 ]->x - template->vertices[ 0 ]->x
        );
    }
}

/*************************************************
 * Cleanup the plugin
 ************************************************/

void cleanupPlugin() {

    releaseList( templates );
    cleanupLogging();
}

/*************************************************
 * Return a template by its list position
 ************************************************/

_polygon *getTemplateByPos(
    short pos
) {
    return getFromListByPos( pos, templates );
}
/*************************************************
 * Return the number of template polygons
 ************************************************/

short getTemplateCount() {
    return templates->count;
}

/*************************************************
 * Render the image to the given file
 ************************************************/

void drawTiling(
    char    *json_path,
    double  exclusion_radius,
    char    *pdf_path
) {
    char    buf[100];

    if ( pdf_path == NULL )
        pdf_path = DEFAULT_PDF_PATH;

    sprintf( buf, "%s -i -x %f %s %s", RENDER_SCRIPT,
                    exclusion_radius, json_path, pdf_path );

    system( buf );
}

/*************************************************
 * Return the number of edges for the type
 ************************************************/

short polygonTypeEdges(
    _template_type type 
) {
    switch( type ) {

        case TEMPLATE_TYPE_DART:
            return 4;

        case TEMPLATE_TYPE_KITE:
            return 4;
    }

    return -1;
}

/*************************************************
 * Initialise the DART template polygon
 ************************************************/

_polygon *initialiseDartTemplate() {

    _polygon    *template;

    double      ang = 2.0 * PI / 5.0;
    double      long_side = 1.0 + PHI;
    double      short_side = 1.0 + 1.0 / PHI;

    template = newPolygon( TEMPLATE_TYPE_DART );

    template->sides[ 0 ] = newSide( SIDE_TYPE_SHORT_A, template );
    template->sides[ 1 ] = newSide( SIDE_TYPE_LONG_A, template );
    template->sides[ 2 ] = newSide( SIDE_TYPE_LONG_B, template );
    template->sides[ 3 ] = newSide( SIDE_TYPE_SHORT_B, template );

    template->angles[ 0 ] = 3.0 * ang;
    template->angles[ 1 ] = ang / 2.0;
    template->angles[ 2 ] = ang;
    template->angles[ 3 ] = ang / 2.0;

    template->vertices[ 0 ] = newVertex( 0.0, 0.0, template->angles[ 0 ] );
    template->vertices[ 1 ] = newVertex( short_side, 0, template->angles[ 1 ] );
    template->vertices[ 2 ] = newVertex( short_side - long_side * cos( ang / 2.0 ),
                                         long_side * sin( ang / 2.0 ),
                                         template->angles[ 2 ] );
    template->vertices[ 3 ] = newVertex( short_side * cos( 3.0 * ang ),
                                         short_side * sin( 3.0 * ang ),
                                         template->angles[ 3 ] );

    offsetPolygon( template,
                        long_side * cos( ang / 2.0 ) - short_side,
                        -1 * long_side * sin( ang / 2.0 ) 
    );

    logMsg( "Dart template @ %lx", (long)template );

    return template;
}

/*************************************************
 * Initialise a KITE template polygon
 ************************************************/

_polygon *initialiseKiteTemplate() {

    _polygon    *template;

    double      ang = 2.0 * PI / 5.0;
    double      long_side = 1.0 + PHI;
    double      short_side = 1.0 + 1.0 / PHI;

    template = newPolygon( TEMPLATE_TYPE_KITE );

    template->sides[ 0 ] = newSide( SIDE_TYPE_LONG_A, template );
    template->sides[ 1 ] = newSide( SIDE_TYPE_SHORT_A, template );
    template->sides[ 2 ] = newSide( SIDE_TYPE_SHORT_B, template );
    template->sides[ 3 ] = newSide( SIDE_TYPE_LONG_B, template );

    template->angles[ 0 ] = ang;
    template->angles[ 1 ] = ang;
    template->angles[ 2 ] = 2.0 * ang;
    template->angles[ 3 ] = ang;

    template->vertices[ 0 ] = newVertex( 0.0, 0.0, ang );
    template->vertices[ 1 ] = newVertex( long_side, 0, ang );
    template->vertices[ 2 ] = newVertex( long_side - short_side * cos( ang ),
                                                                short_side * sin( ang ), 2 * ang );
    template->vertices[ 3 ] = newVertex( long_side * cos( ang ),
                                                                long_side * sin( ang ), ang );

    logMsg( "Kite template @ %lx", (long)template );

    return template;
}

/*************************************************
 * Return a template from the list by type
 ************************************************/

_polygon *getTemplateByType(
    _template_type   type
) {

    _polygon    *template;
    _list_item  *item;

    for( item = templates->first; item != NULL; item = item->next ) {

        template = item->data;
        if ( template->type == type )
            return template;
    }

    return NULL;
}

/*************************************************
 * Return the char id for the polygon type
 ************************************************/

char polygonTypeChar(
    _polygon *polygon
) {
    switch( polygon->type ) {

        case TEMPLATE_TYPE_DART:
            return 'D';

        case TEMPLATE_TYPE_KITE:
            return 'K';
    }

    return '?';
}

/*************************************************
 * Find the pos of the type in the template list
 ************************************************/

short findTemplateTypePos(
    _template_type  type
) {
    _polygon    *template;
    short       pos;

    for( pos = 0; pos < templates->count; pos++ ) {

        template = getFromListByPos( pos, templates );
        if ( template->type == type )
            return pos;
    }

    return -1;
}

/*************************************************
 * Return the number of side types available
 ************************************************/

int getSideTypeCount() {

    return SIDE_TYPE_COUNT;
}

/*************************************************
 * Return a description of the given side type
 ************************************************/

char *getSideTypeDescription(
    _side_type   pos
) {

    switch( pos ) {

        case SIDE_TYPE_LONG_A:
            return "LONG SIDE - PHI + 1";
        case SIDE_TYPE_LONG_B:
            return "LONG SIDE - 1 + PHI";
        case SIDE_TYPE_SHORT_A:
            return "SHORT SIDE - PHI + 1/PHI";
        case SIDE_TYPE_SHORT_B:
            return "SHORT SIDE - 1/PHI + PHI";
        default:
            break;
    }

    return "?";
}

/*************************************************
 * Test if the sides are allowed to join
 ************************************************/

short checkForValidSides(
    _polygon    *polygon1,      // the existing polygon
    short       edge1,
    _polygon    *polygon2,      // the candidate (template) polygon
    short       edge2
) {
    _side_type  side_type1;
    _side_type  side_type2;

    // Check for a valid combination of sides
    side_type1 = polygon1->sides[ edge1 ]->type;
    side_type2 = polygon2->sides[ edge2 ]->type;

    logMsg( "Checking sides %lx/%d with %lx/%d",
                                        (long)polygon1, edge1,
                                        (long)polygon2, edge2 
    );

    if (
        ( ( side_type1 == SIDE_TYPE_LONG_A ) && ( side_type2 == SIDE_TYPE_LONG_B ) ) ||
        ( ( side_type1 == SIDE_TYPE_LONG_B ) && ( side_type2 == SIDE_TYPE_LONG_A ) ) 
    ) 
        return OK;

    if (
        ( ( side_type1 == SIDE_TYPE_SHORT_A ) && ( side_type2 == SIDE_TYPE_SHORT_B ) ) ||
        ( ( side_type1 == SIDE_TYPE_SHORT_B ) && ( side_type2 == SIDE_TYPE_SHORT_A ) )
    ) { 
        // Short side matching not allowed for 2 darts
        if (
            ( polygon1->type == TEMPLATE_TYPE_KITE ) ||
            ( polygon2->type == TEMPLATE_TYPE_KITE )
        )
            return OK;
    }

    logMsg( "rejected - bad side combination %lx: (%c/%d) and %lx: (%c/%d)", 
                                        (long)polygon1, polygonTypeChar( polygon1 ), edge1,
                                        (long)polygon2, polygonTypeChar( polygon2 ), edge2
    );

    return NOK;
}
