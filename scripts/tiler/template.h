/*******************************************************************************
 *
 * template.h
 *
 *  Definitions required for tiling plugins
 *
 ******************************************************************************/

#include "tiling-geometry.h"
#include "tiling-utils.h"

/*************************************************
 * Template types
 ************************************************/

typedef enum {

    TEMPLATE_TYPE_????

} _template_type;

/*************************************************
 * Polygon side type values
 ************************************************/

typedef enum {

    SIDE_TYPE_????,

    SIDE_TYPE_COUNT

} _side_type;

/*************************************************
 * Prototypes for the exposed functions
 ************************************************/

void initialisePlugin();
short getTemplateCount();
short polygonTypeEdges( _template_type type );
_polygon *getTemplateByType( _template_type type );
_polygon *getTemplateByPos( short pos );
char polygonTypeChar( _polygon *polygon );
short findTemplateTypePos( _template_type type );
char *getSideTypeDescription( _side_type pos );
int getSideTypeCount();
short checkForValidSides( _polygon *polygon1, short edge1,
                                _polygon *polygon2, short edge2 );

/*************************************************
 * Prototypes for internal functions
 ************************************************/

_polygon *initialise????Template();
