#!/usr/bin/perl

use warnings;
use strict;

use Math::Trig;
use File::Path qw(make_path);
use File::Temp qw/ tempfile tempdir /;
use Getopt::Std;

use Data::Dumper;

############################################################
#
#   p1-tilting.pl
#
#   Model the Penrose P1 tiling
#
############################################################

use constant    TRUE                        =>  1;
use constant    FALSE                       =>  0;

use constant    IMAGE_WIDTH                 =>  1000;
use constant    IMAGE_DEPTH                 =>  1000;

use constant    POLYGON_TYPE_PENTAGON       =>  'pentagon';
use constant    POLYGON_TYPE_BOAT           =>  'boat';
use constant    POLYGON_TYPE_DIAMOND        =>  'diamond';
use constant    POLYGON_TYPE_OPEN_DIAMOND   =>  'open-diamond';
use constant    POLYGON_TYPE_STAR           =>  'star';

use constant    PHI                         =>  ( 1.0 + sqrt( 5.0 ) ) / 2.0;

use constant    SMALL_DIST                  =>  1.0e-10;

############################################################
# Check the runline options

my %options;
getopts( 'a:c:dD:f:F:hi:k:lpr:s:S:tW:', \%options );

help() if $options{ 'h' };

my $TEXT_MULTIPLIER = $options{ 'a' } || 1.0;
my $CENTER_POLYGON = $options{ 'c' };
my $DRAWING_SCALE = 50 * ( $options{ 's' } || 1 );
my $INVERT_DIAMONDS = $options{ 'd' };
my $ITERATIONS = $options{ 'i' } || 1;
my $LABEL_POLYGONS = $options{ 'l' };
my $FILL_POLYGON = $options{ 'f' };
my $PS_TEMP = $options{ 'k' } || 'penrose.ps';
my $DUMP_DATA = $options{ 'p' };
my $DISPLAY_RADIUS = $options{ 'r' };
my $LEAVE_PS_TEMP = $options{ 't' };

my $IMAGE_DEPTH = $options{ 'D' } || IMAGE_DEPTH;
my $IMAGE_WIDTH = $options{ 'W' } || IMAGE_WIDTH;
my $OUTPUT_FILE = $options{ 'F' } || 'tiling.pdf';
my $DRAWING_SCHEME = $options{ 'S' } || 'line';

help( "$DRAWING_SCHEME is not a supported drawing scheme" ) if ( 
    ( $DRAWING_SCHEME ne 'line' ) &&
    ( $DRAWING_SCHEME ne 'fill' )
);

############################################################
# Setup the environment

$| = 1;

my $IMAGE_SIZE = [ $IMAGE_WIDTH, $IMAGE_DEPTH ];
my $DRAWING_OFFSET = [ 0, 0 ];

my $POLYGON_LOOKUP = {};

my $POLYGONS = [];
my $NEXT_POLYGON_ID = 1;

setupInitalPolygons();

############################################################
# Process the tiling

my $CURRENT_ITERATION = 0;

while ( $CURRENT_ITERATION < $ITERATIONS ) {

    # Make 2 passes trhough the polygons - firstly split out pentagons and
    # consolidate any half diamonds. Then porcess the more complex figures

    my $new_polygons = [];
    for ( @{ $POLYGONS } ) {

          isPentagon( $_ )  ? subdividePentagon( $_, $new_polygons )
          :                   push @{ $new_polygons }, $_
          ;
    }

    $POLYGONS = consolidateDiamonds( $new_polygons );

    $new_polygons = [];

    for ( @{ $POLYGONS } ) {
        isStar( $_ )                ? subdivideStar( $_, $new_polygons )
        : isDiamond( $_ )           ? subdivideDiamond( $_, $new_polygons )
        : isBoat( $_ )              ? subdivideBoat( $_, $new_polygons )
        : ! $_->{ 'CONSOLIDATED' }  ? push @{ $new_polygons }, $_
        :                             noop()
        ;
    }

    $POLYGONS = $new_polygons;
    
    $CURRENT_ITERATION++;
}

drawImage();

dumpPolygons() if $DUMP_DATA;

exit;

############################################################
# Simple mod/min/max functions

sub isPentagon { return shift->{ 'TYPE' } eq POLYGON_TYPE_PENTAGON }
sub isBoat { return shift->{ 'TYPE' } eq POLYGON_TYPE_BOAT }
sub isDiamond { return shift->{ 'TYPE' } eq POLYGON_TYPE_DIAMOND }
sub isOpenDiamond { return shift->{ 'TYPE' } eq POLYGON_TYPE_OPEN_DIAMOND }
sub isStar { return shift->{ 'TYPE' } eq POLYGON_TYPE_STAR }

sub notConsolidated { return ! shift->{ 'CONSOLIDATED' } }
   
############################################################
# Return the number of polygons that share the vertx

sub vertexPolygons {

    my $polygon = shift;
    my $vertex_no = shift;

    return keys %{ $polygon->{ 'VERTICES' }->[ $vertex_no ]->{ 'POLYGONS' } };
}

############################################################
# Rejoin any open diamonds

sub consolidateDiamonds {

    my $polygon_list = shift;

    my $new_list = [];
    my $open_diamonds = [];

    for ( @{ $polygon_list } ) {

        if (
            isOpenDiamond( $_ ) 
        ) {

            push @{ $open_diamonds }, $_;
        } 
        else {

            push @{ $new_list }, $_;
        }
    }

    push @{ $new_list }, checkBoundaryDiamonds( $open_diamonds );
    push @{ $new_list }, consolidateOpenDiamonds( $open_diamonds );

    return $new_list;
}

############################################################
# Find common polygon at a vertex - with exclusions

sub findCommonVertexPolygon {

    my $polygon = shift;
    my $vertex_no = shift;

    my $exclusions = { $polygon->{ 'ID' } => 1 };

    while ( my $exclusion = shift ) {
        $exclusions->{ $exclusion } = 1;
    }

    for ( keys %{ $polygon->{ 'VERTICES' }->[ $vertex_no ]->{ 'POLYGONS' } } ) {

        return $_ unless ( $exclusions->{ $_ } );
    }

    return -1;
}

############################################################
# Check any 2nd generation open diamonds on the boundary

sub checkBoundaryDiamonds {

    my $open_diamonds = shift;

    my $new_list = [];

    for ( my $p1 = 0; $p1 < scalar @{ $open_diamonds }; $p1++ ) {

        my $polygon = $open_diamonds->[ $p1 ];

        next if $polygon->{ 'CONSOLIDATED' };
        next if ( $CURRENT_ITERATION - $polygon->{ 'CREATED' } ) != 1;

        my $leg_pentagon1 = findCommonVertexPolygon( $polygon, 1 );
        fatal( "checkBoundaryDiamonds() failure" ) if ( $leg_pentagon1 < 0 );

        my $polygon1 = $POLYGON_LOOKUP->{ $leg_pentagon1 };
        my $leg_id1 = findCommonVertexPolygon( $polygon1, 2 );
        my $leg_diamond1 = $POLYGON_LOOKUP->{ $leg_id1 };

        my $leg_pentagon2 = findCommonVertexPolygon( $polygon, 2 );
        fatal( "checkBoundaryDiamonds() failure" ) if ( $leg_pentagon2 < 0 );

        my $polygon2 = $POLYGON_LOOKUP->{ $leg_pentagon2 };
        my $leg_id2 = findCommonVertexPolygon( $polygon2, 4 );
        my $leg_diamond2 = $POLYGON_LOOKUP->{ $leg_id2 };

        my $new_vertex = findIntersection(
                            $polygon1->{ 'VERTICES' }->[ 1 ], $polygon1->{ 'VERTICES' }->[ 2 ],
                            $polygon2->{ 'VERTICES' }->[ 0 ], $polygon2->{ 'VERTICES' }->[ 4 ],
        );

        my $new_polygon = {

                        'ID'        => $NEXT_POLYGON_ID++,
                        'TYPE'      => POLYGON_TYPE_STAR,
                        'PARENT'    => $polygon->{ 'ID' },
                        'CREATED'   => $CURRENT_ITERATION,
                        'VERTICES'  => [
                            $polygon->{ 'VERTICES' }->[ 0 ],
                            $leg_diamond1->{ 'VERTICES' }->[ 1 ],
                            $leg_diamond1->{ 'VERTICES' }->[ 0 ],
                            $leg_diamond1->{ 'VERTICES' }->[ 2 ],
                            $polygon1->{ 'VERTICES' }->[ 3 ],
                            $new_vertex,
                            $polygon2->{ 'VERTICES' }->[ 3 ],
                            $leg_diamond2->{ 'VERTICES' }->[ 1 ],
                            $leg_diamond2->{ 'VERTICES' }->[ 0 ],
                            $leg_diamond2->{ 'VERTICES' }->[ 2 ],
                        ],
        };

        delVertexPolygons( $new_polygon, $polygon->{ 'ID' } );
        delVertexPolygons( $new_polygon, $leg_id1 );
        delVertexPolygons( $new_polygon, $leg_id2 );

        addVertexPolygons( $new_polygon, $new_polygon->{ 'ID' } );

        push @{ $new_list }, $new_polygon;

        $POLYGON_LOOKUP->{ $new_polygon->{ 'ID' } } = $new_polygon;

        $polygon->{ 'CONSOLIDATED' } = 1;
        $leg_diamond1->{ 'CONSOLIDATED' } = 1;
        $leg_diamond2->{ 'CONSOLIDATED' } = 1;
    }

    return @{ $new_list };
}

############################################################
# Rejoin any open diamonds

sub consolidateOpenDiamonds {

    my $open_diamonds = shift;

    my $new_list = [];

    for ( my $p1 = 0; $p1 < scalar @{ $open_diamonds }; $p1++ ) {

        my $polygon1 = $open_diamonds->[ $p1 ];

        next if $polygon1->{ 'CONSOLIDATED' };

        for ( my $p2 = $p1 + 1; $p2 < scalar @{ $open_diamonds }; $p2++ ) {

            next if ( $p1 == $p2 );

            my $polygon2 = $open_diamonds->[ $p2 ];

            next if $polygon2->{ 'CONSOLIDATED' };

            my $v1 = findVertexNumber( $polygon2, $polygon1->{ 'VERTICES' }->[ 1 ] );
            next if $v1 < 0;

            my $v2 = findVertexNumber( $polygon2, $polygon1->{ 'VERTICES' }->[ 2 ] );
            next if $v2 < 0;

            my $idx = $polygon1->{ 'VERTICES' }->[ 1 ] == $polygon2->{ 'VERTICES' }->[ 2 ] ? 1 : 2;

            my $new_polygon = {

                        'ID'        => $NEXT_POLYGON_ID++,
                        'TYPE'      => POLYGON_TYPE_DIAMOND,
                        'PARENT'    => $polygon1->{ 'ID' },
                        'CREATED'   => $CURRENT_ITERATION,
                        'VERTICES'  => [
                            $polygon1->{ 'VERTICES' }->[ 0 ],
                            $polygon1->{ 'VERTICES' }->[ 1 ],
                            $polygon2->{ 'VERTICES' }->[ 0 ],
                            $polygon2->{ 'VERTICES' }->[ $idx ],
                        ],
            };

            $polygon1->{ 'CONSOLIDATED' } = 1;
            $polygon2->{ 'CONSOLIDATED' } = 1;

            delVertexPolygons( $new_polygon, $polygon1->{ 'ID' } );
            delVertexPolygons( $new_polygon, $polygon2->{ 'ID' } );
            addVertexPolygons( $new_polygon, $new_polygon->{ 'ID' } );

            $POLYGON_LOOKUP->{ $new_polygon->{ 'ID' } } = $new_polygon;

            push @{ $new_list }, $new_polygon;

            last;
        }

        # If the polygon was not consolidated - push it back onto the list
        push @{ $new_list }, $polygon1 unless $polygon1->{ 'CONSOLIDATED' };
    }

    return @{ $new_list };
}

############################################################
# Find the distance between 2 vertices

sub distance {

    my $vtx1 = shift;
    my $vtx2 = shift;

    my $dx = $vtx1->{ 'X' } - $vtx2->{ 'X' };
    my $dy = $vtx1->{ 'Y' } - $vtx2->{ 'Y' };

    return sqrt( ( $dx * $dx ) +  ( $dy * $dy ) );
}

############################################################
# Find a point the given proportion between 2 points

sub pointAlong {

    my $vertex1 = shift;
    my $vertex2 = shift;
    my $val = shift;

    return newVertex(
            ( ( 1.0 - $val ) * $vertex1->{ 'X' } ) + ( $val * $vertex2->{ 'X' } ),
            ( ( 1.0 - $val ) * $vertex1->{ 'Y' } ) + ( $val * $vertex2->{ 'Y' } ),
    );
}

############################################################
# Add the polygons' id to each vertex polygon list

sub addVertexPolygons {

    my $polygon = shift;

    for my $vertex ( @{ $polygon->{ 'VERTICES' } } ) {

        $vertex->{ 'POLYGONS' }->{ $polygon->{ 'ID' } } = 1;
    }

    return;
}

############################################################
# Remove the id from each vertex polygon list

sub delVertexPolygons {

    my $polygon = shift;
    my $id = shift;

    for my $vertex ( @{ $polygon->{ 'VERTICES' } } ) {

        delete $vertex->{ 'POLYGONS' }->{ $id };
    }

    return;
}

############################################################
# Find the vertex number in the given polygon

sub findVertexNumber {

    my $polygon = shift;
    my $vertex = shift;

    for ( my $v = 0; $v < scalar @{ $polygon->{ 'VERTICES' } }; $v++ ) {

        return $v if ( $vertex == $polygon->{ 'VERTICES' }->[ $v ] );
    }

    return -1;
}

############################################################
# Are the 2 polygon vertices shared?

sub checkSharedVertex {

    my $polygon1 = shift;
    my $vertex_no1 = shift;
    my $polygon2 = shift;
    my $vertex_no2 = shift;

    my $vertex1 = $polygon1->{ 'VERTICES' }->[ $vertex_no1 ];
    my $vertex2 = $polygon2->{ 'VERTICES' }->[ $vertex_no2 ];

    my $dist = distance( $vertex1, $vertex2 );
    
    return 0 if ( $dist > SMALL_DIST );
    
    # Add the common polygons from vertex2 into vertex1
    for ( keys %{ $vertex2->{ 'POLYGONS' } } ) {

        $vertex1->{ 'POLYGONS' }->{ $_ } = 1;

        my $polygon = $POLYGON_LOOKUP->{ $_ };
        my $v = findVertexNumber( $polygon, $vertex2 );
        $polygon->{ 'VERTICES' }->[ $v ] = $vertex1;
    }

    $polygon2->{ 'VERTICES' }->[ $vertex_no2 ] = $vertex1;

    return 1;
}

############################################################
# Build star/pentagon/boat in a diamond starting at vtx 0

sub subDivideDiamondStyle0 {

    my $polygon = shift;
    my $new_polygons = shift;
    my $side_diamonds = shift;

    my $vertices = $polygon->{ 'VERTICES' };

    my $new_vertex = pointAlong( $vertices->[ 2 ], $vertices->[ 0 ], 1 / PHI );

    my $v1 = closerVertex( $vertices->[ 0 ], $side_diamonds->[ 0 ], 1, 2 );
    my $v2 = closerVertex( $vertices->[ 0 ], $side_diamonds->[ 3 ], 1, 2 );

    my $new_star = {

            'ID'        => $NEXT_POLYGON_ID++,
            'TYPE'      => POLYGON_TYPE_STAR,
            'PARENT'    => $polygon->{ 'ID' },
            'CREATED'   => $CURRENT_ITERATION,
            'VERTICES'  => [
                $polygon->{ 'VERTICES' }->[ 0 ],
                $side_diamonds->[ 0 ]->{ 'VERTICES' }->[ $v1 ],
                $side_diamonds->[ 0 ]->{ 'VERTICES' }->[ 0 ],
                $side_diamonds->[ 0 ]->{ 'VERTICES' }->[ 3 - $v1 ],
                $polygon->{ 'VERTICES' }->[ 1 ],
                $new_vertex,
                $polygon->{ 'VERTICES' }->[ 3 ],
                $side_diamonds->[ 3 ]->{ 'VERTICES' }->[ 3 - $v2 ],
                $side_diamonds->[ 3 ]->{ 'VERTICES' }->[ 0 ],
                $side_diamonds->[ 3 ]->{ 'VERTICES' }->[ $v2 ],
            ],
    };

    addVertexPolygons( $new_star );
    delVertexPolygons( $new_star, $polygon->{ 'ID' } );
    delVertexPolygons( $new_star, $side_diamonds->[ 0 ]->{ 'ID' } );
    delVertexPolygons( $new_star, $side_diamonds->[ 3 ]->{ 'ID' } );

    $side_diamonds->[ 0 ]->{ 'CONSOLIDATED' } = 1;
    $side_diamonds->[ 3 ]->{ 'CONSOLIDATED' } = 1;

    push @{ $new_polygons }, $new_star;

    $POLYGON_LOOKUP->{ $new_star->{ 'ID' } } = $new_star;

    $v1 = closerVertex( $vertices->[ 2 ], $side_diamonds->[ 1 ], 1, 2 );
    $v2 = closerVertex( $vertices->[ 2 ], $side_diamonds->[ 2 ], 1, 2 );

    my $new_pentagon = {

            'ID'        => $NEXT_POLYGON_ID++,
            'TYPE'      => POLYGON_TYPE_PENTAGON,
            'PARENT'    => $polygon->{ 'ID' },
            'CREATED'   => $CURRENT_ITERATION,
            'VERTICES'  => [
                $new_vertex,
                $polygon->{ 'VERTICES' }->[ 1 ],
                $side_diamonds->[ 1 ]->{ 'VERTICES' }->[ 3 - $v1 ],
                $side_diamonds->[ 2 ]->{ 'VERTICES' }->[ 3 - $v2 ],
                $polygon->{ 'VERTICES' }->[ 3 ],
            ],
    };

    addVertexPolygons( $new_pentagon );
    delVertexPolygons( $new_pentagon, $polygon->{ 'ID' } );
    delVertexPolygons( $new_pentagon, $side_diamonds->[ 1 ]->{ 'ID' } );
    delVertexPolygons( $new_pentagon, $side_diamonds->[ 2 ]->{ 'ID' } );

    push @{ $new_polygons }, $new_pentagon;

    $POLYGON_LOOKUP->{ $new_pentagon->{ 'ID' } } = $new_pentagon;

    my $new_boat = {

            'ID'        => $NEXT_POLYGON_ID++,
            'TYPE'      => POLYGON_TYPE_BOAT,
            'PARENT'    => $polygon->{ 'ID' },
            'CREATED'   => $CURRENT_ITERATION,
            'VERTICES'  => [
                $vertices->[ 2 ],
                $side_diamonds->[ 1 ]->{ 'VERTICES' }->[ $v1 ],
                $side_diamonds->[ 1 ]->{ 'VERTICES' }->[ 0 ],
                $side_diamonds->[ 1 ]->{ 'VERTICES' }->[ 3 - $v1 ],
                $side_diamonds->[ 2 ]->{ 'VERTICES' }->[ 3 - $v2 ],
                $side_diamonds->[ 2 ]->{ 'VERTICES' }->[ 0 ],
                $side_diamonds->[ 2 ]->{ 'VERTICES' }->[ $v2 ],
            ],
    };

    addVertexPolygons( $new_boat );
    delVertexPolygons( $new_boat, $polygon->{ 'ID' } );
    delVertexPolygons( $new_boat, $side_diamonds->[ 1 ]->{ 'ID' } );
    delVertexPolygons( $new_boat, $side_diamonds->[ 2 ]->{ 'ID' } );

    $side_diamonds->[ 1 ]->{ 'CONSOLIDATED' } = 1;
    $side_diamonds->[ 2 ]->{ 'CONSOLIDATED' } = 1;

    push @{ $new_polygons }, $new_boat;

    $POLYGON_LOOKUP->{ $new_boat->{ 'ID' } } = $new_boat;

    return;
}

############################################################
# Build star/pentagon/boat in a diamond starting at vtx 2

sub subDivideDiamondStyle2 {

    my $polygon = shift;
    my $new_polygons = shift;
    my $side_diamonds = shift;

    my $vertices = $polygon->{ 'VERTICES' };

    my $new_vertex = pointAlong( $vertices->[ 0 ], $vertices->[ 2 ], 1 / PHI );

    my $v1 = closerVertex( $vertices->[ 2 ], $side_diamonds->[ 1 ], 1, 2 );
    my $v2 = closerVertex( $vertices->[ 2 ], $side_diamonds->[ 2 ], 1, 2 );

    my $new_star = {

            'ID'        => $NEXT_POLYGON_ID++,
            'TYPE'      => POLYGON_TYPE_STAR,
            'PARENT'    => $polygon->{ 'ID' },
            'CREATED'   => $CURRENT_ITERATION,
            'VERTICES'  => [
                $polygon->{ 'VERTICES' }->[ 2 ],
                $side_diamonds->[ 2 ]->{ 'VERTICES' }->[ $v2 ],
                $side_diamonds->[ 2 ]->{ 'VERTICES' }->[ 0 ],
                $side_diamonds->[ 2 ]->{ 'VERTICES' }->[ 3 - $v2 ],
                $polygon->{ 'VERTICES' }->[ 3 ],
                $new_vertex,
                $polygon->{ 'VERTICES' }->[ 1 ],
                $side_diamonds->[ 1 ]->{ 'VERTICES' }->[ 3 - $v1 ],
                $side_diamonds->[ 1 ]->{ 'VERTICES' }->[ 0 ],
                $side_diamonds->[ 1 ]->{ 'VERTICES' }->[ $v1 ],
            ],
    };

    addVertexPolygons( $new_star );
    delVertexPolygons( $new_star, $polygon->{ 'ID' } );
    delVertexPolygons( $new_star, $side_diamonds->[ 1 ]->{ 'ID' } );
    delVertexPolygons( $new_star, $side_diamonds->[ 2 ]->{ 'ID' } );

    push @{ $new_polygons }, $new_star;

    $POLYGON_LOOKUP->{ $new_star->{ 'ID' } } = $new_star;

    $v1 = closerVertex( $vertices->[ 0 ], $side_diamonds->[ 3 ], 1, 2 );
    $v2 = closerVertex( $vertices->[ 0 ], $side_diamonds->[ 0 ], 1, 2 );

    my $new_pentagon = {

            'ID'        => $NEXT_POLYGON_ID++,
            'TYPE'      => POLYGON_TYPE_PENTAGON,
            'PARENT'    => $polygon->{ 'ID' },
            'CREATED'   => $CURRENT_ITERATION,
            'VERTICES'  => [
                $new_vertex,
                $polygon->{ 'VERTICES' }->[ 3 ],
                $side_diamonds->[ 3 ]->{ 'VERTICES' }->[ 3 - $v1 ],
                $side_diamonds->[ 0 ]->{ 'VERTICES' }->[ 3 - $v2 ],
                $polygon->{ 'VERTICES' }->[ 1 ],
            ],
    };

    addVertexPolygons( $new_pentagon );
    delVertexPolygons( $new_pentagon, $polygon->{ 'ID' } );
    delVertexPolygons( $new_pentagon, $side_diamonds->[ 3 ]->{ 'ID' } );
    delVertexPolygons( $new_pentagon, $side_diamonds->[ 0 ]->{ 'ID' } );

    push @{ $new_polygons }, $new_pentagon;

    $POLYGON_LOOKUP->{ $new_pentagon->{ 'ID' } } = $new_pentagon;

    my $new_boat = {

            'ID'        => $NEXT_POLYGON_ID++,
            'TYPE'      => POLYGON_TYPE_BOAT,
            'PARENT'    => $polygon->{ 'ID' },
            'CREATED'   => $CURRENT_ITERATION,
            'VERTICES'  => [
                $vertices->[ 0 ],
                $side_diamonds->[ 3 ]->{ 'VERTICES' }->[ $v1 ],
                $side_diamonds->[ 3 ]->{ 'VERTICES' }->[ 0 ],
                $side_diamonds->[ 3 ]->{ 'VERTICES' }->[ 3 - $v1 ],
                $side_diamonds->[ 0 ]->{ 'VERTICES' }->[ 3 - $v2 ],
                $side_diamonds->[ 0 ]->{ 'VERTICES' }->[ 0 ],
                $side_diamonds->[ 0 ]->{ 'VERTICES' }->[ $v2 ],
            ],
    };

    addVertexPolygons( $new_boat );
    delVertexPolygons( $new_boat, $polygon->{ 'ID' } );
    delVertexPolygons( $new_boat, $side_diamonds->[ 0 ]->{ 'ID' } );
    delVertexPolygons( $new_boat, $side_diamonds->[ 3 ]->{ 'ID' } );

    for ( @{ $side_diamonds } ) { $_->{ 'CONSOLIDATED' } = 1 };

    push @{ $new_polygons }, $new_boat;

    $POLYGON_LOOKUP->{ $new_boat->{ 'ID' } } = $new_boat;

    return;
}

############################################################
# Subdivide a diamond

sub subdivideDiamond {

    my $polygon = shift;
    my $new_polygons = shift;

    if ( ( $CURRENT_ITERATION - $polygon->{ 'CREATED' } ) != 1 ) {
        push @{ $new_polygons }, $polygon;
        return;
    }

    my $vertices = $polygon->{ 'VERTICES' };

    my $side_diamonds = [];

    my $trial1 = $POLYGON_LOOKUP->{ findCommonVertexPolygon( $polygon, 1 ) };
    my $trial2 = $POLYGON_LOOKUP->{ findCommonVertexPolygon( $polygon, 1, $trial1->{ 'ID' } ) };

    my $target = closerPolygon( $vertices->[ 0 ], $trial1, $trial2 );

    my $idx = closerVertex( $vertices->[ 0 ], $target, 2, 4 );

    my $id = findCommonVertexPolygon( $target, $idx, $polygon->{ 'ID' } );
    push @{ $side_diamonds }, $POLYGON_LOOKUP->{ $id };

    $target = ( $target == $trial1 ) ? $trial2 : $trial1;
    $idx = closerVertex( $vertices->[ 2 ], $target, 2, 4 );

    $id = findCommonVertexPolygon( $target, $idx, $polygon->{ 'ID' } );
    push @{ $side_diamonds }, $POLYGON_LOOKUP->{ $id };

    $trial1 = $POLYGON_LOOKUP->{ findCommonVertexPolygon( $polygon, 3 ) };
    $trial2 = $POLYGON_LOOKUP->{ findCommonVertexPolygon( $polygon, 3, $trial1->{ 'ID' } ) };

    $target = closerPolygon( $vertices->[ 2 ], $trial1, $trial2 );

    $idx = closerVertex( $vertices->[ 2 ], $target, 2, 4 );

    $id = findCommonVertexPolygon( $target, $idx, $polygon->{ 'ID' } );
    push @{ $side_diamonds }, $POLYGON_LOOKUP->{ $id };

    $target = ( $target == $trial1 ) ? $trial2 : $trial1;
    $idx = closerVertex( $vertices->[ 0 ], $target, 2, 4 );

    $id = findCommonVertexPolygon( $target, $idx, $polygon->{ 'ID' } );
    push @{ $side_diamonds }, $POLYGON_LOOKUP->{ $id };

    $INVERT_DIAMONDS ? 
            subDivideDiamondStyle0( $polygon, $new_polygons, $side_diamonds )
            :
            subDivideDiamondStyle2( $polygon, $new_polygons, $side_diamonds )
    ;

    delete $POLYGON_LOOKUP->{ $polygon->{ 'ID' } };

    return;
}

############################################################
# Offset a vertex by the line between 1 others

sub offsetVertex {

    my $polygon = shift;
    my $vertex_no = shift;
    my $offset_polygon = shift;
    my $vertex1 = shift;
    my $vertex2 = shift;

    $polygon->{ 'VERTICES' }->[ $vertex_no ]->{ 'X' } +=
            $offset_polygon->{ 'VERTICES' }->[ $vertex1 ]->{ 'X' } -
            $offset_polygon->{ 'VERTICES' }->[ $vertex2 ]->{ 'X' };

    $polygon->{ 'VERTICES' }->[ $vertex_no ]->{ 'Y' } +=
            $offset_polygon->{ 'VERTICES' }->[ $vertex1 ]->{ 'Y' } -
            $offset_polygon->{ 'VERTICES' }->[ $vertex2 ]->{ 'Y' };

    return;
}

############################################################
# Return the closer of the polgons to the point

sub closerPolygon {

    my $point = shift;
    my $polygon1 = shift;
    my $polygon2 = shift;

    my $dist1 = distance( findPolygonCentre( $polygon1 ), $point );
    my $dist2 = distance( findPolygonCentre( $polygon2 ), $point );

    return ( $dist1 < $dist2 ) ? $polygon1 : $polygon2;
}

############################################################
# Return the number of the vertex closest to the given point

sub closerVertex {

    my $point = shift;
    my $polygon = shift;
    my $vertex1 = shift;
    my $vertex2 = shift;

    my $dist1 = distance( $point, $polygon->{ 'VERTICES' }->[ $vertex1 ] );
    my $dist2 = distance( $point, $polygon->{ 'VERTICES' }->[ $vertex2 ] );

    return ($ dist1 < $dist2 ) ? $vertex1 : $vertex2;
}

############################################################
# Subdivide a boat

sub subdivideBoat {

    my $polygon = shift;
    my $new_polygons = shift;

    if ( ( $CURRENT_ITERATION - $polygon->{ 'CREATED' } ) != 1 ) {

        push @{ $new_polygons }, $polygon;
        return;
    }

    my $vertices = $polygon->{ 'VERTICES' };

    my $boundary_boat = ( vertexPolygons( $polygon, 0 ) == 2 );

    my $side_diamonds = [];

    my $trial1 = $POLYGON_LOOKUP->{ findCommonVertexPolygon( $polygon, 3 ) };
    my $trial2 = $POLYGON_LOOKUP->{ findCommonVertexPolygon( $polygon, 3, $trial1->{ 'ID' } ) };

    my $target = closerPolygon( $vertices->[ 2 ], $trial1, $trial2 );

    my $idx = closerVertex( $vertices->[ 2 ], $target, 2, 4 );
    my $id = findCommonVertexPolygon( $target, $idx, $polygon->{ 'ID' } );

    push @{ $side_diamonds }, $POLYGON_LOOKUP->{ $id };

    if ( $boundary_boat && ( vertexPolygons( $polygon, 1 ) == 1 ) ) {

        push @{ $side_diamonds }, {

                    'ID'        => $NEXT_POLYGON_ID++,
                    'TYPE'      => POLYGON_TYPE_OPEN_DIAMOND,
                    'PARENT'    => $polygon->{ 'ID' },
                    'CREATED'   => $CURRENT_ITERATION,
                    'VERTICES'  =>  [
                            pointAlong( $vertices->[ 2 ], $vertices->[ 1 ], 1 / ( PHI * PHI ) ),
                            pointAlong( $vertices->[ 2 ], $vertices->[ 1 ], 1 / ( PHI * PHI ) ),
                            pointAlong( $vertices->[ 1 ], $vertices->[ 2 ], 1 / ( PHI * PHI ) ),
                    ],
        };

        push @{ $side_diamonds }, {

                    'ID'        => $NEXT_POLYGON_ID++,
                    'TYPE'      => POLYGON_TYPE_OPEN_DIAMOND,
                    'PARENT'    => $polygon->{ 'ID' },
                    'CREATED'   => $CURRENT_ITERATION,
                    'VERTICES'  =>  [
                            pointAlong( $vertices->[ 0 ], $vertices->[ 1 ], 1 / ( PHI * PHI ) ),
                            pointAlong( $vertices->[ 0 ], $vertices->[ 1 ], 1 / ( PHI * PHI ) ),
                            pointAlong( $vertices->[ 1 ], $vertices->[ 0 ], 1 / ( PHI * PHI ) ),
                    ],
        };
    }
    else {

        $trial1 = $POLYGON_LOOKUP->{ findCommonVertexPolygon( $polygon, 1 ) };
        $idx = closerVertex( $vertices->[ 2 ], $trial1, 2, 4 );

        $id = findCommonVertexPolygon( $trial1, $idx, $polygon->{ 'ID' } );
        push @{ $side_diamonds }, $POLYGON_LOOKUP->{ $id };

        $id = findCommonVertexPolygon( $trial1, 6 - $idx, $polygon->{ 'ID' } );
        push @{ $side_diamonds }, $POLYGON_LOOKUP->{ $id };
    }

    if ( $boundary_boat && ( vertexPolygons( $polygon, 6 ) == 1 ) ) {

        push @{ $side_diamonds }, {

                    'ID'        => $NEXT_POLYGON_ID++,
                    'TYPE'      => POLYGON_TYPE_OPEN_DIAMOND,
                    'PARENT'    => $polygon->{ 'ID' },
                    'CREATED'   => $CURRENT_ITERATION,
                    'VERTICES'  =>  [
                            pointAlong( $vertices->[ 0 ], $vertices->[ 6 ], 1 / ( PHI * PHI ) ),
                            pointAlong( $vertices->[ 0 ], $vertices->[ 6 ], 1 / ( PHI * PHI ) ),
                            pointAlong( $vertices->[ 6 ], $vertices->[ 0 ], 1 / ( PHI * PHI ) ),
                    ],
        };

        push @{ $side_diamonds }, {

                    'ID'        => $NEXT_POLYGON_ID++,
                    'TYPE'      => POLYGON_TYPE_OPEN_DIAMOND,
                    'PARENT'    => $polygon->{ 'ID' },
                    'CREATED'   => $CURRENT_ITERATION,
                    'VERTICES'  =>  [
                            pointAlong( $vertices->[ 5 ], $vertices->[ 6 ], 1 / ( PHI * PHI ) ),
                            pointAlong( $vertices->[ 5 ], $vertices->[ 6 ], 1 / ( PHI * PHI ) ),
                            pointAlong( $vertices->[ 6 ], $vertices->[ 5 ], 1 / ( PHI * PHI ) ),
                    ],
        };
    }
    else {

        $trial1 = $POLYGON_LOOKUP->{ findCommonVertexPolygon( $polygon, 6 ) };
        $idx = closerVertex( $vertices->[ 0 ], $trial1, 2, 4 );

        $id = findCommonVertexPolygon( $trial1, $idx, $polygon->{ 'ID' } );
        push @{ $side_diamonds }, $POLYGON_LOOKUP->{ $id };

        $id = findCommonVertexPolygon( $trial1, 6 - $idx, $polygon->{ 'ID' } );
        push @{ $side_diamonds }, $POLYGON_LOOKUP->{ $id };
    }

    $trial1 = $POLYGON_LOOKUP->{ findCommonVertexPolygon( $polygon, 4 ) };
    $trial2 = $POLYGON_LOOKUP->{ findCommonVertexPolygon( $polygon, 4, $trial1->{ 'ID' } ) };

    $target = closerPolygon( $vertices->[ 5 ], $trial1, $trial2 );
    $idx = closerVertex($vertices->[ 5 ], $target, 2, 4 );
    $id = findCommonVertexPolygon( $target, $idx, $polygon->{ 'ID' } );

    push @{ $side_diamonds }, $POLYGON_LOOKUP->{ $id };

    if ( $boundary_boat && ( vertexPolygons( $polygon, 1 ) == 1 ) ) {

        my $v1 = closerVertex( $vertices->[ 6 ], $side_diamonds->[ 4 ], 1, 2 );
        offsetVertex( $side_diamonds->[ 1 ], 0, $side_diamonds->[ 4 ], 0, $v1 );

        my $v2 = closerVertex( $vertices->[ 0 ], $side_diamonds->[ 3 ], 1, 2 );
        offsetVertex( $side_diamonds->[ 2 ], 0, $side_diamonds->[ 3 ], $v2, 0 );
    }
    elsif ( $boundary_boat && ( vertexPolygons( $polygon, 6 ) == 1 ) ) {

        my $v1 = closerVertex( $vertices->[ 0 ], $side_diamonds->[ 2 ], 1, 2 );
        offsetVertex( $side_diamonds->[ 3 ], 0, $side_diamonds->[ 2 ], $v1, 0 );

        my $v2 = closerVertex( $vertices->[ 1 ], $side_diamonds->[ 1 ], 1, 2 );
        offsetVertex( $side_diamonds->[ 4 ], 0, $side_diamonds->[ 1 ], 0, $v2 );
    }

    my $v1 = closerVertex( $vertices->[ 2 ], $side_diamonds->[ 0 ], 1, 2 );
    my $v2 = closerVertex( $vertices->[ 2 ], $side_diamonds->[ 1 ], 1, 2 );

    my $new_boat1 = {

                'ID'        => $NEXT_POLYGON_ID++,
                'TYPE'      => POLYGON_TYPE_BOAT,
                'PARENT'    => $polygon->{ 'ID' },
                'CREATED'   => $CURRENT_ITERATION,
                'VERTICES'  =>  [
                        $vertices->[ 2 ],
                        $side_diamonds->[ 0 ]->{ 'VERTICES' }->[ $v1 ],
                        $side_diamonds->[ 0 ]->{ 'VERTICES' }->[ 0 ],
                        $side_diamonds->[ 0 ]->{ 'VERTICES' }->[ 3 - $v1 ],
                        $side_diamonds->[ 1 ]->{ 'VERTICES' }->[ 3 - $v2 ],
                        $side_diamonds->[ 1 ]->{ 'VERTICES' }->[ 0 ],
                        $side_diamonds->[ 1 ]->{ 'VERTICES' }->[ $v2 ],
                ],
    };

    addVertexPolygons( $new_boat1 );
    delVertexPolygons( $new_boat1, $polygon->{ 'ID' } );
    delVertexPolygons( $new_boat1, $side_diamonds->[ 0 ]->{ 'ID' } );
    delVertexPolygons( $new_boat1, $side_diamonds->[ 1 ]->{ 'ID' } );

    $POLYGON_LOOKUP->{ $new_boat1->{ 'ID' } } = $new_boat1;
    
    push @{ $new_polygons }, $new_boat1;

    $v1 = closerVertex( $vertices->[ 0 ], $side_diamonds->[ 2 ], 1, 2 );
    $v2 = closerVertex( $vertices->[ 0 ], $side_diamonds->[ 3 ], 1, 2 );

    my $new_boat2 = {

                'ID'        => $NEXT_POLYGON_ID++,
                'TYPE'      => POLYGON_TYPE_BOAT,
                'PARENT'    => $polygon->{ 'ID' },
                'CREATED'   => $CURRENT_ITERATION,
                'VERTICES'  =>  [
                        $vertices->[ 0 ],
                        $side_diamonds->[ 2 ]->{ 'VERTICES' }->[ $v1 ],
                        $side_diamonds->[ 2 ]->{ 'VERTICES' }->[ 0 ],
                        $side_diamonds->[ 2 ]->{ 'VERTICES' }->[ 3 - $v1 ],
                        $side_diamonds->[ 3 ]->{ 'VERTICES' }->[ 3 - $v2 ],
                        $side_diamonds->[ 3 ]->{ 'VERTICES' }->[ 0 ],
                        $side_diamonds->[ 3 ]->{ 'VERTICES' }->[ $v2 ],
                ],
    };

    addVertexPolygons( $new_boat2 );
    delVertexPolygons( $new_boat2, $polygon->{ 'ID' } );
    delVertexPolygons( $new_boat2, $side_diamonds->[ 2 ]->{ 'ID' } );
    delVertexPolygons( $new_boat2, $side_diamonds->[ 3 ]->{ 'ID' } );

    $POLYGON_LOOKUP->{ $new_boat2->{ 'ID' } } = $new_boat2;
    
    push @{ $new_polygons }, $new_boat2;

    $v1 = closerVertex( $vertices->[ 5 ], $side_diamonds->[ 4 ], 1, 2 );
    $v2 = closerVertex( $vertices->[ 5 ], $side_diamonds->[ 5 ], 1, 2 );

    my $new_boat3 = {

                'ID'        => $NEXT_POLYGON_ID++,
                'TYPE'      => POLYGON_TYPE_BOAT,
                'PARENT'    => $polygon->{ 'ID' },
                'CREATED'   => $CURRENT_ITERATION,
                'VERTICES'  =>  [
                        $vertices->[ 5 ],
                        $side_diamonds->[ 4 ]->{ 'VERTICES' }->[ $v1 ],
                        $side_diamonds->[ 4 ]->{ 'VERTICES' }->[ 0 ],
                        $side_diamonds->[ 4 ]->{ 'VERTICES' }->[ 3 - $v1 ],
                        $side_diamonds->[ 5 ]->{ 'VERTICES' }->[ 3 - $v2 ],
                        $side_diamonds->[ 5 ]->{ 'VERTICES' }->[ 0 ],
                        $side_diamonds->[ 5 ]->{ 'VERTICES' }->[ $v2 ],
                ],
    };

    addVertexPolygons( $new_boat3 );
    delVertexPolygons( $new_boat3, $polygon->{ 'ID' } );
    delVertexPolygons( $new_boat3, $side_diamonds->[ 4 ]->{ 'ID' } );
    delVertexPolygons( $new_boat3, $side_diamonds->[ 5 ]->{ 'ID' } );

    $POLYGON_LOOKUP->{ $new_boat3->{ 'ID' } } = $new_boat3;
    
    push @{ $new_polygons }, $new_boat3;

    for ( @{ $side_diamonds } ) { $_->{ 'CONSOLIDATED' } = 1; }

    # Place the star on the base - find the common open-diamond
 
    $trial1 = $POLYGON_LOOKUP->{ findCommonVertexPolygon( $polygon, 3 ) };
    $trial2 = $POLYGON_LOOKUP->{ findCommonVertexPolygon( $polygon, 3, $trial1->{ 'ID' } ) };

    $target = closerPolygon( $vertices->[ 4 ], $trial1, $trial2 );
    $id = findCommonVertexPolygon( $target, 2, $polygon->{ 'ID' } );
    my $base_diamond = $POLYGON_LOOKUP->{ $id };

    my $new_star = {

            'ID'        => $NEXT_POLYGON_ID++,
            'TYPE'      => POLYGON_TYPE_STAR,
            'PARENT'    => $polygon->{ 'ID' },
            'CREATED'   => $CURRENT_ITERATION,
            'VERTICES'  =>  [
                $base_diamond->{ 'VERTICES' }->[ 0 ],
                $base_diamond->{ 'VERTICES' }->[ 2 ],
                $vertices->[ 3 ],
                pointAlong( $vertices->[ 1 ], $base_diamond->{ 'VERTICES' }->[ 2 ], 1 / PHI ),
                $vertices->[ 1 ],
                pointAlong( $vertices->[ 0 ], $base_diamond->{ 'VERTICES' }->[ 0 ], 1 / PHI ),
                $vertices->[ 6 ],
                pointAlong( $vertices->[ 6 ], $base_diamond->{ 'VERTICES' }->[ 1 ], 1 / PHI ),
                $vertices->[ 4 ],
                $base_diamond->{ 'VERTICES' }->[ 1 ],
            ],
    };

    addVertexPolygons( $new_star );
    delVertexPolygons( $new_star, $polygon->{ 'ID' } );
    delVertexPolygons( $new_star, $id );

    $POLYGON_LOOKUP->{ $new_star->{ 'ID' } } = $new_star;
    
    $base_diamond->{ 'CONSOLIDATED' } = 1;

    push @{ $new_polygons }, $new_star;

    my $new_pentagon1 = {

            'ID'        => $NEXT_POLYGON_ID++,
            'TYPE'      => POLYGON_TYPE_PENTAGON,
            'PARENT'    => $polygon->{ 'ID' },
            'CREATED'   => $CURRENT_ITERATION,
            'VERTICES'  =>  [
                $new_star->{ 'VERTICES' }->[ 3 ],
                $new_star->{ 'VERTICES' }->[ 2 ],
                $new_boat1->{ 'VERTICES' }->[ 3 ],
                $new_boat1->{ 'VERTICES' }->[ 4 ],
                $new_star->{ 'VERTICES' }->[ 4 ],
            ],
    };

    addVertexPolygons( $new_pentagon1 );

    $POLYGON_LOOKUP->{ $new_pentagon1->{ 'ID' } } = $new_pentagon1;
    
    push @{ $new_polygons }, $new_pentagon1;

    my $new_pentagon2 = {

            'ID'        => $NEXT_POLYGON_ID++,
            'TYPE'      => POLYGON_TYPE_PENTAGON,
            'PARENT'    => $polygon->{ 'ID' },
            'CREATED'   => $CURRENT_ITERATION,
            'VERTICES'  =>  [
                $new_star->{ 'VERTICES' }->[ 5 ],
                $new_star->{ 'VERTICES' }->[ 4 ],
                $new_boat2->{ 'VERTICES' }->[ 3 ],
                $new_boat2->{ 'VERTICES' }->[ 4 ],
                $new_star->{ 'VERTICES' }->[ 6 ],
            ],
    };

    addVertexPolygons( $new_pentagon2 );

    $POLYGON_LOOKUP->{ $new_pentagon2->{ 'ID' } } = $new_pentagon2;
    
    push @{ $new_polygons }, $new_pentagon2;

    my $new_pentagon3 = {

            'ID'        => $NEXT_POLYGON_ID++,
            'TYPE'      => POLYGON_TYPE_PENTAGON,
            'PARENT'    => $polygon->{ 'ID' },
            'CREATED'   => $CURRENT_ITERATION,
            'VERTICES'  =>  [
                $new_star->{ 'VERTICES' }->[ 7 ],
                $new_star->{ 'VERTICES' }->[ 6 ],
                $new_boat3->{ 'VERTICES' }->[ 3 ],
                $new_boat3->{ 'VERTICES' }->[ 4 ],
                $new_star->{ 'VERTICES' }->[ 8 ],
            ],
    };

    addVertexPolygons( $new_pentagon3 );

    $POLYGON_LOOKUP->{ $new_pentagon3->{ 'ID' } } = $new_pentagon3;
    
    push @{ $new_polygons }, $new_pentagon3;

    if ( $boundary_boat ) {

        if ( vertexPolygons( $new_boat2, 3 ) == 2 ) {

            my $new_pentagon4 = {

                    'ID'        => $NEXT_POLYGON_ID++,
                    'TYPE'      => POLYGON_TYPE_PENTAGON,
                    'PARENT'    => $polygon->{ 'ID' },
                    'CREATED'   => $CURRENT_ITERATION,
                    'VERTICES'  =>  [
                        $new_star->{ 'VERTICES' }->[ 4 ],
                        $new_boat1->{ 'VERTICES' }->[ 4 ],
                        $new_boat1->{ 'VERTICES' }->[ 5 ],
                        $new_boat2->{ 'VERTICES' }->[ 2 ],
                        $new_boat2->{ 'VERTICES' }->[ 3 ],
                    ],
            };

            addVertexPolygons( $new_pentagon4 );
    
            $POLYGON_LOOKUP->{ $new_pentagon4->{ 'ID' } } = $new_pentagon4;
    
            push @{ $new_polygons }, $new_pentagon4;
        }
        else {

            my $new_pentagon4 = {

                    'ID'        => $NEXT_POLYGON_ID++,
                    'TYPE'      => POLYGON_TYPE_PENTAGON,
                    'PARENT'    => $polygon->{ 'ID' },
                    'CREATED'   => $CURRENT_ITERATION,
                    'VERTICES'  =>  [
                        $new_star->{ 'VERTICES' }->[ 6 ],
                        $new_boat2->{ 'VERTICES' }->[ 4 ],
                        $new_boat2->{ 'VERTICES' }->[ 5 ],
                        $new_boat3->{ 'VERTICES' }->[ 2 ],
                        $new_boat3->{ 'VERTICES' }->[ 3 ],
                    ],
            };

            addVertexPolygons( $new_pentagon4 );
    
            $POLYGON_LOOKUP->{ $new_pentagon4->{ 'ID' } } = $new_pentagon4;
    
            push @{ $new_polygons }, $new_pentagon4;
        }
    }

    return;
}

############################################################
# Subdivide a star

sub subdivideStar {

    my $polygon = shift;
    my $new_polygons = shift;

    if ( ( $CURRENT_ITERATION - $polygon->{ 'CREATED' } ) != 1 ) {

        push @{ $new_polygons }, $polygon;
        return;
    }

    my $vertices = $polygon->{ 'VERTICES' };

    my $boundary_star = ( keys %{ $vertices->[ 5 ]->{ 'POLYGONS' } } == 1 );

    my $new_boats = [];
    my $new_pentagons = [];

    for ( my $p = 0; $p < 5; $p++ ) {   # the apex vertex

        my $v1 = ( ( $p * 2 ) - 1 ) % 10;
        my $v2 = ( ( $p * 2 ) + 1 ) % 10;

        my $side_diamonds = [];

        for ( my $c = 0; $c < 2; $c++ ) {

            # Created place-holder polygons for any that may missing
            # from boundary stars

            if ( ( $p == 2 ) && $boundary_star && ( $c == 1 ) ) {

                push @{ $side_diamonds }, {

                    'ID'        => $NEXT_POLYGON_ID++,
                    'TYPE'      => POLYGON_TYPE_OPEN_DIAMOND,
                    'PARENT'    => $polygon->{ 'ID' },
                    'CREATED'   => $CURRENT_ITERATION,
                    'VERTICES'  =>  [
                        pointAlong( $vertices->[ 4 ], $vertices->[ 5 ], 1 / ( PHI * PHI ) ),
                        pointAlong( $vertices->[ 5 ], $vertices->[ 4 ], 1 / ( PHI * PHI ) ),
                        pointAlong( $vertices->[ 4 ], $vertices->[ 5 ], 1 / ( PHI * PHI ) ),
                    ]
                };
            }
            elsif ( ( $p == 3 ) && $boundary_star && ( $c == 0 ) ) {

                push @{ $side_diamonds }, {

                    'ID'        => $NEXT_POLYGON_ID++,
                    'TYPE'      => POLYGON_TYPE_OPEN_DIAMOND,
                    'PARENT'    => $polygon->{ 'ID' },
                    'CREATED'   => $CURRENT_ITERATION,
                    'VERTICES'  =>  [
                        pointAlong( $vertices->[ 6 ], $vertices->[ 5 ], 1 / ( PHI * PHI ) ),
                        pointAlong( $vertices->[ 6 ], $vertices->[ 5 ], 1 / ( PHI * PHI ) ),
                        pointAlong( $vertices->[ 5 ], $vertices->[ 6 ], 1 / ( PHI * PHI ) ),
                    ]
                };
            }
            else {

                my $first = $POLYGON_LOOKUP->{ findCommonVertexPolygon( $polygon, $c ? $v2 : $v1 ) };

                my $dist1 = distance( $first->{ 'VERTICES' }->[ 2 ], $vertices->[ $p * 2 ] );
                my $dist2 = distance( $first->{ 'VERTICES' }->[ 4 ], $vertices->[ $p * 2 ] );

                my $idx1 = ( $dist1 < $dist2 ) ? 2 : 4;
    
                my $id = findCommonVertexPolygon( $first, $idx1, $polygon->{ 'ID' } );
                push @{ $side_diamonds }, $POLYGON_LOOKUP->{ $id };
            }
        }

        if ( $boundary_star ) {

            if ( $p == 2 ) {

                offsetVertex( $side_diamonds->[ 1 ], 0, $side_diamonds->[ 0 ], 1, 0 );
            }
            elsif ( $p == 3 ) {

                offsetVertex( $side_diamonds->[ 0 ], 0, $side_diamonds->[ 1 ], 2, 0 );
            }
        }

        my $dist1 = distance( $vertices->[ $p * 2 ], $side_diamonds->[ 0 ]->{ 'VERTICES' }->[ 1 ] );
        my $dist2 = distance( $vertices->[ $p * 2 ], $side_diamonds->[ 0 ]->{ 'VERTICES' }->[ 2 ] );

        my $idx1 = $dist1 < $dist2 ? 1 : 2;

        $dist1 = distance( $vertices->[ $p * 2 ], $side_diamonds->[ 1 ]->{ 'VERTICES' }->[ 1 ] );
        $dist2 = distance( $vertices->[ $p * 2 ], $side_diamonds->[ 1 ]->{ 'VERTICES' }->[ 2 ] );

        my $idx2 = $dist1 < $dist2 ? 2 : 1;     # we want the furthest vertex first here

        my $new_boat = {

                    'ID'        => $NEXT_POLYGON_ID++,
                    'TYPE'      => POLYGON_TYPE_BOAT,
                    'PARENT'    => $polygon->{ 'ID' },
                    'CREATED'   => $CURRENT_ITERATION,
                    'VERTICES'  => [
                        $vertices->[ $p * 2 ],
                        $side_diamonds->[ 0 ]->{ 'VERTICES' }->[ $idx1 ],
                        $side_diamonds->[ 0 ]->{ 'VERTICES' }->[ 0 ],
                        $side_diamonds->[ 0 ]->{ 'VERTICES' }->[ 3 - $idx1 ],
                        $side_diamonds->[ 1 ]->{ 'VERTICES' }->[ $idx2 ],
                        $side_diamonds->[ 1 ]->{ 'VERTICES' }->[ 0 ],
                        $side_diamonds->[ 1 ]->{ 'VERTICES' }->[ 3 - $idx2 ],
                    ],
        };
 
        addVertexPolygons( $new_boat );
        delVertexPolygons( $new_boat, $polygon->{ 'ID' } );
        delVertexPolygons( $new_boat, $side_diamonds->[ 0 ]->{ 'ID' } );
        delVertexPolygons( $new_boat, $side_diamonds->[ 1 ]->{ 'ID' } );

        $side_diamonds->[ 0 ]->{ 'CONSOLIDATED' } = 1;
        $side_diamonds->[ 1 ]->{ 'CONSOLIDATED' } = 1;

        push @{ $new_boats }, $new_boat;

        $POLYGON_LOOKUP->{ $new_boat->{ 'ID' } } = $new_boat;
    }

    my $new_star = {

                'ID'        => $NEXT_POLYGON_ID++,
                'TYPE'      => POLYGON_TYPE_STAR,
                'PARENT'    => $polygon->{ 'ID' },
                'CREATED'   => $CURRENT_ITERATION,
                'VERTICES'  => [
                    $vertices->[ 9 ],
                    pointAlong( $vertices->[ 0 ], $vertices->[ 5 ], 1 / PHI ),
                    $vertices->[ 1 ],
                    pointAlong( $vertices->[ 2 ], $vertices->[ 7 ], 1 / PHI ),
                    $vertices->[ 3 ],
                    pointAlong( $vertices->[ 4 ], $vertices->[ 9 ], 1 / PHI ),
                    $vertices->[ 5 ],
                    pointAlong( $vertices->[ 6 ], $vertices->[ 1 ], 1 / PHI ),
                    $vertices->[ 7 ],
                    pointAlong( $vertices->[ 8 ], $vertices->[ 3 ], 1 / PHI ),
                ],
    };

    addVertexPolygons( $new_star );
    delVertexPolygons( $new_star, $polygon->{ 'ID' } );

    $POLYGON_LOOKUP->{ $new_star->{ 'ID' } } = $new_star;

    for ( my $p = 0; $p < 5; $p++ ) {

        my $new_pentagon = {

                'ID'        => $NEXT_POLYGON_ID++,
                'TYPE'      => POLYGON_TYPE_PENTAGON,
                'PARENT'    => $polygon->{ 'ID' },
                'CREATED'   => $CURRENT_ITERATION,
                'VERTICES'  => [
                    $new_star->{ 'VERTICES' }->[ ( $p * 2 ) + 1 ],
                    $new_star->{ 'VERTICES' }->[ $p * 2 ],
                    $new_boats->[ $p ]->{ 'VERTICES' }->[ 3 ],
                    $new_boats->[ $p ]->{ 'VERTICES' }->[ 4 ],
                    $new_star->{ 'VERTICES' }->[ ( ( $p * 2 ) + 2 ) % 10 ],
                ],
        };

        addVertexPolygons( $new_pentagon );
        $POLYGON_LOOKUP->{ $new_pentagon->{ 'ID' } } = $new_pentagon;

        push @{ $new_pentagons }, $new_pentagon;
    }

    # If this is a boundary star we can fit another pentagon 
    if ( $boundary_star ) {

        my $new_pentagon = {

                'ID'        => $NEXT_POLYGON_ID++,
                'TYPE'      => POLYGON_TYPE_PENTAGON,
                'PARENT'    => $polygon->{ 'ID' },
                'CREATED'   => $CURRENT_ITERATION,
                'VERTICES'  => [
                    $new_star->{ 'VERTICES' }->[ 6 ],
                    $new_boats->[ 2 ]->{ 'VERTICES' }->[ 4 ],
                    $new_boats->[ 2 ]->{ 'VERTICES' }->[ 5 ],
                    $new_boats->[ 3 ]->{ 'VERTICES' }->[ 2 ],
                    $new_boats->[ 3 ]->{ 'VERTICES' }->[ 3 ],
                ],
        };

        addVertexPolygons( $new_pentagon );
        delVertexPolygons( $new_pentagon, $polygon->{ 'ID' } );
        $POLYGON_LOOKUP->{ $new_pentagon->{ 'ID' } } = $new_pentagon;

        push @{ $new_pentagons }, $new_pentagon;
    }

    push @{ $new_polygons }, $new_star;
    push @{ $new_polygons }, @{ $new_boats };
    push @{ $new_polygons }, @{ $new_pentagons };

    delete $POLYGON_LOOKUP->{ $polygon->{ 'ID' } };

    return;
}

############################################################
# Subdivide a pentagon

sub subdividePentagon {

    my $pentagon = shift;
    my $new_polygons = shift;

    my $vertices = $pentagon->{ 'VERTICES' };

    my $central_pentagon = {

        'ID'        => $NEXT_POLYGON_ID++,
        'TYPE'      => POLYGON_TYPE_PENTAGON,
        'PARENT'    => $pentagon->{ 'ID' },
        'CREATED'   => $CURRENT_ITERATION,
        'VERTICES'  => [
            findIntersection( $vertices->[ 0 ], $vertices->[ 3 ], $vertices->[ 1 ], $vertices->[ 4 ] ),
            findIntersection( $vertices->[ 1 ], $vertices->[ 4 ], $vertices->[ 2 ], $vertices->[ 0 ] ),
            findIntersection( $vertices->[ 2 ], $vertices->[ 0 ], $vertices->[ 3 ], $vertices->[ 1 ] ),
            findIntersection( $vertices->[ 3 ], $vertices->[ 1 ], $vertices->[ 4 ], $vertices->[ 2 ] ),
            findIntersection( $vertices->[ 4 ], $vertices->[ 2 ], $vertices->[ 0 ], $vertices->[ 3 ] ),
        ]
    };
    addVertexPolygons( $central_pentagon );
    delVertexPolygons( $central_pentagon, $pentagon->{ 'ID' } );

    push @{ $new_polygons }, $central_pentagon;
    $POLYGON_LOOKUP->{ $central_pentagon->{ 'ID' } } = $central_pentagon;

    my $new_pentagons = [];
    for ( my $p = 0; $p < 5; $p++ ) {
 
        my $new_polygon = {

            'ID'        => $NEXT_POLYGON_ID++,
            'TYPE'      => POLYGON_TYPE_PENTAGON,
            'PARENT'    => $pentagon->{ 'ID' },
            'CREATED'   => $CURRENT_ITERATION,
            'VERTICES'  => [
                $central_pentagon->{ 'VERTICES' }->[ $p ],
                $central_pentagon->{ 'VERTICES' }->[ ( $p + 1 ) % 5 ],
                pointAlong( $vertices->[ $p ], $vertices->[ ( $p + 1 ) % 5 ], 1 / ( PHI * PHI ) ),
                $pentagon->{ 'VERTICES' }->[ $p ],
                pointAlong( $vertices->[ $p ], $vertices->[ ( $p + 4 ) % 5 ], 1 / ( PHI * PHI ) ),
            ]
        };

        addVertexPolygons( $new_polygon );
        delVertexPolygons( $new_polygon, $pentagon->{ 'ID' } );
    
        $POLYGON_LOOKUP->{ $new_polygon->{ 'ID' } } = $new_polygon;

        # Check if the vertex (#3) opposite the common central pentagon
        # edge is shared - if so one of the remaining vertices will
        # also be shared with one of those vertex pentagons

        for ( keys %{ $new_polygon->{ 'VERTICES' }->[ 3 ]->{ 'POLYGONS' } } ) {

            next if ( $_ eq $new_polygon->{ 'ID' } );

            my $trial_polygon = $POLYGON_LOOKUP->{ $_ };

            next unless isPentagon( $trial_polygon );
            next unless ( $trial_polygon->{ 'CREATED' } == $CURRENT_ITERATION );

            checkSharedVertex( $new_polygon, 2, $trial_polygon, 2 )       ? noop()
                : checkSharedVertex( $new_polygon, 4, $trial_polygon, 4 ) ? noop()
                : checkSharedVertex( $new_polygon, 2, $trial_polygon, 4 ) ? noop()
                : checkSharedVertex( $new_polygon, 4, $trial_polygon, 2 )
                ;
        }

        # If this was a pentagon generated by a diamond subdivision - check
        # the pentagons common to the boat for half-diamonds on vertices 
        # #2 and #4 that may be consolidated

        push @{ $new_pentagons }, $new_polygon;
    }

    # Between each of the new_pentagons will be a open half diamond
    for ( my $p = 0; $p < 5; $p++ ) {

        my $new_polygon = {

            'ID'        => $NEXT_POLYGON_ID++,
            'TYPE'      => POLYGON_TYPE_OPEN_DIAMOND,
            'PARENT'    => $pentagon->{ 'ID' },
            'CREATED'   => $CURRENT_ITERATION,
            'VERTICES'  => [
                $central_pentagon->{ 'VERTICES' }->[ $p ],
                $new_pentagons->[ $p ]->{ 'VERTICES' }->[ 4 ],
                $new_pentagons->[ ( $p + 4 ) % 5 ]->{ 'VERTICES' }->[ 2 ],
            ]
        };
    
        push @{ $new_polygons }, $new_polygon;
        $POLYGON_LOOKUP->{ $new_polygon->{ 'ID' } } = $new_polygon;

        addVertexPolygons( $new_polygon );
        delVertexPolygons( $new_polygon, $pentagon->{ 'ID' } );
    }

    push @{ $new_polygons }, @{ $new_pentagons };

    delete $POLYGON_LOOKUP->{ $pentagon->{ 'ID' } };
}

############################################################
# Setup the inital polygons

sub setupInitalPolygons {

    my $ang = 2 * pi / 5.0;

    my $initial_polygon = {

        'ID'        => $NEXT_POLYGON_ID++,
        'TYPE'      => POLYGON_TYPE_PENTAGON,
        'PARENT'    => 0,
        'VERTICES'  => [
            newVertex( cos( 0 * $ang ), sin( 0 * $ang ) ),
            newVertex( cos( 1 * $ang ), sin( 1 * $ang ) ),
            newVertex( cos( 3 * $ang ), sin( 2 * $ang ) ),
            newVertex( cos( 3 * $ang ), sin( 3 * $ang ) ),
            newVertex( cos( 4 * $ang ), sin( 4 * $ang ) ),
        ]
    };

    push @{ $POLYGONS }, $initial_polygon;

    $POLYGON_LOOKUP->{ $initial_polygon->{ 'ID' } } = $initial_polygon;

    return;
}

############################################################
# Return a new vertex object

sub newVertex {

   return {
        'X'         =>  shift,
        'Y'         =>  shift,
   };
}

############################################################
# Return the polygons' centre point

sub findPolygonCentre {

    my $polygon = shift;

    my $vertices = $polygon->{ 'VERTICES' };
    my $cx = 0;
    my $cy = 0;

    for ( @{ $vertices } ) {
        $cx += $_->{ 'X' };
        $cy += $_->{ 'Y' };
    }

    return newVertex(
        $cx / scalar( @{ $vertices } ),
        $cy / scalar( @{ $vertices } ),
    );
}

############################################################
# Label a polygon

sub labelPolygon {

   my $fh = shift;
   my $polygon = shift;
   my $label_vertices = shift;

   my $centre = findPolygonCentre( $polygon );

    printf $fh "gsave 
        %f %f translate
        (%s) dup stringwidth pop -2 div -3 moveto
        1 setgray
        show
        grestore\n",
                $DRAWING_SCALE * $centre->{ 'X' },
                $DRAWING_SCALE * $centre->{ 'Y' },
                $polygon->{ 'ID' },
    ; 

    return unless $label_vertices;

    my $vertices = $polygon->{ 'VERTICES' };
    for ( my $v = 0; $v < scalar @{ $vertices }; $v++ ) {

        my $font_sz = 10 / ( $ITERATIONS ** 0.3 );

        printf $fh "gsave 
            /Arial-Bold findfont %f scalefont setfont 
            %f %f translate
            (%s) dup stringwidth pop -2 div %f moveto
            .8 setgray
            show
            grestore\n",
                $font_sz * $TEXT_MULTIPLIER,
                $DRAWING_SCALE * ( $centre->{ 'X' } + 3 * $vertices->[ $v ]->{ 'X' } ) / 4.0,
                $DRAWING_SCALE * ( $centre->{ 'Y' } + 3 * $vertices->[ $v ]->{ 'Y' } ) / 4.0,
                $v,
                $font_sz / -3,
    } 
}

############################################################
# Check drawing raduis

sub insideDrawingRadius {

    my $polygon = shift;

    return TRUE unless $DISPLAY_RADIUS;

    return ( $DRAWING_SCALE * distance( findPolygonCentre( $polygon ),
                                    { 'X' => 0, 'Y' => 0 } ) < $DISPLAY_RADIUS );
}

############################################################
# Draw an open diamond

sub drawOpenDiamond {

    my $fh = shift;
    my $polygon = shift;

    return unless insideDrawingRadius( $polygon );

    my $vertices = $polygon->{ 'VERTICES' };

    printf $fh "%f %f mv %f %f ln %f %f ln stroke\n",
                    $DRAWING_SCALE * $vertices->[ 1 ]->{ 'X' },
                    $DRAWING_SCALE * $vertices->[ 1 ]->{ 'Y' },
                    $DRAWING_SCALE * $vertices->[ 0 ]->{ 'X' },
                    $DRAWING_SCALE * $vertices->[ 0 ]->{ 'Y' },
                    $DRAWING_SCALE * $vertices->[ 2 ]->{ 'X' },
                    $DRAWING_SCALE * $vertices->[ 2 ]->{ 'Y' },
    ;

    printf $fh "gsave 0 0 1 setrgbcolor %f %f mv %f %f ln stroke grestore\n",
                    $DRAWING_SCALE * $vertices->[ 1 ]->{ 'X' },
                    $DRAWING_SCALE * $vertices->[ 1 ]->{ 'Y' },
                    $DRAWING_SCALE * $vertices->[ 2 ]->{ 'X' },
                    $DRAWING_SCALE * $vertices->[ 2 ]->{ 'Y' },
    ;

    labelPolygon( $fh, $polygon ) if ( $LABEL_POLYGONS );
}

############################################################
# Find the color to fill the given pentagon

sub findPentagonFillColor {

    my $pentagon = shift;

    # If this polygon is at the vertex of a diamond - color it blue 
    my $boat_vertices = 0;
    my $star_vertices = 0;
    my $diamond_points_only = FALSE;

    for ( my $vertex_no = 0; $vertex_no < 5; $vertex_no++ ) {

        my $vertex = $pentagon->{ 'VERTICES' }->[ $vertex_no ];

        for ( keys %{ $vertex->{ 'POLYGONS' } } ) {

            my $polygon = $POLYGON_LOOKUP->{ $_ };

            if ( isDiamond( $polygon ) ) {
            
                my $v = findVertexNumber( $polygon, $vertex );
                $diamond_points_only = ! ( $v % 2 );
            }

            $star_vertices++ if ( isStar( $polygon ) );
            $boat_vertices++ if ( isBoat( $polygon ) );
        }
    }

    return '.12 .24 .48 setrgbcolor' if ( $diamond_points_only );
    return '1 0 0 setrgbcolor' if ( $boat_vertices == 3 );
    return '1 0 0 setrgbcolor' if ( $star_vertices == 3 );

    return '.48 setgray';
}

############################################################
# Find the color to fill the given polygon

sub findPolygonFillColor {

    my $polygon = shift;
    my $fill = shift;

    my $color = '';

    if ( $DRAWING_SCHEME eq 'fill' ) {

        $color = 
            isStar( $polygon )      ? "0 1 0 setrgbcolor" :
            isBoat( $polygon )      ? ".5 .5 1 setrgbcolor" :
            isPentagon( $polygon )  ? findPentagonFillColor( $polygon ) :
            isDiamond( $polygon )   ? "1 1 0 setrgbcolor" :
            ''
        ;
    }
    elsif ( $fill ) {

        $color = ".3 setgray";
    }

    return $color ? " gsave $color fill grestore\n" : "";
}


############################################################
# Draw a polygon

sub drawPolygon {

    my $fh = shift;
    my $polygon = shift;
    my $fill = shift;

    return unless insideDrawingRadius( $polygon );

    my $vertices = $polygon->{ 'VERTICES' };

    for ( my $v = 0; $v < scalar @{ $vertices }; $v++ ) {

        printf $fh "%f %f %s ",
                    $DRAWING_SCALE * $vertices->[ $v ]->{ 'X' },
                    $DRAWING_SCALE * $vertices->[ $v ]->{ 'Y' },
                    ( $v == 0 ) ? 'mv' : 'ln'
        ;
    }

    printf $fh findPolygonFillColor( $polygon, $fill );
    printf $fh "closepath stroke\n";

    labelPolygon( $fh, $polygon, TRUE ) if ( $LABEL_POLYGONS );

    return;
}

############################################################
# Set the default line color

sub defaultLineColor {
    
    return ( $DRAWING_SCHEME eq 'fill' ) ? '0 setgray' : '1 1 0 setrgbcolor';
}

############################################################
# Draw the polygons

sub drawPolygons {

    my $fh = shift;

    for my $polygon ( @{$POLYGONS} ) {

        my $fill = $FILL_POLYGON && ( $polygon->{ 'ID' } eq $FILL_POLYGON );

        isPentagon( $polygon )      ? drawPolygon( $fh, $polygon, $fill ) 
        : isDiamond( $polygon )     ? drawPolygon( $fh, $polygon, $fill ) 
        : isOpenDiamond( $polygon ) ? drawOpenDiamond( $fh, $polygon, $fill )
        : isStar( $polygon )        ? drawPolygon( $fh, $polygon, $fill )
        : isBoat( $polygon )        ? drawPolygon( $fh, $polygon, $fill )
        : undef 
        ;

    }

    return;
}

############################################################
# Write the image header

sub writeImageHeader {

    my $fh = shift;

    $DRAWING_OFFSET = [ $IMAGE_SIZE->[ 0 ] / 2.0, $IMAGE_SIZE->[ 1 ] / 2.0 ];

    if ( $CENTER_POLYGON ) {

        if ( my $polygon = $POLYGON_LOOKUP->{ $CENTER_POLYGON } ) {

            my $center = findPolygonCentre( $polygon );
            $DRAWING_OFFSET = [
                $IMAGE_SIZE->[ 0 ] / 2.0 - $DRAWING_SCALE * $center->{ 'X' }, 
                $IMAGE_SIZE->[ 0 ] / 2.0 - $DRAWING_SCALE * $center->{ 'Y' }
            ]; 
        }
        else {

            printf "Could not find polygon %s\n", $CENTER_POLYGON;
            exit -1;
        }
    }

#$DRAWING_OFFSET = [ -1568.883707, -1589.091814 ];

    my $font_sz = 12 / ( $ITERATIONS ** 0.3 );

    printf $fh "%%!PS\n%%BoundingBox: 0 0 %f %f\n%%EndComments

        <<
            /PageSize [ %f %f ]
            /ImagingBBox null
        >> setpagedevice

        /mv { moveto } bind def
        /ln { lineto } bind def

        0 0 mv %f 0 ln %f %f ln 0 %f ln fill

        /Arial findfont %f scalefont setfont 

        %f %f translate

        1 setlinejoin
        %f setlinewidth
        %s  %% default line color
    ",
        $IMAGE_WIDTH, $IMAGE_DEPTH,
        $IMAGE_SIZE->[ 0 ], $IMAGE_SIZE->[ 1 ],
        $IMAGE_WIDTH, $IMAGE_WIDTH,
        $IMAGE_DEPTH, $IMAGE_DEPTH,
        $font_sz * $TEXT_MULTIPLIER,
        $DRAWING_OFFSET->[ 0 ],
        $DRAWING_OFFSET->[ 1 ],
        1 / ( $ITERATIONS ** 0.5 ),
        defaultLineColor(),
    ;

    return;
}

############################################################
# Find intersection of lines (vtx1->vtx2) and (vtx3->vtx4)

sub findIntersection {

    my $vtx1 = shift;
    my $vtx2 = shift;
    my $vtx3 = shift;
    my $vtx4 = shift;

    my $val1 = ( $vtx1->{ 'X' } * $vtx2->{ 'Y' } - $vtx1->{ 'Y' } * $vtx2->{ 'X' } );
    my $val2 = ( $vtx3->{ 'X' } * $vtx4->{ 'Y' } - $vtx3->{ 'Y' } * $vtx4->{ 'X' } );
    my $val3 = ( ( $vtx1->{ 'X' } - $vtx2->{ 'X' } )* ( $vtx3->{ 'Y' } - $vtx4->{ 'Y' } ) -
            ( $vtx3->{ 'X' } - $vtx4->{ 'X' } )* ( $vtx1->{ 'Y' } - $vtx2->{ 'Y' } ) );

    return newVertex (

        ( $val1 * ($vtx3->{ 'X' } - $vtx4->{ 'X' } ) - $val2 * ( $vtx1->{ 'X' } - $vtx2->{ 'X' } ) ) / $val3,
        ( $val1 * ($vtx3->{ 'Y' } - $vtx4->{ 'Y' } ) - $val2 * ( $vtx1->{ 'Y' } - $vtx2->{ 'Y' } ) ) / $val3,
    )
}

############################################################
# Draw the image

sub drawImage {

    my $drawing_type = shift;

    open my $fh, '>', $PS_TEMP or die "Could not open $PS_TEMP - $!\n";

    writeImageHeader( $fh );

    drawPolygons( $fh );

    close $fh;

    my $convert = "/usr/local/bin/convert";
    $convert = "/usr/bin/convert" unless ( -f $convert );
    $convert = "/Applications/ImageMagick/ImageMagick-6.6.5/bin/convert" unless ( -f $convert );

    unless ( -f $convert ) {

        printf "Could not locate convert at $convert\n";
    }
    else {

        system( "$convert $PS_TEMP $OUTPUT_FILE" );
    }

    unlink $PS_TEMP unless $options{ 'k' };

    return;
}

############################################################
# Help doco

sub help {

    print "\n@_\n" if @_;

    printf "\n$0 " .
                "[-c <id>] [-d] [-f] [-F <file>] [-h] [-i <count>] [-l] " .
                "[-p] [-r <rad>] [-s <scale>] [-t]

    where
        -c  center the image on the given polygon
        -d  invert diamonds filling
        -f  fill the given polygon
        -h  this help message
        -i  iterations (default 1)
        -l  label the polygons
        -p  dump polygon data
        -r  drawing radius
        -s  drawing scale
        -t  leave the temp PS file

        -D  image depth
        -W  image width
        -F  output file
        -S  drawing scheme (see below)
        
    The following drawing schemes are supports:

        line    - outlined polygons (the default)
        fill    - simple polygon coloring
    \n";

    exit;
}

############################################################
# Return the polygon type decsription

sub getPolygonDescription {

    my $polygon = shift;

    return isPentagon( $polygon )       ? 'Pentagon'
         :  isDiamond( $polygon )       ? 'Diamond'
         :  isOpenDiamond( $polygon )   ? 'Open-Diamond'
         :  isStar( $polygon )          ? 'Star'
         :  isBoat( $polygon )          ? 'Boat'
         : '';
}

############################################################
# Dump polygon status

sub dumpPolygons {

    for my $polygon ( @{ $POLYGONS } ) {

        printf "P%s - %s (%d old) (parent P%s) consolidated\n",
                            $polygon->{ 'ID' },
                            getPolygonDescription( $polygon ),
                            $CURRENT_ITERATION - $polygon->{ 'CREATED' },
                            $polygon->{ 'PARENT' },
                            $polygon->{ 'CONSOLIDATED' } ? '' : 'not ',
                            ;

        for ( @{ $polygon->{ 'VERTICES' } } ) {
            my $v = $_;
            printf "    %x [ ", $v;
            printf join ',', ( sort keys %{ $v->{ 'POLYGONS' } } );
            printf " ]\n";
        };
    }

    exit;
}

############################################################
# Null op

sub noop { return }

############################################################
# Report a ghastly error

sub fatal {

    printf "@_\n";
    exit -1;
}

__END__

