#!/usr/bin/perl

use warnings;
use strict;

use Math::Trig;

use constant FALSE   =>  0;
use constant TRUE    =>  1;

my $TEMP = 'spiral.tmp';
my $DEST = '../spiral.png';

open my $FH, '>', $TEMP or die "Could not open $TEMP - $!\n";

my $e = 2.718281828459045;

my $ang = 80 * ( pi / 180 );

my $spx = 300;
my $spy = 350;
 
print $FH "%!PS

        /mv { moveto } bind def
        /ln { lineto } bind def
        /rl { rlineto } bind def

        0 0 mv 1000 0 rl 0 1000 rl -1000 0 rl fill
 
        /ang 80 def
        300 350 translate
        .3 .3 scale
";

## drawSpiral();
for( my $a = 4 * pi; $a < 11 * pi; $a += deg2rad( 23 ) ) {
    invertSpiral( $a )
}

printf "converting...\n";
system( "/usr/bin/convert $TEMP $DEST" );
#system( "cat $TEMP " );
#unlink $TEMP;

exit;

##################################################
# Draw the main equi-angular spiral

sub drawSpiral {

    for( my $c = 0; $c < 50; $c++ ) {
 
        my $col_div = $c / 50;
        my $width = ( 1 - $col_div ) * 0.5;
           
        printf $FH "%f 0 0 setrgbcolor\n", $col_div * $col_div;
 
        my $step = ( pi / 180 );

        my $prev_val;

        for( my $a = 0; $a < 16 * pi; $a += $step ) {
 
            my $val = $e ** ( $a * cos( $ang ) / sin( $ang ) );
    
            if ( $a > 0 ) {
       
                printf $FH "%0.4f %0.4f mv\n", 
                            $prev_val * ( 1 - $width ) * cos( $a ),
                            $prev_val * ( 1 - $width ) * sin( $a );

                printf $FH "%0.4f %0.4f ln\n", 
                            $val * ( 1 - $width ) * cos( $a + $step ),
                            $val * ( 1 - $width ) * sin( $a + $step );

                printf $FH "%0.4f %0.4f ln\n", 
                            $val * ( 1 + $width ) * cos( $a + $step ),
                            $val * ( 1 + $width ) * sin( $a + $step );

                printf $FH "%0.4f %0.4f ln\n", 
                            $prev_val * ( 1 + $width ) * cos( $a ),
                            $prev_val * ( 1 + $width ) * sin( $a );

                printf $FH "fill newpath\n";
            }
 
            $prev_val = $val
        }
    }
}
 
##################################################
# Draw a disc and clip or stroke it

sub disc {
   
    my $disc_x = shift;
    my $disc_y = shift;
    my $disc_r = shift;
 
    my $clipit = shift;
 
    for ( my $a = 0; $a <= 360; $a += 5 ) {
        
        my $x = cos( deg2rad $a ) * $disc_r + $disc_x;
        my $y = sin( deg2rad $a ) * $disc_r + $disc_y;
        printf $FH "%0.4f %0.4f %s\n", $x, $y,
                        ( $a == 0 ) ? 'mv' : 'ln';
    }
 
    printf $FH "viewclip\n" if $clipit;
}

##################################################
# Invert the main spiral

sub invertSpiral {

    my $inv_ang = shift;

    my $v1 = $e ** ( cos( $ang ) / sin( $ang ) * $inv_ang );
    my $v2 = $e ** ( cos( $ang ) / sin( $ang ) * ( 2 * pi + $inv_ang ) );

    #/v1 e ang cos ang sin div inv_a 180 div pi mul mul exp def
    #/v2 e ang cos ang sin div inv_a 360 add 180 div pi mul mul exp def
 
    #/ixc 2 v1 mul v2 add 3 div inv_a cos mul def
    #/iyc 2 v1 mul v2 add 3 div inv_a sin mul def
 
    #/irad v2 v1 sub 6 div def

    my $rad = ( $v1 + $v2 ) / 6;
 
    printf $FH "gsave\n";

    disc (
        cos( $inv_ang ) * ( 2 * $v1 + $v2 ) / 3,
        sin( $inv_ang ) * ( 2 * $v1 + $v2 ) / 3,
        $rad,
        TRUE
    );

    for( my $d = 0; $d < 50; $d++ ) {

        printf $FH "
            gsave
                %0.4f dup 0 setrgbcolor
                %0.4f setlinewidth
                stroke
            grestore\n",
        ( $d / 50 ) ** 3,
        1 + ( 2 * ( 1 - ( $d / 50 ) ) * $rad );
    } 

    disc (
        cos( $inv_ang ) * ( 2 * $v1 + $v2 ) / 3,
        sin( $inv_ang ) * ( 2 * $v1 + $v2 ) / 3,
        $rad,
        FALSE
    );

    printf $FH "grestore\n";
}

__END__

/invertSpiral {

    /inv_a exch def

    gsave 

    /sc 1 def
 
    /v1 e ang cos ang sin div inv_a 180 div pi mul mul exp def
    /v2 e ang cos ang sin div inv_a 360 add 180 div pi mul mul exp def
 
    /ixc 2 v1 mul v2 add 3 div inv_a cos mul def
    /iyc 2 v1 mul v2 add 3 div inv_a sin mul def
 
    /irad v2 v1 sub 6 div def
 
    ixc iyc irad true disc

    0 5 360 8 mul {
 
        /sp_a exch def
   
        /v2 e ang cos ang sin div sp_a 180 div pi mul mul exp def
        /x v2 sp_a cos mul def
        /y v2 sp_a sin mul def
        /d x ixc sub dup mul y iyc sub dup mul add sqrt def
    
        /ix x ixc sub irad dup mul mul d div d div sc mul ixc add def
        /iy y iyc sub irad dup mul mul d div d div sc mul iyc add def
 
        ix iy sp_a 0 eq { moveto } { lineto } ifelse
    } for
 
    ixc iyc irad false disc

    /divs 50 def

    0 1 divs {
 
        /v exch divs div def
 
        gsave
   
        v 3 exp dup 0 setrgbcolor
 
        1 v sub irad 2 mul mul 1 add setlinewidth
 
        stroke
        grestore
    } for
 
    0 setgray stroke

    grestore
} def
 
