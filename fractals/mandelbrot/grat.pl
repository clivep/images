#!/usr/bin/perl

use warnings;
use strict;

use POSIX qw(ceil);

sub log10 {
    return log(shift)/log(10);
}

if ( $#ARGV < 1 ) {
    printf "Usage: %s <axis min> <axis max>\n", $0;
    exit 0;
}

my $axis_min = $ARGV[0];
my $axis_max = $ARGV[1];

my $scale = 10 ** int( log10( $axis_max ) );

my $scaled_min = $axis_min / $scale;
my $scaled_max = $axis_max / $scale;

my $range = ceil( $scaled_max - $scaled_min );

my $step =    ( $range < 2 ) ? 0.1
            : ( $range < 3 ) ? 0.2
            : ( $range < 6 ) ? 0.5
            : 1;

my $v = $step * int($scaled_min / $step);

printf "%f\n", $v * $scale;

do {
    $v += $step;
    printf "%f\n", $v * $scale;
} while (  $v < $scaled_max );
