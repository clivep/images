#!/usr/bin/perl
  
use warnings;
use strict;

use Getopt::Std;
use Math::Trig;
use Readonly;
use Data::Dumper;

my Readonly    $EVEN                    =   "E";
my Readonly    $ODD                     =   "O";

my Readonly    $PS_TEMP                 =   '/tmp/collatz.ps';
my Readonly    $DEFAULT_OUTPUT_FILE     =   'collatz.pdf';

my Readonly    $DEFAULT_CANDIDATES      =   5000;

my Readonly    $DEFAULT_IMAGE_DEP       =   1000;
my Readonly    $DEFAULT_IMAGE_WID       =   1000;

my Readonly    $DEFAULT_EVEN_ANG        =   -10;
my Readonly    $DEFAULT_ODD_ANG         =   18;

my Readonly    $DEFAULT_COLOR_FUNC      =   "FullSpectrum";
my Readonly    $DEFAULT_LENTH_FUNC      =   "Default";
my Readonly    $DEFAULT_VALUE_FUNC      =   "Sequence";
my Readonly    $DEFAULT_WIDTH_FUNC      =   "Default";

my Readonly    $DEFAULT_LINE_MULTIPLIER =   1;

# Map the functions that describe the colors for given values (0<=v<1)
my $_COLOR_FUNCS = {
    'FullSpectrum'  =>  [   \&fullSpectrumColor,    'the full color range' ], 
    'BlueGreen'     =>  [   \&blueGreenColor,       'lower half of the full spectrum' ],
    'BlueWhite'     =>  [   \&blueWhiteColor,       'blue to white' ],
};

# Map the functions that calculate the segment lengths
my $_LENTH_FUNCS = {
    'Default'     =>  [   \&getLenthDefault,       'fixed length - 10' ],
    'NodeVal'     =>  [   \&getLenthByNodeVal,     'relative to the node value' ],
};

# Map the functions that produce values for lines segments (which are then converted to colors)
my $_VALUE_FUNCS = {
    'Sequence'      =>  [   \&getValueByTrailSequence,  'distance along the line' ],   
    'TrailLength'   =>  [   \&getValueByTrailLength,    'relative length if the line' ],   
    'Visits'        =>  [   \&getValueByVisits,         'according to the number of visits' ],   
};

# Map the functions that produce values for lines widths
my $_WIDTH_FUNCS = {
    'Default'       =>  [   \&getWidthDefault,  'fixed width - 1' ],   
    'Visits'        =>  [   \&getWidthByVisits, 'according to the number of visits' ],   
};

my @_SPECTRUM_COLORS = (
        [148, 0, 211],    # Violet
        [75, 0, 130],     # Indigo
        [0, 0, 255],      # Blue
        [0, 255, 0],      # Green
        [255, 255, 0],    # Yellow
        [255, 127, 0],    # Orange
        [255, 0, 0],      # Red
);

############################################################
# Check the runline options

my %options;
getopts( 'c:e:f:hkl:m:o:pr:s:v:w:D:ILW:', \%options );

help() if $options{ 'h' };

############################################################
# Setup the working environment

my $CANDIDATES = $options{ 'c' } || 1000;
my $EVEN_ANG = $options{ 'e' } || $DEFAULT_EVEN_ANG;
my $ODD_ANG = $options{ 'o' } || $DEFAULT_ODD_ANG;
my $OUTPUT_FILE = $options{ 'f' } || $DEFAULT_OUTPUT_FILE;
my $LINE_MULTIPLIER = $options{ 'm' } || $DEFAULT_LINE_MULTIPLIER;
my $COLOR_FUNC = $options{ 's' } || $DEFAULT_COLOR_FUNC;
my $LENTH_FUNC = $options{ 'l' } || $DEFAULT_LENTH_FUNC;
my $VALUE_FUNC = $options{ 'v' } || $DEFAULT_VALUE_FUNC;
my $WIDTH_FUNC = $options{ 'w' } || $DEFAULT_WIDTH_FUNC;

my $IMAGE_DEP = $options{ 'D' } || $DEFAULT_IMAGE_DEP;
my $IMAGE_WID = $options{ 'W' } || $DEFAULT_IMAGE_WID;

help( "Spectrum func '$COLOR_FUNC' is not valid" ) unless $_COLOR_FUNCS->{ $COLOR_FUNC };
help( "Length func '$LENTH_FUNC' is not valid" ) unless $_LENTH_FUNCS->{ $LENTH_FUNC };
help( "Value func '$VALUE_FUNC' is not valid" ) unless $_VALUE_FUNCS->{ $VALUE_FUNC };
help( "Width func '$WIDTH_FUNC' is not valid" ) unless $_WIDTH_FUNCS->{ $WIDTH_FUNC };

my $SOLUTIONS = {
        1           =>  {
                        'VISITS'    =>  0,
                        'ANG'       =>  0,
                        'X'         =>  0,
                        'Y'         =>  0,
                        'VISITS'    =>  0,
                    },
        'MAX-VALUE' =>  0,
};

############################################################
# Do the work

processImage();
renderImage();
printf Dumper $SOLUTIONS if ( $options{ 'I' } );

exit;

############################################################
# Some utility funcs

sub MAX { $_[$_[0] < $_[1]] }
sub MIN { $_[$_[0] > $_[1]] }

############################################################
# Process the image

sub processImage {

    for my $candidate ( 2..$CANDIDATES ) {
    
        printf "Processing: %0.3f%%\r", 100 * $candidate / $CANDIDATES if $options{ 'p' };
    
        next if $SOLUTIONS->{ $candidate };

        my @trail = ();
        my $value = $candidate;
        my $joined_leaf = 0;
        my $got_intersection = 0;
    
        while ( $value != 1 ) {
    
            my $next = ( $value % 2 ) ? 3 * $value + 1 : int($value / 2);
            
            if ( ! $SOLUTIONS->{ $value } ) {

                $SOLUTIONS->{ 'MAX-VALUE' } = MAX( $SOLUTIONS->{ 'MAX-VALUE' }, $value );

                $SOLUTIONS->{ $value } = {
                    'LEAF'      =>   ( $value == $candidate ) ? 1 : 0,
                    'VISITS'    =>   1,
                    'NEXT'      =>   $next,
                    'TRAIL'     =>   [],
                    'TYPE'      =>   ( $value % 2 ) ? $ODD : $EVEN,
                };
    
            } else {
    
                if ( $SOLUTIONS->{ $value }->{ 'LEAF' } ) {
    
                    # If we join a leaf then there is no intersection and this is just an extension
                    $SOLUTIONS->{ $value }->{ 'LEAF' } = 0;
                    $joined_leaf = 1;
                    
                    # Release the trail - they are only used when drawing leaf tracks
                    delete $SOLUTIONS->{ $value }->{ 'TRAIL' };
                }
                else {
    
                    if ( $value != $candidate && ! $joined_leaf && ! $got_intersection ) {

                        $SOLUTIONS->{ $value }->{ 'INTERSECTION' } = 1;
                    }
                    $got_intersection = 1;
                }
    
                # only increment the visits if we have joined an intersection (not a leaf)
                $SOLUTIONS->{ $value }->{ 'VISITS' }++ unless $joined_leaf;
            }
    
            push @{ $SOLUTIONS->{ $candidate }->{ 'TRAIL' } }, $value if ( $value != $candidate );
    
            $value = $next;
        }
    
        $SOLUTIONS->{ 1 }->{ 'VISITS' }++ unless $joined_leaf;
    }

    printf "Processing: done    \n" if $options{ 'p' };
}

############################################################
# Render the image

sub renderImage {

    open my $fh, '>', $PS_TEMP or die "Could not open $PS_TEMP: $!\n";

    writeImageHeader( $fh );

    my $bounds = {
        'MIN-X' =>  0,
        'MIN-Y' =>  0,
        'MAX-X' =>  0,
        'MAX-Y' =>  0,
    };

    for my $candidate ( 1..$CANDIDATES ) {

        printf "Rendering: %0.3f%%\r", 100 * $candidate / $CANDIDATES if $options{ 'p' };

        next unless $SOLUTIONS->{ $candidate }->{ 'LEAF' };
        renderLeafPath( $fh, $bounds, $candidate );
    }

    scaleImageToFit( $fh, $bounds );

    for my $candidate ( 1..$CANDIDATES ) {

        next unless $SOLUTIONS->{ $candidate }->{ 'LEAF' };
        printf $fh "path%d ", $candidate;
    }

    labelNodes( $fh ) if $options{ 'L' };

    close $fh;

    printf "Rendering: done     \nConverting..." if $options{ 'p' };

    system( sprintf "convert '%s' '%s'", $PS_TEMP, $OUTPUT_FILE );
    if ( $options{ 'k' } ) {
        printf "Kept: %s\n", $PS_TEMP;
    }
    else {
        unlink $PS_TEMP;
    }

    printf "\nDone\n" if $options{ 'p' };
}

############################################################
# Scale the image to fit the rendering area

sub writeImageHeader {

    my $fh = shift;

    printf $fh "%%!PS\n%%%%BoundingBox: 0 0 %f %f\n%%%%EndComments\n
    
        /imgw %f def
        /imgd %f def

        <<
            /PageSize [ imgw imgd ]
            /ImagingBBox null
        >> setpagedevice
    
        /sc 1 def
        /mv { sc mul exch sc mul exch moveto } def
        /ln { sc mul exch sc mul exch lineto } def
    
        /Arial-Bold findfont 16 scalefont setfont

        0 0 mv
        imgw 2 mul 0 ln
        imgw 2 mul imgd 2 mul ln
        0 imgd 2 mul ln
        0 setgray fill

        imgw 2 div imgd 2 div translate
        %f rotate
        imgw -2 div imgd -2 div translate

        0.5 setlinewidth
    ",
    $IMAGE_WID, $IMAGE_DEP,
    $IMAGE_WID, $IMAGE_DEP,
    $options{ 'r' } || 0,
    ;
}

############################################################
# Scale the image to fit the rendering area

sub scaleImageToFit {

    my $fh = shift;
    my $bounds = shift;

    my $mrgn = 50;
    
    my $sx = ( $IMAGE_WID - 2 * $mrgn ) / ( $bounds->{ 'MAX-X' } - $bounds->{ 'MIN-X' } );
    my $sy = ( $IMAGE_DEP - 2 * $mrgn ) / ( $bounds->{ 'MAX-Y' } - $bounds->{ 'MIN-Y' } );
    
    my $gtm = {};
    
    if ( $sx < $sy ) {
        $gtm-> { 'SCALE' } = $sx;
    }
    else {
        $gtm-> { 'SCALE' } = $sy;
    }
    
    $gtm-> { 'X_OFF' } = ( ( $IMAGE_WID - $gtm-> { 'SCALE' } * ( $bounds->{ 'MAX-X' } + $bounds->{ 'MIN-X' } ) ) / 2 );
    $gtm-> { 'Y_OFF' } = ( ( $IMAGE_DEP - $gtm-> { 'SCALE' } * ( $bounds->{ 'MAX-Y' } + $bounds->{ 'MIN-Y' } ) ) / 2 );
    
    printf $fh "
        %f %f translate
        /sc %f def
    ",
        $gtm->{ 'X_OFF' },
        $gtm->{ 'Y_OFF' },
        $gtm->{ 'SCALE' },
    ;
}
 
############################################################
# Write a func to render the given leaf path

sub renderLeafPath {

    my $fh = shift;
    my $bounds = shift;
    my $candidate = shift;

    my $trail = "";
    for ( reverse @{ $SOLUTIONS->{ $candidate }->{ 'TRAIL' } } ) {
        $trail .= $SOLUTIONS->{ $_ }->{ 'TYPE' };
    }
    $trail .= $SOLUTIONS->{ $candidate }->{ 'TYPE' };

    my @nodes = ( reverse( @{ $SOLUTIONS->{ $candidate }->{ 'TRAIL' } } ), $candidate );

    printf $fh "/path%d { gsave\n", $candidate;

    # Render the path from node #1
    my $x = 0;
    my $y = 0;
    my $ang = 90;
    my $seq = 0;

    my $prev_width = $LINE_MULTIPLIER * getSegmentWidth( $candidate, 1, 0 );
        
    for ( @nodes ) {

        my $len = getSegmentLenth( $candidate, $_, $seq );;

        $ang += $SOLUTIONS->{ $_ }->{ 'TYPE' } eq $ODD ? $ODD_ANG : $EVEN_ANG;
        my $nx = $x + $len * cos( deg2rad( $ang ) );
        my $ny = $y + $len * sin( deg2rad( $ang ) );
        
        my $width = $LINE_MULTIPLIER * getSegmentWidth( $candidate, $_, $seq );

        printf $fh "%f %f mv\n", $x, $y;

        printf $fh "%f %f ln\n",
                $x + $prev_width * sin( deg2rad( $ang ) ), 
                $y - $prev_width * cos( deg2rad( $ang ) );

        printf $fh "%f %f ln\n",
                $nx + $width * sin( deg2rad( $ang ) ),
                $ny - $width * cos( deg2rad( $ang ) );

        for ( my $a = 0; $a <= 180; $a += 10 ) {
            printf $fh "%f %f ln\n",
                    $nx + $width * sin( deg2rad( $ang + $a ) ),
                    $ny - $width * cos( deg2rad( $ang + $a ) );
        }

        printf $fh "%f %f ln\n",
                $nx - $width * sin( deg2rad( $ang ) ),
                $ny + $width * cos( deg2rad( $ang ) );

        printf $fh "%f %f ln\n",
                $x - $prev_width * sin( deg2rad( $ang ) ), 
                $y + $prev_width * cos( deg2rad( $ang ) );

        printf $fh "%f %f ln\n", $x, $y;

        printf $fh "%f %f %f setrgbcolor\n", getSegmentColor( $candidate, $_, $seq );
        printf $fh "gsave stroke grestore fill\n";

        $x = $nx;
        $y = $ny;

        $prev_width = $width;
    
        $bounds->{ 'MIN-X' } = $x if $x < $bounds->{ 'MIN-X' };
        $bounds->{ 'MAX-X' } = $x if $x > $bounds->{ 'MAX-X' };
        $bounds->{ 'MIN-Y' } = $y if $y < $bounds->{ 'MIN-Y' };
        $bounds->{ 'MAX-Y' } = $y if $y > $bounds->{ 'MAX-Y' };

        $SOLUTIONS->{ $_ }->{ 'X' } = $x;
        $SOLUTIONS->{ $_ }->{ 'Y' } = $y;
        $SOLUTIONS->{ $_ }->{ 'ANG' } = $ang;

        $seq++;
    }

    printf $fh "grestore } def\n";
}

############################################################
# Find the lenth for this line segment

sub getSegmentLenth {

    my $candidate = shift;
    my $node = shift;
    my $seq = shift;

    return  $_LENTH_FUNCS->{ $LENTH_FUNC }->[ 0 ]->( $candidate, $node, $seq );
}

############################################################
# Return the fixed length value - 10

sub getLenthDefault {

    return 10
}

############################################################
# Return a length value based on the start/end node values

sub getLenthByNodeVal {

    my $candidate = shift;
    my $node = shift;
    my $seq = shift;

    my $val = log( $candidate - 1 ) / log( $SOLUTIONS->{ 'MAX-VALUE' } );

    return 10 * $val;
}

############################################################
# Find the width for this line segment

sub getSegmentWidth {
        
    my $candidate = shift;
    my $node = shift;
    my $seq = shift;

    return  $_WIDTH_FUNCS->{ $WIDTH_FUNC }->[ 0 ]->( $candidate, $node, $seq );
}

############################################################
# Return the fixed width value - 1

sub getWidthDefault {
        
    return 1
}

############################################################
# Find a width value given the number of segment visits

sub getWidthByVisits {
        
    my $candidate = shift;
    my $node = shift;
    my $seq = shift;

    return $SOLUTIONS->{ $node }->{ 'VISITS' } / $SOLUTIONS->{ 1 }->{ 'VISITS' };
}

############################################################
# Find the color for this line segment

sub getSegmentColor {
        
    my $candidate = shift;
    my $node = shift;
    my $seq = shift;

    my $val = $_VALUE_FUNCS->{ $VALUE_FUNC }->[ 0 ]->( $candidate, $node, $seq );

    return $_COLOR_FUNCS->{ $COLOR_FUNC }->[ 0 ]->( $val );
}

############################################################
# Return a fixed line length - 10

sub getLengthDefault {
        
    return 10;
}

############################################################
# Find a length relative to the start/end node values

sub getLengthByNodeDiff {
        
    my $candidate = shift;
    my $node = shift;
    my $seq = shift;

}

############################################################
# Find a value given the number of segment visits

sub getValueByVisits {
        
    my $candidate = shift;
    my $node = shift;
    my $seq = shift;

    return log( $SOLUTIONS->{ $node }->{ 'VISITS' } ) / log ( $SOLUTIONS->{ 1 }->{ 'VISITS' } + 1 );
}

############################################################
# Find the relative length of the trail

sub getValueByTrailLength {

    my $candidate = shift;
    my $node = shift;
    my $seq = shift;

    # Find the longest trail - once
    unless ( $SOLUTIONS->{ 'MAX-TRAIL-LENGTH' } ) {
    
        $SOLUTIONS->{ 'MAX-TRAIL-LENGTH' } = 0;

        for ( 2..$CANDIDATES ) {

            next unless $SOLUTIONS->{ $_ }->{ 'LEAF' };

            my $trail_length = scalar @{ $SOLUTIONS->{ $_ }->{ 'TRAIL' } };

            $SOLUTIONS->{ 'MAX-TRAIL-LENGTH' } =
                    MAX( $SOLUTIONS->{ 'MAX-TRAIL-LENGTH' }, $trail_length );
        }
    }

    return scalar( @{ $SOLUTIONS->{ $candidate }->{ 'TRAIL' } } ) /
                                 ( $SOLUTIONS->{ 'MAX-TRAIL-LENGTH' } + 1 );
}

############################################################
# Select a value according to distance along the spectrum

sub getValueByTrailSequence {

    my $candidate = shift;
    my $node = shift;
    my $seq = shift;

    my $val = $seq / ( scalar @{ $SOLUTIONS->{ $candidate }->{ 'TRAIL' } } + 1 );

    return $val * $val;
}

############################################################
# Label the nodes - do this after the trails have been drawn

sub labelNodes {

    my $fh = shift;

    printf $fh "gsave 1 setgray\n";

    for ( keys %{ $SOLUTIONS } ) {
    
        printf $fh "%f 5 sc div add %f mv (%s) show\n", 
            $SOLUTIONS->{ $_ }->{ 'X' },
            $SOLUTIONS->{ $_ }->{ 'Y' },
            $_;
    }

    printf $fh "grestore\n";
}

############################################################
# Return a value from the spectrum as an array ref [r,g,b]

sub blueGreenColor { 

    my $val = sqrt( shift );    # 0 <= val <= 1

    my $spectrum_limit = 3;

    my $blkwid = 1.0 / $spectrum_limit;

    my $blk = int($spectrum_limit * $val);
    my $from = $_SPECTRUM_COLORS[ $blk ];
    my $to = $_SPECTRUM_COLORS[ $blk + 1 ];

    my $v = $val - $blk * $blkwid;
   
    my $r = ( $from->[0] + ($to->[0] - $from->[0]) * $v / $blkwid ) / 256;
    my $g = ( $from->[1] + ($to->[1] - $from->[1]) * $v / $blkwid ) / 256;
    my $b = ( $from->[2] + ($to->[2] - $from->[2]) * $v / $blkwid ) / 256;

    return ( $r, $g, $b );
}

############################################################
# Return a value from the spectrum as an array ref [r,g,b]

sub fullSpectrumColor { 

    my $val = shift;    # 0 <= val < 1

    my $blkwid = 1.0 / $#_SPECTRUM_COLORS;

    my $blk = int($#_SPECTRUM_COLORS * $val);
    my $from = $_SPECTRUM_COLORS[ $blk ];
    my $to = $_SPECTRUM_COLORS[ $blk + 1 ];

    my $v = $val - $blk * $blkwid;
   
    my $r = ( $from->[0] + ($to->[0] - $from->[0]) * $v / $blkwid ) / 256;
    my $g = ( $from->[1] + ($to->[1] - $from->[1]) * $v / $blkwid ) / 256;
    my $b = ( $from->[2] + ($to->[2] - $from->[2]) * $v / $blkwid ) / 256;

    return ( $r, $g, $b );
}

############################################################
# Return a value from the spectrum as an array ref [r,g,b]

sub blueWhiteColor { 

    my $val = shift;    # 0 <= val < 1

    my $r = 1.0 - $val;
    my $g = 1.0 - $val;
    my $b = 1.0;

    return ( 0.9 * $r * $r, 0.9 * $g * $g, $b );
}

############################################################
# Help doco

sub help {

    print "\n@_\n" if @_;

    my $spectrum_funcs = "\n";
    for ( sort keys %{ $_COLOR_FUNCS } ) {

        my $func_desc = $_COLOR_FUNCS->{ $_ }->[ 1 ];
        $spectrum_funcs .= sprintf( "% 8s% -16s - %s\n", "", $_, $func_desc );
    }

    my $lenth_funcs = "\n";
    for ( sort keys %{ $_LENTH_FUNCS } ) {

        my $func_desc = $_LENTH_FUNCS->{ $_ }->[ 1 ];
        $lenth_funcs .= sprintf( "% 8s% -16s - %s\n", "", $_, $func_desc );
    }

    my $value_funcs = "\n";
    for ( sort keys %{ $_VALUE_FUNCS } ) {

        my $func_desc = $_VALUE_FUNCS->{ $_ }->[ 1 ];
        $value_funcs .= sprintf( "% 8s% -16s - %s\n", "", $_, $func_desc );
    }

    my $width_funcs = "\n";
    for ( sort keys %{ $_WIDTH_FUNCS } ) {

        my $func_desc = $_WIDTH_FUNCS->{ $_ }->[ 1 ];
        $width_funcs .= sprintf( "% 8s% -16s - %s\n", "", $_, $func_desc );
    }

    printf "\n$0 " .
                "[-c <candidates>] [-e <ang>] [-f <file>] [-h] [-k] [-m <val>] " .
                "[-o <ang>] [-p] [-r <ang>] [-s <func>] [-v <func>]  [-w <func>] " .
                "[-D <dep>] [-I] [-L] [-W <wid>]\n
    where
        -c  number of candidates to iterate (default 1000)
        -e  angle for even functions (default $DEFAULT_EVEN_ANG)
        -f  output file (default $DEFAULT_OUTPUT_FILE)
        -h  this help documnetation
        -k  retain the temp ps file ($PS_TEMP)
        -l  length function to use (default '$DEFAULT_LENTH_FUNC')
        -m  line width multipler (default $DEFAULT_LINE_MULTIPLIER)
        -o  angle for odd functions (default $DEFAULT_ODD_ANG)
        -p  display processing progress
        -r  rotation of the image (default 0)
        -s  color spectrum to use (default '$DEFAULT_COLOR_FUNC')
        -v  value function to use (default '$DEFAULT_VALUE_FUNC')
        -w  width function to use (default '$DEFAULT_WIDTH_FUNC')

        -D  depth of the image (default $DEFAULT_IMAGE_DEP)
        -I  dump the path data
        -L  draw labels
        -W  width of the image (default $DEFAULT_IMAGE_WID)

    available length functions (-l) $lenth_funcs

    available spectrum functions (-s) $spectrum_funcs

    available value functions (-v)  $value_funcs

    available width functions (-w)  $width_funcs
    \n";

    exit;
}

__END__
