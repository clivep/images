#!/usr/bin/perl

use warnings;
use strict;

############################################################
#
#   pinwheel-tiling.pl
#
#   Pinwheel aperiodic tiling of the plane
#
############################################################

use Math::Trig;
use Getopt::Std;

use Data::Dumper;
use Readonly;

############################################################
# Setup the environment

my Readonly $TRIANGLE_TYPE_1    =  'type1';
my Readonly $TRIANGLE_TYPE_2    =  'type2';
my Readonly $TRIANGLE_TYPE_3A   =  'type3a';
my Readonly $TRIANGLE_TYPE_3B   =  'type3b';

my Readonly $PS_TEMP = '/tmp/pinwheel.ps';

my Readonly $THETA = cos( 1 * pi / 7 );
my Readonly $PSI = cos( 2 * pi / 7 );
my Readonly $BETA = cos( 3 * pi / 7 );

my Readonly $DEFAULT_ITERATIONS = 1;

my Readonly $DEFAULT_LAYOUT = 'base';
my Readonly $LAYOUT_MAP = {
        'base' =>  [ 'Single type 1 triangle', \&setupBaseImage ],
        'fan1'  =>  [ 'Fan of type 1 triangles', \&setupFan1Image ],
};

my $IMAGE = {};

############################################################
# Check the runline options

my %options;
getopts( 'hi:kl:CD:ILO:S:W:X:Y:', \%options );

help() if $options{ 'h' };

$IMAGE->{ 'DEST' } = $options{ 'O' } || 'danzer-tiling.pdf';

$IMAGE->{ 'ITERATIONS' } = $options{ 'i' } || $DEFAULT_ITERATIONS;

$IMAGE->{ 'LAYOUT' } = $options{ 'l' } || $DEFAULT_LAYOUT;
help( "Not a supported layout" ) unless $LAYOUT_MAP->{ $IMAGE->{ 'LAYOUT' } };

$IMAGE->{ 'SCALE' } = $options{ 'S' } || 1;
help( "Scale must be >= 1.0" ) if $IMAGE->{ 'SCALE' } < 1;

$IMAGE->{ 'DEPTH' } = $options{ 'W' } || 1000;
$IMAGE->{ 'WIDTH' } = $options{ 'D' } || 1000;

$IMAGE->{ 'X_OFFSET' } = $options{ 'X' } || 0;
$IMAGE->{ 'Y_OFFSET' } = $options{ 'Y' } || 0;

$IMAGE->{ 'TRIANGLES' } = {};
$IMAGE->{ 'VERTICES' } = {};

############################################################
# Process the image

setupImage();
processImage();
drawImage();

exit;

############################################################
# Create a new image vertex and return a ref to it

sub newVertex {

    my $x = shift;
    my $y = shift;

    my $id = keys %{$IMAGE->{ 'VERTICES' }};

    my $vertex = {
        'ID'    =>  $id,
        'X'     =>  $x,
        'Y'     =>  $y,
    };

    $IMAGE->{ 'VERTICES' }->{ $id } = $vertex;

    return $vertex;
}
    
############################################################
# Create a new image triangle and return a ref to it

sub newTriangle {

    my $type = shift;
    my $depth = shift;
    my $nodes = shift;

    my $id = 1 + keys %{$IMAGE->{ 'TRIANGLES' }};

    my ( $n0, $n1, $n2 ) = @{ $nodes };
    my $cx = ( $n0->{ 'X' } + $n1->{ 'X' } + $n2->{ 'X' } ) / 3;
    my $cy = ( $n0->{ 'Y' } + $n1->{ 'Y' } + $n2->{ 'Y' } ) / 3;

    my $triangle = {
        'ID'        =>  $id,
        'TYPE'      =>  $type,
        'DEPTH'     =>  $depth,
        'NODES'     =>  $nodes,
        'CENTROID'  =>  newVertex( $cx, $cy ),
    };

    $IMAGE->{ 'TRIANGLES' }->{ $id } = $triangle;

    return $triangle;
}
    
############################################################
# Return the distance between 2 vertices

sub distance {

    my $v1 = shift;
    my $v2 = shift;

    return sqrt(
        ( ( $v1->{ 'X' } - $v2->{ 'X' } ) * ( $v1->{ 'X' } - $v2->{ 'X' } ) ) +
        ( ( $v1->{ 'Y' } - $v2->{ 'Y' } ) * ( $v1->{ 'Y' } - $v2->{ 'Y' } ) )
    );
}

############################################################
# Setup the image with a single triangle

sub setupBaseImage {

    my $nodes = [
        newVertex( 500, 60 + 150 / tan( pi / 18 ) ),
        newVertex( 350, 60 ),
        newVertex( 650, 60 ),
    ];

    newTriangle( $TRIANGLE_TYPE_1, 0, $nodes );

    my $a = distance( $nodes->[ 0 ], $nodes->[ 1 ] );
    my $b = distance( $nodes->[ 1 ], $nodes->[ 2 ] );

    my $v = $b / sin( pi / 3 );

    my $c = $v * sin( 5 * pi / 9 );
    my $d = $v * sin( pi / 9 );

    $IMAGE->{ 'SIDES' } = {
        'A'     =>  $a,
        'B'     =>  $b,
        'C'     =>  $c,
        'D'     =>  $d,
        'SCALE' =>  2 * sin( pi / 18 ),
    };
}

############################################################
# Setup the image with a fan of type 1 triangles

sub setupFan1Image {

    my $da = pi / 7;

    my $c = 400;
    my $b = 2 * $c * sin( $da / 2 );

    my $a = $b / ( 4 * $BETA * $PSI );

    $IMAGE->{ 'SIDES' } = {
        'A'     =>  $a,
        'B'     =>  $b,
        'C'     =>  $c,
        'SCALE' =>  2 * $PSI / ( 4 * $PSI + 1 ),
    };
}

############################################################
# Setup the initial image and it's bounds

sub setupImage {

    # Call the (validated) setup function
    $LAYOUT_MAP->{ $IMAGE->{ 'LAYOUT' } }->[ 1 ]->();

    my @bounds;
    for my $vid ( keys %{ $IMAGE->{ 'VERTICES' } } ) {
    
        my $vertex = $IMAGE->{ 'VERTICES' }->{ $vid };

        unless ( @bounds ) {
            @bounds = (
                $vertex->{ 'X' },
                $vertex->{ 'Y' },
                $vertex->{ 'X' },
                $vertex->{ 'Y' },
            );
            next;
        }
        
        $bounds[ 0 ] = $vertex->{ 'X' } if $vertex->{ 'X' } < $bounds[ 0 ];
        $bounds[ 1 ] = $vertex->{ 'Y' } if $vertex->{ 'Y' } < $bounds[ 1 ];
        $bounds[ 2 ] = $vertex->{ 'X' } if $vertex->{ 'X' } > $bounds[ 2 ];
        $bounds[ 3 ] = $vertex->{ 'Y' } if $vertex->{ 'Y' } > $bounds[ 3 ];
    }   

    map { $_ *= $IMAGE->{ 'SCALE' } } ( @bounds );

    $IMAGE->{ 'BOUNDS' } = \@bounds;
}

############################################################
# Inflate a type 1 triangle

sub inflateType1Triangle {

    my $triangle = shift;

    my ( $n0, $n1, $n2 ) = @{ $triangle->{ 'NODES' } };

    my $a = $IMAGE->{ 'SIDES' }->{ 'A' };
    my $b = $IMAGE->{ 'SIDES' }->{ 'B' };
    my $c = $IMAGE->{ 'SIDES' }->{ 'C' };

    my $d1 = distance( $n0, $n1 );
    my $d2 = distance( $n1, $n2 );
    my $d3 = distance( $n2, $n0 );

    my $v1 = newVertex(
        $n0->{ 'X' } + $a * ( $n1->{ 'X' } - $n0->{ 'X' } ) / $d1,
        $n0->{ 'Y' } + $a * ( $n1->{ 'Y' } - $n0->{ 'Y' } ) / $d1,
    );

    my $v2 = newVertex(
        $n0->{ 'X' } + $a * ( $n2->{ 'X' } - $n0->{ 'X' } ) / $d3,
        $n0->{ 'Y' } + $a * ( $n2->{ 'Y' } - $n0->{ 'Y' } ) / $d3,
    );

    my $v3 = newVertex(
        $n1->{ 'X' } + $a * ( $n0->{ 'X' } - $n1->{ 'X' } ) / $d1,
        $n1->{ 'Y' } + $a * ( $n0->{ 'Y' } - $n1->{ 'Y' } ) / $d1,
    );

    my $v4 = newVertex(
        $n2->{ 'X' } + $b * ( $n0->{ 'X' } - $n2->{ 'X' } ) / $d3,
        $n2->{ 'Y' } + $b * ( $n0->{ 'Y' } - $n2->{ 'Y' } ) / $d3,
    );

    my $v5 = newVertex(
        $n2->{ 'X' } + ( $b + $c ) * ( $n0->{ 'X' } - $n2->{ 'X' } ) / $d3,
        $n2->{ 'Y' } + ( $b + $c ) * ( $n0->{ 'Y' } - $n2->{ 'Y' } ) / $d3,
    );

    my $d4 = distance( $v3, $v5 );
    my $v6 = newVertex(
        $v5->{ 'X' } + $c * ( $v3->{ 'X' } - $v5->{ 'X' } ) / $d4,
        $v5->{ 'Y' } + $c * ( $v3->{ 'Y' } - $v5->{ 'Y' } ) / $d4,
    );

    newTriangle( $TRIANGLE_TYPE_1, $triangle->{ 'DEPTH' } + 1, [ $n0, $v1, $v2 ] );
    newTriangle( $TRIANGLE_TYPE_2, $triangle->{ 'DEPTH' } + 1, [ $v3, $v2, $v1 ] );
    newTriangle( $TRIANGLE_TYPE_1, $triangle->{ 'DEPTH' } + 1, [ $n1, $n2, $v4 ] );
    newTriangle( $TRIANGLE_TYPE_3B, $triangle->{ 'DEPTH' } + 1, [ $n1, $v4, $v5 ] );
    newTriangle( $TRIANGLE_TYPE_3B, $triangle->{ 'DEPTH' } + 1, [ $n1, $v6, $v5 ] );
    newTriangle( $TRIANGLE_TYPE_1, $triangle->{ 'DEPTH' } + 1, [ $n1, $v3, $v6 ] );
    newTriangle( $TRIANGLE_TYPE_1, $triangle->{ 'DEPTH' } + 1, [ $v2, $v3, $v6 ] );
    newTriangle( $TRIANGLE_TYPE_3B, $triangle->{ 'DEPTH' } + 1, [ $v2, $v6, $v5 ] );
}

############################################################
# Inflate a type 2 triangle

sub inflateType2Triangle {

    my $triangle = shift;

    my ( $n0, $n1, $n2 ) = @{ $triangle->{ 'NODES' } };

    my $a = $IMAGE->{ 'SIDES' }->{ 'A' };
    my $b = $IMAGE->{ 'SIDES' }->{ 'B' };
    my $c = $IMAGE->{ 'SIDES' }->{ 'C' };

    my $d1 = distance( $n0, $n1 );
    my $d2 = distance( $n1, $n2 );
    my $d3 = distance( $n2, $n0 );

    my $v1 = newVertex(
        $n0->{ 'X' } + $a * ( $n1->{ 'X' } - $n0->{ 'X' } ) / $d1,
        $n0->{ 'Y' } + $a * ( $n1->{ 'Y' } - $n0->{ 'Y' } ) / $d1,
    );

    my $v2 = newVertex(
        $n0->{ 'X' } + $a * ( $n2->{ 'X' } - $n0->{ 'X' } ) / $d3,
        $n0->{ 'Y' } + $a * ( $n2->{ 'Y' } - $n0->{ 'Y' } ) / $d3,
    );

    my $v3 = newVertex(
        $n1->{ 'X' } + $a * ( $n0->{ 'X' } - $n1->{ 'X' } ) / $d1,
        $n1->{ 'Y' } + $a * ( $n0->{ 'Y' } - $n1->{ 'Y' } ) / $d1,
    );

    my $v4 = newVertex(
        $n2->{ 'X' } + $c * ( $n0->{ 'X' } - $n2->{ 'X' } ) / $d3,
        $n2->{ 'Y' } + $c * ( $n0->{ 'Y' } - $n2->{ 'Y' } ) / $d3,
    );

    my $d4 = distance( $v3, $v4 );
    my $v5 = newVertex(
        $v4->{ 'X' } + $c * ( $v3->{ 'X' } - $v4->{ 'X' } ) / $d4,
        $v4->{ 'Y' } + $c * ( $v3->{ 'Y' } - $v4->{ 'Y' } ) / $d4,
    );

    newTriangle( $TRIANGLE_TYPE_1, $triangle->{ 'DEPTH' } + 1, [ $n0, $v1, $v2 ] );
    newTriangle( $TRIANGLE_TYPE_2, $triangle->{ 'DEPTH' } + 1, [ $v3, $v2, $v1 ] );
    newTriangle( $TRIANGLE_TYPE_3A, $triangle->{ 'DEPTH' } + 1, [ $n1, $n2, $v4 ] );
    newTriangle( $TRIANGLE_TYPE_3A, $triangle->{ 'DEPTH' } + 1, [ $n1, $v5, $v4 ] );
    newTriangle( $TRIANGLE_TYPE_1, $triangle->{ 'DEPTH' } + 1, [ $n1, $v5, $v3 ] );
    newTriangle( $TRIANGLE_TYPE_1, $triangle->{ 'DEPTH' } + 1, [ $v2, $v3, $v5 ] );
    newTriangle( $TRIANGLE_TYPE_3A, $triangle->{ 'DEPTH' } + 1, [ $v2, $v4, $v5 ] );
}

############################################################
# Inflate a type 3A triangle

sub inflateType3ATriangle {

    my $triangle = shift;

    my ( $n0, $n1, $n2 ) = @{ $triangle->{ 'NODES' } };

    my $a = $IMAGE->{ 'SIDES' }->{ 'A' };
    my $b = $IMAGE->{ 'SIDES' }->{ 'B' };
    my $c = $IMAGE->{ 'SIDES' }->{ 'C' };

    my $d1 = distance( $n0, $n1 );
    my $d2 = distance( $n1, $n2 );
    my $d3 = distance( $n2, $n0 );

    my $v1 = newVertex(
        $n0->{ 'X' } + $a * ( $n1->{ 'X' } - $n0->{ 'X' } ) / $d1,
        $n0->{ 'Y' } + $a * ( $n1->{ 'Y' } - $n0->{ 'Y' } ) / $d1,
    );

    my $v2 = newVertex(
        $n0->{ 'X' } + $a * ( $n2->{ 'X' } - $n0->{ 'X' } ) / $d3,
        $n0->{ 'Y' } + $a * ( $n2->{ 'Y' } - $n0->{ 'Y' } ) / $d3,
    );

    my $v3 = newVertex(
        $n1->{ 'X' } + $a * ( $n0->{ 'X' } - $n1->{ 'X' } ) / $d1,
        $n1->{ 'Y' } + $a * ( $n0->{ 'Y' } - $n1->{ 'Y' } ) / $d1,
    );

    my $v4 = newVertex(
        $n2->{ 'X' } + $c * ( $n0->{ 'X' } - $n2->{ 'X' } ) / $d3,
        $n2->{ 'Y' } + $c * ( $n0->{ 'Y' } - $n2->{ 'Y' } ) / $d3,
    );

    my $v5 = newVertex(
        $n2->{ 'X' } + ( $b + $c ) * ( $n0->{ 'X' } - $n2->{ 'X' } ) / $d3,
        $n2->{ 'Y' } + ( $b + $c ) * ( $n0->{ 'Y' } - $n2->{ 'Y' } ) / $d3,
    );

    my $v6 = newVertex(
        $n2->{ 'X' } + ( $b + 2 * $c ) * ( $n0->{ 'X' } - $n2->{ 'X' } ) / $d3,
        $n2->{ 'Y' } + ( $b + 2 * $c ) * ( $n0->{ 'Y' } - $n2->{ 'Y' } ) / $d3,
    );

    my $d4 = distance( $v3, $v6 );
    my $v7 = newVertex(
        $v6->{ 'X' } + $c * ( $v3->{ 'X' } - $v6->{ 'X' } ) / $d4,
        $v6->{ 'Y' } + $c * ( $v3->{ 'Y' } - $v6->{ 'Y' } ) / $d4,
    );

    newTriangle( $TRIANGLE_TYPE_1, $triangle->{ 'DEPTH' } + 1, [ $n0, $v1, $v2 ] );
    newTriangle( $TRIANGLE_TYPE_2, $triangle->{ 'DEPTH' } + 1, [ $v3, $v2, $v1 ] );
    newTriangle( $TRIANGLE_TYPE_1, $triangle->{ 'DEPTH' } + 1, [ $v2, $v3, $v7 ] );
    newTriangle( $TRIANGLE_TYPE_3A, $triangle->{ 'DEPTH' } + 1, [ $v2, $v6, $v7 ] );
    newTriangle( $TRIANGLE_TYPE_1, $triangle->{ 'DEPTH' } + 1, [ $n1, $v7, $v3 ] );
    newTriangle( $TRIANGLE_TYPE_3A, $triangle->{ 'DEPTH' } + 1, [ $n1, $v6, $v7 ] );
    newTriangle( $TRIANGLE_TYPE_3A, $triangle->{ 'DEPTH' } + 1, [ $n1, $v5, $v6 ] );
    newTriangle( $TRIANGLE_TYPE_1, $triangle->{ 'DEPTH' } + 1, [ $n1, $v4, $v5 ] );
    newTriangle( $TRIANGLE_TYPE_3A, $triangle->{ 'DEPTH' } + 1, [ $n1, $v4, $n2 ] );
}

############################################################
# Inflate a type 3B triangle

sub inflateType3BTriangle {

    my $triangle = shift;

    my ( $n0, $n1, $n2 ) = @{ $triangle->{ 'NODES' } };

    my $a = $IMAGE->{ 'SIDES' }->{ 'A' };
    my $b = $IMAGE->{ 'SIDES' }->{ 'B' };
    my $c = $IMAGE->{ 'SIDES' }->{ 'C' };

    my $d1 = distance( $n0, $n1 );
    my $d2 = distance( $n1, $n2 );
    my $d3 = distance( $n2, $n0 );

    my $v1 = newVertex(
        $n0->{ 'X' } + $a * ( $n1->{ 'X' } - $n0->{ 'X' } ) / $d1,
        $n0->{ 'Y' } + $a * ( $n1->{ 'Y' } - $n0->{ 'Y' } ) / $d1,
    );

    my $v2 = newVertex(
        $n0->{ 'X' } + $a * ( $n2->{ 'X' } - $n0->{ 'X' } ) / $d3,
        $n0->{ 'Y' } + $a * ( $n2->{ 'Y' } - $n0->{ 'Y' } ) / $d3,
    );

    my $v3 = newVertex(
        $n1->{ 'X' } + ( $b + $c ) * ( $n0->{ 'X' } - $n1->{ 'X' } ) / $d1,
        $n1->{ 'Y' } + ( $b + $c ) * ( $n0->{ 'Y' } - $n1->{ 'Y' } ) / $d1,
    );

    my $v4 = newVertex(
        $n1->{ 'X' } + $b * ( $n0->{ 'X' } - $n1->{ 'X' } ) / $d1,
        $n1->{ 'Y' } + $b * ( $n0->{ 'Y' } - $n1->{ 'Y' } ) / $d1,
    );

    my $v5 = newVertex(
        $n2->{ 'X' } + $c * ( $n0->{ 'X' } - $n2->{ 'X' } ) / $d3,
        $n2->{ 'Y' } + $c * ( $n0->{ 'Y' } - $n2->{ 'Y' } ) / $d3,
    );

    my $v6 = newVertex(
        $n2->{ 'X' } + ( $a + $c ) * ( $n0->{ 'X' } - $n2->{ 'X' } ) / $d3,
        $n2->{ 'Y' } + ( $a + $c ) * ( $n0->{ 'Y' } - $n2->{ 'Y' } ) / $d3,
    );

    my $d4 = distance( $v3, $v6 );
    my $v7 = newVertex(
        $v6->{ 'X' } + $b * ( $v3->{ 'X' } - $v6->{ 'X' } ) / $d4,
        $v6->{ 'Y' } + $b * ( $v3->{ 'Y' } - $v6->{ 'Y' } ) / $d4,
    );

    newTriangle( $TRIANGLE_TYPE_1, $triangle->{ 'DEPTH' } + 1, [ $n0, $v1, $v2 ] );
    newTriangle( $TRIANGLE_TYPE_3A, $triangle->{ 'DEPTH' } + 1, [ $v1, $v7, $v3 ] );
    newTriangle( $TRIANGLE_TYPE_1, $triangle->{ 'DEPTH' } + 1, [ $v1, $v7, $v6 ] );
    newTriangle( $TRIANGLE_TYPE_2, $triangle->{ 'DEPTH' } + 1, [ $v6, $v2, $v1 ] );
    newTriangle( $TRIANGLE_TYPE_3A, $triangle->{ 'DEPTH' } + 1, [ $v5, $v7, $v3 ] );
    newTriangle( $TRIANGLE_TYPE_3A, $triangle->{ 'DEPTH' } + 1, [ $v5, $v3, $v4 ] );
    newTriangle( $TRIANGLE_TYPE_1, $triangle->{ 'DEPTH' } + 1, [ $v5, $v7, $v6 ] );
    newTriangle( $TRIANGLE_TYPE_1, $triangle->{ 'DEPTH' } + 1, [ $v5, $n1, $v4 ] );
    newTriangle( $TRIANGLE_TYPE_3A, $triangle->{ 'DEPTH' } + 1, [ $n1, $v5, $n2 ] );
}

############################################################
# Inflate the image triangles 

sub inflateImage {

    my $depth = shift;

    $IMAGE->{ 'SIDES' }->{ 'A' } *= $IMAGE->{ 'SIDES' }->{ 'SCALE' };
    $IMAGE->{ 'SIDES' }->{ 'B' } *= $IMAGE->{ 'SIDES' }->{ 'SCALE' };
    $IMAGE->{ 'SIDES' }->{ 'C' } *= $IMAGE->{ 'SIDES' }->{ 'SCALE' };
    $IMAGE->{ 'SIDES' }->{ 'D' } *= $IMAGE->{ 'SIDES' }->{ 'SCALE' };

    for my $tid ( keys %{$IMAGE->{ 'TRIANGLES' }} ) {

        my $triangle = $IMAGE->{ 'TRIANGLES' }->{ $tid };
        next unless $triangle->{ 'DEPTH' } == $depth;
   
        for ( $triangle->{ 'TYPE' } ) {

            /$TRIANGLE_TYPE_1/  && do { inflateType1Triangle( $triangle ); last };
            /$TRIANGLE_TYPE_2/  && do { inflateType2Triangle( $triangle ); last };
            /$TRIANGLE_TYPE_3A/  && do { inflateType3ATriangle( $triangle ); last };
            /$TRIANGLE_TYPE_3B/  && do { inflateType3BTriangle( $triangle ); last };
        }
    }
}

############################################################
# Process the image

sub processImage {

    for ( my $i = 0; $i < $IMAGE->{ 'ITERATIONS' }; $i++ ) {
        inflateImage( $i );
    }

    printf Dumper $IMAGE if $options{ 'I' };
}

############################################################
# Write the image header

sub writeImageHeader {

    my $fh = shift;

    printf $fh "%%!PS\n%%%%BoundingBox: 0 0 %f %f\n%%%%EndComments\n

        /imgw %f def
        /imgd %f def

        <<
            /PageSize [ imgw imgd ]
            /ImagingBBox null
        >> setpagedevice

        /mv { %f mul exch %f mul exch moveto } bind def
        /ln { %f mul exch %f mul exch lineto } bind def

        /Arial findfont 14 scalefont setfont 

        0 0 mv imgw 0 ln imgw imgd ln 0 imgd ln fill

        0 0 moveto
        90 rotate
        0 -1000 translate 
        %f %f translate
        %f %f translate

        1 setlinejoin
        1 1 0 setrgbcolor
    ",
        ( $IMAGE->{ 'WIDTH' }, $IMAGE->{ 'DEPTH' } )x2,
        ( $IMAGE->{ 'SCALE' } )x4,
        ( $IMAGE->{ 'WIDTH' } - $IMAGE->{ 'BOUNDS' }->[ 0 ] - $IMAGE->{ 'BOUNDS' }->[ 2 ] ) / 2,
        ( $IMAGE->{ 'DEPTH' } - $IMAGE->{ 'BOUNDS' }->[ 1 ] - $IMAGE->{ 'BOUNDS' }->[ 3 ] ) / 2,
        $IMAGE->{ 'X_OFFSET' } * -1, $IMAGE->{ 'Y_OFFSET' } * -1,
    ;

    return;
}

############################################################
# Draw and fill the image triangles

sub colorTriangles {

    my $fh = shift;
    my $depth = shift || 0;

    for my $tid ( keys %{$IMAGE->{ 'TRIANGLES' }} ) {

        my $triangle = $IMAGE->{ 'TRIANGLES' }->{ $tid };
                next unless $triangle->{ 'DEPTH' } == $depth;

        my ( $n0, $n1, $n2 ) = @{ $triangle->{ 'NODES' } };
        my $c = $triangle->{ 'CENTROID' };

        if ( $triangle->{ 'TYPE' } eq $TRIANGLE_TYPE_1 ) {

            printf $fh "%f %f mv ", $n0->{ 'X' }, $n0->{ 'Y' };
            printf $fh "%f %f ln ", $n1->{ 'X' }, $n1->{ 'Y' };
            printf $fh "%f %f ln ", $n2->{ 'X' }, $n2->{ 'Y' };
            printf $fh "1 1 0 setrgbcolor fill\n";
            next;

            printf $fh "%f %f mv ", $n0->{ 'X' }, $n0->{ 'Y' };
            printf $fh "%f %f ln ", $n1->{ 'X' }, $n1->{ 'Y' };
            printf $fh "%f %f ln ", $c->{ 'X' }, $c->{ 'Y' };
            printf $fh "%f %f ln ", $n2->{ 'X' }, $n2->{ 'Y' };
            printf $fh "1 1 0 setrgbcolor fill\n";

            printf $fh "%f %f mv ", $c->{ 'X' }, $c->{ 'Y' };
            printf $fh "%f %f ln ", $n1->{ 'X' }, $n1->{ 'Y' };
            printf $fh "%f %f ln ", $n2->{ 'X' }, $n2->{ 'Y' };
            printf $fh "1 0 1 setrgbcolor fill\n";
        }
        elsif ( $triangle->{ 'TYPE' } eq $TRIANGLE_TYPE_2 ) {

            printf $fh "%f %f mv ", $n0->{ 'X' }, $n0->{ 'Y' };
            printf $fh "%f %f ln ", $n1->{ 'X' }, $n1->{ 'Y' };
            printf $fh "%f %f ln ", $n2->{ 'X' }, $n2->{ 'Y' };
            printf $fh "0 1 0 setrgbcolor fill\n";
            next;

            printf $fh "%f %f mv ", $c->{ 'X' }, $c->{ 'Y' };
            printf $fh "%f %f ln ", $n0->{ 'X' }, $n0->{ 'Y' };
            printf $fh "%f %f ln ", $n1->{ 'X' }, $n1->{ 'Y' };
            printf $fh "1 1 0 setrgbcolor fill\n";

            printf $fh "%f %f mv ", $c->{ 'X' }, $c->{ 'Y' };
            printf $fh "%f %f ln ", $n1->{ 'X' }, $n1->{ 'Y' };
            printf $fh "%f %f ln ", $n2->{ 'X' }, $n2->{ 'Y' };
            printf $fh "1 0 1 setrgbcolor fill\n";

            printf $fh "%f %f mv ", $c->{ 'X' }, $c->{ 'Y' };
            printf $fh "%f %f ln ", $n2->{ 'X' }, $n2->{ 'Y' };
            printf $fh "%f %f ln ", $n0->{ 'X' }, $n0->{ 'Y' };
            printf $fh "0 0 1 setrgbcolor fill\n";
        }
        elsif ( $triangle->{ 'TYPE' } eq $TRIANGLE_TYPE_3A ) {

            printf $fh "%f %f mv ", $n0->{ 'X' }, $n0->{ 'Y' };
            printf $fh "%f %f ln ", $n1->{ 'X' }, $n1->{ 'Y' };
            printf $fh "%f %f ln ", $n2->{ 'X' }, $n2->{ 'Y' };
            printf $fh "0 1 1 setrgbcolor fill\n";
            next;

            printf $fh "%f %f mv ", $c->{ 'X' }, $c->{ 'Y' };
            printf $fh "%f %f ln ", $n0->{ 'X' }, $n0->{ 'Y' };
            printf $fh "%f %f ln ", $n1->{ 'X' }, $n1->{ 'Y' };
            printf $fh "1 1 0 setrgbcolor fill\n";

            printf $fh "%f %f mv ", $c->{ 'X' }, $c->{ 'Y' };
            printf $fh "%f %f ln ", $n1->{ 'X' }, $n1->{ 'Y' };
            printf $fh "%f %f ln ", $n2->{ 'X' }, $n2->{ 'Y' };
            printf $fh "1 0 1 setrgbcolor fill\n";

            printf $fh "%f %f mv ", $c->{ 'X' }, $c->{ 'Y' };
            printf $fh "%f %f ln ", $n2->{ 'X' }, $n2->{ 'Y' };
            printf $fh "%f %f ln ", $n0->{ 'X' }, $n0->{ 'Y' };
            printf $fh "0 1 1 setrgbcolor fill\n";
        }
        elsif ( $triangle->{ 'TYPE' } eq $TRIANGLE_TYPE_3B ) {

            printf $fh "%f %f mv ", $n0->{ 'X' }, $n0->{ 'Y' };
            printf $fh "%f %f ln ", $n1->{ 'X' }, $n1->{ 'Y' };
            printf $fh "%f %f ln ", $n2->{ 'X' }, $n2->{ 'Y' };
            printf $fh "0 1 1 setrgbcolor fill\n";
        }
    }
}

############################################################
# Draw the image triangles

sub drawTriangles {

    my $fh = shift;
    my $depth = shift || 0;

    for my $tid ( keys %{$IMAGE->{ 'TRIANGLES' }} ) {

        my $triangle = $IMAGE->{ 'TRIANGLES' }->{ $tid };
        next unless $triangle->{ 'DEPTH' } == $depth;

        my ( $n0, $n1, $n2 ) = @{ $triangle->{ 'NODES' } };

        printf $fh "%f %f mv ", $n0->{ 'X' }, $n0->{ 'Y' };
        printf $fh "%f %f ln ", $n1->{ 'X' }, $n1->{ 'Y' };
        printf $fh "%f %f ln ", $n2->{ 'X' }, $n2->{ 'Y' };
        printf $fh "%f %f ln ", $n0->{ 'X' }, $n0->{ 'Y' };

        printf $fh "0.2 setlinewidth\n";
        printf $fh "%s setrgbcolor stroke\n", $options{ 'C' } ? '0 0 0' : '1 1 0';

        if ( $options{ 'L' } ) {

            printf $fh "%f 10 add %f 10 add mv ", $n0->{ 'X' }, $n0->{ 'Y' };
            printf $fh "(%s) 1 setgray show\n", $n0->{ 'ID' };
            printf $fh "%f 10 add %f 10 add mv ", $n1->{ 'X' }, $n1->{ 'Y' };
            printf $fh "(%s) 1 setgray show\n", $n1->{ 'ID' };
            printf $fh "%f 10 add %f 10 add mv ", $n2->{ 'X' }, $n2->{ 'Y' };
            printf $fh "(%s) 1 setgray show\n", $n2->{ 'ID' };

            my $cx = ( $n0->{ 'X' } + $n1->{ 'X' } + $n2->{ 'X' } ) / 3;
            my $cy = ( $n0->{ 'Y' } + $n1->{ 'Y' } + $n2->{ 'Y' } ) / 3;
            printf $fh "%f %f mv ", $cx, $cy;
            printf $fh "(%s) 0 1 1 setrgbcolor show\n", $tid;
        }
    }
}

############################################################
# Write the image details

sub writeImageDetail {

    my $fh = shift;

    if ( $options{ 'C' } ) {
    
        colorTriangles( $fh, $IMAGE->{ 'ITERATIONS' } );
    }

    drawTriangles( $fh, $IMAGE->{ 'ITERATIONS' } );

    return;
}

############################################################
# Draw the image

sub drawImage {

    open my $fh, '>', $PS_TEMP or do {
        printf "Could not open %s: $!\n", $PS_TEMP;
        exit -1;
    };

    writeImageHeader( $fh );
    writeImageDetail( $fh );

    close $fh;

    my $convert = "/usr/local/bin/convert";
    $convert = "/usr/bin/convert" unless ( -f $convert );
    $convert = "/Applications/ImageMagick/ImageMagick-6.6.5/bin/convert" unless ( -f $convert );

    unless ( -f $convert ) {

        printf "Could not locate convert at $convert\n";
    }
    else {

        unlink $IMAGE->{ 'DEST' };
        my $cmd = sprintf "$convert '%s' '%s'", $PS_TEMP, $IMAGE->{ 'DEST' };
        system( $cmd );
    }

    if ( $options{ 'k' } ) {
        printf "Kept $PS_TEMP\n";
    }
    else {
        unlink $PS_TEMP 
    }

    return;
}

############################################################
# Help doco

sub help {

    print "\n@_\n" if @_;

    my $layouts = "";
    map {
        $layouts .= sprintf "% 14s - %s\n", $_, $LAYOUT_MAP->{ $_ }->[ 0 ];
    } ( keys $LAYOUT_MAP );

    printf "\n$0 " .
                "[-h] [-i <iters>] [-k] [-l <layout>] " .
                "[-C] [-D <depth>] [-F <file>] [-I] [-L] [-S <scale>] [-W <width>] " .
                "[-X <xoff>] [-Y <yoff>] " .
    "
        where
            -h          this help message
            -i <iters>  iteration depth (default $DEFAULT_ITERATIONS)
            -k          retain the temp ps file ($PS_TEMP)
            -l <layout> initial image layout (default $DEFAULT_LAYOUT)

            -C          color the image
            -D <depth>  image depth (default 1000)
            -I          dump the image info
            -L          label the image
            -O <file>   output file (default 'template.png')
            -S <scale>  drawing scale (default 1)
            -W <width>  image width (default 1000)
            -X <xoff>   horizontal image offset (default 0)
            -Y <yoff>   vertical image offset (default 0)

    Available image layouts:
    \n$layouts
    \n";

    exit;
}

__END__

https://www.mathpages.com/home/kmath539/kmath539.htm
