#!/usr/bin/perl

use warnings;
use strict;

############################################################
#
#   make-ray-config.pl
#
#   Convert a data dump from apollonian.pl to a ray tracer
#   config file
#
############################################################

use Getopt::Std;

############################################################
# Check the runline options

my %options;
getopts( 'hm:', \%options );

help() if $options{ 'h' };
help() if $#ARGV != 1;

my $min_rad = $options{ 'm' } || 0.5;

open my $src, '<', $ARGV[ 0 ] or die "Could not open $ARGV[ 0 ]: $!\n";
open my $dst, '>', $ARGV[ 1 ] or die "Could not open $ARGV[ 1 ]: $!\n";

############################################################
# Process the config header from the _DATA_ section

while ( my $ln = <DATA> ) {
    print $dst $ln;
}

############################################################
# Now run out the modified data lines

while ( my $ln = <$src> ) {

    chomp $ln;

    last if ( $ln eq "# Reflectors:" );

    my ( $x, $y, $r ) = split( /\s/, $ln );

    next unless $r;
    next if ( $r < $min_rad );
    next if ( $r == 450 );

    printf $dst "    object:
        type: sphere
        surface: default
        centre:
            i: $x
            j: $y
            k: 0.000000
        radius: $r
        color-name: grey8\n"
}

close $src;
close $dst;

exit;

############################################################
# Help doco

sub help {

    print "\n@_\n" if @_;

    printf "\n$0 [-h] [-m <rad>] <dat file> <cfg file>
    
        where
            -h          this help message
            -m <rad>    minimum radius to consider

    \n";

    exit;
}

__DATA__

defaults:
    background:
        red: 0.0
        green: 0.0
        blue: 0.0
    properties:
        anti-alias: on
        diffusion_index: 1.0
        specular_index: 0.0
        specular_exponent: 2.0
        color_scramble: 0.00

screen:
    centre:
        i: 10000
        j: 0
        k: 10000
    offset:
        i: 0
        j: 0
        k: 0
    eye_dist: 140
    height: 10
    width: 10

custom-color-defs:
    name: grey8
        red: 0.80
        green: 0.80
        blue: 0.80
    name: yellow
        red: 0.90
        green: 0.90
        blue: 0.00

image:
    height: 1000
    width: 1000
    units: PIX
    resolution: 1

illumination:
    ambience: 0.3
    lamp:
        brightness: 80
        sample_size: 10
        centre:
            i: 10000
            j: 10000
            k: 20000
        radius: 5

    lamp:
        brightness: 10
        sample_size: 10
        centre:
            i: 0000
            j: 0000
            k: 20000
        radius: 5

objects:
