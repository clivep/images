#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "fractal-utils.h"
#include "fractal-draw.h"

/*******************************************************************************
 *
 * fractal-draw.c
 *
 *  Functions to render fractal images
 *
 ******************************************************************************/

#define GENERATOR                       "fractal"

#define DEFAULT_RENDERER                "set"
#define DEFAULT_SCHEME                  "black-blue"

#define DEFAULT_IMAGE_BACKGROUND        "000000"

#define DEFAULT_ALIASING_RATIO          1.0

#define DEFAULT_BRIGHTNESS_MULTIPLIER   2.0

#define DEFAULT_COLOR_SCHEME            "default"

#define SUPPORTED_RENDERERS             { "set", "histogram", NULL }

#define TMP_POSTSCRIPT_FILE             "/tmp/fractal-draw.ps"

#define A4_DEP                          1000
#define A4_WID                          1000

#define BORDER                          25

/*************************************************
 * The display context
 ************************************************/

struct context {

    char            *renderer;
    char            *color_scheme;
    _color          *(*colorScheme)( _image *image, int point );

    char            *source_file;
    char            *dest_file;
    char            *ps_temp;
    char            keep_ps_temp;

    long double     image_pixel_wid;    // width of a pixel in the image space

    char            draw_grid;
    char            draw_outline;
    char            show_info;
    char            show_runline;

    long double     aliasing_ratio;     // contribution of source point (1.0 = no aliasing)

    long double     image_xsz;
    long double     image_ysz;

    char            draw_target;
    long double     target_dx;
    long double     target_dy;
    long double     target_x;
    long double     target_y;

    long double     brightness_multiplier;

    _orientation    orientation;
    int             display_wid;        // images size minus borders
    int             display_dep;

    int             layer;

    int             act_display_wid;    // final image after scaling to fit
    int             act_display_dep;
    int             act_border_wid;     // final borders
    int             act_border_dep;
} context;

/*************************************************
 * setup the default operating context
 ************************************************/
void setupContext() {

    context.renderer = (char *[])SUPPORTED_RENDERERS[ 0 ];
    context.color_scheme = DEFAULT_COLOR_SCHEME;

    context.draw_target = 'N';
    context.draw_grid = 'N';
    context.draw_outline = 'N';

    context.show_runline = 'N';

    context.layer = -1;

    context.aliasing_ratio = DEFAULT_ALIASING_RATIO;

    context.display_wid = A4_WID - 2 * BORDER;
    context.display_dep = A4_DEP - 2 * BORDER;

    context.orientation = ( context.display_wid > context.display_dep ) ?
                                                        LANDSCAPE : PORTRAIT;

    context.brightness_multiplier = DEFAULT_BRIGHTNESS_MULTIPLIER;

    context.act_display_wid = context.display_wid;
    context.act_display_dep = context.display_dep;

    context.act_border_wid = BORDER;
    context.act_border_dep = BORDER;

    context.ps_temp = TMP_POSTSCRIPT_FILE;
    context.keep_ps_temp = 'N';
}

/*************************************************
 * Display image info
 ************************************************/

void showImageInfo(
    _image  *image
) {
    printf( "Version:      %s\n", image->version );
    if ( ( strcmp( image->version, "1.4" ) >= 0 ) && ( *image->description != '\0' ) )
        printf( "Description:  %s\n", image->description );
    printf( "Image centre: (%0.10Lf,%0.10Lf)\n", image->xc, image->yc );
    printf( "Image size:   %0.10Lf\n", image->sz );
    printf( "Escape:       %0.10Lf\n", image->escape );
    printf( "Trajectories: %d\n", image->trajectories );
    printf( "Max Iters:    r: %d g: %d b: %d\n",
                                image->max_iters.buckets[ 0 ], 
                                image->max_iters.buckets[ 1 ], 
                                image->max_iters.buckets[ 2 ] 
    );
    printf( "Max Visits:   r: %d g: %d b: %d\n",
                                image->max_visits.buckets[ 0 ], 
                                image->max_visits.buckets[ 1 ], 
                                image->max_visits.buckets[ 2 ] 
    );
    printf( "Total Visits: r: %d g: %d b: %d\n",
                                image->total_visits.buckets[ 0 ], 
                                image->total_visits.buckets[ 1 ], 
                                image->total_visits.buckets[ 2 ] 
    );
    printf( "Pic size:     %d x %d\n", image->pic_wid, image->pic_dep );
}

/*************************************************
 * Show the runline used to create the image
 ************************************************/

void showRunline(
    _image  *image
) {
    int     dp;

    dp = -1 * (int)( log( image->sz ) / log( 10.0 ) ) + 3;

    printf( "%s -x %.*Lf -y %.*Lf -s %.*Lf -e %Lf -f %s\n",
                GENERATOR,
                dp, ( context.draw_target == 'Y' ) ? context.target_x : image->xc,
                dp, ( context.draw_target == 'Y' ) ? context.target_y : image->yc,
                dp, image->sz,
                image->escape,
                context.source_file
    );
}

/*************************************************
 * Load an integer from a file
 ************************************************/

int loadImageInteger(
    FILE    *fp
) {
    int     val;

    if ( fread( &val, sizeof( int ), 1, fp ) == 0 ) {
        printf( "Could not read integer from file\n" );
        exit( -1 );
    }

    return val;
}

/*************************************************
 * Load a long double from a file
 ************************************************/

long double loadImageDouble(
    FILE        *fp
) {
    long double val;

    if ( fread( &val, sizeof( long double ), 1, fp ) == 0 ) {
        printf( "Could not read long double from file\n" );
        exit( -1 );
    }

    return val;
}

/*************************************************
 * Load a newline terminated string from a file
 ************************************************/

char *loadImageString(
    FILE        *fp
) {
    static char *buf = NULL;
    static int  buflen = 256;
    int         new_buflen;

    if ( ( buf == NULL ) && ( ( buf = NALLOC( buflen, char ) ) == NULL ) )
        logFatal( "Could not calloc() string buf" );

    if ( fgets( buf, buflen - 1, fp ) == NULL )
        logFatal( "Could not fgets() from data file" );

    // If the buffer was filled by the string - double the buffer size
    while ( strlen( buf ) == ( buflen - 1 ) ) {
    
        new_buflen = buflen * 2;

        if ( ( buf = realloc( buf, new_buflen ) ) == NULL )
            logFatal( "Could not realloc() string buf" );

        buf[ new_buflen - 1 ] = '\0';

        if ( fgets( &buf[ buflen - 1 ], new_buflen - 1, fp ) == NULL )
            logFatal( "Could not fgets() from data file" );

        buflen = new_buflen;
    }

    buf[ strlen( buf ) - 1 ] = '\0';    // remove the newline

    return strdup( buf );
}

/*************************************************
 * Load image data from a file
 ************************************************/

_image *loadImageData(
    char    *data_path
) {
    int     data_points;
    _image  *image;
    FILE    *fp;

    if ( ( image = ALLOC( _image ) ) == NULL )
        logFatal( "Could not calloc() image memory" );

    if ( ( fp = fopen( data_path, "r" ) ) == NULL )
        logFatal( "Could not open %s", data_path );

    image->version = loadImageString( fp ); 

    // Not supporting version prior to 1.5 now...
    if ( strcmp( image->version, "1.5" ) < 0 )
        logFatal( "Version %s not supported", image->version );
        
    image->type = loadImageInteger( fp ); 

    image->description = loadImageString( fp ); 

    image->xc = loadImageDouble( fp ); 
    image->yc = loadImageDouble( fp ); 
    image->sz = loadImageDouble( fp ); 

    image->escape = loadImageDouble( fp ); 
    image->trajectories = loadImageInteger( fp ); 

    image->max_image_iters = loadImageInteger( fp ); 

    image->max_iters.buckets[ 0 ] = loadImageInteger( fp ); 
    image->max_iters.buckets[ 1 ] = loadImageInteger( fp ); 
    image->max_iters.buckets[ 2 ] = loadImageInteger( fp ); 

    image->max_visits.buckets[ 0 ] = loadImageInteger( fp ); 
    image->max_visits.buckets[ 1 ] = loadImageInteger( fp ); 
    image->max_visits.buckets[ 2 ] = loadImageInteger( fp ); 

    image->total_visits.buckets[ 0 ] = loadImageInteger( fp ); 
    image->total_visits.buckets[ 1 ] = loadImageInteger( fp ); 
    image->total_visits.buckets[ 2 ] = loadImageInteger( fp ); 

    image->pic_wid = loadImageInteger( fp ); 
    image->pic_dep = loadImageInteger( fp ); 

    data_points = image->pic_wid * image->pic_dep;

    // Only allocate points data if we are rendering an image
    if ( context.dest_file != NULL ) {

        if ( ( image->data = NALLOC( data_points, _visits ) ) == NULL )
            logFatal( "Could not calloc the image->data block" );
    
        fread( image->data, sizeof( _visits ), data_points, fp );
    }

    fclose( fp );

    context.image_pixel_wid = image->sz / (long double)MAX( image->pic_wid, image->pic_dep );

    if ( image->pic_wid > image->pic_dep ) {
        context.image_ysz = image->sz;
        context.image_xsz = image->sz * image->pic_wid / (long double)image->pic_dep;
    }
    else {
        context.image_xsz = image->sz;
        context.image_ysz = image->sz * image->pic_dep / (long double)image->pic_wid;
    }

    if ( ( context.display_dep / context.display_wid ) < ( context.image_ysz / context.image_xsz ) ) {
        context.act_display_dep = context.display_wid * context.image_ysz / context.image_xsz;
        context.act_border_dep = ( A4_DEP - context.act_display_dep ) / 2.0;
    }
    else {
        context.act_display_wid = context.display_dep * context.image_xsz / context.image_ysz;
        context.act_border_wid = ( A4_WID - context.act_display_wid ) / 2.0;
    }

    return image;
}

/*************************************************
 * Check the color scheme
 ************************************************/

void checkColorScheme()
{
    if ( strcmp( context.color_scheme, "default" ) == 0 )
        context.colorScheme = getDefaultColors;
    else if ( strcmp( context.color_scheme, "test" ) == 0 )
        context.colorScheme = getTestColors;
    else
        logFatal( "Failed to find scheme '%s'..?", context.color_scheme );
}

/*************************************************
 * Find the color for the given point
 ************************************************/

void getPointColorStr(
    _image  *image,
    int     point,
    char    *color_str
) {
    _color  *color;

    color = context.colorScheme( image, point );

    sprintf( color_str, "%02x%02x%02x",
                                (int)color->red, 
                                (int)color->green, 
                                (int)color->blue );
}

/*************************************************
 * Find the color using a test method
 ************************************************/

_color *getTestColors(
    _image  *image,
    int     point
) {
    static _color   color;
    _visits         visit_info;

    visit_info = image->data[ point ];

    color.red = 2100 * visit_info.buckets[ 0 ] / image->max_visits.buckets[ 0 ];
    color.green = 2500 * visit_info.buckets[ 1 ] / image->max_visits.buckets[ 1 ];
    color.blue = 3000 * visit_info.buckets[ 2 ] / image->max_visits.buckets[ 2 ];

    if ( color.red > 255 )
        color.red = 255;

    if ( color.green > 255 )
        color.green = 255;

    if ( color.blue > 255 )
        color.blue = 255;

    return &color;
} 

/*************************************************
 * Find the color using a default method
 ************************************************/

_color *getDefaultColors(
    _image          *image,
    int             point
) {
    static _color   color;
    float           value;
    float           multiplier;
    _visits         visit_info;

    visit_info = image->data[ point ];

    multiplier = 255 * context.brightness_multiplier;


    if ( context.layer < 0 ) { 
    
        color.red = MIN( 255, multiplier * visit_info.buckets[ 0 ] / image->max_visits.buckets[ 0 ] );
        color.green = MIN( 255, multiplier * visit_info.buckets[ 1 ] / image->max_visits.buckets[ 1 ] );
        color.blue = MIN( 255, multiplier * visit_info.buckets[ 2 ] / image->max_visits.buckets[ 2 ] );
    
        return &color;
    } 

    value = multiplier * visit_info.buckets[ context.layer ] /
                                image->max_visits.buckets[ context.layer ];

    color.red = color.green = color.blue = MIN( 255, value );

    return &color;
}

/*************************************************
 * Render the image data
 ************************************************/

void renderImageData( _image *image ) {

    if ( strcmp( context.renderer, "set" ) == 0 )
        renderSet( image );
    else
        logFatal( "Failed to run renderer %s..?", context.renderer );
}

/*************************************************
 * Setup the page for rendering
 ************************************************/

FILE *renderSetup(
    _image  *image
) {
    int imgw;
    int imgd;
    FILE    *fp;

    if ( ( fp = fopen( context.ps_temp, "w" ) ) == NULL ) {
        printf( "Could not open image file %s\n", context.ps_temp );
        exit( -1 );
    }
    
    imgw = ( context.orientation == LANDSCAPE ) ? A4_WID : A4_DEP;
    imgd = A4_WID + A4_DEP - imgw;

    fprintf( fp, "%%!PS\n" );
    fprintf( fp, "%%%%BoundingBox: 0 0 %d %d\n", imgw, imgd );
    fprintf( fp, "%%%%EndComments\n" );
    fprintf( fp, "\n" );
    fprintf( fp, "<<\n" );
    fprintf( fp, "  /PageSize [ %d %d ]\n", imgw, imgd );
    fprintf( fp, "  /ImagingBBox null\n" );
    fprintf( fp, ">>\n" );
    fprintf( fp, "setpagedevice\n" );
    fprintf( fp, "\n" );

    fprintf( fp, "/Arial findfont 10 scalefont setfont\n" );

    fprintf( fp, "0 0 moveto 1000 0 lineto 1000 1000 lineto 0 1000 lineto\n" );
    fprintf( fp, "0 setgray fill\n" );
    fprintf( fp, "\n" );

    return fp;
}

/*************************************************
 * Complete the rendering
 ************************************************/

void renderFinish(
    FILE    *fp
) {
    char    buf[ 100 ];

    fprintf( fp, "showpage\n" );

    fclose ( fp );

    sprintf( buf, "convert %s %s", context.ps_temp, context.dest_file );
    system( buf );

    if ( context.keep_ps_temp == 'N' )
        remove( context.ps_temp );
}

/*************************************************
 * Convert an image point to pixel coordinates
 ************************************************/

void imgPoint2Pixel(
    _image      *image,
    long double img_x,
    long double img_y,
    long double *pic_x,
    long double *pic_y
) {
    *pic_x = ( A4_WID / 2.0 ) + context.act_display_wid * ( img_x - image->xc ) / ( 2.0 * context.image_xsz );
    *pic_y = ( A4_DEP / 2.0 ) + context.act_display_dep * ( img_y - image->yc ) / ( 2.0 * context.image_ysz );
}

/*************************************************
 * Draw an oriented  line through the point
 ************************************************/

void drawSetGridLine(
    FILE            *fp, 
    _image          *image,
    long double     img_x,
    long double     img_y,
    _orientation    orientation,
    char            label
) {
    long double     pic_x;
    long double     pic_y;

    imgPoint2Pixel( image, img_x, img_y, &pic_x, &pic_y );

    if ( orientation == LANDSCAPE ) {

        if (
            ( pic_y < context.act_border_dep ) ||
            ( pic_y > ( A4_DEP - context.act_border_dep ) )
        )
            return;

        if ( label == 'Y' ) {
            fprintf( fp, "gsave\n" );
            fprintf( fp, ".7 setgray\n" );
            fprintf( fp, "%d %Lf moveto\n", context.act_border_wid - 10, pic_y );
            fprintf( fp, "90 rotate\n" );
            fprintf( fp, "(%0.10Lf) dup stringwidth pop -2 div 0 rmoveto show\n", img_y );
            fprintf( fp, "grestore\n" );
        }

        fprintf( fp, "%d %Lf moveto\n", context.act_border_wid, pic_y );
        fprintf( fp, "%d 0 rlineto\n", context.act_display_wid );
    }
    else {

        if (
            ( pic_x < context.act_border_wid ) ||
            ( pic_x > ( A4_WID - context.act_border_wid ) )
        )
            return;

        if ( label == 'Y' ) {
            fprintf( fp, ".7 setgray\n" );
            fprintf( fp, "%Lf %d moveto\n", pic_x, context.act_border_dep - 18 );
            fprintf( fp, "(%0.10Lf) dup stringwidth pop -2 div 0 rmoveto show\n", img_x );
        }

        fprintf( fp, "%Lf %d moveto\n", pic_x, context.act_border_dep );
        fprintf( fp, "0 %d rlineto\n", context.act_display_dep );
    }

    fprintf( fp, "%f setgray\n", ( label == 'N' ) ? .5 : .8 );
    fprintf( fp, "0.5 setlinewidth\n" );
    fprintf( fp, "stroke\n" );
}

/*************************************************
 * Draw an outline over some of the set
 ************************************************/

void drawSetOutline(
    _image      *image,
    FILE        *fp
) {
    long double x;
    long double y;
    long double ang;
    long double pic_x;
    long double pic_y;

    // the cardioid
    for ( ang = 0; ang < 360; ang += 0.1 ) {

        x = ( cos( DEG2RAD( ang ) ) / 2.0 ) - ( cos( DEG2RAD( 2.0 * ang ) ) / 4.0 );
        y = ( sin( DEG2RAD( ang ) ) / 2.0 ) - ( sin( DEG2RAD( 2.0 * ang ) ) / 4.0 );

        imgPoint2Pixel( image, x, y, &pic_x, &pic_y );

        fprintf( fp, "%Lf %Lf %s\n", pic_x, pic_y, ( ang == 0 ) ? "moveto" : "lineto" );
    }

    // the p2 bulb
    for ( ang = 0; ang < 360; ang += 0.2 ) {

        x = 0.25 * cos( DEG2RAD( ang ) ) - 1.0;
        y = 0.25 * sin( DEG2RAD( ang ) );

        imgPoint2Pixel( image, x, y, &pic_x, &pic_y );

        fprintf( fp, "%Lf %Lf %s\n", pic_x, pic_y, ( ang == 0 ) ? "moveto" : "lineto" );
    }

    fprintf( fp, "0 1 0 setrgbcolor stroke\n" );
}

/*************************************************
 * Draw a grid over the set
 ************************************************/

void drawSetGrid(
    _image      *image,
    FILE        *fp
) {
    long double step;
    long double value;
    long double pic_x;
    long double pic_y;

    fprintf( fp, "gsave\n" );

    step = getGraticuleSeparation( image->xc, image->yc, context.orientation,
                        context.image_xsz, context.image_ysz, image->sz );

    fprintf( fp, "/Arial findfont 10 scalefont setfont\n" );

    value = step * (long)( -1.0 + ( image->yc - context.image_ysz ) / step );
    drawSetGridLine( fp, image, 0, value, LANDSCAPE, 'Y' );
    drawSetGridLine( fp, image, 0, value + ( step / 2.0 ), LANDSCAPE, 'N' );

    do {
        value += step;

        drawSetGridLine( fp, image, 0, value, LANDSCAPE, 'Y' );
        drawSetGridLine( fp, image, 0, value + ( step / 2.0 ), LANDSCAPE, 'N' );

    } while ( value < ( image->yc + context.image_ysz ) );

    value = step * (long)( -1.0 + ( image->xc - context.image_xsz ) / step );
    drawSetGridLine( fp, image, value, 0, PORTRAIT, 'Y' );
    drawSetGridLine( fp, image, value + ( step / 2.0 ), 0, PORTRAIT, 'N' );

    do {
        value += step;
    
        drawSetGridLine( fp, image, value, 0, PORTRAIT, 'Y' );
        drawSetGridLine( fp, image, value + ( step / 2.0 ), 0, PORTRAIT, 'N' );

    } while ( value < ( image->xc + context.image_xsz ) );

    // Draw the image centre target

    imgPoint2Pixel( image, image->xc, image->yc, &pic_x, &pic_y );

    fprintf( fp, "gsave\n" );
    fprintf( fp, "%Lf %Lf moveto 40 0 rlineto\n", pic_x - 20, pic_y );
    fprintf( fp, "%Lf %Lf moveto 0 40 rlineto\n", pic_x, pic_y - 20 );
    fprintf( fp, "gsave 1 setgray 2 setlinewidth stroke grestore\n" );
    fprintf( fp, "0.5 setlinewidth 1 0 0 setrgbcolor stroke\n" );
    fprintf( fp, "grestore\n" );

    // If requested draw the offset target
    if ( context.draw_target == 'Y' ) {

        context.target_x = image->xc + ( step * context.target_dx );
        context.target_y = image->yc + ( step * context.target_dy );

        imgPoint2Pixel( image, context.target_x, context.target_y, &pic_x, &pic_y );

        fprintf( fp, "gsave\n" );
        fprintf( fp, "%Lf %Lf moveto 40 0 rlineto\n", pic_x - 20, pic_y );
        fprintf( fp, "%Lf %Lf moveto 0 40 rlineto\n", pic_x, pic_y - 20 );
        fprintf( fp, "gsave 1 setgray 2 setlinewidth stroke grestore\n" );
        fprintf( fp, "0.5 setlinewidth 0 1 0 setrgbcolor stroke\n" );
        fprintf( fp, "grestore\n" );
    }

    /* border around the imaging area... 

    fprintf( fp, "%d %d moveto\n", BORDER, BORDER );
    fprintf( fp, "%d %d lineto\n", A4_WID - BORDER, BORDER );
    fprintf( fp, "%d %d lineto\n", A4_WID - BORDER, A4_DEP - BORDER );
    fprintf( fp, "%d %d lineto\n", BORDER, A4_DEP - BORDER );
    fprintf( fp, "%d %d lineto\n", BORDER, BORDER );
    fprintf( fp, "0 1 0 setrgbcolor stroke\n" );
    */

    fprintf( fp, "grestore\n" );
}

/*************************************************
 * Render the set
 ************************************************/
void copyColor (
    _color      *dest,
    _color      *src
) {

    memcpy( dest, src, sizeof( _color ) );
}

/*************************************************
 * Render the set
 ************************************************/

void renderSet(
    _image      *image
) {
    FILE        *fp;
    int         pic_x;
    int         pic_y;
    int         xoff;
    int         yoff;
    int         p;
    long double ratio;
    _color      *color;
    long double scale_x;
    long double scale_y;
    long double scale;
    long double offset_x;
    long double offset_y;
    int         point;
    _color      *aliasing_buf;
    _color      aliased_color;

    if ( ( aliasing_buf = NALLOC( image->pic_wid * image->pic_dep, _color ) ) == NULL )
        logFatal( "Could not calloc() aliasing buf" );

    fp = renderSetup( image );

    scale_x = context.display_wid / (long double)image->pic_wid;
    scale_y = context.display_dep / (long double)image->pic_dep;

    scale = MIN( scale_x, scale_y );

    offset_x = ( A4_WID - scale * image->pic_wid ) / 2.0;
    offset_y = ( A4_DEP - scale * image->pic_dep ) / 2.0;

    fprintf( fp, "gsave\n" );
    fprintf( fp, "/buf %d string def\n", 6 * image->pic_wid );
    fprintf( fp, "%Lf %Lf translate\n", offset_x, offset_y );
    fprintf( fp, "%d %d scale\n", image->pic_wid, image->pic_dep );
    fprintf( fp, "%Lf dup scale\n", scale );

    fprintf( fp, "%d %d 8\n", image->pic_wid, image->pic_dep );
    fprintf( fp, "[%d 0 0 %d 0 0] {\n", image->pic_wid, image->pic_dep );
    fprintf( fp, "  currentfile buf readhexstring pop\n" );
    fprintf( fp, "} false 3 colorimage\n" );

    for( pic_y = 0; pic_y < image->pic_dep; pic_y++ ) {

        for( pic_x = 0; pic_x < image->pic_wid; pic_x++ ) {

            point = ( pic_y * image->pic_wid ) + pic_x;
            color = context.colorScheme( image, point );
            copyColor( &aliasing_buf[ ( pic_y * image->pic_wid ) + pic_x ], color );
        }

        printf( "\rProcessing - %d%%", (int)(100.0 * pic_y / image->pic_dep ) );
    }

    // Print the aliased values
    for( pic_y = 0; pic_y < image->pic_dep; pic_y++ ) {

        for( pic_x = 0; pic_x < image->pic_wid; pic_x++ ) {

            aliased_color.red = aliased_color.blue = aliased_color.green = 0; 

            for ( p = 0; p < 9; p++ ) {

                xoff = ( p % 3 ) - 1;
                yoff = (int)( p / 3 ) - 1;

                if ( ( pic_x == 0 ) && ( xoff < 0 ) ) {
                    xoff = 0;
                }
                else if ( ( pic_x == image->pic_wid - 1 ) && ( xoff > 0 ) ) {
                    xoff = 0;
                }

                if ( ( pic_y == 0 ) && ( yoff < 0 ) ) {
                    yoff = 0;
                }
                else if ( ( pic_y == image->pic_dep - 1 ) && ( yoff > 0 ) ) {
                    yoff = 0;
                }

                color = &aliasing_buf[ ( ( yoff + pic_y ) * image->pic_wid ) + pic_x + xoff ];
                ratio = ( p == 4 ) ? context.aliasing_ratio : ( 1 - context.aliasing_ratio ) / 8.0;

                aliased_color.red += color->red * ratio;
                aliased_color.blue += color->blue * ratio;
                aliased_color.green += color->green * ratio;
            }

            fprintf( fp, "%02x%02x%02x",
                                    (int)aliased_color.red,
                                    (int)aliased_color.green,
                                    (int)aliased_color.blue );
        }

        printf( "\rAliasing - %d%%  ", (int)(100.0 * pic_y / image->pic_dep ) );

        fflush( stdout );

        fprintf( fp, "\n" );
    }

    fprintf( fp, "grestore\n" );

    if ( context.draw_grid == 'Y' )
        drawSetGrid( image, fp );

    if ( context.draw_outline == 'Y' )
        drawSetOutline( image, fp );

    printf( "\rConverting      " );
    fflush( stdout );

    renderFinish( fp );

    printf( "\rDone           \n" );

    if ( context.draw_target == 'Y' )
        printf( "Target point (%0.20Lf,%0.20Lf)\n", context.target_x, context.target_y );

    if ( context.show_runline == 'Y' )
        showRunline( image );
}

/*************************************************
 * Process any runline parameters
 ************************************************/

void processParameters(
    int     argc,
    char    **argv
) {
    int     pnt = 1;
    char    **check;
    char    expect_dest = 'Y';

    while ( ( pnt < ( argc - 1 ) ) && ( *argv[ pnt ] == '-' ) ) {
    
        if ( strcmp( &argv[ pnt ][ 1 ], "-show-grid" ) == 0 ) {
            context.draw_grid = 'Y';
            pnt++;
            continue;
        }

        if ( strcmp( &argv[ pnt ][ 1 ], "-show-outline" ) == 0 ) {
            context.draw_outline = 'Y';
            pnt++;
            continue;
        }

        if ( strcmp( &argv[ pnt ][ 1 ], "-show-runline" ) == 0 ) {
            context.show_runline = 'Y';
            pnt++;
            continue;
        }

        if ( strcmp( &argv[ pnt ][ 1 ], "-show-info" ) == 0 ) {
            context.show_info = 'Y';
            expect_dest = 'N';
            pnt++;
            continue;
        }

        if ( strcmp( &argv[ pnt ][ 1 ], "dx" ) == 0 ) {
            context.draw_target = 'Y';
            getDoubleParameter( argc, argv, ++pnt, &context.target_dx, "target x offset" );
            pnt++;
            continue;
        }

        if ( strcmp( &argv[ pnt ][ 1 ], "dy" ) == 0 ) {
            context.draw_target = 'Y';
            getDoubleParameter( argc, argv, ++pnt, &context.target_dy, "target y offset" );
            pnt++;
            continue;
        }

        if ( strlen( argv[ pnt ] ) != 2 )
            logFatal( "Unknown switch %s", argv[ pnt ] );

        switch( argv[ pnt ][ 1 ] ) {

            case 'a' :
                getDoubleParameter( argc, argv, ++pnt, &context.aliasing_ratio, "aliasing ratio" );
                if ( ( context.aliasing_ratio <= 0 ) || ( context.aliasing_ratio > 1 ) )
                    logFatal( "Invalid aliasing ratio (0 < a <= 1)" );
                break;

            case 'h' :
                usage( argv[ 0 ], "" );
                break;

            case 'c' :
                getCharParameter( argc, argv, ++pnt, &context.color_scheme, "color scheme" );
                break;

            case 'k' :
                getCharParameter( argc, argv, ++pnt, &context.ps_temp, "temp ps file" );
                context.keep_ps_temp = 'Y';
                break;

            case 'l' :
                getIntParameter( argc, argv, ++pnt, &context.layer, "layer" );
                break;

            case 'm' :
                getDoubleParameter( argc, argv, ++pnt, &context.brightness_multiplier, "brightness multiplier" );
                if ( context.brightness_multiplier <= 0 )
                    logFatal( "Invalid brightness multiplier (m > 0)" );
                break;

            case 'r' :
                getCharParameter( argc, argv, ++pnt, &context.renderer, "renderer" );
                break;

            default:
                logFatal( "Unknown switch %s", argv[ pnt ] );
        }

        pnt++;
    }

    if ( argv[ pnt ][ 1 ] == 'h' )
        usage( argv[ 0 ], "" );

    if ( pnt >= argc )
        logFatal( "Missing data file" );

    context.source_file = argv[ pnt++ ];

    if ( ( expect_dest == 'Y' ) && ( pnt >= argc ) )
        logFatal( "Missing destination image file" );

    if ( pnt < argc )
        context.dest_file = argv[ pnt ];

    // check the given renderer is valid   
    for ( check = (char *[])SUPPORTED_RENDERERS; *check != NULL; check++ ) {
        if ( strcmp( *check, context.renderer ) == 0 )
            break;
    }
    
    if ( *check == NULL ) 
        logFatal( "Not a supported renderer: %s", context.renderer );

    // and check the color scheme...
    checkColorScheme();
}

/*************************************************
 * Usage
 ************************************************/

void usage(
    char    *self,
    char    *message
) {
    
    if ( ( message != NULL ) && ( *message != '\0' ) )
        printf( "%s\n\n", message );

    printf( "Usage: %s [-a <ratio>] [-c <scheme>] [-k <ps_file>] [-l <layer>] ", self );
    printf( "[-dx <x>] [-dy <y>] [-m <mul>] [-r <renderer>] <src file> <dest file>\n" );
    printf( "\n" );
    printf( "   where\n" );
    printf( "       -a <ratio>          aliasing ratio (default aliasing off)\n" );
    printf( "       -c <color scheme>   coloring sceheme to use (default bronze)\n" );
    printf( "       -dx <x>             x offset (from the image centre) for the target\n" );
    printf( "       -dy <y>             y offset (from the image centre) for the target\n" );
    printf( "       -k <ps_temp>        temp ps file (to retain)\n" );
    printf( "       -l <layer>          specific layer to render (default all)\n" );
    printf( "       -m <mul>            brightness multiplier (default 2.0)\n" );
    printf( "       -r <renderer>       type of the renderer (default 'set')\n" );
    printf( "\n" );
    printf( "   supported renderers (and additional options) are\n" );
    printf( "\n" );
    printf( "       'histogram'     graph the distribution of iter counts and distance\n" );
    printf( "\n" );
    printf( "       'set'           draw the image (include points close to the set)\n" );
    printf( "\n" );
    printf( "   supported color schemes are\n" );
    printf( "       'default'       rgb channels matching the iter ranges\n" );
    printf( "       'test'          testing new display algorithms\n" );
    printf( "\n" );
    printf( "   other options\n" );
    printf( "       --show-runline  display the run-line for this image\n" );
    printf( "       --show-grid     overlay the coordinate grid\n" );
    printf( "       --show-outline  draw the set outline\n" );
    printf( "       --show-info     display image metrics\n" );
    printf( "\n" );

    exit( 0 );
}

/*************************************************
 * Main
 ************************************************/

int main(
    int     argc,
    char    **argv
) {
    _image  *image;

    setupContext();

    initialiseLogging( "draw.log", LOGGING_LEVEL_QUIET );

    processParameters( argc, argv );

    image = loadImageData( context.source_file );

    if ( context.show_info == 'Y' )
        showImageInfo( image );

    if ( context.dest_file != '\0' )
        renderImageData( image );

    return 0;
}
